# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.4.14-MariaDB)
# Database: bkd_simgaji2021
# Generation Time: 2022-08-07 15:21:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table simgaji_token
# ------------------------------------------------------------

DROP TABLE IF EXISTS `simgaji_token`;

CREATE TABLE `simgaji_token` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `stakeholder_code` int(1) DEFAULT NULL COMMENT '1 bank jateng, 2 bpjs, 3 disdik',
  `token` text DEFAULT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `simgaji_token` WRITE;
/*!40000 ALTER TABLE `simgaji_token` DISABLE KEYS */;

INSERT INTO `simgaji_token` (`id`, `stakeholder_code`, `token`, `ket`, `created_at`)
VALUES
	(1,1,'a4b0a5db-cf3c-8c08-aa7a-01add61f4fb0','bank jateng','2022-08-07 17:10:04'),
	(2,2,'1b48bb24-a4f0-5e53-4aeb-fb71b959d349','bpjs','2022-08-07 22:19:47'),
	(3,3,'zzz','disdik prov',NULL);

/*!40000 ALTER TABLE `simgaji_token` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
