<?php defined('SYSPATH') or die('No direct script access.');
 
class Controller_Admin_Kalkulasi extends Controller_Admin_Backend {
	public $auth_required = array('login','admin','root');
	
	function action_index(){	
		$this->auto_render = false;	
		
		if($_POST['jenis'] != "GAJI SUSULAN") {
			$periode = $_POST['tahun'];
			
			if(isset($_POST['bulan_id'])) {	
				$periode .= "-".$_POST['bulan_id']."-01";
			}
			
			$arrLokasi = array();
			if($_POST['lokasi_end']) {
				$lokasi_start = ORM::factory('lokasi',$_POST['lokasi_start']);
				$lokasi_end = ORM::factory('lokasi',$_POST['lokasi_end']);
				
				if(substr($lokasi_end->kode,-3) == "000") {
					$max = ORM::factory('lokasi')
						->where('LEFT("kode",2)','=',substr($lokasi_end->kode,0,2))
						->order_by('kode','DESC')
						->find();
				}
				else {
					$max = ORM::factory('lokasi')
						->where('LEFT("kode",6)','=',substr($lokasi_end->kode,0,6))
						->order_by('kode','DESC')
						->find();
				}
				
				$lokasis = ORM::factory('lokasi')
					->where('kode','>=',$lokasi_start->kode)
					->where('kode','<=',$max->kode)
					->find_all();
					
				foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	
			}		
			else {
				$lokasi_start = ORM::factory('lokasi',$_POST['lokasi_start']);
				
				if(substr($lokasi_start->kode,-4) == "0000") {
					if(substr($lokasi_start->kode,0,2) == "34") {
						$lokasis = ORM::factory('lokasi')
							->where('LEFT("kode",2)','=',34)
							->where('LEFT("kode",4)','!=',3411)
							->find_all();
					}
					else {
						$lokasis = ORM::factory('lokasi')
							->where('LEFT("kode",2)','=',substr($lokasi_start->kode,0,2))
							->find_all();	
					}
				}
				else {
					$lokasis = ORM::factory('lokasi')
						->where('LEFT("kode",6)','=',substr($lokasi_start->kode,0,6))
						->find_all();
				}
					
				foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}		
			}
		}
		
		if($_POST['jenis'] == "GAJI BULANAN") {
			DB::delete('kalkulasis')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gaji_bulanan($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP") {
			DB::delete('tpps')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tpp($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI 13") {
			DB::delete('kalkulasitbs')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->gaji_13($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI 14") {
			DB::delete('kalkulasiebs')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->gaji_14($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP 13") {
			DB::delete('tpptbs')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tpp_13($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI TAHUNAN") {
			DB::delete('tahunangajis')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->gaji_tahunan($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP TAHUNAN") {
			DB::delete('tahunantpps')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			/*$b = "";
			foreach($arrLokasi as $a) {
				$b .= $a.",";
			}
			
			echo $b;
			die();*/
			
			echo $this->tpp_tahunan($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI SUSULAN") {
			DB::delete('kalkulasisusulans')
				->execute();
		 	
			$this->gaji_susulan();
		}
		elseif($_POST['jenis'] == "GAJI DES") {
			DB::delete('kalkulasis')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
				
		 	$this->gaji_desember($arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP DES") {
			DB::delete('tpps')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tpp_desember($arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP RS RAPEL") {
			DB::delete('tppsrsrapels')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tpp_rs_rapel($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP KELAS JABATAN") {
			DB::delete('tppkelasjabatans')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tpp_kelas_jabatan($periode,$arrLokasi);
		}
		// elseif($_POST['jenis'] == "GAJI BULANAN PPPK") {
			// DB::delete('kalkulasip3ks') //Artinya delete from kalkulasip3ks
				// ->where('lokasi_id', 'IN', $arrLokasi)
				// ->where('periode','=',$periode)
				// ->executes();
				
			//print_r($arrLokasi);
			//die();	
		  	// $this->gajip3k_bulanan($periode,$arrLokasi);
		// }
		elseif($_POST['jenis'] == "GAJI BULANAN PPPK") {
			DB::delete('kalkulasip3ks') //Artinya delete from kalkulasip3ks
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gajip3k_bulanan($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP PPPK") {
			DB::delete('tppp3ks')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tppp3k($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI 13 PPPK") {
			DB::delete('kalkulasitbp3ks')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->gajip3k_13($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI 14 PPPK") {
			DB::delete('kalkulasiebp3ks')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->gajip3k_14($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP 13 PPPK") {
			DB::delete('tpptbp3ks')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->tppp3k_13($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "GAJI TAHUNAN PPPK") {
			DB::delete('tahunangajip3ks')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
		 	
			$this->gajip3k_tahunan($periode,$arrLokasi);
		}
		elseif($_POST['jenis'] == "TPP TAHUNAN PPPK") {
			DB::delete('tahunantppp3ks')
				->where('lokasi_id', 'IN', $arrLokasi)
				->where('periode','=',$periode)
				->execute();
			echo $this->tppp3k_tahunan($periode,$arrLokasi);
		}
	}
	
	function action_new(){							
		$this->auto_render = false;
		
		$arrKalkulasi = array(
			'GAJI BULANAN' => 'GAJI BULANAN',
			'TPP' => 'TPP',
			'GAJI 13' => 'GAJI 13',
			'GAJI 14' => 'GAJI 14',
			'TPP 13' => 'TPP 13',
			'GAJI TAHUNAN' => 'GAJI TAHUNAN',
			'TPP TAHUNAN' => 'TPP TAHUNAN',
			'GAJI SUSULAN' => 'GAJI SUSULAN',
			'RAPEL GAJI' => 'RAPEL GAJI',
			'GAJI DES' => 'GAJI DES',
			'TPP DES' => 'TPP DES',
			'TPP RS RAPEL' => 'TPP RS RAPEL',
			'TPP KELAS JABATAN' => 'TPP KELAS JABATAN',
			'GAJI BULANAN PPPK' => 'GAJI BULANAN PPPK',
			'TPP PPPK' => 'TPP PPPK',
			'GAJI 13 PPPK' => 'GAJI 13 PPPK',
			'GAJI 14 PPPK' => 'GAJI 14 PPPK',
			'TPP 13 PPPK' => 'TPP 13 PPPK',
			'GAJI TAHUNAN PPPK' => 'GAJI TAHUNAN PPPK',
			'TPP TAHUNAN PPPK' => 'TPP TAHUNAN PPPK'
		);
		
		$view = View::factory('admin/kalkulasi_form')
			->set('url',URL::base()."admin/kalkulasi")
			->set('submit_value','Kalkulasi')
			->set('jenis',$arrKalkulasi)			
			->set('bulan_list',$this->getList('bulan','name'))
			->set('lokasi_list',$this->getList('lokasi',array('kode'=>0,' - '=>'0','name'=>0),'Pilih Lokasi',array(array('bool_id','=',2)),array(array('kode','ASC'))))
			->set('notification','');		
		
		echo $view;
	}
	
	function gaji_bulanan($periode, $arrLokasi){							
		$this->auto_render = false;
				
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'					
		);
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawais = ORM::factory('pegawai')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			//->where('nip','=','198102072010011014')
			->find_all();
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
						 //$tunjangan_fungsional = 0;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();
				
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'"					
				);
				
				$value .= "(".implode(",",$arrValue)."),";
				
				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasis (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/
				
				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();
			}
		}
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasis (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function gajip3k_bulanan($periode, $arrLokasi){							
		$this->auto_render = false;
				
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji',
			'npwp'		
		);
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawaip3ks = ORM::factory('pegawaip3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			//->where('nip','=','198102072010011014')
			->find_all();
		
		foreach($pegawaip3ks as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				$npwp = $pegawai->npwp;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumump3k')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();
				
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'",
					"'".mysql_real_escape_string($npwp)."'"	
				);
				
				$value .= "(".implode(",",$arrValue)."),";
				
				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/
				
				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();
			}
		}
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES ".$value;	
		//$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES ".$arrLokasi;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
		
	function gaji_bulanan_test_nip($periode, $nip){							
		$this->auto_render = false;
				
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawais = ORM::factory('pegawai')
			->where('nip','=',$nip)
			->find_all();
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->kedudukan->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				elseif($pegawai->kedudukan_id != 72) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where_open()
						->where('kedudukan_id','=',$pegawai->kedudukan_id)
						->where('golongan','=',$pegawai->golongan_id[0])
						->where_close()						
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				
				//echo $pegawai->fungsional_id;
				//echo $tunjangan_fungsional;
				//die();
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 701 AND $pegawai->fungsional_id <= 708) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				$potongan_tpp = 0;
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				
				$bea_jabatan = round(0.05 * $gaji_bruto,6);
				$pen_tht = round(0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak),4);
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
								
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				echo "nip : ".number_format($nip)."<br>";
				echo "gajipokok : ".number_format($gaji_pokok)."<br>";
				echo "tunjangan_istri : ".number_format($tunjangan_istri)."<br>";
				echo "tunjangan_anak : ".number_format($tunjangan_anak)."<br>";
				echo "tunjangan_umum : ".number_format($tunjangan_umum)."<br>";
				echo "tunjangan_umum_tambahan : ".number_format($tunjangan_umum_tambahan)."<br>";
				echo "tunjangan_struktural : ".number_format($tunjangan_struktural)."<br>";
				echo "tunjangan_fungsional : ".number_format($tunjangan_fungsional)."<br>";
				echo "tunjangan_beras : ".number_format($tunjangan_beras)."<br>";
				echo "pph_bulan : ".number_format($pph_bulan)."<br>";
				echo "pembulatan : ".number_format($pembulatan)."<br>";
				echo "jumlah kotor : ".number_format($jumlah_kotor)."<br>";
				echo "pot bpjs : ".number_format($potongan_bpjs_kesehatan)."<br>";
				echo "pot pensiun : ".number_format($potongan_pensiun)."<br>";
				echo "iwp : ".number_format($potongan_iwp)."<br>";
				echo "pot lain : ".number_format($potongan_lain)."<br>";
				echo "pot beras : ".number_format($potongan_beras)."<br>";
				echo "pot cp : ".number_format($potongan_cp)."<br>";
				echo "jumlah potongan : ".number_format($jumlah_potongan)."<br>";
				echo "jumlah bersih : ".number_format($jumlah_bersih)."<br>";
				echo "jumlah bersih bayar : ".number_format($jumlah_bersih_bayar)."<br>";
				echo "askes : ".number_format($askes)."<br>";				
				echo "gaji bruto :".number_format($gaji_bruto)."<br>";
				echo "bea jabatan :".number_format($bea_jabatan)."<br>";
				echo "pen tht :".number_format($pen_tht)."<br>";				
			}
		}
	}	
	
	function gaji_bulanan_test_lokasi($periode, $lokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawais = ORM::factory('pegawai')
			->where('lokasi_gaji','=',$lokasi)
			->find_all();
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->kedudukan->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
			
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				elseif($pegawai->kedudukan_id != 72) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where_open()
						->where('kedudukan_id','=',$pegawai->kedudukan_id)
						->where('golongan','=',$pegawai->golongan_id[0])
						->where_close()						
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 701 AND $pegawai->fungsional_id <= 708) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				
				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				$potongan_tpp = 0;
				
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				
				$bea_jabatan = round(0.05 * $gaji_bruto,6);
				$pen_tht = round(0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak),4);
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan);
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				echo "nama : ".$nama."<br>";
				echo "nip : ".$nip."<br>";
				echo "gajipokok : ".number_format($gaji_pokok)."<br>";
				echo "tunjangan_istri : ".number_format($tunjangan_istri)."<br>";
				echo "tunjangan_anak : ".number_format($tunjangan_anak)."<br>";
				echo "tunjangan_umum : ".number_format($tunjangan_umum)."<br>";
				echo "tunjangan_umum_tambahan : ".number_format($tunjangan_umum_tambahan)."<br>";
				echo "tunjangan_struktural : ".number_format($tunjangan_struktural)."<br>";
				echo "tunjangan_fungsional : ".number_format($tunjangan_fungsional)."<br>";
				echo "tunjangan_beras : ".number_format($tunjangan_beras)."<br>";
				echo "pph_bulan : ".number_format($pph_bulan)."<br>";
				echo "pembulatan : ".number_format($pembulatan)."<br>";
				echo "jumlah kotor : ".number_format($jumlah_kotor)."<br>";
				echo "pot bpjs : ".number_format($potongan_bpjs_kesehatan)."<br>";
				echo "pot pensiun : ".number_format($potongan_pensiun)."<br>";
				echo "iwp : ".number_format($potongan_iwp)."<br>";
				echo "pot lain : ".number_format($potongan_lain)."<br>";
				echo "pot beras : ".number_format($potongan_beras)."<br>";
				echo "pot cp : ".number_format($potongan_cp)."<br>";
				echo "jumlah potongan : ".number_format($jumlah_potongan)."<br>";
				echo "jumlah bersih : ".number_format($jumlah_bersih)."<br>";
				echo "jumlah bersih bayar : ".number_format($jumlah_bersih_bayar)."<br>";
				echo "askes : ".number_format($askes)."<br>";				
				echo "gaji bruto :".number_format($gaji_bruto)."<br>";
				echo "bea jabatan :".number_format($bea_jabatan)."<br>";
				echo "pen tht :".number_format($pen_tht)."<br><br><br>";				
			}
		}
	}	
	
	function action_testnip(){	
		$nip = $this->request->param('id');
		return $this->gaji_bulanan_test_nip('2016-02-01',$nip);
	}
	
	function action_testlokasi(){	
		return $this->gaji_bulanan_test_lokasi('2015-12-01','2088');
	}	
	
	function gaji_13($periode, $arrLokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawais = ORM::factory('pegawaitb')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))//CARI pegawai CPNS,PNS,TB,Gaji Terusan, Pejabat Negara
			->find_all();
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$askes = 0;
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];	
				
				// Beras
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				//$tunjangan_beras 	= 0; //Revisi Gaji13 2021
				$tunjangan_beras = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;	
				$potongan_bpjs_kesehatan = 0 ;	
				$potongan_pensiun = 0 ;	
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if($bea_jabatan > 500000){
					$bea_jabatan = 500000;
					}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
				
				
				// Tanpa Beras				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// Dengan Beras
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				$gaji_kotor_13	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				#$gaji_bersih_13 = round($gaji_kotor - $jumlah_potongan);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
				
				// Tanpa Beras
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				// Dengan Beras
				$pembulatan_13	= 0;
				if(substr($gaji_bersih_13,-2)!="00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				$pph_bulan = $this->pph_13($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
								
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13 + $pph_bulan;
				$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				
				$pembulatan	= 0;
				if(substr($jumlah_k,-2)!="00") {
					$pembulatan = 100 - substr($jumlah_k,-2);
				}
				
				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan + $pph_bulan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;
									
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'"					
				);
				
				$value .= "(".implode(",",$arrValue)."),";
			}
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'					
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasitbs (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function gajip3k_13($periode, $arrLokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawaip3ks = ORM::factory('pegawaitbp3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			->find_all();
		
		foreach($pegawaip3ks as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				$npwp = $pegawai->npwp;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumump3k')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$askes = 0;
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];	
				
				// Beras
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				//$tunjangan_beras 	= 0; //Perubahan Gaji13 2021
				$tunjangan_beras = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;	
				$potongan_bpjs_kesehatan = 0 ;	
				$potongan_pensiun = 0 ;	
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if($bea_jabatan > 500000){
					$bea_jabatan = 500000;
					}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
				
				
				// Tanpa Beras				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// Dengan Beras
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				$gaji_kotor_13	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				#$gaji_bersih_13 = round($gaji_kotor - $jumlah_potongan);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
				
				// Tanpa Beras
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				// Dengan Beras
				$pembulatan_13	= 0;
				if(substr($gaji_bersih_13,-2)!="00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				$pph_bulan = $this->pph_13($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
								
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				
				$pembulatan	= 0;
				if(substr($jumlah_k,-2)!="00") {
					$pembulatan = 100 - substr($jumlah_k,-2);
				}
				
				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;
									
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'",
					"'".mysql_real_escape_string($npwp)."'"	
				);
				
				$value .= "(".implode(",",$arrValue)."),";
			}
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji',
			'npwp'
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasitbp3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function gaji_14($periode, $arrLokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawais = ORM::factory('pegawaieb')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			->find_all();
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			//Jika pegawai eselon 2-7 (eselon 3A - 5B) dan usianya melebihi ketentuan maka pensiun = 1 
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else { //Jika pegawai fungsional selain staff(fungs_id 1) dan usianya melebihi ketentuan maka pensiun = 1
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {//Jika pegawai berdasarkan tabel keduduka usianya melebihi ketentuan maka pensiun = 1
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {//Jika tunjangan istri ada 10% dari gapok					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak; //Untuk menampilkan di cetakannya saja
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;//Untuk menampilkan di cetakannya saja
					}
					
					$jiwa_string = "10".$jumlah_anak;//Untuk menampilkan di cetakannya saja 1011 contohnya
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$askes = 0;
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];	
				
				// Beras
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				//=============================================================
				//Ini THR 2020 tanpa beras 
				//$tunjangan_beras 	= 0;
				//=============================================================
				
				//Ini THR 2021 dengan beras dan tunjangan radiologi dihapus diganti tunjangan melekat jabatan ( diubah langsung di table pegawais_14_2021_04
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				
				$potongan_bpjs_kesehatan = 0 ;	
				$potongan_pensiun = 0 ;	
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if($bea_jabatan > 500000){
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
				
				// Biasa				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// 13
				$potongan_lain = 0;			
				$potongan_iwp = 0;
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				#edy
				#$gaji_kotor_13 = $gaji_pokok;
				#gaji ke 14 / THR th 2018 dibayarkan tj jab & tj keluarga
				$gaji_kotor_13 = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
				
				// Tanpa Beras
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp;
					die();
				}*/
				
				// Dengan Beras
				$pembulatan_13	= 0;
				if(substr($gaji_bersih_13,-2)!="00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				
				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_kotor_pembulatan."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp."=";
					echo $gaji_kotor_13;
					die();
				}*/
				
				$pph_bulan = $this->pph_14($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
				#edy				
				#$jumlah_kotor = $gaji_pokok + $pph_bulan;
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5  ;
				$jumlah_k = $jumlah_kotor;
				#$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13 + $pph_bulan;
				#$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				$pembulatan	= 0;
				#if(substr($gaji_pokok,-2)!="00") {
				#	$pembulatan = 100 - substr($gaji_pokok,-2);
				#}
				if(substr($jumlah_k,-2)!="00") {
					$pembulatan = 100 - substr($jumlah_k,-2);
				}
				
				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan + $pph_bulan;;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;
				
				#$tunjangan_anak = $tunjangan_istri = $tunjangan_beras = 0;
				#$tunjangan_umum = $tunjangan_fungsional = $tunjangan_struktural = 0;
				$potongan_iwp = $potongan_lain = $jumlah_potongan = 0;
									
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'"
				);
				
				$value .= "(".implode(",",$arrValue)."),";
			}
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasiebs (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function gajip3k_14($periode, $arrLokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$pegawaip3ks = ORM::factory('pegawaiebp3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			->find_all();
		
		foreach($pegawaip3ks as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				$npwp = $pegawai->npwp;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumump3k')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
					
					$tunjangan_fungsional = 0;	
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$askes = 0;
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];	
				
				// Beras
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= 0;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				
				$potongan_bpjs_kesehatan = 0 ;	
				$potongan_pensiun = 0 ;	
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if($bea_jabatan > 500000){
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
				
				// Biasa				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// 13
				$potongan_lain = 0;			
				$potongan_iwp = 0;
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				#edy
				#$gaji_kotor_13 = $gaji_pokok;
				#gaji ke 14 / THR th 2018 dibayarkan tj jab & tj keluarga
				$gaji_kotor_13 = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
				
				// Tanpa Beras
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp;
					die();
				}*/
				
				// Dengan Beras
				$pembulatan_13	= 0;
				if(substr($gaji_bersih_13,-2)!="00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				
				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_kotor_pembulatan."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp."=";
					echo $gaji_kotor_13;
					die();
				}*/
				
				$pph_bulan = $this->pph_14($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
				#edy				
				#$jumlah_kotor = $gaji_pokok + $pph_bulan;
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5  ;
				$jumlah_k = $jumlah_kotor;
				#$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				#$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				$pembulatan	= 0;
				#if(substr($gaji_pokok,-2)!="00") {
				#	$pembulatan = 100 - substr($gaji_pokok,-2);
				#}
				if(substr($jumlah_k,-2)!="00") {
					$pembulatan = 100 - substr($jumlah_k,-2);
				}
				
				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;
				
				#$tunjangan_anak = $tunjangan_istri = $tunjangan_beras = 0;
				#$tunjangan_umum = $tunjangan_fungsional = $tunjangan_struktural = 0;
				$potongan_iwp = $potongan_lain = $jumlah_potongan = 0;
									
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'",
					"'".mysql_real_escape_string($npwp)."'"	
				);
				
				$value .= "(".implode(",",$arrValue)."),";
			}
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji',
			'npwp'	
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasiebp3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tpp($periode, $arrLokasi) {
							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode); //explode mengubah string dengan separator khusus menjadi array
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		// Dinsos
		$arrDinsos = array(21);
		$arrKodeDinsos = array();
		$lokasi_dinsos = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrDinsos)
			->find_all();
			
		foreach($lokasi_dinsos as $dinsos) {
			array_push($arrKodeDinsos,$dinsos->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentip')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			->find_all();
		
		/*$insentips = ORM::factory('insentip')
			->where('nip','=','195912041991031002')
			->find_all();*/
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) { // Jika 0 berarti pegawai ada di insentip tapi ga ada dikalkulasi berarti pegawai tsb tdk aktif di gaji otomatis tidak dibayarkan tppnya
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))//Hentikan tpp:ya
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue; //Menskip foreach dan langsung melanjutkan ke foreach selanjutnya, klo break dia exit foreach gak lanjut foreach nya
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$bpjs_gaji = $kalkulasi->jumlah_penghasilan + $kalkulasi->tunjangan_umum + $kalkulasi->tunjangan_struktural + $kalkulasi->tunjangan_fungsional;
			$max_bpjs = 12000000 - $bpjs_gaji;
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
					
				$lhkpn 	  = $masttppk->lhkpn;
				$lhkasn   = $masttppk->lhkasn;
				$bmd      = $masttppk->bmd;
				$grat     = $masttppk->grat;
				$tptgr    = $masttppk->tptgr;
				if($periode <= "2015-11-01") {	//Jika mau cetak tpp periode 1 November 2015 kebawah, pakai tabel mastpp ( Ini peraturan yang lama )						
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						$tppkotor = ($masttpp->prosentase / 100) * $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor);
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = $eselon->finsentip - $tppkotor;
						$tunjangan_insentip = $eselon->finsentip;
					}
					else { 
						$tppkotor = $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
				}
				else { //Jika mau cetak tpp DIATAS PERIODE 1 November 2015 , pakai tabel mastpp_kinerja
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
					
					//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
 
					$tppkotor = (($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)) 
								- ($eselon->finsentip * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
					//==========================================================================================================	
						//$tppkotor = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = $eselon->finsentip - $tppkotor;
						$tunjangan_insentip = $eselon->finsentip;
						
						/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tppkotor = (0.8 * $eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = (0.8 * $eselon->finsentip) - $tppkotor;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}*/
					}
					else {
						$tppkotor = $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
				
						/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tppkotor = 0.8 * $eselon->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}*/
					}
				}					
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				$pph_kespeg = $pph_all - $tunjangan_pph;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjkonker;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = ceil(0.01 * $konker_kotor) ;
							$askes_konker = ceil(0.04 * $konker_kotor) ;
						}
						else if ($konker_kotor > $max_bpjs_konker){
							$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
							$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjdinsos;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = ceil(0.01 * $konker_kotor) ;
							$askes_konker = ceil(0.04 * $konker_kotor) ;
						}
						else if ($konker_kotor > $max_bpjs_konker){
							$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
							$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjapbj;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = ceil(0.01 * $konker_kotor) ;
							$askes_konker = ceil(0.04 * $konker_kotor) ;
						}
						else if ($konker_kotor >= $max_bpjs_konker){
							$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
							$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$jakarta_kotor = $eselon->ftjjakarta;
						$max_bpjs_jakarta = $max_bpjs - $tppkotor;
						if ($max_bpjs_jakarta <= 0) {
							$bpjs_jakarta = 0;
							$askes_jakarta = 0;
						}
						else if($jakarta_kotor <= $max_bpjs_jakarta) {
							$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
							$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
						}
						else if ($jakarta_kotor >= $max_bpjs_jakarta){
							$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
							$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
						}
						$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
						$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolongan')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
					
				$lhkpn 	  = $masttppk->lhkpn;
				$lhkasn   = $masttppk->lhkasn;
				$bmd      = $masttppk->bmd;
				$grat     = $masttppk->grat;
				$tptgr    = $masttppk->tptgr;	
				
				if($periode <= "2015-11-01") {
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						if($pegawai->sertifikasi_guru == 2) {
							$tppkotor = ($masttpp->prosentase / 100) * ($tgolongan->finsentip - $pegawai->gaji_pokok);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {	
							$tppkotor = ($masttpp->prosentase / 100) * $tgolongan->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						
						$potongan = $tgolongan->finsentip - $tppkotor;							
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						} 	
					}
					else {
						$tunjangan_insentip = $tgolongan->finsentip;
						
						if($pegawai->sertifikasi_guru == 2) {
							$tppkotor = $tgolongan->finsentip - $pegawai->gaji_pokok;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
						}
						else {	
							$tppkotor = $tgolongan->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
						}
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}	
					}
				}
				else { //Non Eselon > 1 Nov 2015 periodenya
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
		 
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)
										- ($tunjangan_insentip  * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
							//==========================================================================================================							
							//$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
						if($insentip->profesi_id == 3) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',3)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
		 
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)
										- ($tunjangan_insentip  * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
							//==========================================================================================================							
							//$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjguru')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $gurus->tunjangan;
								//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
			 
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)
											- ($tunjangan_insentip  * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
								//==========================================================================================================								
								//$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
									$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
									$askes_tpp = ceil(0.04 * $tppkotor) ;
									}
									else {
										$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
										$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $p3d->tunjangan;
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									}
									else {*/
										$tunjangan_insentip = $p3d->tunjangan;
										//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
					 
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)
													- ($tunjangan_insentip  * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
										//==========================================================================================================										
										//$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
										$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
										$askes_tpp = ceil(0.04 * $tppkotor) ;
										}
										else {
											$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
											$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									//}
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tunjangan_insentip = $penugasan->tunjangan;
									//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
				 
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)
												- ($tunjangan_insentip  * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
									//==========================================================================================================									
									//$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
											$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
											$askes_tpp = ceil(0.04 * $tppkotor) ;
										}
										else {
											$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
											$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}						
								else {	
									$tunjangan_insentip = $tgolongan->finsentip;
									//==============================Tambahan coding untuk update tppkotor Purnomo ==============================
				 
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100)
												- ($tunjangan_insentip  * ( ($lhkpn / 100) + ($lhkasn / 100) + ($bmd / 100)+ ($grat / 100) + ($tptgr / 100))); 
									//==========================================================================================================									
									//$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
											$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
											$askes_tpp = ceil(0.04 * $tppkotor) ;
										}
										else {
											$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
											$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									}*/
								}
							}
						}
						}
						
						// POTONGAN
						$potongan = $tunjangan_insentip - $tppkotor;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							} 	
						}
					}
					else {
						// TIDAK ADA DI MASTTPP BERARTI 100%
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = $gurus->tunjangan;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
								}
								else {
									$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
									$askes_tpp = ceil(0.04 * $max_bpjs) ;
								}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
						if($insentip->profesi_id == 3) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',3)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = $gurus->tunjangan;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
								}
								else {
									$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
									$askes_tpp = ceil(0.04 * $max_bpjs) ;
								}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjguru')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tppkotor = $gurus->tunjangan;
								if($tppkotor <= $max_bpjs) {
									$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
									$askes_tpp = ceil(0.04 * $tppkotor) ;
									}
								else {
									$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
									$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								$tunjangan_insentip = $gurus->tunjangan;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									$tppkotor = $p3d->tunjangan;
									if($tppkotor <= $max_bpjs) {
										$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
										$askes_tpp = ceil(0.04 * $tppkotor) ;
										}
									else {
										$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
										$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $p3d->tunjangan;
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tppkotor = 0.8 * $p3d->tunjangan;
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
										$tunjangan_insentip = 0.8 * $p3d->tunjangan;
									}*/
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tppkotor = $penugasan->tunjangan;
									if($tppkotor <= $max_bpjs) {
											$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
											$askes_tpp = ceil(0.04 * $tppkotor) ;
											}
										else {
											$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
											$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $penugasan->tunjangan;
								}						
								else {	
									$tppkotor = $tgolongan->finsentip;
									if($tppkotor <= $max_bpjs) {
											$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
											$askes_tpp = ceil(0.04 * $tppkotor) ;
											}
										else {
											$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
											$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $tgolongan->finsentip;
									
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tppkotor = 0.8 * $tgolongan->finsentip;
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
									}*/
								}
							}
						}
						}
						
						// POTONGAN
						$potongan = 0;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {						
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}						
					}
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,
				$askes_jakarta,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tpps (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tppp3k($periode, $arrLokasi) {
							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		// Dinsos
		$arrDinsos = array(21);
		$arrKodeDinsos = array();
		$lokasi_dinsos = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrDinsos)
			->find_all();
			
		foreach($lokasi_dinsos as $dinsos) {
			array_push($arrKodeDinsos,$dinsos->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentipp3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			->find_all();
		
		/*$insentips = ORM::factory('insentip')
			->where('nip','=','195912041991031002')
			->find_all();*/
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasip3k')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasip3k')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentipp3ks";
				}
				else {
					$table = "insentipp3k_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawaip3k')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$bpjs_gaji = $kalkulasi->jumlah_penghasilan + $kalkulasi->tunjangan_umum + $kalkulasi->tunjangan_struktural + $kalkulasi->tunjangan_fungsional;
			$max_bpjs = 12000000 - $bpjs_gaji;
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				
				if($periode <= "2015-11-01") {							
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						$tppkotor = ($masttpp->prosentase / 100) * $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = $eselon->finsentip - $tppkotor;
						$tunjangan_insentip = $eselon->finsentip;
					}
					else {
						$tppkotor = $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
				}
				else {
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tppkotor = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = $eselon->finsentip - $tppkotor;
						$tunjangan_insentip = $eselon->finsentip;
						
						/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tppkotor = (0.8 * $eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = (0.8 * $eselon->finsentip) - $tppkotor;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}*/
					}
					else {
						$tppkotor = $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
							$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
							$askes_tpp = ceil(0.04 * $tppkotor) ;
						}
						else {
							$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
							$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
				
						/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tppkotor = 0.8 * $eselon->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}*/
					}
				}					
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				$pph_kespeg = $pph_all - $tunjangan_pph;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjkonker;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = ceil(0.01 * $konker_kotor) ;
							$askes_konker = ceil(0.04 * $konker_kotor) ;
						}
						else if ($konker_kotor > $max_bpjs_konker){
							$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
							$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjdinsos;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = ceil(0.01 * $konker_kotor) ;
							$askes_konker = ceil(0.04 * $konker_kotor) ;
						}
						else if ($konker_kotor > $max_bpjs_konker){
							$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
							$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjapbj;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = ceil(0.01 * $konker_kotor) ;
							$askes_konker = ceil(0.04 * $konker_kotor) ;
						}
						else if ($konker_kotor >= $max_bpjs_konker){
							$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
							$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$jakarta_kotor = $eselon->ftjjakarta;
						$max_bpjs_jakarta = $max_bpjs - $tppkotor;
						if ($max_bpjs_jakarta <= 0) {
							$bpjs_jakarta = 0;
							$askes_jakarta = 0;
						}
						else if($jakarta_kotor <= $max_bpjs_jakarta) {
							$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
							$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
						}
						else if ($jakarta_kotor >= $max_bpjs_jakarta){
							$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
							$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
						}
						$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
						$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolonganp3k')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($periode <= "2015-11-01") {
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						if($pegawai->sertifikasi_guru == 2) {
							$tppkotor = ($masttpp->prosentase / 100) * ($tgolongan->finsentip - $pegawai->gaji_pokok);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {	
							$tppkotor = ($masttpp->prosentase / 100) * $tgolongan->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						
						$potongan = $tgolongan->finsentip - $tppkotor;							
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						} 	
					}
					else {
						$tunjangan_insentip = $tgolongan->finsentip;
						
						if($pegawai->sertifikasi_guru == 2) {
							$tppkotor = $tgolongan->finsentip - $pegawai->gaji_pokok;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
						}
						else {	
							$tppkotor = $tgolongan->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
						}
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}	
					}
				}
				else {
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjgurup3k')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
						if($insentip->profesi_id == 3) {
							$gurus = ORM::factory('tjgurup3k')
								->where('profesi_id','=',3)
								->where('prefix_golongan','=',substr($insentip->golongan_id))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjgurup3k')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id))
									->find();
								
								$tunjangan_insentip = $gurus->tunjangan;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $p3d->tunjangan;
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									}
									else {*/
										$tunjangan_insentip = $p3d->tunjangan;
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									//}
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tunjangan_insentip = $penugasan->tunjangan;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}						
								else {	
									$tunjangan_insentip = $tgolongan->finsentip;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									}*/
								}
							}
						}
						}
						
						// POTONGAN
						$potongan = $tunjangan_insentip - $tppkotor;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							} 	
						}
					}
					else {
						// TIDAK ADA DI MASTTPP BERARTI 100%
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjgurup3k')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = $gurus->tunjangan;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
								}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
						if($insentip->profesi_id == 3) {
							$gurus = ORM::factory('tjgurup3k')
								->where('profesi_id','=',3)
								->where('prefix_golongan','=',substr($insentip->golongan_id))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = $gurus->tunjangan;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
								}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjgurup3k')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id))
									->find();
								
								$tppkotor = $gurus->tunjangan;
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								$tunjangan_insentip = $gurus->tunjangan;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									$tppkotor = $p3d->tunjangan;
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $p3d->tunjangan;
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tppkotor = 0.8 * $p3d->tunjangan;
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
										$tunjangan_insentip = 0.8 * $p3d->tunjangan;
									}*/
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tppkotor = $penugasan->tunjangan;
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $penugasan->tunjangan;
								}						
								else {	
									$tppkotor = $tgolongan->finsentip;
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $tgolongan->finsentip;
									
									/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tppkotor = 0.8 * $tgolongan->finsentip;
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
									}*/
								}
							}
						}
						}
						
						// POTONGAN
						$potongan = 0;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// Dinsos	
						if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjdinsos;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {						
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}						
					}
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
			$tjKespeg = $tjKespeg - $pph_kespeg;
			$tjKonker = $tjKonker - $pph_konker;
			$tjJakarta = $tjJakarta - $pph_jakarta;
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,
				$askes_jakarta,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tppp3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tpp_13($periode, $arrLokasi) {							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Dinsos
		$arrDinsos = array(21);
		$arrKodeDinsos = array();
		$lokasi_dinsos = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrDinsos)
			->find_all();
			
		foreach($lokasi_dinsos as $dinsos) {
			array_push($arrKodeDinsos,$dinsos->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentiptb')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			//->where('nip','=','196806141990011001')
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_13_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$tunjangan_insentip = $eselon->finsentip;
				$potongan_tpp_bpjs = 0 ;
				$askes_tpp = 0 ;
				$potongan = 0;
				$tjKespeg = $eselon->finsentip;
				
				/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
					$tjKespeg = 0.8 * $eselon->finsentip;
					$potongan_tpp_bpjs = 0 ;
					$askes_tpp = 0 ;
					$potongan = 0;
					$tunjangan_insentip = 0.8 * $eselon->finsentip;
				}*/
									
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				//$pph_kespeg_13 = $pph_all - $tunjangan_pph;
				$pph_kespeg_13 = $pph_all;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjkonker;							
						$bpjs_konker = 0;
						$askes_konker = 0;										
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjdinsos;							
						$bpjs_konker = 0;
						$askes_konker = 0;										
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjapbj;		
						$bpjs_konker = 0;
						$askes_konker = 0;								
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$tjJakarta = $eselon->ftjjakarta;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;						
						$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolongan')
					->where('fkode','=',$insentip->golongan_id)
					->find();
					
				// TIDAK ADA DI MASTTPP BERARTI 100%
				
				$tunjangan_insentip = $tgolongan->finsentip;
				
				// CEK PROFESI GURU (profesi_id = 1)
				if($insentip->profesi_id == 1) {
					$gurus = ORM::factory('tjguru')
						->where('profesi_id','=',1)
						->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
						->where('sertifikasi','=',$insentip->sertifikasi_guru)
						->find();
					
					$tunjangan_insentip = $gurus->tunjangan;
					$tjKespeg = $gurus->tunjangan;
					$potongan_tpp_bpjs = 0 ;
					$askes_tpp = 0 ;
				}
				else {
				if($insentip->profesi_id == 3) {
					$gurus = ORM::factory('tjguru')
						->where('profesi_id','=',3)
						->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
						->where('sertifikasi','=',$insentip->sertifikasi_guru)
						->find();
					
					$tunjangan_insentip = $gurus->tunjangan;
					$tjKespeg = $gurus->tunjangan;
					$potongan_tpp_bpjs = 0 ;
					$askes_tpp = 0 ;
				}
				else {
					if($insentip->profesi_id == 2) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',2)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tjKespeg = $gurus->tunjangan;
						$potongan_tpp_bpjs = 0 ;
						$askes_tpp = 0 ;

					}
					else {
						if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
							$p3d = ORM::factory('tjp3d')
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $p3d->tunjangan;
							/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
								$tunjangan_insentip = 0.8 * $p3d->tunjangan;
							}*/
						}
						elseif($insentip->penugasan_khusus == 2) {
							$penugasan = ORM::factory('tjpenugasan')->find();
							
							$tunjangan_insentip = $penugasan->tunjangan;
						}						
						else {	
							$tunjangan_insentip = $tgolongan->finsentip;
							
							/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
								$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
							}*/
						}
					}
				}
				}
				
				// POTONGAN
				$potongan = 0;
				$potongan_tpp_bpjs = 0 ;
				$askes_tpp = 0 ;
				$tjKespeg = $tunjangan_insentip;
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				//$pph_kespeg_13 = $pph_all - $tunjangan_pph;
				$pph_kespeg_13 = $pph_all;
				
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $tgolongan->ftjkonker;
						$bpjs_konker = 0;
						$askes_konker = 0;
								
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $tgolongan->ftjdinsos;
						$bpjs_konker = 0;
						$askes_konker = 0;
								
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						if($insentip->pokja_apbj == 2) {
						$tjKonker = 2000000;
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						$bpjs_konker = 0;
						$askes_konker = 0;
						}
						else {
						$tjKonker = $tgolongan->ftjapbj;
						$bpjs_konker = 0;
						$askes_konker = 0;
						}										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$tjKonker = 2000000;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							$bpjs_konker = 0;
							$askes_konker = 0;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				

				
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;	
						$pph_jakarta = 0;
					}
					else {						
						$tjJakarta = $tgolongan->ftjjakarta;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
						}
					}
				}						
			}
			
			//echo $tjKespeg;
			//echo $pph_all;
			//die();
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				0,
				0,
				$pph_kespeg_13,
				$konker_kotor,
				$tjKonker,
				0,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				0,
				$pph_jakarta,
				$eselon_id,
				0,
				0,
				0,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
		
		//print_r($value);
		
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tpptbs (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tppp3k_13($periode, $arrLokasi) {							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Dinsos
		$arrDinsos = array(21);
		$arrKodeDinsos = array();
		$lokasi_dinsos = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrDinsos)
			->find_all();
			
		foreach($lokasi_dinsos as $dinsos) {
			array_push($arrKodeDinsos,$dinsos->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentiptbp3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			//->where('nip','=','196806141990011001')
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasip3k')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasip3k')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentipsp3k";
				}
				else {
					$table = "insentipp3k_13_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawaip3k')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$tunjangan_insentip = $eselon->finsentip;
				$potongan_tpp_bpjs = 0 ;
				$askes_tpp = 0 ;
				$potongan = 0;
				$tjKespeg = $eselon->finsentip;
				
				/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
					$tjKespeg = 0.8 * $eselon->finsentip;
					$potongan_tpp_bpjs = 0 ;
					$askes_tpp = 0 ;
					$potongan = 0;
					$tunjangan_insentip = 0.8 * $eselon->finsentip;
				}*/
									
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				//$pph_kespeg_13 = $pph_all - $tunjangan_pph;
				$pph_kespeg_13 = $pph_all;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjkonker;							
						$bpjs_konker = 0;
						$askes_konker = 0;										
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjdinsos;							
						$bpjs_konker = 0;
						$askes_konker = 0;										
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjapbj;		
						$bpjs_konker = 0;
						$askes_konker = 0;								
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$tjJakarta = $eselon->ftjjakarta;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;						
						$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolonganp3k')
					->where('fkode','=',$insentip->golongan_id)
					->find();
					
				// TIDAK ADA DI MASTTPP BERARTI 100%
				
				$tunjangan_insentip = $tgolongan->finsentip;
				
				// CEK PROFESI GURU (profesi_id = 1)
				if($insentip->profesi_id == 1) {
					$gurus = ORM::factory('tjgurup3k')
						->where('profesi_id','=',1)
						->where('prefix_golongan','=',substr($insentip->golongan_id))
						->where('sertifikasi','=',$insentip->sertifikasi_guru)
						->find();
					
					$tunjangan_insentip = $gurus->tunjangan;
					$tjKespeg = $gurus->tunjangan;
					$potongan_tpp_bpjs = 0 ;
					$askes_tpp = 0 ;
				}
				else {
				if($insentip->profesi_id == 3) {
					$gurus = ORM::factory('tjgurup3k')
						->where('profesi_id','=',3)
						->where('prefix_golongan','=',substr($insentip->golongan_id))
						->where('sertifikasi','=',$insentip->sertifikasi_guru)
						->find();
					
					$tunjangan_insentip = $gurus->tunjangan;
					$tjKespeg = $gurus->tunjangan;
					$potongan_tpp_bpjs = 0 ;
					$askes_tpp = 0 ;
				}
				else {
					if($insentip->profesi_id == 2) {
						$gurus = ORM::factory('tjgurup3k')
							->where('profesi_id','=',2)
							->where('prefix_golongan','=',substr($insentip->golongan_id))
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tjKespeg = $gurus->tunjangan;
						$potongan_tpp_bpjs = 0 ;
						$askes_tpp = 0 ;

					}
					else {
						if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
							$p3d = ORM::factory('tjp3d')
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $p3d->tunjangan;
							/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
								$tunjangan_insentip = 0.8 * $p3d->tunjangan;
							}*/
						}
						elseif($insentip->penugasan_khusus == 2) {
							$penugasan = ORM::factory('tjpenugasan')->find();
							
							$tunjangan_insentip = $penugasan->tunjangan;
						}						
						else {	
							$tunjangan_insentip = $tgolongan->finsentip;
							
							/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
								$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
							}*/
						}
					}
				}
				}
				
				// POTONGAN
				$potongan = 0;
				$potongan_tpp_bpjs = 0 ;
				$askes_tpp = 0 ;
				$tjKespeg = $tunjangan_insentip;
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				//$pph_kespeg_13 = $pph_all - $tunjangan_pph;
				$pph_kespeg_13 = $pph_all;
				
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $tgolongan->ftjkonker;
						$bpjs_konker = 0;
						$askes_konker = 0;
								
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $tgolongan->ftjdinsos;
						$bpjs_konker = 0;
						$askes_konker = 0;
								
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						if($insentip->pokja_apbj == 2) {
						$tjKonker = 2000000;
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						$bpjs_konker = 0;
						$askes_konker = 0;
						}
						else {
						$tjKonker = $tgolongan->ftjapbj;
						$bpjs_konker = 0;
						$askes_konker = 0;
						}										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$tjKonker = 2000000;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							$bpjs_konker = 0;
							$askes_konker = 0;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				

				
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;	
						$pph_jakarta = 0;
					}
					else {						
						$tjJakarta = $tgolongan->ftjjakarta;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
						}
					}
				}						
			}
			
			//echo $tjKespeg;
			//echo $pph_all;
			//die();
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
			$tjKespeg = $tjKespeg - $pph_kespeg_13;
			$tjKonker = $tjKonker - pph_kespeg;
			$tjJakarta = $tjJakarta - pph_jakarta;
			
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				0,
				0,
				$pph_kespeg_13,
				$konker_kotor,
				$tjKonker,
				0,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				0,
				$pph_jakarta,
				$eselon_id,
				0,
				0,
				0,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
		
		//print_r($value);
		
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tpptbp3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tpp_tahunan($periode, $arrLokasi) {							
		$this->auto_render = false;
		
		$tpps = ORM::factory('tpp')
			->where('lokasi_id','IN',$arrLokasi)
			->where('YEAR("periode")','=',$periode)
			->find_all();	
		
		$arrNip = array();	
		foreach($tpps as $k) {
			array_push($arrNip, $k->nip);
		}
		$arrNip = array_unique($arrNip);
	
		//$arrNip = array('196806141990011001');
		
		$value = "";		
				
		foreach($arrNip as $nip) {
			$n = ORM::factory('tpp')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				continue;
			}
			
			$last_tpp = ORM::factory('tpp')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->order_by('periode','DESC')
				->find();
			
			$lokasi_id = $last_tpp->lokasi_id;			
			$lokasi_kode = $last_tpp->lokasi_kode;
			$lokasi_string = $last_tpp->lokasi_string;
			$name = $last_tpp->name;	
			$jabatan = $last_tpp->jabatan;
			$eselon_id = $last_tpp->eselon_id;
			$golongan_id = $last_tpp->golongan_id;
			$golongan_string = $last_tpp->golongan_string;
			$kelompok_gaji = $last_tpp->kelompok_gaji;
									
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$pph_kespeg = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
			$potongan_tpp_bpjs = 0;
			$askes_tpp = 0;
			
			// TPP SETAHUN
			$sql =
			"SELECT 
			SUM(insentip) as tunjangan_insentip,
			SUM(tpp) as tjKespeg,
			SUM(potongan) as potongan,
			SUM(potongan_tpp_bpjs) as potongan_tpp_bpjs,
			SUM(pph) as pph_kespeg,
			SUM(konker_bruto) as konker_kotor,
			SUM(konker) as tjKonker,
			SUM(bpjs_konker) as bpjs_konker,
			SUM(pph_konker) as pph_konker,
			SUM(jakarta_bruto) as jakarta_kotor,
			SUM(jakarta) as tjJakarta,
			SUM(bpjs_jakarta) as bpjs_jakarta,
			SUM(pph_jakarta) as pph_jakarta,
			SUM(askes_tpp) as askes_tpp,
			SUM(askes_konker) as askes_konker,
			SUM(askes_jakarta) as askes_jakarta
			FROM tpps
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip = $tunjangan_insentip + $q->tunjangan_insentip;
				$tjKespeg = $tjKespeg + $q->tjKespeg;
				$potongan = $potongan + $q->potongan;
				$potongan_tpp_bpjs = $potongan_tpp_bpjs + $q->potongan_tpp_bpjs;
				$pph_kespeg = $pph_kespeg + $q->pph_kespeg;
				$konker_kotor = $konker_kotor + $q->konker_kotor;
				$tjKonker = $tjKonker + $q->tjKonker;
				$bpjs_konker = $bpjs_konker + $q->bpjs_konker;
				$pph_konker = $pph_konker + $q->pph_konker;
				$jakarta_kotor = $jakarta_kotor + $q->jakarta_kotor;
				$tjJakarta = $tjJakarta + $q->tjJakarta;
				$bpjs_jakarta = $bpjs_jakarta + $q->bpjs_jakarta;
				$pph_jakarta = $pph_jakarta + $q->pph_jakarta;
				$askes_tpp = $askes_tpp + $q->askes_tpp;
				$askes_konker = $askes_konker + $q->askes_konker;
				$askes_jakarta = $askes_jakarta + $q->askes_jakarta;
			}
			
			// TPP 13
			$sql =
			"SELECT 
			SUM(insentip) as tunjangan_insentip,
			SUM(tpp) as tjKespeg,
			SUM(potongan) as potongan,
			SUM(potongan_tpp_bpjs) as potongan_tpp_bpjs,
			SUM(pph) as pph_kespeg,
			SUM(konker_bruto) as konker_kotor,
			SUM(konker) as tjKonker,
			SUM(bpjs_konker) as bpjs_konker,
			SUM(pph_konker) as pph_konker,
			SUM(jakarta_bruto) as jakarta_kotor,
			SUM(jakarta) as tjJakarta,
			SUM(bpjs_jakarta) as bpjs_jakarta,
			SUM(pph_jakarta) as pph_jakarta,
			SUM(askes_tpp) as askes_tpp,
			SUM(askes_konker) as askes_konker,
			SUM(askes_jakarta) as askes_jakarta
			FROM tpptbs
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip = $tunjangan_insentip + $q->tunjangan_insentip;
				$tjKespeg = $tjKespeg + $q->tjKespeg;
				$potongan = $potongan + $q->potongan;
				$potongan_tpp_bpjs = $potongan_tpp_bpjs + $q->potongan_tpp_bpjs;
				$pph_kespeg = $pph_kespeg + $q->pph_kespeg;
				$konker_kotor = $konker_kotor + $q->konker_kotor;
				$tjKonker = $tjKonker + $q->tjKonker;
				$bpjs_konker = $bpjs_konker + $q->bpjs_konker;
				$pph_konker = $pph_konker + $q->pph_konker;
				$jakarta_kotor = $jakarta_kotor + $q->jakarta_kotor;
				$tjJakarta = $tjJakarta + $q->tjJakarta;
				$bpjs_jakarta = $bpjs_jakarta + $q->bpjs_jakarta;
				$pph_jakarta = $pph_jakarta + $q->pph_jakarta;
				$askes_tpp = $askes_tpp + $q->askes_tpp;
				$askes_konker = $askes_konker + $q->askes_konker;
				$askes_jakarta = $askes_jakarta + $q->askes_jakarta;
			}
			
			// RAPEL TPP
			$sql =
			"SELECT 
			SUM(insentip3) as tunjangan_insentip,
			SUM(tpp3) as tjKespeg,
			SUM(potongan3) as potongan,
			SUM(potongan_tpp_bpjs3) as potongan_tpp_bpjs,
			SUM(pph3) as pph_kespeg,
			SUM(konker_bruto3) as konker_kotor,
			SUM(konker3) as tjKonker,
			SUM(bpjs_konker3) as bpjs_konker,
			SUM(pph_konker3) as pph_konker,
			SUM(jakarta_bruto3) as jakarta_kotor,
			SUM(jakarta3) as tjJakarta,
			SUM(bpjs_jakarta3) as bpjs_jakarta,
			SUM(pph_jakarta3) as pph_jakarta,
			SUM(askes_tpp3) as askes_tpp,
			SUM(askes_konker3) as askes_konker,
			SUM(askes_jakarta3) as askes_jakarta
			FROM tppsrsrapels
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip = $tunjangan_insentip + $q->tunjangan_insentip;
				$tjKespeg = $tjKespeg + $q->tjKespeg;
				$potongan = $potongan + $q->potongan;
				$potongan_tpp_bpjs = $potongan_tpp_bpjs + $q->potongan_tpp_bpjs;
				$pph_kespeg = $pph_kespeg + $q->pph_kespeg;
				$konker_kotor = $konker_kotor + $q->konker_kotor;
				$tjKonker = $tjKonker + $q->tjKonker;
				$bpjs_konker = $bpjs_konker + $q->bpjs_konker;
				$pph_konker = $pph_konker + $q->pph_konker;
				$jakarta_kotor = $jakarta_kotor + $q->jakarta_kotor;
				$tjJakarta = $tjJakarta + $q->tjJakarta;
				$bpjs_jakarta = $bpjs_jakarta + $q->bpjs_jakarta;
				$pph_jakarta = $pph_jakarta + $q->pph_jakarta;
				$askes_tpp = $askes_tpp + $q->askes_tpp;
				$askes_konker = $askes_konker + $q->askes_konker;
				$askes_jakarta = $askes_jakarta + $q->askes_jakarta;
			}
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,	
				$askes_jakarta,
				"'".mysql_real_escape_string($kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tahunantpps (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
		
		//return $sql;
	}

	function tppp3k_tahunan($periode, $arrLokasi) {							
		$this->auto_render = false;
		
		$tpps = ORM::factory('tppp3k')
			->where('lokasi_id','IN',$arrLokasi)
			->where('YEAR("periode")','=',$periode)
			->find_all();	
		
		$arrNip = array();	
		foreach($tpps as $k) {
			array_push($arrNip, $k->nip);
		}
		$arrNip = array_unique($arrNip);
	
		//$arrNip = array('196806141990011001');
		
		$value = "";		
				
		foreach($arrNip as $nip) {
			$n = ORM::factory('tpp')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				continue;
			}
			
			$last_tpp = ORM::factory('tppp3k')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->order_by('periode','DESC')
				->find();
			
			$lokasi_id = $last_tpp->lokasi_id;			
			$lokasi_kode = $last_tpp->lokasi_kode;
			$lokasi_string = $last_tpp->lokasi_string;
			$name = $last_tpp->name;	
			$jabatan = $last_tpp->jabatan;
			$eselon_id = $last_tpp->eselon_id;
			$golongan_id = $last_tpp->golongan_id;
			$golongan_string = $last_tpp->golongan_string;
			$kelompok_gaji = $last_tpp->kelompok_gaji;
									
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$pph_kespeg = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
			$potongan_tpp_bpjs = 0;
			$askes_tpp = 0;
			
			// TPP SETAHUN
			$sql =
			"SELECT 
			SUM(insentip) as tunjangan_insentip,
			SUM(tpp) as tjKespeg,
			SUM(potongan) as potongan,
			SUM(potongan_tpp_bpjs) as potongan_tpp_bpjs,
			SUM(pph) as pph_kespeg,
			SUM(konker_bruto) as konker_kotor,
			SUM(konker) as tjKonker,
			SUM(bpjs_konker) as bpjs_konker,
			SUM(pph_konker) as pph_konker,
			SUM(jakarta_bruto) as jakarta_kotor,
			SUM(jakarta) as tjJakarta,
			SUM(bpjs_jakarta) as bpjs_jakarta,
			SUM(pph_jakarta) as pph_jakarta,
			SUM(askes_tpp) as askes_tpp,
			SUM(askes_konker) as askes_konker,
			SUM(askes_jakarta) as askes_jakarta
			FROM tppp3ks
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip = $tunjangan_insentip + $q->tunjangan_insentip;
				$tjKespeg = $tjKespeg + $q->tjKespeg;
				$potongan = $potongan + $q->potongan;
				$potongan_tpp_bpjs = $potongan_tpp_bpjs + $q->potongan_tpp_bpjs;
				$pph_kespeg = $pph_kespeg + $q->pph_kespeg;
				$konker_kotor = $konker_kotor + $q->konker_kotor;
				$tjKonker = $tjKonker + $q->tjKonker;
				$bpjs_konker = $bpjs_konker + $q->bpjs_konker;
				$pph_konker = $pph_konker + $q->pph_konker;
				$jakarta_kotor = $jakarta_kotor + $q->jakarta_kotor;
				$tjJakarta = $tjJakarta + $q->tjJakarta;
				$bpjs_jakarta = $bpjs_jakarta + $q->bpjs_jakarta;
				$pph_jakarta = $pph_jakarta + $q->pph_jakarta;
				$askes_tpp = $askes_tpp + $q->askes_tpp;
				$askes_konker = $askes_konker + $q->askes_konker;
				$askes_jakarta = $askes_jakarta + $q->askes_jakarta;
			}
			
			// TPP 13
			$sql =
			"SELECT 
			SUM(insentip) as tunjangan_insentip,
			SUM(tpp) as tjKespeg,
			SUM(potongan) as potongan,
			SUM(potongan_tpp_bpjs) as potongan_tpp_bpjs,
			SUM(pph) as pph_kespeg,
			SUM(konker_bruto) as konker_kotor,
			SUM(konker) as tjKonker,
			SUM(bpjs_konker) as bpjs_konker,
			SUM(pph_konker) as pph_konker,
			SUM(jakarta_bruto) as jakarta_kotor,
			SUM(jakarta) as tjJakarta,
			SUM(bpjs_jakarta) as bpjs_jakarta,
			SUM(pph_jakarta) as pph_jakarta,
			SUM(askes_tpp) as askes_tpp,
			SUM(askes_konker) as askes_konker,
			SUM(askes_jakarta) as askes_jakarta
			FROM tpptbp3ks
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip = $tunjangan_insentip + $q->tunjangan_insentip;
				$tjKespeg = $tjKespeg + $q->tjKespeg;
				$potongan = $potongan + $q->potongan;
				$potongan_tpp_bpjs = $potongan_tpp_bpjs + $q->potongan_tpp_bpjs;
				$pph_kespeg = $pph_kespeg + $q->pph_kespeg;
				$konker_kotor = $konker_kotor + $q->konker_kotor;
				$tjKonker = $tjKonker + $q->tjKonker;
				$bpjs_konker = $bpjs_konker + $q->bpjs_konker;
				$pph_konker = $pph_konker + $q->pph_konker;
				$jakarta_kotor = $jakarta_kotor + $q->jakarta_kotor;
				$tjJakarta = $tjJakarta + $q->tjJakarta;
				$bpjs_jakarta = $bpjs_jakarta + $q->bpjs_jakarta;
				$pph_jakarta = $pph_jakarta + $q->pph_jakarta;
				$askes_tpp = $askes_tpp + $q->askes_tpp;
				$askes_konker = $askes_konker + $q->askes_konker;
				$askes_jakarta = $askes_jakarta + $q->askes_jakarta;
			}
			
			// RAPEL TPP
			$sql =
			"SELECT 
			SUM(insentip3) as tunjangan_insentip,
			SUM(tpp3) as tjKespeg,
			SUM(potongan3) as potongan,
			SUM(potongan_tpp_bpjs3) as potongan_tpp_bpjs,
			SUM(pph3) as pph_kespeg,
			SUM(konker_bruto3) as konker_kotor,
			SUM(konker3) as tjKonker,
			SUM(bpjs_konker3) as bpjs_konker,
			SUM(pph_konker3) as pph_konker,
			SUM(jakarta_bruto3) as jakarta_kotor,
			SUM(jakarta3) as tjJakarta,
			SUM(bpjs_jakarta3) as bpjs_jakarta,
			SUM(pph_jakarta3) as pph_jakarta,
			SUM(askes_tpp3) as askes_tpp,
			SUM(askes_konker3) as askes_konker,
			SUM(askes_jakarta3) as askes_jakarta
			FROM tppsrsrapels
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip = $tunjangan_insentip + $q->tunjangan_insentip;
				$tjKespeg = $tjKespeg + $q->tjKespeg;
				$potongan = $potongan + $q->potongan;
				$potongan_tpp_bpjs = $potongan_tpp_bpjs + $q->potongan_tpp_bpjs;
				$pph_kespeg = $pph_kespeg + $q->pph_kespeg;
				$konker_kotor = $konker_kotor + $q->konker_kotor;
				$tjKonker = $tjKonker + $q->tjKonker;
				$bpjs_konker = $bpjs_konker + $q->bpjs_konker;
				$pph_konker = $pph_konker + $q->pph_konker;
				$jakarta_kotor = $jakarta_kotor + $q->jakarta_kotor;
				$tjJakarta = $tjJakarta + $q->tjJakarta;
				$bpjs_jakarta = $bpjs_jakarta + $q->bpjs_jakarta;
				$pph_jakarta = $pph_jakarta + $q->pph_jakarta;
				$askes_tpp = $askes_tpp + $q->askes_tpp;
				$askes_konker = $askes_konker + $q->askes_konker;
				$askes_jakarta = $askes_jakarta + $q->askes_jakarta;
			}
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,	
				$askes_jakarta,
				"'".mysql_real_escape_string($kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tahunantppp3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
		
		//return $sql;
	}

	/*function tpp_13($periode, $arrLokasi) {							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentiptb')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			->find_all();
		
		/*$insentips = ORM::factory('insentip')
			->where('nip','=','195912041991031002')
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_13_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				
				if($periode <= "2015-11-01") {							
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						$tjKespeg = ($masttpp->prosentase / 100) * $eselon->finsentip;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
					else {
						$tjKespeg = $eselon->finsentip;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
				}
				else {
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tjKespeg = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
						
						if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tjKespeg = (0.8 * $eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}
					}
					else {
						$tjKespeg = $eselon->finsentip;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
						
						if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tjKespeg = 0.8 * $eselon->finsentip;
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}
					}
				}					
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				$pph_kespeg_13 = $pph_all - $tunjangan_pph;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjkonker;										
						$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjJakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$tjJakarta = $eselon->ftjjakarta;
						$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolongan')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($periode <= "2015-11-01") {
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						if($pegawai->sertifikasi_guru == 2) {
							$tjKespeg = ($masttpp->prosentase / 100) * ($tgolongan->finsentip - $pegawai->gaji_pokok);
						}
						else {	
							$tjKespeg = ($masttpp->prosentase / 100) * $tgolongan->finsentip;
						}
						
						$potongan = 0;							
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg_13 = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								$tjKonker = $tgolongan->ftjkonker;										
								$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjJakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$tjJakarta = $tgolongan->ftjjakarta;
								$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						} 	
					}
					else {
						$tunjangan_insentip = $tgolongan->finsentip;
						
						if($pegawai->sertifikasi_guru == 2) {
							$tjKespeg = $tgolongan->finsentip - $pegawai->gaji_pokok;
							$potongan = 0;
						}
						else {	
							$tjKespeg = $tgolongan->finsentip;
							$potongan = 0;
						}
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg_13 = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								$tjKonker = $tgolongan->ftjkonker;										
								$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjJakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$tjJakarta = $tgolongan->ftjjakarta;
								$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}	
					}
				}
				else {
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjguru')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $gurus->tunjangan;
								$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									$tunjangan_insentip = $p3d->tunjangan;
									$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									
									if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $p3d->tunjangan;
										$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									}
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tunjangan_insentip = $penugasan->tunjangan;
									$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								}						
								else {	
									$tunjangan_insentip = $tgolongan->finsentip;
									$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									
									if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
										$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									}
								}
							}
						}
						
						// POTONGAN
						$potongan = 0;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg_13 = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								$tjKonker = $tgolongan->ftjkonker;										
								$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjJakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$tjJakarta = $tgolongan->ftjjakarta;
								$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							} 	
						}
					}
					else {
						// TIDAK ADA DI MASTTPP BERARTI 100%
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tjKespeg = $gurus->tunjangan;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjguru')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $gurus->tunjangan;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									$tunjangan_insentip = $p3d->tunjangan;
									if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $p3d->tunjangan;
									}
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tunjangan_insentip = $penugasan->tunjangan;
								}						
								else {	
									$tunjangan_insentip = $tgolongan->finsentip;
									
									if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
										$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
									}
								}
							}
						}
						
						// POTONGAN
						$potongan = 0;
						$tjKespeg = $tunjangan_insentip;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg_13 = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								$tjKonker = $tgolongan->ftjkonker;										
								$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjJakarta = 0;
								$pph_jakarta = 0;
							}
							else {						
								$tjJakarta = $tgolongan->ftjjakarta;
								$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}						
					}
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				0,
				$pph_kespeg_13,
				$tjKonker,
				$pph_konker,
				$tjJakarta,
				$pph_jakarta,
				$eselon_id,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
		
		//print_r($value);
		
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'pph',
			'konker',
			'pph_konker',
			'jakarta',
			'pph_jakarta',
			'eselon_id',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tpptbs (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}*/

	
	function tpp_13_backup($periode, $arrLokasi) {							
		$this->auto_render = false;
				
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentiptb')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
						
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
							
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_13_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
																
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppktb = ORM::factory('masttppktb')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				
				if($periode <= "2015-11-01") {							
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						$tjKespeg = ($masttpp->prosentase / 100) * $eselon->finsentip;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
					else {
						$tjKespeg = $eselon->finsentip;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
				}
				else {
					if($masttppktb->reset(FALSE)->count_all()) {
						$masttppktb = $masttppktb->find();	
						
						$tjKespeg = ($eselon->finsentip * ((($masttppktb->skp / 100)*(60/100))+((40/100)-($masttppktb->perilaku/100))))*($masttppktb->hukdis / 100);
						$potongan = $eselon->finsentip - $tjKespeg;
						$tunjangan_insentip = $eselon->finsentip;
						
						if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tjKespeg = (0.8 * $eselon->finsentip * ((($masttppktb->skp / 100)*(60/100))+((40/100)-($masttppktb->perilaku/100))))*($masttppktb->hukdis / 100);
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}
					}
					else {
						$tjKespeg = $eselon->finsentip;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
						
						if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tjKespeg = 0.8 * $eselon->finsentip;
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $eselon->finsentip;
						}
					}
				}					
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$pph_kespeg = $pph_all;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					$tjKonker = $eselon->ftjkonker;										
					$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
					
					if($pphBlnAll_KK > 0) {
						$pph_konker	= $pphBlnAll_KK - $pph_all;	
					}
				} 
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjapbj;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					$tjJakarta = $eselon->ftjjakarta;
					$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
					
					if($pphBlnAll_TB > 0) {
						$pph_jakarta = $pphBlnAll_TB - $pph_all;	
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolongan')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppktb = ORM::factory('masttppktb')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($periode <= "2015-11-01") {
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						if($pegawai->sertifikasi_guru == 2) {
							$tjKespeg = ($masttpp->prosentase / 100) * ($tgolongan->finsentip - $pegawai->gaji_pokok);
						}
						else {	
							$tjKespeg = ($masttpp->prosentase / 100) * $tgolongan->finsentip;
						}
						
						$potongan = 0;							
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$pph_kespeg = $pph_all;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							$tjKonker = $tgolongan->ftjkonker;										
							$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$tjKonker = 2000000;
								}
								else {
								$tjKonker = $tgolongan->ftjapbj;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						

						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							$tjJakarta = $tgolongan->ftjjakarta;
							$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						} 	
					}
					else {
						$tunjangan_insentip = $tgolongan->finsentip;
						
						if($insentip->sertifikasi_guru == 2) {
							$tjKespeg = $tgolongan->finsentip - $pegawai->gaji_pokok;
							$potongan = 0;
						}
						else {	
							$tjKespeg = $tgolongan->finsentip;
							$potongan = 0;
						}
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$pph_kespeg = $pph_all;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							$tjKonker = $tgolongan->ftjkonker;										
							$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$tjKonker = 2000000;
								}
								else {
								$tjKonker = $tgolongan->ftjapbj;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							$tjJakarta = $tgolongan->ftjjakarta;
							$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						}	
					}
				}
				else {
					if($masttppktb->reset(FALSE)->count_all()) {
						$masttppktb = $masttppktb->find();	
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						if($insentip->sertifikasi_guru == 2) {
							//$tunjangan_insentip = $tgolongan->finsentip - $gaji_pokok;
							//$tjKespeg = ($tunjangan_insentip * ((($masttppktb->skp / 100)*(60/100))+((40/100)-($masttppktb->perilaku/100))))*($masttppktb->hukdis / 100);	
							
							$tunjangan_insentip = $tgolongan->finsentip;
							$tjKespeg = $tgolongan->finsentip;
						}
						else {	
							$tunjangan_insentip = $tgolongan->finsentip;
							$tjKespeg = ($tunjangan_insentip * ((($masttppktb->skp / 100)*(60/100))+((40/100)-($masttppktb->perilaku/100))))*($masttppktb->hukdis / 100);
							
							if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
								$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
								$tjKespeg = ($tunjangan_insentip * ((($masttppktb->skp / 100)*(60/100))+((40/100)-($masttppktb->perilaku/100))))*($masttppktb->hukdis / 100);
							}
						}
						
						// GURU DAN TENAGA KEPENDIDIKAN
						if(in_array($insentip->lokasi_gaji, range(3491,3527))) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',$insentip->profesi_id)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tjKespeg = $gurus->tunjangan;
							//$tjKespeg = ($tunjangan_insentip * ((($masttppktb->skp / 100)*(60/100))+((40/100)-($masttppktb->perilaku/100))))*($masttppktb->hukdis / 100);
						}
						
						$potongan = 0;
						
						if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$potongan = 0;
							$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
						}
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$pph_kespeg = $pph_all;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							$tjKonker = $tgolongan->ftjkonker;										
							$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$tjKonker = 2000000;
								}
								else {
								$tjKonker = $tgolongan->ftjapbj;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							$tjJakarta = $tgolongan->ftjjakarta;
							$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						} 	
					}
					else {
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// Jika tidak ada di tabel masttppktb
						if($pegawai->sertifikasi_guru == 2) {
							$tjKespeg = $tgolongan->finsentip;
							$potongan = 0;
						}
						else {	
							$tjKespeg = $tgolongan->finsentip;
							$potongan = 0;
							
							if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
								$tjKespeg = 0.8* $tgolongan->finsentip;
								$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
							}
						}
						
						// GURU DAN TENAGA KEPENDIDIKAN
						if(in_array($insentip->lokasi_gaji, range(3491,3527))) {							
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',$insentip->profesi_id)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tjKespeg = $gurus->tunjangan;
							$potongan = 0;
						}
						
						if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
							$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
						}
												
						$pph_all = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$pph_kespeg = $pph_all;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							$tjKonker = $tgolongan->ftjkonker;										
							$pphBlnAll_KK = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$tjKonker = 2000000;
								}
								else {
								$tjKonker = $tgolongan->ftjapbj;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						

						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							$tjJakarta = $tgolongan->ftjjakarta;
							$pphBlnAll_TB = $this->pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						}						
					}
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
									
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$pph_kespeg,
				$tjKonker,
				$pph_konker,
				$tjJakarta,
				$pph_jakarta,
				$eselon_id,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
		
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'pph',
			'konker',
			'pph_konker',
			'jakarta',
			'pph_jakarta',
			'eselon_id',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tpptbs (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();		
	}
	
	function pph($istri, $anak, $pen_tht, $gaji_kotor_pembulatan,$nip){
		if($gaji_kotor_pembulatan < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
		} 
		else {
			$tambahan = 0;
		}
		
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
		
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12 ;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	////pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		$pph_bulan	 = floor($pph_tahun/12) ;
		
		return $pph_bulan ;
	}
	
	function pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$kespeg,$konker_jakarta,$nip){
		$harga_beras = ORM::factory('beras')
			->where('bool_id','=',2)
			->find();
		
		$jumlah_jiwa	= 1 + $istri + $anak ;
		$tjistri		= 0.1 * $gaji_pokok * $istri;
		$tjanak			= 0.02 * $gaji_pokok * $anak;
		$tjberas		= 10 * $harga_beras->harga * $jumlah_jiwa;
			
		$gator = $gaji_pokok + $tjistri + $tjanak + $tunjangan_struktural + $tunjangan_fungsional + $tjberas + $tunjangan_umum + $pembulatan + $kespeg;
		
		if($konker_jakarta > 0) {
			$gator = $gator + $konker_jakarta;
		}
		
		if ($gator < 1000000) {
			$tmhumum = 1000000 - $gator;
		} 
		else {
			$tmhumum = 0;
		}
		$gator 		= $gator + $tmhumum;
		$beajab		= 0.05 * $gator;
		if ($beajab > 500000) {
			$beajab = 500000 ;
		}
		$pentht		= 0.0475 * ($gaji_pokok + $tjistri + $tjanak) ;
		#edy
				if($pentht > 200000){
					$pentht = 200000;
				}
		$kurgaji	= $beajab + $pentht ;
		$pengnet1	= $gator - $kurgaji ;
		$pengnet12	= $pengnet1 * 12 ;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);		//pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}
		
		$pengkpj	= (int) round($pengnet12 - $ptkp);
		
		if($pengkpj < 0) {
			$pengkpj = 0;
		}
	
		$pKPJ = substr_replace($pengkpj,"000",-3); 
	
		$pphThn = 0;
		
		if($pKPJ <= 50000000) {
			$pphThn	= (0.05*$pKPJ);
		}
		elseif($pKPJ>50000000 && $pKPJ<=250000000) {
			$pphThn	=(0.05*50000000) + (0.15*($pKPJ-50000000));
		}
		elseif($pKPJ>250000000 && $pKPJ<=500000000) {
			$pphThn	=(0.05*50000000) + (0.15*200000000)+(0.25*($pKPJ-250000000));
		}
		elseif($pKPJ>500000000) {
			$pphThn	=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($pKPJ-500000000));
		}
		return floor($pphThn/12);
	}
	
	function pph_kespeg_13($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$kespeg,$konker_jakarta,$nip){		
		$harga_beras = ORM::factory('beras')
			->where('bool_id','=',2)
			->find();
		
		$jumlah_jiwa	= 1 + $istri + $anak ;
		$tjistri		= 0.1 * $gaji_pokok * $istri;
		$tjanak			= 0.02 * $gaji_pokok * $anak;
		$tjberas		= 10 * $harga_beras->harga * $jumlah_jiwa;
			
		$gator = $gaji_pokok + $tjistri + $tjanak + $tjberas + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_umum + $pembulatan + $kespeg;
		
		if($konker_jakarta > 0) {
			$gator = $gator + $konker_jakarta;
		}
		
		// Bruto 12 & Bruto 12 + 1 bulan
		$gator_12 = $gator * 12;
		$gator_12_kespeg = $gator_12 + $kespeg;

		$beajab = 0.05 * $gator;
		if ($beajab > 500000) {
			$beajab = 500000;
		}
		
		$pentht		= 0.0475 * ($gaji_pokok + $tjistri + $tjanak) ;
		#edy
		if($pentht > 200000){
			$pentht = 200000;
		}
		$kurgaji	= $beajab + $pentht ;
		$pentht_12 	= 12 * $pentht;
		
		$beajab_kespeg = 0.05 * $gator_12_kespeg;
		if ($beajab_kespeg > 6000000) {
			$beajab_kespeg = 6000000;
		}
		
		$kurgaji_kespeg = $beajab_kespeg + $pentht_12;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);		
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}
		
		$net_bln = $gator - $kurgaji;
		$net_kespeg = $gator_12_kespeg - $kurgaji_kespeg;
		$net_12 = 12 * $net_bln;
		$pkpj = (int) round($net_12 - $ptkp);
		$pkpj_kespeg = (int) round($net_kespeg - $ptkp);
		
		// PKPJ
		if($pkpj < 0) {
			$pkpj = 0;
		}
		$pKPJ = substr_replace($pkpj,"000",-3); 
		$pphThn = 0;
		
		if($pKPJ <= 50000000) {
			$pphThn	= (0.05*$pKPJ);
		}
		elseif($pKPJ>50000000 && $pKPJ<=250000000) {
			$pphThn	=(0.05*50000000) + (0.15*($pKPJ-50000000));
		}
		elseif($pKPJ>250000000 && $pKPJ<=500000000) {
			$pphThn	=(0.05*50000000) + (0.15*200000000)+(0.25*($pKPJ-250000000));
		}
		elseif($pKPJ>500000000) {
			$pphThn	=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($pKPJ-500000000));
		}
		
		// PKPJ + KESPEG
		if($pkpj_kespeg < 0) {
			$pkpj_kespeg = 0;
		}
		
		$pKPJ_Kespeg = substr_replace($pkpj_kespeg,"000",-3); 
		$pphThn_kespeg = 0;
		
		if($pKPJ_Kespeg <= 50000000) {
			$pphThn_kespeg	= (0.05*$pKPJ_Kespeg);
		}
		elseif($pKPJ_Kespeg>50000000 && $pKPJ_Kespeg<=250000000) {
			$pphThn_kespeg	=(0.05*50000000) + (0.15*($pKPJ_Kespeg-50000000));
		}
		elseif($pKPJ_Kespeg>250000000 && $pKPJ_Kespeg<=500000000) {
			$pphThn_kespeg	=(0.05*50000000) + (0.15*200000000)+(0.25*($pKPJ_Kespeg-250000000));
		}
		elseif($pKPJ_Kespeg>500000000) {
			$pphThn_kespeg	=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($pKPJ_Kespeg-500000000));
		}
		
		$pph_kespeg_13 = $pphThn_kespeg - $pphThn;
		
		//echo $kespeg;
		
		//echo $net_bln."=".$net_12."=".$net_kespeg."=".$pkpj."=".$pkpj_kespeg."=".$gator_12."=".$gator_12_kespeg."=".$kespeg;
		
		//echo $gaji_pokok."=".$kespeg."=".$tjistri."=".$tjanak."=".$tjberas."=".$gator."=".$gator_12_kespeg."=".$ptkp."=".$beajab."=".$beajab_kespeg."=".$pph_kespeg_13;		
		return $pph_kespeg_13;
	}
	
	function pph_13($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip){
		if($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} 
		else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}
			
		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;
		
		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}
		
		$pen_tht_tahun = $pen_tht * 12;
		
		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;
		
		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
			
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);		
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}
	
		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
		
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}
		
		if($penghasilan_kpj_13 < 0) {
			 $penghasilan_kpj_13 = 0;
		}
				
		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13,"000",-3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		// PPH Biasa
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		
		// PPH 13
		if($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		}
		elseif($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		}
		elseif($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj_13-250000000));
		}
		elseif($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj_13-500000000));
		}
		
		return $pph_tahun_13 - $pph_tahun;
	}
	
	function pph_14($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip){
		if($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} 
		else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}
			
		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;
		
		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}
		
		$pen_tht_tahun = $pen_tht * 12;
		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;
		
		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
			
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;
				
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);		
		if(substr($nip,14,1)==2) { //Jika pegawainya cewek
			$ptkp = 54000000;
		}
	
		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
		
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}
		
		if($penghasilan_kpj_13 < 0) {
			 $penghasilan_kpj_13 = 0;
		}
				
		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13,"000",-3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		// PPH Biasa
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		
		// PPH 13
		if($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		}
		elseif($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		}
		elseif($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj_13-250000000));
		}
		elseif($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj_13-500000000));
		}
		
		/*if($nip == '196002291986031004') {
			echo $bea_jabatan."=".$bea_jabatan_13."=";
			echo $kurang_gaji."=".$penghasilan_bulan."=@";
			echo $ptkp."=";
			echo $penghasilan_tahun."=".$penghasilan_kpj."=".$penghasilan_kpj_13."=#";
			echo $pph_tahun."=".$pph_tahun_13."=";
			echo $pph_tahun_13 - $pph_tahun;
			
			die();
		}*/
		
		return $pph_tahun_13 - $pph_tahun;	
	}
	
	function gaji_tahunan($periode, $arrLokasi) {
		$kalkulasis = ORM::factory('kalkulasi')
			->where('lokasi_id','IN',$arrLokasi)
			->find_all();	
		
		$arrNip = array();	
		foreach($kalkulasis as $k) {
			array_push($arrNip, $k->nip);
		}
		$arrNip = array_unique($arrNip);
		//$arrNip = array('196806141990011001');
		
		$value = "";		
				
		foreach($arrNip as $nip) {
			$n = ORM::factory('kalkulasi')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				continue;
			}
			
			$gaji_pokok = 0;
			$tunjangan_istri = 0;	
			$tunjangan_anak = 0;
			$jumlah_tunjangan_keluarga = 0;
			$tunjangan_umum = 0;
			$tunjangan_umum_tambahan = 0;
			$tunjangan_struktural = 0;
			$tunjangan_fungsional = 0;
			$tunjangan_beras = 0;
			$tunjangan_pph = 0;
			$pembulatan = 0;
			$jumlah_kotor = 0;
			$potongan_bpjs_kesehatan = 0;
			$potongan_pensiun = 0;
			$potongan_iwp = 0;
			$potongan_lain = 0;
			$potongan_cp = 0;
			$jumlah_potongan = 0;
			$jumlah_bersih = 0;
			$jumlah_bersih_bayar = 0;
			$askes = 0;
			$sgaji_pokok = 0;
			$stunjangan_istri = 0;	
			$stunjangan_anak = 0;
			$sjumlah_tunjangan_keluarga = 0;
			$stunjangan_umum = 0;
			$stunjangan_umum_tambahan = 0;
			$stunjangan_struktural = 0;
			$stunjangan_fungsional = 0;
			$stunjangan_beras = 0;
			$stunjangan_pph = 0;
			$spembulatan = 0;
			$sjumlah_kotor = 0;
			$spotongan_bpjs_kesehatan = 0;
			$spotongan_pensiun = 0;
			$spotongan_iwp = 0;
			$spotongan_lain = 0;
			$spotongan_cp = 0;
			$sjumlah_potongan = 0;
			$sjumlah_bersih = 0;
			$sjumlah_bersih_bayar = 0;
			$saskes = 0;
			$n_bea_jabatan = 0;
			$n_tht = 0;
			$n_netto = 0;
			$n_ptkp = 0;
			$n_pkpj = 0;
			
			// SUM GAJI BULANAN
			$sql = "SELECT 
			lokasi_id, lokasi_kode, lokasi_string, name, tanggal_lahir, nip, 
			status_string, golongan_id, golongan_string, jabatan, marital_id,
			marital_string, istri, anak, jiwa, jiwa_string, status_id, kelompok_gaji,
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM kalkulasis		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$lokasi_id = $q->lokasi_id;
				$lokasi_kode = $q->lokasi_kode;
				$lokasi_string = $q->lokasi_string;
				$name = $q->name;
				$tanggal_lahir = $q->tanggal_lahir;
				$nip = $q->nip;
				$status_id = $q->status_id;
				$status_string = $q->status_string;
				$golongan_id = $q->golongan_id;
				$golongan_string = $q->golongan_string;
				$jabatan = $q->jabatan;
				$marital_id = $q->marital_id;
				$marital_string = $q->marital_string;
				$istri = $q->istri;
				$anak = $q->anak;
				$jiwa = $q->jiwa;
				$jiwa_string = $q->jiwa_string;
				$kelompok_gaji = $q->kelompok_gaji;
				
				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = $tunjangan_pph + $q->tunjangan_pph;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$potongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$askes = $askes + $q->askes;
			}
			
			//echo $gaji_pokok;
			//die();
				
			// GAJI 13
			$n = ORM::factory('kalkulasitb')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				$sgaji_pokok = $gaji_pokok + 0;
				$stunjangan_istri = $tunjangan_istri + 0;	
				$stunjangan_anak = $tunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $tunjangan_umum + 0;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $tunjangan_struktural + 0;
				$stunjangan_fungsional = $tunjangan_fungsional + 0;
				$stunjangan_beras = $tunjangan_beras + 0;
				$stunjangan_pph = $tunjangan_pph + 0;
				$spembulatan = $pembulatan + 0;
				$sjumlah_kotor = $jumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $potongan_pensiun + 0;
				$spotongan_iwp = $potongan_iwp + 0;
				$spotongan_lain = $potongan_lain + 0;
				$spotongan_cp = $potongan_cp + 0;
				$sjumlah_potongan = $jumlah_potongan + 0;
				$sjumlah_bersih = $jumlah_bersih + 0;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + 0;
				$saskes = $askes + 0;
			}
			
			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM kalkulasitbs		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$sgaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$stunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = $tunjangan_pph + $q->tunjangan_pph;
				$spembulatan = $pembulatan + $q->pembulatan;
				$sjumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $potongan_lain + $q->potongan_lain;
				$spotongan_cp = $potongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $askes + $q->askes;
			}
			
			// RAPEL GAJI
			//$n = ORM::factory('kalkulasis_rapels_final')
			//	->where('YEAR("periode")','=',$periode)
			//	->where('nip','=',$nip)
			//	->count_all();
			
			//if($n == 0) {
			//	continue;
			//}
			
			/*$sql = "SELECT 
			SUM(gaji_pokok_4) as gaji_pokok,
			SUM(tunjangan_istri_4) as tunjangan_istri,	
			SUM(tunjangan_anak_4) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga_4) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum_4) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan_4) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural_4) as tunjangan_struktural,
			SUM(tunjangan_fungsional_4) as tunjangan_fungsional,
			SUM(tunjangan_beras_4) as tunjangan_beras,
			SUM(tunjangan_pph_4) as tunjangan_pph,
			SUM(pembulatan_4) as pembulatan,
			SUM(jumlah_kotor_4) as jumlah_kotor,
			SUM(potongan_iwp_4) as potongan_iwp,
			SUM(potongan_lain_4) as potongan_lain,
			SUM(potongan_cp_4) as potongan_cp,
			SUM(jumlah_potongan_4) as jumlah_potongan,
			SUM(jumlah_bersih_4) as jumlah_bersih,
			SUM(jumlah_bersih_bayar_4) as jumlah_bersih_bayar
			FROM kalkulasis_rapels_finals		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = $tunjangan_pph + $q->tunjangan_pph;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
			}*/
			
			// GAJI 14
			$n = ORM::factory('kalkulasieb')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				$sgaji_pokok = $sgaji_pokok + 0;
				$stunjangan_istri = $stunjangan_istri + 0;	
				$stunjangan_anak = $stunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $stunjangan_umum + 0;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $stunjangan_struktural + 0;
				$stunjangan_fungsional = $stunjangan_fungsional + 0;
				$stunjangan_beras = $stunjangan_beras + 0;
				$stunjangan_pph = $stunjangan_pph + 0;
				$spembulatan = $spembulatan + 0;
				$sjumlah_kotor = $sjumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $spotongan_pensiun + 0;
				$spotongan_iwp = $spotongan_iwp + 0;
				$spotongan_lain = $spotongan_lain + 0;
				$spotongan_cp = $spotongan_cp + 0;
				$sjumlah_potongan = $sjumlah_potongan + 0;
				$sjumlah_bersih = $sjumlah_bersih + 0;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + 0;
				$saskes = $saskes + 0;
			}
			
			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM kalkulasiebs		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$sgaji_pokok = $sgaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $stunjangan_istri + $q->tunjangan_istri;	
				$stunjangan_anak = $stunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $stunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $stunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $stunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $stunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = $stunjangan_pph + $q->tunjangan_pph;
				$spembulatan = $spembulatan + $q->pembulatan;
				$sjumlah_kotor = $sjumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $spotongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $spotongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $spotongan_lain + $q->potongan_lain;
				$spotongan_cp = $spotongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $sjumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $sjumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $saskes + $q->askes;
			}
			
			$pertama = ORM::factory('kalkulasi')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->order_by('periode','DESC')
				->find();
			
			$n_bea_jabatan = 0.05 * ($sjumlah_kotor - $stunjangan_pph);
			$n_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
			if($n_tht > 2400000){
					$n_tht = 2400000;
				}
			$n_netto = $sjumlah_kotor - ($stunjangan_pph + $n_bea_jabatan + $n_tht);
			$n_ptkp = 54000000 + ((4500000 * $pertama->istri) + (4500000 * $pertama->anak));
			if(substr($nip,14,1)==2) {
			$n_ptkp = 54000000;
			}
			
			$n_pkpj = $n_netto - $n_ptkp;
			if($n_pkpj < 0 ) {
				$n_pkpj = 0;
			}
			
			$arrValue = array(
				"'".mysql_real_escape_string(date("Y-m-d"))."'",
				"'".mysql_real_escape_string($periode)."'",
				$lokasi_id,
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($tanggal_lahir)."'",
				$nip,
				$status_id,
				"'".mysql_real_escape_string($status_string)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($pertama->jabatan)."'",
				$marital_id,
				"'".mysql_real_escape_string($marital_string)."'",
				$istri,
				$anak,
				$jiwa,
				"'".mysql_real_escape_string($jiwa_string)."'",
				$sgaji_pokok,
				$stunjangan_istri,
				$stunjangan_anak,
				$stunjangan_istri + $stunjangan_anak,
				$sgaji_pokok + $stunjangan_istri + $stunjangan_anak,
				$stunjangan_umum,
				$stunjangan_umum_tambahan,
				$stunjangan_struktural,
				$stunjangan_fungsional,
				$stunjangan_beras,
				$stunjangan_pph,
				$spembulatan,
				$sjumlah_kotor,
				$spotongan_bpjs_kesehatan,
				$spotongan_pensiun,
				$spotongan_iwp,
				$spotongan_lain,
				$stunjangan_beras,
				$spotongan_cp,
				$sjumlah_potongan,
				$sjumlah_bersih,
				$sjumlah_bersih_bayar,
				$saskes,
				"'".mysql_real_escape_string($kelompok_gaji)."'",
				$n_bea_jabatan,
				$n_tht,
				$n_netto,
				$n_ptkp,
				$n_pkpj				
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji',
			'bea_jabatan',
			'tht',
			'penghasilan_netto',
			'ptkp',
			'pkpj'					
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO tahunangajis (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();
	}	
	
	function gajip3k_tahunan($periode, $arrLokasi) {
		$kalkulasis = ORM::factory('kalkulasip3k')
			->where('lokasi_id','IN',$arrLokasi)
			->find_all();	
		
		$arrNip = array();	
		foreach($kalkulasis as $k) {
			array_push($arrNip, $k->nip);
		}
		$arrNip = array_unique($arrNip);
		//$arrNip = array('196806141990011001');
		
		$value = "";		
				
		foreach($arrNip as $nip) {
			$n = ORM::factory('kalkulasip3k')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				continue;
			}
			
			$gaji_pokok = 0;
			$tunjangan_istri = 0;	
			$tunjangan_anak = 0;
			$jumlah_tunjangan_keluarga = 0;
			$tunjangan_umum = 0;
			$tunjangan_umum_tambahan = 0;
			$tunjangan_struktural = 0;
			$tunjangan_fungsional = 0;
			$tunjangan_beras = 0;
			$tunjangan_pph = 0;
			$pembulatan = 0;
			$jumlah_kotor = 0;
			$potongan_bpjs_kesehatan = 0;
			$potongan_pensiun = 0;
			$potongan_iwp = 0;
			$potongan_lain = 0;
			$potongan_cp = 0;
			$jumlah_potongan = 0;
			$jumlah_bersih = 0;
			$jumlah_bersih_bayar = 0;
			$askes = 0;
			$sgaji_pokok = 0;
			$stunjangan_istri = 0;	
			$stunjangan_anak = 0;
			$sjumlah_tunjangan_keluarga = 0;
			$stunjangan_umum = 0;
			$stunjangan_umum_tambahan = 0;
			$stunjangan_struktural = 0;
			$stunjangan_fungsional = 0;
			$stunjangan_beras = 0;
			$stunjangan_pph = 0;
			$spembulatan = 0;
			$sjumlah_kotor = 0;
			$spotongan_bpjs_kesehatan = 0;
			$spotongan_pensiun = 0;
			$spotongan_iwp = 0;
			$spotongan_lain = 0;
			$spotongan_cp = 0;
			$sjumlah_potongan = 0;
			$sjumlah_bersih = 0;
			$sjumlah_bersih_bayar = 0;
			$saskes = 0;
			$n_bea_jabatan = 0;
			$n_tht = 0;
			$n_netto = 0;
			$n_ptkp = 0;
			$n_pkpj = 0;
			
			// SUM GAJI BULANAN
			$sql = "SELECT 
			lokasi_id, lokasi_kode, lokasi_string, name, tanggal_lahir, nip, 
			status_string, golongan_id, golongan_string, jabatan, marital_id,
			marital_string, istri, anak, jiwa, jiwa_string, status_id, kelompok_gaji,
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM kalkulasip3ks		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$lokasi_id = $q->lokasi_id;
				$lokasi_kode = $q->lokasi_kode;
				$lokasi_string = $q->lokasi_string;
				$name = $q->name;
				$tanggal_lahir = $q->tanggal_lahir;
				$nip = $q->nip;
				$status_id = $q->status_id;
				$status_string = $q->status_string;
				$golongan_id = $q->golongan_id;
				$golongan_string = $q->golongan_string;
				$jabatan = $q->jabatan;
				$marital_id = $q->marital_id;
				$marital_string = $q->marital_string;
				$istri = $q->istri;
				$anak = $q->anak;
				$jiwa = $q->jiwa;
				$jiwa_string = $q->jiwa_string;
				$kelompok_gaji = $q->kelompok_gaji;
				
				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = 0;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$potongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$askes = $askes + $q->askes;
			}
			
			//echo $gaji_pokok;
			//die();
				
			// GAJI 13
			$n = ORM::factory('kalkulasitbp3k')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				$sgaji_pokok = $gaji_pokok + 0;
				$stunjangan_istri = $tunjangan_istri + 0;	
				$stunjangan_anak = $tunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $tunjangan_umum + 0;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $tunjangan_struktural + 0;
				$stunjangan_fungsional = $tunjangan_fungsional + 0;
				$stunjangan_beras = $tunjangan_beras + 0;
				$stunjangan_pph = 0;
				$spembulatan = $pembulatan + 0;
				$sjumlah_kotor = $jumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $potongan_pensiun + 0;
				$spotongan_iwp = $potongan_iwp + 0;
				$spotongan_lain = $potongan_lain + 0;
				$spotongan_cp = $potongan_cp + 0;
				$sjumlah_potongan = $jumlah_potongan + 0;
				$sjumlah_bersih = $jumlah_bersih + 0;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + 0;
				$saskes = $askes + 0;
			}
			
			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM kalkulasitbp3ks		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$sgaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$stunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = 0;
				$spembulatan = $pembulatan + $q->pembulatan;
				$sjumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $potongan_lain + $q->potongan_lain;
				$spotongan_cp = $potongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $askes + $q->askes;
			}
			
			// RAPEL GAJI
			//$n = ORM::factory('kalkulasis_rapels_final')
			//	->where('YEAR("periode")','=',$periode)
			//	->where('nip','=',$nip)
			//	->count_all();
			
			//if($n == 0) {
			//	continue;
			//}
			
			/*$sql = "SELECT 
			SUM(gaji_pokok_4) as gaji_pokok,
			SUM(tunjangan_istri_4) as tunjangan_istri,	
			SUM(tunjangan_anak_4) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga_4) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum_4) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan_4) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural_4) as tunjangan_struktural,
			SUM(tunjangan_fungsional_4) as tunjangan_fungsional,
			SUM(tunjangan_beras_4) as tunjangan_beras,
			SUM(tunjangan_pph_4) as tunjangan_pph,
			SUM(pembulatan_4) as pembulatan,
			SUM(jumlah_kotor_4) as jumlah_kotor,
			SUM(potongan_iwp_4) as potongan_iwp,
			SUM(potongan_lain_4) as potongan_lain,
			SUM(potongan_cp_4) as potongan_cp,
			SUM(jumlah_potongan_4) as jumlah_potongan,
			SUM(jumlah_bersih_4) as jumlah_bersih,
			SUM(jumlah_bersih_bayar_4) as jumlah_bersih_bayar
			FROM kalkulasis_rapels_finals		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = $tunjangan_pph + $q->tunjangan_pph;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
			}*/
			
			// GAJI 14
			$n = ORM::factory('kalkulasiebp3k')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->count_all();
			
			if($n == 0) {
				$sgaji_pokok = $sgaji_pokok + 0;
				$stunjangan_istri = $stunjangan_istri + 0;	
				$stunjangan_anak = $stunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $stunjangan_umum + 0;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $stunjangan_struktural + 0;
				$stunjangan_fungsional = $stunjangan_fungsional + 0;
				$stunjangan_beras = $stunjangan_beras + 0;
				$stunjangan_pph = $stunjangan_pph + 0;
				$spembulatan = 0;
				$sjumlah_kotor = $sjumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $spotongan_pensiun + 0;
				$spotongan_iwp = $spotongan_iwp + 0;
				$spotongan_lain = $spotongan_lain + 0;
				$spotongan_cp = $spotongan_cp + 0;
				$sjumlah_potongan = $sjumlah_potongan + 0;
				$sjumlah_bersih = $sjumlah_bersih + 0;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + 0;
				$saskes = $saskes + 0;
			}
			
			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM kalkulasiebp3ks		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$sgaji_pokok = $sgaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $stunjangan_istri + $q->tunjangan_istri;	
				$stunjangan_anak = $stunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $stunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $stunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $stunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $stunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = 0;
				$spembulatan = $spembulatan + $q->pembulatan;
				$sjumlah_kotor = $sjumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $spotongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $spotongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $spotongan_lain + $q->potongan_lain;
				$spotongan_cp = $spotongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $sjumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $sjumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $saskes + $q->askes;
			}
			
			$pertama = ORM::factory('kalkulasip3k')
				->where('YEAR("periode")','=',$periode)
				->where('nip','=',$nip)
				->order_by('periode','DESC')
				->find();
			
			$n_bea_jabatan = 0.05 * ($sjumlah_kotor - $stunjangan_pph);
			$n_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
			if($n_tht > 2400000){
					$n_tht = 2400000;
				}
			$n_netto = $sjumlah_kotor - ($stunjangan_pph + $n_bea_jabatan + $n_tht);
			$n_ptkp = 54000000 + ((4500000 * $pertama->istri) + (4500000 * $pertama->anak));
			if(substr($nip,14,1)==2) {
			$n_ptkp = 54000000;
			}
			
			$n_pkpj = $n_netto - $n_ptkp;
			if($n_pkpj < 0 ) {
				$n_pkpj = 0;
			}
			
			$arrValue = array(
				"'".mysql_real_escape_string(date("Y-m-d"))."'",
				"'".mysql_real_escape_string($periode)."'",
				$lokasi_id,
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($tanggal_lahir)."'",
				$nip,
				$status_id,
				"'".mysql_real_escape_string($status_string)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($pertama->jabatan)."'",
				$marital_id,
				"'".mysql_real_escape_string($marital_string)."'",
				$istri,
				$anak,
				$jiwa,
				"'".mysql_real_escape_string($jiwa_string)."'",
				$sgaji_pokok,
				$stunjangan_istri,
				$stunjangan_anak,
				$stunjangan_istri + $stunjangan_anak,
				$sgaji_pokok + $stunjangan_istri + $stunjangan_anak,
				$stunjangan_umum,
				$stunjangan_umum_tambahan,
				$stunjangan_struktural,
				$stunjangan_fungsional,
				$stunjangan_beras,
				0,
				$spembulatan,
				$sjumlah_kotor,
				$spotongan_bpjs_kesehatan,
				$spotongan_pensiun,
				$spotongan_iwp,
				$spotongan_lain,
				$stunjangan_beras,
				$spotongan_cp,
				$sjumlah_potongan,
				$sjumlah_bersih,
				$sjumlah_bersih_bayar,
				$saskes,
				"'".mysql_real_escape_string($kelompok_gaji)."'",
				$n_bea_jabatan,
				$n_tht,
				$n_netto,
				$n_ptkp,
				$n_pkpj				
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji',
			'bea_jabatan',
			'tht',
			'penghasilan_netto',
			'ptkp',
			'pkpj'					
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO tahunangajip3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();
	}	
	
	function gaji_susulan() {
		$value = "";
		$susulans = ORM::factory('susulan')->find_all();
		
		$last_susulan = ORM::factory('susulan')
			->order_by('periode_end','DESC')
			->find();
		
		foreach($susulans as $susulan) {
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$susulan->nip)
				->count_all();
			
			if($n_kalkulasi) {	
				$kalkulasi = ORM::factory('kalkulasi')
					->where('nip','=',$susulan->nip)
					->order_by('periode','ASC')
					->find();
				
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($last_susulan->periode_end)."'",
					"'".mysql_real_escape_string($susulan->periode_start)."'",
					"'".mysql_real_escape_string($susulan->periode_end)."'",
					$kalkulasi->lokasi_id,
					"'".mysql_real_escape_string($kalkulasi->lokasi_kode)."'",
					"'".mysql_real_escape_string($kalkulasi->lokasi_string)."'",
					"'".mysql_real_escape_string($kalkulasi->name)."'",
					"'".mysql_real_escape_string($kalkulasi->tanggal_lahir)."'",
					$kalkulasi->nip,
					$kalkulasi->status_id,
					"'".mysql_real_escape_string($kalkulasi->status_string)."'",
					$kalkulasi->golongan_id,
					"'".mysql_real_escape_string($kalkulasi->golongan_string)."'",
					"'".mysql_real_escape_string($kalkulasi->jabatan)."'",
					$kalkulasi->marital_id,
					"'".mysql_real_escape_string($kalkulasi->marital_string)."'",
					$kalkulasi->istri,
					$kalkulasi->anak,
					$kalkulasi->jiwa,
					"'".mysql_real_escape_string($kalkulasi->jiwa_string)."'",
					$kalkulasi->gaji_pokok * $susulan->jumlah,
					$kalkulasi->tunjangan_istri * $susulan->jumlah,
					$kalkulasi->tunjangan_anak * $susulan->jumlah,
					($kalkulasi->tunjangan_istri + $kalkulasi->tunjangan_anak) * $susulan->jumlah,
					($kalkulasi->gaji_pokok + $kalkulasi->tunjangan_istri + $kalkulasi->tunjangan_anak) * $susulan->jumlah,
					$kalkulasi->tunjangan_umum * $susulan->jumlah,
					$kalkulasi->tunjangan_umum_tambahan * $susulan->jumlah,
					$kalkulasi->tunjangan_struktural * $susulan->jumlah,
					$kalkulasi->tunjangan_fungsional * $susulan->jumlah,
					$kalkulasi->tunjangan_beras * $susulan->jumlah,
					$kalkulasi->tunjangan_pph * $susulan->jumlah,
					$kalkulasi->pembulatan * $susulan->jumlah,
					$kalkulasi->jumlah_kotor * $susulan->jumlah,
					$kalkulasi->potongan_iwp * $susulan->jumlah,
					$kalkulasi->potongan_lain * $susulan->jumlah,
					$kalkulasi->potongan_beras * $susulan->jumlah,
					$kalkulasi->potongan_cp * $susulan->jumlah,
					$kalkulasi->jumlah_potongan * $susulan->jumlah,
					$kalkulasi->jumlah_bersih * $susulan->jumlah,
					$kalkulasi->jumlah_bersih_bayar * $susulan->jumlah,
					$kalkulasi->askes * $susulan->jumlah,
					"'".mysql_real_escape_string($kalkulasi->kelompok_gaji)."'"					
				);
					
				$value .= "(".implode(",",$arrValue)."),";	
			}
		}
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'periode_start',
			'periode_end',
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'					
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasisusulans (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();
	}
	
	function gaji_desember($arrLokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		$periode 	= "2020-12-01";
		
		$pegawais = ORM::factory('pegawai')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			//->where('nip','=','196806141990011001') // Arief Irwanto
			//->where('nip','=','196708131989032005') // Widyowati
			->find_all();
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				//$gaji_kotor_pembulatan = $gaji_kotor;
				
				// 13
				$kalkulasitbs = ORM::factory('kalkulasitb')
					->where('nip','=',$pegawai->nip)
					->where('YEAR("periode")','=',$periode)
					->find();
				
				$gaji_13 = $kalkulasitbs->gaji_pokok + $kalkulasitbs->tunjangan_istri + $kalkulasitbs->tunjangan_anak + $kalkulasitbs->tunjangan_umum + $kalkulasitbs->tunjangan_fungsional + $kalkulasitbs->tunjangan_struktural + $kalkulasitbs->pembulatan;	
					
				// 14
				$kalkulasiebs = ORM::factory('kalkulasieb')
					->where('nip','=',$pegawai->nip)
					->where('YEAR("periode")','=',$periode)
					->find();
				
				$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->tunjangan_istri + $kalkulasiebs->tunjangan_anak + $kalkulasiebs->tunjangan_umum + $kalkulasiebs->tunjangan_fungsional + $kalkulasiebs->tunjangan_struktural + $kalkulasiebs->pembulatan;
				//$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->pembulatan;
				
				// Gaji Jan sd Nov 
				$sql_pph_tahun = 
				"SELECT SUM(tunjangan_pph) AS pph_tahun
				FROM kalkulasis WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$xperiode[0];
				
				$pph_tahun = DB::query(Database::SELECT, $sql_pph_tahun)->execute()->get('pph_tahun', 0);

				$sql_pph_13_tahun = 
				"SELECT SUM(tunjangan_pph) AS pph_13_tahun
				FROM kalkulasitbs WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$xperiode[0];
				
				$pph_13_tahun = DB::query(Database::SELECT, $sql_pph_13_tahun)->execute()->get('pph_13_tahun', 0);
				
				$sql_pph_14_tahun = 
				"SELECT SUM(tunjangan_pph) AS pph_14_tahun
				FROM kalkulasiebs WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$xperiode[0];
				
				$pph_14_tahun = DB::query(Database::SELECT, $sql_pph_14_tahun)->execute()->get('pph_14_tahun', 0);
				$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun;
				
				$pph_bulan = $this->pph_desember($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip,$gaji_13,$gaji_14,$jumlah_pph,$xperiode[0],$gaji_pokok,$tunjangan_istri,$tunjangan_anak);
			
				//echo $gaji_pokok."#".$tunjangan_istri."#".$tunjangan_anak;
				//echo $tunjangan_umum."#".$tunjangan_fungsional."#".$tunjangan_struktural."#".$tunjangan_beras;
				//echo $gaji_13."#".$gaji_14."<br>";
				
				//echo $pph_bulan."#".$pph_tahun."#".$pph_13_tahun."#".$pph_14_tahun;
				//echo $pph_bulan;
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				if($pph_bulan < 0) {
					$pph_bulan = 0;
				}
				
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'"					
				);
				
				$value .= "(".implode(",",$arrValue)."),";
			}
		}
		
		//die();
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'					
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasis (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function gajip3k_desember($arrLokasi){							
		$this->auto_render = false;
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		$periode 	= "2020-12-01";
		
		$pegawaip3ks = ORM::factory('pegawaip3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('status_id','IN',array(1,2,7,8,9))
			//->where('nip','=','196806141990011001') // Arief Irwanto
			//->where('nip','=','196708131989032005') // Widyowati
			->find_all();
		
		foreach($pegawaip3ks as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon->usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan->name;			
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional->usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional->name;
				}
				else {
					if($pegawai->kedudukan->usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan->name;
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {					
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak;
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				//$gaji_kotor_pembulatan = $gaji_kotor;
				
				// 13
				$kalkulasitbs = ORM::factory('kalkulasitbp3k')
					->where('nip','=',$pegawai->nip)
					->where('YEAR("periode")','=',$periode)
					->find();
				
				$gaji_13 = $kalkulasitbs->gaji_pokok + $kalkulasitbs->tunjangan_istri + $kalkulasitbs->tunjangan_anak + $kalkulasitbs->tunjangan_umum + $kalkulasitbs->tunjangan_fungsional + $kalkulasitbs->tunjangan_struktural + $kalkulasitbs->pembulatan;	
					
				// 14
				$kalkulasiebs = ORM::factory('kalkulasiebp3k')
					->where('nip','=',$pegawai->nip)
					->where('YEAR("periode")','=',$periode)
					->find();
				
				$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->tunjangan_istri + $kalkulasiebs->tunjangan_anak + $kalkulasiebs->tunjangan_umum + $kalkulasiebs->tunjangan_fungsional + $kalkulasiebs->tunjangan_struktural + $kalkulasiebs->pembulatan;
				//$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->pembulatan;
				
				// Gaji Jan sd Nov 
				$sql_pph_tahun = 
				"SELECT SUM(tunjangan_pph) AS pph_tahun
				FROM kalkulasip3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$xperiode[0];
				
				$pph_tahun = DB::query(Database::SELECT, $sql_pph_tahun)->execute()->get('pph_tahun', 0);

				$sql_pph_13_tahun = 
				"SELECT SUM(tunjangan_pph) AS pph_13_tahun
				FROM kalkulasitbp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$xperiode[0];
				
				$pph_13_tahun = DB::query(Database::SELECT, $sql_pph_13_tahun)->execute()->get('pph_13_tahun', 0);
				
				$sql_pph_14_tahun = 
				"SELECT SUM(tunjangan_pph) AS pph_14_tahun
				FROM kalkulasiebp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$xperiode[0];
				
				$pph_14_tahun = DB::query(Database::SELECT, $sql_pph_14_tahun)->execute()->get('pph_14_tahun', 0);
				$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun;
				
				$pph_bulan = $this->pph_desember($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip,$gaji_13,$gaji_14,$jumlah_pph,$xperiode[0],$gaji_pokok,$tunjangan_istri,$tunjangan_anak);
			
				//echo $gaji_pokok."#".$tunjangan_istri."#".$tunjangan_anak;
				//echo $tunjangan_umum."#".$tunjangan_fungsional."#".$tunjangan_struktural."#".$tunjangan_beras;
				//echo $gaji_13."#".$gaji_14."<br>";
				
				//echo $pph_bulan."#".$pph_tahun."#".$pph_13_tahun."#".$pph_14_tahun;
				//echo $pph_bulan;
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				if($pph_bulan < 0) {
					$pph_bulan = 0;
				}
				
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'"					
				);
				
				$value .= "(".implode(",",$arrValue)."),";
			}
		}
		
		//die();
		
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'					
		);
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function pph_desember($istri, $anak, $pen_tht, $gaji_kotor_pembulatan,$nip,$gaji_13,$gaji_14,$jumlah_pph,$tahun,$gaji_pokok,$tunjangan_istri,$tunjangan_anak){
		$sql_gaji_kotor = 
		"SELECT SUM(gaji_pokok + tunjangan_istri + tunjangan_anak + tunjangan_struktural + tunjangan_beras + tunjangan_fungsional + tunjangan_umum + pembulatan) AS gaji_kotor
		FROM kalkulasis WHERE nip = '".$nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
		/*$sql_gaji_kotor = 
		"SELECT SUM(k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak + k.tunjangan_struktural + k.tunjangan_beras + k.tunjangan_fungsional 
+ k.tunjangan_umum + k.pembulatan) + f.jumlah_penghasilan_4 AS gaji_kotor
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND MONTH(k.periode) < 12 AND YEAR(k.periode) = ".$tahun;*/
		
		$gaji_kotor = DB::query(Database::SELECT, $sql_gaji_kotor)->execute()->get('gaji_kotor', 0);
		$bruto_12 = $gaji_kotor + $gaji_kotor_pembulatan + $gaji_13 + $gaji_14;
		
		$sql_pen_tht = 
		"SELECT SUM(0.0475 * (gaji_pokok + tunjangan_istri + tunjangan_anak)) AS pen_tht_12
		FROM kalkulasis WHERE nip = '".$nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
		
		/*$sql_pen_tht = 
		"SELECT SUM(0.0475 * (k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak))+ (0.0475 * (f.gaji_pokok_4 + f.tunjangan_istri_4 + f.tunjangan_anak_4)) AS pen_tht_12
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND MONTH(k.periode) < 12 AND YEAR(k.periode) = ".$tahun;*/
		
		$pen_tht_12 = DB::query(Database::SELECT, $sql_pen_tht)->execute()->get('pen_tht_12', 0);
		
		$pen_tht_12 = $pen_tht_12 + (0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak));
		if($pen_tht_12 > 2400000){
			$pen_tht_12 = 2400000;
		}
		
		$bea_jabatan_12 = ceil(0.05 * $bruto_12);
		if ($bea_jabatan_12 > 6000000) {
			$bea_jabatan_12 = 6000000;
		}
		
		$kurang_gaji_12 = $bea_jabatan_12 + $pen_tht_12;
		$penghasilan_tahun	= $bruto_12 - $kurang_gaji_12;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}
		
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		//$pph_bulan	 = floor($pph_tahun/12) ;
		
		//dafiz
		/*$sql_pph_rapel = 
		"SELECT tunjangan_pph_4 AS pph_rapel
FROM kalkulasis_rapels_finals WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
				
		$pph_rapel = DB::query(Database::SELECT, $sql_pph_rapel)->execute()->get('pph_rapel', 0);*/
		//dafiz end
		
		$pph_bulan = $pph_tahun - $jumlah_pph ;
		if($pph_bulan <= 0) {
			$pph_bulan = 0;
		}
	
		return $pph_bulan;
		//return $bruto_12."#".$bea_jabatan_12."#".$pen_tht_12."#".$kurang_gaji_12;
		//return $gaji_kotor_pembulatan."#".$gaji_kotor_pembulatan_12."#".$bruto_12."#".$bea_jabatan_12."#".$kurang_gaji_12."#".$penghasilan_tahun."#".$ptkp."#".$penghasilan_kpj;
		//return $gaji_kotor."#".$bruto_12."#".$gaji_kotor_pembulatan."#".$gaji_13."#".$gaji_14;
		//return $pph_tahun."#".$jumlah_pph;
		//return $bruto_12;
	}
	
	function tpp_desember($arrLokasi) {							
		$this->auto_render = false;
		
		$i = 1;
		$periode = "2020-12-01";
		$xperiode = explode("-",$periode);
		$tahun = $xperiode[0];
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Dinsos
		$arrDinsos = array(21);
		$arrKodeDinsos = array();
		$lokasi_dinsos = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrDinsos)
			->find_all();
			
		foreach($lokasi_dinsos as $dinsos) {
			array_push($arrKodeDinsos,$dinsos->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentip')
			->where('lokasi_gaji','IN',$arrLokasi)
			//->where('nip','=','196806141990011001')
			//->where('nip','=','196211161990101001')
			->where('tpp_stop','=',1)
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$bpjs_gaji = $kalkulasi->jumlah_penghasilan + $kalkulasi->tunjangan_umum + $kalkulasi->tunjangan_struktural + $kalkulasi->tunjangan_fungsional;
			$max_bpjs = 12000000 - $bpjs_gaji;
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
			
			$tjKespeg2 = 0;
			$potongan2 = 0;
			$tunjangan_insentip2 = 0;
			$tjKonker2 = 0;
			$tjJakarta2 = 0;
			$pph_konker2 = 0;
			$pph_jakarta2 = 0;
			$bpjs_konker2 = 0;	
			$bpjs_jakarta2 = 0;	
			$askes_konker2 = 0;	
			$askes_jakarta2 = 0;
			$konker_kotor2 = 0;
			$jakarta_kotor2 = 0;
			$potongan_tpp_bpjs2 = 0;
			$pph_kespeg2 = 0;
			$askes_tpp2 = 0;
			
			$tjKespeg3 = 0;
			$potongan3 = 0;
			$tunjangan_insentip3 = 0;
			$tjKonker3 = 0;
			$tjJakarta3 = 0;
			$pph_konker3 = 0;
			$pph_jakarta3 = 0;
			$bpjs_konker3 = 0;	
			$bpjs_jakarta3 = 0;	
			$askes_konker3 = 0;	
			$askes_jakarta3 = 0;
			$konker_kotor3 = 0;
			$jakarta_kotor3 = 0;
			$potongan_tpp_bpjs3 = 0;
			$pph_kespeg3 = 0;
			$askes_tpp3 = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			// 13
			$kalkulasitbs = ORM::factory('kalkulasitb')
				->where('nip','=',$pegawai->nip)
				->where('YEAR("periode")','=',$periode)
				->find();
			
			$gaji_13 = $kalkulasitbs->gaji_pokok + $kalkulasitbs->tunjangan_istri + $kalkulasitbs->tunjangan_anak + $kalkulasitbs->tunjangan_umum + $kalkulasitbs->tunjangan_fungsional + $kalkulasitbs->tunjangan_struktural + $kalkulasitbs->pembulatan;	
				
			// 14
			$kalkulasiebs = ORM::factory('kalkulasieb')
				->where('nip','=',$pegawai->nip)
				->where('YEAR("periode")','=',$periode)
				->find();
			
			$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->tunjangan_istri + $kalkulasiebs->tunjangan_anak + $kalkulasiebs->tunjangan_umum + $kalkulasiebs->tunjangan_fungsional + $kalkulasiebs->tunjangan_struktural + $kalkulasiebs->pembulatan;
			//$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->pembulatan;
			
			// 14
			//$tpptbs = ORM::factory('tpptb')
			//	->where('nip','=',$pegawai->nip)
			//	->where('YEAR("periode")','=',$periode)
			//	->find();
			
			//$tpp_13 = $tpptbs->insentip;
			
			//dafiz tpp_13
			$sql_tpp_13 = 
			"SELECT SUM(tpp) AS tpp_13
			FROM tpptbs WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$tpp_13 = DB::query(Database::SELECT, $sql_tpp_13)->execute()->get('tpp_13', 0);
			//end dafiz tpp_13
			
			//dafiz tpp_rapel
			$sql_tpp_rapel = 
			"SELECT SUM(tpp3) AS tpp_rapel
			FROM tppsrsrapels WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			//$sql_tpp_rapel = 
			//"SELECT SUM(tpp_4) AS tpp_rapel
			//FROM tpps_rapels_specials WHERE nip = ".$pegawai->nip;
			
			$tpp_rapel = DB::query(Database::SELECT, $sql_tpp_rapel)->execute()->get('tpp_rapel', 0);
			//end dafiz tpp_rapel
			
			// PPH TPP JAN SD NOV
			$sql_pph_tpp_tahun = 
			"SELECT SUM(pph) AS pph_tpp_tahun
			FROM tpps WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_tpp_tahun = DB::query(Database::SELECT, $sql_pph_tpp_tahun)->execute()->get('pph_tpp_tahun', 0);

			$sql_pph_tpp_13 = 
			"SELECT SUM(pph) AS pph_tpp_13
			FROM tpptbs WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_tpp_13 = DB::query(Database::SELECT, $sql_pph_tpp_13)->execute()->get('pph_tpp_13', 0);
			
			// PPH GAJI JAN SD NOV
			$sql_pph_tahun = 
			"SELECT SUM(tunjangan_pph) AS pph_tahun
			FROM kalkulasis WHERE nip = '".$pegawai->nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
			
			$pph_tahun = DB::query(Database::SELECT, $sql_pph_tahun)->execute()->get('pph_tahun', 0);

			$sql_pph_13_tahun = 
			"SELECT SUM(tunjangan_pph) AS pph_13_tahun
			FROM kalkulasitbs WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_13_tahun = DB::query(Database::SELECT, $sql_pph_13_tahun)->execute()->get('pph_13_tahun', 0);
			
			$sql_pph_14_tahun = 
			"SELECT SUM(tunjangan_pph) AS pph_14_tahun
			FROM kalkulasiebs WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_14_tahun = DB::query(Database::SELECT, $sql_pph_14_tahun)->execute()->get('pph_14_tahun', 0);
			
			//dafiz pph_tpp_rapel
			
			$sql_pph_tpp_rapel = 
			"SELECT SUM(pph3) AS pph_tpp_rapel
			FROM tppsrsrapels WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			//$sql_pph_tpp_rapel = 
			//"SELECT SUM(pph_4) AS pph_tpp_rapel
			//FROM tpps_rapels_specials WHERE nip = ".$pegawai->nip;
			
			$pph_tpp_rapel = DB::query(Database::SELECT, $sql_pph_tpp_rapel)->execute()->get('pph_tpp_rapel', 0);
			
			//end dafiz pph_tpp_rapel
			
			$sql_pph_konker_tahun = 
			"SELECT SUM(pph_konker) AS pph_konker_tahun
			FROM tpps WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_konker_tahun = DB::query(Database::SELECT, $sql_pph_konker_tahun)->execute()->get('pph_konker_tahun', 0);
			
			//$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun + $pph_tpp_tahun + $pph_konker_tahun;
			//$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun + $pph_tpp_tahun + $pph_tpp_13;
			$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun + $pph_tpp_tahun + $pph_tpp_13 + $pph_tpp_rapel;
			
			//echo $jumlah_pph."#";
			
			$sql_pph_gaji_desember = 
			"SELECT tunjangan_pph AS pph_gaji_desember
			FROM kalkulasis WHERE nip = '".$pegawai->nip."' AND MONTH(periode) = 12 AND YEAR(periode) = ".$tahun;
			
			$pph_gaji_desember = DB::query(Database::SELECT, $sql_pph_gaji_desember)->execute()->get('pph_gaji_desember', 0);
			
			$sql_pph_tpp_desember = 
			"SELECT pph AS pph_tpp_desember
			FROM tpps WHERE nip = '".$pegawai->nip."' AND MONTH(periode) = 12 AND YEAR(periode) = ".$tahun;
			
			$pph_tpp_desember = DB::query(Database::SELECT, $sql_pph_tpp_desember)->execute()->get('pph_tpp_desember', 0);
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				
				if($masttppk->reset(FALSE)->count_all()) {
					$masttppk = $masttppk->find();	
					
					$tppkotor = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
					if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
					$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					$potongan = $eselon->finsentip - $tppkotor;
					$tunjangan_insentip = $eselon->finsentip;
					
					/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
						$tppkotor = (0.8 * $eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
					$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = (0.8 * $eselon->finsentip) - $tppkotor;
						$tunjangan_insentip = 0.8 * $eselon->finsentip;
					}*/
				}
				else {
					$tppkotor = $eselon->finsentip;
					if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
					$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					$potongan = 0;
					$tunjangan_insentip = $eselon->finsentip;
					
					/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
						$tppkotor = 0.8 * $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = 0.8 * $eselon->finsentip;
					}*/
				}					
				
				// PPH KESPEG			
				$pph_all = $this->pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel);
				$pph_kespeg = $pph_all - $pph_gaji_desember;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjkonker;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjdinsos;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjapbj;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$jakarta_kotor = $eselon->ftjjakarta;
						$max_bpjs_jakarta = $max_bpjs - $tppkotor;
						if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
						}
						$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
						$pphBlnAll_TB = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjJakarta,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolongan')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($masttppk->reset(FALSE)->count_all()) {
					$masttppk = $masttppk->find();	
					
					$tunjangan_insentip = $tgolongan->finsentip;
					
					// CEK PROFESI GURU (profesi_id = 1)
					if($insentip->profesi_id == 1) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',1)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
					if($insentip->profesi_id == 3) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',3)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
						if($insentip->profesi_id == 2) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',2)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
								$p3d = ORM::factory('tjp3d')
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $p3d->tunjangan;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $p3d->tunjangan;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}*/
							}
							elseif($insentip->penugasan_khusus == 2) {
								$penugasan = ORM::factory('tjpenugasan')->find();
								
								$tunjangan_insentip = $penugasan->tunjangan;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							}						
							else {	
								$tunjangan_insentip = $tgolongan->finsentip;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}*/
							}
						}
					}
					}
					
					// POTONGAN
					$potongan = $tunjangan_insentip - $tppkotor;
					
					// PPH KESPEG
					$pph_all = $this->pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel);
					$pph_kespeg = $pph_all - $pph_gaji_desember;
					
					// RSJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjkonker;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;										
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
					// Dinsos	
					if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjdinsos;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;										
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						

					
					// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
					if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
						if($insentip->tpp_tb_stop == 2) {
							$tjJakarta = 0;
							$bpjs_jakarta = 0;
							$askes_jakarta = 0;
							$pph_jakarta = 0;
						}
						else {
							$jakarta_kotor = $tgolongan->ftjjakarta;
							$max_bpjs_jakarta = $max_bpjs - $tppkotor;
							if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
							}
							$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
							$pphBlnAll_TB = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjJakarta,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						} 	
					}
				}
				else {
					// TIDAK ADA DI MASTTPP BERARTI 100%
					$tunjangan_insentip = $tgolongan->finsentip;
					
					// CEK PROFESI GURU (profesi_id = 1)
					if($insentip->profesi_id == 1) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',1)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = $gurus->tunjangan;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
					if($insentip->profesi_id == 3) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',3)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = $gurus->tunjangan;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
						if($insentip->profesi_id == 2) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',2)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
						}
						else {
							if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
								$p3d = ORM::factory('tjp3d')
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $p3d->tunjangan;
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $p3d->tunjangan;
								}*/
							}
							elseif($insentip->penugasan_khusus == 2) {
								$penugasan = ORM::factory('tjpenugasan')->find();
								
								$tunjangan_insentip = $penugasan->tunjangan;
							}						
							else {	
								$tunjangan_insentip = $tgolongan->finsentip;
								
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
								}*/
							}
						}
					}
					}
					
					// POTONGAN
					$potongan = 0;
					$tppkotor = $tunjangan_insentip;
					if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					
					// PPH KESPEG
					$pph_all = $this->pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel);
					$pph_kespeg = $pph_all - $pph_gaji_desember;
					
					// RSJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjkonker;	
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;									
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
					// Dinsos	
					if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjdinsos;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;										
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						

					
					// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
					if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
						if($insentip->tpp_tb_stop == 2) {
							$tjJakarta = 0;
							$bpjs_jakarta = 0;
							$askes_jakarta = 0;
							$pph_jakarta = 0;
						}
						else {						
							$jakarta_kotor = $tgolongan->ftjjakarta;
							$max_bpjs_jakarta = $max_bpjs - $tppkotor;
							if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
							}
							$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
							$pphBlnAll_TB = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjJakarta,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						}
					}						
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
			
			if($pph_kespeg < 0) {
				$pph_kespeg = 0;
			}
			
			if($pph_konker < 0) {
				$pph_konker = 0;
			}
			
			if($pph_jakarta < 0) {
				$pph_jakarta = 0;
			}
			
			//echo $pph_gaji_desember."#".$pph_all;
			//echo $pph_kespeg;
			//echo $pph_all."#".$jumlah_pph."#".$pph_gaji_desember;
			
			//echo $pph_all;
			//echo $pph_tpp_tahun."#".$pph_tpp_13."#".$pph_tahun."#".$pph_13_tahun."#".$pph_14_tahun."#".$jumlah_pph."#".$pph_kespeg;
			//echo $pphBlnAll_KK."#".$pph_all;
			//echo $pph_konker."#".$pph_jakarta;
			//die();
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,
				$askes_jakarta,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tpps (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tppp3k_desember($arrLokasi) {							
		$this->auto_render = false;
		
		$i = 1;
		$periode = "2020-12-01";
		$xperiode = explode("-",$periode);
		$tahun = $xperiode[0];
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Dinsos
		$arrDinsos = array(21);
		$arrKodeDinsos = array();
		$lokasi_dinsos = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrDinsos)
			->find_all();
			
		foreach($lokasi_dinsos as $dinsos) {
			array_push($arrKodeDinsos,$dinsos->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentipp3k')
			->where('lokasi_gaji','IN',$arrLokasi)
			//->where('nip','=','196806141990011001')
			//->where('nip','=','196211161990101001')
			->where('tpp_stop','=',1)
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasip3k')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasip3k')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawaip3k')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$bpjs_gaji = $kalkulasi->jumlah_penghasilan + $kalkulasi->tunjangan_umum + $kalkulasi->tunjangan_struktural + $kalkulasi->tunjangan_fungsional;
			$max_bpjs = 12000000 - $bpjs_gaji;
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
			
			$tjKespeg2 = 0;
			$potongan2 = 0;
			$tunjangan_insentip2 = 0;
			$tjKonker2 = 0;
			$tjJakarta2 = 0;
			$pph_konker2 = 0;
			$pph_jakarta2 = 0;
			$bpjs_konker2 = 0;	
			$bpjs_jakarta2 = 0;	
			$askes_konker2 = 0;	
			$askes_jakarta2 = 0;
			$konker_kotor2 = 0;
			$jakarta_kotor2 = 0;
			$potongan_tpp_bpjs2 = 0;
			$pph_kespeg2 = 0;
			$askes_tpp2 = 0;
			
			$tjKespeg3 = 0;
			$potongan3 = 0;
			$tunjangan_insentip3 = 0;
			$tjKonker3 = 0;
			$tjJakarta3 = 0;
			$pph_konker3 = 0;
			$pph_jakarta3 = 0;
			$bpjs_konker3 = 0;	
			$bpjs_jakarta3 = 0;	
			$askes_konker3 = 0;	
			$askes_jakarta3 = 0;
			$konker_kotor3 = 0;
			$jakarta_kotor3 = 0;
			$potongan_tpp_bpjs3 = 0;
			$pph_kespeg3 = 0;
			$askes_tpp3 = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			// 13
			$kalkulasitbs = ORM::factory('kalkulasitbp3k')
				->where('nip','=',$pegawai->nip)
				->where('YEAR("periode")','=',$periode)
				->find();
			
			$gaji_13 = $kalkulasitbs->gaji_pokok + $kalkulasitbs->tunjangan_istri + $kalkulasitbs->tunjangan_anak + $kalkulasitbs->tunjangan_umum + $kalkulasitbs->tunjangan_fungsional + $kalkulasitbs->tunjangan_struktural + $kalkulasitbs->pembulatan;	
				
			// 14
			$kalkulasiebs = ORM::factory('kalkulasiebp3k')
				->where('nip','=',$pegawai->nip)
				->where('YEAR("periode")','=',$periode)
				->find();
			
			$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->tunjangan_istri + $kalkulasiebs->tunjangan_anak + $kalkulasiebs->tunjangan_umum + $kalkulasiebs->tunjangan_fungsional + $kalkulasiebs->tunjangan_struktural + $kalkulasiebs->pembulatan;
			//$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->pembulatan;
			
			// 14
			//$tpptbs = ORM::factory('tpptb')
			//	->where('nip','=',$pegawai->nip)
			//	->where('YEAR("periode")','=',$periode)
			//	->find();
			
			//$tpp_13 = $tpptbs->insentip;
			
			//dafiz tpp_13
			$sql_tpp_13 = 
			"SELECT SUM(tpp) AS tpp_13
			FROM tpptbp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$tpp_13 = DB::query(Database::SELECT, $sql_tpp_13)->execute()->get('tpp_13', 0);
			//end dafiz tpp_13
			
			//dafiz tpp_rapel
			$sql_tpp_rapel = 
			"SELECT SUM(tpp3) AS tpp_rapel
			FROM tppsrsrapels WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			//$sql_tpp_rapel = 
			//"SELECT SUM(tpp_4) AS tpp_rapel
			//FROM tpps_rapels_specials WHERE nip = ".$pegawai->nip;
			
			$tpp_rapel = DB::query(Database::SELECT, $sql_tpp_rapel)->execute()->get('tpp_rapel', 0);
			//end dafiz tpp_rapel
			
			// PPH TPP JAN SD NOV
			$sql_pph_tpp_tahun = 
			"SELECT SUM(pph) AS pph_tpp_tahun
			FROM tppp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_tpp_tahun = DB::query(Database::SELECT, $sql_pph_tpp_tahun)->execute()->get('pph_tpp_tahun', 0);

			$sql_pph_tpp_13 = 
			"SELECT SUM(pph) AS pph_tpp_13
			FROM tpptbp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_tpp_13 = DB::query(Database::SELECT, $sql_pph_tpp_13)->execute()->get('pph_tpp_13', 0);
			
			// PPH GAJI JAN SD NOV
			$sql_pph_tahun = 
			"SELECT SUM(tunjangan_pph) AS pph_tahun
			FROM kalkulasip3ks WHERE nip = '".$pegawai->nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
			
			$pph_tahun = DB::query(Database::SELECT, $sql_pph_tahun)->execute()->get('pph_tahun', 0);

			$sql_pph_13_tahun = 
			"SELECT SUM(tunjangan_pph) AS pph_13_tahun
			FROM kalkulasitbp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_13_tahun = DB::query(Database::SELECT, $sql_pph_13_tahun)->execute()->get('pph_13_tahun', 0);
			
			$sql_pph_14_tahun = 
			"SELECT SUM(tunjangan_pph) AS pph_14_tahun
			FROM kalkulasiebp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_14_tahun = DB::query(Database::SELECT, $sql_pph_14_tahun)->execute()->get('pph_14_tahun', 0);
			
			//dafiz pph_tpp_rapel
			
			$sql_pph_tpp_rapel = 
			"SELECT SUM(pph3) AS pph_tpp_rapel
			FROM tppsrsrapels WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			//$sql_pph_tpp_rapel = 
			//"SELECT SUM(pph_4) AS pph_tpp_rapel
			//FROM tpps_rapels_specials WHERE nip = ".$pegawai->nip;
			
			$pph_tpp_rapel = DB::query(Database::SELECT, $sql_pph_tpp_rapel)->execute()->get('pph_tpp_rapel', 0);
			
			//end dafiz pph_tpp_rapel
			
			$sql_pph_konker_tahun = 
			"SELECT SUM(pph_konker) AS pph_konker_tahun
			FROM tppp3ks WHERE nip = '".$pegawai->nip."' AND YEAR(periode) = ".$tahun;
			
			$pph_konker_tahun = DB::query(Database::SELECT, $sql_pph_konker_tahun)->execute()->get('pph_konker_tahun', 0);
			
			//$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun + $pph_tpp_tahun + $pph_konker_tahun;
			//$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun + $pph_tpp_tahun + $pph_tpp_13;
			$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun + $pph_tpp_tahun + $pph_tpp_13 + $pph_tpp_rapel;
			
			//echo $jumlah_pph."#";
			
			$sql_pph_gaji_desember = 
			"SELECT tunjangan_pph AS pph_gaji_desember
			FROM kalkulasip3ks WHERE nip = '".$pegawai->nip."' AND MONTH(periode) = 12 AND YEAR(periode) = ".$tahun;
			
			$pph_gaji_desember = DB::query(Database::SELECT, $sql_pph_gaji_desember)->execute()->get('pph_gaji_desember', 0);
			
			$sql_pph_tpp_desember = 
			"SELECT pph AS pph_tpp_desember
			FROM tppp3ks WHERE nip = '".$pegawai->nip."' AND MONTH(periode) = 12 AND YEAR(periode) = ".$tahun;
			
			$pph_tpp_desember = DB::query(Database::SELECT, $sql_pph_tpp_desember)->execute()->get('pph_tpp_desember', 0);
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				
				if($masttppk->reset(FALSE)->count_all()) {
					$masttppk = $masttppk->find();	
					
					$tppkotor = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
					if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
					$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					$potongan = $eselon->finsentip - $tppkotor;
					$tunjangan_insentip = $eselon->finsentip;
					
					/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
						$tppkotor = (0.8 * $eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
					$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = (0.8 * $eselon->finsentip) - $tppkotor;
						$tunjangan_insentip = 0.8 * $eselon->finsentip;
					}*/
				}
				else {
					$tppkotor = $eselon->finsentip;
					if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
					$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					$potongan = 0;
					$tunjangan_insentip = $eselon->finsentip;
					
					/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
						$tppkotor = 0.8 * $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = 0.8 * $eselon->finsentip;
					}*/
				}					
				
				// PPH KESPEG			
				$pph_all = $this->pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel);
				$pph_kespeg = $pph_all - $pph_gaji_desember;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjkonker;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// Dinsos	
				if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjdinsos;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjapbj;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$jakarta_kotor = $eselon->ftjjakarta;
						$max_bpjs_jakarta = $max_bpjs - $tppkotor;
						if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
						}
						$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
						$pphBlnAll_TB = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjJakarta,$pph_konker_tahun,$tpp_13,$tpp_rapel);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolonganp3k')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($masttppk->reset(FALSE)->count_all()) {
					$masttppk = $masttppk->find();	
					
					$tunjangan_insentip = $tgolongan->finsentip;
					
					// CEK PROFESI GURU (profesi_id = 1)
					if($insentip->profesi_id == 1) {
						$gurus = ORM::factory('tjgurup3k')
							->where('profesi_id','=',1)
							->where('prefix_golongan','=',substr($insentip->golongan_id))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
					if($insentip->profesi_id == 3) {
						$gurus = ORM::factory('tjgurup3k')
							->where('profesi_id','=',3)
							->where('prefix_golongan','=',substr($insentip->golongan_id))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
						if($insentip->profesi_id == 2) {
							$gurus = ORM::factory('tjgurup3k')
								->where('profesi_id','=',2)
								->where('prefix_golongan','=',substr($insentip->golongan_id))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
								$p3d = ORM::factory('tjp3d')
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $p3d->tunjangan;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $p3d->tunjangan;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}*/
							}
							elseif($insentip->penugasan_khusus == 2) {
								$penugasan = ORM::factory('tjpenugasan')->find();
								
								$tunjangan_insentip = $penugasan->tunjangan;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							}						
							else {	
								$tunjangan_insentip = $tgolongan->finsentip;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}*/
							}
						}
					}
					}
					
					// POTONGAN
					$potongan = $tunjangan_insentip - $tppkotor;
					
					// PPH KESPEG
					$pph_all = $this->pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel);
					$pph_kespeg = $pph_all - $pph_gaji_desember;
					
					// RSJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjkonker;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;										
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
					// Dinsos	
					if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjdinsos;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;										
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						

					
					// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
					if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
						if($insentip->tpp_tb_stop == 2) {
							$tjJakarta = 0;
							$bpjs_jakarta = 0;
							$askes_jakarta = 0;
							$pph_jakarta = 0;
						}
						else {
							$jakarta_kotor = $tgolongan->ftjjakarta;
							$max_bpjs_jakarta = $max_bpjs - $tppkotor;
							if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
							}
							$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
							$pphBlnAll_TB = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjJakarta,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						} 	
					}
				}
				else {
					// TIDAK ADA DI MASTTPP BERARTI 100%
					$tunjangan_insentip = $tgolongan->finsentip;
					
					// CEK PROFESI GURU (profesi_id = 1)
					if($insentip->profesi_id == 1) {
						$gurus = ORM::factory('tjgurup3k')
							->where('profesi_id','=',1)
							->where('prefix_golongan','=',substr($insentip->golongan_id))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = $gurus->tunjangan;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
					if($insentip->profesi_id == 3) {
						$gurus = ORM::factory('tjgurup3k')
							->where('profesi_id','=',3)
							->where('prefix_golongan','=',substr($insentip->golongan_id))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tppkotor = $gurus->tunjangan;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					}
					else {
						if($insentip->profesi_id == 2) {
							$gurus = ORM::factory('tjgurup3k')
								->where('profesi_id','=',2)
								->where('prefix_golongan','=',substr($insentip->golongan_id))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
						}
						else {
							if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
								$p3d = ORM::factory('tjp3d')
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $p3d->tunjangan;
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $p3d->tunjangan;
								}*/
							}
							elseif($insentip->penugasan_khusus == 2) {
								$penugasan = ORM::factory('tjpenugasan')->find();
								
								$tunjangan_insentip = $penugasan->tunjangan;
							}						
							else {	
								$tunjangan_insentip = $tgolongan->finsentip;
								
								/*if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $tgolongan->finsentip;
								}*/
							}
						}
					}
					}
					
					// POTONGAN
					$potongan = 0;
					$tppkotor = $tunjangan_insentip;
					if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
					
					// PPH KESPEG
					$pph_all = $this->pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel);
					$pph_kespeg = $pph_all - $pph_gaji_desember;
					
					// RSJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjkonker;	
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;									
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
					// Dinsos	
					if(in_array($pegawai->lokasi_gaji,$arrKodeDinsos)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$bpjs_konker = 0;
							$askes_konker = 0;
							$pph_konker = 0;
						}
						else {
							$konker_kotor = $tgolongan->ftjdinsos;
							$max_bpjs_konker = $max_bpjs - $tppkotor;
							if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
							}
							$tjKonker = $konker_kotor - $bpjs_konker;										
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjKonker,$pph_konker_tahun,$tpp_13,$tpp_rapel);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						

					
					// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
					if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
						if($insentip->tpp_tb_stop == 2) {
							$tjJakarta = 0;
							$bpjs_jakarta = 0;
							$askes_jakarta = 0;
							$pph_jakarta = 0;
						}
						else {						
							$jakarta_kotor = $tgolongan->ftjjakarta;
							$max_bpjs_jakarta = $max_bpjs - $tppkotor;
							if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
							}
							$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
							$pphBlnAll_TB = $this->pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tjKespeg,$tahun,$jumlah_pph,$tjJakarta,$pph_konker_tahun,$tpp_13,$tpp_rapel);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						}
					}						
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
			
			if($pph_kespeg < 0) {
				$pph_kespeg = 0;
			}
			
			if($pph_konker < 0) {
				$pph_konker = 0;
			}
			
			if($pph_jakarta < 0) {
				$pph_jakarta = 0;
			}
			$tjKespeg = $tjKespeg - $pph_kespeg;
			$tjKonker = $tjKonker - $pph_konker;
			$tjJakarta = $tjJakarta - $pph_jakarta;
			
			
			//echo $pph_gaji_desember."#".$pph_all;
			//echo $pph_kespeg;
			//echo $pph_all."#".$jumlah_pph."#".$pph_gaji_desember;
			
			//echo $pph_all;
			//echo $pph_tpp_tahun."#".$pph_tpp_13."#".$pph_tahun."#".$pph_13_tahun."#".$pph_14_tahun."#".$jumlah_pph."#".$pph_kespeg;
			//echo $pphBlnAll_KK."#".$pph_all;
			//echo $pph_konker."#".$pph_jakarta;
			//die();
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,
				$askes_jakarta,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tppp3ks (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function pph_kespeg_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tpp_desember,$tahun,$jumlah_pph,$tpp_13,$tpp_rapel){
		$sql_gaji_kotor = 
		"SELECT SUM(gaji_pokok + tunjangan_istri + tunjangan_anak + tunjangan_struktural + tunjangan_beras + tunjangan_fungsional + tunjangan_umum + pembulatan) AS gaji_kotor
		FROM kalkulasis WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
		
		/*$sql_gaji_kotor = 
		"SELECT SUM(k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak + k.tunjangan_struktural + k.tunjangan_beras + k.tunjangan_fungsional 
+ k.tunjangan_umum + k.pembulatan) + f.jumlah_penghasilan_4 AS gaji_kotor
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND YEAR(k.periode) = ".$tahun;*/

		$gaji_kotor = DB::query(Database::SELECT, $sql_gaji_kotor)->execute()->get('gaji_kotor', 0);
		
		$sql_tpp_jan_nov = 
		"SELECT SUM(tpp) AS tpp_jan_nov
		FROM tpps WHERE nip = '".$nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
		
		$tpp_jan_nov = DB::query(Database::SELECT, $sql_tpp_jan_nov)->execute()->get('tpp_jan_nov', 0);
		
		$bruto_12 = $gaji_kotor + $gaji_13 + $gaji_14 + $tpp_jan_nov + $tpp_desember + $tpp_13 + $tpp_rapel;
		
		$sql_pen_tht = 
		"SELECT SUM(0.0475 * (gaji_pokok + tunjangan_istri + tunjangan_anak)) AS pen_tht_12
		FROM kalkulasis WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
		
		/*$sql_pen_tht = 
		"SELECT SUM(0.0475 * (k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak))+ (0.0475 * (f.gaji_pokok_4 + f.tunjangan_istri_4 + f.tunjangan_anak_4)) AS pen_tht_12
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND YEAR(k.periode) = ".$tahun;*/
		
		$pen_tht_12 = DB::query(Database::SELECT, $sql_pen_tht)->execute()->get('pen_tht_12', 0);
		
		if($pen_tht_12 > 2400000){
			$pen_tht_12 = 2400000;
		}
		
		$bea_jabatan_12 = ceil(0.05 * $bruto_12);
		if ($bea_jabatan_12 > 6000000) {
			$bea_jabatan_12 = 6000000;
		}
		
		$kurang_gaji_12 = $bea_jabatan_12 + $pen_tht_12;
		$penghasilan_tahun	= $bruto_12 - $kurang_gaji_12;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}
		
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		//$pph_bulan	 = floor($pph_tahun/12) ;
		
		//dafiz
		/*$sql_pph_rapel = 
		"SELECT tunjangan_pph_4 AS pph_rapel
FROM kalkulasis_rapels_finals WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
				
		$pph_rapel = DB::query(Database::SELECT, $sql_pph_rapel)->execute()->get('pph_rapel', 0);*/
		//dafiz end
		
		$pph_bulan = $pph_tahun - $jumlah_pph;
		if($pph_bulan <= 0) {
			$pph_bulan = 0;
		}
	
		return $pph_bulan;
		//return $bruto_12."#".$penghasilan_tahun."#".$penghasilan_kpj."#".$pph_tahun."#".$jumlah_pph;
		//return $pph_tahun."#".$ptkp."#".$penghasilan_kpj."#".$penghasilan_tahun."#".$bea_jabatan_12."#".$pen_tht_12."#".$gaji_13."#".$gaji_14."#".$tpp_desember."#".$jumlah_pph."#".$bruto_12."#".$tpp_jan_nov;
	}
	
	function pph_kespeg_konker_desember($istri,$anak,$nip,$gaji_13,$gaji_14,$tpp_desember,$tahun,$jumlah_pph,$konker_jakarta,$pph_tahun_konker,$tpp_13,$tpp_rapel){
		$sql_gaji_kotor = 
		"SELECT SUM(gaji_pokok + tunjangan_istri + tunjangan_anak + tunjangan_struktural + tunjangan_beras + tunjangan_fungsional + tunjangan_umum + pembulatan) AS gaji_kotor
		FROM kalkulasis WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
		
		/*$sql_gaji_kotor = 
		"SELECT SUM(k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak + k.tunjangan_struktural + k.tunjangan_beras + k.tunjangan_fungsional 
+ k.tunjangan_umum + k.pembulatan) + f.jumlah_penghasilan_4 AS gaji_kotor
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND YEAR(k.periode) = ".$tahun;*/
		
		$gaji_kotor = DB::query(Database::SELECT, $sql_gaji_kotor)->execute()->get('gaji_kotor', 0);
		
		$bruto_12_gaji = $gaji_kotor + $gaji_13 + $gaji_14;
		
		$sql_tpp_jan_nov = 
		"SELECT SUM(tpp) AS tpp_jan_nov
		FROM tpps WHERE nip = '".$nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
		
		$tpp_jan_nov = DB::query(Database::SELECT, $sql_tpp_jan_nov)->execute()->get('tpp_jan_nov', 0);
		
		$sql_konker_jan_nov = 
		"SELECT SUM(konker) AS konker_jan_nov
		FROM tpps WHERE nip = '".$nip."' AND MONTH(periode) < 12 AND YEAR(periode) = ".$tahun;
		
		$konker_jan_nov = DB::query(Database::SELECT, $sql_konker_jan_nov)->execute()->get('konker_jan_nov', 0);
		
		$bruto_12 = $gaji_kotor + $gaji_13 + $gaji_14 + $tpp_jan_nov + $tpp_desember + $konker_jan_nov + $konker_jakarta + $tpp_13 + $tpp_rapel;
		
		
		$sql_pen_tht = 
		"SELECT SUM(0.0475 * (gaji_pokok + tunjangan_istri + tunjangan_anak)) AS pen_tht_12
		FROM kalkulasis WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
		
		/*$sql_pen_tht = 
		"SELECT SUM(0.0475 * (k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak))+ (0.0475 * (f.gaji_pokok_4 + f.tunjangan_istri_4 + f.tunjangan_anak_4)) AS pen_tht_12
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND YEAR(k.periode) = ".$tahun;*/

		$pen_tht_12 = DB::query(Database::SELECT, $sql_pen_tht)->execute()->get('pen_tht_12', 0);
		
		if($pen_tht_12 > 2400000){
			$pen_tht_12 = 2400000;
		}
		
		$bea_jabatan_12 = ceil(0.05 * $bruto_12);
		if ($bea_jabatan_12 > 6000000) {
			$bea_jabatan_12 = 6000000;
		}
		
		$kurang_gaji_12 = $bea_jabatan_12 + $pen_tht_12;
		$penghasilan_tahun	= $bruto_12 - $kurang_gaji_12;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}
		
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		//$pph_bulan	 = floor($pph_tahun/12) ;
		
		//dafiz
		/*$sql_pph_rapel = 
		"SELECT tunjangan_pph_4 AS pph_rapel
FROM kalkulasis_rapels_finals WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
				
		$pph_rapel = DB::query(Database::SELECT, $sql_pph_rapel)->execute()->get('pph_rapel', 0);*/
		//dafiz end
		
		$pph_bulan = $pph_tahun - ($jumlah_pph + $pph_tahun_konker);
		if($pph_bulan <= 0) {
			$pph_bulan = 0;
		}
	
		return $pph_bulan;
		//return $bruto_12."#".$gaji_13."#".$gaji_14."#".$tpp_jan_nov."#".$konker_jan_nov."#".$konker_jakarta;
		//return $pph_tahun."#".$ptkp."#".$penghasilan_kpj."#".$penghasilan_tahun."#".$bea_jabatan_12."#".$pen_tht_12;
		//return $jumlah_pph + $pph_tahun_konker;
	}

	function tpp_rs_rapel($periode, $arrLokasi) {
							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentip')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$bpjs_gaji = $kalkulasi->jumlah_penghasilan + $kalkulasi->tunjangan_umum + $kalkulasi->tunjangan_struktural + $kalkulasi->tunjangan_fungsional;
			$max_bpjs = 12000000 - $bpjs_gaji;
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
			$bpjs_konker = 0;	
			$bpjs_jakarta = 0;	
			$askes_konker = 0;	
			$askes_jakarta = 0;
			$konker_kotor = 0;
			$jakarta_kotor = 0;
			
			$tjKespeg2 = 0;
			$potongan2 = 0;
			$tunjangan_insentip2 = 0;
			$tjKonker2 = 0;
			$tjJakarta2 = 0;
			$pph_konker2 = 0;
			$pph_jakarta2 = 0;
			$bpjs_konker2 = 0;	
			$bpjs_jakarta2 = 0;	
			$askes_konker2 = 0;	
			$askes_jakarta2 = 0;
			$konker_kotor2 = 0;
			$jakarta_kotor2 = 0;
			$potongan_tpp_bpjs2 = 0;
			$pph_kespeg2 = 0;
			$askes_tpp2 = 0;
			
			$tjKespeg3 = 0;
			$potongan3 = 0;
			$tunjangan_insentip3 = 0;
			$tjKonker3 = 0;
			$tjJakarta3 = 0;
			$pph_konker3 = 0;
			$pph_jakarta3 = 0;
			$bpjs_konker3 = 0;	
			$bpjs_jakarta3 = 0;	
			$askes_konker3 = 0;	
			$askes_jakarta3 = 0;
			$konker_kotor3 = 0;
			$jakarta_kotor3 = 0;
			$potongan_tpp_bpjs3 = 0;
			$pph_kespeg3 = 0;
			$askes_tpp3 = 0;
								
			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			if($insentip->eselon_id > 3) {
				// Eselon
				$eselon = ORM::factory('eselon')
					->where('id','=',$insentip->eselon_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
				
				if($periode <= "2015-11-01") {							
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						$tppkotor = ($masttpp->prosentase / 100) * $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = $eselon->finsentip - $tppkotor;
						$tunjangan_insentip = $eselon->finsentip;
					}
					else {
						$tppkotor = $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
					}
				}
				else {
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tppkotor = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = $eselon->finsentip - $tppkotor;
						$tunjangan_insentip = $eselon->finsentip;
			
					}
					else {
						$tppkotor = $eselon->finsentip;
						if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
						}
						$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						$potongan = 0;
						$tunjangan_insentip = $eselon->finsentip;
				
					}
				}					
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				$pph_kespeg = $pph_all - $tunjangan_pph;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjkonker;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
							$bpjs_konker = 0;
							$askes_konker = 0;
						}
						else if($konker_kotor <= $max_bpjs_konker) {
							$bpjs_konker = 0.01 * $konker_kotor ;
							$askes_konker = 0.04 * $konker_kotor ;
						}
						else if ($konker_kotor > $max_bpjs_konker){
							$bpjs_konker = 0.01 * $max_bpjs_konker ;
							$askes_konker = 0.04 * $max_bpjs_konker ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$bpjs_konker = 0;
						$askes_konker = 0;
						$pph_konker = 0;
					}
					else {
						$konker_kotor = $eselon->ftjapbj;	
						$max_bpjs_konker = $max_bpjs - $tppkotor;
						if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
						}
						$tjKonker = $konker_kotor - $bpjs_konker;									
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_tb_stop == 2) {
						$tjJakarta = 0;
						$bpjs_jakarta = 0;
						$askes_jakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$jakarta_kotor = $eselon->ftjjakarta;
						$max_bpjs_jakarta = $max_bpjs - $tppkotor;
						if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
						}
						$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
						$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			else {
				// Non Eselon
				$tgolongan = ORM::factory('tgolongan')
					->where('fkode','=',$insentip->golongan_id)
					->find();
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($periode <= "2015-11-01") {
					if($masttpp->reset(FALSE)->count_all()) {
						$masttpp = $masttpp->find();	
						
						if($pegawai->sertifikasi_guru == 2) {
							$tppkotor = ($masttpp->prosentase / 100) * ($tgolongan->finsentip - $pegawai->gaji_pokok);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {	
							$tppkotor = ($masttpp->prosentase / 100) * $tgolongan->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						
						$potongan = $tgolongan->finsentip - $tppkotor;							
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;	
																	
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						} 	
					}
					else {
						$tunjangan_insentip = $tgolongan->finsentip;
						
						if($pegawai->sertifikasi_guru == 2) {
							$tppkotor = $tgolongan->finsentip - $pegawai->gaji_pokok;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
						}
						else {	
							$tppkotor = $tgolongan->finsentip;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							$potongan = 0;
						}
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}	
					}
				}
				else {
					if($masttppk->reset(FALSE)->count_all()) {
						$masttppk = $masttppk->find();	
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
						if($insentip->profesi_id == 3) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',3)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
							}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjguru')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $gurus->tunjangan;
								$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
					
										$tunjangan_insentip = $p3d->tunjangan;
										$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
										if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									//}
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tunjangan_insentip = $penugasan->tunjangan;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}						
								else {	
									$tunjangan_insentip = $tgolongan->finsentip;
									$tppkotor = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
										$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								}
							}
						}
						}
						
						// POTONGAN
						$potongan = $tunjangan_insentip - $tppkotor;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
				
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							} 	
						}
					}
					else {
						// TIDAK ADA DI MASTTPP BERARTI 100%
						
						$tunjangan_insentip = $tgolongan->finsentip;
						
						// CEK PROFESI GURU (profesi_id = 1)
						if($insentip->profesi_id == 1) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',1)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = $gurus->tunjangan;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
								}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
						if($insentip->profesi_id == 3) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',3)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->where('sertifikasi','=',$insentip->sertifikasi_guru)
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tppkotor = $gurus->tunjangan;
							if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
								}
							$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
						}
						else {
							if($insentip->profesi_id == 2) {
								$gurus = ORM::factory('tjguru')
									->where('profesi_id','=',2)
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tppkotor = $gurus->tunjangan;
								if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
									}
								$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
								$tunjangan_insentip = $gurus->tunjangan;
							}
							else {
								if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
									$p3d = ORM::factory('tjp3d')
										->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
										->find();
									
									$tppkotor = $p3d->tunjangan;
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
										}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $p3d->tunjangan;
									
								}
								elseif($insentip->penugasan_khusus == 2) {
									$penugasan = ORM::factory('tjpenugasan')->find();
									
									$tppkotor = $penugasan->tunjangan;
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $penugasan->tunjangan;
								}						
								else {	
									$tppkotor = $tgolongan->finsentip;
									if($tppkotor <= $max_bpjs) {
								$potongan_tpp_bpjs = ceil(0.01 * $tppkotor) ;
								$askes_tpp = ceil(0.04 * $tppkotor) ;
							}
							else {
								$potongan_tpp_bpjs = ceil(0.01 * $max_bpjs) ;
								$askes_tpp = ceil(0.04 * $max_bpjs) ;
											}
									$tjKespeg = $tppkotor - $potongan_tpp_bpjs;
									$tunjangan_insentip = $tgolongan->finsentip;
									
									
								}
							}
						}
						}
						
						// POTONGAN
						$potongan = 0;
						
						// PPH KESPEG
						$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
						$tunjangan_pph = $kalkulasi->tunjangan_pph;
						$pph_kespeg = $pph_all - $tunjangan_pph;
						
						// RSJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								$konker_kotor = $tgolongan->ftjkonker;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$bpjs_konker = 0;
								$askes_konker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}
								else {
								$konker_kotor = $tgolongan->ftjapbj;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						// pokja diluar APBJ
						else {
							if($insentip->pokja_apbj == 2 && $insentip->tpp_kk_stop == 1) {
							$konker_kotor = 2000000;	
								$max_bpjs_konker = $max_bpjs - $tppkotor;
								if ($max_bpjs_konker <= 0) {
									$bpjs_konker = 0;
									$askes_konker = 0;
								}
								else if($konker_kotor <= $max_bpjs_konker) {
									$bpjs_konker = ceil(0.01 * $konker_kotor) ;
									$askes_konker = ceil(0.04 * $konker_kotor) ;
								}
								else if ($konker_kotor >= $max_bpjs_konker){
									$bpjs_konker = ceil(0.01 * $max_bpjs_konker) ;
									$askes_konker = ceil(0.04 * $max_bpjs_konker) ;
								}
								$tjKonker = $konker_kotor - $bpjs_konker;
							}
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
						
						
						// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
						if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
							if($insentip->tpp_tb_stop == 2) {
								$tjJakarta = 0;
								$bpjs_jakarta = 0;
								$askes_jakarta = 0;
								$pph_jakarta = 0;
							}
							else {						
								$jakarta_kotor = $tgolongan->ftjjakarta;
								$max_bpjs_jakarta = $max_bpjs - $tppkotor;
								if ($max_bpjs_jakarta <= 0) {
									$bpjs_jakarta = 0;
									$askes_jakarta = 0;
								}
								else if($jakarta_kotor <= $max_bpjs_jakarta) {
									$bpjs_jakarta = ceil(0.01 * $jakarta_kotor) ;
									$askes_jakarta = ceil(0.04 * $jakarta_kotor) ;
								}
								else if ($jakarta_kotor >= $max_bpjs_jakarta){
									$bpjs_jakarta = ceil(0.01 * $max_bpjs_jakarta) ;
									$askes_jakarta = ceil(0.04 * $max_bpjs_jakarta) ;
								}
								$tjJakarta = $jakarta_kotor - $bpjs_jakarta;
								$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
								
								if($pphBlnAll_TB > 0) {
									$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
								}
							}
						}						
					}
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
			
			//TPP YANG SUDAH DITERIMAKAN

			$sql = "SELECT 
					insentip,
					tpp,
					potongan,
					potongan_tpp_bpjs,
					pph,
					konker_bruto,
					konker,
					bpjs_konker,
					pph_konker,
					jakarta_bruto,
					jakarta,
					bpjs_jakarta,
					pph_jakarta,
					askes_tpp,
					askes_konker,
					askes_jakarta
					FROM tpps		
					WHERE periode = '".$periode."' AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$tunjangan_insentip2 = $q->insentip;
				$tjKespeg2 = $q->tpp;
				$potongan2 = $q->potongan;
				$potongan_tpp_bpjs2 = $q->potongan_tpp_bpjs;
				$pph_kespeg2 = $q->pph;
				$konker_kotor2 = $q->konker_bruto;
				$tjKonker2 = $q->konker;
				$bpjs_konker2 = $q->bpjs_konker;
				$pph_konker2 = $q->pph_konker;
				$jakarta_kotor2 = $q->jakarta_bruto;
				$tjJakarta2 = $q->jakarta;
				$bpjs_jakarta2 = $q->bpjs_jakarta;
				$pph_jakarta2 = $q->pph_jakarta;
				$askes_tpp2 = $q->askes_tpp;
				$askes_konker2 = $q->askes_konker;
				$askes_jakarta2 = $q->askes_jakarta;
			}
			
			//KEKURANGAN TPP
			
			$tunjangan_insentip3 = $tunjangan_insentip - $tunjangan_insentip2;
			$tjKespeg3 = $tjKespeg - $tjKespeg2;
			$potongan3 = $potongan - $potongan2;
			$potongan_tpp_bpjs3 = $potongan_tpp_bpjs - $potongan_tpp_bpjs2;
			$pph_kespeg3 = $pph_kespeg - $pph_kespeg2;
			$konker_kotor3 = $konker_kotor - $konker_kotor2;
			$tjKonker3 = $tjKonker - $tjKonker2;
			$bpjs_konker3 = $bpjs_konker - $bpjs_konker2;
			$pph_konker3 = $pph_konker - $pph_konker2;
			$jakarta_kotor3 = $jakarta_kotor - $jakarta_kotor2;
			$tjJakarta3 = $tjJakarta - $tjJakarta2;
			$bpjs_jakarta3 = $bpjs_jakarta - $bpjs_jakarta2;
			$pph_jakarta3 = $pph_jakarta - $pph_jakarta2;
			$askes_tpp3 = $askes_tpp - $askes_tpp2;
			$askes_konker3 = $askes_konker - $askes_konker2;
			$askes_jakarta3 = $askes_jakarta - $askes_jakarta2;
							
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$potongan_tpp_bpjs,
				$pph_kespeg,
				$konker_kotor,
				$tjKonker,
				$bpjs_konker,
				$pph_konker,
				$jakarta_kotor,
				$tjJakarta,
				$bpjs_jakarta,
				$pph_jakarta,
				$eselon_id,
				$askes_tpp,
				$askes_konker,
				$askes_jakarta,
				$tunjangan_insentip2,
				$tjKespeg2,
				$potongan2,
				$potongan_tpp_bpjs2,
				$pph_kespeg2,
				$konker_kotor2,
				$tjKonker2,
				$bpjs_konker2,
				$pph_konker2,
				$jakarta_kotor2,
				$tjJakarta2,
				$bpjs_jakarta2,
				$pph_jakarta2,
				$askes_tpp2,
				$askes_konker2,
				$askes_jakarta2,
				$tunjangan_insentip3,
				$tjKespeg3,
				$potongan3,
				$potongan_tpp_bpjs3,
				$pph_kespeg3,
				$konker_kotor3,
				$tjKonker3,
				$bpjs_konker3,
				$pph_konker3,
				$jakarta_kotor3,
				$tjJakarta3,
				$bpjs_jakarta3,
				$pph_jakarta3,
				$askes_tpp3,
				$askes_konker3,
				$askes_jakarta3,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'potongan_tpp_bpjs',
			'pph',
			'konker_bruto',
			'konker',
			'bpjs_konker',
			'pph_konker',
			'jakarta_bruto',
			'jakarta',
			'bpjs_jakarta',
			'pph_jakarta',
			'eselon_id',
			'askes_tpp',
			'askes_konker',
			'askes_jakarta',
			'insentip2',
			'tpp2',
			'potongan2',
			'potongan_tpp_bpjs2',
			'pph2',
			'konker_bruto2',
			'konker2',
			'bpjs_konker2',
			'pph_konker2',
			'jakarta_bruto2',
			'jakarta2',
			'bpjs_jakarta2',
			'pph_jakarta2',
			'askes_tpp2',
			'askes_konker2',
			'askes_jakarta2',
			'insentip3',
			'tpp3',
			'potongan3',
			'potongan_tpp_bpjs3',
			'pph3',
			'konker_bruto3',
			'konker3',
			'bpjs_konker3',
			'pph_konker3',
			'jakarta_bruto3',
			'jakarta3',
			'bpjs_jakarta3',
			'pph_jakarta3',
			'askes_tpp3',
			'askes_konker3',
			'askes_jakarta3',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tppsrsrapels (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
	
	function tpp_kelas_jabatan($periode, $arrLokasi) {							
		$this->auto_render = false;
		
		$i = 1;
		$xperiode = explode("-",$periode);
		$value = "";
		
		// RSJ
		$arrRSJ = array(50,67,68,69);
		$arrKodeRSJ = array();
		$lokasi_rsj = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrRSJ)
			->find_all();
			
		foreach($lokasi_rsj as $rsj) {
			array_push($arrKodeRSJ,$rsj->id);
		}
		
		// APBJ
		$arrAPBJ = array('02020250');
		$arrKodeAPBJ = array();
		$lokasi_APBJ = ORM::factory('lokasi')
			->where('kode','IN',$arrAPBJ)
			->find_all();
			
		foreach($lokasi_APBJ as $APBJ) {
			array_push($arrKodeAPBJ,$APBJ->id);
		}
		
		// Jakarta
		$arrJakarta = array(43);
		$arrKodeJakarta = array();
		$lokasi_jakarta = ORM::factory('lokasi')
			->where('LEFT("kode",2)','IN',$arrJakarta)
			->find_all();
			
		foreach($lokasi_jakarta as $jakarta) {
			array_push($arrKodeJakarta,$jakarta->id);
		}
		
		$lokasi_kks = ORM::factory('lokasi')
			->where('status_tb_kk','=',1)
			->find_all();
		
		$lokasi_tbs = ORM::factory('lokasi')
			->where('status_tb_kk','=',2)
			->find_all();	
		
		$arrKK = array();
		$arrTB = array();
		
		foreach($lokasi_kks as $kk) {
			array_push($arrKK,$kk->id);
		}
		
		foreach($lokasi_tbs as $tb) {
			array_push($arrTB,$tb->id);
		}
		
		$insentips = ORM::factory('insentip')
			->where('lokasi_gaji','IN',$arrLokasi)
			->where('tpp_stop','=',1)
			->where('nip','=','198102072010011014')
			->find_all();
		
		$kelompok = "";
		foreach($insentips as $insentip) {	
			$kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->find();
			
			$n_kalkulasi = ORM::factory('kalkulasi')
				->where('nip','=',$insentip->nip)
				->where('periode','=',$periode)
				->count_all();
				
			if($n_kalkulasi == 0) {
				$setting = ORM::factory('setting',1);
				if($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
					$table = "insentips";
				}
				else {
					$table = "insentip_".$xperiode[0]."_".$xperiode[1];
				}
				
				DB::update($table)
					->set(array('tpp_stop' => '2'))
					->where('nip', '=', $insentip->nip)
					->execute();
					
				continue;
			}
			
			$pegawai = ORM::factory('pegawai')
				->where('nip','=',$insentip->nip)
				->find();	
													
			$lokasi_id = $insentip->lokasi_gaji;
			$lokasi = ORM::factory('lokasi',$lokasi_id);
			
			$lokasi_kode = $lokasi->kode;
			$lokasi_string = $lokasi->name;
			$name = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			$golongan_id = $kalkulasi->golongan_id;
			$golongan_string = $kalkulasi->golongan_string;		
			
			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;	
			
			$tjKespeg = 0;
			$potongan = 0;
			$tunjangan_insentip = 0;
			$tjKonker = 0;
			$pphBlnAll_KK = 0;
			$tjJakarta = 0;
			$pphBlnAll_TB = 0;
			$pph_konker = 0;
			$pph_jakarta = 0;
								
			//$jabatan = $insentip->jabatan;
			
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			$golongan_string = $insentip->golongan->kode;
			
			// ESELON
			if($insentip->mastersotk_id > 0) {
				$eselon = ORM::factory('mastersotk')
					->where('id','=',$insentip->mastersotk_id)
					->find();
				
				$jabatan = $eselon->name;	
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));	
					
				if($masttppk->reset(FALSE)->count_all()) {
					$masttppk = $masttppk->find();	
					
					$tjKespeg = ($eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
					$potongan = $eselon->finsentip - $tjKespeg;
					$tunjangan_insentip = $eselon->nominal;
					
					if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
						$tjKespeg = (0.8 * $eselon->finsentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						$potongan = (0.8 * $eselon->finsentip) - $tjKespeg;
						$tunjangan_insentip = 0.8 * $eselon->nominal;
					}
				}
				else {
					$tjKespeg = $eselon->nominal;
					$potongan = 0;
					$tunjangan_insentip = $eselon->nominal;
					
					if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
						$tjKespeg = 0.8 * $eselon->nominal;
						$potongan = 0;
						$tunjangan_insentip = 0.8 * $eselon->nominal;
					}
				}
				
				// PPH KESPEG
				$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
				$tunjangan_pph = $kalkulasi->tunjangan_pph;
				$pph_kespeg = $pph_all - $tunjangan_pph;
								
				// RSJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjkonker;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
						
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				}
				
				// APBJ	
				if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjKonker = 0;
						$pph_konker = 0;
					}
					else {
						$tjKonker = $eselon->ftjapbj;										
						$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
						if($pphBlnAll_KK > 0) {
							$pph_konker	= $pphBlnAll_KK - $pph_all;	
						}
					}
				} 
				 
				// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
				if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
					if($insentip->tpp_kk_stop == 2) {
						$tjJakarta = 0;
						$pph_jakarta = 0;
					}
					else {
						$tjJakarta = $eselon->ftjjakarta;
						$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
						
						if($pphBlnAll_TB > 0) {
							$pph_jakarta = $pphBlnAll_TB - $pph_all;	
						}
					}
				} 					
			}
			
			// NON ESELON
			if($insentip->mastersotk_id == 0) {
				if($insentip->masterjfu_id > 0) {
					$tpp_staf = ORM::factory('masterjfu')
						->where('id','=',$insentip->masterjfu_id)
						->find();	
				}
				elseif($insentip->masterjft_id > 0) {
					$tpp_staf = ORM::factory('masterjft')
						->where('id','=',$insentip->masterjft_id)
						->find();
				}
				
				$jabatan = $tpp_staf->name;
				
				$masttpp = ORM::factory('masttpp')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));					
				
				$masttppk = ORM::factory('masttppk')
					->where('nip','=',$pegawai->nip)
					->where('bulan','=',intval($xperiode[1]))
					->where('tahun','=',intval($xperiode[0]));
				
				if($masttppk->reset(FALSE)->count_all()) {
					$masttppk = $masttppk->find();	
					
					$tunjangan_insentip = $tpp_staf->nominal;
					
					// CEK PROFESI GURU (profesi_id = 1)
					if($insentip->profesi_id == 1) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',1)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
					}
					else {
					if($insentip->profesi_id == 3) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',3)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
					}
					else {
						if($insentip->profesi_id == 2) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',2)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
							$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
						}
						else {
							if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
								$p3d = ORM::factory('tjp3d')
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $p3d->tunjangan;
								$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								
								if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $p3d->tunjangan;
									$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								}
							}
							elseif($insentip->penugasan_khusus == 2) {
								$penugasan = ORM::factory('tjpenugasan')->find();
								
								$tunjangan_insentip = $penugasan->tunjangan;
								$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
							}						
							else {	
								$tunjangan_insentip = $tpp_staf->nominal;
								$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								
								if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $tpp_staf->nominal;
									$tjKespeg = ($tunjangan_insentip * ((($masttppk->skp / 100)*(60/100))+((40/100)-($masttppk->perilaku/100))))*($masttppk->hukdis / 100);
								}
							}
						}
					}
					}
					
					// POTONGAN
					$potongan = $tunjangan_insentip - $tjKespeg;
					
					// PPH KESPEG
					$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
					$tunjangan_pph = $kalkulasi->tunjangan_pph;
					$pph_kespeg = $pph_all - $tunjangan_pph;
					
					// RSJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$pph_konker = 0;
						}
						else {
							$tjKonker = $tgolongan->ftjkonker;										
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
					// APBJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$pph_konker = 0;
						}
						else {
							if($insentip->pokja_apbj == 2) {
							$tjKonker = 2000000;
							}
							else {
							$tjKonker = $tgolongan->ftjapbj;
								}										
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					

					
					// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
					if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjJakarta = 0;
							$pph_jakarta = 0;
						}
						else {
							$tjJakarta = $tgolongan->ftjjakarta;
							$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						} 	
					}
				}
				else {
					// TIDAK ADA DI MASTTPP BERARTI 100%
					
					$tunjangan_insentip = $tpp_staf->nominal;
					
					// CEK PROFESI GURU (profesi_id = 1)
					if($insentip->profesi_id == 1) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',1)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tjKespeg = $gurus->tunjangan;
					}
					else {
					if($insentip->profesi_id == 3) {
						$gurus = ORM::factory('tjguru')
							->where('profesi_id','=',3)
							->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
							->where('sertifikasi','=',$insentip->sertifikasi_guru)
							->find();
						
						$tunjangan_insentip = $gurus->tunjangan;
						$tjKespeg = $gurus->tunjangan;
					}
					else {
						if($insentip->profesi_id == 2) {
							$gurus = ORM::factory('tjguru')
								->where('profesi_id','=',2)
								->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
								->find();
							
							$tunjangan_insentip = $gurus->tunjangan;
						}
						else {
							if($insentip->p3d == 2 || $insentip->pindahan == 2 || $insentip->profesi_id == "") {
								$p3d = ORM::factory('tjp3d')
									->where('prefix_golongan','=',substr($insentip->golongan_id,0,1))
									->find();
								
								$tunjangan_insentip = $p3d->tunjangan;
								if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $p3d->tunjangan;
								}
							}
							elseif($insentip->penugasan_khusus == 2) {
								$penugasan = ORM::factory('tjpenugasan')->find();
								
								$tunjangan_insentip = $penugasan->tunjangan;
							}						
							else {	
								$tunjangan_insentip = $tpp_staf->nominal;
								
								if(in_array($insentip->lokasi_kerja, array(3417,3420,3426))) {
									$tunjangan_insentip = 0.8 * $tpp_staf->nominal;
								}
							}
						}
					}
					}
					
					// POTONGAN
					$potongan = 0;
					$tjKespeg = $tunjangan_insentip;
					
					// PPH KESPEG
					$pph_all = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,0,$nip);
					$tunjangan_pph = $kalkulasi->tunjangan_pph;
					$pph_kespeg = $pph_all - $tunjangan_pph;
					
					// RSJ	
					if(in_array($pegawai->lokasi_gaji,$arrKodeRSJ)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjKonker = 0;
							$pph_konker = 0;
						}
						else {
							$tjKonker = $tgolongan->ftjkonker;										
							$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
							
							if($pphBlnAll_KK > 0) {
								$pph_konker	= $pphBlnAll_KK - $pph_all;	
							}
						}
					} 
					
						// APBJ	
						if(in_array($pegawai->lokasi_gaji,$arrKodeAPBJ)) {
							if($insentip->tpp_kk_stop == 2) {
								$tjKonker = 0;
								$pph_konker = 0;
							}
							else {
								if($insentip->pokja_apbj == 2) {
								$tjKonker = 2000000;
								}
								else {
								$tjKonker = $tgolongan->ftjapbj;
								}										
								$pphBlnAll_KK = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjKonker,$nip);
								
								if($pphBlnAll_KK > 0) {
									$pph_konker	= $pphBlnAll_KK - $pph_all;	
								}
							}
						} 
						

					
					// KANTOR PERWAKILAN JAKARTA (BERDASAR TEMPAT BERTUGAS) 
					if(in_array($pegawai->lokasi_gaji,$arrKodeJakarta)) {
						if($insentip->tpp_kk_stop == 2) {
							$tjJakarta = 0;
							$pph_jakarta = 0;
						}
						else {						
							$tjJakarta = $tgolongan->ftjjakarta;
							$pphBlnAll_TB = $this->pph_kespeg($gaji_pokok,$istri,$anak,$tunjangan_struktural,$tunjangan_fungsional,$tunjangan_umum,$pembulatan,$tjKespeg,$tjJakarta,$nip);
							
							if($pphBlnAll_TB > 0) {
								$pph_jakarta	= $pphBlnAll_TB - $pph_all;	
							}
						}
					}						
				}
			}
						
			if(in_array($insentip->lokasi_gaji,$arrKK)) {
				if($insentip->tpp_kk_stop == 2) {
					$tjKonker = 0;
					$pph_konker = 0;
				}
			}
			
			if(in_array($insentip->lokasi_gaji,$arrTB)) {
				if($insentip->tpp_tb_stop == 2) {
					$tjJakarta = 0;
					$pph_jakarta = 0;
				}
			}
						
			$arrValue = array(
				"'".$lokasi_id."'",
				"'".mysql_real_escape_string($lokasi_kode)."'",
				"'".mysql_real_escape_string($lokasi_string)."'",
				"'".mysql_real_escape_string($periode)."'",				
				"'".mysql_real_escape_string($name)."'",
				"'".mysql_real_escape_string($nip)."'",
				$golongan_id,
				"'".mysql_real_escape_string($golongan_string)."'",
				"'".mysql_real_escape_string($jabatan)."'",
				$tunjangan_insentip,
				$tjKespeg,
				$potongan,
				$pph_kespeg,
				$tjKonker,
				$pph_konker,
				$tjJakarta,
				$pph_jakarta,
				$eselon_id,
				"'".mysql_real_escape_string($insentip->kelompok_gaji)."'"
			);
			
			$value .= "(".implode(",",$arrValue)."),";
		}
				
		$arrField = array(
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'periode',
			'name', 
			'nip', 
			'golongan_id', 
			'golongan_string',
			'jabatan',
			'insentip',
			'tpp',
			'potongan',
			'pph',
			'konker',
			'pph_konker',
			'jakarta',
			'pph_jakarta',
			'eselon_id',
			'kelompok_gaji'
		);
				
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
				
		$sql = "INSERT INTO tppkelasjabatans (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}
}
?>