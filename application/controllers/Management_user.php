<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_user extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Management_user_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('management_user/management_user_index');

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getData(){
        $nip = $this->session->userdata('B_02B');

        $query  = 
        "SELECT u.*, lok.`name` skpd
        FROM simgaji_users u
        JOIN `simgaji_lokasis` lok ON u.`kode_skpd` = lok.`kode`";

        $search = array('u.`nip`','u.`nama`');
        $where = null;
        // $where  = array('lokasi_kerja' => $lokasis_id); 
        // $where  = array('nama_kategori' => 'Tutorial');
        
        // jika memakai IS NULL pada where sql
        $isWhere = 'deleted_at IS NULL';
        header('Content-Type: application/json');
        echo $this->Management_user_model->getDataUsers($query,$search,$where,$isWhere);
    }

    public function tambah(){
        $data_header['session'] = $this->session->all_userdata();

        $data['unit_organisasi'] = $this->Management_user_model->getUnitOrganisasi();

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('management_user/management_user_tambah', $data);
    }

    public function tambah_action(){
        $nip = $this->input->post('nip', TRUE);
        $nama = $this->input->post('nama', TRUE);
        $role = $this->input->post('role', TRUE);

        $data = array(
            'nip' => $nip,
            'nama' => $nama,
            'type_role' => $role
        );

        $ins = $this->Fungsi_model->tambah('simgaji_bkd2021', $data);
        

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function cari_pns(){
        $nip = $this->input->get('nipnya');

        $data = $this->Management_user_model->getDataPns($nip);

        header('Content-Type: application/json');
		echo json_encode($data);
    }

    public function getSubByUnor(){
        $unor_id = $this->input->get('unit_organisasi_id');
        $get_sub = $this->Management_user_model->getSubUnitByUnor($unor_id);

		$data['sub_unit'] = array();
		foreach ($get_sub as $key => $value) {
			$data['sub_unit'][] = array(
				'id' => $value->KOLOK,
				'name' => $value->NALOK
			);
		}

		header("Content-type: application/json; charset=utf-8");
		echo json_encode($data);
    }
}