<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Pendidikan_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $get = $this->Pendidikan_model->getData();

        $datatable = array();
		foreach ($get as $key => $value) {

            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->kode .'</a>';

			$datatable[$key] = array(
				'kode' => $klik_kode,
				'nama' => $value->name,
			);
		}
		$data['datatable'] = $datatable;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/pendidikan/pendidikan_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function tambah(){
        $data_header['session'] = $this->session->all_userdata();

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/pendidikan/pendidikan_tambah');
    }

    public function tambah_action(){
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $insert = $this->Fungsi_model->tambah('simgaji_pendidikans', $data);
        if ($insert) {
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil menambahkan data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('pendidikan');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal menambahkan data'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('pendidikan');
        }

		// header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function update_action(){
        $pendidikanid = $this->input->post('pendidikanid', TRUE);
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'updated_at' => $now
        );

        $upd = $this->Fungsi_model->edit('simgaji_pendidikans', $pendidikanid, $data);
        if ($upd) {
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil memperbaharui data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('pendidikan');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal memperbaharui data'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('pendidikan');
        }

		// header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getDetailPendidikan(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Pendidikan_model->getDataById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus('simgaji_pendidikans', $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
}