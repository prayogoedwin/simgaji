<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulasi_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');

		// $this->load->model('Fungsi_model');

        $this->tb_lokasi = 'simgaji_lokasis';
        $this->tb_kalkulasi = 'simgaji_kalkulasis';
        $this->tb_pegawai = 'simgaji_pegawaip3ks';
        $this->tb_tjumum = 'simgaji_tjumump3ks';

	}

    public function index(){
		$data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Verifikasi P3K';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Kalkulasi P3K</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
		$this->load->view('kalkulasi/kalkulasip3k_index', $data);
    }

    function gaji_bulanan($periode, $arrLokasi){							
				
		$arrField = array(
			'tanggal', 
			'periode', 
			'lokasi_id', 
			'lokasi_kode',
			'lokasi_string',
			'name', 
			'tanggal_lahir', 
			'nip', 
			'status_id', 
			'status_string',
			'golongan_id', 
			'golongan_string',
			'jabatan', 
			'marital_id', 
			'marital_string',
			'istri', 
			'anak', 
			'jiwa', 
			'jiwa_string', 
			'gaji_pokok', 
			'tunjangan_istri',
			'tunjangan_anak',
			'jumlah_tunjangan_keluarga',
			'jumlah_penghasilan',
			'tunjangan_umum',
			'tunjangan_umum_tambahan',
			'tunjangan_struktural',
			'tunjangan_fungsional',
			'tunjangan_beras', 
			'tunjangan_pph',
			'pembulatan',
			'jumlah_kotor',
			'potongan_bpjs_kesehatan',
			'potongan_pensiun',
			'potongan_iwp',
			'potongan_lain',
			'potongan_beras',
			'potongan_cp',
			'jumlah_potongan',
			'jumlah_bersih',
			'jumlah_bersih_bayar',
			'askes',
			'kelompok_gaji'					
		);
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
        
        $this->db->where_in('lokasi_gaji', $arrLokasi);
        $this->db->where_in('status_id', array(1,2,7,8,9));
        $pegawais = $this->db->get($this->tb_pegawai)->result();
        
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;

			$jabatan = $pegawai->jabatan_string;
			
			// if($pegawai->eselon_id > 1) {
			// 	if($pegawai->eselon_id < 8) {
			// 		if($pegawai->eselon->usia <= $selisih) {
			// 			$pensiun = 1;					
			// 		}	
			// 	}
			// 	$jabatan = $pegawai->kedudukan->name;			
			// }
			// else {
			// 	if($pegawai->fungsional_id > 1) {
			// 		if($pegawai->fungsional->usia <= $selisih) {
			// 			$pensiun = 1;						
			// 		}
			// 		$jabatan = $pegawai->fungsional->name;
			// 	}
			// 	else {
			// 		if($pegawai->kedudukan->usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->kedudukan->name;
			// 	}
			// }
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan->kode;
				$status_string = $pegawai->status->name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital->name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;	
				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {
					$tunjangan_fungsional = ORM::factory('tjfungsional')
						->where('fungsional_id','=',$pegawai->fungsional_id)
						->or_where_open()
						->or_where('fungsional_id','=',$pegawai->fungsional_id)
						->where('golongan','=',$pegawai->golongan_id)
						->or_where_close()
						->find()
						->tunjangan;
						 //$tunjangan_fungsional = 0;
				}
				else {
					if($pegawai->kedudukan_id != 72) {
						$tunjangan_fungsional = ORM::factory('tjfungsional')
							->where_open()
							->where('kedudukan_id','=',$pegawai->kedudukan_id)
							->where('golongan','=',$pegawai->golongan_id[0])
							->where_close()						
							->or_where_open()
							->or_where('fungsional_id','=',$pegawai->fungsional_id)
							->where('golongan','=',$pegawai->golongan_id)
							->or_where_close()
							->find()
							->tunjangan;
					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$tunjangan_struktural = ORM::factory('eselon')
					->where('id','=',$pegawai->eselon_id)
					->find()
					->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {
					$tunjangan_umum = ORM::factory('tjumum')
						->where('golongan','=',$pegawai->golongan_id[0])
						->find()
						->tunjangan;
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 
				
				$beras = ORM::factory('beras')
					->where('bool_id','=',2)
					->find();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();
				
				$arrValue = array(
					"'".mysql_real_escape_string(date("Y-m-d"))."'",
					"'".mysql_real_escape_string($periode)."'",
					$pegawai->lokasi_gaji,
					"'".mysql_real_escape_string($pegawai->gaji->kode)."'",
					"'".mysql_real_escape_string($pegawai->gaji->name)."'",
					"'".mysql_real_escape_string($nama)."'",
					"'".mysql_real_escape_string($tanggal_lahir)."'",
					$nip,
					$pegawai->status_id,
					"'".mysql_real_escape_string($status_string)."'",
					$pegawai->golongan_id,
					"'".mysql_real_escape_string($golongan_string)."'",
					"'".mysql_real_escape_string($jabatan)."'",
					$pegawai->marital_id,
					"'".mysql_real_escape_string($marital_string)."'",
					$jumlah_istri,
					$jumlah_anak,
					$total_jiwa,
					"'".mysql_real_escape_string($jiwa_string)."'",
					$gaji_pokok,
					$tunjangan_istri,
					$tunjangan_anak,
					$tunjangan_istri + $tunjangan_anak,
					$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					$tunjangan_umum,
					$tunjangan_umum_tambahan,
					$tunjangan_struktural,
					$tunjangan_fungsional,
					$tunjangan_beras,
					$pph_bulan,
					$pembulatan,
					$jumlah_kotor,
					$potongan_bpjs_kesehatan,
					$potongan_pensiun,
					$potongan_iwp,
					$potongan_lain,
					$potongan_beras,
					$potongan_cp,
					$jumlah_potongan,
					$jumlah_bersih,
					$jumlah_bersih_bayar,
					$askes,
					"'".mysql_real_escape_string($pegawai->kelompok_gaji)."'"					
				);
				
				$value .= "(".implode(",",$arrValue)."),";
				
				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasis (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/
				
				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();
			}
		}
		
		$field = implode(",",$arrField);		
		$value = substr_replace($value,"",-1);
		
		$sql = "INSERT INTO kalkulasis (".$field.") VALUES ".$value;	
		$query = DB::query(Database::INSERT, $sql)->execute();	
	}

    public function action(){

       
        $lokasi_start = $_POST['lokasi_start'];
		$lokasi_end =$_POST['lokasi_end'];

        if($_POST['jenis'] != "GAJI_SUSULAN") {
			$periode = $_POST['tahun'];
			
			if(isset($_POST['bulan_id'])) {	
				$periode .= "-".$_POST['bulan_id']."-01";
			}
			
			$arrLokasi = array();
			if($_POST['lokasi_end']) {

                if(substr($lokasi_end,-3) == "000"){
                    $this->db->where('LEFT("kode",2)',substr($lokasi_end,0,2));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->result();
                }else{
                    $this->db->where('LEFT("kode",6)',substr($lokasi_end,0,2));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->result();
                }

                $this->db->where('kode >=', $lokasi_start);
                $this->db->where('kode <=', $max->kode);
                $lokasis = $this->db->get($this->tb_lokasi)->result();
					
				foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

			} else {

                if(substr($lokasi_start,-4) == "0000") {

                    if(substr($lokasi_start,0,2) == "34") {

                        $this->db->where('LEFT("kode",2) =', 34);
                        $this->db->where('LEFT("kode",4) !=', 3411);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();

					} else {

                        $this->db->where('LEFT("kode",2) =', substr($lokasi_start,0,2));
                        $lokasis = $this->db->get($this->tb_lokasi)->result();

					}


                }else{

                    $this->db->where('LEFT("kode",6) =', substr($lokasi_start,0,6));
                    $lokasis = $this->db->get($this->tb_lokasi)->result();

                }

                foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	
				
			}
		}

        if($_POST['jenis'] == "GAJI_BULANAN") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasi);
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gaji_bulanan($periode,$arrLokasi);
		}
    }

	function gajip3k_bulanan_old($periode, $arrLokasi){							
		error_reporting(0);
		
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
        $this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
        $this->db->where_in('lokasi_gaji', $arrLokasi);
        $this->db->where_in('status_id', array(1,2,7,8,9));
        $pegawaip3ks = $this->db->get()->result();

		// echo json_encode($pegawaip3ks);
		// die();
		
		foreach($pegawaip3ks as $pegawai) {

			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			// $xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			// $dd = $xperiode[2]-$xtgllahir[2];
			// $mm = ($xperiode[1]-$xtgllahir[1])*30;
			// $yy = ($xperiode[0]-$xtgllahir[0])*363;
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			
			$selisih = $dd + $mm + $yy;
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan_name	;	
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional_name;
				}
				else {
					if($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name	;	
				}
			}

			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

					
					//$tunjangan_fungsional = 0;
				} else {
					
					if($pegawai->kedudukan_id != 72) {

						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();

					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}

				
				// Kondisi Khusus untk Tunjangan Sandiman, Nominal Tunj Fungs dipindahkan ke Nominal $tunjangan_umum_tambahan di cetakan disebut sbg Tunj Kompensasi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) { //Jika Ahli Sandi tingkat 1-6
					if($pegawai->kompensasi_id >= 879 AND $pegawai->kompensasi_id <= 891) { //Jika Sandiman ( tunjangan kompensasi pemula-madya) dan merupakan JFT
						//Diisi tunjangan funstionalnya sesuai nominal
						//Diisi tunjangan kompensasinya di tunj umum tambahan
						//Dinolkan tunjangan umumnya karena merupakan fungsional umum
					

						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						
						
					$tunjangan_umum = 0;
					} else { //Kalau yang ini fungsional tertentu (JFT) tapi bukan sandiman
						$tunjangan_fungsional = 0;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						}
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 

				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// $jumlah_potongan = round($potongan_lain + $potongan_iwp);
				// $gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum + $tunjangan_umum_tambahan);
				// $gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// $jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras + $tunjangan_umum_tambahan);
				// $jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);

				// echo json_encode($arrFieldVal);
				// die();

			}
		}

		
		
		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi'
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}




}