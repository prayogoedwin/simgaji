<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verifikator extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->verifikator = 'simgaji_verifikator';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Verifikator_model');
    }

    // public function index(){
    //     redirect('dashboard');
    // }

    public function cari_pns(){
        $nip = $this->input->get('nipnya');
        $A_01 = 'B2';

        $data = $this->Verifikator_model->getDataPns($nip, $A_01);

        header('Content-Type: application/json');
		echo json_encode($data);
    }

    public function cari_pns_all_dinas(){
        $nip = $this->input->get('nipnya');
        $A_01 = 'B2';

        $data = $this->Verifikator_model->getDataPnsAllDinas($nip, $A_01);

        header('Content-Type: application/json');
		echo json_encode($data);
    }



    //CRUD users START//
    public function get_verifikator()
    {
        error_reporting(0);
        $get = $this->Verifikator_model->get_verifikator();
        $datatable = array();
        foreach ($get as $key => $value) {
            
            // $select = explode(',',$value->kode_skpd_simpeg);
            // $select = explode(',',$value->kode_skpd);
            // $datasel = '<ul>';

            // foreach ($select as $val){
            //     $datasel .= '<li>'.getKodeByLokasis($val).' - '.$val.'</li>';
            // }

            // $datasel .= '</ul>';
            // // $datasel = NULL;

            // $select2 = explode(',',$value->kode_skpd_simpeg);
            // $datasel2 = '<ul>';

            // foreach ($select2 as $val2){
            //     $datasel2 .= '<li>'.getKolokSimpegByLokasisSimpeg($val2).' - '.$val2.'</li>';
            // }

            // $datasel2 .= '</ul>';

            // echo $datasel;
            // die();

            $lihat = '<a href="javascript:void(0)" onclick="detailData(' . $value->id . ')">Lihat SKPD Yang Diampu</a>';


            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData(' . $value->id . ')">' . $value->nip . '</a>';
            $add = '<a class="btn btn-primary btn-sm"chref="javascript:void(0)" onclick="addData(' . $value->id . ')">+</a>';
            $datatable[$key] = array(
                'id' => '<input type="checkbox" class="data-check" value="' . $value->id . '">',
                'nip' => $klik_kode,
                'nama' => $value->nama,
                'role' => role_data($value->type_role),
                'lihat' => $lihat,
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$add,
                // 'kode_skpd' => $datasel,
                // 'mngampu' => $datasel2,
            );
        }
        // $data['datatable'] = $datatable;

        $output = array(

            "recordsTotal" => $this->Verifikator_model->verifikator_count_all(),
            "recordsFiltered" => $this->Verifikator_model->verifikator_count_filtered(),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Verifikator';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Verifikator</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();
        $nip = $this->session->userdata('B_02B');
        $data['verifikator'] = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip ='$nip'");



        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('verifikator/index', $data);
    }

    public function get_detail_verifikator()
    {
        $id = $this->input->post('id', TRUE);
        $data = $this->Verifikator_model->getVerifikatorById($id);

        // $select = explode(',',$data->kode_skpd_simpeg);
        $select = explode(',',$data->kode_skpd);

        $datasel = array();
        foreach ($select as $key => $val){
            $datasel[$key] = $val;
        }


        
        if ($data != null) {
            $response = array(
                'status' => 1,
                'message' => 'Data berhasil ditemukan',
                'data' => $data,
                'sel' => $datasel
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Data gagal ditemukan',
                'data' => null,
                'sel' => $datasel
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function verifikator_add_action()
    {
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);
        $nip = $this->input->post('nip', TRUE);
        $nama = $this->input->post('nama', TRUE);
        $role = $this->input->post('role', TRUE);
        if($role == 2){
            $kode_skpd = implode(',', $_POST['kode_skpd_add']);
        }else{
            $kode_skpd = null;
        }
        

        $select = explode(',',$kode_skpd);

        $count = count($select)-1;
        
        $datasel = NULL;
        foreach ($select as $key => $val){
            if($count == $key){
                $datasel .= getKolokByLokasis($val);
            }else{
                $datasel .= getKolokByLokasis($val).',';
            }
           
        }

        $datasel_id = NULL;
        foreach ($select as $key_id => $val_id){
            if($count == $key_id){
                $datasel_id .= getIdLokasisByLokasis($val_id);
            }else{
                $datasel_id .= getIdLokasisByLokasis($val).',';
            }
           
        }

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        if($role != 2){

            $data = array(
                // 'kode_skpd' => $nama,
                'nip'       => $username,
                'nama'       => $nip.' - '.$nama,
                'type_role'  => $role,
                'id_skpd' => $datasel_id,
                'kode_skpd' => $kode_skpd,
                'kode_skpd_simpeg' => $datasel,
                'created_at' => $now,
                'created_by' => $userid,
                'password' => md5($password)
            );

        }else{

            $data = array(
                // 'kode_skpd' => $nama,
                'nip'       => $nip,
                'nama'       => $nama,
                'type_role'  => $role,
                'id_skpd' => $datasel_id,
                'kode_skpd' => $kode_skpd,
                'kode_skpd_simpeg' => $datasel,
                'created_at' => $now,
                'created_by' => $userid
            );

        }
        

        $update = $this->Fungsi_model->tambah($this->verifikator, $data);
        if ($update) {
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil add data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal add data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function verifikator_update_action()
    {
        $id = $this->input->post('id', TRUE);
        // $nip = $this->input->post('nip', TRUE);
        $username = $this->input->post('username', TRUE);
        // $kode_skpd = implode(',', $_POST['kode_skpd_add']);

       

        $role = $this->input->post('role', TRUE);
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        if($role == 2 || $role == 1){
            $kode_skpd = implode(',', $_POST['kode_skpd_add']);
        }else{
            $kode_skpd = null;
        }

        $select = explode(',',$kode_skpd);

        $count = count($select)-1;
     
        
        
        $datasel = NULL;
        foreach ($select as $key => $val){
            if($count == $key){
                $datasel .= getKolokByLokasis($val);
            }else{
                $datasel .= getKolokByLokasis($val).',';
            }
           
        }

        $datasel_id = NULL;
        foreach ($select as $key_id => $val_id){
            if($count == $key_id){
                $datasel_id .= getIdLokasisByLokasis($val_id);
            }else{
                $datasel_id .= getIdLokasisByLokasis($val_id).',';
            }
           
        }

        if($role == 1){

            $data = array(
                // 'kode_skpd' => $nama,
                
                'id_skpd' => $datasel_id,
                'nip'       => $username,
                'kode_skpd' => $kode_skpd,
                'kode_skpd_simpeg' => $datasel,
                'type_role'       => $role,
                'updated_at' => $now,
                'updated_by' => $userid
            );

        }else{

            $data = array(
                'id_skpd' => $datasel_id,
                'kode_skpd' => $kode_skpd,
                'kode_skpd_simpeg' => $datasel,
                'type_role'       => $role,
                'updated_at' => $now,
                'updated_by' => $userid
            );

        }
        
        // header('Content-Type: application/json');
        // echo json_encode($data);

       

        $update = $this->Fungsi_model->edit($this->verifikator, $id, $data);
        if ($update) {
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil update data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal update data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function verifikator_hapus_action()
    {
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->verifikator, $id, $data);
        if ($hps) {
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function verifikator_hapus_soft_action()
    {
        $list_id = $this->input->post('id');

        $data = array(
            'is_deleted' => 1
        );
        foreach ($list_id as $id) {

            $this->Fungsi_model->soft_deleted('simgaji_verifikator', 'id', $id, $data);
            // $user_x = $this->session->userdata('id');
            // create_log($user_x, 5, 'Sukses Hapus Data (Id Pengguna :' . $id . ')');
        }
        echo json_encode(array("status" => TRUE));
    }
    //CRUD JFT END//





}
