<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masterpegawai_p3ks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->pegawais = 'simgaji_pegawaip3ks';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Masterpegawai_p3ks_model');
        $this->load->model('History_p3k_model');
        $this->load->model('Pegawai_p3k_model');

        $this->tb_pegawai = 'simgaji_pegawaip3ks';
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Master Pegawai P3K';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Master Pegawai P3K</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();
        $nip = $this->session->userdata('B_02B');
        $data['verifikator'] = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip ='$nip'");

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_p3k/index', $data);
    }

    //CRUD users START//
    public function get_pegawai()
    {
        // if($session['role'] == 2);
        

        $nip =  $this->session->userdata('B_02B');
        $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$nip' AND deleted_at IS NULL");
        if($x->num_rows() > 0){
            $x = $x->row();
            $role = $x->type_role;
            $skpd = $x->kode_skpd;
        }else{
            $role = '';
            $skpd = '';
        }
        if($role == 2){
            $get = $this->Masterpegawai_p3ks_model->get_pegawai_where($skpd);
        }else{
            $get = $this->Masterpegawai_p3ks_model->get_pegawai();
        }
        
        $datatable = array();
        foreach ($get as $key => $value) {

            // $merah = $value->tolak + $value->revisi;
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a target="BLANK" href="' . base_url('verifikasi_p3ks/detail/') . encode_url($value->pegawai_id) . '" >' . $value->B_02B . '</a>';
            // $klik_kode = '<a href="javascript:void(0)" onclick="detailData(' . $value->id . ')">' . $value->nip . '</a>';
            $x = encode_url($value->id);
            $klik_kode = '<a href="'.BASE_URL('pegawai_p3k/data/'.$x).'">' . $value->nip . '</a>';
            $datatable[$key] = array(
                'nip' => $klik_kode,
                'nama' => $value->name,
                'lokasikerja' => $value->lokasi_kerja,
                'lokasigaji' => $value->lokasi_gaji,
                // 'id' => '<input type="checkbox" class="data-check" value="' . $value->id . '">',
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$add,
            );
        }
        // $data['datatable'] = $datatable;

        if($role == 2){
            
            $output = array(
                "recordsTotal" => $this->Masterpegawai_p3ks_model->pegawai_count_all_skpd($skpd),
                "recordsFiltered" => $this->Masterpegawai_p3ks_model->pegawai_count_filtered_skpd($skpd),
                "data" => $datatable,
            );
        }else{
            $output = array(
                "recordsTotal" => $this->Masterpegawai_p3ks_model->pegawai_count_all(),
                "recordsFiltered" => $this->Masterpegawai_p3ks_model->pegawai_count_filtered(),
                "data" => $datatable,
            );
        }

        

        // $output = array(

        //     "recordsTotal" => array(),
        //     "recordsFiltered" => array(),
        //     "data" => $datatable,
        // );


        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function add(){
        // $real_id = decode_url($id);
        $cek = TRUE;
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

                // $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
                $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();

                $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
                $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
                $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
                $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
                $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
                $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
                $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
                $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
                $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
                $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
                $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
                $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');
                
                $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
                $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
                // $data['masterjfu'] = array();
                // $data['masterjft'] = array();
                // $data['mastersotk'] = array();
                // $data['mastersotk'] = array();
            
                $this->load->view('template/head');
                $this->load->view('template/header', $data_header);
                $this->load->view('master/pegawai_p3k/add', $data);

                // header('Content-Type: application/json');
                // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('masterpegawai_p3ks');
        }
    }

    

   # fungsi untuk mengecek status username dari db
	function cek_nip(){
		# ambil username dari form
		$nip = $_POST['nip'];
						# select ke model member username yang diinput user
		$hasil_nip = $this->Masterpegawai_p3ks_model->cek_nip($nip);
	
		if($hasil_nip == 1){
			echo "1";
		}else{
			echo "0";
		}

    }

    public function add_action(){

        foreach ($_POST as $key => $value) {
            $data[htmlspecialchars($key)] = htmlspecialchars($value); 
        }
        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);
        $x = $this->db->insert($this->tb_pegawai, $merge);
        $pegawai_id = $this->db->insert_id();
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Tambah Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Tambah Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action(){


        $pegawai_id = $this->input->post('id');
        foreach ($_POST as $key => $value) {
            $data[htmlspecialchars($key)] = htmlspecialchars($value); 
        }

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus($this->tb_pegawai, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

}
?>