<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsdisdik extends CI_Controller {

    public function __construct(){
		parent::__construct();

        $this->load->model('Wsdisdik_model');
    }

    public function action_auth(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $params = $_REQUEST;

            if(isset($_POST['username']) && isset($_POST['password'])){

                $username = $params['username'];
                $pass = $params['password'];

                if($username == 'disdik' && $pass == 'ls959*HEz$Y3'){
                    $statusHeader = array(
                        'status' => 200,
                    );

                    // $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) );
                    $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($this->randomss(16)), 4) );
                    $upd_uuid = array(
                        'token' => $UUID,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Wsdisdik_model->update_token('simgaji_token', '3', $upd_uuid);

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Authentication success',
                        'auth_token' => $UUID
                    );
                } else {
                    $statusHeader = array(
                        'status' => 401,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Username & Password not found'
                    );
                }
                
            } else {
                $statusHeader = array(
                    'status' => 403,
                );

                $response = array(
                    'status' => $statusHeader['status'],
                    'message' => 'Required parameter'
                );
            }

            json_output($statusHeader['status'],$response);
        }
    }

    public function gaji_asn(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsdisdik_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['sub_unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = '34';
                    $sub_unit_kerja = $params['sub_unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if(isset($_POST['nip'])){
                        $nip = $params['nip'];
                    } else {
                        $nip = null;
                    }

                    if($service_code == 'pns'){
                        if($nip != null){
                            // 196301241987121001
                            $data = $this->Wsdisdik_model->getGajiPnsByPeriodeNip($periode, $nip);
                        } else {
                            $data = $this->Wsdisdik_model->getGajiPnsByPeriodeUkSuk($periode, $unit_kerja, $sub_unit_kerja);
                        }
                    } else if ($service_code == 'p3k'){
                        if($nip != null){
                            $data = $this->Wsdisdik_model->getGajiP3kByPeriodeNip($periode, $nip);
                        } else {
                            $data = $this->Wsdisdik_model->getGajiP3kByPeriodeUkSuk($periode, $unit_kerja, $sub_unit_kerja);
                        }
                        
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_tpp(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsdisdik_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['sub_unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = '34';
                    $sub_unit_kerja = $params['sub_unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if(isset($_POST['nip'])){
                        $nip = $params['nip'];
                    } else {
                        $nip = null;
                    }

                    if($service_code == 'pns'){
                        if($nip != null){
                            $data = $this->Wsdisdik_model->getTppPnsByPeriodeNip($periode, $nip);
                        } else {
                            $data = $this->Wsdisdik_model->getTppPnsByPeriodeUkSuk($periode, $unit_kerja, $sub_unit_kerja);
                        }
                    } else if ($service_code == 'p3k'){
                        // if($nip != null){
                        //     $data = $this->Wsdisdik_model->getGajiP3kByPeriodeNip($periode, $nip);
                        // } else {
                        //     $data = $this->Wsdisdik_model->getGajiP3kByPeriodeUkSuk($periode, $unit_kerja, $sub_unit_kerja);
                        // }
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    private function randomss($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }
}