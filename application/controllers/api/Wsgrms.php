<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsgrms extends CI_Controller {
    public function __construct(){
		parent::__construct();

        $this->load->model('Wsgrms_model');
    }

    public function action_auth(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $params = $_REQUEST;

            if(isset($_POST['username']) && isset($_POST['password'])){

                $username = $params['username'];
                $pass = $params['password'];

                if($username == 'grmsjateng' && $pass == 'L10F6*4^X#a5'){
                    $statusHeader = array(
                        'status' => 200,
                    );

                    // $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) );
                    $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($this->randomss(16)), 4) );
                    $upd_uuid = array(
                        'token' => $UUID,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Wsgrms_model->update_token('simgaji_token', '5', $upd_uuid);

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Authentication success',
                        'auth_token' => $UUID
                    );
                } else {
                    $statusHeader = array(
                        'status' => 401,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Username & Password not found'
                    );
                }
                
            } else {
                $statusHeader = array(
                    'status' => 403,
                );

                $response = array(
                    'status' => $statusHeader['status'],
                    'message' => 'Required parameter'
                );
            }

            json_output($statusHeader['status'],$response);
        }
    }
    
    public function gaji_asn(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsgrms_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'pns'){
                        $data = $this->Wsgrms_model->getGajiPnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsgrms_model->getGajiP3kByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $data_res = array();
                        foreach ($data as $key => $value) {

                            $iwp = strval((($value->gaji_pokok + $value->jumlah_tunjangan_keluarga) * 8) / 100);

                            $tj_jabatan = strval($value->tunjangan_umum + $value->tunjangan_fungsional + $value->tunjangan_struktural);
                            //$iuran_jkk = strval(round(($value->gaji_pokok)* 0.0024));
                            $iuran_jkk = ($value->status_id == 8 ? 0 : strval(round(($value->gaji_pokok)* 0.0024)));
                            //$iuran_jkm = strval(round(($value->gaji_pokok)* 0.0072));
                            $iuran_jkm = ($value->status_id == 8 ? 0 : strval(round(($value->gaji_pokok)* 0.0072)));

                           $data_res[$key] = array(
                            'urutan' => $key+1,
                            'nik' => $value->NIK,
                            'periode_gaji' => $value->periode_gaji,
                            'kode_lokasi' => $value->kode_lokasi,
                            'lokasi_gaji' => $value->lokasi_gaji,
                            'nip' => $value->NIP,
                            'nama' => $value->Nama,
                            'golongan' => $value->Golongan,
                            'jabatan' => $value->Jabatan,
                            'gapok' => $value->gaji_pokok,
                            'tj_istri_suami' => $value->tunjangan_istri,
                            'tj_anak' => $value->tunjangan_anak,
                            'tj_jabatan' => $tj_jabatan,
                            'tj_kompensasi' => $value->tunjangan_umum_tambahan,
                            'tj_beras' => $value->tunjangan_beras,
                            'tj_pph' => $value->tunjangan_pph,
                            'tj_pembulatan' => $value->pembulatan,
                            'gaji_bruto' => $value->jumlah_kotor,
                            'pot_bpjs_kesehatan_1persen' => $value->potongan_bpjs_kesehatan,
                            'pot_pensiun_taspen' => $value->potongan_pensiun,
                            'jumlah_pot' => $value->jumlah_potongan,
                            'pot_pph' => $value->tunjangan_pph,
                            'gaji_bersih' => $value->Gaji,
                            'bpjs_kesehatan_4persen' => $value->askes,
                            'iuran_jkk' => $iuran_jkk,
                            'iuran_jkm' => $iuran_jkm
                           );
                        }

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data_res
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_asn_13(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsgrms_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    $data = array();
                    if($service_code == 'pns'){
                        $data = $this->Wsgrms_model->getGaji13PnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsgrms_model->getGaji13P3kByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_susulan(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsgrms_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'cpns'){
                        $data = $this->Wsgrms_model->getGajiSusulanCpnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsgrms_model->getGajiSusulanP3kByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $data_res = array();
                        foreach ($data as $key => $value) {

                            $iwp = strval((($value->gaji_pokok + $value->jumlah_tunjangan_keluarga) * 8) / 100);

                            $tj_jabatan = strval($value->tunjangan_umum + $value->tunjangan_fungsional + $value->tunjangan_struktural);
                            $iuran_jkk = strval(round($value->gaji_pokok * 0.0072));
                            $iuran_jkm = strval(round($value->gaji_pokok * 0.0024));

                           $data_res[$key] = array(
                            'urutan' => $key+1,
                            'nik' => $value->nik,
                            'periode_gaji_susulanp3k' => $value->periode,
                            'kode_lokasi' => $value->lokasi_kode,
                            'lokasi_gaji' => $value->lokasi_string,
                            'nip' => $value->nip,
                            'nama' => $value->name,
                            'golongan' => $value->golongan_string,
                            'jabatan' => $value->jabatan,
                            'gaji' => $value->jumlah_bersih_bayar,

                            'tj_istri_suami' => $value->tunjangan_istri,
                            'tj_anak' => $value->tunjangan_anak,
                            'tj_jabatan' => $tj_jabatan,
                            'tj_kompensasi' => $value->tunjangan_umum_tambahan,
                            'tj_beras' => $value->tunjangan_beras,
                            'tj_pph' => $value->tunjangan_pph,
                            'tj_pembulatan' => $value->pembulatan,
                            'gaji_bruto' => $value->jumlah_kotor,
                            'pot_bpjs_kesehatan_1persen' => $value->potongan_bpjs_kesehatan,
                            'pot_pensiun_taspen' => $value->potongan_pensiun,
                            'jumlah_pot' => $value->jumlah_potongan,
                            'pot_pph' => $value->tunjangan_pph,
                            // 'gaji_bersih' => $value->Gaji,
                            'bpjs_kesehatan_4persen' => $value->askes,
                            'iuran_jkk' => $iuran_jkk,
                            'iuran_jkm' => $iuran_jkm
                           );
                        }

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data_res
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_tpp(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsgrms_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    $get = array();
                    if($service_code == 'pns'){
                        $get = $this->Wsgrms_model->getGajiTppPnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $get = $this->Wsgrms_model->getGajiTppP3kByUk($periode, $unit_kerja);
                    }

                    $data = array();
                    foreach ($get as $key => $val) {

                        $tpp_bebankerja_bruto = ($val->beban_kerja - $val->beban_kerja_pot) + $val->beban_kerja_pph;
                        $tpp_bebankerja_khusus_bruto = $val->beban_kerja_khusus + $val->beban_kerja_khusus_pph;
                        $tpp_tempat_bruto = $val->tempat + $val->tempat_pph;
                        $tpp_kondisi_bruto = $val->kondisi + $val->kondisi_pph;
                        $tpp_plt_bruto = $val->plt + $val->plt_pph;

                        $pot_bpjskes1p_plt = strval(0);
                        if($val->plt_pot_bpjs != null || $val->plt_pot_bpjs != ''){
                            $pot_bpjskes1p_plt = $val->plt_pot_bpjs;
                        }

                        $data[$key] = array(
                            'no' => $key + 1,
                            'nik' => $val->nik,
                            'periode_tpp' => $val->periode,
                            'kode_lokasi' => $val->lokasi,
                            'lokasi_gaji' => $val->lokasi_gaji_string,
                            'nip' => $val->nip,
                            'nama' => $val->name,
                            'golongan' => $val->golongan_string,
                            'jabatan' => $val->jabatan_string,
                            'kelas_jabatan' => $val->kelas_jabatan,
                            'tpp_beban_kerja_bruto' => strval($tpp_bebankerja_bruto),
                            'pph_bebankerja' => $val->beban_kerja_pph,
                            'pot_bpjskes_1persen_bebankerja' => $val->beban_kerja_pot_bpjs,
                            'tpp_bebankerja_bersih' => $val->beban_kerja_nominal,
                            'tunj_bpjskes_4persen_bebankerja' => $val->beban_kerja_tun_bpjs,
                            'tpp_beban_kerja_khusus_bruto' => strval($tpp_bebankerja_khusus_bruto),
                            'pph_bebankerja_khusus' => $val->beban_kerja_khusus_pph,
                            'pot_bpjskes_1persen_khusus' => $val->beban_kerja_khusus_pot_bpjs,
                            'tpp_beban_kerja_khusus_bersih' => $val->beban_kerja_khusus_nominal,
                            'tunj_bpjskes_4persen' => $val->beban_kerja_khusus_tun_bpjs,
                            'tpp_tempat_bruto' => strval($tpp_tempat_bruto),
                            'pph_tempat' => $val->tempat_pph,
                            'pot_bpjskes_1persen_tempat' => $val->tempat_pot_bpjs,
                            'tpp_tempat_bersih' => $val->tempat_nominal,
                            'tunj_bpjskes_4persen_tempat' => $val->tempat_tun_bpjs,
                            'tpp_kondisi_bruto' => strval($tpp_kondisi_bruto),
                            'pph_kondisi' => $val->kondisi_pph,
                            'pot_bpjskes_1persen_kondisi' => $val->kondisi_pot_bpjs,
                            'tpp_kondisi_bersih' => $val->kondisi_nominal,
                            'tunj_bpjskes_4persen_kondisi' => $val->kondisi_tun_bpjs,
                            'tpp_plt_bruto' => strval($tpp_plt_bruto),
                            'pph_plt' => $val->plt_pph,
                            'pot_bpjskes_1persen_plt' => $pot_bpjskes1p_plt,
                            'tpp_plt_bersih' => $val->plt_nominal,
                            'tunj_bpjskes_4persen_plt' => $val->plt_tun_bpjs
                        );
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_asn_14(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsgrms_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    $data = array();
                    if($service_code == 'pns'){
                        $data = $this->Wsgrms_model->getGajiPns14ByUk($periode, $unit_kerja);
                    } 
                    else if($service_code == 'p3k'){
                        $data = $this->Wsgrms_model->getGajiP3k14ByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    private function randomss($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }
}