<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbpjs extends CI_Controller {
    public function __construct(){
		parent::__construct();

        $this->load->model('Wsbpjs_model');
    }

    public function action_auth(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $params = $_REQUEST;

            if(isset($_POST['username']) && isset($_POST['password'])){

                $username = $params['username'];
                $pass = $params['password'];

                if($username == 'bpjssmg' && $pass == 'RF0u^#9wy1QP'){
                    $statusHeader = array(
                        'status' => 200,
                    );

                    // $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) );
                    $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($this->randomss(16)), 4) );
                    $upd_uuid = array(
                        'token' => $UUID,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Wsbpjs_model->update_token('simgaji_token', '2', $upd_uuid);

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Authentication success',
                        'auth_token' => $UUID
                    );
                } else {
                    $statusHeader = array(
                        'status' => 401,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Username & Password not found'
                    );
                }
                
            } else {
                $statusHeader = array(
                    'status' => 403,
                );

                $response = array(
                    'status' => $statusHeader['status'],
                    'message' => 'Required parameter'
                );
            }

            json_output($statusHeader['status'],$response);
        }
    }

    public function gaji_asn(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpjs_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    // if($service_code == 'pns'){
                    //     $data = $this->Wsbpjs_model->getGajiPnsByUk($periode, $unit_kerja);
                    // } else if($service_code == 'p3k'){
                    //     $data = $this->Wsbpjs_model->getGajiP3kByUk($periode, $unit_kerja);
                    // }

                    $data = $this->Wsbpjs_model->getGajiPnsByUk($service_code, $periode, $unit_kerja);
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_susulan(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpjs_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    // if($service_code == 'pns'){
                    //     $data = $this->Wsbpjs_model->getGajiPnsByUk($periode, $unit_kerja);
                    // } else if($service_code == 'p3k'){
                    //     $data = $this->Wsbpjs_model->getGajiP3kByUk($periode, $unit_kerja);
                    // }
                    if($service_code == 'p3k'){
                        $data = $this->Wsbpjs_model->getGajiSusulanP3k($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_tpp(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpjs_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    // if($service_code == 'pns'){
                    //     $data = $this->Wsbpjs_model->getGajiPnsByUk($periode, $unit_kerja);
                    // } else if($service_code == 'p3k'){
                    //     $data = $this->Wsbpjs_model->getGajiP3kByUk($periode, $unit_kerja);
                    // }
                    if($service_code == 'pns'){
                        $data = $this->Wsbpjs_model->getGajiTppByPeriodeUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_tpp_keu(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpjs_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];

                    $data = $this->Wsbpjs_model->getGajiTppKeuByPeriodeUk($periode, $unit_kerja);
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_keu(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpjs_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];

                    $data = $this->Wsbpjs_model->getGajiKeuByPeriodeUk($periode, $unit_kerja);
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    private function randomss($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }
}