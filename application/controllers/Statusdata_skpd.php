<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Statusdata_skpd extends CI_Controller{
    public function __construct(){
        parent::__construct();

        $lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Statusdata_skpd_model');
    }

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $lokasis_id = $this->session->userdata('id_lokasis');

        $data['status_skpd'] = $this->Statusdata_skpd_model->getDataById($lokasis_id);

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('statusdata_skpd/statusdata_skpd_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function update_action(){
        $lokasis_id = $this->session->userdata('id_lokasis');
        $status_gaji = $this->input->post('status_gaji', TRUE);

        $data = array(
            'status_gaji' => $status_gaji
        );

        $upd = $this->Fungsi_model->edit('simgaji_lokasis', $lokasis_id, $data);
        if($upd){
            $array_msg = array(
				'status'	=>	'success',
				'message'	=>	'Berhasil memperbaharui data'
			);

			$this->session->set_flashdata($array_msg);
			redirect('statusdata_skpd');
        } else {
            $array_msg = array(
				'status'	=>	'error',
				'message'	=>	'Gagal memperbaharui data'
			);

			$this->session->set_flashdata($array_msg);
			redirect('statusdata_skpd');
        }
    }
}