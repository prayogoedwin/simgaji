<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pegawai_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_pegawai_p3k_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;

        $kode_lokasis = getDetailLokasisById($lokasis_id)->kode;
        $kolok_simpeg = getDetailLokasisById($lokasis_id)->kolok_simpeg;
        $get = $this->Tpp_pegawai_p3k_model->getDataPegawaiByKode($kode_lokasis, $kolok_simpeg);

        $datatable = array();
        foreach ($get as $key => $value) {
            // if($statusSkpd == '2'){
            //     $klik_nip = '<a href="'. base_url('pegawai_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
            // } else {
            //     $klik_nip = $value->nip;
            // }

            // $skp = 
            // '<div>
            //     <input type="hidden" class="form-control font-weight-semibold" name="pegawai_id[]" value="'. $value->id .'">
            //     <input type="hidden" class="form-control font-weight-semibold" name="nips[]" value="'. $value->nip .'">
            //     <input type="text" class="form-control font-weight-semibold" placeholder="Nilai SKP" name="skp[]" value="100" autocomplete="off">
            // </div>';

            // $pot = 
            // '<div">
            //     <input type="text" class="form-control font-weight-semibold" placeholder="Potongan" name="potongan[]" value="0" autocomplete="off">
            // </div>';
            
            $jab = '-';
            if($value->I_JB != null){
                $jab = $value->I_JB;
            }

            $klsjab = '-';
            if($value->kelasjab != null){
                $klsjab = $value->kelasjab;
            }

            $rmpn = '-';
            if($value->RUMPUN != null){
                $rmpn = $value->RUMPUN;
            }

            $datatable[$key] = array(
                'B_02B' => $value->B_02B,
                'nama' => $value->nama,
                'jabatan' => $jab,
                'cuti_2bulanlalu' => hitungCSByNip($value->B_02B),
                'kelas_jabatan' => $klsjab,
                'rumpun' => $rmpn,
                'F_03' => $value->F_03,
                'hukdis' => hitungHukdisByNip($value->B_02B)
                // 'skp' => $skp,
                // 'potongan' => $pot,
            );
        }
        $data['pegawais'] = $datatable;

        // $data['pegawais'] = $this->Tpp_pegawai_p3k_model->getDataPegawai($lokasis_id);

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('tpp_pegawai_p3k/tpp_pegawai_p3k_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function ajukan_tpp_action(){
        $user_id = $this->session->userdata('id');
        $nip = $this->session->userdata('B_02B');
        // $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $nips = $this->input->post('nips', TRUE);
        // $skp = $this->input->post('skp', TRUE);
        // $potongan = $this->input->post('potongan', TRUE);
        // $periode = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
        $gols = $this->input->post('gols', TRUE);
        $periode_bulan = getDataPeriode()->bulan;
        $periode_tahun = getDataPeriode()->tahun;
        $jabatans = $this->input->post('jabatans', TRUE);
        $sakit = $this->input->post('sakit', TRUE);
        // $tbs = $this->input->post('tb_' ., TRUE);
        // for($i=0; $i < count($tbs); $i++){
        //     echo "Selected " . $tbs[$i] . "<br/>";
        // }
        // echo json_encode($tbs);
        // die();
        // if($tb == null){
        //     $tb = 0;
        // }
        $skp = $this->input->post('skp', TRUE);
        $keterangan = $this->input->post('keterangan', TRUE);

        $tanggal = date('Y-m-d');
        $now = date('Y-m-d H:i:s');

        $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
        $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
        foreach ($dataAllMyVerifikator as $ki => $valVerif) {
            $idVerif[$ki] = $valVerif->id;
        }
        $allIdVerifikator = implode(",", $idVerif);
        if(count($dataAllMyVerifikator) > 0){
            $data_bulk = array();
            // foreach ($skp as $key => $value) {
            //     $data_bulk[$key] = array(
            //         'tanggal' => $tanggal,
            //         'periode' => $periode,
            //         'pegawai_id' => $pegawai_id[$key],
            //         'verifikator' => $allIdVerifikator,
            //         'status_ajuan' => 1,
            //         'status_acc' => 0,
            //         'posisi_acc' => getNipSekByKolok($myKolok),
            //         'posted_by' => $user_id,
            //         'nip_by' => $nip,
            //         'skp' => $skp[$key],
            //         'potongan' => $potongan[$key],
            //         'created_at' => $now
            //     );
            // }

            foreach ($nips as $key => $val) {
                $data_bulk[$key] = array(
                    'nip' => $nips[$key],
                    'bulan' => $periode_bulan,
                    'tahun' => $periode_tahun,
                    'gol' => $gols[$key],
                    'tunjab' => 0,
                    'jab' => $jabatans[$key],
                    'sakit' => $sakit[$key],
                    'tb' => $this->input->post('tb_' . $nips[$key]),
                    'skp' => $skp[$key],
                    'hari' => 0,
                    'jam' => 0,
                    'perilaku' => 0,
                    'hukdis' => 100,
                    'ket' => $keterangan[$key],
                    'petugas' => $nip,
                    'waktu' => $now


                    // 'verifikator' => $allIdVerifikator,
                    // 'status_ajuan' => 1,
                    // 'status_acc' => 0,
                    // 'posisi_acc' => getNipSekByKolok($myKolok),
                    // 'posted_by' => $user_id,
                    // 'nip_by' => $nip,
                    // 'skp' => $skp[$key],
                    // 'potongan' => $potongan[$key],
                    // 'created_at' => $now
                );
            }
        } else {
            //SKPD ybs blm pny verifikator
            $array_msg = array(
                'status'=>'error',
                'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        }

        // echo json_encode($data_bulk);
        // die();

        $ins_bulk = $this->Fungsi_model->tambah_bulk('simgaji_masttpp_kinerja', $data_bulk);
        if($ins_bulk){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil mengusulkan data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_history_p3k');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal mengusulkan data'
            );
            redirect('tpp_pegawai_p3k');
        }
    }
}