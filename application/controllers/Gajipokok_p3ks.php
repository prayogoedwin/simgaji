<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gajipokok_p3ks extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Gajipokok_p3ks_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $get = $this->Gajipokok_p3ks_model->getData();

        $datatable = array();
		foreach ($get as $key => $value) {

            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->gol .'</a>';

			$datatable[$key] = array(
				'id' => $klik_kode,
				'masa' => $value->masa,
				'lama' => $value->lama,
                'baru' => $value->baru,
			);
		}
		$data['datatable'] = $datatable;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/gajipokok_p3k/lokasi_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    // public function tambah(){
    //     $data_header['session'] = $this->session->all_userdata();

	// 	$this->load->view('template/head');
	// 	$this->load->view('template/header', $data_header);
	// 	$this->load->view('master/lokasi/lokasi_tambah');
    // }

    // public function tambah_action(){
    //     $kode = $this->input->post('kode', TRUE);
    //     $name = $this->input->post('name', TRUE);
    //     $is_induk = $this->input->post('is_induk', TRUE);

    //     $userid = $this->session->userdata('id');
    //     $now = date('Y-m-d H:i:s');

    //     $data = array(
    //         'kode' => $kode,
    //         'name' => $name,
    //         'bool_id' => $is_induk,
    //         'posted_by' => $userid,
    //         'created_at' => $now
    //     );

    //     $insert = $this->Fungsi_model->tambah('simgaji_lokasis', $data);
    //     if ($insert) {
    //         $array_msg = array(
    //             'status'=>'success',
    //             'message'=>'Berhasil menambahkan data'
    //         );
    //         $this->session->set_flashdata($array_msg);
    //         redirect('lokasi');
    //     } else {
    //         $array_msg = array(
    //             'status'=>'error',
    //             'message'=>'Gagal menambahkan data'
    //         );
            
    //         $this->session->set_flashdata($array_msg);
    //         redirect('lokasi');
    //     }

	// 	// header('Content-Type: application/json');
    //     // echo json_encode($data);
    // }

    public function update_action(){
        $id = $this->input->post('idgajipokok', TRUE);
        $masa = $this->input->post('masa', TRUE);
        $lama = $this->input->post('lama', TRUE);
        $baru = $this->input->post('baru', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'masa' => $masa,
            'lama' => $lama,
            'baru' => $baru,
        );

        $insert = $this->Fungsi_model->edit('simgaji_gajipokoks', $id, $data);
        if ($insert) {
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil memperbaharui data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('gajipokok');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal memperbaharui data'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('gajipokok');
        }

		// header('Content-Type: application/json');
        // echo json_encode($data);
    }

    // public function hapus_action(){
    //     $id = $this->input->post('id', TRUE);
    //     $now = date('Y-m-d H:i:s');

    //     $data = array(
    //         'deleted_at' => $now
    //     );

    //     $hps = $this->Fungsi_model->hapus('simgaji_lokasis', $id, $data);
    //     if($hps){
    //         $reponse = array(
    //             'status' => 1,
    //             'message' => 'Berhasil menghapus data'
    //         );
    //     } else {
    //         $reponse = array(
    //             'status' => 0,
    //             'message' => 'Gagal menghapus data'
    //         );
    //     }

    //     header('Content-Type: application/json');
    //     echo json_encode($reponse);
    // }

    public function getDetailGaji(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Gajipokok_p3ks_model->getDataById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
}