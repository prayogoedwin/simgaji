<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standargaji extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();
        $this->mastersotks = 'mastersotks';
        $this->masterjfts = 'masterjfts';
        $this->masterjfus = 'masterjfus';

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Standargaji_model');
	}

    public function index(){
        redirect('dashboard');
    }


    //CRUD SOTK START//
    public function get_struktural(){
        $get = $this->Standargaji_model->get_struktural();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'name' => $klik_kode,
                'kelas' => $value->kelas,
                'nominal' => number_format($value->nominal),
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(
       
            "recordsTotal" => $this->Standargaji_model->struktural_count_all(),
            "recordsFiltered" => $this->Standargaji_model->struktural_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function struktural(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Gaji - Struktural [SOTK]';
        $data['breadcrumb'] = 
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Gaji - Struktural [SOTK]</span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/standargaji/struktural/index', $data);
    }

    public function get_detail_struktural(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standargaji_model->getStrukturalById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function struktural_update_action(){
        $id = $this->input->post('id', TRUE);
        $nama = $this->input->post('name', TRUE);
        $kelas = $this->input->post('kelas', TRUE);
        $nominal = $this->input->post('nominal', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'name' => $nama,
            'kelas' => $kelas,
            'nominal' => $nominal,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->mastersotks, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function struktural_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->mastersotks, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD SOTK END//


    //CRUD JFT START//
    public function get_jft(){
        $get = $this->Standargaji_model->get_jft();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'name' => $klik_kode,
                'kelas' => $value->kelas,
                'nominal' => number_format($value->nominal),
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(
       
            "recordsTotal" => $this->Standargaji_model->jft_count_all(),
            "recordsFiltered" => $this->Standargaji_model->jft_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function jft(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Gaji - JFT';
        $data['breadcrumb'] = 
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Gaji - JFT </span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/standargaji/jft/index', $data);
    }

    public function get_detail_jft(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standargaji_model->getJftById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function jft_update_action(){
        $id = $this->input->post('id', TRUE);
        $nama = $this->input->post('name', TRUE);
        $kelas = $this->input->post('kelas', TRUE);
        $nominal = $this->input->post('nominal', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'name' => $nama,
            'kelas' => $kelas,
            'nominal' => $nominal,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->masterjfts, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function jft_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->masterjfts, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD JFT END//




    //CRUD JFU START//
    public function get_jfu(){
        $get = $this->Standargaji_model->get_jfu();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'name' => $klik_kode,
                'kelas' => $value->kelas,
                'nominal' => number_format($value->nominal),
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(
       
            "recordsTotal" => $this->Standargaji_model->jfu_count_all(),
            "recordsFiltered" => $this->Standargaji_model->jfu_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function jfu(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Gaji - JFU';
        $data['breadcrumb'] = 
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Gaji - JFU </span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/standargaji/jfu/index', $data);
    }

    public function get_detail_jfu(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standargaji_model->getJfuById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function jfu_update_action(){
        $id = $this->input->post('id', TRUE);
        $nama = $this->input->post('name', TRUE);
        $kelas = $this->input->post('kelas', TRUE);
        $nominal = $this->input->post('nominal', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'name' => $nama,
            'kelas' => $kelas,
            'nominal' => $nominal,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->masterjfus, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function jfu_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->masterjfus, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD JFYU END//
}