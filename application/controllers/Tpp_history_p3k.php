<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_history_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_history_p3k_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $periode_bulan = getDataPeriode()->bulan;
        $periode_tahun = getDataPeriode()->tahun;
        $get = $this->Tpp_history_p3k_model->getDataPeriode($periode_bulan, $periode_tahun);

        $datatable = array();
        foreach ($get as $key => $value) {
            $per = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
            if($value->periode != $per){
                $period = $value->periode;
            } else {
                $period = '<a href="'. base_url('tpp_history_p3k/periode/') . $value->periode .'">'. $value->periode .'</a>';
            }

            $datatable[$key] = array(
                'periode' => $period
            );
        }
        $data['datatable'] = $datatable;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('tpp_history_p3k/tpp_history_p3k_periode', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function periode($per){
        $data_header['session'] = $this->session->all_userdata();

        $user_id = $this->session->userdata('id');
        $A_01 = $this->session->userdata('A_01');
        $data['detail_byperiode'] = $this->Tpp_history_p3k_model->getStatusAllDataByPeriode($per, $user_id);

        $get = $this->Tpp_history_p3k_model->getDataHistory($A_01, $per, $user_id);

        $datatable = array();
		foreach ($get as $key => $value) {

            $sts_acc = '-';
            if($value->status_acc == 0){
                $sts_acc = '<span class="badge badge-warning">Menunggu</span>';
            } else if($value->status_acc == 1){
                $sts_acc = '<span class="badge badge-success">Disetujui</span>';
            } else if($value->status_acc == 9) {
                $sts_acc = '<span class="badge badge-success">Proses Data Pada BKD</span>';
            } else {
                $sts_acc = '<span class="badge badge-danger">Direvisi: '.$value->keterangan.'</span>';
            }

            $datatable[$key] = array(
                'no' => $key + 1,
				'periode' => $value->periode,
				'nip_nama_jabatan' => $value->nip . '</br>' . $value->nama . '</br>' . $value->jabatan,
                'status' => $sts_acc,
                'waktu' => $value->updated_at
			);
        }
        $data['datatable'] = $datatable;

        $getTop = $this->Tpp_history_p3k_model->getDataByKasubbagKeu($per, $user_id);
        $datatable_top = array();
		foreach ($getTop as $key => $value) {

            
            if($value->updated_at == null){
                $ac = '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="hapusAjuan('. $value->id .')"><i class="icon-trash"></i></a>';
            } else {
                $ac = '-';
            }
            
            $klik_nip = '<a href="'. base_url('history_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
            
			$datatable_top[$key] = array(
                'no' => $key+1,
                'nip' => $klik_nip,
                'nama' => $value->nama,
                'act' => $ac
			);
		}
        $data['datatable_top'] = $datatable_top;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
        $this->load->view('tpp_history_p3k/tpp_history_p3k_index', $data);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function ajukan_action(){
        $role = $this->session->userdata('role');
        $user_id = $this->session->userdata('id');
        $id_lokasis = $this->session->userdata('id_lokasis');
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');

        $A_01 = $this->session->userdata('A_01');
        $B_02B = $this->session->userdata('B_02B');
        $B_03A = getDataMastfip($B_02B)->B_03A;
        $B_03 = getDataMastfip($B_02B)->B_03;
        $B_03B = getDataMastfip($B_02B)->B_03B;

        $kodeSekretaris = cari_kode_sekertaris($A_01);
        $nipSekretaris = getNipByKolok($kodeSekretaris);
        $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
        $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
        $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
        $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;

        //add notif
        tambahNotif('8',null, base_url('approval_usulan_tpp_p3k'), 'approval_usulan_tpp_p3k', 'TPP Pegawai P3K', 'Usulan data TPP pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipSekretaris, '0');

        //START add history acc
        $data_acc = array(
            'A_01' => $A_01,
            'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
            'nip' => $nipSekretaris,
            'nama' => $namaSekretaris,
            'jabatan' => 'SEKRETARIS',
            'status_acc' => 0,
            'is_plt' => 0,
            'created_at' => $now,
            'posted_by' => $user_id
        );
        $this->Fungsi_model->tambah('simgaji_historyp3ks_tpp_acc', $data_acc);
        //END add history acc

        $data = array(
            'status_ajuan' => 2,
            'posisi_acc' => $nipSekretaris,
            'status_acc_bkd' => 0,
            'updated_at' => $now
        );

        $upd = $this->Tpp_history_p3k_model->updateDataUsulanByPeriode('simgaji_historyp3ks_tpp', $data, $periode, $user_id);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
}