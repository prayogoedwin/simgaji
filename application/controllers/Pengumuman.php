<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Pengumuman_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $get = $this->Pengumuman_model->getData();

        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            
            $action =  
                    '<div class="text-center">
                        <a href="'. base_url('pengumuman/detail/') .  encode_url($value->id) .'" class="btn btn-outline bg-primary border-primary text-primary btn-icon rounded-round legitRipple"><i class="icon-file-text"></i></a>
                        <a href="javascript:void(0)" onclick="hapusData('.$value->id.')" class="btn btn-outline bg-pink-400 border-pink-400 text-pink-800 btn-icon rounded-round legitRipple"><i class="icon-trash"></i></a>
                    </div>';


			$datatable[$key] = array(
				'pengumuman' => $value->pengumuman,
				'waktu' => $value->created_at,
				'aksi' => $action
			);
		}
		$data['datatable'] = $datatable;

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('pengumuman/pengumuman_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    function upload_image(){
        $csrfName = $this->security->get_csrf_token_name(); 
        $csrfHash = $this->security->get_csrf_hash(); 
        if(isset($_FILES["image"]["name"])){
            $config['upload_path']   = './assets/pengumuman';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['max_size'] = '5024';
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('image')){
                $this->upload->display_errors();
                return FALSE;
            }else{
                $data = $this->upload->data();
                $response = array(
                    'status' => 1,
                    'message' => 'Berhasil Hapus Data',
                    'data' =>  base_url().'assets/pengumuman/'.$data['file_name']
                );
                $response[$csrfName] = $csrfHash;
                header('Content-Type: application/json');
                echo json_encode($response);

            }
        }
    }

    function upload_file(){


        if(isset($_FILES["image"]["name"])){
            $config['upload_path']   = './assets/pengumuman/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '5024';
            // $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('image')){
                // echo 'asd';
                // $this->upload->display_errors();
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal upload File PDF'
                );
                $this->session->set_flashdata($array_msg);
                redirect('pengumuman/tambah');

            }else{
               
                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil upload File PDF'
                );
                $this->session->set_flashdata($array_msg);
                redirect('pengumuman/tambah');
                // $response[$csrfName] = $csrfHash;
                // header('Content-Type: application/json');
                // echo json_encode($response);

            }
        }
    }

    function upload_file_inedit($uri){


        if(isset($_FILES["image"]["name"])){
            $config['upload_path']   = './assets/pengumuman/';
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '5024';
            // $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
            if(!$this->upload->do_upload('image')){
                // echo 'asd';
                // $this->upload->display_errors();
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal upload File PDF'
                );
                $this->session->set_flashdata($array_msg);
                redirect('pengumuman/detail/'.encode_url($uri));

            }else{
               
                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil upload File PDF'
                );
                $this->session->set_flashdata($array_msg);
                redirect('pengumuman/detail/'.encode_url($uri));
                // $response[$csrfName] = $csrfHash;
                // header('Content-Type: application/json');
                // echo json_encode($response);

            }
        }
    }

    public function unlink_path($file){

        $this->load->helper("file");
        $path = './assets/pengumuman/'.$file;
        unlink($path); 
        $array_msg = array(
            'status'=>'success',
            'message'=>'Berhasil hapus File PDF'
        );
        $this->session->set_flashdata($array_msg);
        redirect('pengumuman/tambah');
    }

    public function unlink_path_in_edit($file, $id){

        $this->load->helper("file");
        $path = './assets/pengumuman/'.$file;
        unlink($path); 
        $array_msg = array(
            'status'=>'success',
            'message'=>'Berhasil hapus File PDF'
        );
        $this->session->set_flashdata($array_msg);
        redirect('pengumuman/detail/'.encode_url($id));
    }

    public function tambah(){
        $data_header['session'] = $this->session->all_userdata();

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('pengumuman/pengumuman_tambah');
    }

    public function tambah_action(){
		$pengumuman = $this->input->post('pengumuman', TRUE);
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'pengumuman' => $pengumuman,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $insert = $this->Fungsi_model->tambah('simgaji_pengumuman', $data);
        if ($insert) {
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil menambahkan data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('pengumuman');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal menambahkan data'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('pengumuman');
        }

		// header('Content-Type: application/json');
        // echo json_encode($data);
	}

    public function detail($id){
        $real_id = decode_url($id);
		$cek = cek_realparams('simgaji_pengumuman', $real_id);
		if($cek){
			$data_header['session'] = $this->session->all_userdata();

			$data['detail'] = $this->Pengumuman_model->getDetailById($real_id);
	
			$this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('pengumuman/pengumuman_detail', $data);
	
			// header('Content-Type: application/json');
			// echo json_encode($data);
		} else {
			$array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('pengumuman');
		}
    }

    public function detail_action(){
        $id = $this->input->post('idne', TRUE);
        $pengumuman = $this->input->post('pengumuman', TRUE);
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'pengumuman' => $pengumuman,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $insert = $this->Fungsi_model->edit('simgaji_pengumuman', $id, $data);
        if ($insert) {
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil memperbaharui data'
            );
            $this->session->set_flashdata($array_msg);
            // redirect('pengumuman');
            redirect('pengumuman/detail/'.encode_url($id));
        } else {
            
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal memperbaharui data'
            );
            
            $this->session->set_flashdata($array_msg);
            // redirect('pengumuman');
            redirect('pengumuman/detail/'.encode_url($id));
        }

		// header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus('simgaji_pengumuman', $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
}