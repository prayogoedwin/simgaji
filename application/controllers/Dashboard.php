<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		// $this->load->model('Fungsi_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
		$data['pengumuman'] = $this->db->query("SELECT * FROM simgaji_pengumuman WHERE deleted_at IS NULL")->result();

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('dashboard/dashboard_index', $data);
		// jal push leno
		// echo json_encode($this->session->all_userdata());
	}

	function open_notif($encode_id){
		$id = decode_url($encode_id);
		$get = $this->db->query("SELECT * FROM inbox2 WHERE id = $id")->row();

		$data = array(
		  'is_open' => 1
		);

		$this->db->where('id', $id);
		$this->db->update('inbox2', $data);
		redirect(base_url($get->url_web));
	}

	public function jajal(){
		$B_02B = $this->session->userdata('B_02B');
		echo getMyLokasi($this->session->userdata('A_01'), getDataMastfip($B_02B)->A_02, getDataMastfip($B_02B)->A_03, getDataMastfip($B_02B)->A_04, getDataMastfip($B_02B)->A_05);
		// echo getDataMastfip($B_02B)->A_01;
		// echo json_encode(getMyVerifikator($myCabdinKolok));
	}
}
