<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Rekap_p3k_model');
	}

	public function index(){
		$data_header['session'] = $this->session->all_userdata();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('rekap_p3k/rekap_p3k_index');
	}

	public function getDataPeriode(){
        $user_id = $this->session->userdata('id');

        $query  = 
        "SELECT DISTINCT(periode)
        FROM `simgaji_historyp3ks`";

        $search = array('periode');
        $where  = null;
        
        // jika memakai IS NULL pada where sql
        $isWhere = null;
        $isGroupBy = null;


        header('Content-Type: application/json');
        echo $this->Rekap_p3k_model->getDataPeriode($query,$search,$where,$isWhere, $isGroupBy);
    }

	public function periode(){
		$data_header['session'] = $this->session->all_userdata();

        // $A_01 = $this->session->userdata('A_01');
        $user_id = $this->session->userdata('id');

        $periode = $this->uri->segment(3);

		$data['rekap'] = $this->Rekap_p3k_model->getDataByPeriode($user_id, $periode);
        $data['kumulatif'] = $this->Rekap_p3k_model->getDataKumulatifByPeriode($user_id, $periode);

        $sum_belumdiproses = 0;
        $sum_ditolak = 0;
        $sum_disetujui = 0;
        foreach($data['kumulatif'] as $i=>$valkum){
            $rev_tolak = $valkum->revisi + $valkum->tolak;
            
            $sum_belumdiproses += $valkum->belum;
            $sum_ditolak += $rev_tolak;
            $sum_disetujui += $valkum->setuju;
        }

        $data['sum_kumulatif'] = array(
            'blm' => $sum_belumdiproses,
            'tlk' => $sum_ditolak,
            'stj' => $sum_disetujui
        );

        // $datatable = array();
		// foreach ($get as $key => $value) {
            
        //     $datatable[$key] = array(
        //         'no' => $key + 1,
		// 		'nip' => $value->nip,
		// 		'nama' => $value->name
		// 	);
        // }
        // $data['datatable'] = $datatable;

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
        $this->load->view('rekap_p3k/rekap_p3k_periode', $data);

        // echo json_encode($sum_kumulatif);
	}
}