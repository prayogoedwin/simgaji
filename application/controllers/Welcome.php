<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('template/head');
		$this->load->view('template/header');
		$this->load->view('sample/index');
	}


	public function cek_kalkulasi(){
		$data['lama'] = $this->db->query("SELECT * FROM kalkulasis where id=7937064")->row();
		$data['baru'] = $this->db->query("SELECT * FROM simgaji_kalkulasis where id=203")->row();
		
		echo json_encode($data);

		
	}

	public function cek(){
		$data['lama'] = $this->db->query("SELECT * FROM kalkulasis where id=7937064")->row();
		$data['baru'] = $this->db->query("SELECT * FROM simgaji_kalkulasis where id=203")->row();
		
		$this->load->view("cek", $data);

		
	}


}
