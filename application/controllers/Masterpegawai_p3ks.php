<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Masterpegawai_p3ks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->pegawais = 'simgaji_pegawaip3ks';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Masterpegawai_p3ks_model');
        $this->load->model('History_p3k_model');
        $this->load->model('Pegawai_p3k_model');

        $this->tb_pegawai = 'simgaji_pegawaip3ks';
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Master Pegawai P3K';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Master Pegawai P3K</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();
        $nip = $this->session->userdata('B_02B');
        // echo $nip;
        // die();
        $data['verifikator'] = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip ='$nip'");

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_p3k/index', $data);
    }

    //CRUD users START//
    public function get_pegawai()
    {
        // if($session['role'] == 2);

        $role = $this->session->userdata('role');

       
        
        

        $nip =  $this->session->userdata('B_02B');
        if($role == 1){
            $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE id = '$nip' AND deleted_at IS NULL");
        }else{
            $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$nip' AND deleted_at IS NULL");
        }

        // echo json_encode($x->row());
        // die();
       
        if($x->num_rows() > 0){
            $x = $x->row();
            // $role = $x->type_role;
            $skpd = $x->kode_skpd;
            $skpd_id = $x->id_skpd;
        }else{
            // $role = '';
            $skpd = '';
            $skpd_id = '';
        }

        
        // echo $skpd;
        // // echo substr($skpd,0,6);
        // die();

        // if($role == 2){
        //     $get = $this->Masterpegawai_p3ks_model->get_pegawai_where($skpd);
        // }else{
        //     $get = $this->Masterpegawai_p3ks_model->get_pegawai();
        // }

        if($role == 1){
           $get = $this->Masterpegawai_p3ks_model->get_pegawai();
        }else{
            $get = $this->Masterpegawai_p3ks_model->get_pegawai_where($skpd); 
            // echo json_encode($get);
            // die();
        }

        $datatable = array();
        foreach ($get as $key => $value) {

            if($value->status_id == 2){
                $bk = '<span>V</span>';
            }else{
                $bk = '<span style="color:red">X</span>';
            }

     
            // $merah = $value->tolak + $value->revisi;
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a target="BLANK" href="' . base_url('verifikasi_p3ks/detail/') . encode_url($value->pegawai_id) . '" >' . $value->B_02B . '</a>';
            // $klik_kode = '<a href="javascript:void(0)" onclick="detailData(' . $value->id . ')">' . $value->nip . '</a>';


            $x = encode_url($value->id);
            $klik_kode = '<a href="'.BASE_URL('masterpegawai_p3ks/data/'.$x).'">' . $value->nip . '</a>';
            $datatable[$key] = array(
                'nip' => $klik_kode,
                'nama' => $value->name,
                'lokasikerja' => $value->lokasi_kerja,
                'lokasigaji' => $value->lokasi_gaji,
                'status' => $bk
                // 'id' => '<input type="checkbox" class="data-check" value="' . $value->id . '">',
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$(add),
            );
            
        }
        // $data['datatable'] = $datatable;

        if($role == 1){
            
            $output = array(
                "recordsTotal" => $this->Masterpegawai_p3ks_model->pegawai_count_all(),
                "recordsFiltered" => $this->Masterpegawai_p3ks_model->pegawai_count_filtered(),
                "data" => $datatable,
            );
        }else{

            $output = array(
                "recordsTotal" => $this->Masterpegawai_p3ks_model->pegawai_count_all_skpd($skpd),
                "recordsFiltered" => $this->Masterpegawai_p3ks_model->pegawai_count_filtered_skpd($skpd),
                "data" => $datatable,
            );
            
        }

        

        // $output = array(

        //     "recordsTotal" => array(),
        //     "recordsFiltered" => array(),
        //     "data" => $datatable,
        // );


        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function add(){
        // $real_id = decode_url($id);
        $cek = TRUE;
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

                // $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
                $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();
                $data['kodes2'] = $this->Pegawai_p3k_model->getDataKodes2();
                // echo json_encode($data);
                // die();

                $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
                $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
                $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
                $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
                $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
                $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
                $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
                $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
                $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
                $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
                $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
                $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');
                
                $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
                $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
                // $data['masterjfu'] = array();
                // $data['masterjft'] = array();
                // $data['mastersotk'] = array();
                // $data['mastersotk'] = array();
            
                $this->load->view('template/head');
                $this->load->view('template/header', $data_header);
                $this->load->view('master/pegawai_p3k/add', $data);

                // header('Content-Type: application/json');
                // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('masterpegawai_p3ks');
        }
    }

    public function data($id){
        $real_id = decode_url($id);
        $cek = cek_realparams('simgaji_pegawaip3ks', $real_id);
        if($cek){
            $this->lanjutData($real_id);
        } else {

            $cek_cpns = cek_realparams('simgaji_pegawais', $real_id);
            if($cek_cpns){
                $this->lanjutDataCpns($real_id);
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Invalid parameter'
                );

                $this->session->set_flashdata($array_msg);
                redirect('masterpegawai_p3ks');
            }
        }
    }

    private function lanjutData($real_id){
        $data_header['session'] = $this->session->all_userdata();

        $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
        $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();

        $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
        $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
        $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
        $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
        $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
        $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
        $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
        $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
        $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
        $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
        $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

        $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
        $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
        // $data['masterjfu'] = array();
        // $data['masterjft'] = array();
        // $data['mastersotk'] = array();
        // $data['mastersotk'] = array();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_p3k/data_pegawai', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    private function lanjutDataCpns($real_id){
        $data_header['session'] = $this->session->all_userdata();

        $data['detail'] = $this->Pegawai_model->getDetailById($real_id);
        $data['kodes'] = $this->Pegawai_model->getDataKodes();

        $data['lokasi'] = $this->Pegawai_model->getDataDataMaster('simgaji_lokasis');
        $data['gender'] = $this->Pegawai_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Pegawai_model->getDataDataMaster('simgaji_agamas');
        $data['marital'] = $this->Pegawai_model->getDataDataMaster('simgaji_maritals');
        $data['bool'] = $this->Pegawai_model->getDataDataMaster('simgaji_bools');
        $data['statusp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_statusp3ks');
        $data['status'] = $this->Pegawai_model->getDataDataMaster('simgaji_statuses');
        $data['golongan'] = $this->Pegawai_model->getDataDataMaster('simgaji_golongans');
        $data['golonganp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_golonganp3ks');
        $data['eselon'] = $this->Pegawai_model->getDataDataMaster('simgaji_eselons');
        $data['pendidikan'] = $this->Pegawai_model->getDataDataMaster('simgaji_pendidikans');
        $data['kedudukan'] = $this->Pegawai_model->getDataDataMaster('simgaji_kedudukans');

        $data['fungsional'] = $this->Pegawai_model->getDataDataMaster('simgaji_fungsionals');
        $data['profesi'] = $this->Pegawai_model->getDataDataMaster('simgaji_profesis');
        // $data['masterjfu'] = array();
        // $data['masterjft'] = array();
        // $data['mastersotk'] = array();
        // $data['mastersotk'] = array();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_p3k/data_pegawai', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    

   # fungsi untuk mengecek status username dari db
	function cek_nip(){
		# ambil username dari form
		$nip = $_POST['nip'];
						# select ke model member username yang diinput user
		$hasil_nip = $this->Masterpegawai_p3ks_model->cek_nip($nip);
	
		if($hasil_nip == 1){
			echo "1";
		}else{
			echo "0";
		}

    }

    public function cek_nip_backend()
	{
		$nip = $this->input->post('nip');
        $this->db->where('nip', $nip);
        $cek = $this->db->get($this->pegawais);
		if ($cek->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

    public function add_action(){

        $nip = $this->input->post('nip');
        if($this->cek_nip_backend($nip) == 1){

            $array_msg = array(
                'status'=>'error',
                'message'=>'NIP sudah pernah di daftarkan'
            );
            $this->session->set_flashdata($array_msg);
            redirect('masterpegawai_p3ks/add');
            die();

        }

        foreach ($_POST as $key => $value) {
            $data[htmlspecialchars($key)] = htmlspecialchars($value); 
        }
        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);
        $x = $this->db->insert($this->tb_pegawai, $merge);

        $bulan = getDataPeriode()->bulan;
        $tahun = getDataPeriode()->tahun;

        $periode = $tahun.'-'.$bulan.'-01';
        
        $pegawai_id = $this->db->insert_id();
        if($x){
            $arr = array(
                'nip_verifikator' => $this->session->userdata('B_02B'),
                'ip_address' => $this->input->ip_address(),
                'log_by'  => $this->session->userdata('id'),
                'log_at'  => date('Y-m-d H:i:s'),
                'periode' => $periode
            );
            $merge2 = array_replace($arr, $merge);
            $this->db->insert('simgaji_pegawaip3ks_log', $merge2);
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Tambah Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Tambah Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action(){


        $pegawai_id = $this->input->post('id');
        foreach ($_POST as $key => $value) {
            $data[htmlspecialchars($key)] = htmlspecialchars($value); 
        }

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_3(){


        $pegawai_id = $this->input->post('id');
        $kode_id = $this->input->post('kode_id', TRUE);
        $nip = $this->input->post('nip', TRUE);

        // echo json_encode($kode_id);
        // die();

        // if(count($kode_id) > 0){
        // $datahistory = array();

        // $bulan = getDataPeriode()->bulan;
        // $tahun = getDataPeriode()->tahun;
        // $periode = $tahun.'-'.$bulan.'-01';

        //     foreach ($kode_id as $key => $value) {
        //         $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);
        //         $nilai_lama = $this->Pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;
        //         $nilai_baru = $this->input->post($field_name, TRUE);

        //         // $datahistory[$key] = array(
        //         //     'nip_verifikator' => $this->session->userdata('B_02B'),
        //         //     'nip_ybs' => $nip,
        //         //     'lama' => $nilai_lama,
        //         //     'baru' => $nilai_baru,
        //         //     'ip_address' => $this->input->ip_address(),
        //         //     'created_by'  => $this->session->userdata('id'),
        //         //     'created_at'  => date('Y-m-d H:i:s'),
        //         //     'periode'   => $periode,
        //         //     'kode_id'   => $kode_id
        //         // );

        //         $datahistory = array(
        //             'nip_verifikator' => $this->session->userdata('B_02B'),
        //             'nip_ybs' => $nip,
        //             'lama' => $nilai_lama,
        //             'baru' => $nilai_baru,
        //             'ip_address' => $this->input->ip_address(),
        //             'created_by'  => $this->session->userdata('id'),
        //             'created_at'  => date('Y-m-d H:i:s'),
        //             'periode'   => $periode,
        //             'kode_id'   => $kode_id
        //         );

        //         $this->db->insert('simgaji_log_verifikator', $datahistory);
        //     }
        // }
        // $ins = $this->Fungsi_model->tambah_bulk('simgaji_log_verifikator', $datahistory);

        $pegawai_id = $this->input->post('id');
        foreach ($_POST as $key => $value) {
           
                $data[htmlspecialchars($key)] = htmlspecialchars($value);  
           
        }

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode,
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('masterpegawai_p3ks/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_2(){

        error_reporting(0);
        $pegawai_id = $this->input->post('id');
        $kode_id = $this->input->post('kode_id', TRUE);
        $data = [];
        $data2 = [];
        $update = [];

        foreach ($_POST as $key => $value) {

            
            $asli = substr($key,0,5);
            if(substr($key,0,5) != 'asli_'){
                // $data[$key] = $value; 
                $data[htmlspecialchars($key)] = htmlspecialchars($value); 

            }else if(substr($key,0,5) == 'asli_'){
                $data2[$key] = $value; 
                $data2[htmlspecialchars($key)] = htmlspecialchars($value); 
                echo $data2[$key][$value];
            }
        }


        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_2_proses(){

        error_reporting(0);
        $pegawai_id = $this->input->post('id');
        $data = [];
        $data2 = [];
        $update = [];

        foreach ($_POST as $key => $value) {

            $x = cekDataPns('simgaji_pegawaip3ks', $pegawai_id);
            // echo $x->nip;
            // echo '<br/>';
            // echo $x->$key;
            if($x->$key != '1961020120212110wer'){
                // echo 'asli<br/>';
                // echo $x->$key.'<br/>';
                // echo 'perubahan<br/>';
                // echo $x->$key.'<br/>';

                // $update[$key] = $value;
                // break;
            }else{
                '';
            }
            //     echo $x->$value;
            // }
            // $asli = substr($key,0,5);
            // if(substr($key,0,5) != 'asli_'){
            //     $data[$key] = $value; 
                

            // }else if(substr($key,0,5) == 'asli_'){
            //     $data2[$key] = $value; 
            //     // $data2[htmlspecialchars($key)] = htmlspecialchars($value); 
            //     // echo $data2[$key][$value];
            // }
            // // echo $data2[$key][$value];
                
            //     // die();
            //     if($data2[$value] != $data[$value]){

            //         // echo $data[$key][$value];
            //         // echo '<br/>';
            //         // echo $data2[$key][$value];
            //         $update = 1;
            //         // break;
            //     }

              
            
            
        }
        // echo json_encode($data);

        // echo json_encode($update);
        die();

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_0(){
        $user_id = $this->session->userdata('id');
        // $id_lokasis = $this->session->userdata('id_lokasis');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $now = date('Y-m-d H:i:s');

        if(count($kode_id) > 0){
            $datahistory = array();
            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);

                $nilai_lama = $this->Pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;

                $nilai_baru = $this->input->post($field_name, TRUE);

                $A_01 = $this->session->userdata('A_01');
                $A_02 = $this->session->userdata('A_02');

                if($A_01 == 'D0' && $A_02 != '00'){
                    //bukan SKPD induk, turunan disdik prov. jenjang here

                    // $kodeumpeg_atasan = getKodeCabdinSekolah($A_01, $A_02, $A_03, $A_04, $A_05);
					// $lokasi = getNalokFromKolok($kodeumpeg_atasan);

					// $nip_acc = getNipUmpegCabdinByLokasi($posisi_acc);

                    $nip = $this->input->post('nipe', TRUE);
                    $nama = $this->input->post('nama', TRUE);
                    $jabatan = $this->input->post('jabatan', TRUE);

                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
                    $myCabdinKolok = getKolokCabdin($myKolok);
                    $fix_myCabdinKolok = getKolokCabdin($myCabdinKolok); //fix perubahan tablokb08

                    $dataMyVerifikator = getMyVerifikator($fix_myCabdinKolok);
                    if($dataMyVerifikator != null){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => 'Data berhasil diusulkan',
                            'verifikator' => $dataMyVerifikator->id,
                            'status_ajuan' => 0,
                            'posisi_acc' => $nip,
                            'posted_by' => $user_id,
                            'created_at' => $now
                        );
                    } else {
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'Sekolah / Cabdin anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }

                } else {
                    //SKPD induk prov
                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

                    // $dataMyVerifikator = getMyVerifikator($myKolok);

                    $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
                    foreach ($dataAllMyVerifikator as $key => $valVerif) {
                        $idVerif[] = $valVerif->id;
                    }
                    $allIdVerifikator = implode(",", $idVerif);
                    if(count($dataAllMyVerifikator) > 0){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => 'Data berhasil diusulkan',
                            // 'verifikator' => $dataMyVerifikator->id,
                            'verifikator' => $allIdVerifikator,
                            'status_ajuan' => 1,
                            // 'posisi_acc' => $dataMyVerifikator->nip,
                            'posisi_acc' => getNipSekByKolok($myKolok),
                            'posted_by' => $user_id,
                            'created_at' => $now
                        );
                    } else {
                        //SKPD ybs blm pny verifikator
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }
                }

            }

            // echo json_encode($datahistory);

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_historyp3ks', $datahistory);
            if($ins){
                //START add history acc ke verifikator
                // $data_acc = array(
                //     'pegawais_id' => $pegawai_id,
                //     'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                //     'nip' => $dataMyVerifikator->nip,
                //     'nama' => $dataMyVerifikator->nama,
                //     'jabatan' => 'Verifikator',
                //     'status_acc' => 0,
                //     'is_plt' => 0,
                //     'created_at' => $now
                // );
                // $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
                //END add history acc ke verifikator

                //add notif inbox2 for verifikator
                // $A_01 = $this->session->userdata('A_01');
                // $B_02B = $this->session->userdata('B_02B');
                // $B_03A = getDataMastfip($B_02B)->B_03A;
				// $B_03 = getDataMastfip($B_02B)->B_03;
				// $B_03B = getDataMastfip($B_02B)->B_03B;
                // tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Ada usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $dataMyVerifikator->nip, '0');

                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('history_p3k');
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('pegawai_p3k');
            }
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Pastikan ada data yang dipilih'
            );

            $this->session->set_flashdata($array_msg);
            redirect('pegawai_p3k/detail/' . encode_url($pegawai_id));
        }

    }

    public function hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus($this->tb_pegawai, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

}
?>