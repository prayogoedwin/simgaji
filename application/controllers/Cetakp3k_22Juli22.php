<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cetakp3k extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();

        // if(!isset($lgn['B_02B'])) {
        // 	redirect('/', 'refresh');
        // }

        $this->load->model('Fungsi_model');

        $this->tb_lokasi = 'simgaji_lokasis';
        $this->tb_kalkulasi = 'simgaji_kalkulasip3ks';
        $this->tb_pegawai = 'simgaji_pegawaip3ks';
        $this->tb_tjumum = 'simgaji_tjumump3ks';
    }

    public function cek()
    {

        $this->load->view('cetakp3k/index/cek');
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Cetak';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Cetak</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('cetakp3k/index/cetak_index', $data);
    }


    public function gaji_bulanan()
    {

        $data_header['session'] = $this->session->all_userdata();
        $data['session'] = $this->session->all_userdata();
        $data['title'] = 'Cetak Gaji Bulanan';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Cetak Gaji Bulanan</span>';
        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        // $this->load->view('cetakp3k/gaji_bulanan_index', $data);
        $this->load->view('cetakp3k/index/gaji_bulanan', $data);
    }

    public function gaji_13()
    {

        $data_header['session'] = $this->session->all_userdata();
        $data['session'] = $this->session->all_userdata();
        $data['title'] = 'Cetak Gaji 13';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Cetak Gaji 13</span>';
        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        // $this->load->view('cetakp3k/gaji_bulanan_index', $data);
        $this->load->view('cetakp3k/index/gaji_13', $data);
    }


    function gaji_action()
    {
        $periode = $_POST['tahun'];
        if (isset($_POST['bulan_id'])) {
            $bulan = $_POST['bulan_id'];
            if ($_POST['bulan_id'] < 10) {
                $bulan = $_POST['bulan_id'];
            }
            $periode .= "-" . $bulan . "-01";
        }

        if (isset($_POST['nip'])) {
            $nip = $_POST['nip'];
        }

        $start = $_POST['lokasi_start'];
        $end = $_POST['lokasi_end'];

        if (isset($_POST['lokasi_start']) || isset($_POST['lokasi_end'])) {
            $arrLokasi = array();
            $arrLokasiInduk    = array();

            if ($_POST['lokasi_end']) {

                $lokasi_start = get_lokasi($start);
                $lokasi_end =  get_lokasi($end);

                // $lokasi_start = ORM::factory('lokasi',$_POST['lokasi_start']);
                // $lokasi_end = ORM::factory('lokasi',$_POST['lokasi_end']);

                if (substr($lokasi_start->name, 0, 4) == "BIRO") {

                    $this->db->where('LEFT(kode,6)', substr($lokasi_end, 0, 6));
                    $this->db->order_by('kode', 'DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
                    // $max = ORM::factory('lokasi')
                    //     ->where('LEFT("kode",6)','=',substr($lokasi_end->kode,0,6))
                    //     ->order_by('kode','DESC')
                    //     ->find();
                } else {

                    $this->db->like('kode', $lokasi_end->kode, 'after');
                    $this->db->order_by('kode', 'DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
                    // $max = ORM::factory('lokasi')
                    //     ->where('kode','LIKE',$lokasi_end->kode."%")
                    //     ->order_by('kode','DESC')
                    //     ->find();	

                }

                $this->db->where('kode >=', $lokasi_start->kode);
                $this->db->where('kode <=', $max->kode);
                $this->db->order_by('kode', 'ASC');
                $lokasis = $this->db->get($this->tb_lokasi)->result();


                foreach ($lokasis as $lokasi) {
                    array_push($arrLokasi, $lokasi->id);
                }

                // $lokasis = ORM::factory('lokasi')
                //     ->where('kode','>=',$lokasi_start->kode)
                //     ->where('kode','<=',$max->kode)
                //     ->order_by('kode','ASC')
                //     ->find_all();

                // foreach($lokasis as $lokasi) {
                //     array_push($arrLokasi,$lokasi->id);
                // }	

                if (substr($lokasi_start->name, 0, 4) == "BIRO") {
                    // $lokasis = ORM::factory('lokasi')
                    //     ->where('kode','>=',$lokasi_start->kode)
                    //     ->where('kode','<=',$lokasi_end->kode)
                    //     ->order_by('kode','ASC')
                    //     ->find_all();
                    $this->db->where('kode >=', $lokasi_start->kode);
                    $this->db->where('kode <=', $lokasi_end->kode);
                    $this->db->order_by('kode', 'ASC');
                    $lokasis = $this->db->get($this->tb_lokasi)->result();
                } else {
                    // $lokasis = ORM::factory('lokasi')
                    // ->where('kode','>=',$lokasi_start->kode)
                    // ->where('kode','<=',$lokasi_end->kode)
                    // ->where('RIGHT("kode",6)','=','000000')
                    // ->order_by('kode','ASC')
                    // ->find_all();
                    $this->db->where('kode >=', $lokasi_start->kode);
                    $this->db->where('kode <=', $lokasi_end->kode);
                    $this->db->where('RIGHT("kode",6)', 000000);
                    $this->db->order_by('kode', 'ASC');
                    $lokasis = $this->db->get($this->tb_lokasi)->result();
                }

                foreach ($lokasis as $lokasi) {
                    array_push($arrLokasiInduk, $lokasi->id);
                }
            } else {
                // $lokasi_start = ORM::factory('lokasi',$_POST['lokasi_start']);
                $lokasi_start = get_lokasi($start);

                if (substr($lokasi_start->kode, -6) == "000000") {
                    if (substr($lokasi_start->kode, 0, 2)     == "34") {
                        // $lokasis = ORM::factory('lokasi')
                        //     ->where('LEFT("kode",4)','=',substr($lokasi_start->kode,0,4))
                        //     ->order_by('kode','ASC')
                        //     ->find_all();	
                        $this->db->where('LEFT(kode,4)', substr($lokasi_start->kode, 0, 4));
                        $this->db->order_by('kode', 'ASC');
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
                    } else {
                        // $lokasis = ORM::factory('lokasi')
                        //     ->where('LEFT("kode",2)','=',substr($lokasi_start->kode,0,2))
                        //     ->order_by('kode','ASC')
                        //     ->find_all();
                        $this->db->where('LEFT(kode,2)', substr($lokasi_start->kode, 0, 2));
                        $this->db->order_by('kode', 'ASC');
                        $lokasis = $this->db->get($this->tb_lokasi)->result();

                    }
                } else {
                    // $lokasis = ORM::factory('lokasi')
                    //     ->where('kode','LIKE',substr($lokasi_start->kode,0,6).'%')
                    //     ->order_by('kode','ASC')
                    //     ->find_all();

                    $this->db->like('kode', substr($lokasi_start->kode, 0, 6), 'after');
                    $this->db->order_by('kode', 'DESC');
                    $lokasis = $this->db->get($this->tb_lokasi)->result();
                }

                foreach ($lokasis as $lokasi) {
                    array_push($arrLokasi, $lokasi->id);
                }

                array_push($arrLokasiInduk, $_POST['lokasi_start']);
            }
        } else {
            $arrLokasi = array();
            $arrLokasiInduk = array();
        }

        $nip = "";
        if (isset($_POST['nip'])) {
            $nip = $_POST['nip'];
        }

        $arrJenis = array(
            'DAFTAR GAJI' => 'cetak_daftar_gaji',
            'REKAP GAJI PER LOKASI INDUK' => 'cetak_lokasi_induk',
            'REKAP GAJI PER LOKASI' => 'cetak_lokasi',
            'REKAP GAJI KIRI KANAN' => 'cetak_kiri_kanan',
            'REKAP GAJI LOKASI DETAIL' => 'cetak_lokasi_detail',
            'REKAP GAJI SELURUH SKPD' => 'cetak_gaji_skpd',
            'REKAP BPJS KESEHATAN' => 'cetak_askes',
            #'KEKURANGAN ASKES' => 'cetak_askes_kurang',
            #'REKAP BPJS KK' => 'cetak_bpjs',
            'REKAP BPJS JKK' => 'cetak_bpjs_jkk',
            'REKAP BPJS JKM' => 'cetak_bpjs_jkm',
            'GAJI PERSONAL' => 'cetak_daftar_personal',
            'REKAP PEGAWAI' => 'cetak_rekap_pegawai',
            'REKAP TUNJANGAN UMUM' => 'cetak_umum',
            'REKAP TUNJANGAN UMUM DETAIL' => 'cetak_umum_detail',
            'REKAP TUNJANGAN FUNGSIONAL' => 'cetak_fungsional',
            'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'cetak_fungsional_detail',
            'REKAP TUNJANGAN STRUKTURAL' => 'cetak_struktural',
            'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'cetak_struktural_detail',
            'DAFTAR TPP' => 'cetak_daftar_tpp',
            'REKAP TPP PER LOKASI INDUK' => 'cetak_tpp_induk',
            'REKAP TPP PER LOKASI' => 'cetak_tpp_lokasi',
            'REKAP TPP SELURUH SKPD' => 'cetak_tpp_skpd',
            'DAFTAR TPP KONDISI KERJA' => 'cetak_daftar_tpp_kk',
            'REKAP TPP KONDISI KERJA PER LOKASI INDUK' => 'cetak_tpp_induk_kk',
            'REKAP TPP KONDISI KERJA PER LOKASI' => 'cetak_tpp_lokasi_kk',
            'REKAP TPP KONDISI KERJA BPJS KESEHATAN' => 'cetak_konker_askes',
            'DAFTAR TPP TEMPAT BERTUGAS' => 'cetak_daftar_tpp_tb',
            'REKAP TPP TEMPAT BERTUGAS PER LOKASI INDUK' => 'cetak_tpp_induk_tb',
            'REKAP TPP TEMPAT BERTUGAS PER LOKASI' => 'cetak_tpp_lokasi_tb',
            'REKAP TPP TEMPAT BERTUGAS BPJS KESEHATAN' => 'cetak_jakarta_askes',
            'REKAP TPP BPJS KESEHATAN' => 'cetak_tpp_askes',
            'DAFTAR GAJI 13' => 'cetak_daftar_gaji_13',
            'REKAP GAJI 13 PER LOKASI INDUK' => 'cetak_lokasi_induk_13',
            'REKAP GAJI 13 PER LOKASI' => 'cetak_lokasi_13',
            'REKAP GAJI 13 KIRI KANAN' => 'cetak_kiri_kanan_13',
            'REKAP GAJI 13 LOKASI DETAIL' => 'cetak_lokasi_detail_13',
            'REKAP GAJI 13 SELURUH SKPD' => 'cetak_gaji_skpd_13',
            'DAFTAR BPJS KESEHATAN 13' => 'cetak_askes_13',
            #'KEKURANGAN ASKES 13' => 'cetak_askes_kurang_13',
            #'REKAP BPJS KK 13' => 'cetak_bpjs_13',
            'REKAP BPJS JKK 13' => 'cetak_bpjs_jkk_13',
            'REKAP BPJS JKM 13' => 'cetak_bpjs_jkm13',
            'GAJI PERSONAL 13' => 'cetak_daftar_personal_13',
            'REKAP PEGAWAI 13' => 'cetak_rekap_pegawai_13',
            'REKAP GAJI 13 UMUM' => 'cetak_umum_13',
            'REKAP GAJI 13 UMUM DETAIL' => 'cetak_umum_detail_13',
            'REKAP GAJI 13 FUNGSIONAL' => 'cetak_fungsional_13',
            'REKAP GAJI 13 FUNGSIONAL DETAIL' => 'cetak_fungsional_detail_13',
            'REKAP GAJI 13 STRUKTURAL' => 'cetak_struktural_13',
            'REKAP GAJI 13 STRUKTURAL DETAIL' => 'cetak_struktural_detail_13',
            'DAFTAR GAJI 14' => 'cetak_daftar_gaji_14',
            'REKAP GAJI 14 PER LOKASI INDUK' => 'cetak_lokasi_induk_14',
            'REKAP GAJI 14 PER LOKASI' => 'cetak_lokasi_14',
            'REKAP GAJI 14 KIRI KANAN' => 'cetak_kiri_kanan_14',
            'REKAP GAJI 14 LOKASI DETAIL' => 'cetak_lokasi_detail_14',
            'REKAP GAJI 14 SELURUH SKPD' => 'cetak_gaji_skpd_14',
            'DAFTAR BPJS KESEHATAN 14' => 'cetak_askes_14',
            #'KEKURANGAN ASKES 14' => 'cetak_askes_kurang_14',
            #'REKAP BPJS KK 14' => 'cetak_bpjs_14',
            'REKAP BPJS JKK 14' => 'cetak_bpjs_jkk_14',
            'REKAP BPJS JKM 14' => 'cetak_bpjs_jkm_14',
            'GAJI PERSONAL 14' => 'cetak_daftar_personal_14',
            'REKAP PEGAWAI 14' => 'cetak_rekap_pegawai_14',
            'REKAP GAJI 14 UMUM' => 'cetak_umum_14',
            'REKAP GAJI 14 UMUM DETAIL' => 'cetak_umum_detail_14',
            'REKAP GAJI 14 FUNGSIONAL' => 'cetak_fungsional_14',
            'REKAP GAJI 14 FUNGSIONAL DETAIL' => 'cetak_fungsional_detail_14',
            'REKAP GAJI 14 STRUKTURAL' => 'cetak_struktural_14',
            'REKAP GAJI 14 STRUKTURAL DETAIL' => 'cetak_struktural_detail_14',
            'DAFTAR TPP 13' => 'cetak_daftar_tpp_13',
            'REKAP TPP 13 PER LOKASI INDUK' => 'cetak_tpp_induk_13',
            'REKAP TPP 13 PER LOKASI' => 'cetak_tpp_lokasi_13',
            'REKAP TPP 13 SELURUH SKPD' => 'cetak_tpp_skpd_13',
            'DAFTAR TPP 13 KONDISI KERJA' => 'cetak_daftar_tpp_kk_13',
            'REKAP TPP 13 KONDISI KERJA PER LOKASI INDUK' => 'cetak_tpp_induk_kk_13',
            'REKAP TPP 13 KONDISI KERJA PER LOKASI' => 'cetak_tpp_lokasi_kk_13',
            'DAFTAR TPP 13 TEMPAT BERTUGAS' => 'cetak_daftar_tpp_tb_13',
            'REKAP TPP 13 TEMPAT BERTUGAS PER LOKASI INDUK' => 'cetak_tpp_induk_tb_13',
            'REKAP TPP 13 TEMPAT BERTUGAS PER LOKASI' => 'cetak_tpp_lokasi_tb_13',
            'PENAMBAHAN PEGAWAI' => 'cetak_tambah_pegawai',
            'UPDATE PEGAWAI' => 'cetak_update_pegawai',
            'PENAMBAHAN PEGAWAI TPP' => 'cetak_tambah_pegawai_tpp',
            'UPDATE PEGAWAI TPP' => 'cetak_update_pegawai_tpp',
            'REKAP TPP PER LOKASI INDUK NON ESELON' => 'cetak_tpp_induk_non_eselon',
            'REKAP TPP PER LOKASI INDUK ESELON' => 'cetak_tpp_induk_eselon',
            'DAFTAR GAJI TAHUNAN' => 'cetak_daftar_gaji_tahunan',
            'REKAP GAJI PER LOKASI INDUK TAHUNAN' => 'cetak_lokasi_induk_tahunan',
            'REKAP GAJI PER LOKASI TAHUNAN' => 'cetak_lokasi_tahunan',
            'REKAP GAJI KIRI KANAN TAHUNAN' => 'cetak_kiri_kanan_tahunan',
            'REKAP GAJI LOKASI DETAIL TAHUNAN' => 'cetak_lokasi_detail_tahunan',
            'REKAP GAJI SELURUH SKPD TAHUNAN' => 'cetak_gaji_skpd_tahunan',
            'DAFTAR TPP TAHUNAN' => 'cetak_daftar_tpp_tahunan',
            'REKAP TPP PER LOKASI INDUK TAHUNAN' => 'cetak_tpp_induk_tahunan',
            'REKAP TPP PER LOKASI TAHUNAN' => 'cetak_tpp_lokasi_tahunan',
            'REKAP TPP SELURUH SKPD TAHUNAN' => 'cetak_tpp_skpd_tahunan',
            'DAFTAR TPP KONDISI KERJA TAHUNAN' => 'cetak_daftar_tpp_kk_tahunan',
            'REKAP TPP KONDISI KERJA PER LOKASI INDUK TAHUNAN' => 'cetak_tpp_induk_kk_tahunan',
            'REKAP TPP KONDISI KERJA PER LOKASI TAHUNAN' => 'cetak_tpp_lokasi_kk_tahunan',
            'DAFTAR TPP TEMPAT BERTUGAS TAHUNAN' => 'cetak_daftar_tpp_tb_tahunan',
            'REKAP TPP TEMPAT BERTUGAS PER LOKASI INDUK TAHUNAN' => 'cetak_tpp_induk_tb_tahunan',
            'REKAP TPP TEMPAT BERTUGAS PER LOKASI TAHUNAN' => 'cetak_tpp_lokasi_tb_tahunan',
            'DAFTAR GAJI SUSULAN' => 'cetak_daftar_gaji_susulan',
            'REKAP GAJI SUSULAN PER LOKASI INDUK' => 'cetak_lokasi_induk_susulan',
            'REKAP GAJI SUSULAN PER LOKASI' => 'cetak_lokasi_susulan',
            'REKAP GAJI SUSULAN KIRI KANAN' => 'cetak_kiri_kanan_susulan',
            'REKAP GAJI SUSULAN LOKASI DETAIL' => 'cetak_lokasi_detail_susulan',
            'REKAP GAJI SUSULAN SELURUH SKPD' => 'cetak_gaji_skpd_susulan',
            'DAFTAR ASKES SUSULAN' => 'cetak_askes_susulan',
            'REKAP BPJS KK SUSULAN' => 'cetak_bpjs_susulan',
            'REKAP TPP 13 PER LOKASI INDUK NON ESELON' => 'cetak_tpp_13_induk_non_eselon',
            'REKAP TPP 13 PER LOKASI INDUK ESELON' => 'cetak_tpp_13_induk_eselon',
            'DAFTAR RAPEL KEKURANGAN TPP' => 'cetak_daftar_rapeltpp',
            'REKAP RAPEL KEKURANGAN TPP PER LOKASI INDUK' => 'cetak_rapeltpp_induk',
            'REKAP RAPEL KEKURANGAN TPP PER LOKASI' => 'cetak_rapeltpp_lokasi',
            'REKAP RAPEL KEKURANGAN TPP SELURUH SKPD' => 'cetak_rapeltpp_skpd',
            'REKAP RAPEL KEKURANGAN TPP BPJS KESEHATAN' => 'cetak_rapeltpp_askes',
            'DAFTAR TPP KELAS' => 'cetak_daftar_tpp_kelas',
            'DAFTAR GAJI PPPK' => 'cetak_daftar_gajip3k',
            'REKAP GAJI PPPK PER LOKASI INDUK' => 'cetak_lokasi_indukp3k',
            'REKAP GAJI PPPK PER LOKASI' => 'cetak_lokasip3k',
            'REKAP GAJI PPPK KIRI KANAN' => 'cetak_kiri_kananp3k',
            'REKAP GAJI PPPK LOKASI DETAIL' => 'cetak_lokasi_detailp3k',
            'REKAP GAJI PPPK SELURUH SKPD' => 'cetak_gajip3k_skpd',
            'REKAP BPJS KESEHATAN PPPK' => 'cetak_askesp3k',
            'REKAP BPJS JKK PPPK' => 'cetak_bpjsp3k_jkk',
            'REKAP BPJS JKM PPPK' => 'cetak_bpjsp3k_jkm',
            'DAFTAR TPP PPPK' => 'cetak_daftar_tppp3k',
            'REKAP TPP PPPK PER LOKASI INDUK' => 'cetak_tppp3k_induk',
            'REKAP TPP PPPK PER LOKASI' => 'cetak_tppp3k_lokasi',
            'REKAP TPP PPPK SELURUH SKPD' => 'cetak_tppp3k_skpd',
            'DAFTAR TPP PPPK KONDISI KERJA' => 'cetak_daftar_tppp3k_kk',
            'REKAP TPP PPPK KONDISI KERJA PER LOKASI INDUK' => 'cetak_tppp3k_induk_kk',
            'REKAP TPP PPPK KONDISI KERJA PER LOKASI' => 'cetak_tppp3k_lokasi_kk',
            'REKAP TPP PPPK KONDISI KERJA BPJS KESEHATAN' => 'cetak_konkerp3k_askes',
            'DAFTAR TPP PPPK TEMPAT BERTUGAS' => 'cetak_daftar_tppp3k_tb',
            'REKAP TPP PPPK TEMPAT BERTUGAS PER LOKASI INDUK' => 'cetak_tppp3k_induk_tb',
            'REKAP TPP PPPK TEMPAT BERTUGAS PER LOKASI' => 'cetak_tppp3k_lokasi_tb',
            'REKAP TPP PPPK TEMPAT BERTUGAS BPJS KESEHATAN' => 'cetak_jakartap3k_askes',
            'REKAP TPP BPJS KESEHATAN PPPK' => 'cetak_tppp3k_askes',
            'DAFTAR GAJI 13 PPPK' => 'cetak_daftar_gajip3k_13',
            'REKAP GAJI 13 PPPK PER LOKASI INDUK' => 'cetak_lokasi_indukp3k_13',
            'REKAP GAJI 13 PPPK PER LOKASI' => 'cetak_lokasip3k_13',
            'REKAP GAJI 13 PPPK KIRI KANAN' => 'cetak_kiri_kananp3k_13',
            'REKAP GAJI 13 PPPK LOKASI DETAIL' => 'cetak_lokasi_detailp3k_13',
            'REKAP GAJI 13 PPPK SELURUH SKPD' => 'cetak_gajip3k_skpd_13',
            'DAFTAR GAJI 14 PPPK' => 'cetak_daftar_gajip3k_14',
            'REKAP GAJI 14 PPPK PER LOKASI INDUK' => 'cetak_lokasi_indukp3k_14',
            'REKAP GAJI 14 PPPK PER LOKASI' => 'cetak_lokasip3k_14',
            'REKAP GAJI 14 PPPK KIRI KANAN' => 'cetak_kiri_kananp3k_14',
            'REKAP GAJI 14 PPPK LOKASI DETAIL' => 'cetak_lokasi_detailp3k_14',
            'REKAP GAJI 14 PPPK SELURUH SKPD' => 'cetak_gajip3k_skpd_14',
            'DAFTAR TPP 13 PPPK' => 'cetak_daftar_tppp3k_13',
            'REKAP TPP 13 PPPK PER LOKASI INDUK' => 'cetak_tppp3k_induk_13',
            'REKAP TPP 13 PPPK PER LOKASI' => 'cetak_tppp3k_lokasi_13',
            'REKAP TPP 13 PPPK SELURUH SKPD' => 'cetak_tppp3k_skpd_13',
            'DAFTAR TPP 13 PPPK KONDISI KERJA' => 'cetak_daftar_tppp3k_kk_13',
            'REKAP TPP 13 PPPK KONDISI KERJA PER LOKASI INDUK' => 'cetak_tppp3k_induk_kk_13',
            'REKAP TPP 13 PPPK KONDISI KERJA PER LOKASI' => 'cetak_tppp3k_lokasi_kk_13',
            'DAFTAR TPP 13 PPPK TEMPAT BERTUGAS' => 'cetak_daftar_tppp3k_tb_13',
            'REKAP TPP 13 PPPK TEMPAT BERTUGAS PER LOKASI INDUK' => 'cetak_tppp3k_induk_tb_13',
            'REKAP TPP 13 PPPK TEMPAT BERTUGAS PER LOKASI' => 'cetak_tppp3k_lokasi_tb_13',
            'REKAP TPP PPPK PER LOKASI INDUK NON ESELON' => 'cetak_tppp3k_induk_non_eselon',
            'REKAP TPP PPPK PER LOKASI INDUK ESELON' => 'cetak_tppp3k_induk_eselon',
            'DAFTAR GAJI PPPK TAHUNAN' => 'cetak_daftar_gajip3k_tahunan',
            'REKAP GAJI PPPK PER LOKASI INDUK TAHUNAN' => 'cetak_lokasi_indukp3k_tahunan',
            'REKAP GAJI PPPK PER LOKASI TAHUNAN' => 'cetak_lokasip3k_tahunan',
            'REKAP GAJI PPPK KIRI KANAN TAHUNAN' => 'cetak_kiri_kananp3k_tahunan',
            'REKAP GAJI PPPK LOKASI DETAIL TAHUNAN' => 'cetak_lokasi_detailp3k_tahunan',
            'REKAP GAJI PPPK SELURUH SKPD TAHUNAN' => 'cetak_gajip3k_skpd_tahunan',
            'DAFTAR TPP PPPK TAHUNAN' => 'cetak_daftar_tppp3k_tahunan',
            'REKAP TPP PPPK PER LOKASI INDUK TAHUNAN' => 'cetak_tppp3k_induk_tahunan',
            'REKAP TPP PPPK PER LOKASI TAHUNAN' => 'cetak_tppp3k_lokasi_tahunan',
            'REKAP TPP PPPK SELURUH SKPD TAHUNAN' => 'cetak_tppp3k_skpd_tahunan',
            'DAFTAR TPP PPPK KONDISI KERJA TAHUNAN' => 'cetak_daftar_tppp3k_kk_tahunan',
            'REKAP TPP PPPK KONDISI KERJA PER LOKASI INDUK TAHUNAN' => 'cetak_tppp3k_induk_kk_tahunan',
            'REKAP TPP PPPK KONDISI KERJA PER LOKASI TAHUNAN' => 'cetak_tppp3k_lokasi_kk_tahunan',
            'DAFTAR TPP PPPK TEMPAT BERTUGAS TAHUNAN' => 'cetak_daftar_tppp3k_tb_tahunan',
            'REKAP TPP PPPK TEMPAT BERTUGAS PER LOKASI INDUK TAHUNAN' => 'cetak_tppp3k_induk_tb_tahunan',
            'REKAP TPP PPPK TEMPAT BERTUGAS PER LOKASI TAHUNAN' => 'cetak_tppp3k_lokasi_tb_tahunan',

        );

        foreach ($arrJenis as $key => $value) {
            // CEK DATA KALKULASI BERDASAR LOKASI 
            if (strpos($value, 'TAHUNAN') !== false) {
                foreach ($arrLokasi as $key => $lokasi) {
                    // $n = ORM::factory('kalkulasi')
                    //     ->where('lokasi_id','=',$lokasi)
                    //     ->where('periode','=',$periode)
                    //     ->count_all();

                    $this->db->where('lokasi_id', $lokasi);
                    $this->db->where('periode', $periode);
                    $this->db->from($this->tb_kalkulasi);
                    $n = $this->db->count_all_results();

                    if ($n == 0) {
                        unset($arrLokasi[$key]);
                    }
                }
            }

            if ($_POST['jenis'] == $key) {
                // $view = View::factory('cetakp3k/'.$value)
                //     ->set('post',$_POST)
                //     ->set('periode',$periode)
                //     ->set('nip',$nip)
                //     ->set('arrLokasi',$arrLokasi)
                //     ->set('arrLokasiInduk',$arrLokasiInduk);
                

                $data['post'] = $_POST;
                $data['periode'] = $periode;
                $data['nip'] = $nip;
                $data['arrLokasi'] = $arrLokasi;
                $data['arrLokasiInduk'] = $arrLokasiInduk;
                $view = 'cetakp3k/file/' . $value;
            }
        }

        // echo $view;
        // echo json_encode($data);
        $this->load->view($view, $data);
    }
}
