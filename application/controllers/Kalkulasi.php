<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulasi extends CI_Controller {

    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');

		// $this->load->model('Fungsi_model');

		$this->gajipokoks = 'simgaji_gajipokoks';
		$this->kalkulasi_rapelp3k = 'simgaji_kalkulasis_rapel_p3ks';
		$this->kalkulasi_rapel_perbulanp3k = 'simgaji_kalkulasis_rapel_perbulan_p3ks';
		$this->pegawai_rapelp3k = 'simgaji_pegawai_rapelp3ks';
		

        $this->tb_lokasi = 'simgaji_lokasis';
        $this->tb_kalkulasi = 'simgaji_kalkulasis';
		$this->tb_kalkulasip3k = 'simgaji_kalkulasip3ks';
		$this->tb_kalkulasip3k_tb = 'simgaji_kalkulasip3ks_tb';
		$this->tb_kalkulasip3k_eb = 'simgaji_kalkulasip3ks_eb';
		$this->tb_kalkulasip3k_tahunan = 'simgaji_kalkulasip3ks_tahunan';

        $this->tb_pegawai = 'simgaji_pegawais';
		$this->tb_pegawaip3k = 'simgaji_pegawaip3ks';
		$this->tb_pegawaip3k_13 = 'simgaji_pegawaip3ks_13';
		$this->tb_pegawaip3k_14 = 'simgaji_pegawaip3ks_14';
		// $this->tb_pegawaip3k_tahunan = 'simgaji_pegawaip3k_tahunan';
		$this->tb_pegawaip3k_tahunan = $this->tb_pegawaip3k;

		$this->tb_tjumum = 'simgaji_tjumums';
		$this->tb_tjumump3k = 'simgaji_tjumump3ks';
		$this->tb_tjfungsional = 'simgaji_tjfungsionals';
		$this->tb_fungsional = 'simgaji_fungsionals';
		$this->tb_eselon = 'simgaji_eselons';
		$this->tb_kedudukan = 'simgaji_kedudukans';
		$this->tb_golongan = 'simgaji_golongans';
		$this->tb_golonganp3k = 'simgaji_golonganp3ks';
		$this->tb_status = 'simgaji_statuses';
		$this->tb_statusp3k = 'simgaji_statusp3ks';
		$this->tb_marital = 'simgaji_maritals';
		$this->tb_beras = 'simgaji_berases';

	}

    public function index(){
		$data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Kalkulasi';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Kalkulasi</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
		$this->load->view('kalkulasi/kalkulasi_index', $data);
    }

	public function rapel(){
		$data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Kalkulasi';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Kalkulasi</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
		$this->load->view('kalkulasi/kalkulasi_rapel', $data);
    }

	function pph($istri, $anak, $pen_tht, $gaji_kotor_pembulatan,$nip){
		if($gaji_kotor_pembulatan < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
		} 
		else {
			$tambahan = 0;
		}
		
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
		
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12 ;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	////pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		$pph_bulan	 = floor($pph_tahun/12) ;
		
		return $pph_bulan ;
	}

	function pph_13($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip)
	{
		if ($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}

		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;

		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}

		$pen_tht_tahun = $pen_tht * 12;

		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;

		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}

		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}

		if ($penghasilan_kpj_13 < 0) {
			$penghasilan_kpj_13 = 0;
		}

		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13, "000", -3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		// PPH Biasa
		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}

		// PPH 13
		if ($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		} elseif ($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		} elseif ($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj_13 - 250000000));
		} elseif ($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj_13 - 500000000));
		}

		return $pph_tahun_13 - $pph_tahun;
	}

	function pph_14($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip)
	{
		if ($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}

		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;

		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}

		$pen_tht_tahun = $pen_tht * 12;
		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;

		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}

		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}

		if ($penghasilan_kpj_13 < 0) {
			$penghasilan_kpj_13 = 0;
		}

		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13, "000", -3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		// PPH Biasa
		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}

		// PPH 13
		if ($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		} elseif ($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		} elseif ($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj_13 - 250000000));
		} elseif ($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj_13 - 500000000));
		}

		/*if($nip == '196002291986031004') {
			echo $bea_jabatan."=".$bea_jabatan_13."=";
			echo $kurang_gaji."=".$penghasilan_bulan."=@";
			echo $ptkp."=";
			echo $penghasilan_tahun."=".$penghasilan_kpj."=".$penghasilan_kpj_13."=#";
			echo $pph_tahun."=".$pph_tahun_13."=";
			echo $pph_tahun_13 - $pph_tahun;
			
			die();
		}*/

		return $pph_tahun_13 - $pph_tahun;
	}

	function pph_desember($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $nip, $gaji_13, $gaji_14, $jumlah_pph, $tahun, $gaji_pokok, $tunjangan_istri, $tunjangan_anak)
	{
		$sql_gaji_kotor =
			"SELECT SUM(gaji_pokok + tunjangan_istri + tunjangan_anak + tunjangan_struktural + tunjangan_beras + tunjangan_fungsional + tunjangan_umum + pembulatan) AS gaji_kotor
		FROM $this->tb_kalkulasip3k WHERE nip = '" . $nip . "' AND MONTH(periode) < 12 AND YEAR(periode) = " . $tahun;
		/*$sql_gaji_kotor = 
		"SELECT SUM(k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak + k.tunjangan_struktural + k.tunjangan_beras + k.tunjangan_fungsional 
+ k.tunjangan_umum + k.pembulatan) + f.jumlah_penghasilan_4 AS gaji_kotor
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND MONTH(k.periode) < 12 AND YEAR(k.periode) = ".$tahun;*/

		// $gaji_kotor = DB::query(Database::SELECT, $sql_gaji_kotor)->execute()->get('gaji_kotor', 0);
		$gaji_kotor = $this->db->query($sql_gaji_kotor)->row()->gaji_kotor;
		$bruto_12 = $gaji_kotor + $gaji_kotor_pembulatan + $gaji_13 + $gaji_14;

		$sql_pen_tht =
			"SELECT SUM(0.0475 * (gaji_pokok + tunjangan_istri + tunjangan_anak)) AS pen_tht_12
		FROM kalkulasis WHERE nip = '" . $nip . "' AND MONTH(periode) < 12 AND YEAR(periode) = " . $tahun;

		/*$sql_pen_tht = 
		"SELECT SUM(0.0475 * (k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak))+ (0.0475 * (f.gaji_pokok_4 + f.tunjangan_istri_4 + f.tunjangan_anak_4)) AS pen_tht_12
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND MONTH(k.periode) < 12 AND YEAR(k.periode) = ".$tahun;*/

		// $pen_tht_12 = DB::query(Database::SELECT, $sql_pen_tht)->execute()->get('pen_tht_12', 0);
		$pen_tht_12 = $this->db->query($sql_pen_tht)->row()->pen_tht_12;

		$pen_tht_12 = $pen_tht_12 + (0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak));
		if ($pen_tht_12 > 2400000) {
			$pen_tht_12 = 2400000;
		}

		$bea_jabatan_12 = ceil(0.05 * $bruto_12);
		if ($bea_jabatan_12 > 6000000) {
			$bea_jabatan_12 = 6000000;
		}

		$kurang_gaji_12 = $bea_jabatan_12 + $pen_tht_12;
		$penghasilan_tahun	= $bruto_12 - $kurang_gaji_12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}
		//$pph_bulan	 = floor($pph_tahun/12) ;

		//dafiz
		/*$sql_pph_rapel = 
		"SELECT tunjangan_pph_4 AS pph_rapel
		FROM kalkulasis_rapels_finals WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
				
		$pph_rapel = DB::query(Database::SELECT, $sql_pph_rapel)->execute()->get('pph_rapel', 0);*/
		//dafiz end

		$pph_bulan = $pph_tahun - $jumlah_pph;
		if ($pph_bulan <= 0) {
			$pph_bulan = 0;
		}

		return $pph_bulan;
		//return $bruto_12."#".$bea_jabatan_12."#".$pen_tht_12."#".$kurang_gaji_12;
		//return $gaji_kotor_pembulatan."#".$gaji_kotor_pembulatan_12."#".$bruto_12."#".$bea_jabatan_12."#".$kurang_gaji_12."#".$penghasilan_tahun."#".$ptkp."#".$penghasilan_kpj;
		//return $gaji_kotor."#".$bruto_12."#".$gaji_kotor_pembulatan."#".$gaji_13."#".$gaji_14;
		//return $pph_tahun."#".$jumlah_pph;
		//return $bruto_12;
	}
    

    public function action(){

      
		
        $lokasi_start = $_POST['lokasi_start'];
		$lokasi_end =$_POST['lokasi_end'];

		

        if($_POST['jenis'] != "GAJI_SUSULAN") {
			$periode = $_POST['tahun'];
			
			if(isset($_POST['bulan_id'])) {	
				$periode .= "-".$_POST['bulan_id']."-01";
			}
			
			$arrLokasi = array();
			if($_POST['lokasi_end']) {

				

				// echo $lokasi_start;
				// echo "<br/>";
				// echo $lokasi_end;
				// die();

                if(substr($lokasi_end,-3) == "000"){
                    $this->db->where('LEFT(kode,2)',substr($lokasi_end,0,2));
					// $this->db->like('kode', substr($lokasi_start,0,2));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
					// echo $max->kode;
					// die();

                }else{
                    $this->db->where('LEFT(kode,6)',substr($lokasi_end,0,6));
					// $this->db->like('kode', substr($lokasi_start,0,6));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
                }

                $this->db->where('kode >=', $lokasi_start);
                $this->db->where('kode <=', $max->kode);
                $lokasis = $this->db->get($this->tb_lokasi)->result();
					
				foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

				// echo json_encode($arrLokasi);
				// die();	

			} else {

				

                if(substr($lokasi_start,-4) == "0000") {

                    if(substr($lokasi_start,0,2) == "34") {

                        $this->db->where('LEFT("kode",2) =', 34);
                        $this->db->where('LEFT("kode",4) !=', 3411);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
						

					} else {
			
						$this->db->where('LEFT(kode,2) =', substr($lokasi_start,0,2));
						// $this->db->where('LEFT(kode,2) =',substr($lokasi_start,0,2),true);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
						// echo json_encode($lokasis);
						// die();
					}


                }else{

                    $this->db->where('LEFT("kode",6) =', substr($lokasi_start,0,6));
                    $lokasis = $this->db->get($this->tb_lokasi)->result();
				

                }

                foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

			
				// echo json_encode($arrLokasi);
				// die();
				
				
			}
		}

		



        if($_POST['jenis'] == "GAJI_BULANAN") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasi);
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gaji_bulanan($periode,$arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_BULANAN_PPPK") {

			// print_r($arrLokasi);
			// die();	
            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k);
				
			
		  	$this->gajip3k_bulanan($periode,$arrLokasi);
		}

		if($_POST['jenis'] == "GAJI_BULANAN_RAPEL_PPPK") {

			// $this->kalkulasi_rapel = 'simgaji_kalkulasis_rapel';
			// $this->kalkulasi_rapel_perbulan = 'simgaji_kalkulasis_rapel_perbulan';

            // $this->db->where_in('lokasi_id', $arrLokasi);
            // $this->db->where('periode', $periode);
            // $this->db->delete($this->kalkulasi_rapel_perbulanp3k);
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gaji_bulanan_rapelp3k($periode,$arrLokasi);
		}

		if($_POST['jenis'] == "GAJI_RAPEL_PPPK") {

			$this->gaji_rapelp3k($arrLokasi); 
	  }

		elseif($_POST['jenis'] == "GAJI_13_PPPK") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k_tb);
				
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_13($periode, $arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_14_PPPK") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k_eb);
				
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_14($periode, $arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_DES_PPPK") {
	
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_desember($arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_TAHUNAN_PPPK") {

            
			$this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('YEAR(periode)', $periode);
            $this->db->delete($this->tb_kalkulasip3k_tahunan);	
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_tahunan($periode,$arrLokasi);
		}


    }

	

	function gaji_bulanan($periode, $arrLokasi){	
		
		error_reporting(0);
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$this->db->select($this->tb_pegawai.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golongan.'.kode as golongan_kode');
		$this->db->select($this->tb_status.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawai);
		$this->db->join($this->tb_eselon, $this->tb_pegawai.'.eselon_id ='.$this->tb_eselon.'.id');
        $this->db->join($this->tb_kedudukan, $this->tb_pegawai.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawai.'.fungsional_id ='.$this->tb_fungsional.'.id');
		$this->db->join($this->tb_golongan, $this->tb_pegawai.'.golongan_id ='.$this->tb_golongan.'.id');
		$this->db->join($this->tb_status, $this->tb_pegawai.'.status_id ='.$this->tb_status.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawai.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawai.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
        $this->db->where_in('lokasi_gaji', $arrLokasi);
        $this->db->where_in('status_id', array(1,2,7,8,9));
		$this->db->where('nip', '197806022023211001');
        $pegawais = $this->db->get()->result();

		echo json_encode($pegawais);
		die();
        
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;

			// $this->db->where('id', $pegawai->eselon_id);
			// $eselon_ = $this->db->get($this->tb_eselon)->row();

			// $this->db->where('id', $pegawai->fungsional_id);
			// $fungsional_ = $this->db->get($this->tb_fungsional)->row();

			// $this->db->where('id', $pegawai->kedudukan_id);
			// $kedudukan_ = $this->db->get($this->tb_kedudukan)->row();
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan_name	;	
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional_name;
				}
				else {
					if($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name	;	
				}
			}
			
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;

				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

					
					//$tunjangan_fungsional = 0;
				} else {
					
					if($pegawai->kedudukan_id != 72) {

						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();

					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}

				
				// Kondisi Khusus untk Tunjangan Sandiman, Nominal Tunj Fungs dipindahkan ke Nominal $tunjangan_umum_tambahan di cetakan disebut sbg Tunj Kompensasi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) { //Jika Ahli Sandi tingkat 1-6
					if($pegawai->kompensasi_id >= 879 AND $pegawai->kompensasi_id <= 891) { //Jika Sandiman ( tunjangan kompensasi pemula-madya) dan merupakan JFT
						//Diisi tunjangan funstionalnya sesuai nominal
						//Diisi tunjangan kompensasinya di tunj umum tambahan
						//Dinolkan tunjangan umumnya karena merupakan fungsional umum
					

						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						
						
					$tunjangan_umum = 0;
					} else { //Kalau yang ini fungsional tertentu (JFT) tapi bukan sandiman
						$tunjangan_fungsional = 0;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						}
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 

				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// $jumlah_potongan = round($potongan_lain + $potongan_iwp);
				// $gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum + $tunjangan_umum_tambahan);
				// $gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// $jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras + $tunjangan_umum_tambahan);
				// $jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasi, $arrFieldVal);

			}

			// echo json_encode($arrFieldVal);
			
		}

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi'. $periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');

		
	}

	function gajip3k_bulanan_save($periode, $arrLokasi)
	{

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 5, 2);


		if($this_period == $periode){
			$this->tb_pegawaip3k = $this->tb_pegawaip3k;
		}else{
			$this->tb_pegawaip3k = $this->tb_pegawaip3k.'_'.$year_search.'_'.$month_search;
		}

		// echo $month_search;
		// echo '<br/>';
		// echo $this_period;
		// echo '<br/>';
		// echo $periode;
		// echo '<br/>';
		// echo $this->tb_pegawaip3k;
		// die();


		if ( !$this->db->table_exists($this->tb_pegawaip3k) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}
		
	
		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','198102072010011014')
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		// $this->db->where_in('status_id', array(1,2,7,8,9));
		$this->db->where($this->tb_pegawaip3k.'.nip', '196301292021212001');
		$pegawaip3ks = $this->db->get()->result();
		// echo json_encode($arrLokasi);
		// echo json_encode($pegawaip3ks);
		// die();

		foreach ($pegawaip3ks as $key => $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			// $xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$xtgllahir = explode("-", $pegawai->tmt_akhir_kontrak);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			$th_akhir_kontrak = date('Y',strtotime($pegawai->tmt_akhir_kontrak));
			$bl_akhir_kontrak = date('m',strtotime($pegawai->tmt_akhir_kontrak))+1;
			$per = $year_period.'-'.$month_period;
			$per2 = $year_period.'-'.$month_period.'-01';
			$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;

			if($pegawai->tmt_akhir_kontrak < $per2){

				$pensi = array(
					'status_id' => 4
				);
				$this->db->where('nip', $pegawai->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);
				$pensiun = 1;

			}else{

				// $pensi = array(
				// 	'status_id' => 2
				// );
				// $this->db->where('nip', $pegawai->nip);
				// $this->db->update($this->tb_pegawaip3k, $pensi);
				$pensiun = 0;
			}
			echo $pensiun;
			die();
			

			// if ($pegawai->eselon_id > 1) {
			// 	if ($pegawai->eselon_id < 8) {
			// 		if ($pegawai->eselon_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 	}
			// 	$jabatan = $pegawai->kedudukan_name;
			// } else {
			// 	if ($pegawai->fungsional_id > 1) {
			// 		if ($pegawai->fungsional_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->fungsional_name;
			// 	} else {
			// 		if ($pegawai->kedudukan_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->kedudukan_name;
			// 	}
			// }

			if ($pensiun == 0) {
				
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();
						
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/


				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				//$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
				$potongan_pensiun = 0;

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;

				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				// echo '<br/>';
				// echo 'Gaji kotor pembulatan: '.$gaji_kotor_pembulatan;
				// echo '<br/>';
				// echo 'Jumlah Bersih: '.$jumlah_bersih;
				// echo '<br/>';
				// echo 'PPH: '.$pph_bulan;
				// echo '<br/>';
				// echo 'Jumlah: '.$jumlah_bersih_bayar;
				// die();


				//echo $gaji_pokok;
				//die();

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/

				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);

			}
			
			// else if($pensiun == 1){

				
			// 	$pensi = array(
			// 		'status_id' => 4
			// 	);
			// 	$this->db->where('nip', $pegawai->nip);
			// 	$this->db->update($this->tb_pegawaip3k, $pensi);
			// }
		}

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}

	function gajip3k_bulanan_BAK($periode, $arrLokasi)
	{

		// echo json_encode($arrLokasi);
		// die();

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 5, 2);


		if($this_period == $periode){
			$this->tb_pegawaip3k = $this->tb_pegawaip3k;
		}else{
			$this->tb_pegawaip3k = $this->tb_pegawaip3k.'_'.$year_search.'_'.$month_search;
		}

		// echo $month_search;
		// echo '<br/>';
		// echo $this_period;
		// echo '<br/>';
		// echo $periode;
		// echo '<br/>';
		// echo $this->tb_pegawaip3k;
		// die();


		if ( !$this->db->table_exists($this->tb_pegawaip3k) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}
		
	
		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','198102072010011014')
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		// $this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197806022023211001');
		
		$pegawaip3ks2 = $this->db->get()->result();
		// echo json_encode($pegawaip3ks2);
		// die();

		foreach ($pegawaip3ks2 as $key => $pegawai2) {


		$th_akhir_kontrak = date('Y',strtotime($pegawai2->tmt_akhir_kontrak));
		$bl_akhir_kontrak = date('m',strtotime($pegawai2->tmt_akhir_kontrak))+1;
		$per = $year_period.'-'.$month_period;
		$per2 = $year_period.'-'.$month_period.'-01';
		$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;
			if($akon == $per){

				$pensi = array(
					'status_id' => 3
				);
				$this->db->where('nip', $pegawai2->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);

			}

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai2->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			$pensiun = 0;
			if ($pegawai2->eselon_id > 1) {
				if ($pegawai2->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai2->fungsional_id > 1) {
					if ($pegawai2->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai2->fungsional_name;
				} else {
					if ($pegawai2->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai2->kedudukan_name;
				}
			}
			

			if($pensiun == 1){

				$pensi = array(
					'status_id' => 3
				);
				$this->db->where('nip', $pegawai2->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);

			}

		}	
		// die();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		// $this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197806022023211001');
		$pegawaip3ks = $this->db->get()->result();
		// echo json_encode($arrLokasi);
		// echo json_encode($pegawaip3ks);
		// die();
		
		$pensiun = 0;
		foreach ($pegawaip3ks as $key => $pegawai) {
			

			$xperiode = explode("-", $periode);
			// $xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$xtgllahir = explode("-", $pegawai->tmt_akhir_kontrak);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			$th_akhir_kontrak = date('Y',strtotime($pegawai->tmt_akhir_kontrak));
			$bl_akhir_kontrak = date('m',strtotime($pegawai->tmt_akhir_kontrak))+1;
			$per = $year_period.'-'.$month_period;
			$per2 = $year_period.'-'.$month_period.'-01';
			$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;

			// $jabatan = $pegawai->kedudukan_name;

			

			// if(){
			// 	// $x++;
			// 	$pensiun = 0;
			// }

			// echo $pensiun;
			// die();

			

			
			

			if ($pegawai->status_id == 1 || $pegawai->status_id == 2 || $pegawai->status_id == 6 || $pegawai->status_id == 7 || $pegawai->status_id == 8) {
				
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
					// $tunjangan_umum = $pegawai->golongan_id[0];
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						// $this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();
						
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/


				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				//$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
				$potongan_pensiun = 0;

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;

				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				// echo '<br/>';
				// echo 'Gaji kotor pembulatan: '.$gaji_kotor_pembulatan;
				// echo '<br/>';
				// echo 'Jumlah Bersih: '.$jumlah_bersih;
				// echo '<br/>';
				// echo 'PPH: '.$pph_bulan;
				// echo '<br/>';
				// echo 'Jumlah: '.$jumlah_bersih_bayar;
				// die();


				//echo $gaji_pokok;
				//die();

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/

				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				// $this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);

				$insert_query = $this->db->insert_string($this->tb_kalkulasip3k, $arrFieldVal);
				$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
				$this->db->query($insert_query);

				// echo json_encode($arrFieldVal);
				// die();
				

			}

			// echo json_encode($arrFieldVal);
			// die();
			
			// else if($pensiun == 1){

				
			// 	$pensi = array(
			// 		'status_id' => 4
			// 	);
			// 	$this->db->where('nip', $pegawai->nip);
			// 	$this->db->update($this->tb_pegawaip3k, $pensi);
			// }
		}

		// echo json_encode($arrFieldVal);
		// die();

		// echo $x;
		// die();

		// echo json_encode($arrFieldVal);
		// die();

		// $this->db->insert_batch($this->tb_kalkulasip3k, $arrFieldVal);

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}

	function gajip3k_bulanan($periode, $arrLokasi)
	{

		// echo json_encode($arrLokasi);
		// die();

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 5, 2);


		if($this_period == $periode){
			$this->tb_pegawaip3k = $this->tb_pegawaip3k;
		}else{
			$this->tb_pegawaip3k = $this->tb_pegawaip3k.'_'.$year_search.'_'.$month_search;
		}

		// echo $month_search;
		// echo '<br/>';
		// echo $this_period;
		// echo '<br/>';
		// echo $periode;
		// echo '<br/>';
		// echo $this->tb_pegawaip3k;
		// die();


		if ( !$this->db->table_exists($this->tb_pegawaip3k) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}
		
	
		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','198102072010011014')
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		// $this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197806022023211001');
		
		$pegawaip3ks2 = $this->db->get()->result();
		// echo json_encode($pegawaip3ks2);
		// die();

		foreach ($pegawaip3ks2 as $key => $pegawai2) {


		$th_akhir_kontrak = date('Y',strtotime($pegawai2->tmt_akhir_kontrak));
		$bl_akhir_kontrak = date('m',strtotime($pegawai2->tmt_akhir_kontrak))+1;
		$per = $year_period.'-'.$month_period;
		$per2 = $year_period.'-'.$month_period.'-01';
		$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;
			if($akon == $per){

				$pensi = array(
					'status_id' => 3
				);
				$this->db->where('nip', $pegawai2->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);

			}

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai2->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			$pensiun = 0;
			if ($pegawai2->eselon_id > 1) {
				if ($pegawai2->eselon_id < 8) {
					if ($pegawai2->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai2->fungsional_id != 000) {
					if ($pegawai2->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai2->fungsional_name;
				} else {
					if ($pegawai2->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai2->kedudukan_name;
				}
			}
			

			if($pensiun == 1){

				$pensi = array(
					'status_id' => 3
				);
				$this->db->where('nip', $pegawai2->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);

			}

		}	
		// die();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		// $this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197806022023211001');
		$pegawaip3ks = $this->db->get()->result();
		// echo json_encode($arrLokasi);
		// echo json_encode($pegawaip3ks);
		// die();
		
		$pensiun = 0;
		foreach ($pegawaip3ks as $key => $pegawai) {
			

			$xperiode = explode("-", $periode);
			// $xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$xtgllahir = explode("-", $pegawai->tmt_akhir_kontrak);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			$th_akhir_kontrak = date('Y',strtotime($pegawai->tmt_akhir_kontrak));
			$bl_akhir_kontrak = date('m',strtotime($pegawai->tmt_akhir_kontrak))+1;
			$per = $year_period.'-'.$month_period;
			$per2 = $year_period.'-'.$month_period.'-01';
			$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;

			// $jabatan = $pegawai->kedudukan_name;

			

			// if(){
			// 	// $x++;
			// 	$pensiun = 0;
			// }

			// echo $pensiun;
			// die();

			

			
			

			if ($pegawai->status_id == 1 || $pegawai->status_id == 2 || $pegawai->status_id == 6 || $pegawai->status_id == 7 || $pegawai->status_id == 8) {
				
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

		

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
					// $tunjangan_umum = $pegawai->golongan_id[0];
				}

				$tunjangan_umum_tambahan = 0;
				$jabatan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						$jabatan = $pegawai->fungsional_name;
						// die();
				} else {

					$jabatan = $pegawai->kedudukan_name;
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						// $this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();
						$jabatan = $pegawai->kedudukan_name;
						
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/


				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				//$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
				$potongan_pensiun = 0;

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;

				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				// echo '<br/>';
				// echo 'Gaji kotor pembulatan: '.$gaji_kotor_pembulatan;
				// echo '<br/>';
				// echo 'Jumlah Bersih: '.$jumlah_bersih;
				// echo '<br/>';
				// echo 'PPH: '.$pph_bulan;
				// echo '<br/>';
				// echo 'Jumlah: '.$jumlah_bersih_bayar;
				// die();


				//echo $gaji_pokok;
				//die();

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/

				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				// $this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);

				$insert_query = $this->db->insert_string($this->tb_kalkulasip3k, $arrFieldVal);
				$insert_query = str_replace('INSERT INTO','INSERT IGNORE INTO',$insert_query);
				$this->db->query($insert_query);

				// echo json_encode($arrFieldVal);
				// die();
				

			}

			// echo json_encode($arrFieldVal);
			// die();
			
			// else if($pensiun == 1){

				
			// 	$pensi = array(
			// 		'status_id' => 4
			// 	);
			// 	$this->db->where('nip', $pegawai->nip);
			// 	$this->db->update($this->tb_pegawaip3k, $pensi);
			// }
		}

		// echo json_encode($arrFieldVal);
		// die();

		// echo $x;
		// die();

		// echo json_encode($arrFieldVal);
		// die();

		// $this->db->insert_batch($this->tb_kalkulasip3k, $arrFieldVal);

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}


	function gajip3k_13($periode, $arrLokasi)
	{
		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		// $month_search = substr($periode, 6, 1);
		$month_search = substr($periode, 5, 2);


		// if($this_period == $periode){
			// $this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13;
		// }else{
			// $this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13.'_'.$year_search.'_'.$month_search;
		// }

		$this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13;
		if ( !$this->db->table_exists($this->tb_pegawaip3k_13) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}

		// echo $this->tb_pegawaip3k_13;
		// die();


		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k_13.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k_13);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k_13.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k_13.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k_13.'.fungsional_id ='.$this->tb_fungsional.'.id', 'LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k_13.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k_13.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k_13.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k_13.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$pegawaip3ks = $this->db->get()->result();

		// $pegawaip3ks = ORM::factory('pegawaitbp3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	->find_all();

		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
						$this->db->where('golongan', $pegawai->golongan_id[0]);
						$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;


					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
						$this->db->where('golongan', $pegawai->golongan_id[0]);
						$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$askes = 0;

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// Beras
				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				$potongan_bpjs_kesehatan = 0;
				$potongan_pensiun = 0;
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if ($bea_jabatan > 500000) {
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;


				// Tanpa Beras				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				// Dengan Beras
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				$gaji_kotor_13	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				#$gaji_bersih_13 = round($gaji_kotor - $jumlah_potongan);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				// Tanpa Beras
				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				// Dengan Beras
				$pembulatan_13	= 0;
				if (substr($gaji_bersih_13, -2) != "00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				$pph_bulan = $this->pph_13($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;

				$pembulatan	= 0;
				if (substr($jumlah_k, -2) != "00") {
					$pembulatan = 100 - substr($jumlah_k, -2);
				}

				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k_tb, $arrFieldVal);
			}
		}

		

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasitbp3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
	

	$array_msg = array(
		'status'=>'success',
		'message'=>'Berhasil melakukan Kalkulasi 13 Periode '.$periode
	);
	
	$this->session->set_flashdata($array_msg);
	redirect('kalkulasi');

	}

	function gajip3k_14($periode, $arrLokasi)
	{
		// $this->auto_render = false;

		// echo json_encode($arrLokasi);
		// die();

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		// $month_search = substr($periode, 6, 1);
		$month_search = substr($periode, 5, 2);

		// if($this_period == $periode){
		// 	$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14;
		// }else{
		// 	$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14.'_'.$year_search.'_'.$month_search;
		// }

		$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14;

		if ( !$this->db->table_exists($this->tb_pegawaip3k_14) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}


		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k_14.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k_14);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k_14.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k_14.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k_14.'.fungsional_id ='.$this->tb_fungsional.'.id', 'LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k_14.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k_14.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k_14.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k_14.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$pegawaip3ks = $this->db->get()->result();

		// $pegawaip3ks = ORM::factory('pegawaiebp3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	->find_all();



		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$askes = 0;

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// Beras
				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/

				$potongan_bpjs_kesehatan = 0;
				$potongan_pensiun = 0;
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;


				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if ($bea_jabatan > 500000) {
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// Biasa				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				// 13
				$potongan_lain = 0;
				$potongan_iwp = 0;
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				#edy
				#$gaji_kotor_13 = $gaji_pokok;
				#gaji ke 14 / THR th 2018 dibayarkan tj jab & tj keluarga
				$gaji_kotor_13 = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				// Tanpa Beras
				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp;
					die();
				}*/

				// Dengan Beras
				$pembulatan_13	= 0;
				if (substr($gaji_bersih_13, -2) != "00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;

				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_kotor_pembulatan."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp."=";
					echo $gaji_kotor_13;
					die();
				}*/

				$pph_bulan = $this->pph_14($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
				#edy				
				#$jumlah_kotor = $gaji_pokok + $pph_bulan;
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5;
				$jumlah_k = $jumlah_kotor;
				#$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				#$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				$pembulatan	= 0;
				#if(substr($gaji_pokok,-2)!="00") {
				#	$pembulatan = 100 - substr($gaji_pokok,-2);
				#}
				if (substr($jumlah_k, -2) != "00") {
					$pembulatan = 100 - substr($jumlah_k, -2);
				}

				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;

				#$tunjangan_anak = $tunjangan_istri = $tunjangan_beras = 0;
				#$tunjangan_umum = $tunjangan_fungsional = $tunjangan_struktural = 0;
				$potongan_iwp = $potongan_lain = $jumlah_potongan = 0;

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k_eb, $arrFieldVal);
			}

			// echo json_encode($arrFieldVal);
			// die();
		}

		// echo json_encode($arrFieldVal);
		// die();

		


		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasiebp3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi 14 Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	
	}

	function gajip3k_desember($arrLokasi)
	{
		// $this->auto_render = false;
		$month_period = '12';
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$this->db->where_in('lokasi_id', $arrLokasi);
		$this->db->where('periode', $this_period);
		$this->db->delete($this->tb_kalkulasip3k);

		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		$periode 	= $this_period;

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','196806141990011001') // Arief Irwanto
		// 	//->where('nip','=','196708131989032005') // Widyowati
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		$pegawaip3ks2 = $this->db->get()->result();
		foreach ($pegawaip3ks2 as $key => $pegawai2) {


			$th_akhir_kontrak = date('Y',strtotime($pegawai2->tmt_akhir_kontrak));
			$bl_akhir_kontrak = date('m',strtotime($pegawai2->tmt_akhir_kontrak))+1;
			$per = $year_period.'-'.$month_period;
			$per2 = $year_period.'-'.$month_period.'-01';
			$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;
			if($akon == $per){
	
				$pensi = array(
					'status_id' => 3
				);
				$this->db->where('nip', $pegawai2->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);
	
			}
			}	


		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		$pegawaip3ks = $this->db->get()->result();

		$pensiun	= 0;
		foreach ($pegawaip3ks as $pegawai) {
			

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			// if ($pegawai->eselon_id > 1) {
			// 	if ($pegawai->eselon_id < 8) {
			// 		if ($pegawai->eselon_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 	}
			// 	$jabatan = $pegawai->kedudukan->name;
			// } else {
			// 	if ($pegawai->fungsional_id > 1) {
			// 		if ($pegawai->fungsional_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->fungsional_name;
			// 	} else {
			// 		if ($pegawai->kedudukan_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->kedudukan_name;
			// 	}
			// }

			// if ($pensiun == 0) {
			if ($pegawai->status_id == 1 || $pegawai->status_id == 2 || $pegawai->status_id == 6 || $pegawai->status_id == 7 || $pegawai->status_id == 8) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
					
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
					$tunjangan_fungsional = 0;
					
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;
				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				//$gaji_kotor_pembulatan = $gaji_kotor;

				// 13
				// $kalkulasitbs = ORM::factory('kalkulasitbp3k')
				// 	->where('nip', '=', $pegawai->nip)
				// 	->where('YEAR("periode")', '=', $periode)
				// 	->find();
				$this->db->where('nip', $pegawai->nip);
				$this->db->where('YEAR(periode)', $periode);
				$kalkulasitbs = $this->db->get($this->tb_kalkulasip3k_tb)->row();

				$gaji_13 = $kalkulasitbs->gaji_pokok + $kalkulasitbs->tunjangan_istri + $kalkulasitbs->tunjangan_anak + $kalkulasitbs->tunjangan_umum + $kalkulasitbs->tunjangan_fungsional + $kalkulasitbs->tunjangan_struktural + $kalkulasitbs->pembulatan;

				// 14
				// $kalkulasiebs = ORM::factory('kalkulasiebp3k')
				// 	->where('nip', '=', $pegawai->nip)
				// 	->where('YEAR("periode")', '=', $periode)
				// 	->find();
				$this->db->where('nip', $pegawai->nip);
				$this->db->where('YEAR(periode)', $periode);
				$kalkulasiebs = $this->db->get($this->tb_kalkulasip3k_eb)->row();

				$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->tunjangan_istri + $kalkulasiebs->tunjangan_anak + $kalkulasiebs->tunjangan_umum + $kalkulasiebs->tunjangan_fungsional + $kalkulasiebs->tunjangan_struktural + $kalkulasiebs->pembulatan;
				//$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->pembulatan;

				// Gaji Jan sd Nov 
				// $sql_pph_tahun =
				// 	"SELECT SUM(tunjangan_pph) AS pph_tahun
				// FROM kalkulasip3ks WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];
				$sql_pph_tahun =
				"SELECT SUM(tunjangan_pph) AS pph_tahun
				FROM $this->tb_kalkulasip3k WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];

				// $pph_tahun = DB::query(Database::SELECT, $sql_pph_tahun)->execute()->get('pph_tahun', 0);
				$pph_tahun = $this->db->query($sql_pph_tahun)->row()->pph_tahun;

				// $sql_pph_13_tahun =
				// 	"SELECT SUM(tunjangan_pph) AS pph_13_tahun
				// FROM kalkulasitbp3ks WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];
				$sql_pph_13_tahun =
				"SELECT SUM(tunjangan_pph) AS pph_13_tahun
			    FROM $this->tb_kalkulasip3k_tb WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];

				// $pph_13_tahun = DB::query(Database::SELECT, $sql_pph_13_tahun)->execute()->get('pph_13_tahun', 0);
				$pph_13_tahun = $this->db->query($sql_pph_13_tahun)->row()->pph_13_tahun;

				// $sql_pph_14_tahun =
				// 	"SELECT SUM(tunjangan_pph) AS pph_14_tahun
				// FROM kalkulasiebp3ks WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];
				$sql_pph_14_tahun =
					"SELECT SUM(tunjangan_pph) AS pph_14_tahun
				FROM $this->tb_kalkulasip3k_eb WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];

				// $pph_14_tahun = DB::query(Database::SELECT, $sql_pph_14_tahun)->execute()->get('pph_14_tahun', 0);
				$pph_14_tahun = $this->db->query($sql_pph_14_tahun)->row()->pph_14_tahun;

				$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun;

				$pph_bulan = $this->pph_desember($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip, $gaji_13, $gaji_14, $jumlah_pph, $xperiode[0], $gaji_pokok, $tunjangan_istri, $tunjangan_anak);

				//echo $gaji_pokok."#".$tunjangan_istri."#".$tunjangan_anak;
				//echo $tunjangan_umum."#".$tunjangan_fungsional."#".$tunjangan_struktural."#".$tunjangan_beras;
				//echo $gaji_13."#".$gaji_14."<br>";

				//echo $pph_bulan."#".$pph_tahun."#".$pph_13_tahun."#".$pph_14_tahun;
				//echo $pph_bulan;


				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				if ($pph_bulan < 0) {
					$pph_bulan = 0;
				}

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);
			}
		}

		//die();

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Gaji Deseember Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
		
	}

	function gajip3k_tahunan($periode, $arrLokasi)
	{
		// $kalkulasis = ORM::factory('kalkulasip3k')
		// 	->where('lokasi_id', 'IN', $arrLokasi)
		// 	->find_all();
		
		$this->db->where('YEAR(periode)', $periode);
		$this->db->where_in('lokasi_id', $arrLokasi);
		$kalkulasis = $this->db->get($this->tb_kalkulasip3k)->result();

		$arrNip = array();
		foreach ($kalkulasis as $k) {
			array_push($arrNip, $k->nip);
		}
		$arrNip = array_unique($arrNip);
		// $arrNip = array('196302162021211001');

		// echo json_encode($arrNip);

		$value = "";
		foreach ($arrNip as $key => $nip) {
			// $n = ORM::factory('kalkulasip3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->count_all();

			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$n = $this->db->get($this->tb_kalkulasip3k)->num_rows();


				
			if ($n == 0) {
				continue;
				
			}

			$gaji_pokok = 0;
			$tunjangan_istri = 0;
			$tunjangan_anak = 0;
			$jumlah_tunjangan_keluarga = 0;
			$tunjangan_umum = 0;
			$tunjangan_umum_tambahan = 0;
			$tunjangan_struktural = 0;
			$tunjangan_fungsional = 0;
			$tunjangan_beras = 0;
			$tunjangan_pph = 0;
			$pembulatan = 0;
			$jumlah_kotor = 0;
			$potongan_bpjs_kesehatan = 0;
			$potongan_pensiun = 0;
			$potongan_iwp = 0;
			$potongan_lain = 0;
			$potongan_cp = 0;
			$jumlah_potongan = 0;
			$jumlah_bersih = 0;
			$jumlah_bersih_bayar = 0;
			$askes = 0;
			$sgaji_pokok = 0;
			$ssgaji_pokok = 0;
			$stunjangan_istri = 0;
			$stunjangan_anak = 0;
			$sjumlah_tunjangan_keluarga = 0;
			$stunjangan_umum = 0;
			$stunjangan_umum_tambahan = 0;
			$stunjangan_struktural = 0;
			$stunjangan_fungsional = 0;
			$stunjangan_beras = 0;
			$stunjangan_pph = 0;
			$spembulatan = 0;
			$sjumlah_kotor = 0;
			$spotongan_bpjs_kesehatan = 0;
			$spotongan_pensiun = 0;
			$spotongan_iwp = 0;
			$spotongan_lain = 0;
			$spotongan_cp = 0;
			$sjumlah_potongan = 0;
			$sjumlah_bersih = 0;
			$sjumlah_bersih_bayar = 0;
			$saskes = 0;
			$n_bea_jabatan = 0;
			$n_tht = 0;
			$n_netto = 0;
			$n_ptkp = 0;
			$n_pkpj = 0;

			// SUM GAJI BULANAN
			$sql = "SELECT 
			lokasi_id, lokasi_kode, lokasi_string, name, tanggal_lahir, nip, 
			status_string, golongan_id, golongan_string, jabatan, marital_id,
			marital_string, istri, anak, jiwa, jiwa_string, status_id, kelompok_gaji,
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM $this->tb_kalkulasip3k		
			WHERE YEAR(periode) = ? AND nip = ?";

			// $query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			$query =  $this->db->query($sql, array($periode, $nip))->result();


			foreach ($query as $q) {
				$lokasi_id = $q->lokasi_id;
				$lokasi_kode = $q->lokasi_kode;
				$lokasi_string = $q->lokasi_string;
				$name = $q->name;
				$tanggal_lahir = $q->tanggal_lahir;
				$nip = $q->nip;
				$status_id = $q->status_id;
				$status_string = $q->status_string;
				$golongan_id = $q->golongan_id;
				$golongan_string = $q->golongan_string;
				$jabatan = $q->jabatan;
				$marital_id = $q->marital_id;
				$marital_string = $q->marital_string;
				$istri = $q->istri;
				$anak = $q->anak;
				$jiwa = $q->jiwa;
				$jiwa_string = $q->jiwa_string;
				$kelompok_gaji = $q->kelompok_gaji;

				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = 0;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$potongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$askes = $askes + $q->askes;
			}

			//echo $gaji_pokok;
			//die();

			// GAJI 13
			// $n = ORM::factory('kalkulasitbp3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->count_all();
			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$n = $this->db->get($this->tb_kalkulasip3k_tb)->num_rows();
			

			if ($n == 0) {
				$sgaji_pokok = $gaji_pokok + 0;
				$stunjangan_istri = $tunjangan_istri + 0;
				$stunjangan_anak = $tunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $tunjangan_umum + 0;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $tunjangan_struktural + 0;
				$stunjangan_fungsional = $tunjangan_fungsional + 0;
				$stunjangan_beras = $tunjangan_beras + 0;
				$stunjangan_pph = 0;
				$spembulatan = $pembulatan + 0;
				$sjumlah_kotor = $jumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $potongan_pensiun + 0;
				$spotongan_iwp = $potongan_iwp + 0;
				$spotongan_lain = $potongan_lain + 0;
				$spotongan_cp = $potongan_cp + 0;
				$sjumlah_potongan = $jumlah_potongan + 0;
				$sjumlah_bersih = $jumlah_bersih + 0;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + 0;
				$saskes = $askes + 0;
			}

			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM $this->tb_kalkulasip3k_tb		
			WHERE YEAR(periode) = ? AND nip = ?";

			// $query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			$query =  $this->db->query($sql, array($periode, $nip))->result();

			foreach ($query as $q) {
				$sgaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;
				$stunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = 0;
				$spembulatan = $pembulatan + $q->pembulatan;
				$sjumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $potongan_lain + $q->potongan_lain;
				$spotongan_cp = $potongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $askes + $q->askes;
			}

			// RAPEL GAJI
			//$n = ORM::factory('kalkulasis_rapels_final')
			//	->where('YEAR("periode")','=',$periode)
			//	->where('nip','=',$nip)
			//	->count_all();

			//if($n == 0) {
			//	continue;
			//}

			/*$sql = "SELECT 
			SUM(gaji_pokok_4) as gaji_pokok,
			SUM(tunjangan_istri_4) as tunjangan_istri,	
			SUM(tunjangan_anak_4) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga_4) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum_4) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan_4) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural_4) as tunjangan_struktural,
			SUM(tunjangan_fungsional_4) as tunjangan_fungsional,
			SUM(tunjangan_beras_4) as tunjangan_beras,
			SUM(tunjangan_pph_4) as tunjangan_pph,
			SUM(pembulatan_4) as pembulatan,
			SUM(jumlah_kotor_4) as jumlah_kotor,
			SUM(potongan_iwp_4) as potongan_iwp,
			SUM(potongan_lain_4) as potongan_lain,
			SUM(potongan_cp_4) as potongan_cp,
			SUM(jumlah_potongan_4) as jumlah_potongan,
			SUM(jumlah_bersih_4) as jumlah_bersih,
			SUM(jumlah_bersih_bayar_4) as jumlah_bersih_bayar
			FROM kalkulasis_rapels_finals		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = $tunjangan_pph + $q->tunjangan_pph;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
			}*/

			// GAJI 14
			// $n = ORM::factory('kalkulasiebp3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->count_all();
			
			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$n = $this->db->get($this->tb_kalkulasip3k_eb)->num_rows();

			if ($n == 0) {
				$sgaji_pokok = $sgaji_pokok + 0;
				$stunjangan_istri = $stunjangan_istri + 0;
				$stunjangan_anak = $stunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $stunjangan_umum + 0;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $stunjangan_struktural + 0;
				$stunjangan_fungsional = $stunjangan_fungsional + 0;
				$stunjangan_beras = $stunjangan_beras + 0;
				$stunjangan_pph = $stunjangan_pph + 0;
				$spembulatan = 0;
				$sjumlah_kotor = $sjumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $spotongan_pensiun + 0;
				$spotongan_iwp = $spotongan_iwp + 0;
				$spotongan_lain = $spotongan_lain + 0;
				$spotongan_cp = $spotongan_cp + 0;
				$sjumlah_potongan = $sjumlah_potongan + 0;
				$sjumlah_bersih = $sjumlah_bersih + 0;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + 0;
				$saskes = $saskes + 0;
			}

			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM $this->tb_kalkulasip3k_eb		
			WHERE YEAR(periode) = ? AND nip = ?";

			// $query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			$query =  $this->db->query($sql, array($periode, $nip))->result();

			foreach ($query as $q) {
				$ssgaji_pokok = $sgaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $stunjangan_istri + $q->tunjangan_istri;
				$stunjangan_anak = $stunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $stunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $stunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $stunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $stunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = 0;
				$spembulatan = $spembulatan + $q->pembulatan;
				$sjumlah_kotor = $sjumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $spotongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $spotongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $spotongan_lain + $q->potongan_lain;
				$spotongan_cp = $spotongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $sjumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $sjumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $saskes + $q->askes;
			}

			// $pertama = ORM::factory('kalkulasip3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->order_by('periode', 'DESC')
			// 	->find();
			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$this->db->order_by('periode', 'DESC');
			$c = $this->db->get($this->tb_kalkulasip3k);

			if($c->num_rows()>0){
				$pertama = $c->row();
				$n_bea_jabatan = 0.05 * ($sjumlah_kotor - $stunjangan_pph);
				$n_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				if ($n_tht > 2400000) {
					$n_tht = 2400000;
				}
				$n_netto = $sjumlah_kotor - ($stunjangan_pph + $n_bea_jabatan + $n_tht);
				$n_ptkp = 54000000 + ((4500000 * $pertama->istri) + (4500000 * $pertama->anak));
				if (substr($nip, 14, 1) == 2) {
					$n_ptkp = 54000000;
				}

				$n_pkpj = $n_netto - $n_ptkp;
				if ($n_pkpj < 0) {
					$n_pkpj = 0;
				}
			}else{
				$n_bea_jabatan = 0;
				$n_tht = 0;
				$n_netto = 0;
				$n_ptkp = 99;
				$n_pkpj = 0;
			}

			

		// 	$arrValue = array(
		// 		"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
		// 		"'" . mysql_real_escape_string($periode) . "'",
		// 		$lokasi_id,
		// 		"'" . mysql_real_escape_string($lokasi_kode) . "'",
		// 		"'" . mysql_real_escape_string($lokasi_string) . "'",
		// 		"'" . mysql_real_escape_string($name) . "'",
		// 		"'" . mysql_real_escape_string($tanggal_lahir) . "'",
		// 		$nip,
		// 		$status_id,
		// 		"'" . mysql_real_escape_string($status_string) . "'",
		// 		$golongan_id,
		// 		"'" . mysql_real_escape_string($golongan_string) . "'",
		// 		"'" . mysql_real_escape_string($pertama->jabatan) . "'",
		// 		$marital_id,
		// 		"'" . mysql_real_escape_string($marital_string) . "'",
		// 		$istri,
		// 		$anak,
		// 		$jiwa,
		// 		"'" . mysql_real_escape_string($jiwa_string) . "'",
		// 		$sgaji_pokok,
		// 		$stunjangan_istri,
		// 		$stunjangan_anak,
		// 		$stunjangan_istri + $stunjangan_anak,
		// 		$sgaji_pokok + $stunjangan_istri + $stunjangan_anak,
		// 		$stunjangan_umum,
		// 		$stunjangan_umum_tambahan,
		// 		$stunjangan_struktural,
		// 		$stunjangan_fungsional,
		// 		$stunjangan_beras,
		// 		0,
		// 		$spembulatan,
		// 		$sjumlah_kotor,
		// 		$spotongan_bpjs_kesehatan,
		// 		$spotongan_pensiun,
		// 		$spotongan_iwp,
		// 		$spotongan_lain,
		// 		$stunjangan_beras,
		// 		$spotongan_cp,
		// 		$sjumlah_potongan,
		// 		$sjumlah_bersih,
		// 		$sjumlah_bersih_bayar,
		// 		$saskes,
		// 		"'" . mysql_real_escape_string($kelompok_gaji) . "'",
		// 		$n_bea_jabatan,
		// 		$n_tht,
		// 		$n_netto,
		// 		$n_ptkp,
		// 		$n_pkpj
		// 	);

		// 	$value .= "(" . implode(",", $arrValue) . "),";
		// }

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji',
		// 	'bea_jabatan',
		// 	'tht',
		// 	'penghasilan_netto',
		// 	'ptkp',
		// 	'pkpj'
		// );


		$arrFieldVal = array(
			'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
			'periode' 			=> $this->db->escape_str($periode),
			'lokasi_id' 		=> $lokasi_id,
			'lokasi_kode' 		=> $this->db->escape_str($lokasi_kode),
			'lokasi_string' 	=> $this->db->escape_str($lokasi_string),
			'name' 				=> $this->db->escape_str($name),
			'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
			'nip' 				=> $nip,
			'status_id' 		=> $status_id,
			'status_string' 	=> $this->db->escape_str($status_string),
			'golongan_id' 		=> $golongan_id,
			'golongan_string' 	=> $this->db->escape_str($golongan_string),
			'jabatan' 			=> $this->db->escape_str($jabatan),
			'marital_id' 		=> $marital_id,
			'marital_string' 	=> $this->db->escape_str($marital_string),
			'istri' 			=> $istri,
			'anak' 				=> $anak,
			'jiwa' 				=> $jiwa,
			'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
			'gaji_pokok' 		=> $ssgaji_pokok, 
			'tunjangan_istri' 	=> $stunjangan_istri,
			'tunjangan_anak' 	=> $stunjangan_anak,
			'jumlah_tunjangan_keluarga'=> $stunjangan_istri + $stunjangan_anak,
			'jumlah_penghasilan'=> $sgaji_pokok + $stunjangan_istri + $stunjangan_anak,
			'tunjangan_umum' 	=> $stunjangan_umum,
			'tunjangan_umum_tambahan'=> $stunjangan_umum_tambahan,
			'tunjangan_struktural'=> $stunjangan_struktural,
			'tunjangan_fungsional'=> $stunjangan_fungsional,
			'tunjangan_beras' 	=> $stunjangan_beras,
			'tunjangan_pph' 	=> 0,
			'pembulatan' 		=> $spembulatan,
			'jumlah_kotor' 		=> $sjumlah_kotor,
			'potongan_bpjs_kesehatan'=> $spotongan_bpjs_kesehatan,
			'potongan_pensiun' 	=> $spotongan_pensiun,
			'potongan_iwp' 		=> $spotongan_iwp,
			'potongan_lain' 	=> $spotongan_lain,
			'potongan_beras' 	=> $stunjangan_beras,
			'potongan_cp' 		=> $potongan_cp,
			'jumlah_potongan' 	=> $sjumlah_potongan,
			'jumlah_bersih' 	=> $sjumlah_bersih,
			'jumlah_bersih_bayar'=> $sjumlah_bersih_bayar,
			'askes' 			=> $saskes,
			'kelompok_gaji' 	=> $this->db->escape_str($kelompok_gaji),
			'bea_jabatan'		=> $n_bea_jabatan,
			'tht'				=> $n_tht,
			'penghasilan_netto' => $n_netto,
			'ptkp'				=> $n_ptkp,
			'pkpj'				=> $n_pkpj,			
		);

		$this->db->insert($this->tb_kalkulasip3k_tahunan, $arrFieldVal);

		
		

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO tahunangajip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		}

		// echo json_encode($arrX);
		// die();

		

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Gaji Tahunan PPPK Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}

	public function gaji_rapel_generate_table(){

		$tahun = $this->input->post('tahun'); //bulan acuan
		// $bulan_id = $this->input->post('bulan_id'); //bulan acuan
		$bulan_id2 = $this->input->post('bulan_ids'); //bulan rapel

		$total_gaji_pokok = 0; 
		$golongan = 0;
		$pokok_diterima = 0;
		$gaji_pokok_baru = 0;

		foreach($bulan_id2 as $b){

			$periodenya = $tahun.'-'.$b.'-01';

			// $table_sumber = 'simgaji_pegawai_'.$tahun.'_'.$b;
			$table_copy = 'simgaji_pegawai_rapelp3ks_'.$tahun.'_'.$b;

			// $table_kalkulasi_sumber = 'simgaji_pegawai_'.$tahun.'_'.$b;
			// $table__kalkulasi_copy = 'simgaji_pegawai_rapel_'.$tahun.'_'.$b;

			$this->db->from($table_copy);
			$pegawai = $this->db->get()->result();
			foreach($pegawai as $pgw){

				$this->db->where('golongan_id', $pgw->golongan_id);
				$this->db->where('masa',substr($pgw->masa_kerja, 0, 2));
				$this->db->where('lama',$pgw->gaji_pokok);
				$get = $this->db->get($this->gajipokoks);
				if($get->num_rows() > 0){
					$gaji_pokok_baru = $get->row()->baru;
				}

				$updates = array(
					'gaji_pokok' => $gaji_pokok_baru
				);
				$this->db->where('nip', $pgw->nip);
				$this->db->update($table_copy, $updates);

			}
		
		}
		

	}

	function gaji_bulanan_rapelp3k($periode, $arrLokasi){	
		// $this->gajipokoks = 'simgaji_gajipokoks';
		// $this->kalkulasi_rapel = 'simgaji_kalkulasis_rapel';
		// $this->kalkulasi_rapel_perbulan = 'simgaji_kalkulasis_rapel_perbulan';
		
		// echo 'asd';
		// die();
		error_reporting(0);
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		// if()
		// $table_sumber = 'simgaji_pegawai_'.$tahun.'_'.$b;
		// $table_copy = 'simgaji_pegawai_rapel_'.$tahun.'_'.$b;

		// echo $periode;
		// die();

		// 2023-01-01
		$dateString = "2023-01-01";
		$dateParts = explode("-", $periode);
		
		$tahun = $dateParts[0];
		$b = $dateParts[1];
		$d = $dateParts[2];

		$this->pegawai_rapelp3k = $this->pegawai_rapelp3k.'_'.$tahun.'_'.$b;

		// $this->pegawai_rapel = 'simgaji_pegawai_rapel';
		$this->db->select($this->pegawai_rapelp3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golongan.'.kode as golongan_kode');
		$this->db->select($this->tb_status.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->pegawai_rapelp3k);
		$this->db->join($this->tb_eselon, $this->pegawai_rapelp3k.'.eselon_id ='.$this->tb_eselon.'.id');
        $this->db->join($this->tb_kedudukan, $this->pegawai_rapelp3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_fungsional, $this->pegawai_rapelp3k.'.fungsional_id ='.$this->tb_fungsional.'.id');
		$this->db->join($this->tb_golongan, $this->pegawai_rapelp3k.'.golongan_id ='.$this->tb_golongan.'.id');
		$this->db->join($this->tb_status, $this->pegawai_rapelp3k.'.status_id ='.$this->tb_status.'.id');
		$this->db->join($this->tb_marital, $this->pegawai_rapelp3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->pegawai_rapelp3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
        $this->db->where_in('lokasi_gaji', $arrLokasi);
        // $this->db->where_in('status_id', array(1,2,7,8,9));
        $pegawais = $this->db->get()->result();

		// echo json_encode($pegawais);
		// die();
        
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;

			// $this->db->where('id', $pegawai->eselon_id);
			// $eselon_ = $this->db->get($this->tb_eselon)->row();

			// $this->db->where('id', $pegawai->fungsional_id);
			// $fungsional_ = $this->db->get($this->tb_fungsional)->row();

			// $this->db->where('id', $pegawai->kedudukan_id);
			// $kedudukan_ = $this->db->get($this->tb_kedudukan)->row();
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan_name	;	
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional_name;
				}
				else {
					if($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name	;	
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

					
					//$tunjangan_fungsional = 0;
				} else {
					
					if($pegawai->kedudukan_id != 72) {

						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();

					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}

				
				// Kondisi Khusus untk Tunjangan Sandiman, Nominal Tunj Fungs dipindahkan ke Nominal $tunjangan_umum_tambahan di cetakan disebut sbg Tunj Kompensasi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) { //Jika Ahli Sandi tingkat 1-6
					if($pegawai->kompensasi_id >= 879 AND $pegawai->kompensasi_id <= 891) { //Jika Sandiman ( tunjangan kompensasi pemula-madya) dan merupakan JFT
						//Diisi tunjangan funstionalnya sesuai nominal
						//Diisi tunjangan kompensasinya di tunj umum tambahan
						//Dinolkan tunjangan umumnya karena merupakan fungsional umum
					

						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						
						
					$tunjangan_umum = 0;
					} else { //Kalau yang ini fungsional tertentu (JFT) tapi bukan sandiman
						$tunjangan_fungsional = 0;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						}
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 

				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// $jumlah_potongan = round($potongan_lain + $potongan_iwp);
				// $gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum + $tunjangan_umum_tambahan);
				// $gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// $jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras + $tunjangan_umum_tambahan);
				// $jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->kalkulasi_rapel_perbulanp3k, $arrFieldVal);

			}
			
		}

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi'. $periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi/rapel');

		
	}

	function gaji_rapelp3k($arrLokasi){

		$this->gajipokoksp3k = 'simgaji_gajipokoks';
		$this->simgaji_kalkulasisp3k = 'simgaji_kalkulasisp3ks';
		$this->simgaji_rapel_perbulanp3k = 'simgaji_kalkulasis_rapel_perbulanp3ks';
		$this->simgaji_rapel_perbulan_selisihp3k = 'simgaji_kalkulasis_rapel_perbulan_selisihp3ks';
		$this->simgaji_rapelp3k = 'simgaji_kalkulasis_rapelp3ks';

		$tahun = $this->input->post('tahun'); //bulan acuan
		$bulan_id2 = $this->input->post('bulan_id2'); //bulan rapel

		// echo json_encode($bulan_id2);
		// die();

		$periodes = array();
		$periode_date = "";

    	foreach ($bulan_id2 as $bulan) {
			$periode = $tahun . '-' . $bulan . '-01';
			$periodeep[] = $periode;
			$periode_date .= $periode . ', ';

			// Dapatkan data gaji lama dan baru
			$dataGajiLama = $this->db
            ->select('*')
            ->from($this->simgaji_kalkulasis)
            ->where('periode', $periode)
			->where_in('lokasi_id', $arrLokasi)
            ->get()
            ->result();

        	$dataGajiBaru = $this->db
            ->select('*')
            ->from($this->simgaji_rapel_perbulan)
            ->where('periode', $periode)
			->where_in('lokasi_id', $arrLokasi)
            ->get()
            ->result();


			foreach ($dataGajiLama as $gajiLama) {
				$nip = $gajiLama->nip;
				$gajiLamaValue = $gajiLama->gaji_pokok;

				//inisiasi gaji lama
				$istri = $gajiLama->istri;
				$anak = $gajiLama->anak;
				$jiwa = $gajiLama->jiwa;
				$jiwa_string = $gajiLama->jiwa_string;
				$gaji_pokok = $gajiLama->gaji_pokok;
				$tunjangan_istri = $gajiLama->tunjangan_istri;
				$tunjangan_anak = $gajiLama->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $gajiLama->jumlah_tunjangan_keluarga;
				$jumlah_penghasilan = $gajiLama->jumlah_penghasilan;
				$tunjangan_umum = $gajiLama->tunjangan_umum;
				$tunjangan_umum_tambahan = $gajiLama->tunjangan_umum_tambahan;
				$tunjangan_struktural = $gajiLama->tunjangan_struktural;
				$tunjangan_fungsional = $gajiLama->tunjangan_fungsional;
				$tunjangan_beras = $gajiLama->tunjangan_beras;
				$tunjangan_pph = $gajiLama->tunjangan_pph;
				$pembulatan = $gajiLama->pembulatan;
				$jumlah_kotor = $gajiLama->jumlah_kotor;
				$potongan_bpjs_kesehatan = $gajiLama->potongan_bpjs_kesehatan;
				$potongan_pensiun = $gajiLama->potongan_pensiun;
				$potongan_iwp = $gajiLama->potongan_iwp;
				$potongan_lain = $gajiLama->potongan_lain;
				$potongan_beras = $gajiLama->potongan_beras;
				$potongan_cp = $gajiLama->potongan_cp;
				$jumlah_potongan = $gajiLama->jumlah_potongan;
				$jumlah_bersih = $gajiLama->jumlah_bersih;
				$jumlah_bersih_bayar = $gajiLama->jumlah_bersih_bayar;
				$askes = $gajiLama->askes;
	
				// Temukan data gaji baru yang sesuai
				foreach ($dataGajiBaru as $gajiBaru) {
					if ($gajiBaru->nip === $nip) {
						$gajiBaruValue = $gajiBaru->gaji_pokok;

						// $selisihGaji = $gajiBaruValue - $gajiLamaValue;

						//inisiasi gaji baru
						$istriB= $gajiBaru->istri;
						$anakB= $gajiBaru->anak;
						$jiwaB= $gajiBaru->jiwa;
						$jiwa_stringB= $gajiBaru->jiwa_string;
						$gaji_pokokB= $gajiBaru->gaji_pokok;
						$tunjangan_istriB= $gajiBaru->tunjangan_istri;
						$tunjangan_anakB= $gajiBaru->tunjangan_anak;
						$jumlah_tunjangan_keluargaB= $gajiBaru->jumlah_tunjangan_keluarga;
						$jumlah_penghasilanB= $gajiBaru->jumlah_penghasilan;
						$tunjangan_umumB= $gajiBaru->tunjangan_umum;
						$tunjangan_umum_tambahanB= $gajiBaru->tunjangan_umum_tambahan;
						$tunjangan_strukturalB= $gajiBaru->tunjangan_struktural;
						$tunjangan_fungsionalB= $gajiBaru->tunjangan_fungsional;
						$tunjangan_berasB= $gajiBaru->tunjangan_beras;
						$tunjangan_pphB= $gajiBaru->tunjangan_pph;
						$pembulatanB= $gajiBaru->pembulatan;
						$jumlah_kotorB= $gajiBaru->jumlah_kotor;
						$potongan_bpjs_kesehatanB= $gajiBaru->potongan_bpjs_kesehatan;
						$potongan_pensiunB= $gajiBaru->potongan_pensiun;
						$potongan_iwpB= $gajiBaru->potongan_iwp;
						$potongan_lainB= $gajiBaru->potongan_lain;
						$potongan_berasB= $gajiBaru->potongan_beras;
						$potongan_cpB= $gajiBaru->potongan_cp;
						$jumlah_potonganB= $gajiBaru->jumlah_potongan;
						$jumlah_bersihB= $gajiBaru->jumlah_bersih;
						$jumlah_bersih_bayarB= $gajiBaru->jumlah_bersih_bayar;
						$askesB= $gajiBaru->askes;
	
						// Hitung selisih gaji
						$periodeS = $gajiBaru->periode;
						$lokasi_idS = $gajiBaru->lokasi_id;
						$lokasi_kodeS =  $gajiBaru->lokasi_kode;
						$lokasi_stringS =  $gajiBaru->lokasi_string;
						$nameS =  $gajiBaru->name;
						$tanggal_lahirS =  $gajiBaru->tanggal_lahir;
						$nipS =  $gajiBaru->nip;
						$status_idS =  $gajiBaru->status_id;
						$status_stringS =  $gajiBaru->status_string;
						$golongan_idS =  $gajiBaru->golongan_id;
						$golongan_stringS =  $gajiBaru->golongan_string;
						$jabatanS =  $gajiBaru->jabatan;
						$marital_idS =  $gajiBaru->marital_id;
						$marital_stringS =  $gajiBaru->marital_string;
						$istriS = $istriB - $istri;
						$anakS = $anakB - $anak;
						$jiwaS = $jiwaB - $jiwa;
						$jiwa_stringS = $gajiBaru->jiwa_string;
						$gaji_pokokS = $gaji_pokokB - $gaji_pokok;
						$tunjangan_istriS = $tunjangan_istriB - $tunjangan_istri;
						$tunjangan_anakS = $tunjangan_anakB - $tunjangan_anak;
						$jumlah_tunjangan_keluargaS = $jumlah_tunjangan_keluargaB - $jumlah_tunjangan_keluarga;
						$jumlah_penghasilanS = $jumlah_penghasilanB - $jumlah_penghasilan;
						$tunjangan_umumS = $tunjangan_umumB - $tunjangan_umum;
						$tunjangan_umum_tambahanS = $tunjangan_umum_tambahanB - $tunjangan_umum_tambahan;
						$tunjangan_strukturalS = $tunjangan_strukturalB - $tunjangan_struktural;
						$tunjangan_fungsionalS = $tunjangan_fungsionalB - $tunjangan_fungsional;
						$tunjangan_berasS = $tunjangan_berasB - $tunjangan_beras;
						$tunjangan_pphS = $tunjangan_pphB - $tunjangan_pph;
						$pembulatanS = $pembulatanB - $pembulatan;
						$jumlah_kotorS = $jumlah_kotorB - $jumlah_kotor;
						$potongan_bpjs_kesehatanS = $potongan_bpjs_kesehatanB - $potongan_bpjs_kesehatan;
						$potongan_pensiunS = $potongan_pensiunB - $potongan_pensiun;
						$potongan_iwpS = $potongan_iwpB - $potongan_iwp;
						$potongan_lainS = $potongan_lainB - $potongan_lain;
						$potongan_berasS = $potongan_berasB - $potongan_beras;
						$potongan_cpS = $potongan_cpB - $potongan_cp;
						$jumlah_potonganS = $jumlah_potonganB - $jumlah_potongan;
						$jumlah_bersihS = $jumlah_bersihB - $jumlah_bersih;
						$jumlah_bersih_bayarS = $jumlah_bersih_bayarB - $jumlah_bersih_bayar;
						$askesS = $askesB - $askes;
						$kelompok_gajiS = $gajiBaru->kelompok_gaji;
	
						// Masukkan data selisih ke tabel simgaji_kalkulasis_rapel_perbulan_selisih
						// $dataSelisih = array(
						// 	'tanggal' => date('Y-m-d'), // Tanggal saat ini
						// 	'periode' => $periode,
						// 	'nip' => $nip,
						// 	'selisih_gaji' => $selisihGaji
						// );

						$dataSelisih = array(
							'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
							'periode' 			=> $this->db->escape_str($periodeS),
							'lokasi_id' 		=> $lokasi_idS,
							'lokasi_kode' 		=> $lokasi_kodeS,
							'lokasi_string' 	=> $lokasi_stringS,
							'name' 				=> $nameS,
							'tanggal_lahir' 	=> $tanggal_lahirS,
							'nip' 				=> $nipS,
							'status_id' 		=> $status_idS,
							'status_string' 	=> $status_stringS,
							'golongan_id' 		=> $golongan_idS,
							'golongan_string' 	=> $golongan_stringS,
							'jabatan' 			=> $jabatanS,
							'marital_id' 		=> $marital_idS,
							'marital_string' 	=> $marital_stringS,
							'istri' 			=> $jumlah_istriS ?? 0,
							'anak' 				=> $jumlah_anakS ?? 0,
							'jiwa' 				=> $total_jiwaS ?? 0,
							'jiwa_string' 		=> $jiwa_stringS ?? 0,
							'gaji_pokok' 		=> $gaji_pokokS,
							'tunjangan_istri' 	=> $tunjangan_istriS,
							'tunjangan_anak' 	=> $tunjangan_anakS,
							'jumlah_tunjangan_keluarga'=> $jumlah_tunjangan_keluargaS,
							'jumlah_penghasilan'=> $jumlah_penghasilanS,
							'tunjangan_umum' 	=> $tunjangan_umumS,
							'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahanS,
							'tunjangan_struktural'=> $tunjangan_strukturalS,
							'tunjangan_fungsional'=> $tunjangan_fungsionalS,
							'tunjangan_beras' 	=> $tunjangan_berasS,
							'tunjangan_pph' 	=> $pph_bulanS ?? 0,
							'pembulatan' 		=> $pembulatanS,
							'jumlah_kotor' 		=> $jumlah_kotorS,
							'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatanS,
							'potongan_pensiun' 	=> $potongan_pensiunS,
							'potongan_iwp' 		=> $potongan_iwpS,
							'potongan_lain' 	=> $potongan_lainS,
							'potongan_beras' 	=> $potongan_berasS,
							'potongan_cp' 		=> $potongan_cpS,
							'jumlah_potongan' 	=> $jumlah_potonganS,
							'jumlah_bersih' 	=> $jumlah_bersihS,
							'jumlah_bersih_bayar'=> $jumlah_bersih_bayarS,
							'askes' 			=> $askesS,
							'kelompok_gaji' 	=> $kelompok_gajiS		
						);
	
						// Insert data selisih gaji ke dalam tabel simgaji_kalkulasis_rapel_perbulan_selisih
						$this->db->insert($this->simgaji_rapel_perbulan_selisihp3k, $dataSelisih);
					}
				}
			}

    	}

		$periode_datenya = rtrim($periode_date, ', ');



		// Hitung jumlah selisih gaji per NIP dari semua periode
        $totalSelisihPerNIP = $this->db
			->select('*')
			->select('SUM(gaji_pokok) as total_gaji_pokok')
			->select('SUM(tunjangan_istri) as total_tunjangan_istri')
			->select('SUM(tunjangan_anak) as total_tunjangan_anak')
			->select('SUM(jumlah_tunjangan_keluarga) as total_jumlah_tunjangan_keluarga')
			->select('SUM(jumlah_penghasilan) as total_jumlah_penghasilan')
			->select('SUM(tunjangan_umum) as total_tunjangan_umum')
			->select('SUM(tunjangan_umum_tambahan) as total_tunjangan_umum_tambahan')
			->select('SUM(tunjangan_struktural) as total_tunjangan_struktural')
			->select('SUM(tunjangan_fungsional) as total_tunjangan_fungsional')
			->select('SUM(tunjangan_beras) as total_tunjangan_beras')
			->select('SUM(tunjangan_pph) as total_tunjangan_pph')
			->select('SUM(pembulatan) as total_pembulatan')
			->select('SUM(jumlah_kotor) as total_jumlah_kotor')
			->select('SUM(potongan_bpjs_kesehatan) as total_potongan_bpjs_kesehatan')
			->select('SUM(potongan_pensiun) as total_potongan_pensiun')
			->select('SUM(potongan_iwp) as total_potongan_iwp')
			->select('SUM(potongan_lain) as total_potongan_lain')
			->select('SUM(potongan_beras) as total_potongan_beras')
			->select('SUM(potongan_cp) as total_potongan_cp')
			->select('SUM(jumlah_potongan) as total_jumlah_potongan')
			->select('SUM(jumlah_bersih) as total_jumlah_bersih')
			->select('SUM(jumlah_bersih_bayar) as total_jumlah_bersih_bayar')
			->select('SUM(askes) as total_askes')
            ->from($this->simgaji_rapel_perbulan_selisih)
            ->where_in('periode', $periodeep)
            ->group_by('nip')
            ->get()
            ->result();

        // Masukkan jumlah selisih gaji per NIP ke dalam tabel simgaji_kalkulasis_rapel
        foreach ($totalSelisihPerNIP as $total) {

            // Masukkan data rekap ke tabel simgaji_kalkulasis_rapel
            // $dataRekap = array(
            //     'tanggal_periode' => $periode,
            //     'nip' => $nip,
            //     'gaji_pokok' => $totalSelisih
            // );
			// $periode_datenya = $periode_datenya;

			// $periode_datenya = rtrim($periode_date, ', ');

			// Hitung selisih gaji
			$periodeT = $periode_datenya;
			$lokasi_idT = $total->lokasi_id;
			$lokasi_kodeT =  $total->lokasi_kode;
			$lokasi_stringT =  $total->lokasi_string;
			$nameT =  $total->name;
			$tanggal_lahirT =  $total->tanggal_lahir;
			$nipT =  $total->nip;
			$status_idT =  $total->status_id;
			$status_stringT =  $total->status_string;
			$golongan_idT =  $total->golongan_id;
			$golongan_stringT =  $total->golongan_string;
			$jabatanT =  $total->jabatan;
			$marital_idT =  $total->marital_id;
			$marital_stringT =  $total->marital_string;
			$istriT = $istriB - $istri;
			$anakT = $anakB - $anak;
			$jiwaT = $jiwaB - $jiwa;
			$jiwa_stringT = $total->jiwa_string;
			$gaji_pokokT = $total->total_gaji_pokok;
			$tunjangan_istriT = $total->total_tunjangan_istri;
			$tunjangan_anakT = $total->total_tunjangan_anak;
			$jumlah_tunjangan_keluargaT = $total->total_jumlah_tunjangan_keluarga;
			$jumlah_penghasilanT = $total->total_jumlah_penghasilan;
			$tunjangan_umumT = $total->total_tunjangan_umum;
			$tunjangan_umum_tambahanT = $total->total_tunjangan_umum_tambahan;
			$tunjangan_strukturalT = $total->total_tunjangan_struktural;
			$tunjangan_fungsionalT = $total->total_tunjangan_fungsional;
			$tunjangan_berasT = $total->total_tunjangan_beras;
			$tunjangan_pphT = $total->total_tunjangan_pph;
			$pembulatanT = $total->total_pembulatan;
			$jumlah_kotorT = $total->total_jumlah_kotor;
			$potongan_bpjs_kesehatanT = $total->total_potongan_bpjs_kesehatan;
			$potongan_pensiunT = $total->total_potongan_pensiun;
			$potongan_iwpT = $total->total_potongan_iwp;
			$potongan_lainT = $total->total_potongan_lain;
			$potongan_berasT = $total->total_potongan_beras;
			$potongan_cpT = $total->total_potongan_cp;
			$jumlah_potonganT = $total->total_jumlah_potongan;
			$jumlah_bersihT = $total->total_jumlah_bersih;
			$jumlah_bersih_bayarT = $total->total_jumlah_bersih_bayar;
			$askesT = $total->total_askes;
			$kelompok_gajiT = $total->kelompok_gaji;

			$dataRapel = array(
				'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
				'periode' 			=> $periode_datenya,
				'lokasi_id' 		=> $lokasi_idT,
				'lokasi_kode' 		=> $lokasi_kodeT,
				'lokasi_string' 	=> $lokasi_stringT,
				'name' 				=> $nameT,
				'tanggal_lahir' 	=> $tanggal_lahirT,
				'nip' 				=> $nipT,
				'status_id' 		=> $status_idT,
				'status_string' 	=> $status_stringT,
				'golongan_id' 		=> $golongan_idT,
				'golongan_string' 	=> $golongan_stringT,
				'jabatan' 			=> $jabatanT,
				'marital_id' 		=> $marital_idT,
				'marital_string' 	=> $marital_stringT,
				'istri' 			=> $jumlah_istriS ?? 0,
				'anak' 				=> $jumlah_anakS ?? 0,
				'jiwa' 				=> $total_jiwaS ?? 0,
				'jiwa_string' 		=> $jiwa_stringS ?? 0,
				'gaji_pokok' 		=> $gaji_pokokT,
				'tunjangan_istri' 	=> $tunjangan_istriT,
				'tunjangan_anak' 	=> $tunjangan_anakT,
				'jumlah_tunjangan_keluarga'=> $jumlah_tunjangan_keluargaT,
				'jumlah_penghasilan'=> $jumlah_penghasilanT,
				'tunjangan_umum' 	=> $tunjangan_umumT,
				'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahanT,
				'tunjangan_struktural'=> $tunjangan_strukturalT,
				'tunjangan_fungsional'=> $tunjangan_fungsionalT,
				'tunjangan_beras' 	=> $tunjangan_berasT,
				'tunjangan_pph' 	=> $pph_bulanS ?? 0,
				'pembulatan' 		=> $pembulatanT,
				'jumlah_kotor' 		=> $jumlah_kotorT,
				'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatanT,
				'potongan_pensiun' 	=> $potongan_pensiunT,
				'potongan_iwp' 		=> $potongan_iwpT,
				'potongan_lain' 	=> $potongan_lainT,
				'potongan_beras' 	=> $potongan_berasT,
				'potongan_cp' 		=> $potongan_cpT,
				'jumlah_potongan' 	=> $jumlah_potonganT,
				'jumlah_bersih' 	=> $jumlah_bersihT,
				'jumlah_bersih_bayar'=> $jumlah_bersih_bayarT,
				'askes' 			=> $askesT,
				'kelompok_gaji' 	=> $kelompok_gajiS		
			);

			$this->db->insert($this->simgaji_rapelp3k, $dataRapel);

            // // Cek apakah data dengan NIP dan periode tertentu sudah ada di tabel simgaji_kalkulasis_rapel
            // $existingData = $this->db
            //     ->where('tanggal_periode', $periode)
            //     ->where('nip', $nip)
            //     ->get('simgaji_kalkulasis_rapel')
            //     ->row();

            // if ($existingData) {
            //     // Jika sudah ada, update data dengan jumlah selisih gaji
            //     $dataRekap['gaji_pokok'] += $existingData->gaji_pokok;
            //     $this->db
            //         ->where('tanggal_periode', $periode)
            //         ->where('nip', $nip)
            //         ->update('simgaji_kalkulasis_rapel', $dataRekap);
            // } else {
            //     // Jika belum ada, masukkan data baru
            //     $this->db->insert('simgaji_kalkulasis_rapel', $dataRekap);
            // }
        }

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi'. $periode_datenya
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi/rapel');

	}

	





}