<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekapadmin_tpp_pot extends CI_Controller{
	public function __construct(){
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->verifikasi = 'simgaji_history_tpp_acc';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Rekapadmin_tpp_pot_model');
    }

    public function index(){
    	$data_header['session'] = $this->session->all_userdata();
		$nip_sess = $this->session->userdata('B_02B');
		$role = $this->session->userdata('role');

		$b = getDataPeriodeTPP()->bulan;
		$t = getDataPeriodeTPP()->tahun;

		$get = $this->Rekapadmin_tpp_pot_model->get_data();

		$datatable = array();
		foreach ($get as $key => $value) {

			$sisa = totalDataTppPot($b, $t, $value->kode) - totalDataTppPotAcc($b, $t, $value->kode);
			if($sisa != 0){
				$s = '<span class="badge badge-danger">'.$sisa.'</span>';
			} else {
				$s = $sisa;
			}

			$datatable[$key] = array(
				'kode' => $value->kode,
				'nama' => $value->name,
				'total_data' => totalDataTppPot($b, $t, $value->kode),
				'total_acc' => totalDataTppPotAcc($b, $t, $value->kode),
				'total_sisa' => $s
			);
		}

		$data['filter'] = array(
			'bl' => $b,
			'th' => $t
		);

		$data['datatable'] = $datatable;
		

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('rekap_tpp_pot/index', $data);

		// echo json_encode($data);
    }
}