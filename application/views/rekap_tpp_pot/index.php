<?php
$A_01 = $this->session->userdata('A_01');
$role = $this->session->userdata('role');
$lokasis_id = $this->session->userdata('id_lokasis');
?>
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">

        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <span class="breadcrumb-item active">Verifikasi TPP Potongan Pegawai</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">SKPD</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form action="" method="get">

                    <div class="form-group row col-md-12">
                        <div class="col-md-4">
                            <input type="hidden" id="a01" value="<?=$A_01?>">
                            <select class="form-control select2" name="unor" id="unor" required>
                                <option value="">::SKPD::</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="bul" id="bul" required>
                                <option value="">::Pilih::</option>
                                <option value="1" <?php if($filter['bl'] == '1'){echo "selected";} ?>>Januari</option>
                                <option value="2" <?php if($filter['bl'] == '2'){echo "selected";} ?>>Februari</option>
                                <option value="3" <?php if($filter['bl'] == '3'){echo "selected";} ?>>Maret</option>
                                <option value="4" <?php if($filter['bl'] == '4'){echo "selected";} ?>>April</option>
                                <option value="5" <?php if($filter['bl'] == '5'){echo "selected";} ?>>Mei</option>
                                <option value="6" <?php if($filter['bl'] == '6'){echo "selected";} ?>>Juni</option>
                                <option value="7" <?php if($filter['bl'] == '7'){echo "selected";} ?>>Juli</option>
                                <option value="8" <?php if($filter['bl'] == '8'){echo "selected";} ?>>Agustus</option>
                                <option value="9" <?php if($filter['bl'] == '9'){echo "selected";} ?>>September</option>
                                <option value="10" <?php if($filter['bl'] == '10'){echo "selected";} ?>>Oktober</option>
                                <option value="11" <?php if($filter['bl'] == '11'){echo "selected";} ?>>November</option>
                                <option value="12" <?php if($filter['bl'] == '12'){echo "selected";} ?>>Desember</option>
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="thn" id="thn" required>
                                <option value="2022" <?php if($filter['th'] == '2022'){echo "selected";} ?>>2022</option>
                                <option value="2023" <?php if($filter['th'] == '2023'){echo "selected";} ?>>2023</option>
                                <option value="2024" <?php if($filter['th'] == '2024'){echo "selected";} ?>>2024</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn bg-blue ml-3">Cari 
                            <i class="icon-search ml-2"></i>
                            </button>
                        </div>
                    </div>
                    
                </form>
            </div>

        </div> -->

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data Periode berjalan</h5>
                <div class="header-elements">
                    <!-- <input type="checkbox" name="cetak100" id="cetak100">Cetak yang tidak 100% saja -->
                </div>
            </div>
                
            <div class="table-responsive" id="tableDefault">
                <table class="table table-striped table-condensed table-hover" id="tabledt">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>SKPD</th>
                            <th><i class="icon-cog"></i> Total</th>
                            <th><i style="color:green" class="icon-checkmark-circle"></i> Approve</th>
                            <th><i style="color:red" class="icon-cancel-circle2"></i> Belum</th>
                            <!-- <th><input type="checkbox" id="check-all"></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->



    <!-- START MODAL DATA -->
    <div id="modalData" class="modal fade" tabindex="-1" style="display: none;" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="icon-wallet mr-2"></i> &nbsp;Detail TPP Potongan</h5>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>

                <div class="modal-body">
                    <form id="form_veriftpp_pot">
                        <input type="hidden" name="nips" id="nips">

                        <div class="form-group row">
                            <table class="table table-hover table-striped table-bordered">
                                <tbody>
                                    <tr>
                                        <td>NIP</td>
                                        <td id="nip"></td>
                                    </tr>
                                    <tr>
                                        <td>NAMA</td>
                                        <td id="nama"></td>
                                    </tr>
                                    <tr>
                                        <td>NOMINAL TPP</td>
                                        <td id="nominal_tpp"></td>
                                    </tr>
                                    <tr>
                                        <td>NOMINAL TPP BEBAN KERJA KHUSUS</td>
                                        <td id="nominal_tpp_bebankerjakhusus"></td>
                                    </tr>
                                    <tr>
                                        <td>NOMINAL TPP TEMPAT</td>
                                        <td id="nominal_tpp_tempat"></td>
                                    </tr>
                                    <tr>
                                        <td>NOMINAL TPP KONDISI</td>
                                        <td id="nominal_tpp_kondisi"></td>
                                    </tr>
                                    <tr>
                                        <td>NOMINAL PENGURANGAN</td>
                                        <td id="nominal_pengurangan"></td>
                                    </tr>
                                    <tr>
                                        <td>TOTAL</td>
                                        <td id="total"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <!-- <div class="form-group row">
                            <label class="col-form-label col-sm-3">Aksi</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="verif" id="verif" required>
                                    <option value="">:: Pilih ::</option>
                                    <option value="1">Verifikasi</option>
                                </select>
                            </div>
                        </div> -->
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                    <!-- <button type="submit" class="btn btn-sm bg-success">Simpan</button> -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL DATA -->

    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    <script>
        var detailTppPotUrl = '<?php echo base_url('verifikasi_tpp_pot/detailbynipblth') ?>'
        var verifUrl = '<?php echo base_url('verifikasi_tpp_pot/verif_action') ?>'
    </script>

    <script type="text/javascript" src="<?=base_url('assets/js/')?>verifikasi_tpp_pot.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
        // var oTable = $('#tabledt').dataTable({
        //     stateSave: true
        // });

        var tabel = $('#tabledt').DataTable({
            'stateSave': true,
            'destroy'     : true,
            'paging'      : false,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : false,
            'info'        : true,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: null, sortable : false, searceable : false },
                { data: 'kode' },
                { data: 'nama' },
                { data: 'total_data' },
                { data: 'total_acc' },
                { data: 'total_sisa' },
            ],
            // columnDefs: [{ }],
        });

        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();


        var allPages = tabel.cells().nodes();

        $('#check-all').click(function () {
            if($(this).is(":checked")) {
                $(allPages).find('input[type="checkbox"]').prop('checked', true);
            } else {
                $(allPages).find('input[type="checkbox"]').prop('checked', false);
            }
            $(this).toggleClass('allChecked');
        })

        $("#btn-act").click(function(){ // Ketika user mengklik tombol delete
        var confirm = window.confirm("Apakah anda yakin akan menyimpan data yang telah anda pilih?"); // Buat sebuah alert konfirmasi
        
        if(confirm) // Jika user mengklik tombol "Ok"
            $("#form-act").submit(); // Submit form
        });
        
    });
</script>

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }
</script>

</body>
</html>
