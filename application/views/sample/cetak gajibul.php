
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="http://10.1.74.88/gaji/assets/css/cetak.css" rel="stylesheet" type="text/css" />
<link href="http://10.1.74.88/gaji/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class='cetak'><table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0">
<tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;1 + 1 + 2 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.949.400<br />494.940<br />197.976<br />5.642.316</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />3.250.000<br />0<br />289.680<br />144.891</td>
				<td style="width:3.3cm" align='right' valign='top'>14<br />9.326.901</td>
				<td style="width:3.7cm" align='right' valign='top'>88.924<br />451.386<br />0<br />0<br />0<br />540.310</td>
				<td style="width:3cm" align='right'>8.786.591</td>
				<td style="width:3.8cm" align='right'>144.891</td>
				<td style="width:2cm" align='right'>8.641.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11000000 &nbsp;&nbsp;&nbsp;BADAN KEPEGAWAIAN DAERAH<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>Drs. WISNU ZAROH, M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>26-05-1963</td>
						</tr>
						<tr>
							<td colspan='2'>196305261995031002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV C</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA BADAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.949.400<br />494.940<br />197.976<br />5.642.316</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />3.250.000<br />0<br />289.680<br />144.891</td>
				<td style="width:3.3cm" align='right' valign='top'>14<br />9.326.901</td>
				<td style="width:3.7cm" align='right' valign='top'>88.924<br />451.386<br />0<br />0<br />0<br />540.310</td>
				<td style="width:3cm" align='right'>8.786.591</td>
				<td style="width:2.8cm" align='right'>144.891</td>
				<td style="width:3.8cm" align='right'>8.641.700</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;1 + 1 + 2 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.949.400<br />494.940<br />197.976<br />5.642.316</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />3.250.000<br />0<br />289.680<br />144.891</td>
				<td style="width:3.3cm" align='right' valign='top'>14<br />9.326.901</td>
				<td style="width:3.7cm" align='right' valign='top'>88.924<br />451.386<br />0<br />0<br />0<br />540.310</td>
				<td style="width:3cm" align='right'>8.786.591</td>
				<td style="width:2.8cm" align='right'>144.891</td>
				<td style="width:3.8cm" align='right'>8.641.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;1 + 1 + 2 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.949.400<br />494.940<br />197.976<br />5.642.316</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />3.250.000<br />0<br />289.680<br />144.891</td>
				<td style="width:3.3cm" align='right' valign='top'>14<br />9.326.901</td>
				<td style="width:3.7cm" align='right' valign='top'>88.924<br />451.386<br />0<br />0<br />0<br />540.310</td>
				<td style="width:3cm" align='right'>8.786.591</td>
				<td style="width:3.8cm" align='right'>144.891</td>
				<td style="width:2cm" align='right'>8.641.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;1 + 1 + 1 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.898.100<br />489.810<br />97.962<br />5.485.872</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />217.260<br />58.250</td>
				<td style="width:3.3cm" align='right' valign='top'>97<br />7.021.479</td>
				<td style="width:3.7cm" align='right' valign='top'>67.459<br />438.870<br />0<br />0<br />0<br />506.329</td>
				<td style="width:3cm" align='right'>6.515.150</td>
				<td style="width:3.8cm" align='right'>58.250</td>
				<td style="width:2cm" align='right'>6.456.900</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001000 &nbsp;&nbsp;&nbsp;SEKRETARIAT<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>Drs. EKO SUPRAYITNO, MM</td>
						</tr>
						<tr>
							<td colspan='2'>25-09-1967</td>
						</tr>
						<tr>
							<td colspan='2'>196709251993031004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV B</td>
						</tr>
						<tr>
							<td colspan='2'>SEKRETARIS</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>4.898.100<br />489.810<br />97.962<br />5.485.872</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />217.260<br />58.250</td>
				<td style="width:3.3cm" align='right' valign='top'>97<br />7.021.479</td>
				<td style="width:3.7cm" align='right' valign='top'>67.459<br />438.870<br />0<br />0<br />0<br />506.329</td>
				<td style="width:3cm" align='right'>6.515.150</td>
				<td style="width:2.8cm" align='right'>58.250</td>
				<td style="width:3.8cm" align='right'>6.456.900</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;1 + 1 + 1 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.898.100<br />489.810<br />97.962<br />5.485.872</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />217.260<br />58.250</td>
				<td style="width:3.3cm" align='right' valign='top'>97<br />7.021.479</td>
				<td style="width:3.7cm" align='right' valign='top'>67.459<br />438.870<br />0<br />0<br />0<br />506.329</td>
				<td style="width:3cm" align='right'>6.515.150</td>
				<td style="width:2.8cm" align='right'>58.250</td>
				<td style="width:3.8cm" align='right'>6.456.900</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;1 + 1 + 1 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.898.100<br />489.810<br />97.962<br />5.485.872</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />217.260<br />58.250</td>
				<td style="width:3.3cm" align='right' valign='top'>97<br />7.021.479</td>
				<td style="width:3.7cm" align='right' valign='top'>67.459<br />438.870<br />0<br />0<br />0<br />506.329</td>
				<td style="width:3cm" align='right'>6.515.150</td>
				<td style="width:3.8cm" align='right'>58.250</td>
				<td style="width:2cm" align='right'>6.456.900</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;7 + 6 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>24.127.700<br />2.060.960<br />229.356<br />26.418.016</td>
				<td style="width:3.4cm" align='right' valign='top'>925.000<br />0<br />540.000<br />0<br />1.158.720<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>340<br />29.042.076</td>
				<td style="width:3.7cm" align='right' valign='top'>278.833<br />2.113.443<br />0<br />0<br />0<br />2.392.276</td>
				<td style="width:3cm" align='right'>26.649.800</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>26.649.800</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001100 &nbsp;&nbsp;&nbsp;SUB BAGIAN PROGRAM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WAHYU PRASETYO ADI, S.Pi, M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>20-07-1975</td>
						</tr>
						<tr>
							<td colspan='2'>197507202003121005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA SUB BAGIAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.901.500<br />390.150<br />0<br />4.291.650</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>59<br />4.976.549</td>
				<td style="width:3.7cm" align='right' valign='top'>48.317<br />343.332<br />0<br />0<br />0<br />391.649</td>
				<td style="width:3cm" align='right'>4.584.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.584.900</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;1 + 1 + 0 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>3.901.500<br />390.150<br />0<br />4.291.650</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>59<br />4.976.549</td>
				<td style="width:3.7cm" align='right' valign='top'>48.317<br />343.332<br />0<br />0<br />0<br />391.649</td>
				<td style="width:3cm" align='right'>4.584.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.584.900</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DANANG TRI HERMAWAN, SE</td>
						</tr>
						<tr>
							<td colspan='2'>17-04-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198004172003121002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.743.100<br />374.310<br />149.724<br />4.267.134</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>79<br />4.741.893</td>
				<td style="width:3.7cm" align='right' valign='top'>44.522<br />341.371<br />0<br />0<br />0<br />385.893</td>
				<td style="width:3cm" align='right'>4.356.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.356.000</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HERMAWAN SURATMAN, S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>17-01-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198501172003121004</td>
						</tr>
						<tr>
							<td width='70%'>TUGAS BELAJAR</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.518.100<br />0<br />0<br />3.518.100</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>9<br />3.590.529</td>
				<td style="width:3.7cm" align='right' valign='top'>35.181<br />281.448<br />0<br />0<br />0<br />316.629</td>
				<td style="width:3cm" align='right'>3.273.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.273.900</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WAHYU GUNADI SAPUTRO, S.STP , M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>08-10-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198810082010101001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />0<br />3.489.530</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>39<br />3.819.409</td>
				<td style="width:3.7cm" align='right' valign='top'>36.746<br />279.163<br />0<br />0<br />0<br />315.909</td>
				<td style="width:3cm" align='right'>3.503.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.503.500</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>VENI NIRA SARI, S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>20-07-1991</td>
						</tr>
						<tr>
							<td colspan='2'>199107202012062001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />0<br />3.245.660</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>60<br />3.575.560</td>
				<td style="width:3.7cm" align='right' valign='top'>34.307<br />259.653<br />0<br />0<br />0<br />293.960</td>
				<td style="width:3cm" align='right'>3.281.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.281.600</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001100 &nbsp;&nbsp;&nbsp;SUB BAGIAN PROGRAM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'> MUHAMMAD RIZQI ARDIANSYAH , S.STP </td>
						</tr>
						<tr>
							<td colspan='2'>13-08-1994</td>
						</tr>
						<tr>
							<td colspan='2'>199408132016091001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>2.860.500<br />286.050<br />0<br />3.146.550</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>50<br />3.476.440</td>
				<td style="width:3.7cm" align='right' valign='top'>33.316<br />251.724<br />0<br />0<br />0<br />285.040</td>
				<td style="width:3cm" align='right'>3.191.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.191.400</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>PRABATMOJO GEMBONG PRAKOSO</td>
						</tr>
						<tr>
							<td colspan='2'>14-10-1963</td>
						</tr>
						<tr>
							<td colspan='2'>196310141988031008</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.981.600<br />398.160<br />79.632<br />4.459.392</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>44<br />4.861.696</td>
				<td style="width:3.7cm" align='right' valign='top'>46.444<br />356.752<br />0<br />0<br />0<br />403.196</td>
				<td style="width:3cm" align='right'>4.458.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.458.500</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;6 + 5 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;14 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>20.226.200<br />1.670.810<br />229.356<br />22.126.366</td>
				<td style="width:3.4cm" align='right' valign='top'>925.000<br />0<br />0<br />0<br />1.013.880<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>281<br />24.065.527</td>
				<td style="width:3.7cm" align='right' valign='top'>230.516<br />1.770.111<br />0<br />0<br />0<br />2.000.627</td>
				<td style="width:3cm" align='right'>22.064.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>22.064.900</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;7 + 6 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>24.127.700<br />2.060.960<br />229.356<br />26.418.016</td>
				<td style="width:3.4cm" align='right' valign='top'>925.000<br />0<br />540.000<br />0<br />1.158.720<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>340<br />29.042.076</td>
				<td style="width:3.7cm" align='right' valign='top'>278.833<br />2.113.443<br />0<br />0<br />0<br />2.392.276</td>
				<td style="width:3cm" align='right'>26.649.800</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>26.649.800</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;17 + 12 + 17 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;46 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>60.348.900<br />4.062.980<br />1.130.220<br />65.542.100</td>
				<td style="width:3.4cm" align='right' valign='top'>2.960.000<br />0<br />540.000<br />0<br />3.331.320<br />25.645</td>
				<td style="width:3.3cm" align='right' valign='top'>779<br />72.399.844</td>
				<td style="width:3.7cm" align='right' valign='top'>690.426<br />5.243.373<br />0<br />0<br />0<br />5.933.799</td>
				<td style="width:3cm" align='right'>66.466.045</td>
				<td style="width:3.8cm" align='right'>25.645</td>
				<td style="width:2cm" align='right'>66.440.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001200 &nbsp;&nbsp;&nbsp;SUB BAGIAN KEUANGAN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DEASY RINA WIJAYANTI , SE,MSi</td>
						</tr>
						<tr>
							<td colspan='2'>04-12-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198512042009022012</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA SUB BAGIAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.518.100<br />0<br />0<br />3.518.100</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>9<br />4.130.529</td>
				<td style="width:3.7cm" align='right' valign='top'>40.581<br />281.448<br />0<br />0<br />0<br />322.029</td>
				<td style="width:3cm" align='right'>3.808.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.808.500</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>TRI KADARSIH, SH</td>
						</tr>
						<tr>
							<td colspan='2'>18-01-1964</td>
						</tr>
						<tr>
							<td colspan='2'>196401181987102002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.508.600<br />0<br />0<br />4.508.600</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>4<br />4.766.024</td>
				<td style="width:3.7cm" align='right' valign='top'>46.936<br />360.688<br />0<br />0<br />0<br />407.624</td>
				<td style="width:3cm" align='right'>4.358.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.358.400</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>YULIATI MAESAROH, SE</td>
						</tr>
						<tr>
							<td colspan='2'>20-07-1967</td>
						</tr>
						<tr>
							<td colspan='2'>196707201990032004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.370.900<br />0<br />0<br />4.370.900</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>11<br />4.628.331</td>
				<td style="width:3.7cm" align='right' valign='top'>45.559<br />349.672<br />0<br />0<br />0<br />395.231</td>
				<td style="width:3cm" align='right'>4.233.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.233.100</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ARIS SUGIHARTI, SH</td>
						</tr>
						<tr>
							<td colspan='2'>01-03-1969</td>
						</tr>
						<tr>
							<td colspan='2'>196903011990032002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.370.900<br />0<br />0<br />4.370.900</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>11<br />4.628.331</td>
				<td style="width:3.7cm" align='right' valign='top'>45.559<br />349.672<br />0<br />0<br />0<br />395.231</td>
				<td style="width:3cm" align='right'>4.233.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.233.100</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SRI SARTIKA SYATERI, ST</td>
						</tr>
						<tr>
							<td colspan='2'>28-08-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197908282006042019</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.743.100<br />374.310<br />149.724<br />4.267.134</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>79<br />4.741.893</td>
				<td style="width:3.7cm" align='right' valign='top'>44.522<br />341.371<br />0<br />0<br />0<br />385.893</td>
				<td style="width:3cm" align='right'>4.356.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.356.000</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>FITRIATUTI HANDAYANI REJEKI, SE</td>
						</tr>
						<tr>
							<td colspan='2'>20-07-1981</td>
						</tr>
						<tr>
							<td colspan='2'>198107202005012018</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.743.100<br />374.310<br />149.724<br />4.267.134</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>79<br />4.741.893</td>
				<td style="width:3.7cm" align='right' valign='top'>44.522<br />341.371<br />0<br />0<br />0<br />385.893</td>
				<td style="width:3cm" align='right'>4.356.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.356.000</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001200 &nbsp;&nbsp;&nbsp;SUB BAGIAN KEUANGAN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KHURIATUL AMINAH</td>
						</tr>
						<tr>
							<td colspan='2'>17-08-1964</td>
						</tr>
						<tr>
							<td colspan='2'>196408171988102001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>4.461.800<br />446.180<br />0<br />4.907.980</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />13.795</td>
				<td style="width:3.3cm" align='right' valign='top'>49<br />5.251.664</td>
				<td style="width:3.7cm" align='right' valign='top'>50.930<br />392.639<br />0<br />0<br />0<br />443.569</td>
				<td style="width:3cm" align='right'>4.808.095</td>
				<td style="width:2.8cm" align='right'>13.795</td>
				<td style="width:3.8cm" align='right'>4.794.300</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ARIF PRASETYO NUGROHO,SE.AK, M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>14-05-1984</td>
						</tr>
						<tr>
							<td colspan='2'>198405142010011024</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />130.888<br />3.730.308</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>91<br />4.205.079</td>
				<td style="width:3.7cm" align='right' valign='top'>39.154<br />298.425<br />0<br />0<br />0<br />337.579</td>
				<td style="width:3cm" align='right'>3.867.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.867.500</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MARCELLINA WIDIYASTUTI,SE</td>
						</tr>
						<tr>
							<td colspan='2'>09-09-1987</td>
						</tr>
						<tr>
							<td colspan='2'>198709092010012022</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />65.444<br />3.664.864</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>65<br />4.067.189</td>
				<td style="width:3.7cm" align='right' valign='top'>38.499<br />293.190<br />0<br />0<br />0<br />331.689</td>
				<td style="width:3cm" align='right'>3.735.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.735.500</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KISWATI</td>
						</tr>
						<tr>
							<td colspan='2'>06-11-1963</td>
						</tr>
						<tr>
							<td colspan='2'>196311061985032011</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>4.280.800<br />428.080<br />85.616<br />4.794.496</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />11.850</td>
				<td style="width:3.3cm" align='right' valign='top'>99<br />5.208.705</td>
				<td style="width:3.7cm" align='right' valign='top'>49.795<br />383.560<br />0<br />0<br />0<br />433.355</td>
				<td style="width:3cm" align='right'>4.775.350</td>
				<td style="width:2.8cm" align='right'>11.850</td>
				<td style="width:3.8cm" align='right'>4.763.500</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RIA KUMDARTI,S.E</td>
						</tr>
						<tr>
							<td colspan='2'>07-04-1983</td>
						</tr>
						<tr>
							<td colspan='2'>198304072009012004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.139.400<br />313.940<br />125.576<br />3.578.916</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>58<br />4.053.654</td>
				<td style="width:3.7cm" align='right' valign='top'>37.640<br />286.314<br />0<br />0<br />0<br />323.954</td>
				<td style="width:3cm" align='right'>3.729.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.729.700</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>EKA WAHYUNINGTYAS,SE</td>
						</tr>
						<tr>
							<td colspan='2'>07-05-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198805072014022003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />118.024<br />3.363.684</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>18<br />3.838.382</td>
				<td style="width:3.7cm" align='right' valign='top'>35.487<br />269.095<br />0<br />0<br />0<br />304.582</td>
				<td style="width:3cm" align='right'>3.533.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.533.800</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001200 &nbsp;&nbsp;&nbsp;SUB BAGIAN KEUANGAN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>FITRI HANDAYANI SE A Kt</td>
						</tr>
						<tr>
							<td colspan='2'>15-12-1989</td>
						</tr>
						<tr>
							<td colspan='2'>198912152015022001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>JD<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />0<br />0<br />2.950.600</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>84<br />3.208.104</td>
				<td style="width:3.7cm" align='right' valign='top'>31.356<br />236.048<br />0<br />0<br />0<br />267.404</td>
				<td style="width:3cm" align='right'>2.940.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.940.700</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WINDA JULIANA , SE</td>
						</tr>
						<tr>
							<td colspan='2'>10-07-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199007102014022001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />59.012<br />3.304.672</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>39<br />3.706.971</td>
				<td style="width:3.7cm" align='right' valign='top'>34.897<br />264.374<br />0<br />0<br />0<br />299.271</td>
				<td style="width:3cm" align='right'>3.407.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.407.700</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>PUTRI RAHMA AYUNITA,SE</td>
						</tr>
						<tr>
							<td colspan='2'>22-05-1991</td>
						</tr>
						<tr>
							<td colspan='2'>199105222014022003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />118.024<br />3.363.684</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>18<br />3.838.382</td>
				<td style="width:3.7cm" align='right' valign='top'>35.487<br />269.095<br />0<br />0<br />0<br />304.582</td>
				<td style="width:3cm" align='right'>3.533.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.533.800</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>NASITO PRIBOWO, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>20-02-1977</td>
						</tr>
						<tr>
							<td colspan='2'>197702202007011006</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.204.700<br />320.470<br />128.188<br />3.653.358</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>15<br />4.128.053</td>
				<td style="width:3.7cm" align='right' valign='top'>38.384<br />292.269<br />0<br />0<br />0<br />330.653</td>
				<td style="width:3cm" align='right'>3.797.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.797.400</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>CHOLIFATUSH SHAADAH, S.E.</td>
						</tr>
						<tr>
							<td colspan='2'>05-06-1987</td>
						</tr>
						<tr>
							<td colspan='2'>198706052019022007</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />266.070<br />0<br />2.926.770</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>50<br />3.256.660</td>
				<td style="width:3.7cm" align='right' valign='top'>31.118<br />234.142<br />0<br />0<br />0<br />265.260</td>
				<td style="width:3cm" align='right'>2.991.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.991.400</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;17 + 12 + 17 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;46 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>60.348.900<br />4.062.980<br />1.130.220<br />65.542.100</td>
				<td style="width:3.4cm" align='right' valign='top'>2.960.000<br />0<br />540.000<br />0<br />3.331.320<br />25.645</td>
				<td style="width:3.3cm" align='right' valign='top'>779<br />72.399.844</td>
				<td style="width:3.7cm" align='right' valign='top'>690.426<br />5.243.373<br />0<br />0<br />0<br />5.933.799</td>
				<td style="width:3cm" align='right'>66.466.045</td>
				<td style="width:2.8cm" align='right'>25.645</td>
				<td style="width:3.8cm" align='right'>66.440.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001200 &nbsp;&nbsp;&nbsp;SUB BAGIAN KEUANGAN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;17 + 12 + 17 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;46 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>60.348.900<br />4.062.980<br />1.130.220<br />65.542.100</td>
				<td style="width:3.4cm" align='right' valign='top'>2.960.000<br />0<br />540.000<br />0<br />3.331.320<br />25.645</td>
				<td style="width:3.3cm" align='right' valign='top'>779<br />72.399.844</td>
				<td style="width:3.7cm" align='right' valign='top'>690.426<br />5.243.373<br />0<br />0<br />0<br />5.933.799</td>
				<td style="width:3cm" align='right'>66.466.045</td>
				<td style="width:3.8cm" align='right'>25.645</td>
				<td style="width:2cm" align='right'>66.440.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;21 + 19 + 34 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;74 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>70.650.000<br />6.217.100<br />2.265.070<br />79.132.170</td>
				<td style="width:3.4cm" align='right' valign='top'>3.475.000<br />0<br />540.000<br />876.000<br />5.359.080<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>1.173<br />89.383.423</td>
				<td style="width:3.7cm" align='right' valign='top'>840.241<br />6.330.582<br />0<br />0<br />0<br />7.170.823</td>
				<td style="width:3cm" align='right'>82.212.600</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>82.212.600</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001300 &nbsp;&nbsp;&nbsp;SUB BAGIAN UMUM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AMINURDIN, S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>21-10-1977</td>
						</tr>
						<tr>
							<td colspan='2'>197710211997031001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA SUB BAGIAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.982.600<br />398.260<br />159.304<br />4.540.164</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>72<br />5.369.916</td>
				<td style="width:3.7cm" align='right' valign='top'>50.802<br />363.214<br />0<br />0<br />0<br />414.016</td>
				<td style="width:3cm" align='right'>4.955.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.955.900</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WIDYOWATI, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>13-08-1967</td>
						</tr>
						<tr>
							<td colspan='2'>196708131989032005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>JD<br />1001</td>
				<td style="width:2.4cm" align='right' valign='top'>4.370.900<br />0<br />87.418<br />4.458.318</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>42<br />4.788.200</td>
				<td style="width:3.7cm" align='right' valign='top'>46.434<br />356.666<br />0<br />0<br />0<br />403.100</td>
				<td style="width:3cm" align='right'>4.385.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.385.100</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WIDIGDO ADJI SEPUTRO, SH</td>
						</tr>
						<tr>
							<td colspan='2'>26-08-1970</td>
						</tr>
						<tr>
							<td colspan='2'>197008261992031005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.237.500<br />423.750<br />169.500<br />4.830.750</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>88<br />5.305.518</td>
				<td style="width:3.7cm" align='right' valign='top'>50.158<br />386.460<br />0<br />0<br />0<br />436.618</td>
				<td style="width:3cm" align='right'>4.868.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.868.900</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SRI HARTINI, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>28-04-1974</td>
						</tr>
						<tr>
							<td colspan='2'>197404281994012001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1001</td>
				<td style="width:2.4cm" align='right' valign='top'>4.108.100<br />0<br />82.162<br />4.190.262</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>72<br />4.520.174</td>
				<td style="width:3.7cm" align='right' valign='top'>43.753<br />335.221<br />0<br />0<br />0<br />378.974</td>
				<td style="width:3cm" align='right'>4.141.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.141.200</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>NANANG SURYO ADHIE, SE,MM</td>
						</tr>
						<tr>
							<td colspan='2'>01-09-1977</td>
						</tr>
						<tr>
							<td colspan='2'>197709012010011009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>PENGEL. PENGDN BARANG/JASA MUDA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.410.600<br />341.060<br />136.424<br />3.888.084</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />876.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>24<br />5.053.788</td>
				<td style="width:3.7cm" align='right' valign='top'>47.641<br />311.047<br />0<br />0<br />0<br />358.688</td>
				<td style="width:3cm" align='right'>4.695.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.695.100</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BUDIDOJO OETOMO, S.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>17-03-1970</td>
						</tr>
						<tr>
							<td colspan='2'>197003172007011009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.821.000<br />382.100<br />152.840<br />4.355.940</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>66<br />4.830.686</td>
				<td style="width:3.7cm" align='right' valign='top'>45.410<br />348.476<br />0<br />0<br />0<br />393.886</td>
				<td style="width:3cm" align='right'>4.436.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.436.800</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001300 &nbsp;&nbsp;&nbsp;SUB BAGIAN UMUM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ERNY SETYAWATI,SH</td>
						</tr>
						<tr>
							<td colspan='2'>20-10-1975</td>
						</tr>
						<tr>
							<td colspan='2'>197510202009042001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />337.530<br />135.012<br />3.847.842</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>35<br />4.322.557</td>
				<td style="width:3.7cm" align='right' valign='top'>40.329<br />307.828<br />0<br />0<br />0<br />348.157</td>
				<td style="width:3cm" align='right'>3.974.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.974.400</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ELVE OKTAVIA,SE,MM</td>
						</tr>
						<tr>
							<td colspan='2'>01-10-1976</td>
						</tr>
						<tr>
							<td colspan='2'>197610012011012004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />65.444<br />3.664.864</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>65<br />4.067.189</td>
				<td style="width:3.7cm" align='right' valign='top'>38.499<br />293.190<br />0<br />0<br />0<br />331.689</td>
				<td style="width:3cm" align='right'>3.735.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.735.500</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MUHAMMAD HABIL FARID , SE</td>
						</tr>
						<tr>
							<td colspan='2'>21-12-1982</td>
						</tr>
						<tr>
							<td colspan='2'>198212212011011006</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />130.888<br />3.730.308</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>91<br />4.205.079</td>
				<td style="width:3.7cm" align='right' valign='top'>39.154<br />298.425<br />0<br />0<br />0<br />337.579</td>
				<td style="width:3cm" align='right'>3.867.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.867.500</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DANANG BAGUS KUSUMA F , SH</td>
						</tr>
						<tr>
							<td colspan='2'>15-02-1984</td>
						</tr>
						<tr>
							<td colspan='2'>198402152008121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />337.530<br />135.012<br />3.847.842</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>35<br />4.322.557</td>
				<td style="width:3.7cm" align='right' valign='top'>40.329<br />307.828<br />0<br />0<br />0<br />348.157</td>
				<td style="width:3cm" align='right'>3.974.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.974.400</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SRIYADI</td>
						</tr>
						<tr>
							<td colspan='2'>28-05-1967</td>
						</tr>
						<tr>
							<td colspan='2'>196705281989031007</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.023.300<br />402.330<br />160.932<br />4.586.562</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>99<br />5.061.341</td>
				<td style="width:3.7cm" align='right' valign='top'>47.716<br />366.925<br />0<br />0<br />0<br />414.641</td>
				<td style="width:3cm" align='right'>4.646.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.646.700</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DWI SULISTIYONO</td>
						</tr>
						<tr>
							<td colspan='2'>30-05-1971</td>
						</tr>
						<tr>
							<td colspan='2'>197105301994031003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.781.400<br />378.140<br />0<br />4.159.540</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>30<br />4.489.410</td>
				<td style="width:3.7cm" align='right' valign='top'>43.446<br />332.764<br />0<br />0<br />0<br />376.210</td>
				<td style="width:3cm" align='right'>4.113.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.113.200</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001300 &nbsp;&nbsp;&nbsp;SUB BAGIAN UMUM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>VENTHYA YONDA LUKITANINGTYAS, A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>24-11-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197911242010012009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />292.010<br />116.804<br />3.328.914</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>60<br />3.803.654</td>
				<td style="width:3.7cm" align='right' valign='top'>35.140<br />266.314<br />0<br />0<br />0<br />301.454</td>
				<td style="width:3cm" align='right'>3.502.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.502.200</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>JISHNU DONAVIKARA, AMd</td>
						</tr>
						<tr>
							<td colspan='2'>08-01-1989</td>
						</tr>
						<tr>
							<td colspan='2'>198901082010011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />292.010<br />116.804<br />3.328.914</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>60<br />3.803.654</td>
				<td style="width:3.7cm" align='right' valign='top'>35.140<br />266.314<br />0<br />0<br />0<br />301.454</td>
				<td style="width:3cm" align='right'>3.502.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.502.200</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;14 + 12 + 23 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;49 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>50.870.600<br />4.239.160<br />1.648.544<br />56.758.304</td>
				<td style="width:3.4cm" align='right' valign='top'>2.220.000<br />0<br />540.000<br />876.000<br />3.548.580<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>839<br />63.943.723</td>
				<td style="width:3.7cm" align='right' valign='top'>603.951<br />4.540.672<br />0<br />0<br />0<br />5.144.623</td>
				<td style="width:3cm" align='right'>58.799.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>58.799.100</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MARSONO</td>
						</tr>
						<tr>
							<td colspan='2'>05-05-1974</td>
						</tr>
						<tr>
							<td colspan='2'>197405052008011003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.271.400<br />327.140<br />130.856<br />3.729.396</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>70<br />4.199.146</td>
				<td style="width:3.7cm" align='right' valign='top'>39.094<br />298.352<br />0<br />0<br />0<br />337.446</td>
				<td style="width:3cm" align='right'>3.861.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.861.700</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SAKRONI</td>
						</tr>
						<tr>
							<td colspan='2'>06-07-1970</td>
						</tr>
						<tr>
							<td colspan='2'>197007062008011014</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.949.900<br />294.990<br />117.996<br />3.362.886</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>94<br />3.832.660</td>
				<td style="width:3.7cm" align='right' valign='top'>35.429<br />269.031<br />0<br />0<br />0<br />304.460</td>
				<td style="width:3cm" align='right'>3.528.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.528.200</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WIDJANARKO</td>
						</tr>
						<tr>
							<td colspan='2'>24-01-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197901242010011007</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.772.500<br />277.250<br />55.450<br />3.105.200</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>8<br />3.502.468</td>
				<td style="width:3.7cm" align='right' valign='top'>32.852<br />248.416<br />0<br />0<br />0<br />281.268</td>
				<td style="width:3cm" align='right'>3.221.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.221.200</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001300 &nbsp;&nbsp;&nbsp;SUB BAGIAN UMUM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>18</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>IBADUL GHOFUR</td>
						</tr>
						<tr>
							<td colspan='2'>25-08-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197908252010011004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.772.500<br />277.250<br />110.900<br />3.160.650</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>29<br />3.630.359</td>
				<td style="width:3.7cm" align='right' valign='top'>33.407<br />252.852<br />0<br />0<br />0<br />286.259</td>
				<td style="width:3cm" align='right'>3.344.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.344.100</td>
				<td style="width:2cm" align='center'>18</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>19</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ACHMAD ZUHRI</td>
						</tr>
						<tr>
							<td colspan='2'>23-07-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196507232009011005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.000<br />266.000<br />106.400<br />3.032.400</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>36<br />3.502.116</td>
				<td style="width:3.7cm" align='right' valign='top'>32.124<br />242.592<br />0<br />0<br />0<br />274.716</td>
				<td style="width:3cm" align='right'>3.227.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.227.400</td>
				<td style="width:2cm" align='center'>19</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>20</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KUSWARTO</td>
						</tr>
						<tr>
							<td colspan='2'>30-08-1969</td>
						</tr>
						<tr>
							<td colspan='2'>196908301989031004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>2.980.000<br />298.000<br />0<br />3.278.000</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>80<br />3.602.920</td>
				<td style="width:3.7cm" align='right' valign='top'>34.580<br />262.240<br />0<br />0<br />0<br />296.820</td>
				<td style="width:3cm" align='right'>3.306.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.306.100</td>
				<td style="width:2cm" align='center'>20</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. II&nbsp;&nbsp;&nbsp;6 + 6 + 9 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;21 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>17.406.300<br />1.740.630<br />521.602<br />19.668.532</td>
				<td style="width:3.4cm" align='right' valign='top'>1.080.000<br />0<br />0<br />0<br />1.520.820<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>317<br />22.269.669</td>
				<td style="width:3.7cm" align='right' valign='top'>207.486<br />1.573.483<br />0<br />0<br />0<br />1.780.969</td>
				<td style="width:3cm" align='right'>20.488.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>20.488.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>21</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>PARNYO</td>
						</tr>
						<tr>
							<td colspan='2'>22-05-1981</td>
						</tr>
						<tr>
							<td colspan='2'>198105222009011011</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>I D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.373.100<br />237.310<br />94.924<br />2.705.334</td>
				<td style="width:3.4cm" align='right' valign='top'>175.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>17<br />3.170.031</td>
				<td style="width:3.7cm" align='right' valign='top'>28.804<br />216.427<br />0<br />0<br />0<br />245.231</td>
				<td style="width:3cm" align='right'>2.924.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.924.800</td>
				<td style="width:2cm" align='center'>21</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. I&nbsp;&nbsp;&nbsp;1 + 1 + 2 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>2.373.100<br />237.310<br />94.924<br />2.705.334</td>
				<td style="width:3.4cm" align='right' valign='top'>175.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>17<br />3.170.031</td>
				<td style="width:3.7cm" align='right' valign='top'>28.804<br />216.427<br />0<br />0<br />0<br />245.231</td>
				<td style="width:3cm" align='right'>2.924.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.924.800</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 5</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11001300 &nbsp;&nbsp;&nbsp;SUB BAGIAN UMUM<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;21 + 19 + 34 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;74 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>70.650.000<br />6.217.100<br />2.265.070<br />79.132.170</td>
				<td style="width:3.4cm" align='right' valign='top'>3.475.000<br />0<br />540.000<br />876.000<br />5.359.080<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>1.173<br />89.383.423</td>
				<td style="width:3.7cm" align='right' valign='top'>840.241<br />6.330.582<br />0<br />0<br />0<br />7.170.823</td>
				<td style="width:3cm" align='right'>82.212.600</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>82.212.600</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;18 + 11 + 16 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;45 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>59.737.000<br />3.762.600<br />1.118.096<br />64.617.696</td>
				<td style="width:3.4cm" align='right' valign='top'>2.220.000<br />0<br />2.880.000<br />420.000<br />3.258.900<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>1.008<br />73.397.604</td>
				<td style="width:3.7cm" align='right' valign='top'>701.383<br />5.169.421<br />0<br />0<br />0<br />5.870.804</td>
				<td style="width:3cm" align='right'>67.526.800</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>67.526.800</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11002000 &nbsp;&nbsp;&nbsp;BIDANG PERENCANAAN DAN PENGEMBANGAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ARY WIDIYANTORO, SSTP</td>
						</tr>
						<tr>
							<td colspan='2'>05-03-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198003051999121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA BIDANG</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.901.500<br />390.150<br />156.060<br />4.447.710</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>5<br />5.997.395</td>
				<td style="width:3.7cm" align='right' valign='top'>57.078<br />355.817<br />0<br />0<br />0<br />412.895</td>
				<td style="width:3cm" align='right'>5.584.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>5.584.500</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MUHARYO INDRO YULIANTO, S.Kom, M.Ak</td>
						</tr>
						<tr>
							<td colspan='2'>10-07-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197907102005011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.743.100<br />374.310<br />74.862<br />4.192.272</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>73<br />4.949.605</td>
				<td style="width:3.7cm" align='right' valign='top'>47.323<br />335.382<br />0<br />0<br />0<br />382.705</td>
				<td style="width:3cm" align='right'>4.566.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.566.900</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;2 + 2 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>7.644.600<br />764.460<br />230.922<br />8.639.982</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.800.000<br />0<br />506.940<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>78<br />10.947.000</td>
				<td style="width:3.7cm" align='right' valign='top'>104.401<br />691.199<br />0<br />0<br />0<br />795.600</td>
				<td style="width:3cm" align='right'>10.151.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>10.151.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SODIKIN, S.Sos,MSi</td>
						</tr>
						<tr>
							<td colspan='2'>08-10-1977</td>
						</tr>
						<tr>
							<td colspan='2'>197710082010011012</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.410.600<br />0<br />0<br />3.410.600</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>34<br />4.023.054</td>
				<td style="width:3.7cm" align='right' valign='top'>39.506<br />272.848<br />0<br />0<br />0<br />312.354</td>
				<td style="width:3cm" align='right'>3.710.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.710.700</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ALIY MUTTAQIEN, S.STP,M.Si </td>
						</tr>
						<tr>
							<td colspan='2'>14-04-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199004142010101001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />63.446<br />3.552.976</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>33<br />4.310.269</td>
				<td style="width:3.7cm" align='right' valign='top'>40.930<br />284.239<br />0<br />0<br />0<br />325.169</td>
				<td style="width:3cm" align='right'>3.985.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.985.100</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RASTIANA PUSPITARINI</td>
						</tr>
						<tr>
							<td colspan='2'>09-09-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198509092008122002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />0<br />0<br />3.375.300</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>57<br />3.447.777</td>
				<td style="width:3.7cm" align='right' valign='top'>33.753<br />270.024<br />0<br />0<br />0<br />303.777</td>
				<td style="width:3cm" align='right'>3.144.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.144.000</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11002000 &nbsp;&nbsp;&nbsp;BIDANG PERENCANAAN DAN PENGEMBANGAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AHMAD ARIF ZAINUL FATAH</td>
						</tr>
						<tr>
							<td colspan='2'>11-06-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198806112007011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />130.888<br />3.730.308</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>91<br />4.205.079</td>
				<td style="width:3.7cm" align='right' valign='top'>39.154<br />298.425<br />0<br />0<br />0<br />337.579</td>
				<td style="width:3cm" align='right'>3.867.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.867.500</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>UMAR AHMAD SOFYAN LATIF, S.STP,M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>09-11-1989</td>
						</tr>
						<tr>
							<td colspan='2'>198911092010101002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />126.892<br />3.616.422</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>27<br />4.091.129</td>
				<td style="width:3.7cm" align='right' valign='top'>38.015<br />289.314<br />0<br />0<br />0<br />327.329</td>
				<td style="width:3cm" align='right'>3.763.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.763.800</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HILMAN SAHRIZAL , S.STP,M.Si </td>
						</tr>
						<tr>
							<td colspan='2'>23-03-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199003232010101002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />63.446<br />3.552.976</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>83<br />3.955.319</td>
				<td style="width:3.7cm" align='right' valign='top'>37.380<br />284.239<br />0<br />0<br />0<br />321.619</td>
				<td style="width:3cm" align='right'>3.633.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.633.700</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>INDRO ARIS PUJIYANTO, S.STP,M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>30-08-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199008302010101001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />63.446<br />3.552.976</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>83<br />3.955.319</td>
				<td style="width:3.7cm" align='right' valign='top'>37.380<br />284.239<br />0<br />0<br />0<br />321.619</td>
				<td style="width:3cm" align='right'>3.633.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.633.700</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AGUS PURNOMO</td>
						</tr>
						<tr>
							<td colspan='2'>14-06-1964</td>
						</tr>
						<tr>
							<td colspan='2'>196406141985101001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>ANALIS KEPEG. P.LANJUTAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.415.600<br />441.560<br />176.624<br />5.033.784</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />420.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>77<br />5.743.541</td>
				<td style="width:3.7cm" align='right' valign='top'>54.538<br />402.703<br />0<br />0<br />0<br />457.241</td>
				<td style="width:3cm" align='right'>5.286.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>5.286.300</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AGUS WAHYUDI</td>
						</tr>
						<tr>
							<td colspan='2'>20-05-1966</td>
						</tr>
						<tr>
							<td colspan='2'>196605201990031004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.023.300<br />0<br />0<br />4.023.300</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>27<br />4.280.747</td>
				<td style="width:3.7cm" align='right' valign='top'>42.083<br />321.864<br />0<br />0<br />0<br />363.947</td>
				<td style="width:3cm" align='right'>3.916.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.916.800</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11002000 &nbsp;&nbsp;&nbsp;BIDANG PERENCANAAN DAN PENGEMBANGAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>IS ADE ROFIQ,S.KOM</td>
						</tr>
						<tr>
							<td colspan='2'>15-11-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198511152009121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.043.600<br />304.360<br />0<br />3.347.960</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>67<br />3.677.867</td>
				<td style="width:3.7cm" align='right' valign='top'>35.330<br />267.837<br />0<br />0<br />0<br />303.167</td>
				<td style="width:3cm" align='right'>3.374.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.374.700</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DHARU PUNJUNG WIJAYA </td>
						</tr>
						<tr>
							<td colspan='2'>08-05-1986</td>
						</tr>
						<tr>
							<td colspan='2'>198605082009121003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.043.600<br />304.360<br />121.744<br />3.469.704</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>41<br />3.944.425</td>
				<td style="width:3.7cm" align='right' valign='top'>36.548<br />277.577<br />0<br />0<br />0<br />314.125</td>
				<td style="width:3cm" align='right'>3.630.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.630.300</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RENNA AYU KUSUMANINGTYAS , S.STP </td>
						</tr>
						<tr>
							<td colspan='2'>12-05-1993</td>
						</tr>
						<tr>
							<td colspan='2'>199305122015072002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.860.500<br />0<br />0<br />2.860.500</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>75<br />3.117.995</td>
				<td style="width:3.7cm" align='right' valign='top'>30.455<br />228.840<br />0<br />0<br />0<br />259.295</td>
				<td style="width:3cm" align='right'>2.858.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.858.700</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SINDI ARISANDI , S.STP, M.PA</td>
						</tr>
						<tr>
							<td colspan='2'>13-06-1993</td>
						</tr>
						<tr>
							<td colspan='2'>199306132015072001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.860.500<br />0<br />0<br />2.860.500</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>75<br />3.117.995</td>
				<td style="width:3.7cm" align='right' valign='top'>30.455<br />228.840<br />0<br />0<br />0<br />259.295</td>
				<td style="width:3cm" align='right'>2.858.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.858.700</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SILFANUS</td>
						</tr>
						<tr>
							<td colspan='2'>06-07-1967</td>
						</tr>
						<tr>
							<td colspan='2'>196707061990011002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.517.200<br />351.720<br />140.688<br />4.009.608</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>28<br />4.484.316</td>
				<td style="width:3.7cm" align='right' valign='top'>41.947<br />320.769<br />0<br />0<br />0<br />362.716</td>
				<td style="width:3cm" align='right'>4.121.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.121.600</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WIDYANINGRUM MULATSIH S</td>
						</tr>
						<tr>
							<td colspan='2'>02-01-1976</td>
						</tr>
						<tr>
							<td colspan='2'>197601022010012004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />0<br />0<br />2.920.100</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>39<br />3.177.559</td>
				<td style="width:3.7cm" align='right' valign='top'>31.051<br />233.608<br />0<br />0<br />0<br />264.659</td>
				<td style="width:3cm" align='right'>2.912.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.912.900</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11002000 &nbsp;&nbsp;&nbsp;BIDANG PERENCANAAN DAN PENGEMBANGAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>18</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>YUSRIL ASSHIDQI, S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>29-05-1996</td>
						</tr>
						<tr>
							<td colspan='2'>199605292018081001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />0<br />0<br />2.660.700</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>93<br />2.918.213</td>
				<td style="width:3.7cm" align='right' valign='top'>28.457<br />212.856<br />0<br />0<br />0<br />241.313</td>
				<td style="width:3cm" align='right'>2.676.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.676.900</td>
				<td style="width:2cm" align='center'>18</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;16 + 9 + 13 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;38 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>52.092.400<br />2.998.140<br />887.174<br />55.977.714</td>
				<td style="width:3.4cm" align='right' valign='top'>2.220.000<br />0<br />1.080.000<br />420.000<br />2.751.960<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>930<br />62.450.604</td>
				<td style="width:3.7cm" align='right' valign='top'>596.982<br />4.478.222<br />0<br />0<br />0<br />5.075.204</td>
				<td style="width:3cm" align='right'>57.375.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>57.375.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;18 + 11 + 16 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;45 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>59.737.000<br />3.762.600<br />1.118.096<br />64.617.696</td>
				<td style="width:3.4cm" align='right' valign='top'>2.220.000<br />0<br />2.880.000<br />420.000<br />3.258.900<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>1.008<br />73.397.604</td>
				<td style="width:3.7cm" align='right' valign='top'>701.383<br />5.169.421<br />0<br />0<br />0<br />5.870.804</td>
				<td style="width:3cm" align='right'>67.526.800</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>67.526.800</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;24 + 20 + 31 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;75 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>80.166.400<br />6.709.220<br />2.091.452<br />88.967.072</td>
				<td style="width:3.4cm" align='right' valign='top'>3.320.000<br />0<br />2.880.000<br />900.000<br />5.431.500<br />71.636</td>
				<td style="width:3.3cm" align='right' valign='top'>985<br />101.571.193</td>
				<td style="width:3.7cm" align='right' valign='top'>960.682<br />7.117.375<br />0<br />0<br />0<br />8.078.057</td>
				<td style="width:3cm" align='right'>93.493.136</td>
				<td style="width:3.8cm" align='right'>71.636</td>
				<td style="width:2cm" align='right'>93.421.500</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11003000 &nbsp;&nbsp;&nbsp;BIDANG MUTASI PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>Drs. LEGIMAN, M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>19-04-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196504191997101001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV B</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA BAGIAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>4.748.500<br />474.850<br />94.970<br />5.318.320</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />217.260<br />50.291</td>
				<td style="width:3.3cm" align='right' valign='top'>70<br />6.845.941</td>
				<td style="width:3.7cm" align='right' valign='top'>65.784<br />425.466<br />0<br />0<br />0<br />491.250</td>
				<td style="width:3cm" align='right'>6.354.691</td>
				<td style="width:2.8cm" align='right'>50.291</td>
				<td style="width:3.8cm" align='right'>6.304.400</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BUDI SUSATYO, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>22-11-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196511221986031008</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA SUB BAGIAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>4.847.300<br />484.730<br />96.946<br />5.428.976</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />217.260<br />21.345</td>
				<td style="width:3.3cm" align='right' valign='top'>73<br />6.207.654</td>
				<td style="width:3.7cm" align='right' valign='top'>59.690<br />434.319<br />0<br />0<br />0<br />494.009</td>
				<td style="width:3cm" align='right'>5.713.645</td>
				<td style="width:2.8cm" align='right'>21.345</td>
				<td style="width:3.8cm" align='right'>5.692.300</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;2 + 2 + 2 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>9.595.800<br />959.580<br />191.916<br />10.747.296</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.800.000<br />0<br />434.520<br />71.636</td>
				<td style="width:3.3cm" align='right' valign='top'>143<br />13.053.595</td>
				<td style="width:3.7cm" align='right' valign='top'>125.474<br />859.785<br />0<br />0<br />0<br />985.259</td>
				<td style="width:3cm" align='right'>12.068.336</td>
				<td style="width:2.8cm" align='right'>71.636</td>
				<td style="width:3.8cm" align='right'>11.996.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DEYAS YANI RAHMAWAN, S.STP, MM</td>
						</tr>
						<tr>
							<td colspan='2'>25-05-1983</td>
						</tr>
						<tr>
							<td colspan='2'>198305252002121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.628.900<br />362.890<br />145.156<br />4.136.946</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>0<br />4.966.626</td>
				<td style="width:3.7cm" align='right' valign='top'>46.770<br />330.956<br />0<br />0<br />0<br />377.726</td>
				<td style="width:3cm" align='right'>4.588.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.588.900</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>OKY JUNED CAHYONUGROHO, SSTP</td>
						</tr>
						<tr>
							<td colspan='2'>23-11-1983</td>
						</tr>
						<tr>
							<td colspan='2'>198311232003121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.518.100<br />351.810<br />140.724<br />4.010.634</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>44<br />4.840.358</td>
				<td style="width:3.7cm" align='right' valign='top'>45.507<br />320.851<br />0<br />0<br />0<br />366.358</td>
				<td style="width:3cm" align='right'>4.474.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.474.000</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>JOKO PURWANTO, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>07-01-1968</td>
						</tr>
						<tr>
							<td colspan='2'>196801071989031002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.370.900<br />437.090<br />174.836<br />4.982.826</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>0<br />5.457.506</td>
				<td style="width:3.7cm" align='right' valign='top'>51.679<br />398.627<br />0<br />0<br />0<br />450.306</td>
				<td style="width:3cm" align='right'>5.007.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>5.007.200</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11003000 &nbsp;&nbsp;&nbsp;BIDANG MUTASI PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SLAMET NURSALIM , S.SOS., MM</td>
						</tr>
						<tr>
							<td colspan='2'>08-05-1973</td>
						</tr>
						<tr>
							<td colspan='2'>197305081997031001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.518.100<br />351.810<br />140.724<br />4.010.634</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>94<br />4.485.408</td>
				<td style="width:3.7cm" align='right' valign='top'>41.957<br />320.851<br />0<br />0<br />0<br />362.808</td>
				<td style="width:3cm" align='right'>4.122.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.122.600</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'> FERY YUDATAMA , S.SIP, M.Si </td>
						</tr>
						<tr>
							<td colspan='2'>31-05-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198805312007011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.410.600<br />341.060<br />136.424<br />3.888.084</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>14<br />4.362.778</td>
				<td style="width:3.7cm" align='right' valign='top'>40.731<br />311.047<br />0<br />0<br />0<br />351.778</td>
				<td style="width:3cm" align='right'>4.011.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.011.000</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DWI INTEGRASIA, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>27-02-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198002272011012005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />130.888<br />3.730.308</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>91<br />4.205.079</td>
				<td style="width:3.7cm" align='right' valign='top'>39.154<br />298.425<br />0<br />0<br />0<br />337.579</td>
				<td style="width:3cm" align='right'>3.867.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.867.500</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KHARISMA ARDANDA,S.IP</td>
						</tr>
						<tr>
							<td colspan='2'>15-11-1981</td>
						</tr>
						<tr>
							<td colspan='2'>198111152011011004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />327.220<br />65.444<br />3.664.864</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>65<br />4.067.189</td>
				<td style="width:3.7cm" align='right' valign='top'>38.499<br />293.190<br />0<br />0<br />0<br />331.689</td>
				<td style="width:3cm" align='right'>3.735.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.735.500</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BHAKTI WISNU WARDHANA, S.IP,M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>30-12-1987</td>
						</tr>
						<tr>
							<td colspan='2'>198712302007011003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />0<br />0<br />3.272.200</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>28<br />3.529.648</td>
				<td style="width:3.7cm" align='right' valign='top'>34.572<br />261.776<br />0<br />0<br />0<br />296.348</td>
				<td style="width:3cm" align='right'>3.233.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.233.300</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>JEIHAN YOGANA , S.STP,M.Si </td>
						</tr>
						<tr>
							<td colspan='2'>08-09-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199009082010101003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />126.892<br />3.616.422</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>27<br />4.091.129</td>
				<td style="width:3.7cm" align='right' valign='top'>38.015<br />289.314<br />0<br />0<br />0<br />327.329</td>
				<td style="width:3cm" align='right'>3.763.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.763.800</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11003000 &nbsp;&nbsp;&nbsp;BIDANG MUTASI PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SRI HARTANTI</td>
						</tr>
						<tr>
							<td colspan='2'>09-09-1963</td>
						</tr>
						<tr>
							<td colspan='2'>196309091985032009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.280.800<br />0<br />0<br />4.280.800</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>2<br />4.538.222</td>
				<td style="width:3.7cm" align='right' valign='top'>44.658<br />342.464<br />0<br />0<br />0<br />387.122</td>
				<td style="width:3cm" align='right'>4.151.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.151.100</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MUHAMMAD TAUFIQ UROHMAN S.A.P</td>
						</tr>
						<tr>
							<td colspan='2'>10-11-1982</td>
						</tr>
						<tr>
							<td colspan='2'>198211102006041013</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>ANALIS KEPEG. PERTAMA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.139.400<br />313.940<br />125.576<br />3.578.916</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />480.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>8<br />4.348.604</td>
				<td style="width:3.7cm" align='right' valign='top'>40.590<br />286.314<br />0<br />0<br />0<br />326.904</td>
				<td style="width:3cm" align='right'>4.021.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.021.700</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AHMAD FAIZ, S.IP</td>
						</tr>
						<tr>
							<td colspan='2'>11-12-1989</td>
						</tr>
						<tr>
							<td colspan='2'>198912112014021001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.043.600<br />304.360<br />121.744<br />3.469.704</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>41<br />3.944.425</td>
				<td style="width:3.7cm" align='right' valign='top'>36.548<br />277.577<br />0<br />0<br />0<br />314.125</td>
				<td style="width:3cm" align='right'>3.630.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.630.300</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>FERRY AJI PERMANA, S.STP </td>
						</tr>
						<tr>
							<td colspan='2'>17-10-1991</td>
						</tr>
						<tr>
							<td colspan='2'>199110172012061001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />118.024<br />3.363.684</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>18<br />3.838.382</td>
				<td style="width:3.7cm" align='right' valign='top'>35.487<br />269.095<br />0<br />0<br />0<br />304.582</td>
				<td style="width:3cm" align='right'>3.533.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.533.800</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BANGUN YOGA PRATAMA, S,STP </td>
						</tr>
						<tr>
							<td colspan='2'>20-10-1991</td>
						</tr>
						<tr>
							<td colspan='2'>199110202014061001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />0<br />3.245.660</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>60<br />3.575.560</td>
				<td style="width:3.7cm" align='right' valign='top'>34.307<br />259.653<br />0<br />0<br />0<br />293.960</td>
				<td style="width:3cm" align='right'>3.281.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.281.600</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'> PUTRI FRISTADEVI ASTUTI NUGRAHITA , S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>28-05-1993</td>
						</tr>
						<tr>
							<td colspan='2'>199305282016092001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.860.500<br />286.050<br />57.210<br />3.203.760</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>69<br />3.606.089</td>
				<td style="width:3.7cm" align='right' valign='top'>33.888<br />256.301<br />0<br />0<br />0<br />290.189</td>
				<td style="width:3cm" align='right'>3.315.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.315.900</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11003000 &nbsp;&nbsp;&nbsp;BIDANG MUTASI PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>18</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>YAMAUDINA SAHADIZAH,S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>24-05-1994</td>
						</tr>
						<tr>
							<td colspan='2'>199405242016092003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.860.500<br />0<br />0<br />2.860.500</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>75<br />3.117.995</td>
				<td style="width:3.7cm" align='right' valign='top'>30.455<br />228.840<br />0<br />0<br />0<br />259.295</td>
				<td style="width:3cm" align='right'>2.858.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.858.700</td>
				<td style="width:2cm" align='center'>18</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>19</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>G. RULI TRI NUGROHO , A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>01-07-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198807012010011003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>ANALIS KEPEG. P.LANJUTAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />292.010<br />116.804<br />3.328.914</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />420.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>10<br />4.038.604</td>
				<td style="width:3.7cm" align='right' valign='top'>37.490<br />266.314<br />0<br />0<br />0<br />303.804</td>
				<td style="width:3cm" align='right'>3.734.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.734.800</td>
				<td style="width:2cm" align='center'>19</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>20</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>FARIZ ZAKARIA RINALDI S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>31-12-1992</td>
						</tr>
						<tr>
							<td colspan='2'>199212312017081001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />266.070<br />0<br />2.926.770</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>50<br />3.256.660</td>
				<td style="width:3.7cm" align='right' valign='top'>31.118<br />234.142<br />0<br />0<br />0<br />265.260</td>
				<td style="width:3cm" align='right'>2.991.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.991.400</td>
				<td style="width:2cm" align='center'>20</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>21</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MUHAMMAD ARDIANSYAH AZ ZHAAHIR S.IP</td>
						</tr>
						<tr>
							<td colspan='2'>20-06-1995</td>
						</tr>
						<tr>
							<td colspan='2'>199506202017081002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />266.070<br />53.214<br />2.979.984</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>5<br />3.382.249</td>
				<td style="width:3.7cm" align='right' valign='top'>31.650<br />238.399<br />0<br />0<br />0<br />270.049</td>
				<td style="width:3cm" align='right'>3.112.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.112.200</td>
				<td style="width:2cm" align='center'>21</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>22</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HARY PURWANTONO S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>28-08-1995</td>
						</tr>
						<tr>
							<td colspan='2'>199508282017081001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />0<br />0<br />2.660.700</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>93<br />2.918.213</td>
				<td style="width:3.7cm" align='right' valign='top'>28.457<br />212.856<br />0<br />0<br />0<br />241.313</td>
				<td style="width:3cm" align='right'>2.676.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.676.900</td>
				<td style="width:2cm" align='center'>22</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;20 + 16 + 25 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;61 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>64.423.700<br />5.134.950<br />1.653.660<br />71.212.310</td>
				<td style="width:3.4cm" align='right' valign='top'>2.960.000<br />0<br />1.080.000<br />900.000<br />4.417.620<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>794<br />80.570.724</td>
				<td style="width:3.7cm" align='right' valign='top'>761.532<br />5.696.992<br />0<br />0<br />0<br />6.458.524</td>
				<td style="width:3cm" align='right'>74.112.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>74.112.200</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 5</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11003000 &nbsp;&nbsp;&nbsp;BIDANG MUTASI PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>23</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BUDIYANTO</td>
						</tr>
						<tr>
							<td colspan='2'>20-09-1968</td>
						</tr>
						<tr>
							<td colspan='2'>196809202003121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.374.400<br />337.440<br />134.976<br />3.846.816</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>19<br />4.316.515</td>
				<td style="width:3.7cm" align='right' valign='top'>40.269<br />307.746<br />0<br />0<br />0<br />348.015</td>
				<td style="width:3cm" align='right'>3.968.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.968.500</td>
				<td style="width:2cm" align='center'>23</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>24</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SUMADI</td>
						</tr>
						<tr>
							<td colspan='2'>02-08-1976</td>
						</tr>
						<tr>
							<td colspan='2'>197608022010011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.772.500<br />277.250<br />110.900<br />3.160.650</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>29<br />3.630.359</td>
				<td style="width:3.7cm" align='right' valign='top'>33.407<br />252.852<br />0<br />0<br />0<br />286.259</td>
				<td style="width:3cm" align='right'>3.344.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.344.100</td>
				<td style="width:2cm" align='center'>24</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. II&nbsp;&nbsp;&nbsp;2 + 2 + 4 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>6.146.900<br />614.690<br />245.876<br />7.007.466</td>
				<td style="width:3.4cm" align='right' valign='top'>360.000<br />0<br />0<br />0<br />579.360<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>48<br />7.946.874</td>
				<td style="width:3.7cm" align='right' valign='top'>73.676<br />560.598<br />0<br />0<br />0<br />634.274</td>
				<td style="width:3cm" align='right'>7.312.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>7.312.600</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;24 + 20 + 31 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;75 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>80.166.400<br />6.709.220<br />2.091.452<br />88.967.072</td>
				<td style="width:3.4cm" align='right' valign='top'>3.320.000<br />0<br />2.880.000<br />900.000<br />5.431.500<br />71.636</td>
				<td style="width:3.3cm" align='right' valign='top'>985<br />101.571.193</td>
				<td style="width:3.7cm" align='right' valign='top'>960.682<br />7.117.375<br />0<br />0<br />0<br />8.078.057</td>
				<td style="width:3cm" align='right'>93.493.136</td>
				<td style="width:3.8cm" align='right'>71.636</td>
				<td style="width:2cm" align='right'>93.421.500</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;18 + 10 + 13 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;41 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>62.391.200<br />3.576.700<br />949.288<br />66.917.188</td>
				<td style="width:3.4cm" align='right' valign='top'>2.025.000<br />0<br />2.880.000<br />1.500.000<br />2.969.220<br />43.432</td>
				<td style="width:3.3cm" align='right' valign='top'>798<br />76.335.638</td>
				<td style="width:3.7cm" align='right' valign='top'>733.227<br />5.353.379<br />0<br />0<br />0<br />6.086.606</td>
				<td style="width:3cm" align='right'>70.249.032</td>
				<td style="width:3.8cm" align='right'>43.432</td>
				<td style="width:2cm" align='right'>70.205.600</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11004000 &nbsp;&nbsp;&nbsp;BIDANG PEMBINAAN DAN KESEJAHTERAAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KABUL SUTRIYONO, SH, M.H</td>
						</tr>
						<tr>
							<td colspan='2'>13-05-1969</td>
						</tr>
						<tr>
							<td colspan='2'>196905131989031005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA BIDANG</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.555.800<br />455.580<br />182.232<br />5.193.612</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />289.680<br />29.054</td>
				<td style="width:3.3cm" align='right' valign='top'>34<br />6.772.380</td>
				<td style="width:3.7cm" align='right' valign='top'>64.537<br />415.489<br />0<br />0<br />0<br />480.026</td>
				<td style="width:3cm" align='right'>6.292.354</td>
				<td style="width:2.8cm" align='right'>29.054</td>
				<td style="width:3.8cm" align='right'>6.263.300</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ESTU WIDODO, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>10-12-1966</td>
						</tr>
						<tr>
							<td colspan='2'>196612101987091001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>4.699.300<br />469.930<br />93.986<br />5.263.216</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />217.260<br />13.470</td>
				<td style="width:3.3cm" align='right' valign='top'>15<br />6.033.961</td>
				<td style="width:3.7cm" align='right' valign='top'>58.033<br />421.058<br />0<br />0<br />0<br />479.091</td>
				<td style="width:3cm" align='right'>5.554.870</td>
				<td style="width:2.8cm" align='right'>13.470</td>
				<td style="width:3.8cm" align='right'>5.541.400</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;2 + 2 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>9.255.100<br />925.510<br />276.218<br />10.456.828</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.800.000<br />0<br />506.940<br />42.524</td>
				<td style="width:3.3cm" align='right' valign='top'>49<br />12.806.341</td>
				<td style="width:3.7cm" align='right' valign='top'>122.570<br />836.547<br />0<br />0<br />0<br />959.117</td>
				<td style="width:3cm" align='right'>11.847.224</td>
				<td style="width:2.8cm" align='right'>42.524</td>
				<td style="width:3.8cm" align='right'>11.804.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AGUNG RAHARJO WIBOWO KUSUMO, SE,MM</td>
						</tr>
						<tr>
							<td colspan='2'>07-06-1983</td>
						</tr>
						<tr>
							<td colspan='2'>198306072010011023</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.410.600<br />0<br />0<br />3.410.600</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>34<br />4.023.054</td>
				<td style="width:3.7cm" align='right' valign='top'>39.506<br />272.848<br />0<br />0<br />0<br />312.354</td>
				<td style="width:3cm" align='right'>3.710.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.710.700</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>AGIL JOKO SARJONO, SH</td>
						</tr>
						<tr>
							<td colspan='2'>01-09-1982</td>
						</tr>
						<tr>
							<td colspan='2'>198209012009121002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />0<br />0<br />3.272.200</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>78<br />3.884.698</td>
				<td style="width:3.7cm" align='right' valign='top'>38.122<br />261.776<br />0<br />0<br />0<br />299.898</td>
				<td style="width:3cm" align='right'>3.584.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.584.800</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;2 + 0 + 0 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>6.682.800<br />0<br />0<br />6.682.800</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.080.000<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>112<br />7.907.752</td>
				<td style="width:3.7cm" align='right' valign='top'>77.628<br />534.624<br />0<br />0<br />0<br />612.252</td>
				<td style="width:3cm" align='right'>7.295.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>7.295.500</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11004000 &nbsp;&nbsp;&nbsp;BIDANG PEMBINAAN DAN KESEJAHTERAAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>dr. YULI ASTUTI</td>
						</tr>
						<tr>
							<td colspan='2'>10-07-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198007102010012018</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>DOKTER AHLI MADYA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.666.900<br />0<br />0<br />3.666.900</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />1.200.000<br />72.420<br />908</td>
				<td style="width:3.3cm" align='right' valign='top'>1<br />4.940.229</td>
				<td style="width:3.7cm" align='right' valign='top'>48.669<br />293.352<br />0<br />0<br />0<br />342.021</td>
				<td style="width:3cm" align='right'>4.598.208</td>
				<td style="width:2.8cm" align='right'>908</td>
				<td style="width:3.8cm" align='right'>4.597.300</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;1 + 0 + 0 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>3.666.900<br />0<br />0<br />3.666.900</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />1.200.000<br />72.420<br />908</td>
				<td style="width:3.3cm" align='right' valign='top'>1<br />4.940.229</td>
				<td style="width:3.7cm" align='right' valign='top'>48.669<br />293.352<br />0<br />0<br />0<br />342.021</td>
				<td style="width:3cm" align='right'>4.598.208</td>
				<td style="width:2.8cm" align='right'>908</td>
				<td style="width:3.8cm" align='right'>4.597.300</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>TUTI UNSRIATI, SE</td>
						</tr>
						<tr>
							<td colspan='2'>02-06-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196506021990022002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.508.600<br />0<br />0<br />4.508.600</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>54<br />4.581.074</td>
				<td style="width:3.7cm" align='right' valign='top'>45.086<br />360.688<br />0<br />0<br />0<br />405.774</td>
				<td style="width:3cm" align='right'>4.175.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.175.300</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RENDI DONARDANTO,SH</td>
						</tr>
						<tr>
							<td colspan='2'>21-06-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198006212009021002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.518.100<br />351.810<br />70.362<br />3.940.272</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>43<br />4.342.575</td>
				<td style="width:3.7cm" align='right' valign='top'>41.253<br />315.222<br />0<br />0<br />0<br />356.475</td>
				<td style="width:3cm" align='right'>3.986.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.986.100</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BINA YULIATI, SE</td>
						</tr>
						<tr>
							<td colspan='2'>01-07-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197907012010012012</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />0<br />0<br />3.272.200</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>28<br />3.529.648</td>
				<td style="width:3.7cm" align='right' valign='top'>34.572<br />261.776<br />0<br />0<br />0<br />296.348</td>
				<td style="width:3cm" align='right'>3.233.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.233.300</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'> FERI FERNANDES , S.H. </td>
						</tr>
						<tr>
							<td colspan='2'>05-08-1983</td>
						</tr>
						<tr>
							<td colspan='2'>198308052008121001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />337.530<br />135.012<br />3.847.842</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>35<br />4.322.557</td>
				<td style="width:3.7cm" align='right' valign='top'>40.329<br />307.828<br />0<br />0<br />0<br />348.157</td>
				<td style="width:3cm" align='right'>3.974.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.974.400</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11004000 &nbsp;&nbsp;&nbsp;BIDANG PEMBINAAN DAN KESEJAHTERAAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MARTHA YANIARGO S.STP,M.AP</td>
						</tr>
						<tr>
							<td colspan='2'>23-03-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199003232010101003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />317.230<br />63.446<br />3.552.976</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>83<br />3.955.319</td>
				<td style="width:3.7cm" align='right' valign='top'>37.380<br />284.239<br />0<br />0<br />0<br />321.619</td>
				<td style="width:3cm" align='right'>3.633.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.633.700</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ANDINNA MEUTIA RAHMATIN, S.STP.M.Si </td>
						</tr>
						<tr>
							<td colspan='2'>10-04-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199004102010102001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.172.300<br />0<br />0<br />3.172.300</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>37<br />3.429.757</td>
				<td style="width:3.7cm" align='right' valign='top'>33.573<br />253.784<br />0<br />0<br />0<br />287.357</td>
				<td style="width:3cm" align='right'>3.142.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.142.400</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BUDI SETIYANTO</td>
						</tr>
						<tr>
							<td colspan='2'>22-07-1968</td>
						</tr>
						<tr>
							<td colspan='2'>196807221990091001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.900.500<br />390.050<br />156.020<br />4.446.570</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>92<br />4.921.342</td>
				<td style="width:3.7cm" align='right' valign='top'>46.316<br />355.726<br />0<br />0<br />0<br />402.042</td>
				<td style="width:3cm" align='right'>4.519.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.519.300</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>INDRIANA APRILIANDANI, S.Kep.,Ns.</td>
						</tr>
						<tr>
							<td colspan='2'>12-04-1983</td>
						</tr>
						<tr>
							<td colspan='2'>198304122010012024</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>PERAWAT PERTAMA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.238.300<br />323.830<br />64.766<br />3.626.896</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />300.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>65<br />4.144.221</td>
				<td style="width:3.7cm" align='right' valign='top'>39.269<br />290.152<br />0<br />0<br />0<br />329.421</td>
				<td style="width:3cm" align='right'>3.814.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.814.800</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ROCHVIAH,SE</td>
						</tr>
						<tr>
							<td colspan='2'>01-05-1968</td>
						</tr>
						<tr>
							<td colspan='2'>196805012008012012</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.305.700<br />330.570<br />0<br />3.636.270</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>5<br />3.966.115</td>
				<td style="width:3.7cm" align='right' valign='top'>38.213<br />290.902<br />0<br />0<br />0<br />329.115</td>
				<td style="width:3cm" align='right'>3.637.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.637.000</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>LIOBA AJENG DEBYTA, S.A.B.</td>
						</tr>
						<tr>
							<td colspan='2'>28-09-1986</td>
						</tr>
						<tr>
							<td colspan='2'>198609282019022004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />0<br />0<br />2.660.700</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>93<br />2.918.213</td>
				<td style="width:3.7cm" align='right' valign='top'>28.457<br />212.856<br />0<br />0<br />0<br />241.313</td>
				<td style="width:3cm" align='right'>2.676.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.676.900</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11004000 &nbsp;&nbsp;&nbsp;BIDANG PEMBINAAN DAN KESEJAHTERAAN PEGAWAI<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>JOHAN APRI WIBOWO, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>11-04-1993</td>
						</tr>
						<tr>
							<td colspan='2'>199304112019021009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.660.700<br />0<br />0<br />2.660.700</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>93<br />2.918.213</td>
				<td style="width:3.7cm" align='right' valign='top'>28.457<br />212.856<br />0<br />0<br />0<br />241.313</td>
				<td style="width:3cm" align='right'>2.676.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.676.900</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;11 + 6 + 7 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;24 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>36.784.700<br />2.051.020<br />489.606<br />39.325.326</td>
				<td style="width:3.4cm" align='right' valign='top'>1.665.000<br />0<br />0<br />300.000<br />1.738.080<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>628<br />43.029.034</td>
				<td style="width:3.7cm" align='right' valign='top'>412.905<br />3.146.029<br />0<br />0<br />0<br />3.558.934</td>
				<td style="width:3cm" align='right'>39.470.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>39.470.100</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MARYONO</td>
						</tr>
						<tr>
							<td colspan='2'>31-03-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196503312008011002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II D</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.171.500<br />317.150<br />126.860<br />3.615.510</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>7<br />4.085.197</td>
				<td style="width:3.7cm" align='right' valign='top'>37.956<br />289.241<br />0<br />0<br />0<br />327.197</td>
				<td style="width:3cm" align='right'>3.758.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.758.000</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>18</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>TOTOK HARYANTO</td>
						</tr>
						<tr>
							<td colspan='2'>17-08-1963</td>
						</tr>
						<tr>
							<td colspan='2'>196308172007011035</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.830.200<br />283.020<br />56.604<br />3.169.824</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>1<br />3.567.085</td>
				<td style="width:3.7cm" align='right' valign='top'>33.499<br />253.586<br />0<br />0<br />0<br />287.085</td>
				<td style="width:3cm" align='right'>3.280.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.280.000</td>
				<td style="width:2cm" align='center'>18</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. II&nbsp;&nbsp;&nbsp;2 + 2 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>6.001.700<br />600.170<br />183.464<br />6.785.334</td>
				<td style="width:3.4cm" align='right' valign='top'>360.000<br />0<br />0<br />0<br />506.940<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>8<br />7.652.282</td>
				<td style="width:3.7cm" align='right' valign='top'>71.455<br />542.827<br />0<br />0<br />0<br />614.282</td>
				<td style="width:3cm" align='right'>7.038.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>7.038.000</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;18 + 10 + 13 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;41 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>62.391.200<br />3.576.700<br />949.288<br />66.917.188</td>
				<td style="width:3.4cm" align='right' valign='top'>2.025.000<br />0<br />2.880.000<br />1.500.000<br />2.969.220<br />43.432</td>
				<td style="width:3.3cm" align='right' valign='top'>798<br />76.335.638</td>
				<td style="width:3.7cm" align='right' valign='top'>733.227<br />5.353.379<br />0<br />0<br />0<br />6.086.606</td>
				<td style="width:3cm" align='right'>70.249.032</td>
				<td style="width:3.8cm" align='right'>43.432</td>
				<td style="width:2cm" align='right'>70.205.600</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;0 +  +  = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>0<br />0<br />0<br />0</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />0<br />0<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>0<br />0</td>
				<td style="width:3.7cm" align='right' valign='top'>0<br />0<br />0<br />0<br />0<br />0</td>
				<td style="width:3cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>0</td>
				<td style="width:2cm" align='right'>0</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;21 + 13 + 18 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;52 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>68.398.600<br />4.339.080<br />1.210.138<br />73.947.818</td>
				<td style="width:3.4cm" align='right' valign='top'>740.000<br />0<br />2.340.000<br />7.530.000<br />3.765.840<br />53.845</td>
				<td style="width:3.3cm" align='right' valign='top'>1.157<br />88.378.660</td>
				<td style="width:3.7cm" align='right' valign='top'>845.585<br />5.915.830<br />0<br />0<br />0<br />6.761.415</td>
				<td style="width:3cm" align='right'>81.617.245</td>
				<td style="width:3.8cm" align='right'>53.845</td>
				<td style="width:2cm" align='right'>81.563.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11005000 &nbsp;&nbsp;&nbsp;BIDANG INKA<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RR. UTAMI RAHAJENG, SH, MM</td>
						</tr>
						<tr>
							<td colspan='2'>13-01-1970</td>
						</tr>
						<tr>
							<td colspan='2'>197001131989032002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV B</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA BIDANG</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.748.500<br />0<br />0<br />4.748.500</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.260.000<br />0<br />72.420<br />53.845</td>
				<td style="width:3.3cm" align='right' valign='top'>45<br />6.134.810</td>
				<td style="width:3.7cm" align='right' valign='top'>60.085<br />379.880<br />0<br />0<br />0<br />439.965</td>
				<td style="width:3cm" align='right'>5.694.845</td>
				<td style="width:2.8cm" align='right'>53.845</td>
				<td style="width:3.8cm" align='right'>5.641.000</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KRISTIAWAN NURDIANTO, S.Kom, M.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>29-12-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198012292005011004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.901.500<br />390.150<br />0<br />4.291.650</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>59<br />4.976.549</td>
				<td style="width:3.7cm" align='right' valign='top'>48.317<br />343.332<br />0<br />0<br />0<br />391.649</td>
				<td style="width:3cm" align='right'>4.584.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.584.900</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;2 + 1 + 0 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>8.650.000<br />390.150<br />0<br />9.040.150</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />1.800.000<br />0<br />217.260<br />53.845</td>
				<td style="width:3.3cm" align='right' valign='top'>104<br />11.111.359</td>
				<td style="width:3.7cm" align='right' valign='top'>108.402<br />723.212<br />0<br />0<br />0<br />831.614</td>
				<td style="width:3cm" align='right'>10.279.745</td>
				<td style="width:2.8cm" align='right'>53.845</td>
				<td style="width:3.8cm" align='right'>10.225.900</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WARSONO, S Sos M Si</td>
						</tr>
						<tr>
							<td colspan='2'>17-06-1973</td>
						</tr>
						<tr>
							<td colspan='2'>197306172005011008</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KA SUB BID</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.628.900<br />362.890<br />145.156<br />4.136.946</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>0<br />4.966.626</td>
				<td style="width:3.7cm" align='right' valign='top'>46.770<br />330.956<br />0<br />0<br />0<br />377.726</td>
				<td style="width:3cm" align='right'>4.588.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.588.900</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SUSENO,ST,MTi</td>
						</tr>
						<tr>
							<td colspan='2'>11-12-1976</td>
						</tr>
						<tr>
							<td colspan='2'>197612112005021003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMPUTER MUDA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.743.100<br />374.310<br />149.724<br />4.267.134</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />960.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>29<br />5.516.843</td>
				<td style="width:3.7cm" align='right' valign='top'>52.272<br />341.371<br />0<br />0<br />0<br />393.643</td>
				<td style="width:3cm" align='right'>5.123.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>5.123.200</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>R.M. MICKO TRINARDITYA, ST</td>
						</tr>
						<tr>
							<td colspan='2'>30-05-1978</td>
						</tr>
						<tr>
							<td colspan='2'>197805302006041009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMPUTER MUDA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.628.900<br />362.890<br />145.156<br />4.136.946</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />960.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>0<br />5.386.626</td>
				<td style="width:3.7cm" align='right' valign='top'>50.970<br />330.956<br />0<br />0<br />0<br />381.926</td>
				<td style="width:3cm" align='right'>5.004.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>5.004.700</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11005000 &nbsp;&nbsp;&nbsp;BIDANG INKA<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ROKHARDI,S.Kom, M.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>06-02-1978</td>
						</tr>
						<tr>
							<td colspan='2'>197802062009011006</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMPUTER MUDA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />337.530<br />135.012<br />3.847.842</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />960.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>85<br />5.097.607</td>
				<td style="width:3.7cm" align='right' valign='top'>48.079<br />307.828<br />0<br />0<br />0<br />355.907</td>
				<td style="width:3cm" align='right'>4.741.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.741.700</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>EDY UMARGONO, S.Kom, M.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>07-02-1981</td>
						</tr>
						<tr>
							<td colspan='2'>198102072010011014</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.272.200<br />0<br />0<br />3.272.200</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>28<br />3.529.648</td>
				<td style="width:3.7cm" align='right' valign='top'>34.572<br />261.776<br />0<br />0<br />0<br />296.348</td>
				<td style="width:3cm" align='right'>3.233.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.233.300</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HARYANI</td>
						</tr>
						<tr>
							<td colspan='2'>13-04-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196504131988012001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.900.500<br />0<br />0<br />3.900.500</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>75<br />4.157.995</td>
				<td style="width:3.7cm" align='right' valign='top'>40.855<br />312.040<br />0<br />0<br />0<br />352.895</td>
				<td style="width:3cm" align='right'>3.805.100</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.805.100</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>NUR IMAN</td>
						</tr>
						<tr>
							<td colspan='2'>06-09-1966</td>
						</tr>
						<tr>
							<td colspan='2'>196609061989031006</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.900.500<br />390.050<br />156.020<br />4.446.570</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>92<br />4.921.342</td>
				<td style="width:3.7cm" align='right' valign='top'>46.316<br />355.726<br />0<br />0<br />0<br />402.042</td>
				<td style="width:3cm" align='right'>4.519.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.519.300</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>CHOSIM NUR HIDAYAT, S.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>28-04-1977</td>
						</tr>
						<tr>
							<td colspan='2'>197704282011011004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>3.043.600<br />304.360<br />0<br />3.347.960</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />144.840<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>67<br />3.677.867</td>
				<td style="width:3.7cm" align='right' valign='top'>35.330<br />267.837<br />0<br />0<br />0<br />303.167</td>
				<td style="width:3cm" align='right'>3.374.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.374.700</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>IBONG SWANDY SINAGA,S.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>10-07-1997</td>
						</tr>
						<tr>
							<td colspan='2'>197707102011011002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>ANALIS KEPEG. PERTAMA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.043.600<br />0<br />0<br />3.043.600</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />480.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>4<br />3.596.024</td>
				<td style="width:3.7cm" align='right' valign='top'>35.236<br />243.488<br />0<br />0<br />0<br />278.724</td>
				<td style="width:3cm" align='right'>3.317.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.317.300</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11005000 &nbsp;&nbsp;&nbsp;BIDANG INKA<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SANGAJI RIFQIANTO, S.Kom</td>
						</tr>
						<tr>
							<td colspan='2'>02-09-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198809022011011004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMPUTER PERTAMA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.139.400<br />313.940<br />62.788<br />3.516.128</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />540.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>65<br />4.273.453</td>
				<td style="width:3.7cm" align='right' valign='top'>40.562<br />281.291<br />0<br />0<br />0<br />321.853</td>
				<td style="width:3cm" align='right'>3.951.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.951.600</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DAFIZ ADI NUGROHO , ST </td>
						</tr>
						<tr>
							<td colspan='2'>13-12-1990</td>
						</tr>
						<tr>
							<td colspan='2'>199012132015021003</td>
						</tr>
						<tr>
							<td width='70%'>TUGAS BELAJAR</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMPUTER PERTAMA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />59.012<br />3.304.672</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />540.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>89<br />4.062.021</td>
				<td style="width:3.7cm" align='right' valign='top'>38.447<br />264.374<br />0<br />0<br />0<br />302.821</td>
				<td style="width:3cm" align='right'>3.759.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.759.200</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>USY ADIASTUTI, A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>19-02-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197902192011012004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>ARSIPARIS PELAKS.LANJUTAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />0<br />0<br />2.920.100</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />420.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>89<br />3.412.609</td>
				<td style="width:3.7cm" align='right' valign='top'>33.401<br />233.608<br />0<br />0<br />0<br />267.009</td>
				<td style="width:3cm" align='right'>3.145.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.145.600</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HASTY MAYRINA, A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>24-05-1980</td>
						</tr>
						<tr>
							<td colspan='2'>198005242011012003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMP PELAKS.LANJT.</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.012.000<br />301.200<br />120.480<br />3.433.680</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />450.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>72<br />4.173.432</td>
				<td style="width:3.7cm" align='right' valign='top'>38.837<br />274.695<br />0<br />0<br />0<br />313.532</td>
				<td style="width:3cm" align='right'>3.859.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.859.900</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>FARID USHRIH,A,Md</td>
						</tr>
						<tr>
							<td colspan='2'>11-09-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198809112010011002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMP PELAKS.LANJT.</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />292.010<br />58.402<br />3.270.512</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />450.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>75<br />3.937.847</td>
				<td style="width:3.7cm" align='right' valign='top'>37.206<br />261.641<br />0<br />0<br />0<br />298.847</td>
				<td style="width:3cm" align='right'>3.639.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.639.000</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;14 + 10 + 15 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;39 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>46.478.800<br />3.334.240<br />1.031.750<br />50.844.790</td>
				<td style="width:3.4cm" align='right' valign='top'>740.000<br />0<br />540.000<br />5.760.000<br />2.824.380<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>770<br />60.709.940</td>
				<td style="width:3.7cm" align='right' valign='top'>578.853<br />4.067.587<br />0<br />0<br />0<br />4.646.440</td>
				<td style="width:3cm" align='right'>56.063.500</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>56.063.500</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11005000 &nbsp;&nbsp;&nbsp;BIDANG INKA<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MARSUDI</td>
						</tr>
						<tr>
							<td colspan='2'>31-03-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196503312007011004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II D</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMP PELAKSANA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.374.400<br />337.440<br />67.488<br />3.779.328</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />360.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>53<br />4.356.641</td>
				<td style="width:3.7cm" align='right' valign='top'>41.394<br />302.347<br />0<br />0<br />0<br />343.741</td>
				<td style="width:3cm" align='right'>4.012.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.012.900</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>18</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'> MARSUDI   WAHONO</td>
						</tr>
						<tr>
							<td colspan='2'>27-01-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198501272010011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>PRANATA KOMP PELAKSANA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>2.772.500<br />277.250<br />110.900<br />3.160.650</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />360.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>29<br />3.810.359</td>
				<td style="width:3.7cm" align='right' valign='top'>35.207<br />252.852<br />0<br />0<br />0<br />288.059</td>
				<td style="width:3cm" align='right'>3.522.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.522.300</td>
				<td style="width:2cm" align='center'>18</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>19</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>WENY WAHYUNINGRUM, A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>15-08-1994</td>
						</tr>
						<tr>
							<td colspan='2'>199408152019022003</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>ARSIPARIS PELAKSANA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.374.300<br />0<br />0<br />2.374.300</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />350.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>67<br />2.796.787</td>
				<td style="width:3.7cm" align='right' valign='top'>27.243<br />189.944<br />0<br />0<br />0<br />217.187</td>
				<td style="width:3cm" align='right'>2.579.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.579.600</td>
				<td style="width:2cm" align='center'>19</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>20</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RAHARDIAN MURTI PRATIWI, A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>03-05-1996</td>
						</tr>
						<tr>
							<td colspan='2'>199605032019022009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>ARSIPARIS PELAKSANA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.374.300<br />0<br />0<br />2.374.300</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />350.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>67<br />2.796.787</td>
				<td style="width:3.7cm" align='right' valign='top'>27.243<br />189.944<br />0<br />0<br />0<br />217.187</td>
				<td style="width:3cm" align='right'>2.579.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.579.600</td>
				<td style="width:2cm" align='center'>20</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>21</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>CINDE MERISA, A.Md</td>
						</tr>
						<tr>
							<td colspan='2'>04-05-1996</td>
						</tr>
						<tr>
							<td colspan='2'>199605042019022008</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>ARSIPARIS PELAKSANA</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.374.300<br />0<br />0<br />2.374.300</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />350.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>67<br />2.796.787</td>
				<td style="width:3.7cm" align='right' valign='top'>27.243<br />189.944<br />0<br />0<br />0<br />217.187</td>
				<td style="width:3cm" align='right'>2.579.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.579.600</td>
				<td style="width:2cm" align='center'>21</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. II&nbsp;&nbsp;&nbsp;5 + 2 + 3 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>13.269.800<br />614.690<br />178.388<br />14.062.878</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />1.770.000<br />724.200<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>283<br />16.557.361</td>
				<td style="width:3.7cm" align='right' valign='top'>158.330<br />1.125.031<br />0<br />0<br />0<br />1.283.361</td>
				<td style="width:3cm" align='right'>15.274.000</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>15.274.000</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 5</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11005000 &nbsp;&nbsp;&nbsp;BIDANG INKA<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;21 + 13 + 18 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;52 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>68.398.600<br />4.339.080<br />1.210.138<br />73.947.818</td>
				<td style="width:3.4cm" align='right' valign='top'>740.000<br />0<br />2.340.000<br />7.530.000<br />3.765.840<br />53.845</td>
				<td style="width:3.3cm" align='right' valign='top'>1.157<br />88.378.660</td>
				<td style="width:3.7cm" align='right' valign='top'>845.585<br />5.915.830<br />0<br />0<br />0<br />6.761.415</td>
				<td style="width:3cm" align='right'>81.617.245</td>
				<td style="width:3.8cm" align='right'>53.845</td>
				<td style="width:2cm" align='right'>81.563.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;19 + 12 + 20 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;51 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>69.156.300<br />4.307.410<br />1.447.120<br />74.910.830</td>
				<td style="width:3.4cm" align='right' valign='top'>1.105.000<br />0<br />2.060.000<br />7.000.000<br />3.693.420<br />110.831</td>
				<td style="width:3.3cm" align='right' valign='top'>1.086<br />88.881.167</td>
				<td style="width:3.7cm" align='right' valign='top'>850.765<br />5.992.871<br />0<br />0<br />0<br />6.843.636</td>
				<td style="width:3cm" align='right'>82.037.531</td>
				<td style="width:3.8cm" align='right'>110.831</td>
				<td style="width:2cm" align='right'>81.926.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 1</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11006000 &nbsp;&nbsp;&nbsp;UPT PENILAIAN KOMPETENSI PEGAWAI ASN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>1</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>Drs. SUKARDI, MSi</td>
						</tr>
						<tr>
							<td colspan='2'>19-09-1969</td>
						</tr>
						<tr>
							<td colspan='2'>196909191990011001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>IV A</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA UNIT</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.555.800<br />455.580<br />182.232<br />5.193.612</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />980.000<br />0<br />289.680<br />15.754</td>
				<td style="width:3.3cm" align='right' valign='top'>34<br />6.479.080</td>
				<td style="width:3.7cm" align='right' valign='top'>61.737<br />415.489<br />0<br />0<br />0<br />477.226</td>
				<td style="width:3cm" align='right'>6.001.854</td>
				<td style="width:2.8cm" align='right'>15.754</td>
				<td style="width:3.8cm" align='right'>5.986.100</td>
				<td style="width:2cm" align='center'>1</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. IV&nbsp;&nbsp;&nbsp;1 + 1 + 2 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>4.555.800<br />455.580<br />182.232<br />5.193.612</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />980.000<br />0<br />289.680<br />15.754</td>
				<td style="width:3.3cm" align='right' valign='top'>34<br />6.479.080</td>
				<td style="width:3.7cm" align='right' valign='top'>61.737<br />415.489<br />0<br />0<br />0<br />477.226</td>
				<td style="width:3cm" align='right'>6.001.854</td>
				<td style="width:2.8cm" align='right'>15.754</td>
				<td style="width:3.8cm" align='right'>5.986.100</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>2</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ZM SRI SUMARYANTI, S.Sos</td>
						</tr>
						<tr>
							<td colspan='2'>26-08-1966</td>
						</tr>
						<tr>
							<td colspan='2'>196608261987022002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA SEKSI</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.508.600<br />0<br />0<br />4.508.600</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />72.420<br />8.250</td>
				<td style="width:3.3cm" align='right' valign='top'>54<br />5.129.324</td>
				<td style="width:3.7cm" align='right' valign='top'>50.486<br />360.688<br />0<br />0<br />0<br />411.174</td>
				<td style="width:3cm" align='right'>4.718.150</td>
				<td style="width:2.8cm" align='right'>8.250</td>
				<td style="width:3.8cm" align='right'>4.709.900</td>
				<td style="width:2cm" align='center'>2</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>3</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>DATA HARSANTO, S.STP, M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>04-09-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198509042003121002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>KEPALA SEKSI</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />337.530<br />135.012<br />3.847.842</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />540.000<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>85<br />4.677.607</td>
				<td style="width:3.7cm" align='right' valign='top'>43.879<br />307.828<br />0<br />0<br />0<br />351.707</td>
				<td style="width:3cm" align='right'>4.325.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.325.900</td>
				<td style="width:2cm" align='center'>3</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>4</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>Dra. NUVIE TRI ASTARINI</td>
						</tr>
						<tr>
							<td colspan='2'>09-11-1965</td>
						</tr>
						<tr>
							<td colspan='2'>196511091993032002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1100</td>
				<td style="width:2.4cm" align='right' valign='top'>4.508.600<br />450.860<br />0<br />4.959.460</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />144.840<br />40.704</td>
				<td style="width:3.3cm" align='right' valign='top'>52<br />5.845.056</td>
				<td style="width:3.7cm" align='right' valign='top'>56.595<br />396.757<br />0<br />0<br />0<br />453.352</td>
				<td style="width:3cm" align='right'>5.391.704</td>
				<td style="width:2.8cm" align='right'>40.704</td>
				<td style="width:3.8cm" align='right'>5.351.000</td>
				<td style="width:2cm" align='center'>4</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>5</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RA.OETIE HAPSANI RIANA DEWI, S.Psi</td>
						</tr>
						<tr>
							<td colspan='2'>01-12-1969</td>
						</tr>
						<tr>
							<td colspan='2'>196912011998032002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.108.100<br />0<br />0<br />4.108.100</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>9<br />4.880.529</td>
				<td style="width:3.7cm" align='right' valign='top'>48.081<br />328.648<br />0<br />0<br />0<br />376.729</td>
				<td style="width:3cm" align='right'>4.503.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.503.800</td>
				<td style="width:2cm" align='center'>5</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 2</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11006000 &nbsp;&nbsp;&nbsp;UPT PENILAIAN KOMPETENSI PEGAWAI ASN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>6</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>KUNTA WIDJAJANTI, S.Psi</td>
						</tr>
						<tr>
							<td colspan='2'>10-10-1971</td>
						</tr>
						<tr>
							<td colspan='2'>197110101997032007</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>4.237.500<br />0<br />0<br />4.237.500</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />72.420<br />2.970</td>
				<td style="width:3.3cm" align='right' valign='top'>55<br />5.012.945</td>
				<td style="width:3.7cm" align='right' valign='top'>49.375<br />339.000<br />0<br />0<br />0<br />388.375</td>
				<td style="width:3cm" align='right'>4.624.570</td>
				<td style="width:2.8cm" align='right'>2.970</td>
				<td style="width:3.8cm" align='right'>4.621.600</td>
				<td style="width:2cm" align='center'>6</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>7</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>GALUH WIDYASARI, S.Psi,Psi.</td>
						</tr>
						<tr>
							<td colspan='2'>05-08-1974</td>
						</tr>
						<tr>
							<td colspan='2'>197408051999032009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.108.100<br />410.810<br />164.324<br />4.683.234</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />289.680<br />34.466</td>
				<td style="width:3.3cm" align='right' valign='top'>78<br />5.707.458</td>
				<td style="width:3.7cm" align='right' valign='top'>53.833<br />374.659<br />0<br />0<br />0<br />428.492</td>
				<td style="width:3cm" align='right'>5.278.966</td>
				<td style="width:2.8cm" align='right'>34.466</td>
				<td style="width:3.8cm" align='right'>5.244.500</td>
				<td style="width:2cm" align='center'>7</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>8</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>GALUH NOVIANY, S.Psi,Psi,M.Si</td>
						</tr>
						<tr>
							<td colspan='2'>22-11-1974</td>
						</tr>
						<tr>
							<td colspan='2'>197411222005012009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>JD<br />1002</td>
				<td style="width:2.4cm" align='right' valign='top'>3.743.100<br />0<br />149.724<br />3.892.824</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>71<br />4.810.155</td>
				<td style="width:3.7cm" align='right' valign='top'>45.929<br />311.426<br />0<br />0<br />0<br />357.355</td>
				<td style="width:3cm" align='right'>4.452.800</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.452.800</td>
				<td style="width:2cm" align='center'>8</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>9</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HERLINA MURDIASTUTI, S.Psi, Psi.</td>
						</tr>
						<tr>
							<td colspan='2'>06-05-1978</td>
						</tr>
						<tr>
							<td colspan='2'>197805062006042009</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.628.900<br />362.890<br />145.156<br />4.136.946</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />289.680<br />8.687</td>
				<td style="width:3.3cm" align='right' valign='top'>0<br />5.135.313</td>
				<td style="width:3.7cm" align='right' valign='top'>48.370<br />330.956<br />0<br />0<br />0<br />379.326</td>
				<td style="width:3cm" align='right'>4.755.987</td>
				<td style="width:2.8cm" align='right'>8.687</td>
				<td style="width:3.8cm" align='right'>4.747.300</td>
				<td style="width:2cm" align='center'>9</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>10</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>SRI UMRONAH, S.Psi,Psi</td>
						</tr>
						<tr>
							<td colspan='2'>28-05-1979</td>
						</tr>
						<tr>
							<td colspan='2'>197905282010012013</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III D</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.410.600<br />341.060<br />136.424<br />3.888.084</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>64<br />4.877.828</td>
				<td style="width:3.7cm" align='right' valign='top'>45.881<br />311.047<br />0<br />0<br />0<br />356.928</td>
				<td style="width:3cm" align='right'>4.520.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.520.900</td>
				<td style="width:2cm" align='center'>10</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>11</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RATIH MAYASARI, SE</td>
						</tr>
						<tr>
							<td colspan='2'>04-05-1975</td>
						</tr>
						<tr>
							<td colspan='2'>197505042002122007</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>TK<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>3.704.300<br />0<br />0<br />3.704.300</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>67<br />4.476.787</td>
				<td style="width:3.7cm" align='right' valign='top'>44.043<br />296.344<br />0<br />0<br />0<br />340.387</td>
				<td style="width:3cm" align='right'>4.136.400</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.136.400</td>
				<td style="width:2cm" align='center'>11</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 3</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11006000 &nbsp;&nbsp;&nbsp;UPT PENILAIAN KOMPETENSI PEGAWAI ASN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>12</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ADIB DZIKRON ISTIGHFARY SE MM</td>
						</tr>
						<tr>
							<td colspan='2'>11-12-1985</td>
						</tr>
						<tr>
							<td colspan='2'>198512112015021001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>3.075.500<br />307.550<br />123.020<br />3.506.070</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>97<br />4.495.847</td>
				<td style="width:3.7cm" align='right' valign='top'>42.061<br />280.486<br />0<br />0<br />0<br />322.547</td>
				<td style="width:3cm" align='right'>4.173.300</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.173.300</td>
				<td style="width:2cm" align='center'>12</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>13</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>RENY VIDYA WAHYULY,S.Psi.M.Psi </td>
						</tr>
						<tr>
							<td colspan='2'>03-07-1986</td>
						</tr>
						<tr>
							<td colspan='2'>198607032009012005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.375.300<br />337.530<br />67.506<br />3.780.336</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>85<br />4.182.681</td>
				<td style="width:3.7cm" align='right' valign='top'>39.654<br />302.427<br />0<br />0<br />0<br />342.081</td>
				<td style="width:3cm" align='right'>3.840.600</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.840.600</td>
				<td style="width:2cm" align='center'>13</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>14</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ENJANG GUPITO SE MM</td>
						</tr>
						<tr>
							<td colspan='2'>23-03-1989</td>
						</tr>
						<tr>
							<td colspan='2'>198903232015021002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III C</td>
						</tr>
						<tr>
							<td colspan='2'>Assessor SDM Aparatur Ahli Muda</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>3.075.500<br />307.550<br />61.510<br />3.444.560</td>
				<td style="width:3.4cm" align='right' valign='top'>0<br />0<br />0<br />700.000<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>91<br />4.361.911</td>
				<td style="width:3.7cm" align='right' valign='top'>41.446<br />275.565<br />0<br />0<br />0<br />317.011</td>
				<td style="width:3cm" align='right'>4.044.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.044.900</td>
				<td style="width:2cm" align='center'>14</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>15</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>BISMO</td>
						</tr>
						<tr>
							<td colspan='2'>28-07-1966</td>
						</tr>
						<tr>
							<td colspan='2'>196607281988031004</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1102</td>
				<td style="width:2.4cm" align='right' valign='top'>4.150.100<br />415.010<br />166.004<br />4.731.114</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />289.680<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>58<br />5.205.852</td>
				<td style="width:3.7cm" align='right' valign='top'>49.162<br />378.490<br />0<br />0<br />0<br />427.652</td>
				<td style="width:3cm" align='right'>4.778.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>4.778.200</td>
				<td style="width:2cm" align='center'>15</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>16</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>GILANG BUDI ARIAWAN , SE </td>
						</tr>
						<tr>
							<td colspan='2'>25-08-1988</td>
						</tr>
						<tr>
							<td colspan='2'>198808252015031002</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.950.600<br />295.060<br />59.012<br />3.304.672</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>39<br />3.706.971</td>
				<td style="width:3.7cm" align='right' valign='top'>34.897<br />264.374<br />0<br />0<br />0<br />299.271</td>
				<td style="width:3cm" align='right'>3.407.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.407.700</td>
				<td style="width:2cm" align='center'>16</td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>17</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>HETI DIHASTUTI, S.STP</td>
						</tr>
						<tr>
							<td colspan='2'>05-07-1994</td>
						</tr>
						<tr>
							<td colspan='2'>199407052016092001</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III B</td>
						</tr>
						<tr>
							<td colspan='2'>STAF </td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.860.500<br />0<br />0<br />2.860.500</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>75<br />3.117.995</td>
				<td style="width:3.7cm" align='right' valign='top'>30.455<br />228.840<br />0<br />0<br />0<br />259.295</td>
				<td style="width:3cm" align='right'>2.858.700</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.858.700</td>
				<td style="width:2cm" align='center'>17</td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"><tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. 4</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : 11006000 &nbsp;&nbsp;&nbsp;UPT PENILAIAN KOMPETENSI PEGAWAI ASN<br />
								BAGIAN BULAN : MEI 2021</td>
							<td width='13%' align='right' valign='bottom'></td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style="width:1cm" valign='middle' align='center' rowspan='2'>No</td>
				<td style="width:7cm" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style="width:3.7cm" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style="width:3cm" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style="width:2.8cm" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style="width:3.8cm" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style="width:2cm" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>18</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>MUHARSIH , S.Sos, M.A.P</td>
						</tr>
						<tr>
							<td colspan='2'>11-03-1975</td>
						</tr>
						<tr>
							<td colspan='2'>197503112010012006</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>III A</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1000</td>
				<td style="width:2.4cm" align='right' valign='top'>2.920.100<br />0<br />0<br />2.920.100</td>
				<td style="width:3.4cm" align='right' valign='top'>185.000<br />0<br />0<br />0<br />72.420<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>39<br />3.177.559</td>
				<td style="width:3.7cm" align='right' valign='top'>31.051<br />233.608<br />0<br />0<br />0<br />264.659</td>
				<td style="width:3cm" align='right'>2.912.900</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>2.912.900</td>
				<td style="width:2cm" align='center'>18</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. III&nbsp;&nbsp;&nbsp;17 + 10 + 17 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;44 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>61.740.700<br />3.565.850<br />1.207.692<br />66.514.242</td>
				<td style="width:3.4cm" align='right' valign='top'>925.000<br />0<br />1.080.000<br />7.000.000<br />3.186.480<br />95.077</td>
				<td style="width:3.3cm" align='right' valign='top'>1.019<br />78.801.818</td>
				<td style="width:3.7cm" align='right' valign='top'>755.198<br />5.321.143<br />0<br />0<br />0<br />6.076.341</td>
				<td style="width:3cm" align='right'>72.725.477</td>
				<td style="width:2.8cm" align='right'>95.077</td>
				<td style="width:3.8cm" align='right'>72.630.400</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td style="width:1cm"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>19</td>
					  </tr>
					</table></td>
				<td style="width:7cm" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>ARIF WIBOWO</td>
						</tr>
						<tr>
							<td colspan='2'>09-06-1981</td>
						</tr>
						<tr>
							<td colspan='2'>198106092010011005</td>
						</tr>
						<tr>
							<td width='70%'>PNS</td>
							<td width='30%' align='right'>II C</td>
						</tr>
						<tr>
							<td colspan='2'>STAF</td>
						</tr>
					</table></td>
				<td style="width:1.7cm" align='center'>K<br />1101</td>
				<td style="width:2.4cm" align='right' valign='top'>2.859.800<br />285.980<br />57.196<br />3.202.976</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>33<br />3.600.269</td>
				<td style="width:3.7cm" align='right' valign='top'>33.830<br />256.239<br />0<br />0<br />0<br />290.069</td>
				<td style="width:3cm" align='right'>3.310.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.310.200</td>
				<td style="width:2cm" align='center'>19</td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. II&nbsp;&nbsp;&nbsp;1 + 1 + 1 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>2.859.800<br />285.980<br />57.196<br />3.202.976</td>
				<td style="width:3.4cm" align='right' valign='top'>180.000<br />0<br />0<br />0<br />217.260<br />0</td>
				<td style="width:3.3cm" align='right' valign='top'>33<br />3.600.269</td>
				<td style="width:3.7cm" align='right' valign='top'>33.830<br />256.239<br />0<br />0<br />0<br />290.069</td>
				<td style="width:3cm" align='right'>3.310.200</td>
				<td style="width:2.8cm" align='right'>0</td>
				<td style="width:3.8cm" align='right'>3.310.200</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. &nbsp;&nbsp;&nbsp;19 + 12 + 20 = <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;51 JIWA</td>
						</tr>
					</table></td>
				<td style="width:2.4cm" align='right' valign='top'>69.156.300<br />4.307.410<br />1.447.120<br />74.910.830</td>
				<td style="width:3.4cm" align='right' valign='top'>1.105.000<br />0<br />2.060.000<br />7.000.000<br />3.693.420<br />110.831</td>
				<td style="width:3.3cm" align='right' valign='top'>1.086<br />88.881.167</td>
				<td style="width:3.7cm" align='right' valign='top'>850.765<br />5.992.871<br />0<br />0<br />0<br />6.843.636</td>
				<td style="width:3cm" align='right'>82.037.531</td>
				<td style="width:3.8cm" align='right'>110.831</td>
				<td style="width:2cm" align='right'>81.926.700</td>
				<td style="width:2cm" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr><tr><td>&nbsp;</td></tr></table>
			  <div style="page-break-after:always"></div>
			  <table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0"></table>
</div>
</body>
</html>