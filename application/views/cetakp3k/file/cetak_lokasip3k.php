<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="cetak">
<?
// Check Data Exist
$tb_kalkulasi = 'simgaji_kalkulasip3ks';
$tb_lokasi = 'simgaji_lokasis';
$tb_golongan = 'simgaji_golonganp3ks';

// $n_kalkulasis = ORM::factory('kalkulasip3k')
// 	->where('periode','=',$periode)
// 	->where('lokasi_id','IN',$arrLokasi)
// 	->count_all();
$this->db->where('periode', $periode);
$this->db->where_in('lokasi_id', $arrLokasi);
$this->db->from($tb_kalkulasi);
$n_kalkulasis = $this->db->count_all_results();

if($n_kalkulasis == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk Lokasi dan atau Periode ini tidak tersedia<br>Silakan pilih Lokasi atau Periode lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

$page = 1;
foreach($arrLokasi as $lokasi) {	
	// $lokasi = ORM::factory('lokasi',$lokasi);
	// $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
	// $strLokasi = $lokasi->name;
    $this->db->where('id', $lokasi);
	$this->db->order_by('id');
	$lokasi = $this->db->get($tb_lokasi)->row();
    $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
    $strLokasi = $lokasi->name;
	
	$xPeriode = explode("-",$periode);    	
	// $bulan = ORM::factory('bulan',intval($xPeriode[1]));
    $bulan = get_bulan(intval($xPeriode[1]));
	
	// $kalkulasis = ORM::factory('kalkulasip3k')
	// 	->where('periode','=',$periode)
	// 	->where('lokasi_id','=',$lokasi->id);
	
	// $nPeg = $kalkulasis->reset(FALSE)->count_all();

    $this->db->where('periode', $periode);
    $this->db->where('lokasi_id', $lokasi->id);
	$this->db->order_by('lokasi_kode', 'DESC');
    $kalkulasis = $this->db->get($tb_kalkulasi)->row();

    $this->db->where('periode', $periode);
    $this->db->where('lokasi_id', $lokasi->id);
    $this->db->from($tb_kalkulasi);
    $nPeg = $this->db->count_all_results();

	if($nPeg) {		
		?>	
		<table width="100%" style="width:33.5cm" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="24%" align="left" valign="top">Hal. <? echo $page; ?></td>
				<td width="57%" rowspan="2" align="center" valign="top">PEMERINTAH PROVINSI JAWA TENGAH<br />REKAPITULASI GAJI DSB UNTUK PPPK<br />
																		KODE LOKASI : <? echo $kode; ?>&nbsp;&nbsp;&nbsp;<? echo $strLokasi; ?><br />
																		BAGIAN BULAN : <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?></td>
				<td width="19%" rowspan="2" align="center" valign="top">&nbsp;</td>
			</tr>
			<tr>
			  <td align="left" valign="bottom">REKAP PER LOKASI</td>
		  </tr>
			<tr>
				<td colspan="3" align="center" valign="top">&nbsp;</td>
			</tr>
		</table>
		<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-top:dashed 1px #000000; width:33.5cm">
		  <tr>
				<td width="6%">STATUS<br />GOL.</td>
				<td width="3%" align="center" class="borBoth">PEG.<br />IST.</td>
				<td width="5%" align="center" >ANAK<br />JIWA </td>
				<td width="8%" align="right" class="borBoth">GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width="10%" align="right">TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.</td>
				<td width="11%" align="right" class="borBoth"><p>TJ.BERAS<br />TJ.PPH<br />PEMBULATAN</p></td>
				<td width="9%" align="right"><p>JML KOTOR</p></td>
				<td width="8%" align="right" class="borBoth">BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP</td>
				<td width="8%" align="right">JML POT</td>
				<td width="11%" align="right" class="borBoth">JML BERSIH</td>
				<td width="9%" align="center">POT.PPH</td>
				<td width="12%" align="center" class="borLeft">JML. BERSIH<br />YG DIBAYARKAN</td>
		  </tr>
			<tr align="center" >
				<td width="6%" class="borTopBottom">1</td>
				<td width="3%" class="borTopBottom">2/3</td>
				<td width="5%" class="borTopBottom">4/5</td>
				<td width="8%" class="borTopBottom">6-9</td>
				<td width="10%" class="borTopBottom">10-13</td>
				<td width="11%" class="borTopBottom">14-16</td>
				<td width="9%" class="borTopBottom">17</td>
				<td width="8%" class="borTopBottom">18-22</td>
				<td width="8%" class="borTopBottom">23</td>
				<td width="11%" class="borTopBottom">24</td>
				<td width="9%" class="borTopBottom">25</td>
				<td width="12%" class="borTopBottom">26</td>
			</tr>
			<tr align="left">
				<td colspan="12">&nbsp;</td>
			</tr>	
		<?
		// $golongans = ORM::factory('golonganp3k')->group_by('id')->find_all();
        $this->db->group_by('id');
        $golongans = $this->db->get($tb_golongan)->result();
        foreach($golongans as $golongan) {
			$sql = 
			"SELECT 
			count(id) AS pegawai,
			sum(istri) AS istri,
			sum(anak) AS anak,
			sum(jiwa) AS jiwa,
			sum(gaji_pokok) AS gaji_pokok,
			sum(tunjangan_istri) AS tunjangan_istri,
			sum(tunjangan_anak) AS tunjangan_anak,
			sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
			sum(jumlah_penghasilan) AS jumlah_penghasilan,
			sum(tunjangan_umum) AS tunjangan_umum,
			sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
			sum(tunjangan_struktural) AS tunjangan_struktural,
			sum(tunjangan_fungsional) AS tunjangan_fungsional,
			sum(tunjangan_beras) AS tunjangan_beras,
			sum(tunjangan_pph) AS tunjangan_pph,
			sum(pembulatan) AS pembulatan,
			sum(jumlah_kotor) AS jumlah_kotor,
			sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
			sum(potongan_pensiun) AS potongan_pensiun,
			sum(potongan_lain) AS potongan_lain,
			sum(potongan_beras) AS potongan_beras,
			sum(potongan_cp) AS potongan_cp,
			sum(jumlah_potongan) AS jumlah_potongan,
			sum(jumlah_bersih) AS jumlah_bersih,
			sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar
			FROM $tb_kalkulasi 
			WHERE lokasi_kode='".$lokasi->kode."' AND periode = '".$periode."' AND golongan_id LIKE '".$golongan->id."'";	
			
			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
            $sqls = $this->db->query($sql)->result();			
			foreach($sqls as $row) {
				$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
				$pensiun = round(0.08 * $row->jumlah_penghasilan);
				$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
				$n_jumlah_kotor = $row->jumlah_bersih + $n_potongan;
				$s_jumlah_kotor = $n_jumlah_kotor - $row->jumlah_kotor;
				$n_pembulatan = $row->pembulatan + $s_jumlah_kotor;	
				?>
                <tr>
                    <td valign="top" width="6%"><? echo "GOL. ".$golongan->prefix; ?></td>
                    <td align="center" valign="top" width="3%"><? echo round($row->pegawai); ?><br /><? echo round($row->istri); ?></td>
                    <td align="center" valign="top" width="5%"><? echo round($row->anak); ?><br /><? echo round($row->jiwa); ?></td>
                    <td align="right" valign="top" width="8%"><? echo numFormat($row->gaji_pokok); ?><br /><? echo  numFormat($row->tunjangan_istri); ?><br /><? echo numFormat($row->tunjangan_anak); ?><br /><? echo numFormat($row->jumlah_penghasilan); ?></td>
                    <td align="right" valign="top" width="10%"><? echo numFormat($row->tunjangan_umum); ?><br /><? echo numFormat($row->tunjangan_umum_tambahan); ?><br /><? echo numFormat($row->tunjangan_struktural); ?><br /><? echo numFormat($row->tunjangan_fungsional); ?></td>
                    <td align="right" valign="top" width="11%"><? echo numFormat($row->tunjangan_beras); ?><br /><? echo numFormat(0); ?><br /><? echo numFormat($row->pembulatan); ?></td>
                    <td align="right" valign="top" width="9%"><? echo numFormat($row->jumlah_kotor); ?></td>
                    <td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_bpjs_kesehatan); ?><br /><? echo numFormat($row->potongan_pensiun); ?><br /><? echo numFormat($row->potongan_lain); ?><br /><? echo numFormat($row->potongan_beras); ?><br /><? echo numFormat($row->potongan_cp); ?></td>
                    <td align="right" valign="top" width="8%"><? echo numFormat($row->jumlah_potongan); ?></td>
                    <td align="right" valign="top" width="11%"><? echo numFormat($row->jumlah_bersih); ?></td>
                    <td align="right" valign="top" width="9%"><? echo numFormat($row->tunjangan_pph); ?></td>
                    <td align="right" valign="top" width="12%"><? echo numFormat($row->jumlah_bersih_bayar); ?></td>
                </tr>
                <tr>
                    <td colspan="12"><hr /></td>
                </tr> 
			<?			
			}
		}
		
		$sql = 
		"SELECT 
		count(id) AS pegawai,
		sum(istri) AS istri,
		sum(anak) AS anak,
		sum(jiwa) AS jiwa,
		sum(gaji_pokok) AS gaji_pokok,
		sum(tunjangan_istri) AS tunjangan_istri,
		sum(tunjangan_anak) AS tunjangan_anak,
		sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
		sum(jumlah_penghasilan) AS jumlah_penghasilan,
		sum(tunjangan_umum) AS tunjangan_umum,
		sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
		sum(tunjangan_struktural) AS tunjangan_struktural,
		sum(tunjangan_fungsional) AS tunjangan_fungsional,
		sum(tunjangan_beras) AS tunjangan_beras,
		sum(tunjangan_pph) AS tunjangan_pph,
		sum(pembulatan) AS pembulatan,
		sum(jumlah_kotor) AS jumlah_kotor,
		sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
		sum(potongan_pensiun) AS potongan_pensiun,
		sum(potongan_lain) AS potongan_lain,
		sum(potongan_beras) AS potongan_beras,
		sum(potongan_cp) AS potongan_cp,
		sum(jumlah_potongan) AS jumlah_potongan,
		sum(jumlah_bersih) AS jumlah_bersih,
		sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar
		FROM $tb_kalkulasi 
		WHERE lokasi_kode='".$lokasi->kode."' AND periode = '".$periode."'";	
		
		// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
        $sqls = $this->db->query($sql)->result();
		
		foreach($sqls as $rows) {
			$ttPeg = $rows->pegawai;
			$ttIstri = $rows->istri;
			$ttAnak = $rows->anak;
			$ttJiwa = $rows->jiwa;
			$ttGajiPokok = $rows->gaji_pokok;
			$ttTjKeluarga = $rows->jumlah_tunjangan_keluarga;
			$ttTjUmum = $rows->tunjangan_umum;
			$ttTjUmumPlus = $rows->tunjangan_umum_tambahan;
			$ttTjStruktural = $rows->tunjangan_struktural;
			$ttTjFungsional = $rows->tunjangan_fungsional;
			$ttTjBeras = $rows->tunjangan_beras;
			$ttTjPPH = 0;
			$ttPotLain = $rows->potongan_lain;
			$ttPotCp = $rows->potongan_cp;
			$ttJmlBersih = $rows->jumlah_bersih;
			$ttPotPPH = $rows->tunjangan_pph;
			$ttJmlBersihBayar = $rows->jumlah_bersih_bayar;
			
			#$iwp = round(0.1 * $rows->jumlah_penghasilan);
			$bpjs_kes = $rows->potongan_bpjs_kesehatan;
			$pensiun = $rows->potongan_pensiun;
			$n_potongan = $bpjs_kes + $pensiun + $rows->potongan_lain;
			#$n_jumlah_kotor = $rows->jumlah_bersih + $n_potongan;
			$n_jumlah_kotor = $rows->jumlah_kotor;
			$s_jumlah_kotor = $n_jumlah_kotor - $rows->jumlah_kotor;
			#$n_pembulatan = $rows->pembulatan + $s_jumlah_kotor; per gaji september 2018 diganti
			$n_pembulatan = $rows->pembulatan;
			?>
            <tr>
                <td valign="top" width="6%">JUMLAH</td>
                <td align="center" valign="top" width="3%"><? echo $ttPeg; ?><br /><? echo $ttIstri; ?></td>
                <td align="center" valign="top" width="5%"><? echo $ttAnak; ?><br /><? echo $ttJiwa; ?></td>
                <td align="right" valign="top" width="8%"><? echo numFormat($ttGajiPokok); ?><br /><? echo numFormat($rows->tunjangan_istri); ?><br /><? echo numFormat($rows->tunjangan_anak); ?><br /><? echo numFormat($rows->jumlah_penghasilan); ?></td>
                <td align="right" valign="top" width="10%"><? echo numFormat($ttTjUmum); ?><br /><? echo numFormat($ttTjUmumPlus); ?><br /><? echo numFormat($ttTjStruktural); ?><br /><? echo numFormat($ttTjFungsional); ?></td>
                <td align="right" valign="top" width="11%"><? echo numFormat($ttTjBeras); ?><br /><? echo numFormat($ttTjPPH); ?><br /><? echo numFormat($n_pembulatan); ?></td>
                <td align="right" valign="top" width="9%"><? echo numFormat($n_jumlah_kotor); ?></td>
                <td align="right" valign="top" width="8%"><? echo numFormat($bpjs_kes); ?><br /><? echo numFormat($pensiun); ?><br /><? echo numFormat($ttPotLain); ?><br /><? echo numFormat($rows->potongan_beras); ?><br /><? echo numFormat($ttPotCp); ?></td>
                <td align="right" valign="top" width="8%"><? echo numFormat($n_potongan); ?></td>
                <td align="right" valign="top" width="11%"><? echo numFormat($ttJmlBersih); ?></td>
                <td align="right" valign="top" width="9%"><? echo numFormat($ttPotPPH); ?></td>
                <td align="right" valign="top" width="12%"><? echo numFormat($ttJmlBersihBayar); ?></td>
            </tr>
            <tr>
                <td colspan="12"><hr /></td>
            </tr>		
            <?
        }
        ?>
        </table>
		<?	
		echo "<div style=\"page-break-after:always\"></div>";			
		$page++;
	}
}
?>
</div>
</body>
</html>