<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="cetak">
<?

$tb_kalkulasi = 'simgaji_kalkulasip3ks';
$tb_lokasi = 'simgaji_lokasis';
$tb_golongan = 'simgaji_golonganp3ks';

// Check Data Exist
// $n_kalkulasis = ORM::factory('kalkulasip3k')
// 	->where('periode','=',$periode)
// 	->where('lokasi_id','=',$arrLokasiInduk)
// 	->count_all();

$this->db->where('periode', $periode);
$this->db->where_in('lokasi_id', $arrLokasi);
$this->db->from($tb_kalkulasi);
$n_kalkulasis = $this->db->count_all_results();

if($n_kalkulasis == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk Lokasi dan atau Periode ini tidak tersedia<br>Silakan pilih Lokasi atau Periode lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

$page_break = "<div style=\"page-break-after:always\"></div>";	
// echo json_encode($arrLokasiInduk);
// 	die();

$page=1;
foreach($arrLokasiInduk AS $lokasi) {
	// $lokasi = ORM::factory('lokasi',$lokasi);
	// $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
	// $strLokasi = $lokasi->name;
    $this->db->where('id', $lokasi);
	$lokasi = $this->db->get($tb_lokasi)->row();
	
    $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
    $strLokasi = $lokasi->name;
	
	$xPeriode = explode("-",$periode);    	
	// $bulan = ORM::factory('bulan',intval($xPeriode[1]));
    $bulan = get_bulan(intval($xPeriode[1]));
	
	if($page > 1) {
		echo $page_break;
	}

	?>
	<table style="width:33.5cm" border="0" cellspacing="0" cellpadding="3">
		<tr>
    		<td colspan="2" align="left" valign="top">HALAMAN : 1</td>
			<td width="71%" rowspan="2" align="center" valign="top">PEMERINTAH PROVINSI JAWA TENGAH<br />
			REKAPITULASI IURAN JAMINAN KECELAKAAN KERJA (JKK)(0.24%) UNTUK PARA PEGAWAI / PEKERJA<br />
  																	KODE LOKASI : <? echo $kode; ?>&nbsp;&nbsp;&nbsp;<? echo $strLokasi; ?></td>
			<td width="1%" rowspan="2" align="center" valign="top">&nbsp;</td>
		</tr>
		<tr align="left">
		  <td width="12%" valign="top">BAGIAN BULAN<br />KODE REKENING </td>
		  <td width="16%" valign="top">: <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?><br>
		    :</td>
      </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" style="border-top:dashed 1px #000000;width:33.5cm">
	  <tr>
			<td width="7%" rowspan="2" >GOL. /<br />RUANG</td>
			<td colspan="4" align="center" class="borBothBottom">PEGAWAI &amp; KELUARGA</td>
			<td width="6%" rowspan="2" align="center"><p>GAJI POKOK</p></td>
			<td colspan="2" align="center" class="borBothBottom">TUNJANGAN KELUARGA</td>
			<td width="10%" rowspan="2" align="center">JML <br />PENGHASILAN</td>
			<td width="8%" rowspan="2" align="center">BPJS JKK<br>
			  (0.24%)</td>
	  </tr>
		<tr>
			<td width="3%" align="center" class="borBoth">PEG</td>
			<td width="5%" align="center">IST/<br />SUAMI</td>
			<td width="5%" align="center" class="borBoth">ANAK</td>
			<td width="5%" align="center" class="borRight">JML JIWA</td>
			<td width="10%" align="center" class="borBoth">TJ.ISTRI<br />TJ. ANAK</td>
			<td width="7%" align="center" class="borRight">JUMLAH</td>
	  </tr>
		<tr align="center">
			<td class="borTopBottom">1</td>
			<td width="3%" class="borTopBottom">2</td>
			<td width="5%" class="borTopBottom">3</td>
			<td width="5%" class="borTopBottom">4</td>
			<td class="borTopBottom">5</td>
			<td width="6%" class="borTopBottom">6</td>
			<td width="10%" class="borTopBottom">7/8</td>
			<td width="7%" class="borTopBottom">9</td>
			<td class="borTopBottom">10</td>
			<td class="borTopBottom">11</td>
		</tr>
		<tr align="center">
			<td colspan="10">&nbsp;</td>
		</tr>
	<?

	// $golongans = ORM::factory('golonganp3k')->group_by('id')->find_all();
    $this->db->group_by('id');
    $golongans = $this->db->get($tb_golongan)->result();
		foreach($golongans as $golongan) {	
			$sql = 
			"SELECT 
			count(id) AS pegawai,
			sum(istri) AS istri,
			sum(anak) AS anak,
			sum(jiwa) AS jiwa,
			sum(gaji_pokok) AS gaji_pokok,
			sum(tunjangan_istri) AS tunjangan_istri,
			sum(tunjangan_anak) AS tunjangan_anak,
			sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
			sum(jumlah_penghasilan) AS jumlah_penghasilan,
			sum(tunjangan_umum) AS tunjangan_umum,
			sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
			sum(tunjangan_struktural) AS tunjangan_struktural,
			sum(tunjangan_fungsional) AS tunjangan_fungsional,
			sum(tunjangan_beras) AS tunjangan_beras,
			sum(tunjangan_pph) AS tunjangan_pph,
			sum(pembulatan) AS pembulatan,
			sum(jumlah_kotor) AS jumlah_kotor,
			sum(potongan_iwp) AS potongan_iwp,
			sum(potongan_lain) AS potongan_lain,
			sum(potongan_beras) AS potongan_beras,
			sum(potongan_cp) AS potongan_cp,
			sum(jumlah_potongan) AS jumlah_potongan,
			sum(jumlah_bersih) AS jumlah_bersih,
			sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
			sum(askes) AS askes
			FROM $tb_kalkulasi 
			WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND golongan_id = '".$golongan->id."' AND status_id!=8";	
			
			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
            $sqls = $this->db->query($sql)->result();		
			foreach($sqls as $row) {
				?>
				<tr>
					<td valign="top"><? echo "GOL. ".$golongan->kode; ?></td>
					<td align="center" width="3%" valign="top"><? echo round($row->pegawai); ?></td>
					<td align="center" width="5%" valign="top"><? echo round($row->istri); ?></td>
					<td align="center" width="5%" valign="top"><? echo round($row->anak); ?></td>
					<td align="center" valign="top"><? echo round($row->jiwa); ?></td>
					<td align="right" width="6%" valign="top"><? echo numFormat($row->gaji_pokok); ?></td>
					<td align="right" width="10%" valign="top">0<br />
					  0</td>
					<td align="right" width="7%" valign="top">0</td>
					<td align="right" width="6%" valign="top"><? echo numFormat($row->gaji_pokok); ?></td>
					<td align="right" valign="top"><? echo numFormat($row->gaji_pokok * 0.0024); ?></td>
				</tr>
			<?
			}
		}
		
	
	$sql = 
	"SELECT 
	count(id) AS pegawai,
	sum(istri) AS istri,
	sum(anak) AS anak,
	sum(jiwa) AS jiwa,
	sum(gaji_pokok) AS gaji_pokok,
	sum(tunjangan_istri) AS tunjangan_istri,
	sum(tunjangan_anak) AS tunjangan_anak,
	sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
	sum(jumlah_penghasilan) AS jumlah_penghasilan,
	sum(tunjangan_umum) AS tunjangan_umum,
	sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
	sum(tunjangan_struktural) AS tunjangan_struktural,
	sum(tunjangan_fungsional) AS tunjangan_fungsional,
	sum(tunjangan_beras) AS tunjangan_beras,
	sum(tunjangan_pph) AS tunjangan_pph,
	sum(pembulatan) AS pembulatan,
	sum(jumlah_kotor) AS jumlah_kotor,
	sum(potongan_iwp) AS potongan_iwp,
	sum(potongan_lain) AS potongan_lain,
	sum(potongan_beras) AS potongan_beras,
	sum(potongan_cp) AS potongan_cp,
	sum(jumlah_potongan) AS jumlah_potongan,
	sum(jumlah_bersih) AS jumlah_bersih,
	sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
	sum(askes) AS askes
	FROM $tb_kalkulasi 
	WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND status_id!=8";	
	
	// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
    $sqls = $this->db->query($sql)->result();	
	foreach($sqls as $row) {	
		?>
		<tr>
            <td valign="top">TOTAL</td>
            <td align="center" valign="top" width="3%"><? echo numFormat($row->pegawai); ?></td>
            <td align="center" valign="top" width="5%"><? echo numFormat($row->istri); ?></td>
            <td align="center" valign="top" width="5%"><? echo numFormat($row->anak); ?></td>
            <td align="center" valign="top"><? echo numFormat($row->jiwa); ?></td>
            <td align="right" valign="top" width="6%"><? echo numFormat($row->gaji_pokok); ?></td>
            <td align="right" valign="top" width="10%">0<br />
            0</td>
            <td align="right" valign="top" width="7%">0</td>
            <td align="right" valign="top"><? echo numFormat($row->gaji_pokok); ?></td>
            <td align="right" valign="top"><? echo numFormat($row->gaji_pokok * 0.0024); ?></td>
        </tr>
		<?
	}
	?>
    	<tr align="center">
            <td colspan="10" height="5"><hr /></td>
        </tr>
	</table>
<?	
}
?>
</div>
</body>
</html>