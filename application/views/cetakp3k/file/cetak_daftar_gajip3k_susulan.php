<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php

// echo json_encode($arrLokasi);
// die();
//constract
$tb_kalkulasi = 'simgaji_kalkulasip3k_susulan';
$tb_lokasi = 'simgaji_lokasis';
?>

<?
$pageBreak = "<tr><td>&nbsp;</td></tr></table>
			  <div style=\"page-break-after:always\"></div>
			  <table style=\"width:33.5cm\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";		 


function showPageHeader($strhal,$strkodelokasi,$strlokasigaji,$strperiode,$strkgaji) {
	$xPeriode = explode("-",$strperiode);
	// $bulan = ORM::factory('bulan',$xPeriode[1]);
    $bulan = get_bulan($xPeriode[1]);
	
	return "<tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. ".$strhal."</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI UNTUK PPPK<br />
								KODE LOKASI : ".$strkodelokasi." &nbsp;&nbsp;&nbsp;".$strlokasigaji."<br />
								BAGIAN BULAN : ".strtoupper($bulan->name)." ".$xPeriode[0]."</td>
							<td width='13%' align='right' valign='bottom'>".$strkgaji."</td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style=\"width:1cm\" valign='middle' align='center' rowspan='2'>No</td>
				<td style=\"width:7cm\" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style=\"width:1.7cm\" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style=\"width:3.7cm\" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style=\"width:3cm\" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style=\"width:2.8cm\" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style=\"width:3.8cm\" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style=\"width:2cm\" valign='middle' align='center' rowspan='2'>TANDA<br />TANGAN</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>BPJS KES<br />PENSIUN<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr>";	
}

function showPageData($urut,$nama,$lahir,$nip,$status,$golongan,$jabatan,$marital,$jiwa,$gajipokok,$tjistri,$tjanak,$j4,$tjumum,$tjumum_tambahan,$tjstruktural,$tjfungsional,$tjberas,$tjpph,$pembulatan,$jmlkotor,$potbpjskes,$potpensiun,$potlain,$potberas,$potcp,$jmlpot,$jmlbersih,$jmlbersihbayar) {
	$potonganpph = 0;
	return "<tr valign='top'>
				<td style=\"width:1cm\"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>".$urut."</td>
					  </tr>
					</table></td>
				<td style=\"width:7cm\" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>".$nama."</td>
						</tr>
						<tr>
							<td colspan='2'>".$lahir."</td>
						</tr>
						<tr>
							<td colspan='2'>".$nip."</td>
						</tr>
						<tr>
							<td width='70%'>".$status."</td>
							<td width='30%' align='right'>".$golongan."</td>
						</tr>
						<tr>
							<td colspan='2'>".$jabatan."</td>
						</tr>
					</table></td>
				<td style=\"width:1.7cm\" align='center'>".$marital."<br />".$jiwa."</td>
				<td style=\"width:2.4cm\" align='right' valign='top'>".$gajipokok."<br />".$tjistri."<br />".$tjanak."<br />".$j4."</td>
				<td style=\"width:3.4cm\" align='right' valign='top'>".$tjumum."<br />".$tjumum_tambahan."<br />".$tjstruktural."<br />".$tjfungsional."<br />".$tjberas."<br />".$potonganpph."</td>
				<td style=\"width:3.3cm\" align='right' valign='top'>".$pembulatan."<br />".$jmlkotor."</td>
				<td style=\"width:3.7cm\" align='right' valign='top'>".$potbpjskes."<br />".$potpensiun."<br />".$potlain."<br />".$potberas."<br />".$potcp."<br />".$jmlpot."</td>
				<td style=\"width:3cm\" align='right'>".$jmlbersih."</td>
				<td style=\"width:2.8cm\" align='right'>".$tjpph."</td>
				<td style=\"width:3.8cm\" align='right'>".$jmlbersihbayar."</td>
				<td style=\"width:2cm\" align='center'>".$urut."</td>
			</tr>";			
}

/*function showPageJumlahGolongan($sumgol,$sumpeg,$sumistri,$sumanak,$sumjiwa,$sumgajipokok,$sumtjistri,$sumtjanak,$sumj4,$sumtjumum,$sumtjumum_tambahan,$sumtjstruktural,$sumtjfungsional,$sumtjberas,$sumtjpph,$sumpembulatan,$sumjmlkotor,$sumpotbpjskes,$sumpotpensiun,$sumpotlain,$sumpotberas,$sumpotcp,$sumjmlpot,$sumjmlbersih,$sumjmlbersihbayar) {
	return "<tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>JML GOL. ".$sumgol."&nbsp;&nbsp;&nbsp;".$sumpeg." + ".$sumistri." + ".$sumanak." = <br>".str_repeat("&nbsp;",20).$sumjiwa." JIWA</td>
						</tr>
					</table></td>
				<td style=\"width:2.4cm\" align='right' valign='top'>".$sumgajipokok."<br />".$sumtjistri."<br />".$sumtjanak."<br />".$sumj4."</td>
				<td style=\"width:3.4cm\" align='right' valign='top'>".$sumtjumum."<br />".$sumtjumum_tambahan."<br />".$sumtjstruktural."<br />".$sumtjfungsional."<br />".$sumtjberas."<br />".$sumtjpph."</td>
				<td style=\"width:3.3cm\" align='right' valign='top'>".$sumpembulatan."<br />".$sumjmlkotor."</td>
				<td style=\"width:3.7cm\" align='right' valign='top'>".$sumpotbpjskes."<br />".$sumpotpensiun."<br />".$sumpotlain."<br />".$sumpotberas."<br />".$sumpotcp."<br />".$sumjmlpot."</td>
				<td style=\"width:3cm\" align='right'>".$sumjmlbersih."</td>
				<td style=\"width:2.8cm\" align='right'>".$sumtjpph."</td>
				<td style=\"width:3.8cm\" align='right'>".$sumjmlbersihbayar."</td>
				<td style=\"width:2cm\" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr>";
}*/

function showPageJumlahLokasi($sumlokpeg,$sumlokistri,$sumlokanak,$sumlokjiwa,$sumlokgajipokok,$sumloktjistri,$sumloktjanak,$sumlokj4,$sumloktjumum,$sumloktjumum_tambahan,$sumloktjstruktural,$sumloktjfungsional,$sumloktjberas,$sumloktjpph,$sumlokpembulatan,$sumlokjmlkotor,$sumlokpotbpjskes,$sumlokpotpensiun,$sumlokpotlain,$sumlokpotberas,$sumlokpotcp,$sumlokjmlpot,$sumlokjmlbersih,$sumlokjmlbersihbayar) {
	// echo $sumlokpeg;
	// die();
	$potonganpph =0;
	return "<tr valign='top'>
				<td colspan='3' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td width='70%' colspan='2'>PER LOKASI. "."&nbsp;&nbsp;&nbsp;".$sumlokpeg." + ".$sumlokistri." + ".$sumlokanak." = <br>".str_repeat("&nbsp;",20).$sumlokjiwa." JIWA</td>
						</tr>
					</table></td>
				<td style=\"width:2.4cm\" align='right' valign='top'>".$sumlokgajipokok."<br />".$sumloktjistri."<br />".$sumloktjanak."<br />".$sumlokj4."</td>
				<td style=\"width:3.4cm\" align='right' valign='top'>".$sumloktjumum."<br />".$sumloktjumum_tambahan."<br />".$sumloktjstruktural."<br />".$sumloktjfungsional."<br />".$sumloktjberas."<br />".$potonganpph."</td>
				<td style=\"width:3.3cm\" align='right' valign='top'>".$sumlokpembulatan."<br />".$sumlokjmlkotor."</td>
				<td style=\"width:3.7cm\" align='right' valign='top'>".$sumlokpotbpjskes."<br />".$sumlokpotpensiun."<br />".$sumlokpotlain."<br />".$sumlokpotberas."<br />".$sumlokpotcp."<br />".$sumlokjmlpot."</td>
				<td style=\"width:3cm\" align='right'>".$sumlokjmlbersih."</td>
				<td style=\"width:3.8cm\" align='right'>".$sumloktjpph."</td>
				<td style=\"width:2cm\" align='right'>".$sumlokjmlbersihbayar."</td>
				<td style=\"width:2cm\" align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11' height='8'></td>
			</tr>";
}

// Check Data Exist
// $n_kalkulasis = ORM::factory('kalkulasip3k')
// 	->where('periode','=',$periode)
// 	->where('lokasi_id','IN',$arrLokasi)
// 	->count_all();
$this->db->where('periode', $periode);
	$this->db->where_in('lokasi_id', $arrLokasi);
	$this->db->from($tb_kalkulasi);
	$n_kalkulasis = $this->db->count_all_results();

if($n_kalkulasis == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk Lokasi dan atau Periode ini tidak tersedia<br>Silakan pilih Lokasi atau Periode lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

// CSS Block
echo "<div class='cetak'>";
echo "<table style=\"width:33.5cm\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";	

$arrFilter = array();
foreach($arrLokasi as $lokasi) {	
	// $kelompoks = ORM::factory('kalkulasip3k')
	// 	->where('lokasi_id','=',$lokasi)
	// 	->group_by('kelompok_gaji')
	// 	->order_by('kelompok_gaji','ASC')
	// 	->find_all();
    $this->db->where('lokasi_id', $lokasi);
	$this->db->group_by('kelompok_gaji');
	$this->db->order_by('kelompok_gaji','ASC');
	$kelompoks = $this->db->get($tb_kalkulasi)->result();
	
	foreach($kelompoks as $kelompok) {
		// $kalkulasis = ORM::factory('kalkulasip3k')
		// 	->where('periode','=',$periode)
		// 	->where('lokasi_id','=',$lokasi)
		// 	->where('kelompok_gaji','=',$kelompok->kelompok_gaji)
		// 	->order_by('kelompok_gaji','DESC')
		// 	->order_by('tunjangan_struktural','DESC')
		// 	->order_by('golongan_id','DESC')
		// 	->order_by('nip','ASC');
        $this->db->where('periode', $periode);
			$this->db->where('lokasi_id', $lokasi);
			$this->db->where('kelompok_gaji', $kelompok->kelompok_gaji);
			$this->db->order_by('kelompok_gaji','DESC');
			$this->db->order_by('tunjangan_struktural','DESC');
			$this->db->order_by('golongan_id','DESC');
			$this->db->order_by('nip','ASC');
			$kalkulasis = $this->db->get($tb_kalkulasi)->result();
			
		// $kalkulasis = $kalkulasis->find_all();
		foreach($kalkulasis as $kalkulasi) {
			$arrFilter[$kalkulasi->id] = array($kalkulasi->lokasi_id,$kalkulasi->golongan_id[0]);
		}
		
	}
}
$arrFilter['-'] = array('-','-');

$n = count($arrFilter);
$arrCheck = array();
$arrSum = array();
$keys = array_keys($arrFilter);
foreach(array_keys($keys) AS $k) {
	if($n > $k+1) {
		$this_value = $arrFilter[$keys[$k]];
		$nextval = $arrFilter[$keys[$k+1]];
		
		array_push($arrSum,$keys[$k]);
		
		if($this_value != $nextval) {
			$arrCheck[$keys[$k]] = $arrSum;
			$arrSum = array();
		}
	}
}
?>
<table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0">
<?
foreach($arrLokasi as $lokasi) {	
	$kg = 0;
	
	// $lokasi_gaji = ORM::factory('lokasi',$lokasi);

    $this->db->where('id', $lokasi);
	$lokasi_gaji = $this->db->get($tb_lokasi)->row();

	// $kelompoks = ORM::factory('kalkulasip3k')
	// 	->where('lokasi_id','=',$lokasi)
	// 	->group_by('kelompok_gaji')
	// 	->order_by('kelompok_gaji','ASC')
	// 	->find_all();
    $this->db->where('lokasi_id', $lokasi);
	$this->db->group_by('kelompok_gaji');
	$this->db->order_by('kelompok_gaji','ASC');
	$kelompoks = $this->db->get($tb_kalkulasi)->result();
	
	foreach($kelompoks as $kelompok) {
		if($kg > 0) {
			echo $pageBreak;
		}
		$kg++;
		
		// $kalkulasis = ORM::factory('kalkulasip3k')
		// 	->where('periode','=',$periode)
		// 	->where('lokasi_id','=',$lokasi)
		// 	->where('kelompok_gaji','=',$kelompok->kelompok_gaji)
		// 	->order_by('kelompok_gaji','DESC')
		// 	->order_by('tunjangan_struktural','DESC')
		// 	->order_by('golongan_id','DESC')
		// 	->order_by('nip','ASC');
            $this->db->where('periode', $periode);
            $this->db->where('lokasi_id', $lokasi);
            $this->db->where('kelompok_gaji', $kelompok->kelompok_gaji);
            $this->db->order_by('kelompok_gaji','DESC');
            $this->db->order_by('tunjangan_struktural','DESC');
            $this->db->order_by('golongan_id','DESC');
            $this->db->order_by('nip','ASC');
            $kalkulasis = $this->db->get($tb_kalkulasi);
			
		// $nPegKg = $kalkulasis->reset(FALSE)->count_all();	
		// $nPeg = ORM::factory('kalkulasip3k')
		// 	->where('periode','=',$periode)
		// 	->where('lokasi_id','=',$lokasi)
		// 	->count_all();

            $this->db->where('periode', $periode);
            $this->db->where('lokasi_id', $lokasi);
            $this->db->where('kelompok_gaji', $kelompok->kelompok_gaji);
            $this->db->order_by('kelompok_gaji','DESC');
            $this->db->order_by('tunjangan_struktural','DESC');
            $this->db->order_by('golongan_id','DESC');
            $this->db->order_by('nip','ASC');
			$this->db->from($tb_kalkulasi);
            $nPegKg = $this->db->count_all_results();
       
        $this->db->where('periode', $periode);
		$this->db->where('lokasi_id', $lokasi);
		$this->db->from($tb_kalkulasi);
		$nPeg = $this->db->count_all_results();

		if($nPeg) {
			$z = 0;
			$no = 1;
			$j = 1;
			
			$kalkulasis = $kalkulasis->result();
			foreach($kalkulasis as $kalkulasi) {
				$tanggal_lahir = new DateTime($kalkulasi->tanggal_lahir);
				
				$sNama = $kalkulasi->name;
				$sTglLahir = $tanggal_lahir->format("Y-m-d");
				$sNipLama = $kalkulasi->nip;
				$sNipBaru = $kalkulasi->nip;
				$sStatus = $kalkulasi->status_string;
				$sGolongan = $kalkulasi->golongan_id;
				$sStrGolongan = $kalkulasi->golongan_string;
				$sJabatan = $kalkulasi->jabatan;
				$sMarital = $kalkulasi->marital_string;
				$sJmlJiwa = $kalkulasi->jiwa_string;
				$sGajiPokok = numFormat($kalkulasi->gaji_pokok);
				$sTjIstri = numFormat($kalkulasi->tunjangan_istri);
				$sTjAnak = numFormat($kalkulasi->tunjangan_anak);
				$sJ4 = numFormat($kalkulasi->jumlah_penghasilan);
				$sTjUmum = numFormat($kalkulasi->tunjangan_umum);
				$sTjUmumPlus = numFormat($kalkulasi->tunjangan_umum_tambahan);
				$sTjStruktural = numFormat($kalkulasi->tunjangan_struktural);
				$sTjFungsional = numFormat($kalkulasi->tunjangan_fungsional);
				$sTjBeras = numFormat($kalkulasi->tunjangan_beras);
				$sTjPph = numFormat($kalkulasi->tunjangan_pph);
				$sPembulatan = numFormat($kalkulasi->pembulatan);
				$sJmlKotor = numFormat($kalkulasi->jumlah_kotor);
				$sPotBpjsKes = numFormat($kalkulasi->potongan_bpjs_kesehatan);
				$sPotPensiun = numFormat($kalkulasi->potongan_pensiun);
				$sPotLain = numFormat($kalkulasi->potongan_lain);
				$sPotBeras = numFormat($kalkulasi->potongan_beras);
				$sPotCp = numFormat($kalkulasi->potongan_cp);
				$sJmlPot = numFormat($kalkulasi->jumlah_potongan);
				$sJmlBersih = numFormat($kalkulasi->jumlah_bersih);
				$sPotPph = numFormat($kalkulasi->tunjangan_pph);
				$sJmlBersihBayar = numFormat($kalkulasi->jumlah_bersih_bayar);
				
				if($z%6==0 and $z==0) {
					echo showPageHeader($j,$lokasi_gaji->kode,$lokasi_gaji->name,$periode,$kelompok->kelompok_gaji);		
					$j++;
				}
				
				echo showPageData($no,$sNama,$tanggal_lahir->format('d-m-Y'),$sNipBaru,$sStatus,$sStrGolongan,$sJabatan,$sMarital,$sJmlJiwa,$sGajiPokok,$sTjIstri,$sTjAnak,$sJ4,$sTjUmum,$sTjUmumPlus,$sTjStruktural,$sTjFungsional,$sTjBeras,$sTjPph,$sPembulatan,$sJmlKotor,$sPotBpjsKes,$sPotPensiun,$sPotLain,$sPotBeras,$sPotCp,$sJmlPot,$sJmlBersih,$sJmlBersihBayar);
				$z++;
				
				if($z%6==0 and $z > 0) {
					echo $pageBreak;
					echo showPageHeader($j,$lokasi_gaji->kode,$lokasi_gaji->name,$periode,$kelompok->kelompok_gaji);
					$j++;
				}
								
				/*if(array_key_exists($kalkulasi->id,$arrCheck)) {
					$in_array = "";
					foreach($arrCheck[$kalkulasi->id] as $key=>$val) {
						$in_array .= $val.",";
					}
					$in_array = substr_replace($in_array,"",-1);
					
					$sql = 
					"SELECT count(id) AS jmlpeg, sum(istri) AS jmlistri, sum(anak) AS jmlanak,  sum(gaji_pokok) AS gajipokok, 
					sum(tunjangan_istri) AS tjistri, sum(tunjangan_anak) AS tjanak, sum(jumlah_penghasilan) AS jmlpenghasilan, sum(tunjangan_umum) AS tjumum, 
					sum(tunjangan_umum_tambahan) AS tjumum_tambahan, sum(tunjangan_struktural) AS tjstruktural, sum(tunjangan_fungsional) AS tjfungsional, 
					sum(tunjangan_beras) AS tjberas, sum(tunjangan_pph) AS tjpph, sum(pembulatan) AS pembulatan, sum(jumlah_kotor) AS jmlkotor, 
					 sum(potongan_bpjs_kesehatan) AS potbpjskes, sum(potongan_pensiun) AS potpensiun, sum(potongan_lain) AS potlain, sum(potongan_beras) AS potberas, sum(potongan_cp) AS potcp, 
					sum(jumlah_potongan) AS jmlpotongan, sum(jumlah_bersih) AS jmlbersih, sum(jumlah_bersih_bayar) AS jmlbersihbayar 
					FROM kalkulasip3ks WHERE id IN (".$in_array.")"; #floor(0.1 * sum(jumlah_penghasilan)) diganti sum(potongan_iwp)	
					
					$sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
					foreach($sqls as $sql) {
						$tJmlPeg = $sql->jmlpeg;
						$tJmlIstri = $sql->jmlistri;
						$tJmlAnak = $sql->jmlanak;
						$tGajiPokok = numFormat($sql->gajipokok);
						$tTjIstri = numFormat($sql->tjistri);
						$tTjAnak = numFormat($sql->tjanak);
						$tJ4 = numFormat($sql->jmlpenghasilan);
						$tTjUmum = numFormat($sql->tjumum);
						$tTjUmumPlus = numFormat($sql->tjumum_tambahan);
						$tTjStruktural = numFormat($sql->tjstruktural);
						$tTjFungsional = numFormat($sql->tjfungsional);
						$tTjBeras = numFormat($sql->tjberas);
						$tTjPph = numFormat($sql->tjpph);
						$tPembulatan = numFormat($sql->pembulatan);
						$tJmlKotor = numFormat($sql->jmlkotor);
						$tPotBpjsKes = numFormat($sql->potbpjskes);
						$tPotPensiun = numFormat($sql->potpensiun);
						$tPotLain = numFormat($sql->potlain);
						$tPotBeras = numFormat($sql->potberas);
						$tPotCp = numFormat($sql->potcp);
						$tJmlPot = numFormat($sql->jmlpotongan);
						$tJmlBersih = numFormat($sql->jmlbersih);
						$tPotPph = numFormat($sql->tjpph);
						$tJmlBersihBayar = numFormat($sql->jmlbersihbayar);		
						$tJmlJiwa = $tJmlPeg + $tJmlIstri + $tJmlAnak; 
					} 
					
					$x_golongan = explode(" ",$sStrGolongan);
					
					echo showPageJumlahGolongan($x_golongan[0],$tJmlPeg,$tJmlIstri,$tJmlAnak,$tJmlJiwa,$tGajiPokok,$tTjIstri,$tTjAnak,$tJ4,$tTjUmum,$tTjUmumPlus,$tTjStruktural,$tTjFungsional,$tTjBeras,$tTjPph,$tPembulatan,$tJmlKotor,$tPotBpjsKes,$tPotPensiun,$tPotLain,$tPotBeras,$tPotCp,$tJmlPot,$tJmlBersih,$tJmlBersihBayar);
					$z++;	

					if($z%6==0) {
						echo $pageBreak;
						echo showPageHeader($j,$lokasi_gaji->kode,$lokasi_gaji->name,$periode,$kelompok->kelompok_gaji);
						$j++;
					}
				}*/
				$no++;
			}
		}
		
		// JUMLAH PER LOKASI
		$sql = 
		"SELECT count(id) AS jmlpeg, sum(istri) AS jmlistri, sum(anak) AS jmlanak,  sum(gaji_pokok) AS gajipokok, 
		sum(tunjangan_istri) AS tjistri, sum(tunjangan_anak) AS tjanak, sum(jumlah_penghasilan) AS jmlpenghasilan, sum(tunjangan_umum) AS tjumum, 
		sum(tunjangan_umum_tambahan) AS tjumum_tambahan, sum(tunjangan_struktural) AS tjstruktural, sum(tunjangan_fungsional) AS tjfungsional, 
		sum(tunjangan_beras) AS tjberas, sum(tunjangan_pph) AS tjpph, sum(pembulatan) AS pembulatan, sum(jumlah_kotor) AS jmlkotor, 
		sum(potongan_bpjs_kesehatan) AS potbpjskes, sum(potongan_pensiun) AS potpensiun, sum(potongan_lain) AS potlain, sum(potongan_beras) AS potberas, sum(potongan_cp) AS potcp, 
		sum(jumlah_potongan) AS jmlpotongan, sum(jumlah_bersih) AS jmlbersih, sum(jumlah_bersih_bayar) AS jmlbersihbayar 
		FROM $tb_kalkulasi WHERE lokasi_id = ".$lokasi." AND periode = '".$periode."' AND kelompok_gaji = '".$kelompok->kelompok_gaji."'";	
		
		// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
        $sqls = $this->db->query($sql)->result();
		$z = 0;					
		$j = 0;	
		foreach($sqls as $sql) {
			$tJmlPeg = $sql->jmlpeg;
			$tJmlIstri = $sql->jmlistri;
			$tJmlAnak = $sql->jmlanak;
			$tGajiPokok = numFormat($sql->gajipokok);
			$tTjIstri = numFormat($sql->tjistri);
			$tTjAnak = numFormat($sql->tjanak);
			$tJ4 = numFormat($sql->jmlpenghasilan);
			$tTjUmum = numFormat($sql->tjumum);
			$tTjUmumPlus = numFormat($sql->tjumum_tambahan);
			$tTjStruktural = numFormat($sql->tjstruktural);
			$tTjFungsional = numFormat($sql->tjfungsional);
			$tTjBeras = numFormat($sql->tjberas);
			$tTjPph = numFormat($sql->tjpph);
			$tPembulatan = numFormat($sql->pembulatan);
			$tJmlKotor = numFormat($sql->jmlkotor);
			$tPotBpjsKes = numFormat($sql->potbpjskes);
			$tPotPensiun = numFormat($sql->potpensiun);
			$tPotLain = numFormat($sql->potlain);
			$tPotBeras = numFormat($sql->potberas);
			$tPotCp = numFormat($sql->potcp);
			$tJmlPot = numFormat($sql->jmlpotongan);
			$tJmlBersih = numFormat($sql->jmlbersih);
			$tPotPph = numFormat($sql->tjpph);
			$tJmlBersihBayar = numFormat($sql->jmlbersihbayar);		
			$tJmlJiwa = $tJmlPeg + $tJmlIstri + $tJmlAnak; 
		} 
		
		if($tJmlJiwa != '0 JIWA'){
			echo showPageJumlahLokasi($tJmlPeg,$tJmlIstri,$tJmlAnak,$tJmlJiwa,$tGajiPokok,$tTjIstri,$tTjAnak,$tJ4,$tTjUmum,$tTjUmumPlus,$tTjStruktural,$tTjFungsional,$tTjBeras,$tTjPph,$tPembulatan,$tJmlKotor,$tPotBpjsKes,$tPotPensiun,$tPotLain,$tPotBeras,$tPotCp,$tJmlPot,$tJmlBersih,$tJmlBersihBayar);
			$z++;	
		}
		
		
		echo $pageBreak;
		$j++;
	}	
}
?>
</table>
</div>
</body>
</html>