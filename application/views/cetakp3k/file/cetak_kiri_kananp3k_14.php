<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="cetak">
<?

$tb_kalkulasi = 'simgaji_kalkulasip3ks_eb';
$tb_lokasi = 'simgaji_lokasis';
$tb_golongan = 'simgaji_golonganp3ks';

// Check Data Exist
// $n_kalkulasis = ORM::factory('kalkulasip3k')
// 	->where('periode','=',$periode)
// 	->where('lokasi_id','=',$arrLokasiInduk)
// 	->count_all();

    $this->db->where('periode', $periode);
$this->db->where_in('lokasi_id', $arrLokasi);
$this->db->from($tb_kalkulasi);
$n_kalkulasis = $this->db->count_all_results();

if($n_kalkulasis == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk Lokasi dan atau Periode ini tidak tersedia<br>Silakan pilih Lokasi atau Periode lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

$page_break = "<div style=\"page-break-after:always\"></div>";

$page=1;
foreach($arrLokasiInduk AS $lokasi) {
	// $lokasi = ORM::factory('lokasi',$lokasi);
	// $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
	// $strLokasi = $lokasi->name;
    $this->db->where('id', $lokasi);
	$lokasi = $this->db->get($tb_lokasi)->row();
    $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
    $strLokasi = $lokasi->name;

	$xPeriode = explode("-",$periode);
	// $bulan = ORM::factory('bulan',intval($xPeriode[1]));
    $bulan = get_bulan(intval($xPeriode[1]));

	// KIRI
	if($page > 1) {
		echo $page_break;
	}

	?>
	<table style="width:33.5cm" border="0" cellspacing="0" cellpadding="3">
		<tr>
			<td colspan="2" align="left" valign="top">HALAMAN : 1</td>
			<td width="54%" rowspan="2" align="center" valign="top">PEMERINTAH PROVINSI JAWA TENGAH<br />REKAPITULASI GAJI 14 DSB UNTUK PARA PPPK<br />
		  															KODE LOKASI : <? echo $kode; ?>&nbsp;&nbsp;&nbsp;<? echo $strLokasi; ?></td>
			<td width="18%" rowspan="2" align="center" valign="top">&nbsp;</td>
		</tr>
		<tr align="left">
		  <td width="12%" valign="bottom">BAGIAN BULAN<br />KODE REKENING </td>
		  <td width="16%" valign="top">: <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?></td>
      </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" style="border-top:dashed 1px #000000;width:33.5cm">
	  <tr>
			<td width="7%" rowspan="2" >GOL. /<br />RUANG</td>
			<td colspan="4" align="center" class="borBothBottom">PEGAWAI &amp; KELUARGA</td>
			<td width="6%" rowspan="2" align="center"><p>GAJI POKOK</p></td>
			<td colspan="2" align="center" class="borBothBottom">TUNJANGAN KELUARGA</td>
			<td width="10%" rowspan="2" align="center">JML <br />PENGHASILAN</td>
			<td width="11%" rowspan="2" align="center" class="borLeft">TJ UMUM<br />TMB.TJ.UMUM</td>
			<td colspan="2" align="center" class="borBothBottom">TUNJANGAN</td>
			<td width="8%" rowspan="2" align="center">PEMBULATAN</td>
	  </tr>
		<tr>
			<td width="3%" align="center" class="borBoth">PEG</td>
			<td width="5%" align="center">IST/<br />SUAMI</td>
			<td width="5%" align="center" class="borBoth">ANAK</td>
			<td width="5%" align="center" class="borRight">JML JIWA</td>
			<td width="10%" align="center" class="borBoth">TJ.ISTRI<br />TJ. ANAK</td>
			<td width="7%" align="center" class="borRight">JUMLAH</td>
			<td width="7%" align="center" class="borBoth">STRUK.<br />FUNGS.</td>
			<td width="9%" align="center" class="borRight">BERAS<br />PPH</td>
	  </tr>
		<tr align="center">
			<td class="borTopBottom">1</td>
			<td width="3%" class="borTopBottom">2</td>
			<td width="5%" class="borTopBottom">3</td>
			<td width="5%" class="borTopBottom">4</td>
			<td class="borTopBottom">5</td>
			<td width="6%" class="borTopBottom">6</td>
			<td width="10%" class="borTopBottom">7/8</td>
			<td width="7%" class="borTopBottom">9</td>
			<td class="borTopBottom">10</td>
			<td width="11%" class="borTopBottom">11/12</td>
			<td width="7%" class="borTopBottom">13/14</td>
			<td width="9%" class="borTopBottom">15/16</td>
			<td class="borTopBottom">17</td>
		</tr>
		<tr align="center">
			<td colspan="13">&nbsp;</td>
		</tr>
	<?
	// $prefixes = ORM::factory('golonganp3k')->group_by('id')->find_all();
    $this->db->group_by('id');
    $prefixes = $this->db->get($tb_golongan)->result();
	foreach($prefixes as $prefix) {
		// $golongans = ORM::factory('golonganp3k')->where('prefix','=',$prefix->prefix)->find_all();
        $this->db->where('prefix',$prefix->prefix);
        $golongans = $this->db->get($tb_golongan)->result();
		foreach($golongans as $golongan) {
			$sql =
			"SELECT
			count(id) AS pegawai,
			sum(istri) AS istri,
			sum(anak) AS anak,
			sum(jiwa) AS jiwa,
			sum(gaji_pokok) AS gaji_pokok,
			sum(tunjangan_istri) AS tunjangan_istri,
			sum(tunjangan_anak) AS tunjangan_anak,
			sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
			sum(jumlah_penghasilan) AS jumlah_penghasilan,
			sum(tunjangan_umum) AS tunjangan_umum,
			sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
			sum(tunjangan_struktural) AS tunjangan_struktural,
			sum(tunjangan_fungsional) AS tunjangan_fungsional,
			sum(tunjangan_beras) AS tunjangan_beras,
			sum(tunjangan_pph) AS tunjangan_pph,
			sum(pembulatan) AS pembulatan,
			sum(jumlah_kotor) AS jumlah_kotor,
			sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
			sum(potongan_pensiun) AS potongan_pensiun,
			sum(potongan_lain) AS potongan_lain,
			sum(potongan_beras) AS potongan_beras,
			sum(potongan_cp) AS potongan_cp,
			sum(jumlah_potongan) AS jumlah_potongan,
			sum(jumlah_bersih) AS jumlah_bersih,
			sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
			sum(askes) AS askes
			FROM $tb_kalkulasi
			WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND golongan_id = '".$golongan->id."'";

			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
            $sqls = $this->db->query($sql)->result();
			foreach($sqls as $row) {
				$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
				$pensiun = round(0.08 * $row->jumlah_penghasilan);
				$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
				$n_jml_kotor = $row->jumlah_bersih + $n_potongan;
				$s_jml_kotor = $n_jml_kotor - $row->jumlah_kotor;
				$n_pembulatan = $row->pembulatan + $s_jml_kotor;
				?>
				<tr>
					<td valign="top"><? echo "GOL. ".$golongan->kode; ?></td>
					<td align="center" width="3%" valign="top"><? echo round($row->pegawai); ?></td>
					<td align="center" width="5%" valign="top"><? echo round($row->istri); ?></td>
					<td align="center" width="5%" valign="top"><? echo round($row->anak); ?></td>
					<td align="center" valign="top"><? echo round($row->jiwa); ?></td>
					<td align="right" width="6%" valign="top"><? echo numFormat($row->gaji_pokok); ?></td>
					<td align="right" width="10%" valign="top"><? echo numFormat($row->tunjangan_istri); ?><br /><? echo numFormat($row->tunjangan_anak); ?></td>
					<td align="right" width="7%" valign="top"><? echo numFormat($row->jumlah_tunjangan_keluarga); ?></td>
					<td align="right" valign="top"><? echo numFormat($row->jumlah_penghasilan); ?></td>
					<td align="right" width="11%" valign="top"><? echo numFormat($row->tunjangan_umum); ?><br /><? echo numFormat($row->tunjangan_umum_tambahan); ?></td>
					<td align="right" width="7%" valign="top"><? echo numFormat($row->tunjangan_struktural); ?><br /><? echo numFormat($row->tunjangan_fungsional); ?></td>
					<td align="right" width="9%" valign="top"><? echo numFormat($row->tunjangan_beras); ?><br /><? echo numFormat(0); ?></td>
					<td align="right" valign="top"><? echo numFormat($row->pembulatan); ?></td>
				</tr>
			<?
			}
		}

		/*$sql =
		"SELECT
		count(id) AS pegawai,
		sum(istri) AS istri,
		sum(anak) AS anak,
		sum(jiwa) AS jiwa,
		sum(gaji_pokok) AS gaji_pokok,
		sum(tunjangan_istri) AS tunjangan_istri,
		sum(tunjangan_anak) AS tunjangan_anak,
		sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
		sum(jumlah_penghasilan) AS jumlah_penghasilan,
		sum(tunjangan_umum) AS tunjangan_umum,
		sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
		sum(tunjangan_struktural) AS tunjangan_struktural,
		sum(tunjangan_fungsional) AS tunjangan_fungsional,
		sum(tunjangan_beras) AS tunjangan_beras,
		sum(tunjangan_pph) AS tunjangan_pph,
		sum(pembulatan) AS pembulatan,
		sum(jumlah_kotor) AS jumlah_kotor,
		sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
		sum(potongan_pensiun) AS potongan_pensiun,
		sum(potongan_lain) AS potongan_lain,
		sum(potongan_beras) AS potongan_beras,
		sum(potongan_cp) AS potongan_cp,
		sum(jumlah_potongan) AS jumlah_potongan,
		sum(jumlah_bersih) AS jumlah_bersih,
		sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
		sum(askes) AS askes
		FROM kalkulasis
		WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND LEFT(golongan_id,1) = '".substr($prefix->id,0,1)."'";

		$sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
		foreach($sqls as $row) {
			$tPeg = $row->pegawai;
			$tIstri = $row->istri;
			$tAnak = $row->anak;
			$tJiwa = $row->jiwa;
			$tGajiPokok = $row->gaji_pokok;
			$tTjIstri = $row->tunjangan_istri;
			$tTjAnak = $row->tunjangan_anak;
			$tTjKeluarga = $row->jumlah_tunjangan_keluarga;
			$tPenghasilan = $row->jumlah_penghasilan;
			$tTjUmum = $row->tunjangan_umum;
			$tTjUmumPlus = $row->tunjangan_umum_tambahan;
			$tTjStruktural = $row->tunjangan_struktural;
			$tTjFungsional = $row->tunjangan_fungsional;
			$tTjBeras = $row->tunjangan_beras;
			$tTjPPH =$row->tunjangan_pph;

			$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
			$pensiun = round(0.08 * $row->jumlah_penghasilan);
			$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
			$n_jml_kotor = $row->jumlah_bersih + $n_potongan;
			$s_jml_kotor = $n_jml_kotor - $row->jumlah_kotor;
			$t_pembulatan = $row->pembulatan;# + $s_jml_kotor;
			?>
			<tr>
				<td valign="top"><? echo "JML.GOL.".$prefix->prefix; ?></td>
				<td align="center" valign="top" width="3%"><? echo numFormat($tPeg); ?></td>
				<td align="center" valign="top" width="5%"><? echo numFormat($tIstri); ?></td>
				<td align="center" valign="top" width="5%"><? echo numFormat($tAnak); ?></td>
				<td align="center" valign="top"><? echo numFormat($tJiwa); ?></td>
				<td align="right" valign="top" width="6%"><? echo numFormat($tGajiPokok); ?></td>
				<td align="right" valign="top" width="10%"><? echo numFormat($tTjIstri); ?><br /><? echo numFormat($tTjAnak); ?></td>
				<td align="right" valign="top" width="7%"><? echo numFormat($tTjKeluarga); ?></td>
				<td align="right" valign="top"><? echo numFormat($tPenghasilan); ?></td>
				<td align="right" valign="top" width="11%"><? echo numFormat($tTjUmum); ?><br /><? echo numFormat($tTjUmumPlus); ?></td>
				<td align="right" valign="top" width="7%"><? echo numFormat($tTjStruktural); ?><br /><? echo numFormat($tTjFungsional); ?></td>
				<td align="right" valign="top" width="9%"><? echo numFormat($tTjBeras); ?><br /><? echo numFormat($tTjPPH); ?></td>
				<td align="right" valign="top"><? echo numFormat($t_pembulatan); ?></td>
			</tr>
			<tr align="center">
				<td colspan="13" height="5"><hr /></td>
			</tr>
		<?
		}*/
	}

	$sql =
	"SELECT
	count(id) AS pegawai,
	sum(istri) AS istri,
	sum(anak) AS anak,
	sum(jiwa) AS jiwa,
	sum(gaji_pokok) AS gaji_pokok,
	sum(tunjangan_istri) AS tunjangan_istri,
	sum(tunjangan_anak) AS tunjangan_anak,
	sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
	sum(jumlah_penghasilan) AS jumlah_penghasilan,
	sum(tunjangan_umum) AS tunjangan_umum,
	sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
	sum(tunjangan_struktural) AS tunjangan_struktural,
	sum(tunjangan_fungsional) AS tunjangan_fungsional,
	sum(tunjangan_beras) AS tunjangan_beras,
	sum(tunjangan_pph) AS tunjangan_pph,
	sum(pembulatan) AS pembulatan,
	sum(jumlah_kotor) AS jumlah_kotor,
	sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
	sum(potongan_pensiun) AS potongan_pensiun,
	sum(potongan_lain) AS potongan_lain,
	sum(potongan_beras) AS potongan_beras,
	sum(potongan_cp) AS potongan_cp,
	sum(jumlah_potongan) AS jumlah_potongan,
	sum(jumlah_bersih) AS jumlah_bersih,
	sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
	sum(askes) AS askes
	FROM $tb_kalkulasi
	WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."'";

	// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
    $sqls = $this->db->query($sql)->result();
	foreach($sqls as $row) {
		$ttPeg = $row->pegawai;
		$ttIstri = $row->istri;
		$ttAnak = $row->anak;
		$ttJiwa = $row->jiwa;
		$ttGajiPokok = $row->gaji_pokok;
		$ttTjIstri = $row->tunjangan_istri;
		$ttTjAnak = $row->tunjangan_anak;
		$ttTjKeluarga = $row->jumlah_tunjangan_keluarga;
		$ttPenghasilan = $row->jumlah_penghasilan;
		$ttTjUmum = $row->tunjangan_umum;
		$ttTjUmumPlus = $row->tunjangan_umum_tambahan;
		$ttTjStruktural = $row->tunjangan_struktural;
		$ttTjFungsional = $row->tunjangan_fungsional;
		$ttTjBeras = $row->tunjangan_beras;
		$ttTjPPH = 0;

		$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
		$pensiun = round(0.08 * $row->jumlah_penghasilan);
		$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
		$n_jml_kotor = $row->jumlah_bersih + $n_potongan;
		#$s_jml_kotor = $n_jml_kotor - $row->jumlah_kotor;
		$tt_pembulatan = $row->pembulatan;#+ $s_jml_kotor;
		?>
		<tr>
			<td valign="top">TOTAL</td>
			<td align="center" valign="top" width="3%"><? echo round($ttPeg); ?></td>
			<td align="center" valign="top" width="5%"><? echo round($ttIstri); ?></td>
			<td align="center" valign="top" width="5%"><? echo round($ttAnak); ?></td>
			<td align="center" valign="top"><? echo round($ttJiwa); ?></td>
			<td align="right" valign="top" width="6%"><? echo numFormat($ttGajiPokok); ?></td>
			<td align="right" valign="top" width="10%"><? echo numFormat($ttTjIstri); ?><br /><? echo numFormat($ttTjAnak); ?></td>
			<td align="right" valign="top" width="7%"><? echo numFormat($ttTjKeluarga); ?></td>
			<td align="right" valign="top"><? echo numFormat($ttPenghasilan); ?></td>
			<td align="right" valign="top" width="11%"><? echo numFormat($ttTjUmum); ?><br /><? echo numFormat($ttTjUmumPlus); ?></td>
			<td align="right" valign="top" width="7%"><? echo numFormat($ttTjStruktural); ?><br /><? echo numFormat($ttTjFungsional); ?></td>
			<td align="right" valign="top" width="9%"><? echo numFormat($ttTjBeras); ?><br /><? echo numFormat($ttTjPPH); ?></td>
			<td align="right" valign="top"><? echo numFormat($tt_pembulatan); ?></td>
		</tr>
		<?
	}
	?>
    	<tr align="center">
            <td colspan="13" height="5"><hr /></td>
        </tr>
	</table>
	<?

	// KANAN
	echo "<div style=\"page-break-after:always\"></div>";
	?>
    <table style="width:33.5cm" border="0" cellspacing="0" cellpadding="3">
		<tr align="left">
		  <td width="12%" valign="bottom">BAGIAN BULAN<br />KODE REKENING </td>
		  <td width="52%" valign="top"> : <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?><br />:</td>
		  <td width="36%" align="right" valign="top">HALAMAN : 1<br />KODE LOKASI : <? echo $kode; ?></td>
      </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" style="border-top:dashed 1px #000000;width:33.5cm">
 		<tr>
			<td width="10%" rowspan="2" align="center">JML<br />KOTOR</td>
			<td colspan="5" align="center" class="borBothBottom">POTONGAN</td>
			<td width="12%" rowspan="2" align="center" >JML<br />POTONGAN</td>
			<td width="11%" rowspan="2" align="center" class="borBoth">JML<br />BERSIH</td>
			<td width="10%" rowspan="2" align="center" >PPH<br />(=16)</td>
			<td width="14%" rowspan="2" align="center" class="borLeft">JML. BERSIH<br />YG DIBAYARKAN</td>
		</tr>
		<tr>
			<td width="9%" align="center" class="borBoth">BPJS KES</td>
            <td width="9%" align="center" class="borBoth">PENSIUN</td>
			<td width="8%" align="center" >LAIN-LAIN</td>
			<td width="9%" align="center" class="borBoth">POT. BERAS</td>
			<td width="8%" align="center" class="borRight">CP</td>
		</tr>
		<tr align="center">
			<td width="10%" class="borTopBottom">18</td>
			<td width="9%" class="borTopBottom">19</td>
            <td width="9%" class="borTopBottom">20</td>
			<td width="8%" class="borTopBottom">21</td>
			<td width="9%" class="borTopBottom">22</td>
			<td width="8%" class="borTopBottom">23</td>
			<td width="12%" class="borTopBottom">24</td>
			<td width="11%" class="borTopBottom">25</td>
			<td width="10%" class="borTopBottom">26</td>
			<td width="14%" class="borTopBottom">27</td>
		</tr>
		<tr align="center">
			<td colspan="10">&nbsp;</td>
		</tr>
		<?

	// $prefixes = ORM::factory('golonganp3k')->group_by('id')->find_all();
    $this->db->group_by('id');
    $prefixes = $this->db->get($tb_golongan)->result();
	foreach($prefixes as $prefix) {
		// $golongans = ORM::factory('golonganp3k')->where('prefix','=',$prefix->prefix)->find_all();
        $this->db->where('prefix',$prefix->prefix);
        $golongans = $this->db->get($tb_golongan)->result();
		foreach($golongans as $golongan) {
			$sql =
			"SELECT
			count(id) AS pegawai,
			sum(istri) AS istri,
			sum(anak) AS anak,
			sum(jiwa) AS jiwa,
			sum(gaji_pokok) AS gaji_pokok,
			sum(tunjangan_istri) AS tunjangan_istri,
			sum(tunjangan_anak) AS tunjangan_anak,
			sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
			sum(jumlah_penghasilan) AS jumlah_penghasilan,
			sum(tunjangan_umum) AS tunjangan_umum,
			sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
			sum(tunjangan_struktural) AS tunjangan_struktural,
			sum(tunjangan_fungsional) AS tunjangan_fungsional,
			sum(tunjangan_beras) AS tunjangan_beras,
			sum(tunjangan_pph) AS tunjangan_pph,
			sum(pembulatan) AS pembulatan,
			sum(jumlah_kotor) AS jumlah_kotor,
			sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
			sum(potongan_pensiun) AS potongan_pensiun,
			sum(potongan_lain) AS potongan_lain,
			sum(potongan_beras) AS potongan_beras,
			sum(potongan_cp) AS potongan_cp,
			sum(jumlah_potongan) AS jumlah_potongan,
			sum(jumlah_bersih) AS jumlah_bersih,
			sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
			sum(askes) AS askes
			FROM $tb_kalkulasi
			WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND golongan_id = '".$golongan->id."'";

			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
            $sqls = $this->db->query($sql)->result();
			foreach($sqls as $row) {
				$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
				$pensiun = round(0.08 * $row->jumlah_penghasilan);
				$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
				$n_jml_kotor = $row->jumlah_bersih + $n_potongan;
				$s_jml_kotor = $n_jml_kotor - $row->jumlah_kotor;
				$n_pembulatan = $row->pembulatan + $s_jml_kotor;
				?>
				<tr>
					<td align="right" valign="top" width="10%"><? echo numFormat($row->jumlah_kotor)."<br>"; ?></td>
					<td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_bpjs_kesehatan)."<br>"; ?></td>
                    <td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_pensiun)."<br>"; ?></td>
					<td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_lain)."<br>"; ?></td>
					<td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_beras)."<br>"; ?></td>
					<td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_cp)."<br>"; ?></td>
					<td align="right" valign="top" width="12%"><? echo numFormat($row->jumlah_potongan)."<br>"; ?></td>
					<td align="right" valign="top" width="11%"><? echo numFormat($row->jumlah_bersih)."<br>"; ?></td>
					<td align="right" valign="top" width="10%"><? echo numFormat($row->tunjangan_pph)."<br>"; ?></td>
					<td align="right" valign="top" width="14%"><? echo numFormat($row->jumlah_bersih_bayar); ?><br /><br /></td>
				</tr>
				<?
			}
		}

		/*$sql =
		"SELECT
		count(id) AS pegawai,
		sum(istri) AS istri,
		sum(anak) AS anak,
		sum(jiwa) AS jiwa,
		sum(gaji_pokok) AS gaji_pokok,
		sum(tunjangan_istri) AS tunjangan_istri,
		sum(tunjangan_anak) AS tunjangan_anak,
		sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
		sum(jumlah_penghasilan) AS jumlah_penghasilan,
		sum(tunjangan_umum) AS tunjangan_umum,
		sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
		sum(tunjangan_struktural) AS tunjangan_struktural,
		sum(tunjangan_fungsional) AS tunjangan_fungsional,
		sum(tunjangan_beras) AS tunjangan_beras,
		sum(tunjangan_pph) AS tunjangan_pph,
		sum(pembulatan) AS pembulatan,
		sum(jumlah_kotor) AS jumlah_kotor,
		sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
		sum(potongan_pensiun) AS potongan_pensiun,
		sum(potongan_lain) AS potongan_lain,
		sum(potongan_beras) AS potongan_beras,
		sum(potongan_cp) AS potongan_cp,
		sum(jumlah_potongan) AS jumlah_potongan,
		sum(jumlah_bersih) AS jumlah_bersih,
		sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
		sum(askes) AS askes
		FROM kalkulasis
		WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND LEFT(golongan_id,1) = '".substr($golongan->id,0,1)."'";

		$sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
		foreach($sqls as $row) {
			$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
			$pensiun = round(0.08 * $row->jumlah_penghasilan);
			$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
			$n_jml_kotor = $row->jumlah_bersih + $n_potongan;
			$s_jml_kotor = $n_jml_kotor - $row->jumlah_kotor;
			$n_pembulatan = $row->pembulatan + $s_jml_kotor;
			?>
			<tr>
				<td align="right" valign="top" width="10%"><? echo numFormat($row->jumlah_kotor); ?></td>
				<td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_bpjs_kesehatan); ?></td>
                <td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_pensiun); ?></td>
				<td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_lain); ?></td>
				<td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_beras); ?></td>
				<td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_cp); ?></td>
				<td align="right" valign="top" width="12%"><? echo numFormat($row->jumlah_potongan); ?></td>
				<td align="right" valign="top" width="11%"><? echo numFormat($row->jumlah_bersih); ?></td>
				<td align="right" valign="top" width="10%"><? echo numFormat($row->tunjangan_pph); ?></td>
				<td align="right" valign="top" width="14%"><? echo numFormat($row->jumlah_bersih_bayar); ?><br /><br /></td>
			</tr>
			<tr align="right">
				<td colspan="10" height="5"><hr /></td>
			</tr>
		<?
		}*/
	}

	$sql =
	"SELECT
	count(id) AS pegawai,
	sum(istri) AS istri,
	sum(anak) AS anak,
	sum(jiwa) AS jiwa,
	sum(gaji_pokok) AS gaji_pokok,
	sum(tunjangan_istri) AS tunjangan_istri,
	sum(tunjangan_anak) AS tunjangan_anak,
	sum(jumlah_tunjangan_keluarga) AS jumlah_tunjangan_keluarga,
	sum(jumlah_penghasilan) AS jumlah_penghasilan,
	sum(tunjangan_umum) AS tunjangan_umum,
	sum(tunjangan_umum_tambahan) AS tunjangan_umum_tambahan,
	sum(tunjangan_struktural) AS tunjangan_struktural,
	sum(tunjangan_fungsional) AS tunjangan_fungsional,
	sum(tunjangan_beras) AS tunjangan_beras,
	sum(tunjangan_pph) AS tunjangan_pph,
	sum(pembulatan) AS pembulatan,
	sum(jumlah_kotor) AS jumlah_kotor,
	sum(potongan_bpjs_kesehatan) AS potongan_bpjs_kesehatan,
	sum(potongan_pensiun) AS potongan_pensiun,
	sum(potongan_lain) AS potongan_lain,
	sum(potongan_beras) AS potongan_beras,
	sum(potongan_cp) AS potongan_cp,
	sum(jumlah_potongan) AS jumlah_potongan,
	sum(jumlah_bersih) AS jumlah_bersih,
	sum(jumlah_bersih_bayar) AS jumlah_bersih_bayar,
	sum(askes) AS askes
	FROM $tb_kalkulasi
	WHERE LEFT(lokasi_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."'";

	// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
    $sqls = $this->db->query($sql)->result();
	foreach($sqls as $row) {
		$bpjs_kes = round(0.01 * ($row->jumlah_penghasilan + $row->tunjangan_umum + $row->tunjangan_fungsional + $row->tunjangan_struktural));
		$pensiun = round(0.08 * $row->jumlah_penghasilan);
		$n_potongan = $bpjs_kes + $pensiun + $row->potongan_lain;
		$n_jml_kotor = $row->jumlah_bersih + $n_potongan;
		$s_jml_kotor = $n_jml_kotor - $row->jumlah_kotor;
		$n_pembulatan = $row->pembulatan + $s_jml_kotor;
		?>
		<tr>
			<td align="right" valign="top" width="10%"><? echo numFormat($row->jumlah_kotor); ?></td>
			<td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_bpjs_kesehatan); ?></td>
            <td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_pensiun); ?></td>
			<td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_lain); ?></td>
			<td align="right" valign="top" width="9%"><? echo numFormat($row->potongan_beras); ?></td>
			<td align="right" valign="top" width="8%"><? echo numFormat($row->potongan_cp); ?></td>
			<td align="right" valign="top" width="12%"><? echo numFormat($row->jumlah_potongan); ?></td>
			<td align="right" valign="top" width="11%"><? echo numFormat($row->jumlah_bersih); ?></td>
			<td align="right" valign="top" width="10%"><? echo numFormat($row->tunjangan_pph); ?></td>
			<td align="right" valign="top" width="14%"><? echo numFormat($row->jumlah_bersih_bayar); ?><br /><br /></td>
		</tr>
		<tr align="right">
			<td colspan="10" height="5"><hr /></td>
		</tr>
		<?
	}
	$page++;
	?>
</table>
<?
}
?>
</div>
</body>
</html>
