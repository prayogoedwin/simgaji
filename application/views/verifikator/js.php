<script>
// $(document).ready(function() {
//     $('#kode_skpd_add').select2();
//     $('#kode_skpd_edit').select2({
//         allowClear: true
//     });

    
// });

$('.select2').select2({
    formatResult:function(object, container, query){
        if(object.id=='all' || object.id=='clear')
            return '<span style="color:#31708F;font-size:10px;"><i class="fa fa-chevron-right"></i> '+object.text+'</span>';

        return object.text;
    }
});
$('.select2').on("change", function(e) {
    if($.inArray('all', e.val)===0){
        var selected = [];
        $(this).find("option").each(function(i,e){
            if($(e).attr("value")=='all' || $(e).attr("value")=='clear')
                return true;

            selected[selected.length]=$(e).attr("value");
        });
        $(this).select2('val',selected);
    }else if($.inArray('clear', e.val)===0){
        $(this).select2('val','');
    }
});


// $.fn.select2.amd.require([
//   'select2/utils',
//   'select2/dropdown',
//   'select2/dropdown/attachBody'
// ], function (Utils, Dropdown, AttachBody) {
//   function SelectAll() { }

//   SelectAll.prototype.render = function (decorated) {
//     var $rendered = decorated.call(this);
//     var self = this;

//     var $selectAll = $(
//       '<button type="button">Select All</button>'
//     );

//     var checkOptionsCount = function()  {
//       var count = $('.select2-results__option').length;
//       $selectAll.prop('disabled', count > 25);
//     }
    
    
//     var $container = $('.select2-container');
//     $container.bind('keyup click', checkOptionsCount);

//     var $dropdown = $rendered.find('.select2-dropdown')

    
//     $dropdown.prepend($selectAll);

//     $selectAll.on('click', function (e) {
//       var $results = $rendered.find('.select2-results__option[aria-selected=false]');
      
//       // Get all results that aren't selected
//       $results.each(function () {
//         var $result = $(this);
        
//         // Get the data object for it
//         var data = $result.data('data');
        
//         // Trigger the select event
//         self.trigger('select', {
//           data: data
//         });
//       });
      
//       self.trigger('close');
//     });
    
//     return $rendered;
//   };

//   $("xyz").select2({
//     width: '100%',
//     placeholder: "Select Option(s)...",
//     dropdownAdapter: Utils.Decorate(
//       Utils.Decorate(
//         Dropdown,
//         AttachBody
//       ),
//       SelectAll
//     ),
//   });
// });


</script>



<script>



    $(function() {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                }
            }
        });

        var tabel = $('#tabledt').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'Semua']
            ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                    {
						text: 'Hapus',
						className: 'btn',
						action: function(e, dt, node, config) {
							bulk_delete();
						}
					},
                ]
            },

            'destroy': true,
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "serverSide": true,
            "ajax": {
                "url": "<?= base_url('verifikator/get_verifikator') ?>",
                "type": "POST"
            },


            'columns': [
                {
                    data: 'nip'
                },
                {
                    data: 'nama'
                },
                {
                    data: 'role'
                },
                {
                    data: 'lihat'
                },
                // {
                //     data: 'kode_skpd'
                // },
                // {
                //     data: 'kode_skpd_simpeg'
                // },
                // {
                //     data: 'id'
                // }
            ],

            columnDefs: [{}],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });
</script>

<script>
    function detailData(id) {
        $.ajax({
            url: "<?= base_url('verifikator/get_detail_verifikator') ?>",
            type: "POST",
            data: {
                id: id
            },
            dataType: "json",
            success: function(data) {

                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];

                var selectedValuesTest = data['sel'];
          
                if (sts == 1) {
                    $('#mdlDetail').modal('show');

                    $('#id').val(id)
                    $('#nip').val(dt['nip'])
                    $('#role_edit').val(dt['type_role'])
                    // $('#role').val(dt['type_role'])
                    // $('#role').select2().val(dt['type_role']).trigger('change')
                    // $('#role_edit').select2().val(dt['type_role']).trigger('change')
                    // $('#role_edit').val(dt['type_role']).change();
                    $('#kode_skpd').val(dt['kode_skpd_simpeg'])                   
                    $('#kode_skpd_edit').select2().val(selectedValuesTest).trigger('change')
                    // $("#btn_add").hide();
                    $("#btn_update").show();
                    $("#btn_del").show();

                    // console.log(dt['sel'])

                } else {

                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });

        $('#mdlDetail').on('hidden.bs.modal', function(e) {
            $(this)
            $('#id').val('')
            $('#nip').val('')
            $('#kode_skpd').val('').trigger("change");
            $('#role_edit').val('').trigger("change");
            // $('#fullname').val('')
            // $("input[name=gender][value='1']").prop('checked', true)
            // $('#group').val('').trigger("change");
            // $('#region_code').val('').trigger("change");
            // $("input[name=status][value='1']").prop('checked', true)
            // document.getElementById('btn_update').style.display = 'none';
            // document.getElementById('btn_add').style.display = 'none';
            // document.getElementById('btn_del').style.display = 'none';
        })

    }
</script>

<script>
    $('#btn_add').on('click', function() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        var username = $('#username_verifikator').val();
        var password = $('#password').val();
        var nip = $('#nip_verifikator').val();
        var nama = $('#nama_verifikator').val();
        var role = $('#role').val();
        var kode_skpd_add = $('#kode_skpd_add').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('verifikator/verifikator_add_action') ?>",
            dataType: "JSON",
            data: {
                username: username,
                password: password,
                nip: nip,
                nama: nama,
                role: role,
                kode_skpd_add: kode_skpd_add
            },
            success: function(data) {
                $('[name="nipbos"]').val("");
                $('[name="kode_skpd_add"]').val("");
                $('#mdlAdd').modal('show');
                $('#tabledt').DataTable().ajax.reload(null, false);
                // console.log(data);
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Berhasil',
                    type: 'success', 
                });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                // console.log(data);
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });
        return false;
    });
</script>

<script>
    $('#btn_update').on('click', function() {
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var id = $('#id').val();
        var nip = $('#nip').val();
        var role = $('#role_edit').val();
        var kode_skpd_add = $('#kode_skpd_edit').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('verifikator/verifikator_update_action') ?>",
            dataType: "JSON",
            data: {
                id: id,
                nip: nip,
                role: role,
                kode_skpd_add: kode_skpd_add
            },
            success: function(data) {
                $('[name="id"]').val("");
                $('[name="nip"]').val("");
                $('[name="role_edit"]').val("");
                $('[name="nominal"]').val("");
                $('#mdlDetail').modal('hide');
                $('#tabledt').DataTable().ajax.reload(null, false);
                // console.log(data);
                swalInit.fire(
                    'Berhasil',
                    'Berhasil Update Data :)',
                    'success'
                );
            },
            error: function(xhr, ajaxOptions, thrownError) {
                // console.log(data);
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });
        return false;
    });
</script>

<script>
    function hapusData(id) {

        var id = $('#id').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                // alert('proses ajax'); 
                $.ajax({
                    url: "<?= base_url('verifikator/verifikator_hapus_action') ?>",
                    type: "POST",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if (sts == 1) {
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    $('#mdlDetail').modal('hide');
                                    $('#tabledt').DataTable().ajax.reload(null, false);
                                }
                            });
                        } else {
                            swalInit.fire(
                                'Cancelled',
                                msg,
                                'error'
                            );
                        }

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#mdlDetail').modal('hide');
                                $('#tabledt').DataTable().ajax.reload(null, false);
                            }
                        });
                    }
                });
            }
            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     // swalInit.fire(
            //     //     'Cancelled',
            //     //     'Your imaginary file is safe :)',
            //     //     'error'
            //     // );
            // }
        });
    }
</script>

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?= $this->session->userdata("status"); ?>' == 'error') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'error'
        }).show();
    } else if ('<?= $this->session->userdata("status"); ?>' == 'success') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>



<script>
    $(function () {
        var fixurl = '<?=base_url('verifikator/cari_pns/');?>';
        $('#nipbos').select2({
            ajax: {
                url: fixurl,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        nipnya: params.term
                    }
                    
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            placeholder: "Masukkan NIP pada aplikasi",
            minimumInputLength: 5,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        
        function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }
            
            var markup = "<div>" + repo.name + "</div><div>" + repo.id + "</div>";
            
            return markup;
        }
        
        function formatRepoSelection (repo) {
            return repo.id;
        }
        
        $('#nipbos').change(function(){
            var nip = $('#nipbos').select2('data')[0].id;
            var name = $('#nipbos').select2('data')[0].name;
            var jabatan = $('#nipbos').select2('data')[0].status_pengajuan;
            $('#nip_verifikator').val(nip)
            $('#nama_verifikator').val(name)

            
        });

        
    });

   
</script>

<script>
	//check all
	$("#check-all").click(function() {
		$(".data-check").prop('checked', $(this).prop('checked'));
	});


	function bulk_delete() {
		var list_id = [];
		$(".data-check:checked").each(function() {
			list_id.push(this.value);
		});
		if (list_id.length > 0) {
			Swal.fire({
				title: 'Anda yakin ingin menghapus?',
				// text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						data: {
							id: list_id
						},
						url: "<?php echo site_url('verifikator/verifikator_hapus_soft_action') ?>",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
								Swal.fire({
									icon: 'success',
									title: 'Berhasil Hapus Data',
									showConfirmButton: false,
									timer: 1000
								})
								$('#tabledt').DataTable().ajax.reload(null, false);
							} else {
								// alert('Failed.');
								Swal.fire({
									icon: 'danger',
									title: 'Gagal Hapus Data',
									showConfirmButton: false,
									timer: 1000
								})
							}

						},
						error: function(jqXHR, textStatus, errorThrown) {
							// alert('Error deleting data');
							Swal.fire(
								'Gagal!',
								'Gagal Hapus Data',
								'error'
							)
						}
					});

				}
			})

			// if (confirm('Are you sure delete this ' + list_id.length + ' data?')) {

			// }
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Tidak Ada Data Dipilih',
				showConfirmButton: false,
				timer: 1000
			})
			// alert('no data selected');
		}
	}
</script>