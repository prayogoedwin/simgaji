<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold">Pengumuman</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data</h5>
                <div class="header-elements">
                    <a href="<?=base_url('pengumuman/tambah')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Pengumuman</th>
                            <th>Last Name</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        var tabel = $('#tabledt').DataTable({

            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: null, sortable : false, searceable : false },
                { data: 'pengumuman' },
                { data: 'waktu' },
                { data: 'aksi', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

    });
</script>

<script>
    function hapusData(id){
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        
        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax');
                
                $.ajax({
                    url: "<?=base_url('pengumuman/hapus_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>