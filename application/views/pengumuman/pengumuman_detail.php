<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold">Pengumuman</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <a href="<?=base_url('pengumuman')?>" class="breadcrumb-item">Pengumuman</a>
                    <span class="breadcrumb-item active">Detail</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Form layouts -->
        <div class="row">

            <div class="col-md-6">

                <!-- Vertical form -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="<?=base_url('pengumuman/detail_action')?>" method="post">
                            <input type="hidden" name="idne" value="<?=$detail->id;?>">
                            <div class="form-group">
                                <label>Pengumuman</label>
                             
                                <textarea rows="4" cols="4" id="summernote" class="form-control" placeholder="Isi pengumuman" name="pengumuman"><?=$detail->pengumuman;?></textarea>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Update <i class="icon-checkmark2 ml-1"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /vertical form -->

            </div>

            <div class="col-md-6 ">

                    <!-- Vertical form -->
                    <div class="card">
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Upload File</h5>
                            <div class="header-elements">
                                <div class="list-icons">
                                    <a class="list-icons-item" data-action="collapse"></a>
                                </div>
                            </div>
                        </div>

                        <div class="card-body">
                            <?php echo form_open_multipart('pengumuman/upload_file_inedit/'.decode_url($this->uri->segment(3)));?>
                            
                                <div class="form-group">
                                    <label>File PDF</label>

                                    <input type="file" name="image" id="image" accept="application/pdf">
                                </div>

                                <div class="text-right">
                                    <button type="submit" class="btn btn-primary">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                                </div>
                                <br/>
                            <?php echo form_close();?>

                            <table border="1" class="table">
                                <tr>
                                    <th>Nama File / URL</th>
                                    <th>Aksi</th>
                                </tr>
                                <?php 
                                // echo decode_url($this->uri->segment(3));
                                $map = directory_map('./assets/pengumuman/', FALSE, TRUE);
                                $file =  $map;

                                foreach($file as $f){ 
                                    
                                $pdf = substr($f, -3, 3); 
                                if($pdf == 'pdf'){   
                                ?>
                                <tr>
                                    <td>
                                    <?=base_url('assets/pengumuman/'.$f)?></td>
                                    <td><a href="<?=base_url('pengumuman/unlink_path_in_edit/'.$f.'/'.decode_url($this->uri->segment(3)))?>">Hapus</a></td>
                                   
                                </tr>
                                <?php } }?>
                            </table>
                        </div>
                    </div>
                    <!-- /vertical form -->

                </div>
        </div>
        <!-- /form layouts -->

    </div>
    <!-- /content area -->




<?php $this->load->view('template/footer');?>
</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script src="<?= base_url() ?>assets/vendor/summernote/summernote-bs4.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $('#summernote').summernote({
                    height: "300px",
                    toolbar: [
                        
                        [ 'insert', ['link', 'picture'] ],
                   
                    ],
                   
                    callbacks: {
                        onImageUpload: function(image) {
                            uploadImage(image[0]);
                        },
                        onMediaDelete: function(target) {
                            deleteImage(target[0].src);
                        }
                    }
                });

                function uploadImage(image) {
                    var data = new FormData(document.getElementById("msform"));
                    data.append("image", image);
                    $.ajax({
                        url: "<?php echo site_url('pengumuman/upload_image') ?>",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        type: "POST",
                        success: function(data) {
                            $('input[name=<?php echo $this->security->get_csrf_token_name(); ?>]').val(data.<?php echo $this->security->get_csrf_token_name(); ?>); //dataSrc untuk random request token char (wajib)
                            $('#summernote').summernote("insertImage", data.data);
                        },
                        error: function(data) {
                            console.log(data);
                        }
                    });
                }

                function deleteImage(src) {
                    $.ajax({
                        data: {
                            src: src
                        },
                        type: "POST",
                        url: "<?php echo site_url('pengumuman/delete_image') ?>",
                        cache: false,
                        success: function(response) {
                            console.log(response);
                        }
                    });
                }



            });
        </script>

</body>
</html>