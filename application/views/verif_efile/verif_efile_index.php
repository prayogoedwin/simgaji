<?php
$role = $this->session->userdata('role');
?>
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">

            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Verifikasi e - File</a>
                        <span class="breadcrumb-item active"> <?=$filter['nip']?></span>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <div class="header-elements">
                        <!-- <a href="<?= base_url('potongan/tambah') ?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                    </div>
                </div>

                <?php if(count($efilenya) > 0) :?>
                <div class="table-responsive">
                    <form action="<?=base_url('verif_efile/verif_action')?>" method="post">
                        <input type="hidden" name="rdr_id" id="rdr_id" value="<?=$this->uri->segment('3')?>">
                        <table class="table table-stripe" id="tabledt">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Dokumen</th>
                                    <th>Evidence</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($efilenya as $key => $val) :?>
                                    <tr>
                                        <td><?=$key+1?></td>
                                        <td><?=$val['nama_jenis']?></td>

                                        <?php if($val['efile'] != null) :?>
                                            <td>
                                                <a href="<?=$val['efile']?>" target="_blank">Lihat</a>
                                            </td>
                                        <?php else :?>
                                            <td>
                                                File not found
                                            </td>
                                        <?php endif; ?>

                                        <?php if($val['status_verif'] != null) :?>
                                            <td>Terverifikasi (<?=$val['status_verif']?>)</td>
                                        <?php else :?>
                                            <td>Belum terverifikasi</td>
                                        <?php endif; ?>

                                        <?php if($val['id'] != null && $val['status_verif'] == null) :?>
                                        <td>
                                            <input type="checkbox" name="iddoks[]" id="iddoks" value="<?=$val['id']?>">
                                        </td>
                                        <?php else :?>
                                        <td>
                                            -
                                        </td>
                                        <?php endif; ?>


                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                        <div class="float-right" style="margin: 8px;">
                            <input type="submit" class="btn btn-sm btn-info" value="Verifikasi" onclick="return confirm('anda yakin')">
                        </div>
                    </form>

                </div>
                <?php else :?>
                    <div class="text-center">
                        <h3>Tidak ada data</h3>
                    </div>
                <?php endif; ?>


            </div>
            <!-- /basic table -->

            <!-- START MODAL DETAIL -->
            <div id="mdlDetail" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h6 class="modal-title">Detail</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form action="" method="POST">
                            <div class="modal-body">
                                <input type="text" name="idne" id="idne">
                                <!-- <hr> -->
                                <div class="form-group row col-md-12">
                                    <label>Golongan</label>
                                    <input type="text" class="form-control" name="golongan" id="golongan" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Potongan</label>
                                    <input type="number" class="form-control" name="potongan" id="potongan" required autocomplete="off">
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button id="btn_update" type="submit" class="btn btn-md btn-success">Update</button>
                                <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                                <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MODAL DETAIL -->

        </div>
        <!-- /content area -->

        <?php $this->load->view('template/footer'); ?>

        <script type="text/javascript" src="<?= base_url('assets/plugin/datatables/') ?>datatables.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<script>
    function hapusData() {
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var id = $('#idne').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                // alert('proses ajax');

                $.ajax({
                    url: "<?= base_url('potongan/hapus_action') ?>",
                    type: "POST",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if (sts == 1) {
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                                'Cancelled',
                                msg,
                                'error'
                            );
                        }

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?= $this->session->userdata("status"); ?>' == 'error') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'error'
        }).show();
    } else if ('<?= $this->session->userdata("status"); ?>' == 'success') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'success'
        }).show();
    }
</script>

</body>
</html>
