<script>
    
    $(function () {
        $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
             lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
        }
        });

        var tabel = $('#tabledt').DataTable({
            'lengthMenu' : [
                        [ 10, 25, 50, 100,  -1 ],
                        [ '10', '25', '50', '100', 'Semua' ]
                ],   
            buttons: {            
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                ]
            },

            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            "serverSide": true,
            "ajax": {
                "url": "<?=site_url('standartunjangan/get_eselon') ?>",
                "type": "POST"
            },
           
            
            'columns': [
                { data: 'id', sortable : true, searceable : false },
                { data: 'kode' },
                { data: 'name' },
                { data: 'usia' },
                { data: 'tunjangan' }
            ],
            
            columnDefs: [{ }],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });
</script>

<script>
    function detailData(id){
        $.ajax({
            url: "<?=base_url('eselon/get_detail_eselon')?>",
            type: "POST",
            data: {
                id : id
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                
                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];
                
                if(sts == 1){
                    $('#mdlDetail').modal('show');

                    $('#id').val(id)
                    $('#kode').val(dt['kode'])
                    $('#name').val(dt['name'])
                    $('#usia').val(dt['usia'])
                    $('#tunjangan').val(dt['tunjangan'])

                } else {
                    
                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
                }
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });
    }
</script>

<script>
        $('#btn_update').on('click',function(){
            var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
         });

            var id=$('#id').val();
            var kode=$('#kode').val();
            var name=$('#name').val();
            var usia=$('#usia').val();
            var tunjangan=$('#tunjangan').val();
            
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('eselon/eselon_update_action')?>",
                dataType : "JSON",
                data : {id:id , kode:kode, name:name, usia:usia, tunjangan:tunjangan},
                success: function(data){
                    $('[name="id"]').val("");
                    $('[name="kode"]').val("");
                    $('[name="name"]').val("");
                    $('[name="usia"]').val("");
                    $('[name="tunjangan"]').val("");
                    $('#mdlDetail').modal('hide');
                    $('#tabledt').DataTable().ajax.reload(null, false);
                        swalInit.fire(
                        'Berhasil',
                        'Berhasil Update Data :)',
                        'success'
                        );
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#tabledt').DataTable().ajax.reload(null, false);
                            }
                        });
                }
            });
            return false;
        });
</script>

<script>
    function hapusData(id){

        var id = $('#id').val();
        
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        
        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax'); 
                $.ajax({
                    url: "<?=base_url('eselon/eselon_hapus_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    $('#tabledt').DataTable().ajax.reload(null, false);
                                }
                            });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#tabledt').DataTable().ajax.reload(null, false);
                            }
                        });
                    }
                });
            }
            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     // swalInit.fire(
            //     //     'Cancelled',
            //     //     'Your imaginary file is safe :)',
            //     //     'error'
            //     // );
            // }
        });
    }
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>