<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" type="text/css">
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?= $title ?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                        <?= $breadcrumb ?>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

        

            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title"><?= $title ?></h5>
                    <div class="header-elements">
                        <!-- <a href="<?= base_url('pengumuman/tambah') ?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                        <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#modal_default">Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-stripe" id="example">
                        <thead>
                            <tr>
                                <th>Label</th>
                                <th>Order</th>
                                <th width="10%">Visible</th>
                                <th width="10%">Visible P3K</th>
                                <th width="10%">Editable</th>
                                <th width="10%">Editable P3K</th>
                                <th width="10%">Editable Verifikator</th>
                                <th width="10%">Is Required</th>
                                <!-- <th>Visible</th> -->
                                <!-- <th class="text-center">#</th> -->
                            </tr>
                        </thead>
                        <tbody style="cursor: all-scroll;">

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
        <!-- /content area -->




        <?php $this->load->view('template/footer'); ?>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js" integrity="sha256-eGE6blurk5sHj+rmkfsGYeKyZx3M4bG+ZlFyA7Kns7E=" crossorigin="anonymous"></script>
    </div>
    <!-- /main content -->


   

    <script>
        $(document).ready(function() {

            load_data();

            function load_data() {
                $.ajax({
                    url: "<?= base_url('kode/get_kode') ?>",
                    method: "POST",
                    data: {
                        action: 'fetch_data'
                    },
                    dataType: 'json',
                    success: function(data) {
                        var html = '';
                        var visible = '';
                        var visiblep3k = '';
                        var editable = '';
                        var editablep3k = '';
                        var editable_v = '';
                        var requireds = '';
                        for (var count = 0; count < data.length; count++) {

                            if (data[count].bool_id == 2) {
                                visible = '<button onclick="return changeCode('+ data[count].id +',2)" class="btn btn-success" id="yes">Ya</button>'
                            } else {
                                visible = '<button onclick="return changeCode('+ data[count].id +',1)" class="btn btn-danger" id="no">Tidak</button>'
                            }

                            if (data[count].bool_id_p3k == 2) {
                                visiblep3k = '<button onclick="return changeCodeP('+ data[count].id +',2)" class="btn btn-success" id="yes">Ya</button>'
                            } else {
                                visiblep3k = '<button onclick="return changeCodeP('+ data[count].id +',1)" class="btn btn-danger" id="no">Tidak</button>'
                            }

                            if (data[count].status == 2) {
                                editable = '<button onclick="return changeCodeE('+ data[count].id +',2)" class="btn btn-success" id="yes">Ya</button>'
                            } else {
                                editable = '<button onclick="return changeCodeE('+ data[count].id +',1)" class="btn btn-danger" id="no">Tidak</button>'
                            }

                            if (data[count].status_p3k == 2) {
                                editablep3k = '<button onclick="return changeCodeEP('+ data[count].id +',2)" class="btn btn-success" id="yes">Ya</button>'
                            } else {
                                editablep3k = '<button onclick="return changeCodeEP('+ data[count].id +',1)" class="btn btn-danger" id="no">Tidak</button>'
                            }

                            if (data[count].status_verifikator == 2) {
                                editable_v = '<button onclick="return changeCodeE('+ data[count].id +',2)" class="btn btn-success" id="yes">Ya</button>'
                            } else {
                                editable_v = '<button onclick="return changeCodeE('+ data[count].id +',1)" class="btn btn-danger" id="no">Tidak</button>'
                            }

                            if (data[count].status_required == 1) {
                                requireds = '<button onclick="return changeCodeReq('+ data[count].id +',1)" class="btn btn-success" id="yes">Ya</button>'
                            } else {
                                requireds = '<button onclick="return changeCodeReq('+ data[count].id +',0)" class="btn btn-danger" id="no">Tidak</button>'
                            }

                            html += '<tr id="' + data[count].id + '">';
                            html += '<td>' + data[count].name + '</td>';
                            html += '<td>' + data[count].sort + '</td>';
                            html += '<td>' + visible + '</td>';
                            html += '<td>' + visiblep3k + '</td>';
                            html += '<td>' + editable + '</td>';
                            html += '<td>' + editablep3k + '</td>';
                            html += '<td>' + editable_v + '</td>';
                            html += '<td>' + requireds + '</td>';
                            // html += '<td>' + data[count].bool_id + '</td>';
                            html += '</tr>';
                        }
                        $('tbody').html(html);
                    }
                })
            }

            $('tbody').sortable({
                placeholder: "ui-state-highlight",
                update: function(event, ui) {
                    var page_id_array = new Array();
                    $('tbody tr').each(function() {
                        page_id_array.push($(this).attr('id'));
                    });

                    $.ajax({
                        url: "<?= base_url('kode/get_kode') ?>",
                        method: "POST",
                        data: {
                            page_id_array: page_id_array,
                            action: 'update'
                        },

                        success: function() {
                            load_data();
                            // alert(data);
                        }

                    })

                }
            });

        });
    </script>

<script>

        function changeCode(id, val) {

            $res = confirm('Anda ingin merubah status ?')

            if($res){

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url('kode/kode_change') ?>",
                    dataType: "JSON",
                    data: {
                        id: id,
                        val: val
                    },
                    success: function(data) {
                        iziToast.success({
                            title: 'success',
                            position: 'topCenter',
                            message: 'Berhasil Update data'
                        });
                        


                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        iziToast.error({
                            title: 'error',
                            position: 'topCenter',
                            message: 'Gagal Update data'
                        });
                    }
                });

                window.location.reload();
               
            } else {
                return FALSE;
                // Do nothing!
                
            }


        };
    </script>

<script>

function changeCodeP(id, val) {

    $res = confirm('Anda ingin merubah status ?')

    if($res){

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kode/kode_change_p3k') ?>",
            dataType: "JSON",
            data: {
                id: id,
                val: val
            },
            success: function(data) {
                iziToast.success({
                    title: 'success',
                    position: 'topCenter',
                    message: 'Berhasil Update data'
                });
                


            },
            error: function(xhr, ajaxOptions, thrownError) {
                iziToast.error({
                    title: 'error',
                    position: 'topCenter',
                    message: 'Gagal Update data'
                });
            }
        });

        window.location.reload();
       
    } else {
        return FALSE;
        // Do nothing!
        
    }


};
</script>

<script>

function changeCodeE(id, val) {

    $res = confirm('Anda ingin merubah status ?')

    if($res){

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kode/kode_editable') ?>",
            dataType: "JSON",
            data: {
                id: id,
                val: val
            },
            success: function(data) {
                iziToast.success({
                    title: 'success',
                    position: 'topCenter',
                    message: 'Berhasil Update data'
                });
                


            },
            error: function(xhr, ajaxOptions, thrownError) {
                iziToast.error({
                    title: 'error',
                    position: 'topCenter',
                    message: 'Gagal Update data'
                });
            }
        });

        window.location.reload();
       
    } else {
        return FALSE;
        // Do nothing!
        
    }


};
</script>

<script>

function changeCodeEP(id, val) {

    $res = confirm('Anda ingin merubah status ?')

    if($res){

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kode/kode_editable_p3k') ?>",
            dataType: "JSON",
            data: {
                id: id,
                val: val
            },
            success: function(data) {
                iziToast.success({
                    title: 'success',
                    position: 'topCenter',
                    message: 'Berhasil Update data'
                });
                


            },
            error: function(xhr, ajaxOptions, thrownError) {
                iziToast.error({
                    title: 'error',
                    position: 'topCenter',
                    message: 'Gagal Update data'
                });
            }
        });

        window.location.reload();
       
    } else {
        return FALSE;
        // Do nothing!
        
    }


};
</script>

<script>

function changeCodeReq(id, val) {

    $res = confirm('Anda ingin merubah status ?')

    if($res){

        $.ajax({
            type: "POST",
            url: "<?php echo base_url('kode/kode_required') ?>",
            dataType: "JSON",
            data: {
                id: id,
                val: val
            },
            success: function(data) {
                iziToast.success({
                    title: 'success',
                    position: 'topCenter',
                    message: 'Berhasil Update data'
                });
                


            },
            error: function(xhr, ajaxOptions, thrownError) {
                iziToast.error({
                    title: 'error',
                    position: 'topCenter',
                    message: 'Gagal Update data'
                });
            }
        });

        window.location.reload();
       
    } else {
        return FALSE;
        // Do nothing!
        
    }


};
</script>








    </body>

    </html>