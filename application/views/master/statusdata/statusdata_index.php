<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">

            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                        <span class="breadcrumb-item active">Lokasi</span>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>
        </div>
        <!-- /page header -->

        <?php echo form_open('statusdata/update_bulk')?>
        <!-- Content area -->
        <div class="content">

            <!-- Form inputs -->

            <div class=" ">
                <div class="">
                    <!-- <form action="#"> -->
                   
                        <div class="form-group row">


                            <div class="col-lg-2">
                                <?php echo form_dropdown('status_gaji', statusdata_array('Status Gaji'), '', ' id="status_gaji" class="form-control custom-select select2"   required'); ?>
                            </div>

                            <div class="col-lg-2">
                                <?php echo form_dropdown('status_tpp', statusdata_array('Status TPP'), '', ' id="status_tpp" class="form-control custom-select select2"   required'); ?>

                            </div>

                            <div class="col-lg-2">
                                <?php echo form_dropdown('gaji_verif', statusverif_array('Status Gaji Verifikator'), '', ' id="gaji_verif" class="form-control custom-select select2"   required'); ?>
                            </div>

                            <div class="col-lg-2">
                                <?php echo form_dropdown('tpp_verif', statusverif_array('Status TPP Verifikator'), '', ' id="tpp_verif" class="form-control custom-select select2"   required'); ?>

                            </div>

                            <div class="col-lg-3">
                                <div class="text-left">
                                    <button type="submit" id="btn-filter" class="btn btn-primary pull-left"> <i class="fa fa-save"></i> Set Status Data </button>
                                </div>

                            </div>

                        </div>

                    

                </div>

            </div>
            <!-- /form inputs -->

            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <!-- <div class="header-elements">
                        <a href="<?= base_url('statusdata') ?>" class="btn btn-labeled btn-labeled-right bg-primary">Simpan <b><i class="icon-file-plus2"></i></b></a>
                    </div> -->
                </div>

                <div class="table-responsive">
                    <table class="table table-stripe" id="tabledt">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th width="2%" class="text-center"><input type="checkbox" id="select_all" name="data[]" value="" /></th>

                                <th>Nama</th>
                                <th>Status Gaji</th>
                                <th>Status TPP</th>
                                <th>Status Gaji Verifikator</th>
                                <th>Status TPP Verifikator</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

            <?php echo form_close(); ?>
            
            <!-- START MODAL DETAIL -->
            <div id="mdlDetail" class="modal fade" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h6 class="modal-title">Data Lokasi</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form action="<?= base_url('lokasi/update_action') ?>" method="POST">
                            <div class="modal-body">
                                <input type="hidden" name="idlokasi" id="idlokasi">
                                <!-- <hr> -->
                                <div class="form-group row col-md-12">
                                    <label>Kode</label>
                                    <input type="text" class="form-control" name="kode" id="kode" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="name" id="name" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12 dropinduk">
                                    <label>Induk</label>
                                    <select name="is_induk" id="is_induk" class="form-control" required>
                                        <option value="1">Tidak</option>
                                        <option value="2">Ya</option>
                                    </select>
                                </div>
                            </div>


                            <div class="modal-footer">
                                <button type="submit" class="btn btn-md btn-success">Update</button>
                                <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                                <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MODAL DETAIL -->

        </div>
        <!-- /content area -->





        <?php $this->load->view('template/footer'); ?>

        <script type="text/javascript" src="<?= base_url('assets/plugin/datatables/') ?>datatables.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function() {
        var tabel = $('#tabledt').DataTable({

            'destroy': true,
            'paging': false,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'data': <?= json_encode($datatable); ?>,
            'columns': [{
                    data: null,
                    sortable: false,
                    searceable: false
                },
                {
                    data: 'id'
                },

                {
                    data: 'nama'
                },
                {
                    data: 'status_gaji'
                },
                {
                    data: 'status_tpp'
                },
                {
                    data: 'status_gaji_verifikator'
                },
                {
                    data: 'status_tpp_verifikator'
                },

                // { data: 'aksi', sortable : false, searceable : false  }
            ],
            columnDefs: [{}],
        });

        tabel.on('order.dt search.dt', function() {
            tabel.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    });
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('#select_all').click(function(event) {
        if(this.checked) {
            $('.check_data').each(function() {
                this.checked = true;  
            });
        }
		else{
            $('.check_data').each(function() {
                this.checked = false; 
            });         
        }
    });
    
});
</script>

<script>
    function hapusData() {
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var id = $('#idlokasi').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                // alert('proses ajax');

                $.ajax({
                    url: "<?= base_url('lokasi/hapus_action') ?>",
                    type: "POST",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if (sts == 1) {
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                                'Cancelled',
                                msg,
                                'error'
                            );
                        }

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
    function detailData(id) {
        // $('#mdlDetail').modal('show');

        $.ajax({
            url: "<?= base_url('lokasi/getDetailLokasi') ?>",
            type: "POST",
            data: {
                id: id
            },
            dataType: "json",
            success: function(data) {
                console.log(data);

                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];

                if (sts == 1) {
                    $('#mdlDetail').modal('show');

                    $('#idlokasi').val(id)
                    $('#kode').val(dt['kode'])

                    $('#name').val(dt['name'])

                    if (dt['bool_id'] == 2) {
                        //selected induk
                        $("div.dropinduk select").val("2");
                    } else {
                        //selected no induk
                        $("div.dropinduk select").val("1");
                    }
                    // document.getElementById("namalokasibody").innerHTML = dt['name'];
                    // document.getElementById("radiusbody").innerHTML = 'Radius : ' + dt['radius'];

                } else {

                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            // alert('Notification is closed.');
                            location.reload();
                        }
                    });
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        // alert('Notification is closed.');
                        location.reload();
                    }
                });
            }
        });
    }
</script>

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?= $this->session->userdata("status"); ?>' == 'error') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'error'
        }).show();
    } else if ('<?= $this->session->userdata("status"); ?>' == 'success') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>

</html>