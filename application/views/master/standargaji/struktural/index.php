<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                    <?=$breadcrumb?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><?=$title?></h5>
                <div class="header-elements">
                    <!-- <a href="<?=base_url('pengumuman/tambah')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                    <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#modal_default">Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead>
                        <tr >
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Nominal</th>
                            <!-- <th class="text-center">#</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
   


</div>
<!-- /main content -->


<!-- START MODAL DETAIL -->
<div id="mdlDetail" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Data Lokasi</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="idlokasi" id="idlokasi">
                            <!-- <hr> -->
                            <div class="form-group row col-md-12">
                                <label>Nama</label>
                                <input type="hidden" readonly class="form-control" name="id" id="id" required autocomplete="off">
                                <input type="text" readonly class="form-control" name="name" id="name" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Kelas</label>
                                <input type="text" readonly class="form-control" name="kelas" id="kelas" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12 dropinduk">
                                <label>Nominal</label>
                                <input type="text" class="form-control" name="nominal" id="nominal" required autocomplete="off">
                            </div>
                        </div>
                    

                        <div class="modal-footer">
                            <button type="submit" id="btn_update" class="btn btn-md btn-success">Update</button>
                            <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->


</div>
<!-- /page content -->

<?php $this->load->view('master/standargaji/struktural/js');?>






      

</body>
</html>