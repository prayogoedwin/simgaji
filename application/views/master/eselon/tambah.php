<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold">Pengumuman</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <a href="<?=base_url('pengumuman')?>" class="breadcrumb-item">Pengumuman</a>
                    <span class="breadcrumb-item active">Tambah</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Form layouts -->
        <div class="row">

            <div class="col-md-6 offset-md-3">

                <!-- Vertical form -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="<?=base_url('pengumuman/tambah_action')?>" method="post">
                            <div class="form-group">
                                <label>Pengumuman</label>
                                <textarea rows="4" cols="4" class="form-control" placeholder="Isi pengumuman" name="pengumuman"></textarea>
                            </div>

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /vertical form -->

            </div>
        </div>
        <!-- /form layouts -->

    </div>
    <!-- /content area -->




<?php $this->load->view('template/footer');?>
</div>
<!-- /main content -->

</div>
<!-- /page content -->

</body>
</html>