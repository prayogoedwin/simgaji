<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>SimGaji - Badan Kepegawaian Daerah Provinsi Jawa Tengah</title>

	<link rel="icon" href="<?=base_url();?>assets/images/icons/logo_prov_jateng_uPd_icon.ico" type="image/x-icon"/>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets')?>/global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/layout1/ltr/default/full/')?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/layout1/ltr/default/full/')?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/layout1/ltr/default/full/')?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/layout1/ltr/default/full/')?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?=base_url('assets/layout1/ltr/default/full/')?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?=base_url('assets')?>/global_assets/js/main/jquery.min.js"></script>
	<script src="<?=base_url('assets')?>/global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?=base_url('assets')?>/global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?=base_url('assets/layout1/ltr/default/full/')?>assets/js/app.js"></script>
	<!-- /theme JS files -->

</head>

<body>

	<!-- Main navbar -->
	<!-- <div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="index.html" class="d-inline-block">
				<img src="<?=base_url('assets')?>/global_assets/images/logo_light.png" alt="">
			</a>
		</div>

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-tree5"></i>
			</button>
		</div>

		
	</div> -->
	<!-- /main navbar -->


	<!-- Page content -->
	<div class="page-content">

		<!-- Main content -->
		<div class="content-wrapper">

			<!-- Content area -->
			<div class="content d-flex justify-content-center align-items-center" style='background-image:url("<?=base_url()?>assets/statis/background_login.jpeg")'>

				<!-- Login form -->
				<form class="login-form" action="<?=base_url('user/login_action')?>" method="post">
					<div class="card mb-0">
						<div class="card-body">
								

							<div class="text-center mb-3">
								<!-- <i class="icon-reading icon-2x text-slate-300 border-slate-300 border-3 rounded-round p-3 mb-3 mt-1"></i> -->
								<img src="<?=base_url();?>assets/images/logo_prov_jateng.png" alt="AVATAR" width="100px" style="margin-bottom: 20px;">
								<h5 class="mb-0">- SIMGAJI -</h5>
								<span class="d-block text-muted">Badan Kepegawaian Daerah</span>
								<span class="d-block text-muted">Provinsi Jawa Tengah</span>

								<?php
                                $info = $this->session->flashdata('type');
                                $pesan = $this->session->flashdata('message');
                                
                                 ?>
                                
                                   <?php echo form_open('portal/auth', 'class=" m-t-10"');   
                                  
                                  if( $info == 'danger'){ ?>
                                      
                                      <br/><span style="color:red; text-align:center"><?=$pesan?> </span><br/>
                                     
                                  <?php } ?>
                                  
                                
                                  
                                  <?php if( $info == 'success'){ ?>
                                      
									<br/><span style="color:green; text-align:center"><?=$pesan?> </span><br/>
                                     
                                  <?php } ?>


							</div>

							
							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="text" class="form-control" placeholder="NIP" name="nip" onkeypress="return AvoidSpace(event);" onblur="this.value=removeSpaces(this.value);">
								<div class="form-control-feedback">
									<i class="icon-user text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback form-group-feedback-left">
								<input type="password" class="form-control" placeholder="Password" name="password">
								<div class="form-control-feedback">
									<i class="icon-lock2 text-muted"></i>
								</div>
							</div>

							<div class="form-group form-group-feedback">
								<div class="row" >
								<div class="col-3">
								<input type="text" class="form-control text-center"  readonly name="satu" value="<?php echo (rand(1,9)); ?>">
								</div>

								<div class="col-1" style="margin-left:-30px">
								<div class="form-control-feedback" >
									<!-- <i class="icon-plus3 text-muted"></i> -->
									<span class="text-muted">+</span>
								</div>
								</div>

								<div class="col-3">
								<input type="text" class="form-control text-center"  readonly name="dua" value="<?php echo (rand(1,9)); ?>">
								</div>

								<div class="col-1" style="margin-left:-30px">
								<div class="form-control-feedback" >
									
									<!-- <i class="icon-plus3 text-muted"></i> -->
									<span class="text-muted">=</span>
								</div>
								</div>

								<div class="col-4">
								<input type="number" class="form-control" style="width:136px" name="jawaban">
								</div>

								</div>
							</div>

							

							

							<!-- <div class="form-group row m-t-30">
                                <div class="col-sm-12">
                                    <div class="custom-control custom-checkbox">
                                    <input id="satu" style="width:30px; text-align:center"   readonly name="satu" value="<?php echo (rand(1,9)); ?>" > <span style="color:#fff">+</span> 
                                    <input style="width:30px; text-align:center" id="dua"  readonly name="dua" value="<?php echo (rand(1,9)); ?>" > <span style="color:#fff">=</span>
                                    <input style= "width:60px; text-align:center" name="jawaban"  type="number" placeholder="Hasil" required >
                                    </div>
                                </div>
                                
                            </div> -->

							<div class="form-group">
								<button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 ml-2"></i></button>
							</div>

						</div>
					</div>
				</form>
				<!-- /login form -->

			</div>
			<!-- /content area -->


			<!-- Footer -->
			<!-- <div class="navbar navbar-expand-lg navbar-light">
				<div class="text-center d-lg-none w-100">
					<button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
						<i class="icon-unfold mr-2"></i>
						Footer
					</button>
				</div>

				<div class="navbar-collapse collapse" id="navbar-footer">
					<span class="navbar-text">
						&copy; 2021 - <a href="#">SimGaji</a>
					</span>

					<ul class="navbar-nav ml-lg-auto">
						<li class="nav-item"><a href="https://themeforest.net/item/limitless-responsive-web-application-kit/13080328?ref=kopyov" class="navbar-nav-link font-weight-semibold"><span class="text-pink-400"><i class="icon-chrome mr-2"></i> Badan Kepegawaian Daerah Provinsi Jawa Tengah</span></a></li>
					</ul>
				</div>
			</div> -->
			<!-- /footer -->

		</div>
		<!-- /main content -->

	</div>
	<!-- /page content -->

</body>

<script>
	/* Not Allow Spcace Type in textbox */
function AvoidSpace(event) {
    var k = event ? event.which : window.event.keyCode;
    if (k == 32) return false;
}

/* Remove Blank Space Automatically Before, After & middle of String */

function removeSpaces(string) {
 return string.split(' ').join('');
}
</script>
</html>
