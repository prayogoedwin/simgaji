<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">

            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="<?=base_url('dashboard')?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                        <span class="breadcrumb-item active">Status Data SKPD</span>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
            </div>
        </div>
        <!-- /page header -->

        <!-- Content area -->
        <div class="content">
            <form action="<?=base_url('statusdata_skpd/update_action')?>" method="post">

                <div class="form-group col-lg-12 row">

                    <div class="col-lg-1">
                        <label for="">Status Gaji</label>
                    </div>


                    <div class="col-lg-2">
                        <select name="status_gaji" id="status_gaji" class="form-control">
                            <option value="">Pilih</option>
                            <option value="1" <?php if($status_skpd->status_gaji == 1){echo "selected";}?> >Kalkulasi</option>
                            <option value="3" <?php if($status_skpd->status_gaji == 3){echo "selected";}?> >Fixed</option>
                            <option value="4" <?php if($status_skpd->status_gaji == 4){echo "selected";}?> >Cetak</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <button type="submit" id="btn-filter" class="btn btn-success align-bottom" onclick="return confirm('Apakah anda yakin?');"><i class="icon icon-floppy-disk"></i> Simpan</button>
                    </div>

                </div>
            </form>

            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <div class="header-elements">
                        <!-- <a href="<?=base_url('potongan/tambah')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table table-stripe" id="tabledt">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>SKPD</th>
                                <th>Status Gaji</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td><?=$status_skpd->name?></td>
                                <td><?=textStatusDataStatusGaji($status_skpd->status_gaji)?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <!-- /content area -->


        <?php $this->load->view('template/footer'); ?>

        <script type="text/javascript" src="<?= base_url('assets/plugin/datatables/') ?>datatables.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<script>

    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>

</html>
