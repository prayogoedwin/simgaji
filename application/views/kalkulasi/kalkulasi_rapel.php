<!-- Page content -->
<div class="page-content">

<style>
    .kontensi {display:none;}
.preload { width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;}
</style>


<!-- Main content -->
<div class="content-wrapper">




    <!-- Page header -->
    <div class="page-header page-header-light">
        <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

        </div> -->

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                    <?=$breadcrumb?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->





    <!-- Content area -->
    <div class="content">

     <!-- Form inputs -->

     <div class=" ">



<div class="">

<div class="preload"><img width="80%" src="https://i.gifer.com/ZKZg.gif"></div>

    <!-- <form action="#"> -->
   

    <div class="form-group row kontensi">

    <?php
          $attributes = array('class' => 'form-horizontal');
          echo form_open('kalkulasi/action', $attributes); ?>
    <div class="col-lg-6">

        <div class="form-group">
            Jenis Kalkulasi
            <?php
             $jenis_rapel = array(
                ''  => 'Pilih Jenis Kalkulasi',
                'GAJI_BULANAN_RAPEL_PPPK' => 'GAJI_BULANAN_RAPEL_PPPK',
                'GAJI_RAPEL_PPPK'   => 'GAJI_RAPEL_PPPK'
             );
            echo form_dropdown('jenis',$jenis_rapel, '', ' id="jenis" class="form-control custom-select select2"   required'); ?>
        </div>

        <div class="form-group">
           <div class="row">
           <div class="col-md-6">Periode Gaji Bulanan: <select class="form-control select2" style="width: 100%;" required name="bulan_id" id="bulan_id">

                            <?php for ($j = 1; $j <= 12; $j++) {
                                $month_period = getDataPeriode()->bulan;
                                $year_period = getDataPeriode()->tahun;
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month_period == $bln) {
                                    $selected = "selected";
                                }

                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select></div>
           <div class="col-md-6">Tahun: <input name="tahun" class="form-control" value="<?=$year_period?>"></div>
           </div>
        </div>


        <div class="form-group">
           <div class="row">
           <div class="col-md-6">Periode Bulan Rapel: <select class="form-control select2" multiple style="width: 100%;" required name="bulan_id2[]" id="bulan_id2">

                            <?php for ($j = 1; $j <= 12; $j++) {
                                $month_period = getDataPeriode()->bulan;
                                $year_period = getDataPeriode()->tahun;
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month_period == $bln) {
                                    $selected = "selected";
                                }

                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select></div>
           <div class="col-md-6">Tahun: <input name="tahun" class="form-control" value="<?=$year_period?>"></div>
           </div>
        </div>

        <div class="form-group" >
        Lokasi Start
        <?php echo form_dropdown('lokasi_start', opd_gaji_v3_array('Lokasi Start', 'id', '2'), '', ' id="lokasi_start" class="form-control custom-select select2"   required'); ?>

        </div>

        <div class="form-group" >
        Lokasi End
        <?php echo form_dropdown('lokasi_end', opd_gaji_v3_array('Lokasi End', 'id', '2'), '', ' id="lokasi_end" class="form-control custom-select select2"  '); ?>

        </div>

        <div class="form-group">
        <div class="text-right">
        <!-- <div class="spinner-border " role="status" style="margin-top:20px">
        <span class="sr-only">Loading...</span>
        </div> -->
        <button type="submit" id="btn-filter" class="btn btn-primary pull-right"> <i class="fa fa-save"></i> KALKULASI SEKARANG </button>
       
        </div>
        

        </div>

    </div>
    <?php echo form_close(); ?>
    

    </div>


    <br/>
    <hr/>
    <br/>


    <div class="form-group row kontensi">

    <?php
          $attributes = array('class' => 'form-horizontal');
          echo form_open('kalkulasi/gaji_rapel_generate_table', $attributes); ?>
    <div class="col-lg-6">
        <h2>Generate Tabel</h2>
        <div class="form-group">
           Generate
            <input name="jenis" class="form-control" value="GENRATE_TABEL_GAJI_RAPEL" readonly>
           
            
        </div>

        <div class="form-group">
           <div class="row">
           <div class="col-md-6">Data Gaji Acuan: <select class="form-control select2" multiple style="width: 100%;" required name="bulan_ids[]" id="bulan_ids">

                            <?php for ($j = 1; $j <= 12; $j++) {
                                $month_period = getDataPeriode()->bulan;
                                $year_period = getDataPeriode()->tahun;
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month_period == $bln) {
                                    $selected = "selected";
                                }

                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select>
                  
                    </div>
           <div class="col-md-6">Tahun: <input name="tahun" class="form-control" value="<?=$year_period?>"></div>
           </div>
           <span>Jangan generate lebih dari satu kecuali kamu sabar.</span> 
        </div>


        


        <div class="form-group">
        <div class="text-right">
        <!-- <div class="spinner-border " role="status" style="margin-top:20px">
        <span class="sr-only">Loading...</span>
        </div> -->
        <button type="submit"  class="btn btn-primary pull-right"> <i class="fa fa-file"></i> GENERATE SEKARANG </button>
       
        </div>
        

        </div>

    </div>
    <?php echo form_close(); ?>
    

    </div>


    <br/>
    <hr/>
    <br/>


    <div class="form-group row kontensi">

    <?php
          $attributes = array('class' => 'form-horizontal');
          echo form_open('cetakp3k/gaji_action', $attributes); ?>
    <div class="col-lg-6">
        <h2>Cetak </h2>
        <div class="form-group">
            Jenis Cetak
            <?php echo form_dropdown('jenis', jenis_cetak_rapelgaji('Pilih Jenis Cetak'), '', ' id="jenis_cetak" class="form-control custom-select select2"   required'); ?>
        </div> 

        <div class="form-group">
           <div class="row">
           <div class="col-md-6">Periode Rapel: <select class="form-control select2" multiple style="width: 100%;" required name="bulan_id[]" id="bulan_idz">

                            <?php for ($j = 1; $j <= 12; $j++) {
                                $month_period = getDataPeriode()->bulan;
                                $year_period = getDataPeriode()->tahun;
                                $selected = '';
                                $bln = sprintf("%02d", $j);
                                if ($month_period == $bln) {
                                    $selected = "selected";
                                }

                                echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                            } ?>

                        </select>
                  
                    </div>
           <div class="col-md-6">Tahun: <input name="tahun" class="form-control" value="<?=$year_period?>"></div>
           </div>
           
        </div>

        <div class="form-group" >
        Lokasi Start
        <?php echo form_dropdown('lokasi_start', opd_gaji_v3_array('Lokasi Start', 'id', '2'), '', ' id="lokasi_start2" class="form-control custom-select select2"   required'); ?>

        </div>

        <div class="form-group" >
        Lokasi End
        <?php echo form_dropdown('lokasi_end', opd_gaji_v3_array('Lokasi End', 'id', '2'), '', ' id="lokasi_end2" class="form-control custom-select select2"  '); ?>

        </div>


        


        <div class="form-group">
        <div class="text-right">
        <!-- <div class="spinner-border " role="status" style="margin-top:20px">
        <span class="sr-only">Loading...</span>
        </div> -->
        <button type="submit"  class="btn btn-primary pull-right"> <i class="fa fa-file"></i> CETAK SEKARANG </button>
       
        </div>
        

        </div>

    </div>
    <?php echo form_close(); ?>
    

    </div>

  

</div>

</div>
<!-- /form inputs -->



    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>


    <script>

        $('#lokasi_start').on('change', function() {
            var lokasi_start = $('#lokasi_start').val();
		    if(lokasi_start == '34000000'){
                $('#lokasi_end').prop('required', true);
            }
        });



    </script>



</div>
<!-- /main content -->




</div>
<!-- /page content -->

<?php $this->load->view('kalkulasi/js');?>
<script>
  $(function() {
    $(".preload").fadeOut(2000, function() {
    $(".kontensi").fadeIn(500);
    });
});

    </script>










</body>
</html>
