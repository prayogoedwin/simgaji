<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>SimGaji - Badan Kepegawaian Daerah Provinsi Jawa Tengah</title>

	<link rel="icon" href="<?= base_url(); ?>assets/images/icons/logo_prov_jateng_uPd_icon.ico" type="image/x-icon" />

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/') ?>global_assets/css/icons/icomoon/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/layout1/ltr/default/full/') ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/layout1/ltr/default/full/') ?>assets/css/bootstrap_limitless.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/layout1/ltr/default/full/') ?>assets/css/layout.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/layout1/ltr/default/full/') ?>assets/css/components.min.css" rel="stylesheet" type="text/css">
	<link href="<?= base_url('assets/layout1/ltr/default/full/') ?>assets/css/colors.min.css" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script src="<?= base_url('assets/') ?>global_assets/js/main/jquery.min.js"></script>
	<script src="<?= base_url('assets/') ?>global_assets/js/main/bootstrap.bundle.min.js"></script>
	<script src="<?= base_url('assets/') ?>global_assets/js/plugins/loaders/blockui.min.js"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script src="<?= base_url('assets/layout1/ltr/default/full/') ?>assets/js/app.js"></script>
	<!-- /theme JS files -->

	<!-- <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/jgrowl.min.js"></script> -->
	<script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/noty.min.js"></script>

	<script src="<?= base_url('assets/') ?>global_assets/js/demo_pages/datatables_basic.js"></script>

	<!-- <link href="<?= base_url('assets/plugin/select2new/') ?>select2.min.css" rel="stylesheet" />
	<script src="<?= base_url('assets/plugin/select2new/') ?>select2.min.js"></script> -->

	<script src="<?= base_url('assets/') ?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<!-- <script src="<?= base_url('assets/') ?>global_assets/js/demo_pages/form_select2.js"></script> -->




<style>
	.select2-container .select2-selection--single .select2-selection__rendered {
      /* background-color: #E9ECEF; */
      height: 36px !important;
    }
	

    .select2-selection__rendered {
      line-height: 28px !important;
      font-size: 14px;
      /* color: #495057; */
    }

    .select2-container .select2-selection--single {
      height: 38px !important;
      /* border: 1px solid #ced4da; */
      border-radius: 0.1px;

    }

    .select2-selection__arrow {
      height: 38px !important;
    }

	/* .modal {
	position: absolute;
	top: 50px;
	right: 100px;
	bottom: 0;
	left: 0;
	z-index: 10040;
	overflow: auto;
	overflow-y: auto;
} */

    
	</style>


	
</head>

<body>