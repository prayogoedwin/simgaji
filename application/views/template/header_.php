	<?php $urlFoto = 'https://simpeg.bkd.jatengprov.go.id/eps/showpic/' . $session['B_02B']; ?>

	<?php
	$role = $this->session->userdata('role');
	$lokasis_id = $this->session->userdata('id_lokasis');
	if($role == 3){
		$kode_status_gaji = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;
		$teks_status_gaji = textStatusDataStatusGaji($kode_status_gaji);
	}
	?>
	<!-- Main navbar -->
	<div class="navbar navbar-expand-md navbar-dark">
		<div class="navbar-brand">
			<a href="<?= base_url() ?>" class="d-inline-block">
				<img src="<?= base_url('assets/') ?>statis/logo_white.png" alt="">
			</a>
		</div>

		<?php if($role == 3):?>
		<div class="ml-auto" style="margin-top: 12px; margin-right: 4px;">
			<?=$teks_status_gaji?>
		</div>
		<?php endif; ?>

		<div class="ml-auto" style="margin-top: 12px; margin-right: 4px;">
			<span class="badge badge-danger">GAJI <?= bulanAngkaToHuruf(getDataPeriode()->bulan) ?> <?= getDataPeriode()->tahun ?></span>
		</div>
		<!-- <div class="ml-auto" style="margin-top: 12px;">
			<span class="badge badge-danger">GAJI MEI 2022 - R</span>
		</div> -->

		<div class="d-md-none">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
				<i class="icon-menu7"></i>
			</button>
		</div>

		<div class="collapse navbar-collapse" id="navbar-mobile">
			<ul class="navbar-nav ml-auto">
				<!-- <li class="nav-item">
					<a href="<?= base_url('dashboard') ?>" class="navbar-nav-link">
						<i class="icon-home4 mr-2"></i>
						Dashboard
					</a>
				</li> -->

				<!-- START SUPERADMIN -->
				<?php if($role == 1) :?>
				<li class="nav-item">
					<a href="<?= base_url('kalkulasi') ?>" class="navbar-nav-link">
						Kalkulasi
					</a>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Data Pegawai</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('masterpegawai_p3ks') ?>" class="dropdown-item">Master Pegawai P3K</a>
						<a href="<?= base_url('rekapadmin_p3ks') ?>" class="dropdown-item">Rekap Admin P3K</a>
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Verifikasi</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('verifikasi_p3ks') ?>" class="dropdown-item">Pegawai P3K</a>
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Cetak P3K</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI BULANAN</a>
						<!-- <a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">TPP</a> -->
						<a href="<?= base_url('cetakp3k/gaji_13') ?>" class="dropdown-item">GAJI 13</a>
						<a href="<?= base_url('cetakp3k/gaji_14') ?>" class="dropdown-item">GAJI 14</a>
						<!-- <a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">TPP 13</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI HARI RAYA</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI TAHUNAN</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">TPP TAHUNAN</a>

						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI BULANAN P3K</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">TPP P3K</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI 13 P3K</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">TPP 13 P3K</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI HARI RAYA P3K</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI TAHUNAN P3K</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">TPP TAHUNAN P3K</a>

						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">GAJI SUSULAN</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">MUTASI DATA PEGAWAI</a>
						<a href="<?= base_url('cetakp3k/gaji_bulanan') ?>" class="dropdown-item">MUTASI DATA INSENTIP</a> -->

					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Master</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('pengumuman') ?>" class="dropdown-item">Pengumuman</a>

						<div class="dropdown-submenu">
							<a href="#" class="dropdown-item dropdown-toggle">Standart Gaji Pokok</a>
							<div class="dropdown-menu">
								<a href="<?= base_url('gajipokok') ?>" class="dropdown-item">PNS</a>
								<a href="<?= base_url('gajipokok_p3ks') ?>" class="dropdown-item">PPPK</a>

							</div>
						</div>

						<div class="dropdown-submenu" hidden>
							<a href="<?= base_url('standargaji') ?>" class="dropdown-item dropdown-toggle">Standart Tunjangan</a>
							<div class="dropdown-menu">
								<a href="<?= base_url('standargaji/jft') ?>" class="dropdown-item">JFT</a>
								<a href="<?= base_url('standargaji/jfu') ?>" class="dropdown-item">JFU</a>
								<a href="<?= base_url('standargaji/struktural') ?>" class="dropdown-item">STRUKTURAL</a>
							</div>
						</div>

						<div class="dropdown-submenu">
							<a href="<?= base_url('standartunjangan') ?>" class="dropdown-item dropdown-toggle">Standart Tujangan</a>
							<div class="dropdown-menu">
								<a href="<?= base_url('standartunjangan/eselon') ?>" class="dropdown-item">ESELON</a>
								<a href="<?= base_url('standartunjangan/fungsional') ?>" class="dropdown-item">FUNGSIONAL</a>
								<a href="<?= base_url('standartunjangan/umum') ?>" class="dropdown-item">UMUM</a>
								<a href="<?= base_url('standartunjangan/kedudukan') ?>" class="dropdown-item">KEDUDUKAN</a>
								<a href="<?= base_url('standartunjangan/umump3k') ?>" class="dropdown-item">UMUM P3K</a>
							</div>
						</div>

						<div class="dropdown-submenu">
							<a href="<?= base_url('usia') ?>" class="dropdown-item dropdown-toggle">Usia</a>
							<div class="dropdown-menu">
								<a href="<?= base_url('usia/kedudukan') ?>" class="dropdown-item">Kedudukan</a>
								<a href="<?= base_url('usia/fungsional') ?>" class="dropdown-item">Fungsional</a>
								<!-- <a href="<?= base_url('usia/golonganp3k') ?>" class="dropdown-item">Golongan P3K</a> -->
							</div>
						</div>

						<!-- <a href="<?= base_url('eselon') ?>" class="dropdown-item">Eselon</a> -->
						<a href="<?= base_url('statusdata') ?>" class="dropdown-item">Status Data</a>
						<a href="<?= base_url('lokasi') ?>" class="dropdown-item">Lokasi</a>
						<a href="<?= base_url('pendidikan') ?>" class="dropdown-item">Pendidikan</a>
						<a href="<?= base_url('potongan') ?>" class="dropdown-item">Potongan</a>
						<a href="<?= base_url('kode') ?>" class="dropdown-item">Kode</a>
					</div>
				</li>
				<?php endif; ?>
				<!-- END SUPERADMIN -->


				<!-- START SKPD PENGAJU -->
				<!-- 3 admin SKPD, 4 admin UPT (Sekolah), 5 Sekretaris SKPD Prov, 6 Kadinas SKPD Prov, 7 TU Cabdin -->
				<?php if($role == 3 || $role == 4 || $role == 7) :?>
				<li class="nav-item">
					<a href="<?= base_url('approval_usulan_p3k') ?>" class="navbar-nav-link">
						Approval Usulan P3K
					</a>
				</li>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Data Pegawai</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('pegawai_p3k') ?>" class="dropdown-item">Pegawai P3K</a>
						<a href="<?= base_url('history_p3k') ?>" class="dropdown-item">History Pegawai P3K</a>
						<a href="<?= base_url('rekap_p3k') ?>" class="dropdown-item">Rekap P3K</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Cetak P3K SKPD</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('cetak_skpd/gaji_bulanan_p3k') ?>" class="dropdown-item">GAJI BULANAN P3K</a>
						<a href="<?= base_url('cetak_skpd/gaji_13_p3k') ?>" class="dropdown-item">GAJI 13 P3K</a>
					</div>
				</li>
				<?php endif; ?>
				<!-- END SKPD PENGAJU -->

				<!-- START SEKRETARIS DAN KEPALA DINAS -->
				<?php if($role == 5 || $role == 6) :?>
				<li class="nav-item">
					<a href="<?= base_url('approval_usulan_p3k') ?>" class="navbar-nav-link">
						Approval Usulan P3K
					</a>
				</li>
				<?php endif; ?>
				<!-- END SEKRETARIS DAN KEPALA DINAS -->

				<!-- START VERIFIKATOR -->
				<?php if($role == 2) :?>
				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Verifikasi</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('verifikasi_p3ks') ?>" class="dropdown-item">Pegawai P3K</a>
					</div>
				</li>

				<li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Data Pegawai</a>

					<div class="dropdown-menu">
						<a href="<?= base_url('masterpegawai_p3ks') ?>" class="dropdown-item">Master Pegawai P3K</a>
						<a href="<?= base_url('rekapadmin_p3ks') ?>" class="dropdown-item">Rekap Admin P3K</a>
					</div>
				</li>
				<?php endif; ?>
				<!-- END VERIFIKATOR -->



				<!-- <li class="nav-item">
					<a href="<?= base_url('management_user') ?>" class="navbar-nav-link">
						<i class="icon-users mr-2"></i>
						Management User
					</a>
				</li> -->

				<!-- <li class="nav-item dropdown">
					<a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">Vertical</a>

					<div class="dropdown-menu">
						<a href="#" class="dropdown-item">Abc</a>
						<a href="#" class="dropdown-item">Def</a>
						<a href="#" class="dropdown-item">Ghi</a>
						<div class="dropdown-submenu">
							<a href="#" class="dropdown-item dropdown-toggle">Has child</a>
							<div class="dropdown-menu">
								<a href="#" class="dropdown-item">Third level</a>
								<div class="dropdown-submenu">
									<a href="#" class="dropdown-item dropdown-toggle">Has child</a>
									<div class="dropdown-menu">
										<a href="#" class="dropdown-item">Fourth level</a>
										<a href="#" class="dropdown-item">Fourth level</a>
									</div>
								</div>
								<a href="#" class="dropdown-item">Third level</a>
							</div>
						</div>

					</div>
				</li> -->

				<li class="nav-item">
					<!-- <a href="../full/index.html" class="navbar-nav-link"> -->

					&nbsp; &nbsp;
					&nbsp; &nbsp;
					&nbsp; &nbsp;


					<!-- Dashboard -->
					<!-- </a> -->
				</li>

				<!-- <li class="nav-item dropdown">
						<a href="#" class="navbar-nav-link">
							<i class="icon-bell2"></i>
							<span class="d-md-none ml-2">Notifications</span>
							<span class="badge badge-mark border-white ml-auto ml-md-0"></span>
						</a>
					</li> -->

				<li class="nav-item dropdown">
					<?php
					$nip = $this->session->userdata('B_02B');
					if($role != 1){
						$angka_notif = countingNotif($nip);
					} else {
						$angka_notif = 0;
					}

					?>
					<a href="#" class="navbar-nav-link dropdown-toggle caret-0" data-toggle="dropdown" aria-expanded="false">
						<i class="icon-bell2"></i>
						<span class="d-md-none ml-2">Messages</span>
						<span class="badge badge-pill bg-warning-400 ml-auto ml-md-0"><?= $angka_notif ?></span>
					</a>

					<div class="dropdown-menu dropdown-menu-right dropdown-content wmin-md-350">
						<div class="dropdown-content-header">
							<span class="font-weight-semibold">Notifikasi</span>
							<a href="#" class="text-default"><i class="icon-compose"></i></a>
						</div>

						<div class="dropdown-content-body dropdown-scrollable">
							<ul class="media-list">
								<li class="media">
									<!-- <div class="mr-3 position-relative">
										<img src="../../../../global_assets/images/placeholders/placeholder.jpg" width="36" height="36" class="rounded-circle" alt="">
									</div> -->

									<?php
									if($role != 1){
										$notifikasis = getMyNotif($nip);
									} else {
										$notifikasis = array();
									}

									?>

									<div class="media-body">
										<?php if ($angka_notif > 0) : ?>
											<?php foreach ($notifikasis as $ki => $valnotif) : ?>
												<div class="media-title">
													<a href="<?= base_url('dashboard/open_notif/' . encode_url($valnotif->id)); ?>">
														<span class="font-weight-semibold"><?= $valnotif->judul ?></span>
														<span class="text-muted float-right font-size-sm"><?= $valnotif->created_at ?></span>
													</a>
												</div>

												<span class="text-muted"><?= $valnotif->isi; ?></span>
											<?php endforeach; ?>
										<?php endif; ?>

									</div>
								</li>

							</ul>
						</div>

						<div class="dropdown-content-footer justify-content-center p-0">
							<a href="#" class="bg-light text-grey w-100 py-2" data-popup="tooltip" title="" data-original-title="Load more"><i class="icon-menu7 d-block top-0"></i></a>
						</div>
					</div>
				</li>

				<li class="nav-item dropdown dropdown-user">
					<a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
						<img src="<?= $urlFoto ?>" class="rounded-circle mr-2" height="34" alt="">
						<!-- <img src="http://simpeg.bkd.jatengprov.go.id/eps/showpic/<?= $session['B_02B']; ?>" class="rounded-circle mr-2" height="34" alt=""> -->
						<span><?= $session['B_02B']; ?></span>
					</a>

					<div class="dropdown-menu dropdown-menu-right">
						<a href="#" class="dropdown-item"><i class="icon-user-plus"></i><?= $session['name']; ?></a>
						<!-- <a href="#" class="dropdown-item"><i class="icon-coins"></i> Jabatan</a> -->
						<!-- <a href="#" class="dropdown-item"><i class="icon-comment-discussion"></i> Messages <span class="badge badge-pill bg-blue ml-auto">58</span></a> -->
						<!-- <div class="dropdown-divider"></div> -->
						<!-- <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Setting Akun</a> -->
						<?php if($role == 1) :?>
						<a href="<?= base_url('verifikator') ?>" class="dropdown-item"><i class="icon-users"></i> Users</a>
						<a href="<?= base_url('setting') ?>" class="dropdown-item"><i class="icon-cog5"></i> Setting</a>
						<?php endif ;?>

						<!-- ADMIN SKPD -->
						<?php if($role == 3) :?>
						<a href="<?= base_url('statusdata_skpd') ?>" class="dropdown-item"><i class="icon-cog5"></i> Status Data</a>
						<?php endif ;?>


						<a href="<?= base_url('user/logout') ?>" class="dropdown-item"><i class="icon-switch2"></i> Logout</a>
					</div>
				</li>
			</ul>




		</div>
	</div>
	<!-- /main navbar -->
