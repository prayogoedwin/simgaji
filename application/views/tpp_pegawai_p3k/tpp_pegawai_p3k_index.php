<?php
$role = $this->session->userdata('role');
$lokasis_id = $this->session->userdata('id_lokasis');
?>
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">

        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <span class="breadcrumb-item active">TPP Pegawai P3K</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <form action="<?=base_url('tpp_pegawai_p3k/ajukan_tpp_action')?>" onsubmit="return confirm('Apakah anda sudah yakin data akan diajukan?');" method="post">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <div class="header-elements">
                        <!-- <a href="<?=base_url('tpp_pegawai_p3k/ajukan_tpp_action')?>" class="btn btn-labeled btn-labeled-right bg-primary">Ajukan <b><i class="icon-paperplane"></i></b></a> -->
                        <button type="submit" class="btn btn-labeled btn-labeled-right bg-primary">Ajukan <b><i class="icon-paperplane"></i></b></button>
                    </div>
                </div>

                <div class="table-responsive" id="tableDefault">
                    <table class="table table-stripe table-condensed" id="tabledt">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP <br>Nama <br>Jabatan</th>
                                <th>Cuti Sakit <br>(Hari)</th>
                                <th>TB</th>
                                <th>Kelas Jab <br>Rumpun</th>
                                <th>Potongan</th>
                                <th>Hukdis</th>
                                <th>SKP</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pegawais as $key => $value) { ?>
                                <tr>
                                    <td><?=$key+1;?></td>
                                    <td><?=$value['B_02B'];?><br><?=$value['nama'];?><br><?=$value['jabatan'];?></td>
                                    <td>
                                        <?php if($value['cuti_2bulanlalu'] != 0) :?>
                                            <div class="alert alert-warning">
                                                <input type="text" class="form-control" name="sakit[]" value="<?=$value['cuti_2bulanlalu'];?>">
                                            </div>
                                        <?php else :?>
                                            <input type="text" class="form-control" name="sakit[]" value="<?=$value['cuti_2bulanlalu'];?>">
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <input type="checkbox" name="tb_<?php echo $value['B_02B'] ?>" value="1">
                                    </td>
                                    <td>
                                        <div>
                                            <?=$value['kelas_jabatan']?><br><?=$value['rumpun']?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <input type="text" value="0" class="form-control" name="potongan">
                                            <input type="hidden" value="<?=$value['B_02B']?>" class="form-control" name="nips[]">
                                            <input type="hidden" value="<?=$value['F_03']?>" class="form-control" name="gols[]">
                                            <input type="hidden" value="<?=$value['jabatan']?>" class="form-control" name="jabatans[]">
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?=$value['hukdis']?>
                                            <!-- <select class="form-control" name="hukdis[]" id="hukdis[]">
                                                <option value="100" selected="">Tidak</option>
                                                <option value="90">Ringan</option>
                                                <option value="80">Sedang</option>
                                                <option value="50">Berat</option>
                                            </select> -->
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <input type="text" name="skp[]" class="form-control" value="100">
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <input type="text" name="keterangan[]" class="form-control">
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </form>
            
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    // $(function () {
    //     var tabel = $('#tabledt').DataTable({

    //         'destroy'     : true,
    //         'paging'      : true,
    //         'lengthChange': true,
    //         'searching'   : true,
    //         'ordering'    : true,
    //         'info'        : true,
    //         'autoWidth'   : false,
    //         'data': <?=json_encode($datatable);?>,
    //         'columns': [
    //             { data: null, sortable : false, searceable : false },
    //             { data: 'nip' },
    //             { data: 'name' },
    //             { data: 'skp' },
    //             { data: 'potongan' },
    //             // { data: 'aksi', sortable : false, searceable : false  }
    //         ],
    //         columnDefs: [{ }],
    //     });

    //     tabel.on( 'order.dt search.dt', function () {
    //         tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
    //             cell.innerHTML = i+1;
    //         } );
    //     } ).draw();

    // });
</script>

<script>
    $('#btn_update').on('click',function(){
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var id = $('#idne').val();
        var golongan = $('#golongan').val();
        var potongan = $('#potongan').val();

        $.ajax({
            type : "POST",
            url  : "<?php echo base_url('potongan/update_action')?>",
            data : {
                id : id,
                golongan : golongan,
                potongan : potongan
            },
            dataType : "JSON",
            success: function(data){

                var sts = data['status'];
                var msg = data['message'];

                if(sts == 1){
                    $('[name="idne"]').val("");
                    $('[name="golongan"]').val("");
                    $('[name="potongan"]').val("");
                    $('#mdlDetail').modal('hide');

                    $('#tabledt').DataTable().ajax.reload(null, false);
                    swalInit.fire({
                        title: 'Yeay',
                        text: msg,
                        type: 'success'
                    });
                } else {
                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            // alert('Notification is closed.');
                            location.reload();
                        }
                    });
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swalInit.fire({
                        title: 'Oppss',
                        text: 'Terjadi kesalahan nih',
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
            }
        });
        return false;
    });
</script>

<script>
    function hapusData(){
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var id = $('#idne').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax');

                $.ajax({
                    url: "<?=base_url('potongan/hapus_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
function confirmAjukan(){
    alert('jal ajukan');
}
</script>

<script>

    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>
