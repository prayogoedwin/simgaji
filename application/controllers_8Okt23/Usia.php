<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usia extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Usia_model');
	}

    // START KEDUDUKAN
    public function kedudukan(){
        $data_header['session'] = $this->session->all_userdata();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/usia/kedudukan_index');

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getKedudukan(){
        $get = $this->Usia_model->getDataKedudukan();

        $datatable = array();
		foreach ($get as $key => $value) {

            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->kode .'</a>';

			$datatable[$key] = array(
                'num' => $key + 1,
				'kode' => $klik_kode,
				'name' => $value->name,
				'usia' => $value->usia,
				'jenis' => $value->jenis
			);
		}

        $output = array(
            "recordsTotal" => $this->Usia_model->getDataKedudukanCount(),
            "recordsFiltered" => $this->Usia_model->getDataKedudukanCountFiltered(),
            "data" => $datatable,
        );

        // output to json format
        header('Content-Type: application/json');
        echo json_encode($output);
    }

    public function tambah_kedudukan_action(){
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $usia = $this->input->post('usia', TRUE);
        $jenis = $this->input->post('jenis', TRUE);
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'usia' => $usia,
            'jenis' => $jenis,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $upd = $this->Fungsi_model->tambah('simgaji_kedudukans', $data);
        if ($upd) {
            $response = array(
				'status' => 1,
				'message' => 'Berhasil menambahkan data'
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Gagal menambahkan data'
			);
        }

		header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getDetailKedudukan(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Usia_model->getDataKedudukanById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function update_kedudukan_action(){
        $id = $this->input->post('id', TRUE);
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $usia = $this->input->post('usia', TRUE);
        $jenis = $this->input->post('jenis', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'usia' => $usia,
            'jenis' => $jenis,
            'updated_at' => $now
        );

        $upd = $this->Fungsi_model->edit('simgaji_kedudukans', $id, $data);
        if ($upd) {
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil diperbaharui'
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal diperbaharui'
			);
        }

		header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hapus_kedudukan_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus('simgaji_kedudukans', $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    // END KEDUDUKAN

    // START FUNGSIONAL
    public function fungsional(){
        $data_header['session'] = $this->session->all_userdata();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/usia/fungsional_index');

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getFungsional(){
        $get = $this->Usia_model->getDataFungsional();

        $datatable = array();
		foreach ($get as $key => $value) {

            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->kode .'</a>';

            $rmpn = '-';
            if($value->rumpun != null){
                $rmpn = $value->rumpun;
            }

			$datatable[$key] = array(
                'num' => $key + 1,
				'kode' => $klik_kode,
				'name' => $value->name,
				'usia' => $value->usia,
				'rumpun' => $rmpn
			);
		}

        $output = array(
            "recordsTotal" => $this->Usia_model->getDataFungsionalCount(),
            "recordsFiltered" => $this->Usia_model->getDataFungsionalCountFiltered(),
            "data" => $datatable,
        );

        // output to json format
        header('Content-Type: application/json');
        echo json_encode($output);
    }

    public function tambah_fungsional_action(){
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $usia = $this->input->post('usia', TRUE);
        $rumpun = $this->input->post('rumpun', TRUE);
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'usia' => $usia,
            'rumpun' => $rumpun,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $add = $this->Fungsi_model->tambah('simgaji_fungsionals', $data);
        if ($add) {
            $response = array(
				'status' => 1,
				'message' => 'Berhasil menambahkan data'
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Gagal menambahkan data'
			);
        }

		header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function getDetailFungsional(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Usia_model->getDataFungsionalById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function update_fungsional_action(){
        $id = $this->input->post('id', TRUE);
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $usia = $this->input->post('usia', TRUE);
        $rumpun = $this->input->post('rumpun', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'usia' => $usia,
            'rumpun' => $rumpun,
            'updated_at' => $now
        );

        $upd = $this->Fungsi_model->edit('simgaji_fungsionals', $id, $data);
        if ($upd) {
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil diperbaharui'
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal diperbaharui'
			);
        }

		header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hapus_fungsional_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus('simgaji_fungsionals', $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    // END FUNGSIONAL
}