<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Pegawai_p3k_model');
		$this->load->model('Pegawai_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $role = $this->session->userdata('role');
        if($role == 4){
            //kasubbag TU Sekolah

            $lokasis_id = $this->session->userdata('id_lokasis');
            $row_lokasi = $this->Pegawai_p3k_model->getKodeLokasisById($lokasis_id);
            // $kode6digit = substr($row_lokasi->kode, 0, 6);
            // $get = $this->Pegawai_p3k_model->getDataPegawaiP3kTuSekolah($row_lokasi->kode);
            $get = $this->Pegawai_p3k_model->getDataPegawaiP3kTuSekolah($row_lokasi->id);

            $datatable = array();
            foreach ($get as $key => $value) {
                $klik_nip = '<a href="'. base_url('pegawai_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';

                $datatable[$key] = array(
                    'nip' => $klik_nip,
                    'name' => $value->name,
                    'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('pegawai_p3k/data/') . encode_url($value->id) .'"><i class="icon-lasso2"></i> Lihat</a>'
                );
            }
            $data['datatable'] = $datatable;

        } else if($role == 7){
            //TU Cabdin
            $lokasis_id = $this->session->userdata('id_lokasis');
            $data['dropd'] = $this->Pegawai_p3k_model->getDropCabdinIn($lokasis_id);

            if(isset($_GET['kode'])){

                $kode = $_GET['kode'];

                $get = $this->Pegawai_p3k_model->getDataPegawaiForCabdin($kode);

                $datatable = array();
                foreach ($get as $key => $value) {
                    $klik_nip = '<a href="'. base_url('pegawai_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';

                    $datatable[$key] = array(
                        'nip' => $klik_nip,
                        'name' => $value->name,
                        'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('pegawai_p3k/data/') . encode_url($value->id) .'"><i class="icon-lasso2"></i> Lihat</a>'
                    );
                }
                $data['datatable'] = $datatable;

                $data['filter'] = array(
                    'kodefilter' => $kode
                );

            } else {

                $data['datatable'] = array();

                $data['filter'] = array(
                    'kodefilter' => null
                );
            }

        } else {
            $data['datatable'] = array();
        }

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('pegawai_p3k/pegawai_p3k_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getData(){
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;

        $query  =
        "SELECT p.`id`, p.`nip`, p.`name`
        FROM `simgaji_pegawaip3ks` p";

        $search = array('p.nip','p.`name`');
        $where  = array('lokasi_kerja' => $lokasis_id);
        // $where  = array('lokasi_gaji' => $lokasis_id);
        // $where  = array('nama_kategori' => 'Tutorial');

        // jika memakai IS NULL pada where sql
        $isWhere = null;
        // $isWhere = 'artikel.deleted_at IS NULL';
        header('Content-Type: application/json');
        echo $this->Pegawai_p3k_model->getDataPegawaiP3K($query,$search,$where,$isWhere, $statusSkpd);
    }

    public function data($id){
        $real_id = decode_url($id);
        $cek = cek_realparams('simgaji_pegawaip3ks', $real_id);
        if($cek){
            $this->lanjutData($real_id);
        } else {

            $cek_cpns = cek_realparams('simgaji_pegawais', $real_id);
            if($cek_cpns){
                $this->lanjutDataCpns($real_id);
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Invalid parameter'
                );

                $this->session->set_flashdata($array_msg);
                redirect('pegawai_p3k');
            }
        }
    }

    public function detail($id){
        $real_id = decode_url($id);
        $cek = cek_realparams('simgaji_pegawaip3ks', $real_id);
        if($cek){
            $this->lanjutDetail($real_id);
        } else {

            $cek_cpns = cek_realparams('simgaji_pegawais', $real_id);
            if($cek_cpns){
                $this->lanjutDetailCpns($real_id);
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Invalid parameter'
                );

                $this->session->set_flashdata($array_msg);
                redirect('pegawai_p3k');
            }
        }
    }

    public function detail_action(){
        $user_id = $this->session->userdata('id');
        // $id_lokasis = $this->session->userdata('id_lokasis');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $now = date('Y-m-d H:i:s');

        if(count($kode_id) > 0){
            $datahistory = array();
            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);

                $nilai_lama = $this->Pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;

                $nilai_baru = $this->input->post($field_name, TRUE);

                $A_01 = $this->session->userdata('A_01');
                $A_02 = $this->session->userdata('A_02');

                if($A_01 == 'D0' && $A_02 != '00'){
                    //bukan SKPD induk, turunan disdik prov. jenjang here

                    $nip = $this->input->post('nipe', TRUE);
                    $nama = $this->input->post('nama', TRUE);
                    $jabatan = $this->input->post('jabatan', TRUE);

                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
                    $myCabdinKolok = getKolokCabdin($myKolok);
                    $fix_myCabdinKolok = getKolokCabdin($myCabdinKolok); //fix perubahan tablokb08

                    $dataMyVerifikator = getMyVerifikator($fix_myCabdinKolok);
                    if($dataMyVerifikator != null){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            'verifikator' => $dataMyVerifikator->id,
                            'status_ajuan' => 0,
                            'posisi_acc' => $nip,
                            'posted_by' => $user_id,
                            'created_at' => $now
                        );
                    } else {
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'Sekolah / Cabdin anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }

                } else {
                    //SKPD induk prov
                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

                    // $dataMyVerifikator = getMyVerifikator($myKolok);

                    // $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
                    // foreach ($dataAllMyVerifikator as $key => $valVerif) {
                    //     $idVerif[] = $valVerif->id;
                    // }
                    // $allIdVerifikator = implode(",", $idVerif);
                    // if(count($dataAllMyVerifikator) > 0){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            // 'verifikator' => $dataMyVerifikator->id,
                            // 'verifikator' => $allIdVerifikator,
                            'verifikator' => 17,
                            'status_ajuan' => 1,
                            // 'posisi_acc' => $dataMyVerifikator->nip,
                            'posisi_acc' => getNipSekByKolok($myKolok),
                            'posted_by' => $user_id,
                            'created_at' => $now
                        );
                    // } else {
                    //     //SKPD ybs blm pny verifikator
                    //     $array_msg = array(
                    //         'status'=>'error',
                    //         'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                    //     );
                    //
                    //     $this->session->set_flashdata($array_msg);
                    //     redirect('pegawai_p3k');
                    // }
                }

            }

            // echo json_encode($datahistory);
            // die();

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_historyp3ks', $datahistory);
            if($ins){
                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('history_p3k');
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('pegawai_p3k');
            }
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Pastikan ada data yang dipilih'
            );

            $this->session->set_flashdata($array_msg);
            redirect('pegawai_p3k/detail/' . encode_url($pegawai_id));
        }

    }

    public function getDataCpns(){
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;
        $lokasis_id = '19000000';

        $query  =
        "SELECT p.`id`, p.`nip`, p.`name`
        FROM `simgaji_pegawais` p";

        $search = array('p.nip','p.`name`');
        $where  = array('lokasikerja' => $lokasis_id);
        // $where  = array('lokasi_gaji' => $lokasis_id);
        // $where  = array('nama_kategori' => 'Tutorial');

        // jika memakai IS NULL pada where sql
        $isWhere = null;
        // $isWhere = 'artikel.deleted_at IS NULL';
        header('Content-Type: application/json');
        echo $this->Pegawai_p3k_model->getDataPegawaiP3K($query,$search,$where,$isWhere, $statusSkpd);
    }

    private function lanjutData($real_id){
        $data_header['session'] = $this->session->all_userdata();

        $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
        $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();

        $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
        $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
        $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
        $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
        $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
        $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
        $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
        $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
        $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
        $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
        $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

        $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
        $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
        // $data['masterjfu'] = array();
        // $data['masterjft'] = array();
        // $data['mastersotk'] = array();
        // $data['mastersotk'] = array();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('pegawai_p3k/data_pegawai', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    private function lanjutDataCpns($real_id){
        $data_header['session'] = $this->session->all_userdata();

        $data['detail'] = $this->Pegawai_model->getDetailById($real_id);
        $data['kodes'] = $this->Pegawai_model->getDataKodes();

        $data['lokasi'] = $this->Pegawai_model->getDataDataMaster('simgaji_lokasis');
        $data['gender'] = $this->Pegawai_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Pegawai_model->getDataDataMaster('simgaji_agamas');
        $data['marital'] = $this->Pegawai_model->getDataDataMaster('simgaji_maritals');
        $data['bool'] = $this->Pegawai_model->getDataDataMaster('simgaji_bools');
        $data['statusp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_statusp3ks');
        $data['status'] = $this->Pegawai_model->getDataDataMaster('simgaji_statuses');
        $data['golongan'] = $this->Pegawai_model->getDataDataMaster('simgaji_golongans');
        $data['golonganp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_golonganp3ks');
        $data['eselon'] = $this->Pegawai_model->getDataDataMaster('simgaji_eselons');
        $data['pendidikan'] = $this->Pegawai_model->getDataDataMaster('simgaji_pendidikans');
        $data['kedudukan'] = $this->Pegawai_model->getDataDataMaster('simgaji_kedudukans');

        $data['fungsional'] = $this->Pegawai_model->getDataDataMaster('simgaji_fungsionals');
        $data['profesi'] = $this->Pegawai_model->getDataDataMaster('simgaji_profesis');
        // $data['masterjfu'] = array();
        // $data['masterjft'] = array();
        // $data['mastersotk'] = array();
        // $data['mastersotk'] = array();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('pegawai_p3k/data_pegawai', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    private function lanjutDetail($real_id){
        $jns = $this->input->get('jns', TRUE);

        $data['ekasus'] = array(
            'kode_hot' => 0,
            'psn' => '--'
        );

        if($jns == '1' || $jns == '2'){
            $nip_ybs = $this->Pegawai_p3k_model->getDetailById($real_id)->nip;
            $cekKasus = $this->Pegawai_p3k_model->ekasus($nip_ybs);
            if($cekKasus->hot > 0){
                $data['ekasus'] = array(
                    'kode_hot' => $cekKasus->hot,
                    'psn' => 'PNS bersangkutan terdaftar dalam sistem e - kasus'
                );
            } else {
                $data['ekasus'] = array(
                    'kode_hot' => $cekKasus->hot,
                    'psn' => 'PNS bersangkutan tidak terdaftar dalam sistem e - kasus'
                );
            }
        }

        if($jns == '' || $jns == null || $jns == '1' || $jns == '2' || $jns == '3' || $jns == '4' || $jns == '5' || $jns == '6' || $jns == '7' || $jns == '8'){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
            $data['gol_ybs'] = $this->Pegawai_p3k_model->getGolonganYbs($data['detail']->golongan_id)->name;
            $data['pangkat_ybs'] = $this->Pegawai_p3k_model->getGolonganYbs($data['detail']->golongan_id)->kode;
            // $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();
            $data['kodes'] = $this->Pegawai_p3k_model->getDataKodesByJenis($jns);

            $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

            $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
            // $data['masterjfu'] = array();
            // $data['masterjft'] = array();
            // $data['mastersotk'] = array();
            // $data['mastersotk'] = array();
            $golongan_id = $this->Pegawai_p3k_model->getDetailById($real_id)->golongan_id;
            $masa = $this->Pegawai_p3k_model->getDetailById($real_id)->masa_kerja;
            $masa = substr($masa, 0, 2);
            $data['rekomen_gaji'] = $this->Pegawai_p3k_model->getDataRekomenGaji($golongan_id, $masa);
            $nip_pegawai = $this->Pegawai_p3k_model->getDetailById($real_id)->nip;
            $nip_lama_simpeg = getDataMastfip($nip_pegawai)->B_02;
            $data['family'] = $this->Pegawai_p3k_model->getDataKeluarga($nip_lama_simpeg);

            $data['filter'] = array(
                'params' => $jns
            );

            //start efile
            $paramsDokEfile = '';

            foreach ($data['kodes'] as $kii => $valkodes) {
                if($valkodes->kode_dokumen != null){
                    $paramsDokEfile .= $valkodes->kode_dokumen . ',';
                }
            }

            $params = array(
                'token' => '!BKDoyee123',
                'kodes' => rtrim($paramsDokEfile, ","),
                'nip' => $nip_pegawai
            );

            $res = ClientPost($params);
            $result = json_decode($res, TRUE);
            $data['efilenya'] = $result['data'];
            //end efile

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('pegawai_p3k/detail_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('pegawai_p3k');
        }
    }

    private function lanjutDetailCpns($real_id){
        $jns = $this->input->get('jns', TRUE);

        $data['ekasus'] = array(
            'kode_hot' => 0,
            'psn' => '--'
        );

        if($jns == '1' || $jns == '2'){
            $nip_ybs = $this->Pegawai_model->getDetailById($real_id)->nip;
            $cekKasus = $this->Pegawai_model->ekasus($nip_ybs);
            if($cekKasus->hot > 0){
                $data['ekasus'] = array(
                    'kode_hot' => $cekKasus->hot,
                    'psn' => 'PNS bersangkutan terdaftar dalam sistem e - kasus'
                );
            } else {
                $data['ekasus'] = array(
                    'kode_hot' => $cekKasus->hot,
                    'psn' => 'PNS bersangkutan tidak terdaftar dalam sistem e - kasus'
                );
            }
        }

        if($jns == '' || $jns == null || $jns == '1' || $jns == '2' || $jns == '3' || $jns == '4' || $jns == '5' || $jns == '6' || $jns == '7' || $jns == '8'){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Pegawai_model->getDetailById($real_id);
            $data['gol_ybs'] = $this->Pegawai_model->getGolonganYbs($data['detail']->golongan_id)->name;
            $data['pangkat_ybs'] = $this->Pegawai_model->getGolonganYbs($data['detail']->golongan_id)->kode;
            // $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();
            $data['kodes'] = $this->Pegawai_model->getDataKodesByJenis($jns);

            $data['lokasi'] = $this->Pegawai_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Pegawai_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Pegawai_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Pegawai_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Pegawai_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Pegawai_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Pegawai_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Pegawai_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Pegawai_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Pegawai_model->getDataDataMaster('simgaji_kedudukans');

            $data['fungsional'] = $this->Pegawai_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Pegawai_model->getDataDataMaster('simgaji_profesis');
            // $data['masterjfu'] = array();
            // $data['masterjft'] = array();
            // $data['mastersotk'] = array();
            // $data['mastersotk'] = array();
            $golongan_id = $this->Pegawai_model->getDetailById($real_id)->golongan_id;
            $masa = $this->Pegawai_model->getDetailById($real_id)->masa_kerja;
            $masa = substr($masa, 0, 2);
            $data['rekomen_gaji'] = $this->Pegawai_model->getDataRekomenGaji($golongan_id, $masa);
            $nip_pegawai = $this->Pegawai_model->getDetailById($real_id)->nip;
            $nip_lama_simpeg = getDataMastfip($nip_pegawai)->B_02;
            $data['family'] = $this->Pegawai_model->getDataKeluarga($nip_lama_simpeg);

            $data['filter'] = array(
                'params' => $jns
            );

            //start efile
            $paramsDokEfile = '';

            foreach ($data['kodes'] as $kii => $valkodes) {
                if($valkodes->kode_dokumen != null){
                    $paramsDokEfile .= $valkodes->kode_dokumen . ',';
                }
            }

            $params = array(
                'token' => '!BKDoyee123',
                'kodes' => rtrim($paramsDokEfile, ","),
                'nip' => $nip_pegawai
            );

            $res = ClientPost($params);
            $result = json_decode($res, TRUE);
            $data['efilenya'] = $result['data'];
            //end efile

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('pegawai_p3k/detail_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('pegawai_p3k');
        }
    }
}
