<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standartunjangan extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();
        $this->eselons = 'eselons a';
        $this->umums = 'tjumums a';
        $this->umump3ks = 'tjumump3ks a';
        $this->fungsionals = 'simgaji_fungsionals a';


        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Standartunjangan_model');
	}

    public function index(){
        redirect('dashboard');
    }


    //CRUD ESELONS START//
    public function get_eselon(){
        $get = $this->Standartunjangan_model->get_eselon();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'kode' => $value->kode,
				'name' => $klik_kode,
                'tunjangan' => number_format($value->tunjangan),
				'usia' => $value->usia,
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(

            "recordsTotal" => $this->Standartunjangan_model->eselon_count_all(),
            "recordsFiltered" => $this->Standartunjangan_model->eselon_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function eselon(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Tunjangan - Eselon';
        $data['breadcrumb'] =
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Tunjangan - Eselon</span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/standartunjangan/eselon/index', $data);
    }

    public function get_detail_eselon(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standartunjangan_model->getEselonById($id);
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function eselon_update_action(){
        $id = $this->input->post('id', TRUE);
		$kode = $this->input->post('kode', TRUE);
        $nama = $this->input->post('name', TRUE);
        $usia = $this->input->post('usia', TRUE);
		$tunjangan = $this->input->post('tunjangan', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
			'kode' => $kode,
            'name' => $nama,
            'tunjangan' => $tunjangan,
            'usia' => $usia,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->eselons, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function eselon_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->eselons, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD ESELONS END//


    //CRUD UMUM START//
    public function get_umum(){
        $get = $this->Standartunjangan_model->get_umum();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->golongan .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'golongan' => $klik_kode,
                'tunjangan' => number_format($value->tunjangan),
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(

            "recordsTotal" => $this->Standartunjangan_model->umum_count_all(),
            "recordsFiltered" => $this->Standartunjangan_model->umum_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function umum(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Tunjangan - Umum';
        $data['breadcrumb'] =
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Tunjangan - Umum</span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/standartunjangan/umum/index', $data);
    }

    public function get_detail_umum(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standartunjangan_model->getUmumById($id);
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function umum_update_action(){
        $id = $this->input->post('id', TRUE);
		$golongan = $this->input->post('golongan', TRUE);
		$tunjangan = $this->input->post('tunjangan', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'golongan' => $golongan,
            'tunjangan' => $tunjangan,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->umums, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function umum_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->umums, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD UMUM END//

    //CRUD UMUM P3K START//
    public function get_umump3k(){
        $get = $this->Standartunjangan_model->get_umump3k();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->golongan .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'golongan' => $klik_kode,
                'tunjangan' => number_format($value->tunjangan),
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(

            "recordsTotal" => $this->Standartunjangan_model->umump3k_count_all(),
            "recordsFiltered" => $this->Standartunjangan_model->umump3k_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function umump3k(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Tunjangan - Umum P3K';
        $data['breadcrumb'] =
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Tunjangan - Umum P3K</span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/standartunjangan/umump3k/index', $data);
    }

    public function get_detail_umump3k(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standartunjangan_model->getUmump3kById($id);
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function umump3k_update_action(){
        $id = $this->input->post('id', TRUE);
		$golongan = $this->input->post('golongan', TRUE);
		$tunjangan = $this->input->post('tunjangan', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'golongan' => $golongan,
            'tunjangan' => $tunjangan,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->umump3ks, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function umump3k_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->umump3ks, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD UMUM P3K END//

    public function fungsional(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Tunjangan - Fungsional';
        $data['breadcrumb'] =
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Tunjangan - Fungsional</span>';
        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/standartunjangan/fungsional/index', $data);
    }

    public function get_fungsional(){
      $get = $this->Standartunjangan_model->get_fungsional();
      $datatable = array();

      $no = 0;
      foreach ($get as $key => $value) {
        $no ++;
        $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';

        $datatable[$key] = array(
          'no' => $no,
          'nama' => $klik_kode,
          'fungsional_kode' => $value->fungsional_kode,
          'tunjangan' => number_format($value->tunjangan),
          'usia' => $value->usia,
        );
      }

      $output = array(
        "recordsTotal" => $this->Standartunjangan_model->fungsional_count_all(),
        "recordsFiltered" => $this->Standartunjangan_model->fungsional_count_filtered(),
        "data" => $datatable,
      );
      // output to json format
      echo json_encode($output);
    }

    public function get_detail_fungsional(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standartunjangan_model->getFungsionalById($id);
        if($data != null){
            $response = array(
              'status' => 1,
              'message' => 'Data berhasil ditemukan',
              'data' => $data
            );
        } else {
            $response = array(
              'status' => 0,
              'message' => 'Data gagal ditemukan',
              'data' => null
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function fungsional_update_action(){
      $id = $this->input->post('id', TRUE);
      $nama = $this->input->post('nama', TRUE);
      $fungsional_kode = $this->input->post('fungsional_kode', TRUE);
      $tunjangan = $this->input->post('tunjangan', TRUE);
      $usia = $this->input->post('usia', TRUE);

      $userid = $this->session->userdata('id');
      $now = date('Y-m-d H:i:s');

      $data = array(
          'name' => $nama,
          'fungsional_kode' => $fungsional_kode,
          'tunjangan' => $tunjangan,
          'usia' => $usia,
          'updated_at' => $now,
          'updated_by' => $userid
      );

      $upd = $this->Fungsi_model->edit($this->fungsionals, $id, $data);
      if ($upd) {
        $reponse = array(
            'status' => 1,
            'message' => 'Berhasil menghapus data'
        );
      } else {
        $reponse = array(
            'status' => 0,
            'message' => 'Gagal menghapus data'
        );
      }

      header('Content-Type: application/json');
      echo json_encode($reponse);
    }

    public function fungsional_hapus_action(){
      $id = $this->input->post('id', TRUE);
      $now = date('Y-m-d H:i:s');

      $data = array(
          'deleted_at' => $now,
          'deleted_by' => $id
      );

      $hps = $this->Fungsi_model->hapus($this->fungsionals, $id, $data);
      if($hps){
          $reponse = array(
              'status' => 1,
              'message' => 'Berhasil menghapus data'
          );
      } else {
          $reponse = array(
              'status' => 0,
              'message' => 'Gagal menghapus data'
          );
      }

      header('Content-Type: application/json');
      echo json_encode($reponse);
    }

    //START KEDUDUKAN
    public function kedudukan(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Tunjangan - Kedudukan';
        $data['breadcrumb'] =
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Tunjangan - Kedudukan</span>';
        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/standartunjangan/kedudukan/index', $data);
    }

    public function get_kedudukan(){
        $postData = $this->input->post();

        $data = $this->Standartunjangan_model->getDataKedudukan($postData);

        echo json_encode($data);
    }

    public function get_detail_kedudukan(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Standartunjangan_model->getKedudukanById($id);
        if($data != null){
            $response = array(
                'status' => 1,
                'message' => 'Data berhasil ditemukan',
                'data' => $data
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Data gagal ditemukan',
                'data' => null
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function kedudukan_update_action(){
        $id = $this->input->post('id', TRUE);// id simgaji_tjfungsionals 
        $nama = $this->input->post('nama', TRUE);
        // $fungsional_kode = $this->input->post('fungsional_kode', TRUE);
        $tunjangan = $this->input->post('tunjangan', TRUE);
        $usia = $this->input->post('usia', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        //simgaji_tjfungsionals
        $data = array(
            // 'fungsional_kode' => $fungsional_kode,
            'tunjangan' => $tunjangan,
        );

        $upd = $this->Fungsi_model->edit('simgaji_tjfungsionals', $id, $data);
        if ($upd) {
            //simgaji_kedudukans
            $data_kedudukan = array(
                'name' => $nama,
                'usia' => $usia,
                'updated_at' => $now,
                'updated_by' => $userid
            );

            $id_kedudukan = $this->Standartunjangan_model->getIdTableKedudukan($id)->id;
            $this->Fungsi_model->edit('simgaji_kedudukans', $id_kedudukan, $data_kedudukan);
            
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function kedudukan_hapus_action(){
        $id = $this->input->post('id', TRUE);// id simgaji_tjfungsionals 
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        //simgaji_kedudukans
        $data_kedudukan = array(
            'deleted_at' => $now,
            'deleted_by' => $userid
        );

        $id_kedudukan = $this->Standartunjangan_model->getIdTableKedudukan($id)->id;
        $upd = $this->Fungsi_model->edit('simgaji_kedudukans', $id_kedudukan, $data_kedudukan);

        if ($upd) {
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function kedudukan_add_action(){
        $kode = $this->input->post('kode', TRUE);
        $golongan = $this->input->post('golongan', TRUE);
        $name = $this->input->post('nama', TRUE);
        $tunjangan = $this->input->post('tunjangan', TRUE);
        $usia = $this->input->post('usia', TRUE);
        $jenis = $this->input->post('jenis', TRUE);
        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        //simgaji_kedudukans
        $data = array(
            'kode' => $kode,
            'name' => $name,
            'usia' => $usia,
            'jenis' => $jenis,
            'created_at' => $now,
            'created_by' => $userid
        );

        $id_kedudukan = $this->Standartunjangan_model->tambahKembaliId('simgaji_kedudukans', $data);

        // $data_updkode = array(
        //     'kode' => $kode
        // );

        // $upd = $this->Fungsi_model->edit('simgaji_kedudukans', $id_kedudukan, $data_updkode);

        $data_tjfungsional = array(
            'kedudukan_id' => $id_kedudukan,
            'golongan' => $golongan,
            'tunjangan' => $tunjangan
        );

        $this->Fungsi_model->tambah('simgaji_tjfungsionals', $data_tjfungsional);

        $reponse = array(
            'status' => 1,
            'message' => 'Berhasil memperbaharui data'
        );

        // if ($upd) {

        //     $data_tjfungsional = array(
        //         'kedudukan_id' => $id_kedudukan,
        //         'golongan' => $golongan,
        //         'tunjangan' => $tunjangan
        //     );

        //     $this->Fungsi_model->tambah($this->fungsionals, $data_tjfungsional);

        //     $reponse = array(
        //         'status' => 1,
        //         'message' => 'Berhasil memperbaharui data'
        //     );
        // } else {
        //     $reponse = array(
        //         'status' => 0,
        //         'message' => 'Gagal memperbaharui data'
        //     );
        // }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    // END KEDUDUKAN
}
