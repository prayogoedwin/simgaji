<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Statusdata extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
        	redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Statusdata_model');
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();

        $get = $this->Statusdata_model->getData();

        $datatable = array();
        foreach ($get as $key => $value) {

            $text_induk = '';
            if ($value->bool_id == 2) {
                $text_induk = 'Ya';
            } else {
                $text_induk = 'Tidak';
            }

            // $gaji = form_dropdown("status_gaji[]", statusdata_array("-"), $value->status_gaji, "id=\"status_gaji\" class=\"form-control \"   required ");
            // $tpp = form_dropdown("status_tpp[]", statusdata_array("-"), $value->status_tpp, "id=\"status_gaji\" class=\"form-control \"   required ");
            // $gaji_v = form_dropdown("status_gaji_verifikator[]", statusverif_array("-"), $value->status_gaji_verifikator, "id=\"status_gaji_verifikator\" class=\"form-control \"   required ");
            // $tpp_v = form_dropdown("status_tpp_verifikator[]", statusverif_array("-"), $value->status_tpp_verifikator, "id=\"status_tpp_verifikator\" class=\"form-control \"   required ");

            $gaji = statusdata_nama($value->status_gaji);
            $tpp = statusdata_nama($value->status_tpp);
            $gaji_v = statusverif_nama($value->status_gaji_verifikator);
            $tpp_v = statusverif_nama($value->status_tpp_verifikator);

            $id = '<input type="checkbox" name="lokasi_id[]" value="' . $value->id . '" class="check_data"/>';


            $datatable[$key] = array(
                'id' => $id,
                'nama' => $value->name,
                'status_gaji' => $gaji,
                'status_tpp' => $tpp,
                'status_gaji_verifikator' => $gaji_v,
                'status_tpp_verifikator' => $tpp_v
            );
        }
        $data['datatable'] = $datatable;

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/statusdata/statusdata_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function tambah()
    {
        $data_header['session'] = $this->session->all_userdata();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/statusdata/statusdata_tambah');
    }

    public function tambah_action()
    {
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $is_induk = $this->input->post('is_induk', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'bool_id' => $is_induk,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $insert = $this->Fungsi_model->tambah('simgaji_statusdatas', $data);
        if ($insert) {
            $array_msg = array(
                'status' => 'success',
                'message' => 'Berhasil menambahkan data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('statusdata');
        } else {
            $array_msg = array(
                'status' => 'error',
                'message' => 'Gagal menambahkan data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('statusdata');
        }

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function update_action()
    {
        $idstatusdata = $this->input->post('idstatusdata', TRUE);
        $kode = $this->input->post('kode', TRUE);
        $name = $this->input->post('name', TRUE);
        $is_induk = $this->input->post('is_induk', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'kode' => $kode,
            'name' => $name,
            'bool_id' => $is_induk,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $insert = $this->Fungsi_model->edit('simgaji_statusdatas', $idstatusdata, $data);
        if ($insert) {
            $array_msg = array(
                'status' => 'success',
                'message' => 'Berhasil memperbaharui data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('statusdata');
        } else {
            $array_msg = array(
                'status' => 'error',
                'message' => 'Gagal memperbaharui data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('statusdata');
        }

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function hapus_action()
    {
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus('simgaji_statusdatas', $id, $data);
        if ($hps) {
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function getDetailStatus()
    {
        $id = $this->input->post('id', TRUE);

        $data = $this->Statusdata_model->getDataById($id);
        if ($data != null) {
            $response = array(
                'status' => 1,
                'message' => 'Data berhasil ditemukan',
                'data' => $data
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Data gagal ditemukan',
                'data' => null
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function update_bulk(){

        $lok = $this->input->post('lokasi_id');

        if($lok != ''){  
            $status_gaji = $this->input->post('status_gaji');
            $status_tpp = $this->input->post('status_tpp');
            $gaji_verif = $this->input->post('gaji_verif');
            $tpp_verif = $this->input->post('tpp_verif');
            for($x = 0; $x < sizeof($lok); $x++){
                $reslt[$x] = array(
                    "id"    => $lok[$x],
                    "status_gaji"  => $status_gaji,
                    "status_tpp"  => $status_tpp,
                    "status_gaji_verifikator"  => $gaji_verif,
                    "status_tpp_verifikator"  => $tpp_verif,
                    );
                }
            $res = $this->db->update_batch('simgaji_lokasis', $reslt, 'id');
            // echo json_encode($res);
            // die();
            if($res){
                $response = array(
                    'status' => 'status',
                    'message' => 'Berhasil Update Data',
                    'data' => null
                );
            }else{
                $response = array(
                    'status' => 'error',
                    'message' => 'Gagal Update Data A',
                    'data' => null
                );
            }
        }else{
            $response = array(
                'status' => 'error',
                'message' => 'Gagal Update Data',
                'data' => 'Silahkan klik checkbox terlebih dahulu S'
            );
        }

        $this->session->set_flashdata($response);
        redirect('statusdata');
       

        // echo json_encode($reslt);

    }
}
