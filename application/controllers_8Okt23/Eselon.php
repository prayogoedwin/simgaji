<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eselon extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();
		$this->eselons = 'eselons a';

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Eselon_model');
	}

    // public function index(){
    //     redirect('dashboard');
    // }

	//CRUD ESELONS START//
    public function get_eselon(){
        $get = $this->Eselon_model->get_eselon();
        $datatable = array();
		foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';
			$datatable[$key] = array(
				'id' => $value->id,
                'kode' => $value->kode,
				'name' => $klik_kode,
                'tunjangan' => number_format($value->tunjangan),
				'usia' => $value->usia,
			);
		}
		    // $data['datatable'] = $datatable;

            $output = array(
       
            "recordsTotal" => $this->Eselon_model->eselon_count_all(),
            "recordsFiltered" => $this->Eselon_model->eselon_count_filtered(),
            "data" => $datatable,
            );
            // output to json format
            echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Standar Tunjangan - Eselon';
        $data['breadcrumb'] = 
        '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
        <span class="breadcrumb-item active">Standar Tunjangan - Eselon</span>';
        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/eselon/index', $data);
    }

    public function get_detail_eselon(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Eselon_model->getStrukturalById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function eselon_update_action(){
        $id = $this->input->post('id', TRUE);
		$kode = $this->input->post('kode', TRUE);
        $nama = $this->input->post('name', TRUE);
        $usia = $this->input->post('usia', TRUE);
		$tunjangan = $this->input->post('tunjangan', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
			'kode' => $kode,
            'name' => $nama,
            'tunjangan' => $tunjangan,
            'usia' => $usia,
            'updated_at' => $now,
            'updated_by' => $userid
        );

        $insert = $this->Fungsi_model->edit($this->eselons, $id, $data);
        if ($insert) {
            $data = TRUE;
        } else {
            $data = FALSE;
        }

		header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function eselon_hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now,
            'deleted_by' => $id
        );

        $hps = $this->Fungsi_model->hapus($this->eselons, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }
    //CRUD ESELONS END//

}
?>