<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Potongan extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Potongan_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        // $get = $this->Potongan_model->getData();

        // $datatable = array();
		// foreach ($get as $key => $value) {

        //     $klik_gol = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->golongan .'</a>';

		// 	$datatable[$key] = array(
		// 		'gol' => $klik_gol,
		// 		'potongan' => $value->potongan
		// 	);
		// }
		// $data['datatable'] = $datatable;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/potongan/potongan_index');

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    //FOR SERVERSIDE
    public function getData(){
        $get = $this->Potongan_model->getData();

        $datatable = array();
		foreach ($get as $key => $value) {

            $klik_gol = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->golongan .'</a>';

			$datatable[$key] = array(
                'num' => $key + 1,
				'gol' => $klik_gol,
				'potongan' => number_format($value->potongan)
			);
		}

        $output = array(
            "recordsTotal" => $this->Potongan_model->getDataCount(),
            "recordsFiltered" => $this->Potongan_model->getDataCountFiltered(),
            "data" => $datatable,
        );

        // output to json format
        header('Content-Type: application/json');
        echo json_encode($output);
    }

    public function tambah(){
        $data_header['session'] = $this->session->all_userdata();

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('master/potongan/potongan_tambah');
    }

    public function tambah_action(){
        $golongan = $this->input->post('golongan', TRUE);
        $potongan = $this->input->post('potongan', TRUE);

        $userid = $this->session->userdata('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'golongan' => $golongan,
            'potongan' => $potongan,
            'posted_by' => $userid,
            'created_at' => $now
        );

        $insert = $this->Fungsi_model->tambah('simgaji_potongans', $data);
        if ($insert) {
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil menambahkan data'
            );
            $this->session->set_flashdata($array_msg);
            redirect('potongan');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal menambahkan data'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('potongan');
        }

		// header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function update_action(){
        $id = $this->input->post('id', TRUE);
        $golongan = $this->input->post('golongan', TRUE);
        $potongan = $this->input->post('potongan', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'golongan' => $golongan,
            'potongan' => $potongan,
            'updated_at' => $now
        );

        $upd = $this->Fungsi_model->edit('simgaji_potongans', $id, $data);
        if ($upd) {
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil diperbaharui'
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal diperbaharui'
			);
        }

		header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus('simgaji_potongans', $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

    public function getDetailPotongan(){
        $id = $this->input->post('id', TRUE);

        $data = $this->Potongan_model->getDataById($id);   
        if($data != null){
            $response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $data
			);
        } else {
            $response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => null
			);
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
}