<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulasi extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');

		// $this->load->model('Fungsi_model');
		

        $this->tb_lokasi = 'simgaji_lokasis';
        $this->tb_kalkulasi = 'simgaji_kalkulasis';
		$this->tb_kalkulasip3k = 'simgaji_kalkulasip3ks';
		$this->tb_kalkulasip3k_tb = 'simgaji_kalkulasip3ks_tb';
		$this->tb_kalkulasip3k_eb = 'simgaji_kalkulasip3ks_eb';

        $this->tb_pegawai = 'simgaji_pegawais';
		$this->tb_pegawaip3k = 'simgaji_pegawaip3ks';
		$this->tb_pegawaip3k_13 = 'simgaji_pegawaip3k_13';
		$this->tb_pegawaip3k_14 = 'simgaji_pegawaip3k_14';

		$this->tb_tjumum = 'simgaji_tjumums';
		$this->tb_tjumump3k = 'simgaji_tjumump3ks';
		$this->tb_tjfungsional = 'simgaji_tjfungsionals';
		$this->tb_fungsional = 'simgaji_fungsionals';
		$this->tb_eselon = 'simgaji_eselons';
		$this->tb_kedudukan = 'simgaji_kedudukans';
		$this->tb_golongan = 'simgaji_golongans';
		$this->tb_golonganp3k = 'simgaji_golonganp3ks';
		$this->tb_status = 'simgaji_statuses';
		$this->tb_statusp3k = 'simgaji_statusp3ks';
		$this->tb_marital = 'simgaji_maritals';
		$this->tb_beras = 'simgaji_berases';

	}

    public function index(){
		$data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Kalkulasi';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Kalkulasi</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
		$this->load->view('kalkulasi/kalkulasi_index', $data);
    }

	function pph($istri, $anak, $pen_tht, $gaji_kotor_pembulatan,$nip){
		if($gaji_kotor_pembulatan < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
		} 
		else {
			$tambahan = 0;
		}
		
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
		
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12 ;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	////pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		$pph_bulan	 = floor($pph_tahun/12) ;
		
		return $pph_bulan ;
	}

	function pph_13($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip)
	{
		if ($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}

		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;

		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}

		$pen_tht_tahun = $pen_tht * 12;

		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;

		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}

		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}

		if ($penghasilan_kpj_13 < 0) {
			$penghasilan_kpj_13 = 0;
		}

		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13, "000", -3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		// PPH Biasa
		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}

		// PPH 13
		if ($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		} elseif ($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		} elseif ($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj_13 - 250000000));
		} elseif ($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj_13 - 500000000));
		}

		return $pph_tahun_13 - $pph_tahun;
	}

	function pph_14($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip)
	{
		if ($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}

		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;

		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}

		$pen_tht_tahun = $pen_tht * 12;
		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;

		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}

		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}

		if ($penghasilan_kpj_13 < 0) {
			$penghasilan_kpj_13 = 0;
		}

		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13, "000", -3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		// PPH Biasa
		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}

		// PPH 13
		if ($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		} elseif ($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		} elseif ($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj_13 - 250000000));
		} elseif ($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj_13 - 500000000));
		}

		/*if($nip == '196002291986031004') {
			echo $bea_jabatan."=".$bea_jabatan_13."=";
			echo $kurang_gaji."=".$penghasilan_bulan."=@";
			echo $ptkp."=";
			echo $penghasilan_tahun."=".$penghasilan_kpj."=".$penghasilan_kpj_13."=#";
			echo $pph_tahun."=".$pph_tahun_13."=";
			echo $pph_tahun_13 - $pph_tahun;
			
			die();
		}*/

		return $pph_tahun_13 - $pph_tahun;
	}
    

    public function action(){

      
		
        $lokasi_start = $_POST['lokasi_start'];
		$lokasi_end =$_POST['lokasi_end'];

		

        if($_POST['jenis'] != "GAJI_SUSULAN") {
			$periode = $_POST['tahun'];
			
			if(isset($_POST['bulan_id'])) {	
				$periode .= "-".$_POST['bulan_id']."-01";
			}
			
			$arrLokasi = array();
			if($_POST['lokasi_end']) {

				

				// echo $lokasi_start;
				// echo "<br/>";
				// echo $lokasi_end;
				// die();

                if(substr($lokasi_end,-3) == "000"){
                    $this->db->where('LEFT(kode,2)',substr($lokasi_end,0,2));
					// $this->db->like('kode', substr($lokasi_start,0,2));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
					// echo $max->kode;
					// die();

                }else{
                    $this->db->where('LEFT(kode,6)',substr($lokasi_end,0,6));
					// $this->db->like('kode', substr($lokasi_start,0,6));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
                }

                $this->db->where('kode >=', $lokasi_start);
                $this->db->where('kode <=', $max->kode);
                $lokasis = $this->db->get($this->tb_lokasi)->result();
					
				foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

				// echo json_encode($arrLokasi);
				// die();	

			} else {

				

                if(substr($lokasi_start,-4) == "0000") {

                    if(substr($lokasi_start,0,2) == "34") {

                        $this->db->where('LEFT("kode",2) =', 34);
                        $this->db->where('LEFT("kode",4) !=', 3411);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
						

					} else {
			
						$this->db->where('LEFT(kode,2) =', substr($lokasi_start,0,2));
						// $this->db->where('LEFT(kode,2) =',substr($lokasi_start,0,2),true);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
						// echo json_encode($lokasis);
						// die();
					}


                }else{

                    $this->db->where('LEFT("kode",6) =', substr($lokasi_start,0,6));
                    $lokasis = $this->db->get($this->tb_lokasi)->result();
				

                }

                foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

			
				// echo json_encode($arrLokasi);
				// die();
				
				
			}
		}

		



        if($_POST['jenis'] == "GAJI_BULANAN") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasi);
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gaji_bulanan($periode,$arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_BULANAN_PPPK") {

			// print_r($arrLokasi);
			// die();	
            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k);
				
			
		  	$this->gajip3k_bulanan($periode,$arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_13_PPPK") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k_tb);
				
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_13($periode, $arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_14_PPPK") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k_eb);
				
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_14($periode, $arrLokasi);
		}
    }

	function gaji_bulanan($periode, $arrLokasi){							
		error_reporting(0);
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$this->db->select($this->tb_pegawai.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golongan.'.kode as golongan_kode');
		$this->db->select($this->tb_status.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawai);
		$this->db->join($this->tb_eselon, $this->tb_pegawai.'.eselon_id ='.$this->tb_eselon.'.id');
        $this->db->join($this->tb_kedudukan, $this->tb_pegawai.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawai.'.fungsional_id ='.$this->tb_fungsional.'.id');
		$this->db->join($this->tb_golongan, $this->tb_pegawai.'.golongan_id ='.$this->tb_golongan.'.id');
		$this->db->join($this->tb_status, $this->tb_pegawai.'.status_id ='.$this->tb_status.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawai.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawai.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
        $this->db->where_in('lokasi_gaji', $arrLokasi);
        $this->db->where_in('status_id', array(1,2,7,8,9));
        $pegawais = $this->db->get()->result();
        
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;

			// $this->db->where('id', $pegawai->eselon_id);
			// $eselon_ = $this->db->get($this->tb_eselon)->row();

			// $this->db->where('id', $pegawai->fungsional_id);
			// $fungsional_ = $this->db->get($this->tb_fungsional)->row();

			// $this->db->where('id', $pegawai->kedudukan_id);
			// $kedudukan_ = $this->db->get($this->tb_kedudukan)->row();
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan_name	;	
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional_name;
				}
				else {
					if($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name	;	
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

					
					//$tunjangan_fungsional = 0;
				} else {
					
					if($pegawai->kedudukan_id != 72) {

						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();

					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}

				
				// Kondisi Khusus untk Tunjangan Sandiman, Nominal Tunj Fungs dipindahkan ke Nominal $tunjangan_umum_tambahan di cetakan disebut sbg Tunj Kompensasi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) { //Jika Ahli Sandi tingkat 1-6
					if($pegawai->kompensasi_id >= 879 AND $pegawai->kompensasi_id <= 891) { //Jika Sandiman ( tunjangan kompensasi pemula-madya) dan merupakan JFT
						//Diisi tunjangan funstionalnya sesuai nominal
						//Diisi tunjangan kompensasinya di tunj umum tambahan
						//Dinolkan tunjangan umumnya karena merupakan fungsional umum
					

						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						
						
					$tunjangan_umum = 0;
					} else { //Kalau yang ini fungsional tertentu (JFT) tapi bukan sandiman
						$tunjangan_fungsional = 0;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						}
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 

				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// $jumlah_potongan = round($potongan_lain + $potongan_iwp);
				// $gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum + $tunjangan_umum_tambahan);
				// $gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// $jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras + $tunjangan_umum_tambahan);
				// $jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasi, $arrFieldVal);

			}
			
		}

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi'. $periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');

		
	}

	function gajip3k_bulanan($periode, $arrLokasi)
	{

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 6, 1);


		if($this_period == $periode){
			$this->tb_pegawaip3k = $this->tb_pegawaip3k;
		}else{
			$this->tb_pegawaip3k = $this->tb_pegawaip3k.'_'.$year_search.'_'.$month_search;
		}


		if ( !$this->db->table_exists($this->tb_pegawaip3k) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}
		
	
		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','198102072010011014')
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		$pegawaip3ks = $this->db->get()->result();
		// echo json_encode($pegawaip3ks);
		// die();

		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();
						
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/


				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				//$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
				$potongan_pensiun = 0;

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;

				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				// echo '<br/>';
				// echo 'Gaji kotor pembulatan: '.$gaji_kotor_pembulatan;
				// echo '<br/>';
				// echo 'Jumlah Bersih: '.$jumlah_bersih;
				// echo '<br/>';
				// echo 'PPH: '.$pph_bulan;
				// echo '<br/>';
				// echo 'Jumlah: '.$jumlah_bersih_bayar;
				// die();


				//echo $gaji_pokok;
				//die();

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/

				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);

			}
		}

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}

	function gajip3k_13($periode, $arrLokasi)
	{
		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 6, 1);


		if($this_period == $periode){
			$this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13;
		}else{
			$this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13.'_'.$year_search.'_'.$month_search;
		}


		if ( !$this->db->table_exists($this->tb_pegawaip3k_13) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}

		// echo $this->tb_pegawaip3k_13;
		// die();


		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k_13.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k_13);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k_13.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k_13.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k_13.'.fungsional_id ='.$this->tb_fungsional.'.id', 'LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k_13.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k_13.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k_13.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k_13.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$pegawaip3ks = $this->db->get()->result();

		// $pegawaip3ks = ORM::factory('pegawaitbp3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	->find_all();

		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
						$this->db->where('golongan', $pegawai->golongan_id[0]);
						$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;


					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
						$this->db->where('golongan', $pegawai->golongan_id[0]);
						$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$askes = 0;

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// Beras
				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				$potongan_bpjs_kesehatan = 0;
				$potongan_pensiun = 0;
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if ($bea_jabatan > 500000) {
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;


				// Tanpa Beras				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				// Dengan Beras
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				$gaji_kotor_13	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				#$gaji_bersih_13 = round($gaji_kotor - $jumlah_potongan);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				// Tanpa Beras
				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				// Dengan Beras
				$pembulatan_13	= 0;
				if (substr($gaji_bersih_13, -2) != "00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				$pph_bulan = $this->pph_13($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;

				$pembulatan	= 0;
				if (substr($jumlah_k, -2) != "00") {
					$pembulatan = 100 - substr($jumlah_k, -2);
				}

				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k_tb, $arrFieldVal);
			}
		}

		

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasitbp3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
	

	$array_msg = array(
		'status'=>'success',
		'message'=>'Berhasil melakukan Kalkulasi 13 Periode '.$periode
	);
	
	$this->session->set_flashdata($array_msg);
	redirect('kalkulasi');

	}

	function gajip3k_14($periode, $arrLokasi)
	{
		// $this->auto_render = false;

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 6, 1);

		if($this_period == $periode){
			$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14;
		}else{
			$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14.'_'.$year_search.'_'.$month_search;
		}

		if ( !$this->db->table_exists($this->tb_pegawaip3k_14) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}


		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k_14.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k_14);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k_14.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k_14.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k_14.'.fungsional_id ='.$this->tb_fungsional.'.id', 'LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k_14.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k_14.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k_14.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k_14.'.lokasi_gaji ='.$this->tb_lokasi.'.id', 'LEFT');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$pegawaip3ks = $this->db->get()->result();

		// $pegawaip3ks = ORM::factory('pegawaiebp3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	->find_all();



		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$askes = 0;

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// Beras
				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/

				$potongan_bpjs_kesehatan = 0;
				$potongan_pensiun = 0;
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;


				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if ($bea_jabatan > 500000) {
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// Biasa				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				// 13
				$potongan_lain = 0;
				$potongan_iwp = 0;
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				#edy
				#$gaji_kotor_13 = $gaji_pokok;
				#gaji ke 14 / THR th 2018 dibayarkan tj jab & tj keluarga
				$gaji_kotor_13 = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				// Tanpa Beras
				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp;
					die();
				}*/

				// Dengan Beras
				$pembulatan_13	= 0;
				if (substr($gaji_bersih_13, -2) != "00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;

				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_kotor_pembulatan."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp."=";
					echo $gaji_kotor_13;
					die();
				}*/

				$pph_bulan = $this->pph_14($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
				#edy				
				#$jumlah_kotor = $gaji_pokok + $pph_bulan;
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5;
				$jumlah_k = $jumlah_kotor;
				#$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				#$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				$pembulatan	= 0;
				#if(substr($gaji_pokok,-2)!="00") {
				#	$pembulatan = 100 - substr($gaji_pokok,-2);
				#}
				if (substr($jumlah_k, -2) != "00") {
					$pembulatan = 100 - substr($jumlah_k, -2);
				}

				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;

				#$tunjangan_anak = $tunjangan_istri = $tunjangan_beras = 0;
				#$tunjangan_umum = $tunjangan_fungsional = $tunjangan_struktural = 0;
				$potongan_iwp = $potongan_lain = $jumlah_potongan = 0;

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k_eb, $arrFieldVal);
			}
		}

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasiebp3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi 14 Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	
	}





}