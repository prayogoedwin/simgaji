<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pot_pegawai extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_pegawai_p3k_model');
		$this->load->model('Tpp_pot_pegawai_model');

        $role = $this->session->userdata('role');
        if( $role != 3 && $role != 8){
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('dashboard').'";</script>';       
        }
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
        $A_01 = $this->session->userdata('A_01');
        $role = $this->session->userdata('role');
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;
        $kode_lokasis = getDetailLokasisById($lokasis_id)->kode;

        $kolok_simpeg = getDetailLokasisById($lokasis_id)->kolok_simpeg;
        
        $periodetpp = getDataPeriodeTPP()->tahun . '-' . getDataPeriodeTPP()->bulan . '-01';

        $data['periodetpp_simpeg'] = getStatusPeriodeTppSimpeg($A_01, getDataPeriodeTPP()->bulan, getDataPeriodeTPP()->tahun);
        // echo json_encode($data);
        // die();

        if($A_01 != 'D0'){
            $get = $this->Tpp_pot_pegawai_model->getDataPegawaiByKode($kode_lokasis, $kolok_simpeg, $periodetpp);

            $datatable = array();
            foreach ($get as $key => $value) {
                
                $jab = '-';
                if($value->I_JB != null){
                    $jab = $value->I_JB;
                }
    
                $klsjab = '-';
                if($value->kelasjab != null){
                    $klsjab = $value->kelasjab;
                }
    
                // $rmpn = '-';
                // if($value->RUMPUN != null){
                //     $rmpn = $value->RUMPUN;
                // }
    
                $vbebankhusus = 0;
                if($value->beban != null){
                    $vbebankhusus = $value->beban;
                }
    
                $vtempat = 0;
                if($value->tempat != null){
                    $vtempat = $value->tempat;
                }
    
                $vkondisi = 0;
                if($value->kondisi != null){
                    $vkondisi = $value->kondisi;
                }
    
                $datatable[$key] = array(
                    'B_02B' => $value->nip,
                    'nama' => $value->nama,
                    'jabatan' => $jab,
                    // 'pkt_gol' => getDataPangkatGol($value->F_03)->NAMAX,
                    'pkt_gol' => $value->golongan_id,
                    'cuti_2bulanlalu' => hitungCSByNip($value->nip),
                    'tb' => $value->tb,
                    'kelas_jabatan' => $klsjab,
                    // 'rumpun' => $rmpn,
                    // 'F_03' => $value->F_03,
                    'F_03' => 'ini f03',
                    // 'alpha' => $value->alpha,
                    'alpha' => 0,
                    // 'kwk' => $value->kwk,
                    'kwk' => 0.0,
                    'tingkat_hukdis' => $value->tingkat,
                    'uraian' => $value->uraian,
                    'kena_hk' => $value->kenahk,
                    'lhkpn' => $value->lhkpn,
                    'lhkasn' => $value->lhkasn,
                    'tptgr' => $value->tptgr,
                    'kenabmd' => $value->kenabmd,
                    'kenagrat' => $value->kenagrat,
                    'xbmd' => $value->xbmd,
                    'xgrat' => $value->xgrat,
                    'nominal_bebankerja' => $value->tpp, //basetpp
                    'nominal_bebankerjakhusus' => $vbebankhusus,
                    'nominal_tempat' => $vtempat,
                    'nominal_kondisi' => $vkondisi,
                );
            }
            $data['pegawais'] = $datatable;
        } else {
            //disdik
            $data['subunit_disdik'] = $this->Tpp_pot_pegawai_model->get_uptdisdik()->result();

            if(isset($_GET['kode'])){
                $kd = $this->input->get('kode', TRUE);
                $get = $this->Tpp_pot_pegawai_model->getDataPegawaiKhususDisdikByKode($kd, $periodetpp);

                $datatable = array();
                foreach ($get as $key => $value) {
                    
                    $jab = '-';
                    if($value->I_JB != null){
                        $jab = $value->I_JB;
                    }
        
                    $klsjab = '-';
                    if($value->kelasjab != null){
                        $klsjab = $value->kelasjab;
                    }
        
                    $rmpn = '-';
                    // if($value->RUMPUN != null){
                    //     $rmpn = $value->RUMPUN;
                    // }
        
                    $vbebankhusus = 0;
                    if($value->beban != null){
                        $vbebankhusus = $value->beban;
                    }
        
                    $vtempat = 0;
                    if($value->tempat != null){
                        $vtempat = $value->tempat;
                    }
        
                    $vkondisi = 0;
                    if($value->kondisi != null){
                        $vkondisi = $value->kondisi;
                    }
        
                    $datatable[$key] = array(
                        'B_02B' => $value->nip,
                        'nama' => $value->nama,
                        'jabatan' => $jab,
                        // 'pkt_gol' => getDataPangkatGol($value->F_03)->NAMAX,
                        'pkt_gol' => $this->Tpp_pot_pegawai_model->getRowGolongans($value->golongan_id)->kode,
                        // 'cuti_2bulanlalu' => hitungCSByNip($value->nip),
                        'cuti_2bulanlalu' => $value->alpha,
                        'tb' => $value->tb,
                        'kelas_jabatan' => $klsjab,
                        'rumpun' => $rmpn,
                        // 'F_03' => $value->F_03,
                        'F_03' => $value->golongan_id,
                        // 'alpha' => $value->alpha,
                        'alpha' => 0,
                        // 'kwk' => $value->kwk,
                        'kwk' => 0.0,
                        'tingkat_hukdis' => $value->tingkat,
                        'uraian' => $value->uraian,
                        'kena_hk' => $value->kenahk,
                        'lhkpn' => $value->lhkpn,
                        'lhkasn' => $value->lhkasn,
                        'tptgr' => $value->tptgr,
                        'kenabmd' => $value->kenabmd,
                        'kenagrat' => $value->kenagrat,
                        'xbmd' => $value->xbmd,
                        'xgrat' => $value->xgrat,
                        'nominal_bebankerja' => $value->tpp, //basetpp
                        'nominal_bebankerjakhusus' => $vbebankhusus,
                        'nominal_tempat' => $vtempat,
                        'nominal_kondisi' => $vkondisi,
                    );
                }
                $data['pegawais'] = $datatable;

                $data['filter'] = array(
                    'kodedrop' => $kd
                );
            } else {
                $data['pegawais'] = array();

                $data['filter'] = array(
                    'kodedrop' => null
                );
            }

            // header('Content-Type: application/json');
            // echo json_encode($data);
        }

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('tpp_pot_pegawai/tpp_pot_pegawai_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function data_pegawai($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawaip3ks_tpp', $real_id);
        if($cek){
            // echo 'lanjut';

            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_pegawai_p3k_model->getDetailById($real_id);
            $data['kodes'] = $this->Tpp_pegawai_p3k_model->getDataKodes();

            $data['lokasi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

            $data['fungsional'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_profesis');

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai_p3k/data_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        }
    }

    public function detail($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawaip3ks_tpp', $real_id);
        if($cek){
            $jns = $this->input->get('jns', TRUE);

            $data['ekasus'] = array(
                'kode_hot' => 0,
                'psn' => '--'
            );

            $data['warning'] = array(
            '   pesan' => 'NIP tidak ditemukan di SIMPEG, silahkan hubungi admin'
            );

            $nip_ybs = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->nip;
            $cekNip = getDataMastfip($nip_ybs);
            if($cekNip != null){
                $data['warning'] = array(
                    'pesan' => ''
                );
            }

            if($jns == '1' || $jns == '2'){
                $cekKasus = $this->Tpp_pegawai_p3k_model->ekasus($nip_ybs);
                if($cekKasus->hot > 0){
                    $data['ekasus'] = array(
                        'kode_hot' => $cekKasus->hot,
                        'psn' => 'PNS bersangkutan terdaftar dalam sistem e - kasus'
                    );
                } else {
                    $data['ekasus'] = array(
                        'kode_hot' => $cekKasus->hot,
                        'psn' => 'PNS bersangkutan tidak terdaftar dalam sistem e - kasus'
                    );
                }
            }

            if($jns == '' || $jns == null || $jns == '1' || $jns == '2' || $jns == '3' || $jns == '4' || $jns == '5' || $jns == '6' || $jns == '7' || $jns == '8'){
                $data_header['session'] = $this->session->all_userdata();

                $data['detail'] = $this->Tpp_pegawai_p3k_model->getDetailById($real_id);
                $data['gol_ybs'] = $this->Tpp_pegawai_p3k_model->getGolonganYbs($data['detail']->golongan_id)->name;
                $data['pangkat_ybs'] = $this->Tpp_pegawai_p3k_model->getGolonganYbs($data['detail']->golongan_id)->kode;
                // $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();
                $data['kodes'] = $this->Tpp_pegawai_p3k_model->getDataKodesByJenis($jns);

                $data['lokasi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
                $data['gender'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_genders');
                $data['agama'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
                $data['marital'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
                $data['bool'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_bools');
                $data['statusp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
                $data['status'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
                $data['golongan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
                $data['golonganp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
                $data['eselon'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
                $data['pendidikan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
                $data['kedudukan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

                $data['fungsional'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
                $data['profesi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_profesis');

                $golongan_id = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->golongan_id;
                $masa = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->masa_kerja;
                $masa = substr($masa, 0, 2);

                $data['rekomen_gaji'] = array(
                    'baru' => 0,
                    'masa' => 0
                );
                $cek_rekom = $this->Tpp_pegawai_p3k_model->getDataRekomenGaji($golongan_id, $masa);
                if($cek_rekom != null){
                    $data['rekomen_gaji'] = $cek_rekom;
                }


                $nip_pegawai = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->nip;
                $cek_nipagain = getDataMastfip($nip_pegawai);
                $data['family'] = array();
                if($cek_nipagain != null){
                    $data['warning'] = array(
                        'pesan' => ''
                    );

                    $nip_lama_simpeg = getDataMastfip($nip_pegawai)->B_02;

                    $data['family'] = $this->Tpp_pegawai_p3k_model->getDataKeluarga($nip_lama_simpeg);
                }


                $data['filter'] = array(
                    'params' => $jns
                );

                //start efile
                $paramsDokEfile = '';

                foreach ($data['kodes'] as $kii => $valkodes) {
                    if($valkodes->kode_dokumen != null){
                        $paramsDokEfile .= $valkodes->kode_dokumen . ',';
                    }
                }

                $params = array(
                    'token' => '!BKDoyee123',
                    'kodes' => rtrim($paramsDokEfile, ","),
                    'nip' => $nip_pegawai
                );

                $res = ClientPost($params);
                $result = json_decode($res, TRUE);
                $data['efilenya'] = $result['data'];
                //end efile

                //notif for katu sekolah
                $n = $this->session->userdata('B_02B');
                $nipCabdin = $this->Tpp_pegawai_p3k_model->getNIPCabdinByNipKatuSekolah($n);
                if($nipCabdin != null){
                    $data['info_sekolah'] = array(
                        'message' => 'NIP KaTU Cabang Dinas',
                        'NIP' => $nipCabdin
                    );
                } else {
                    $data['info_sekolah'] = array(
                        'message' => 'Data NIP KaTU Cabang Dinas tidak ditemukan',
                        'NIP' => 0
                    );
                }

                $data['jabatan_pegawai'] = $this->Tpp_pegawai_p3k_model->getJabatanSimpeg($nip_ybs);

                $this->load->view('template/head');
                $this->load->view('template/header', $data_header);
                $this->load->view('tpp_pegawai_p3k/detail_pegawai', $data);

                // header('Content-Type: application/json');
                // echo json_encode($data);
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Invalid parameter detail'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_pegawai_p3k');
            }
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        
        }
    }

    public function detail_action(){
        $user_id = $this->session->userdata('id');
        $role = $this->session->userdata('role');
        // $id_lokasis = $this->session->userdata('id_lokasis');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $now = date('Y-m-d H:i:s');

        if(count($kode_id) > 0){
            $datahistory = array();
            $idVerif = array();
            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);

                $nilai_lama = $this->Tpp_pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;

                $nilai_baru = $this->input->post($field_name, TRUE);

                $A_01 = $this->session->userdata('A_01');
                $A_02 = $this->session->userdata('A_02');

                if($A_01 == 'D0' && $A_02 != '00'){
                    //bukan SKPD induk, turunan disdik prov. jenjang here

                    // $kodeumpeg_atasan = getKodeCabdinSekolah($A_01, $A_02, $A_03, $A_04, $A_05);
					// $lokasi = getNalokFromKolok($kodeumpeg_atasan);

					// $nip_acc = getNipUmpegCabdinByLokasi($posisi_acc);

                    $nip = $this->input->post('nipe', TRUE);
                    $nama = $this->input->post('nama', TRUE);
                    $jabatan = $this->input->post('jabatan', TRUE);

                    if($role == 4){
                        //UPT or sekolah
                        $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
                    } else if($role == 7){
                        //cabdin pendidikan
                        $x = $this->session->userdata('id_lokasis');
                        $pecah = explode(",", $x);
                        $firstKode = $pecah[0];
                        $myKolok = getKolokSimpegByLokasisKode($firstKode);
                    }

                    if($role == 4){
                        //UPT or sekolah
                        $myCabdinKolok = getKolokCabdin($myKolok);
                        $fix_myCabdinKolok = getKolokCabdin($myCabdinKolok); //fix perubahan tablokb08
                    } else {
                        $fix_myCabdinKolok = getKolokCabdinNonAtasan($myKolok);
                    }


                    $dataMyVerifikator = getMyVerifikator($fix_myCabdinKolok);
                    if($dataMyVerifikator != null){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            'verifikator' => $dataMyVerifikator->id,
                            'status_ajuan' => 0,
                            'posisi_acc' => $nip,
                            'posted_by' => $user_id,
                            'nip_by' => $this->session->userdata('B_02B'),
                            'created_at' => $now
                        );
                    } else {
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'Sekolah / Cabdin anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }

                } else {
                    //SKPD induk prov
                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

                    // $dataMyVerifikator = getMyVerifikator($myKolok);

                    $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
                    foreach ($dataAllMyVerifikator as $ki => $valVerif) {
                        $idVerif[$ki] = $valVerif->id;
                    }
                    $allIdVerifikator = implode(",", $idVerif);
                    if(count($dataAllMyVerifikator) > 0){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            // 'verifikator' => $dataMyVerifikator->id,
                            'verifikator' => $allIdVerifikator,
                            'status_ajuan' => 1,
                            // 'posisi_acc' => $dataMyVerifikator->nip,
                            'posisi_acc' => getNipSekByKolok($myKolok),
                            'posted_by' => $user_id,
                            'nip_by' => $this->session->userdata('B_02B'),
                            'created_at' => $now
                        );
                    } else {
                        //SKPD ybs blm pny verifikator
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }
                }

            }

            // echo json_encode($datahistory);
            // die();

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_historyp3ks_tpp', $datahistory);
            if($ins){
                //START add history acc ke verifikator
                // $data_acc = array(
                //     'pegawais_id' => $pegawai_id,
                //     'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                //     'nip' => $dataMyVerifikator->nip,
                //     'nama' => $dataMyVerifikator->nama,
                //     'jabatan' => 'Verifikator',
                //     'status_acc' => 0,
                //     'is_plt' => 0,
                //     'created_at' => $now
                // );
                // $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
                //END add history acc ke verifikator

                //add notif inbox2 for verifikator
                // $A_01 = $this->session->userdata('A_01');
                // $B_02B = $this->session->userdata('B_02B');
                // $B_03A = getDataMastfip($B_02B)->B_03A;
				// $B_03 = getDataMastfip($B_02B)->B_03;
				// $B_03B = getDataMastfip($B_02B)->B_03B;
                // tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Ada usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $dataMyVerifikator->nip, '0');

                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_history_p3k');
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_pegawai_p3k');
            }
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Pastikan ada data yang dipilih'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k/detail/' . encode_url($pegawai_id));
        }

    }

    //for UMPEG action
    public function ajukan_tpp_action(){
        $user_id = $this->session->userdata('id');
        $nip = $this->session->userdata('B_02B');
        // $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $nips = $this->input->post('nips', TRUE);
        // $skp = $this->input->post('skp', TRUE);
        // $potongan = $this->input->post('potongan', TRUE);
        // $periode = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
        $gols = $this->input->post('gols', TRUE);
        $periode_bulan = getDataPeriodeTPP()->bulan;
        $periode_tahun = getDataPeriodeTPP()->tahun;
        $jabatans = $this->input->post('jabatans', TRUE);
        $sakit = $this->input->post('sakit', TRUE);
        // $tbs = $this->input->post('tb_' ., TRUE);
        // for($i=0; $i < count($tbs); $i++){
        //     echo "Selected " . $tbs[$i] . "<br/>";
        // }
        // echo json_encode($tbs);
        // die();
        // if($tb == null){
        //     $tb = 0;
        // }
        $skp = $this->input->post('skp', TRUE);
        $keterangan = $this->input->post('keterangan', TRUE);

        $tanggal = date('Y-m-d');
        $now = date('Y-m-d H:i:s');

        $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
        $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
        foreach ($dataAllMyVerifikator as $ki => $valVerif) {
            $idVerif[$ki] = $valVerif->id;
        }
        $allIdVerifikator = implode(",", $idVerif);
        if(count($dataAllMyVerifikator) > 0){
            $data_bulk = array();
            // foreach ($skp as $key => $value) {
            //     $data_bulk[$key] = array(
            //         'tanggal' => $tanggal,
            //         'periode' => $periode,
            //         'pegawai_id' => $pegawai_id[$key],
            //         'verifikator' => $allIdVerifikator,
            //         'status_ajuan' => 1,
            //         'status_acc' => 0,
            //         'posisi_acc' => getNipSekByKolok($myKolok),
            //         'posted_by' => $user_id,
            //         'nip_by' => $nip,
            //         'skp' => $skp[$key],
            //         'potongan' => $potongan[$key],
            //         'created_at' => $now
            //     );
            // }

            foreach ($nips as $key => $val) {
                $data_bulk[$key] = array(
                    'nip' => $nips[$key],
                    'bulan' => $periode_bulan,
                    'tahun' => $periode_tahun,
                    'gol' => $gols[$key],
                    'tunjab' => 0,
                    'jab' => $jabatans[$key],
                    'sakit' => $sakit[$key],
                    'tb' => $this->input->post('tb_' . $nips[$key]),
                    'skp' => $skp[$key],
                    'hari' => 0,
                    'jam' => 0,
                    'perilaku' => 0,
                    'hukdis' => 100,
                    'ket' => $keterangan[$key],
                    'petugas' => $nip,
                    'waktu' => $now


                    // 'verifikator' => $allIdVerifikator,
                    // 'status_ajuan' => 1,
                    // 'status_acc' => 0,
                    // 'posisi_acc' => getNipSekByKolok($myKolok),
                    // 'posted_by' => $user_id,
                    // 'nip_by' => $nip,
                    // 'skp' => $skp[$key],
                    // 'potongan' => $potongan[$key],
                    // 'created_at' => $now
                );
            }
        } else {
            //SKPD ybs blm pny verifikator
            $array_msg = array(
                'status'=>'error',
                'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        }

        // echo json_encode($data_bulk);
        // die();

        $ins_bulk = $this->Fungsi_model->tambah_bulk('simgaji_masttpp_kinerja', $data_bulk);
        if($ins_bulk){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil mengusulkan data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_history_p3k');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal mengusulkan data'
            );
            redirect('tpp_pegawai_p3k');
        }
    }

    public function ajukan_tpp_actionBaru(){
        $user_id = $this->session->userdata('id');
        $A_01_yanglogin = $this->session->userdata('A_01');
        $nip = $this->session->userdata('B_02B');
        // $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $nips = $this->input->post('nips', TRUE);
        $gols = $this->input->post('gols', TRUE);
        $periode_bulan = getDataPeriodeTPP()->bulan;
        $periode_tahun = getDataPeriodeTPP()->tahun;
        $jabatans = $this->input->post('jabatans', TRUE);
        $sakit = $this->input->post('sakit', TRUE);
        $skp = $this->input->post('skp', TRUE);
        $keterangan = $this->input->post('keterangan', TRUE);
        $alphas = $this->input->post('alphas', TRUE);
        $kwks = $this->input->post('kwks', TRUE);
        $hukdis = $this->input->post('hukdis', TRUE);
        $kelasjabs = $this->input->post('kelasjabs', TRUE);
        $basetpps = $this->input->post('basetpps', TRUE);
        $tpp_bebankerjakhusus = $this->input->post('tpp_bebankerjakhusus', TRUE);
        $tpp_tempat = $this->input->post('tpp_tempat', TRUE);
        $tpp_kondisi = $this->input->post('tpp_kondisi', TRUE);

        $lhkpns = $this->input->post('lhkpns', TRUE);
        $lhkasns = $this->input->post('lhkasns', TRUE);
        $tptgrs = $this->input->post('tptgrs', TRUE);
        $kenabmds = $this->input->post('kenabmds', TRUE);
        $kenagrats = $this->input->post('kenagrats', TRUE);
        $xbmds = $this->input->post('xbmds', TRUE);
        $xgrats = $this->input->post('xgrats', TRUE);

        $tanggal = date('Y-m-d');
        $now = date('Y-m-d H:i:s');

        
        $data_bulk = array();

        foreach ($nips as $key => $val) {
            $rPeg = $this->Tpp_pot_pegawai_model->getRowPegawaiTpp($nips[$key]);
            $kolok = getDataMastfip($nips[$key])->A_01. getDataMastfip($nips[$key])->A_02. getDataMastfip($nips[$key])->A_03. getDataMastfip($nips[$key])->A_04. getDataMastfip($nips[$key])->A_05;

            $t = 0;
            if($this->input->post('tb_' . $nips[$key]) != null){
                $t = $this->input->post('tb_' . $nips[$key]);
            }

            //nominal tpp / nominal_beban kerja untuk rumahsakit yang 80%
            //moewardi, margono, tugu
            // if($A_01_yanglogin == '80' || $A_01_yanglogin == '81' || $A_01_yanglogin == '82'){
            //     $basetpps[$key] = ($basetpps[$key] * 80) / 100;
            // }

            //cek tmt mutasi
            if($rPeg->tmt_mutasi != null){
                $tmt_peg = $rPeg->tmt_mutasi;
                $cur_date = date('Y-m-d');
                $tgl1 = new DateTime($tmt_peg);
                $tgl2 = new DateTime($cur_date);
                $jarak = $tgl2->diff($tgl1);
                // echo $jarak->y.' '. $nips[$key];
                // die();

                if($jarak->y < 1){
                    //kurang dari 1 tahun
                    $basetpps[$key] = ($basetpps[$key] * 50) / 100;
                    $keterangan[$key] .= ' - masa kerja kurang dari 1 tahun';
                }
            }

            $pksp_default = 0;
            // $jmlinsentip = intval($basetpps[$key]) + intval($tpp_bebankerjakhusus[$key]) + intval($tpp_tempat[$key]) + intval($tpp_kondisi[$key]);
            $jmlinsentip = intval($basetpps[$key]);
            $jmlinsentip_3kategori = intval($tpp_bebankerjakhusus[$key]) + intval($tpp_tempat[$key]) + intval($tpp_kondisi[$key]);
            //nominaltpp = tppdefault / nominalbebankerja
            
            //------ Prosentase K6-K7 -------
            $dt_perilaku = (intval($alphas[$key])*2) + (floor($kwks[$key]/7.5)*2);
            if ($dt_perilaku>40) $dt_perilaku=40;

            //------ Nominal SKP
            $dt_nominalskp = $jmlinsentip * ((60/100) * ($skp[$key]/100));

            //------ Nominal Perilaku
            $dt_nominalperilaku = $jmlinsentip * ((40 - $dt_perilaku - $pksp_default)/100);

            //------ Total Nominal sebelum pengurangan
            $nomtppsebelum = $dt_nominalskp + $dt_nominalperilaku;

            //------ Hukdis
            $potonganhukdis = $nomtppsebelum * ((100-$hukdis[$key])/100);
            // $potonganhukdis = $nomtppsebelum * (($hukdis[$key])/100);
            // $nomtppsetelahhukdis = $nomtppsebelum - $potonganhukdis;
            
            $potlhkpn = $jmlinsentip * $lhkpns[$key]/100;
            $potlhkasn = $jmlinsentip * $lhkasns[$key]/100;
            $pottptgr = $jmlinsentip * $tptgrs[$key]/100;
            $potgrat = $jmlinsentip * $kenagrats[$key]/100;
            $potbmd = $jmlinsentip * $kenabmds[$key]/100;

            $dt_nominalkurangkepatuhan = $potlhkpn + $potlhkasn + $pottptgr + $potgrat + $potbmd;

            // $dt_nominalkurang = $potonganhukdis + $potlhkpn + $potlhkasn + $pottptgr + $potgrat + $potbmd;
            $dt_nominalkurang = $potonganhukdis;
            $dt_nominaltpp = $nomtppsebelum + $jmlinsentip_3kategori - $dt_nominalkurang - $dt_nominalkurangkepatuhan;

            $d = array(
                'nip' => $nips[$key],
                'A_01' => getDataMastfip($nips[$key])->A_01,
                'KOLOK' => $kolok,
                'lokasi' => $rPeg->lokasi,
                'lokasi_kerja' => $rPeg->lokasi_kerja,
                'lokasi_gaji' => $rPeg->lokasi_gaji,
                'bulan' => $periode_bulan,
                'tahun' => $periode_tahun,
                'gol' => $gols[$key],
                'jab' => $jabatans[$key],
                // 'sakit' => $sakit[$key],
                'sakit' => 0,
                'tb' => $t,
                'skp' => $skp[$key],
                'hari' => $alphas[$key],
                'jam' => $kwks[$key],
                'perilaku' => $dt_perilaku,
                'hukdis' => $hukdis[$key],
                'tc' => 0, //training center
                'lhkpn' => $lhkpns[$key],
                'lhkasn' => $lhkasns[$key],
                'bmd' => $kenabmds[$key],
                'grat' => $kenagrats[$key],
                'tptgr' => $tptgrs[$key],

                'nominalskp' => $dt_nominalskp,
                'nominalperilaku' => $dt_nominalperilaku,
                'nominalkurang' => $dt_nominalkurang,
                'nominalkurangkepatuhan' => $dt_nominalkurangkepatuhan,
                'nominaltpp' => $dt_nominaltpp, //thp ybs

                'ket' => $keterangan[$key],
                'kelas_jab' => $kelasjabs[$key],
                'nominal_bebankerja' => $basetpps[$key],
                'nominal_bebankerjakhusus' => $tpp_bebankerjakhusus[$key],
                'nominal_tempat' => $tpp_tempat[$key],
                'nominal_kondisi' => $tpp_kondisi[$key],
                'petugas' => $nip,
                'waktu' => $now
            );

            $cekdlu = $this->Tpp_pot_pegawai_model->cekRow($nips[$key], $periode_bulan, $periode_tahun);
            if($cekdlu){
                //update

                $this->Fungsi_model->editMasTppKinerjaByNipBulanTahunA01('simgaji_masttpp_kinerja', $nips[$key], $periode_bulan, $periode_tahun, getDataMastfip($nips[$key])->A_01, $d);
                $globUp = TRUE;
            } else {
                //insert

                $this->Fungsi_model->tambah('simgaji_masttpp_kinerja', $d);
                $globUp = TRUE;
            }
        }
        // echo json_encode($data_bulk[0]['nip']);
        // die();

        // $cekdlu = $this->Tpp_pot_pegawai_model->cekRow($data_bulk[0]['nip'], $periode_bulan, $periode_tahun);
        // if($cekdlu){
        //     //update
        //     $ins_bulk = $this->Fungsi_model->edit_bulk('simgaji_masttpp_kinerja', $data_bulk);
        //     if($ins_bulk){
        //         $array_msg = array(
        //             'status'=>'success',
        //             'message'=>'Berhasil mengusulkan data'
        //         );

        //         $this->session->set_flashdata($array_msg);
        //         redirect('nominatif_tpp_pot_pegawai');
        //     } else {
        //         $array_msg = array(
        //             'status'=>'error',
        //             'message'=>'Gagal mengusulkan data'
        //         );
        //         redirect('nominatif_tpp_pot_pegawai');
        //     }
        // } else {
        //     $ins_bulk = $this->Fungsi_model->tambah_bulk('simgaji_masttpp_kinerja', $data_bulk);
        //     if($ins_bulk){
        //         $array_msg = array(
        //             'status'=>'success',
        //             'message'=>'Berhasil mengusulkan data'
        //         );
    
        //         $this->session->set_flashdata($array_msg);
        //         redirect('nominatif_tpp_pot_pegawai');
        //     } else {
        //         $array_msg = array(
        //             'status'=>'error',
        //             'message'=>'Gagal mengusulkan data'
        //         );
        //         redirect('nominatif_tpp_pot_pegawai');
        //     }
        // }

        if($globUp){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil mengusulkan data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('nominatif_tpp_pot_pegawai');
        } else {
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal mengusulkan data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('nominatif_tpp_pot_pegawai');
        }
    }
}