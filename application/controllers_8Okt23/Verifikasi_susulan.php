<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verifikasi_susulan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Verifikasi_susulan_model');
    }

    public function index($riwayat = null)
    {

        $data_header['session'] = $this->session->all_userdata();
        $data['session'] = $this->session->all_userdata();

        if($this->uri->segment(3) == 'riwayat'){

            $data['title'] = 'Riwayat Verifikasi Susulan';

        }else{

            $data['title'] = 'Verifikasi Susulan';

        }

       
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Periode Verifikasi Susulan</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('verifikasi_susulan/index', $data);
    }

    //CRUD users START//
    public function get_verifikasi()
    {
        $periode = $this->uri->segment(3);

        $get = $this->Verifikasi_susulan_model->get_verifikasi($periode);
        // echo json_encode($get);
        // die();
        $datatable = array();
        foreach ($get as $key => $value) {

            if($value->kepala_skpd_by == '' && $value->kepala_skpd_nip == ''){
                $kepala = '<i style="color:red" class="icon-cancel-circle2"></i>';
            }else{
                $kepala = '<i style="color:green" class="icon-checkmark-circle"></i>';
            }

            if($value->verifikasi_status == 1){
                $bkd = '<i style="color:green" class="icon-checkmark-circle"></i>';
            }else if($value->verifikasi_status == 2){
                $bkd = '<i style="color:red" class="icon-cancel-circle2"></i>';
            }else{
                $bkd = '<i style="color:black" class="icon-cog"></i>';
            }

            $klik_kode = '<a href="javascript:void(0)" onclick="detailData(' . $value->id . ')">' . $value->nip . '</a>';
            $datatable[$key] = array(
               
                'nip' =>  $klik_kode,
                'nama' => $value->nama,
                'lokasikerja' => $value->lokasi_kerja,
                'lokasigaji' => $value->lokasi_gaji,
                'periode_start' => $value->periode_start,
                'periode_end' => $value->periode_end,
                'lama' => $value->lama,
                'baru' => $value->baru,
                'jumlah_bulan' => $value->jumlah_bulan,
                'kekurangan' => $value->kekurangan,
                'tunjangan_istri' => $value->tunjangan_istri,
                'tunjangan_anak' => $value->tunjangan_anak,
                'jumlah_tunjangan_keluarga' => $value->kekurangan,
                'tunjangan_umum' => $value->tunjangan_umum,
                'tunjangan_umum_tambahan' => $value->tunjangan_umum_tambahan,
                'tunjangan_struktural' => $value->tunjangan_struktural,
                'tunjangan_fungsional' => $value->tunjangan_fungsional,
                'tunjangan_beras' => $value->tunjangan_beras,
                'tunjangan_pph' => $value->tunjangan_pph,
                'pembulatan' => $value->pembulatan,
                'status_verifikasi_kepala' => $kepala,
                'status_verifikasi_bkd' =>  $bkd,
                'jenis_susulan' => $value->jenis_susulan,
                'id' => '<input type="checkbox" class="data-check" value="' . $value->id . '">',
            );
        }
        // $data['datatable'] = $datatable;

        $output = array(

            "recordsTotal" => $this->Verifikasi_susulan_model->verifikasi_count_all($periode),
            "recordsFiltered" => $this->Verifikasi_susulan_model->verifikasi_count_filtered($periode),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function acc_action()
    {
        $list_id = $this->input->post('id');


        foreach ($list_id as $id) {
            
            $this->db->where('id', $id);
            $x = $this->db->get('simgaji_susulan')->row();
        
            $data = array(
                'verifikator' => $this->session->userdata('B_02B'),
                'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                'verifikasi_status' => 1,
                'kekurangan' => $x->baru - $x->lama
            );
            $this->db->where('id', $id);
            $this->db->update('simgaji_susulan', $data);
        }
        echo json_encode(array("status" => TRUE));
    }

    public function tolak_action()
    {
        $list_id = $this->input->post('id');
        foreach ($list_id as $id) {
            
            $this->db->where('id', $id);
            $x = $this->db->get('simgaji_susulan')->row();
        
            $data = array(
                'verifikator' => $this->session->userdata('B_02B'),
                'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                'verifikasi_status' => 2,
                'kekurangan' => ''
            );
            $this->db->where('id', $id);
            $this->db->update('simgaji_susulan', $data);
        }
        echo json_encode(array("status" => TRUE));
    }

    public function detail()
    {
        $id = $this->input->post('id', TRUE);
        $data = $this->Verifikasi_susulan_model->getById($id);
        
        if ($data->num_rows() > 0) {
            $response = array(
                'status' => 1,
                'message' => 'Data berhasil ditemukan',
                'data' => $data->row(),
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Data gagal ditemukan',
                'data' => null,
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function acc()
    {
        $id = $this->input->post('id');

        $this->db->where('id', $id);
        $x = $this->db->get('simgaji_susulan')->row();

        if($x){

            $data = array(
                'verifikator' => $this->session->userdata('B_02B'),
                'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                'verifikasi_status' => 1,
                'kekurangan' => $x->baru - $x->lama
            );
            $this->db->where('id', $id);
            $this->db->update('simgaji_susulan', $data);

        }

        $response = array(
            'status' => 1,
            'message' => 'Berhasil Update',
        );
        header('Content-Type: application/json');
        echo json_encode($response);
        
        // echo json_encode(array("status" => TRUE));
    }


    public function tolak()
    {
        $id = $this->input->post('id');

        $this->db->where('id', $id);
        $x = $this->db->get('simgaji_susulan')->row();

        if($x){

            $data = array(
                'verifikator' => $this->session->userdata('B_02B'),
                'verifikasi_tanggal' => date('Y-m-d H:i:s'),
                'verifikasi_status' => 2,
                'kekurangan' => ''
            );
            $this->db->where('id', $id);
            $this->db->update('simgaji_susulan', $data);

        }

        $response = array(
            'status' => 1,
            'message' => 'Berhasil Update',
        );
        header('Content-Type: application/json');
        echo json_encode($response);
        
        // echo json_encode(array("status" => TRUE));
    }


}