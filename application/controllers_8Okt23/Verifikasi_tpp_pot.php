<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_tpp_pot extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Verifikasi_tpp_pot_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
		$nip_sess = $this->session->userdata('B_02B');
		$role = $this->session->userdata('role');

		$skpd = array();
		if($role == '2'){
			$skpds = $this->Verifikasi_tpp_pot_model->getRowVerifikator($nip_sess)->kode_skpd_simpeg;
			$datas = explode(",", $skpds);

			foreach ($datas as $key => $v) {

				$n = '-';
				if($this->Verifikasi_tpp_pot_model->getRowTablokB08($v) != null){
					$n =  $this->Verifikasi_tpp_pot_model->getRowTablokB08($v)->NALOK;
				}

				$skpd[$key] = array(
					'kode' => $v,
					'nama' => $n
				);
			}

			$data['skpd_ampu'] = $skpd;
		}

		if(isset($_GET['unor']) && isset($_GET['bul']) && isset($_GET['thn'])){
			$b = $_GET['bul'];
			$t = $_GET['thn'];
			$nokelas = $this->input->get('tanpa_kelasjab', TRUE);
			$v_nokelas = null;
			if($nokelas == 'on'){
				$v_nokelas = '1';
			}

			$get = $this->Verifikasi_tpp_pot_model->getDataUsulanPerUnor($_GET['unor'], $b, $t, $v_nokelas);
			$datatable = array();
			foreach ($get as $key => $value) {

				$parklik = "'".$value->nip."'," . $b .",". $t;

                $fname = getDataMastfip($value->nip)->B_03A. ' ' .getDataMastfip($value->nip)->B_03. ' ' .getDataMastfip($value->nip)->B_03B;
				$nipklik = '<a href="javascript:void(0)" onclick="detailVerif('.$parklik.')">'.$value->nip.'</a>';

				$cbs = '<input type="checkbox" class="check-item" name="nips[]" value="'.$value->nip.'">';
				if($value->status_verifikasi == '1'){
					$cbs = '<input type="checkbox" class="check-item" name="nips[]" value="'.$value->nip.'" checked>';
				}

                $datatable[$key] = array(
                    'nama_nip' => $nipklik.'</br>'.$fname.'</br>'.getDataPangkatGol($value->gol)->NAMAX,
                    'kelas_jab' => $value->kelas_jab,
                    'jabatan' => $value->jab,
                    'cs' => $value->sakit,
                    'tb' => $value->tb,
                    'skp' => $value->skp,
                    'pksp' => 0,
                    'alpha' => $value->hari,
                    'kwk' => $value->jam,
                    'hukdis' => $value->hukdis,
                    // 'atlit_pelatih' => '',
                    'keterangan' => $value->ket,
					'cb' => $cbs,
                );
            }
			$data['datatable'] = $datatable;


			$data['filter'] = array(
				'bl' => $_GET['bul'],
				'th' => $_GET['thn'],
				'unor' => $_GET['unor'],
				'jml_record' => count($datatable)
			);
		} else {
			$data['filter'] = array(
				'bl' => getDataPeriodeTPP()->bulan,
				'th' => getDataPeriodeTPP()->tahun,
				'unor' => null,
				'jml_record' => 0
			);
		}
		

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('verifikasi_tpp_pot/index', $data);

		// echo json_encode($data);
    }

	public function detailbynipblth(){
		$nip = $this->input->post('nip', TRUE);
		$bulan = $this->input->post('bulan', TRUE);
		$tahun = $this->input->post('tahun', TRUE);

		$data = $this->Verifikasi_tpp_pot_model->getDetailData($nip, $bulan, $tahun);
		if($data != null){

			$fname = getDataMastfip($data->nip)->B_03A . ' ' . getDataMastfip($data->nip)->B_03 . ' ' . getDataMastfip($data->nip)->B_03B;
			$total_pendapatan = $data->nominaltpp + $data->nominal_bebankerjakhusus + $data->nominal_tempat + $data->nominal_kondisi;
			$total_pengurangan = $data->nominalkurang + $data->nominalkurangkepatuhan;
			$total = $total_pendapatan - $total_pengurangan;

			$d = array(
				'nip' => $data->nip,
				'nama' => $fname,
				'nominal_tpp' => 'Rp. ' . number_format($data->nominaltpp),
				'nominal_tpp_bebankerjakhusus' => 'Rp. ' . number_format($data->nominal_bebankerjakhusus),
				'nominal_tpp_tempat' => 'Rp. ' . number_format($data->nominal_tempat),
				'nominal_tpp_kondisi' => 'Rp. ' . number_format($data->nominal_kondisi),
				'nominal_pengurangan' => 'Rp. ' . number_format($total_pengurangan),
				'total' => 'Rp. ' . number_format($total)
			);

			$response = array(
				'status' => 1,
				'message' => 'Data berhasil ditemukan',
				'data' => $d
			);
		} else {
			$response = array(
				'status' => 0,
				'message' => 'Data gagal ditemukan',
				'data' => $data
			);
		}

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($response);
	}

	public function cetak_rekap_tpp(){
        $A_01 = $this->session->userdata('A_01');
        $nip = $this->session->userdata('B_02B');
        $bulan = $this->input->get('bulan', TRUE);
        $tahun = $this->input->get('tahun', TRUE);

        if(($bulan != null || $bulan != '') && ($tahun != null || $tahun != '')){
            $cetak100 = null;
            if(isset($_GET['c100'])){
                $cetak100 = isset($_GET['c100']);
            }

            // $kode = null;
            // if($A_01 == 'D0'){
                $kode = $this->input->get('kode', TRUE);
            // }

            $get = $this->Verifikasi_tpp_pot_model->getDataNominatifCetak($tahun, $bulan, $cetak100, $kode);

            $datatable = array();
            foreach ($get as $key => $value) {

                $fname = getDataMastfip($value->nip)->B_03A. ' ' .getDataMastfip($value->nip)->B_03. ' ' .getDataMastfip($value->nip)->B_03B;

                $datatable[$key] = array(
                    'unit_kerja' => getDataMastfip($value->nip)->A_01,
                    'nip' => $value->nip,
                    'nama' => $fname,
                    'nalok' => $value->NALOK,
                    'pkt_gol' => getDataPangkatGol($value->gol)->NAMAX,
                    'jabatan' => $value->jab,
                    'kelas_jab' => $value->kelas_jab,

                    'skp' => $value->skp,
                    'perilaku' => $value->perilaku,
                    'hukdis' => $value->hukdis,

                    'lhkpn' => $value->lhkpn,
                    'lhkasn' => $value->lhkasn,
                    'bmd' => $value->bmd,
                    'grat' => $value->grat,
                    'tptgr' => $value->tptgr,

                    'nominalskp' => $value->nominalskp,
                    'nominalperilaku' => $value->nominalperilaku,
                    'nominalkurang' => $value->nominalkurang,
                    'nominalkurangkepatuhan' => $value->nominalkurangkepatuhan,
                    'nominaltpp' => $value->nominaltpp,
                    'nominaldefault' => $value->nominal_bebankerja,
                    
                    'ket' => $value->ket,
                    'waktu' => $value->waktu
                );
            }
            $data['datanya'] = $datatable;

            $data['filter'] = array(
                'bl' => $bulan,
                'th' => $tahun
            );

			$A_01cetak = substr($kode, 0, 2);
            $data['kepala'] = $this->Verifikasi_tpp_pot_model->getRowKepala($A_01cetak);
			
            $data['unor'] = $this->Verifikasi_tpp_pot_model->getRowSKPDInduk($A_01cetak)->NALOK;

            $data['subunor_disdik'] = null;
            if($A_01cetak == 'D0'){
				$kolok6digit = substr($kode, 0, 6);
                $data['subunor_disdik'] = $this->Verifikasi_tpp_pot_model->getUkDisdik($kolok6digit)->NALOK;
            }

            $this->load->view('verifikasi_tpp_pot/cetak_rekap_tpp', $data);
            // echo json_encode($data);
        } else {
            echo "Invalid parameter";
        }
    }

	public function simpan_bulk_action(){
		$nip_sess = $this->session->userdata('B_02B');
		$unor = $this->input->post('unor', TRUE);
		$bulan_periode = getDataPeriodeTPP()->bulan;
		$tahun_periode = getDataPeriodeTPP()->tahun;
		$now = date('Y-m-d H:i:s');
		$nips = $this->input->post('nips', TRUE);
		$status = $this->input->post('status', TRUE);

		$yuerel = base_url(). 'verifikasi_tpp_pot?unor=' . $unor . '&bul='.$bulan_periode.'&thn='.$tahun_periode;

		if($status == null || $status == ""){
			$array_msg = array(
                'status'=>'error',
                'message'=>'Silahkan pilih status'
            );

            $this->session->set_flashdata($array_msg);
            redirect($yuerel, 'refresh');
		} else {
			if(!empty($nips)){
				// $dt = array();
				foreach ($nips as $ki => $val) {
					// $dt[$ki] = array(
					// 	'nip' => $val,
					// 	'status' => $status,
					// 	'waktu_verifikasi' => $now,
					// 	'nip_verifikasi' => $nip_sess
					// );

					$dtupdate = array(
						'status_verifikasi' => $status,
						'waktu_verifikasi' => $now,
						'nip_verifikasi' => $nip_sess
					);

					$this->Verifikasi_tpp_pot_model->editbaru('simgaji_masttpp_kinerja', $dtupdate, $val, $bulan_periode, $tahun_periode);
					$globUp = TRUE;
				}

				// echo json_encode($dt);

				if($globUp){
					$array_msg = array(
						'status'=>'success',
						'message'=>'Berhasil memperbaharui data'
					);

					$this->session->set_flashdata($array_msg);
					redirect($yuerel, 'refresh');
				} else {
					$array_msg = array(
						'status'=>'error',
						'message'=>'Gagal memperbaharui data'
					);

					$this->session->set_flashdata($array_msg);
					redirect($yuerel, 'refresh');
				}
			} else {
				$array_msg = array(
					'status'=>'error',
					'message'=>'Pastikan ada data yang dipilih'
				);
	
				$this->session->set_flashdata($array_msg);
				redirect($yuerel, 'refresh');
			}

			
		}
	}
}