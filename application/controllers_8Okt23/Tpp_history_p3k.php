<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_history_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_history_p3k_model');
		$this->load->model('Tpp_pegawai_p3k_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $periode_bulan = getDataPeriodeTPP()->bulan;
        $periode_tahun = getDataPeriodeTPP()->tahun;
        $get = $this->Tpp_history_p3k_model->getDataPeriode($periode_bulan, $periode_tahun);

        $datatable = array();
        foreach ($get as $key => $value) {
            $per = getDataPeriodeTPP()->tahun . '-' . getDataPeriodeTPP()->bulan . '-01';
            if($value->periode != $per){
                $period = $value->periode;
            } else {
                $period = '<a href="'. base_url('tpp_history_p3k/periode/') . $value->periode .'">'. $value->periode .'</a>';
            }

            $datatable[$key] = array(
                'periode' => $period
            );
        }
        $data['datatable'] = $datatable;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('tpp_history_p3k/tpp_history_p3k_periode', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function periode($per){
        $data_header['session'] = $this->session->all_userdata();

        $user_id = $this->session->userdata('id');
        $A_01 = $this->session->userdata('A_01');
        $data['detail_byperiode'] = $this->Tpp_history_p3k_model->getStatusAllDataByPeriode($per, $user_id);

        $get = $this->Tpp_history_p3k_model->getDataHistory($A_01, $per, $user_id);

        $datatable = array();
		foreach ($get as $key => $value) {

            $sts_acc = '-';
            if($value->status_acc == 0){
                $sts_acc = '<span class="badge badge-warning">Menunggu</span>';
            } else if($value->status_acc == 1){
                $sts_acc = '<span class="badge badge-success">Disetujui</span>';
            } else if($value->status_acc == 9) {
                $sts_acc = '<span class="badge badge-success">Proses Data Pada BKD</span>';
            } else {
                $sts_acc = '<span class="badge badge-danger">Direvisi: '.$value->keterangan.'</span>';
            }

            $datatable[$key] = array(
                'no' => $key + 1,
				'periode' => $value->periode,
				'nip_nama_jabatan' => $value->nip . '</br>' . $value->nama . '</br>' . $value->jabatan,
                'status' => $sts_acc,
                'waktu' => $value->updated_at
			);
        }
        $data['datatable'] = $datatable;

        $getTop = $this->Tpp_history_p3k_model->getDataByKasubbagKeu($per, $user_id);
        $datatable_top = array();
		foreach ($getTop as $key => $value) {

            
            if($value->updated_at == null){
                $ac = '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="hapusAjuan('. $value->id .')"><i class="icon-trash"></i></a>';
            } else {
                $ac = '-';
            }
            
            $klik_nip = '<a href="'. base_url('tpp_history_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
            
			$datatable_top[$key] = array(
                'no' => $key+1,
                'nip' => $klik_nip,
                'nama' => $value->nama,
                'act' => $ac
			);
		}
        $data['datatable_top'] = $datatable_top;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
        $this->load->view('tpp_history_p3k/tpp_history_p3k_index', $data);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function detail($id){
        $real_id = decode_url($id);

        $cek = cek_realparamsPegawaiId('simgaji_historyp3ks_tpp', $real_id);
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_history_p3k_model->getDetailPegawaiById($real_id);

            $get = $this->Tpp_history_p3k_model->getDataByPegawaiId($real_id);

            $datatable = array();
            foreach ($get as $key => $value) {
                
                // $sts_ajuan = '';
                // if($value->status_ajuan == 0){
                //     $sts_ajuan = 'UPT / Sekolah';
                // } else if($value->status_ajuan == 1){
                //     $sts_ajuan = 'Kasubag Keuangan';
                // } else if($value->status_ajuan == 2){
                //     $sts_ajuan = 'Sekretaris';
                // } else if($value->status_ajuan == 3){
                //     $sts_ajuan = 'Kepala SKPD';
                // }
                $act = '';

                if($value->status_acc_bkd == '0'){
                    $sts_bkd = '<span class="badge badge-warning">Menunggu</span>';
                } else if($value->status_acc_bkd == '1'){
                    $sts_bkd = '<span class="badge badge-success">Acc</span>';
                } else if($value->status_acc_bkd == '2'){
                    $sts_bkd = '<span class="badge badge-warning">Revisi</span>';
                    $act = '<div>
                        <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="revisiModal('. $value->id .')"><i class="icon-pencil"></i></a>
                        </div>';
                } else if($value->status_acc_bkd == '3'){
                    $sts_bkd = '<span class="badge badge-danger">Ditolak</span>';
                }

                $datatable[$key] = array(
                    'kode_id' => $value->kode_id,
                    'params' => $value->params,
                    // 'lama' => $value->lama,
                    'lama' => cekKode($value->kode_id, $value->lama),
                    // 'baru' => $value->baru,
                    'baru' => cekKode($value->kode_id, $value->baru),
                    'ket' => $value->description,
                    'sts_bkd' => $sts_bkd,
                    'aksi' => $act
                    // 'status' => $sts_ajuan,
                );
            }
            $data['datatable'] = $datatable;

            // $data['data_usulan'] = $this->History_p3k_model->getDataByPegawaiIdBandingkan($real_id);

            //start data existing
            $data['kodes'] = $this->Tpp_pegawai_p3k_model->getDataKodes();

            $data['lokasi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');
            
            $data['fungsional'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
            //end data existing
        
            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
		    $this->load->view('tpp_history_p3k/detail_usulan_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('tpp_history_p3k');
        }
    }

    public function ajukan_action(){
        $role = $this->session->userdata('role');
        $user_id = $this->session->userdata('id');
        $id_lokasis = $this->session->userdata('id_lokasis');
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');

        $A_01 = $this->session->userdata('A_01');
        $B_02B = $this->session->userdata('B_02B');
        $B_03A = getDataMastfip($B_02B)->B_03A;
        $B_03 = getDataMastfip($B_02B)->B_03;
        $B_03B = getDataMastfip($B_02B)->B_03B;

        $kodeSekretaris = cari_kode_sekertaris($A_01);
        $nipSekretaris = getNipByKolok($kodeSekretaris);
        $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
        $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
        $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
        $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;

        //add notif
        tambahNotif('8',null, base_url('approval_usulan_tpp_p3k'), 'approval_usulan_tpp_p3k', 'TPP Pegawai P3K', 'Usulan data TPP pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipSekretaris, '0');

        //START add history acc
        $data_acc = array(
            'A_01' => $A_01,
            'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
            'nip' => $nipSekretaris,
            'nama' => $namaSekretaris,
            'jabatan' => 'SEKRETARIS',
            'status_acc' => 0,
            'is_plt' => 0,
            'created_at' => $now,
            'posted_by' => $user_id
        );
        $this->Fungsi_model->tambah('simgaji_historyp3ks_tpp_acc', $data_acc);
        //END add history acc

        $data = array(
            'status_ajuan' => 2,
            'posisi_acc' => $nipSekretaris,
            'status_acc_bkd' => 0,
            'updated_at' => $now
        );

        $upd = $this->Tpp_history_p3k_model->updateDataUsulanByPeriode('simgaji_historyp3ks_tpp', $data, $periode, $user_id);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
}