<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pegawai_p3k extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_pegawai_p3k_model');
		// $this->load->model('Pegawai_p3k_model');

        $role = $this->session->userdata('role');
        if( $role != 3 && $role != 8){
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('dashboard').'";</script>';       
        }
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
        $role = $this->session->userdata('role');
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;
        $kode_lokasis = getDetailLokasisById($lokasis_id)->kode;

        if($role == 3){
            //kasubbag keu

            $get = $this->Tpp_pegawai_p3k_model->getDataPegawaiByKodeAsKasubbagKeu($kode_lokasis);

            $datatable = array();
            foreach ($get as $key => $value) {

                if($statusSkpd == '2'){
                    $klik_nip = '<a href="'. base_url('tpp_pegawai_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
                } else {
                    $klik_nip = $value->nip;
                }
    
                $datatable[$key] = array(
                    'name' => $value->name,
                    'nip' => $klik_nip,
                    'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('tpp_pegawai_p3k/data_pegawai/') . encode_url($value->id) .'">Lihat Data</a>'
                );
            }
            $data['datatable'] = $datatable;

            // $data['pegawais'] = $this->Tpp_pegawai_p3k_model->getDataPegawai($lokasis_id);

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai_p3k/tpp_pegawai_p3k_index', $data);

        } else {
            //umpeg induk

            $kolok_simpeg = getDetailLokasisById($lokasis_id)->kolok_simpeg;
            $get = $this->Tpp_pegawai_p3k_model->getDataPegawaiByKode($kode_lokasis, $kolok_simpeg);

            $datatable = array();
            foreach ($get as $key => $value) {
                // if($statusSkpd == '2'){
                //     $klik_nip = '<a href="'. base_url('pegawai_p3k/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
                // } else {
                //     $klik_nip = $value->nip;
                // }

                // $skp = 
                // '<div>
                //     <input type="hidden" class="form-control font-weight-semibold" name="pegawai_id[]" value="'. $value->id .'">
                //     <input type="hidden" class="form-control font-weight-semibold" name="nips[]" value="'. $value->nip .'">
                //     <input type="text" class="form-control font-weight-semibold" placeholder="Nilai SKP" name="skp[]" value="100" autocomplete="off">
                // </div>';

                // $pot = 
                // '<div">
                //     <input type="text" class="form-control font-weight-semibold" placeholder="Potongan" name="potongan[]" value="0" autocomplete="off">
                // </div>';
                
                $jab = '-';
                if($value->I_JB != null){
                    $jab = $value->I_JB;
                }

                $klsjab = '-';
                if($value->kelasjab != null){
                    $klsjab = $value->kelasjab;
                }

                $rmpn = '-';
                if($value->RUMPUN != null){
                    $rmpn = $value->RUMPUN;
                }

                $datatable[$key] = array(
                    'B_02B' => $value->B_02B,
                    'nama' => $value->nama,
                    'jabatan' => $jab,
                    'cuti_2bulanlalu' => hitungCSByNip($value->B_02B),
                    'kelas_jabatan' => $klsjab,
                    'rumpun' => $rmpn,
                    'F_03' => $value->F_03,
                    'hukdis' => hitungHukdisByNip($value->B_02B)
                    // 'skp' => $skp,
                    // 'potongan' => $pot,
                );
            }
            $data['pegawais'] = $datatable;

            // $data['pegawais'] = $this->Tpp_pegawai_p3k_model->getDataPegawai($lokasis_id);

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai_p3k/tpp_pegawai_p3k_index', $data);
        }

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function data_pegawai($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawaip3ks_tpp', $real_id);
        if($cek){
            // echo 'lanjut';

            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_pegawai_p3k_model->getDetailById($real_id);
            $data['kodes'] = $this->Tpp_pegawai_p3k_model->getDataKodes();

            $data['lokasi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

            $data['fungsional'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_profesis');

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai_p3k/data_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        }
    }

    public function detail($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawaip3ks_tpp', $real_id);
        if($cek){
            $jns = $this->input->get('jns', TRUE);

            $data['ekasus'] = array(
                'kode_hot' => 0,
                'psn' => '--'
            );

            $data['warning'] = array(
            '   pesan' => 'NIP tidak ditemukan di SIMPEG, silahkan hubungi admin'
            );

            $nip_ybs = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->nip;
            $cekNip = getDataMastfip($nip_ybs);
            if($cekNip != null){
                $data['warning'] = array(
                    'pesan' => ''
                );
            }

            if($jns == '1' || $jns == '2'){
                $cekKasus = $this->Tpp_pegawai_p3k_model->ekasus($nip_ybs);
                if($cekKasus->hot > 0){
                    $data['ekasus'] = array(
                        'kode_hot' => $cekKasus->hot,
                        'psn' => 'PNS bersangkutan terdaftar dalam sistem e - kasus'
                    );
                } else {
                    $data['ekasus'] = array(
                        'kode_hot' => $cekKasus->hot,
                        'psn' => 'PNS bersangkutan tidak terdaftar dalam sistem e - kasus'
                    );
                }
            }

            if($jns == '' || $jns == null || $jns == '1' || $jns == '2' || $jns == '3' || $jns == '4' || $jns == '5' || $jns == '6' || $jns == '7' || $jns == '8'){
                $data_header['session'] = $this->session->all_userdata();

                $data['detail'] = $this->Tpp_pegawai_p3k_model->getDetailById($real_id);
                $data['gol_ybs'] = $this->Tpp_pegawai_p3k_model->getGolonganYbs($data['detail']->golongan_id)->name;
                $data['pangkat_ybs'] = $this->Tpp_pegawai_p3k_model->getGolonganYbs($data['detail']->golongan_id)->kode;
                // $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();
                $data['kodes'] = $this->Tpp_pegawai_p3k_model->getDataKodesByJenis($jns);

                $data['lokasi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
                $data['gender'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_genders');
                $data['agama'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
                $data['marital'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
                $data['bool'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_bools');
                $data['statusp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
                $data['status'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
                $data['golongan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
                $data['golonganp3k'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
                $data['eselon'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
                $data['pendidikan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
                $data['kedudukan'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

                $data['fungsional'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
                $data['profesi'] = $this->Tpp_pegawai_p3k_model->getDataDataMaster('simgaji_profesis');

                $golongan_id = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->golongan_id;
                $masa = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->masa_kerja;
                $masa = substr($masa, 0, 2);

                $data['rekomen_gaji'] = array(
                    'baru' => 0,
                    'masa' => 0
                );
                $cek_rekom = $this->Tpp_pegawai_p3k_model->getDataRekomenGaji($golongan_id, $masa);
                if($cek_rekom != null){
                    $data['rekomen_gaji'] = $cek_rekom;
                }


                $nip_pegawai = $this->Tpp_pegawai_p3k_model->getDetailById($real_id)->nip;
                $cek_nipagain = getDataMastfip($nip_pegawai);
                $data['family'] = array();
                if($cek_nipagain != null){
                    $data['warning'] = array(
                        'pesan' => ''
                    );

                    $nip_lama_simpeg = getDataMastfip($nip_pegawai)->B_02;

                    $data['family'] = $this->Tpp_pegawai_p3k_model->getDataKeluarga($nip_lama_simpeg);
                }


                $data['filter'] = array(
                    'params' => $jns
                );

                //start efile
                $paramsDokEfile = '';

                foreach ($data['kodes'] as $kii => $valkodes) {
                    if($valkodes->kode_dokumen != null){
                        $paramsDokEfile .= $valkodes->kode_dokumen . ',';
                    }
                }

                $params = array(
                    'token' => '!BKDoyee123',
                    'kodes' => rtrim($paramsDokEfile, ","),
                    'nip' => $nip_pegawai
                );

                $res = ClientPost($params);
                $result = json_decode($res, TRUE);
                $data['efilenya'] = $result['data'];
                //end efile

                //notif for katu sekolah
                $n = $this->session->userdata('B_02B');
                $nipCabdin = $this->Tpp_pegawai_p3k_model->getNIPCabdinByNipKatuSekolah($n);
                if($nipCabdin != null){
                    $data['info_sekolah'] = array(
                        'message' => 'NIP KaTU Cabang Dinas',
                        'NIP' => $nipCabdin
                    );
                } else {
                    $data['info_sekolah'] = array(
                        'message' => 'Data NIP KaTU Cabang Dinas tidak ditemukan',
                        'NIP' => 0
                    );
                }

                $data['jabatan_pegawai'] = $this->Tpp_pegawai_p3k_model->getJabatanSimpeg($nip_ybs);

                $this->load->view('template/head');
                $this->load->view('template/header', $data_header);
                $this->load->view('tpp_pegawai_p3k/detail_pegawai', $data);

                // header('Content-Type: application/json');
                // echo json_encode($data);
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Invalid parameter detail'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_pegawai_p3k');
            }
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        
        }
    }

    public function detail_action(){
        $user_id = $this->session->userdata('id');
        $role = $this->session->userdata('role');
        // $id_lokasis = $this->session->userdata('id_lokasis');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $now = date('Y-m-d H:i:s');

        if(count($kode_id) > 0){
            $datahistory = array();
            $idVerif = array();
            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);

                $nilai_lama = $this->Tpp_pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;

                $nilai_baru = $this->input->post($field_name, TRUE);

                $A_01 = $this->session->userdata('A_01');
                $A_02 = $this->session->userdata('A_02');

                if($A_01 == 'D0' && $A_02 != '00'){
                    //bukan SKPD induk, turunan disdik prov. jenjang here

                    // $kodeumpeg_atasan = getKodeCabdinSekolah($A_01, $A_02, $A_03, $A_04, $A_05);
					// $lokasi = getNalokFromKolok($kodeumpeg_atasan);

					// $nip_acc = getNipUmpegCabdinByLokasi($posisi_acc);

                    $nip = $this->input->post('nipe', TRUE);
                    $nama = $this->input->post('nama', TRUE);
                    $jabatan = $this->input->post('jabatan', TRUE);

                    if($role == 4){
                        //UPT or sekolah
                        $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
                    } else if($role == 7){
                        //cabdin pendidikan
                        $x = $this->session->userdata('id_lokasis');
                        $pecah = explode(",", $x);
                        $firstKode = $pecah[0];
                        $myKolok = getKolokSimpegByLokasisKode($firstKode);
                    }

                    if($role == 4){
                        //UPT or sekolah
                        $myCabdinKolok = getKolokCabdin($myKolok);
                        $fix_myCabdinKolok = getKolokCabdin($myCabdinKolok); //fix perubahan tablokb08
                    } else {
                        $fix_myCabdinKolok = getKolokCabdinNonAtasan($myKolok);
                    }


                    $dataMyVerifikator = getMyVerifikator($fix_myCabdinKolok);
                    if($dataMyVerifikator != null){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            'verifikator' => $dataMyVerifikator->id,
                            'status_ajuan' => 0,
                            'posisi_acc' => $nip,
                            'posted_by' => $user_id,
                            'nip_by' => $this->session->userdata('B_02B'),
                            'created_at' => $now
                        );
                    } else {
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'Sekolah / Cabdin anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }

                } else {
                    //SKPD induk prov
                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

                    // $dataMyVerifikator = getMyVerifikator($myKolok);

                    $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
                    foreach ($dataAllMyVerifikator as $ki => $valVerif) {
                        $idVerif[$ki] = $valVerif->id;
                    }
                    $allIdVerifikator = implode(",", $idVerif);
                    if(count($dataAllMyVerifikator) > 0){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            // 'verifikator' => $dataMyVerifikator->id,
                            'verifikator' => $allIdVerifikator,
                            'status_ajuan' => 1,
                            // 'posisi_acc' => $dataMyVerifikator->nip,
                            'posisi_acc' => getNipSekByKolok($myKolok),
                            'posted_by' => $user_id,
                            'nip_by' => $this->session->userdata('B_02B'),
                            'created_at' => $now
                        );
                    } else {
                        //SKPD ybs blm pny verifikator
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }
                }

            }

            // echo json_encode($datahistory);
            // die();

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_historyp3ks_tpp', $datahistory);
            if($ins){
                //START add history acc ke verifikator
                // $data_acc = array(
                //     'pegawais_id' => $pegawai_id,
                //     'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                //     'nip' => $dataMyVerifikator->nip,
                //     'nama' => $dataMyVerifikator->nama,
                //     'jabatan' => 'Verifikator',
                //     'status_acc' => 0,
                //     'is_plt' => 0,
                //     'created_at' => $now
                // );
                // $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
                //END add history acc ke verifikator

                //add notif inbox2 for verifikator
                // $A_01 = $this->session->userdata('A_01');
                // $B_02B = $this->session->userdata('B_02B');
                // $B_03A = getDataMastfip($B_02B)->B_03A;
				// $B_03 = getDataMastfip($B_02B)->B_03;
				// $B_03B = getDataMastfip($B_02B)->B_03B;
                // tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Ada usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $dataMyVerifikator->nip, '0');

                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_history_p3k');
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_pegawai_p3k');
            }
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Pastikan ada data yang dipilih'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k/detail/' . encode_url($pegawai_id));
        }

    }

    //for UMPEG action
    public function ajukan_tpp_action(){
        $user_id = $this->session->userdata('id');
        $nip = $this->session->userdata('B_02B');
        // $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $nips = $this->input->post('nips', TRUE);
        // $skp = $this->input->post('skp', TRUE);
        // $potongan = $this->input->post('potongan', TRUE);
        // $periode = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
        $gols = $this->input->post('gols', TRUE);
        $periode_bulan = getDataPeriodeTPP()->bulan;
        $periode_tahun = getDataPeriodeTPP()->tahun;
        $jabatans = $this->input->post('jabatans', TRUE);
        $sakit = $this->input->post('sakit', TRUE);
        // $tbs = $this->input->post('tb_' ., TRUE);
        // for($i=0; $i < count($tbs); $i++){
        //     echo "Selected " . $tbs[$i] . "<br/>";
        // }
        // echo json_encode($tbs);
        // die();
        // if($tb == null){
        //     $tb = 0;
        // }
        $skp = $this->input->post('skp', TRUE);
        $keterangan = $this->input->post('keterangan', TRUE);

        $tanggal = date('Y-m-d');
        $now = date('Y-m-d H:i:s');

        $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
        $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
        foreach ($dataAllMyVerifikator as $ki => $valVerif) {
            $idVerif[$ki] = $valVerif->id;
        }
        $allIdVerifikator = implode(",", $idVerif);
        if(count($dataAllMyVerifikator) > 0){
            $data_bulk = array();
            // foreach ($skp as $key => $value) {
            //     $data_bulk[$key] = array(
            //         'tanggal' => $tanggal,
            //         'periode' => $periode,
            //         'pegawai_id' => $pegawai_id[$key],
            //         'verifikator' => $allIdVerifikator,
            //         'status_ajuan' => 1,
            //         'status_acc' => 0,
            //         'posisi_acc' => getNipSekByKolok($myKolok),
            //         'posted_by' => $user_id,
            //         'nip_by' => $nip,
            //         'skp' => $skp[$key],
            //         'potongan' => $potongan[$key],
            //         'created_at' => $now
            //     );
            // }

            foreach ($nips as $key => $val) {
                $data_bulk[$key] = array(
                    'nip' => $nips[$key],
                    'bulan' => $periode_bulan,
                    'tahun' => $periode_tahun,
                    'gol' => $gols[$key],
                    'tunjab' => 0,
                    'jab' => $jabatans[$key],
                    'sakit' => $sakit[$key],
                    'tb' => $this->input->post('tb_' . $nips[$key]),
                    'skp' => $skp[$key],
                    'hari' => 0,
                    'jam' => 0,
                    'perilaku' => 0,
                    'hukdis' => 100,
                    'ket' => $keterangan[$key],
                    'petugas' => $nip,
                    'waktu' => $now


                    // 'verifikator' => $allIdVerifikator,
                    // 'status_ajuan' => 1,
                    // 'status_acc' => 0,
                    // 'posisi_acc' => getNipSekByKolok($myKolok),
                    // 'posted_by' => $user_id,
                    // 'nip_by' => $nip,
                    // 'skp' => $skp[$key],
                    // 'potongan' => $potongan[$key],
                    // 'created_at' => $now
                );
            }
        } else {
            //SKPD ybs blm pny verifikator
            $array_msg = array(
                'status'=>'error',
                'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai_p3k');
        }

        // echo json_encode($data_bulk);
        // die();

        $ins_bulk = $this->Fungsi_model->tambah_bulk('simgaji_masttpp_kinerja', $data_bulk);
        if($ins_bulk){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil mengusulkan data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_history_p3k');
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Gagal mengusulkan data'
            );
            redirect('tpp_pegawai_p3k');
        }
    }
}