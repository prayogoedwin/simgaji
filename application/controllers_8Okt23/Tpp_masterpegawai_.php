<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tpp_masterpegawai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->pegawais = 'simgaji_pegawai_tpp';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Tpp_masterpegawai_model');
        $this->load->model('Tpp_history_model');
        $this->load->model('Tpp_pegawai_model');

        $this->tb_pegawai = 'simgaji_pegawai_tpp';
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Master Pegawai TPP';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Master Pegawai TPP</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();
        $nip = $this->session->userdata('B_02B');
        // echo $nip;
        // die();
        $data['verifikator'] = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip ='$nip'");

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_tpp/index', $data);
    }

    //CRUD users START//
    public function get_pegawai()
    {
        // if($session['role'] == 2);

        $role = $this->session->userdata('role');

       
        
        

        $nip =  $this->session->userdata('B_02B');
        if($role == 1){
            $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE id = '$nip' AND deleted_at IS NULL");
        }else{
            $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$nip' AND deleted_at IS NULL");
        }

        // echo json_encode($x->row());
        // die();
       
        if($x->num_rows() > 0){
            $x = $x->row();
            // $role = $x->type_role;
            $skpd = $x->kode_skpd;
            $skpd_id = $x->id_skpd;
        }else{
            // $role = '';
            $skpd = '';
            $skpd_id = '';
        }

        
        // echo $skpd;
        // // echo substr($skpd,0,6);
        // die();

        // if($role == 2){
        //     $get = $this->Masterpegawai_tpps_model->get_pegawai_where($skpd);
        // }else{
        //     $get = $this->Masterpegawai_tpps_model->get_pegawai();
        // }

        if($role == 1){
           $get = $this->Tpp_masterpegawai_model->get_pegawai();
        }else{
            $get = $this->Tpp_masterpegawai_model->get_pegawai_where($skpd); 
            // echo json_encode($get);
            // die();
        }

        $datatable = array();
        foreach ($get as $key => $value) {

     
            // $merah = $value->tolak + $value->revisi;
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a target="BLANK" href="' . base_url('verifikasi_p3ks/detail/') . encode_url($value->pegawai_id) . '" >' . $value->B_02B . '</a>';
            // $klik_kode = '<a href="javascript:void(0)" onclick="detailData(' . $value->id . ')">' . $value->nip . '</a>';
            $x = encode_url($value->id);
            $klik_kode = '<a href="'.BASE_URL('tpp_masterpegawai/detail/'.$x).'">' . $value->nip . '</a>';
            $datatable[$key] = array(
                'nip' => $klik_kode,
                'nama' => $value->name,
                'lokasikerja' => $value->lokasi_kerja,
                'lokasigaji' => $value->lokasi_gaji,
                // 'id' => '<input type="checkbox" class="data-check" value="' . $value->id . '">',
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$(add),
            );
            
        }
        // $data['datatable'] = $datatable;

        if($role == 1){
            
            $output = array(
                "recordsTotal" => $this->Tpp_masterpegawai_model->pegawai_count_all(),
                "recordsFiltered" => $this->Tpp_masterpegawai_model->pegawai_count_filtered(),
                "data" => $datatable,
            );
        }else{

            $output = array(
                "recordsTotal" => $this->Tpp_masterpegawai_model->pegawai_count_all_skpd($skpd),
                "recordsFiltered" => $this->Tpp_masterpegawai_model->pegawai_count_filtered_skpd($skpd),
                "data" => $datatable,
            );
            
        }

        

        // $output = array(

        //     "recordsTotal" => array(),
        //     "recordsFiltered" => array(),
        //     "data" => $datatable,
        // );


        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function detail($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawai_tpp', $real_id);
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_pegawai_model->getDetailById($real_id);

            $data['warning'] = array(
                'pesan' => 'NIP tidak ditemukan di SIMPEG, silahkan hubungi admin'
            );

            $nip_ybs = $data['detail']->nip;
            $cekNip = getDataMastfip($nip_ybs);
            if($cekNip != null){
                $data['warning'] = array(
                    'pesan' => ''
                );
            }

            //notif for katu sekolah
            $n = $this->session->userdata('B_02B');
            $nipCabdin = $this->Tpp_pegawai_model->getNIPCabdinByNipKatuSekolah($n);
            if($nipCabdin != null){
              $data['info_sekolah'] = array(
                'message' => 'NIP KaTU Cabang Dinas',
                'NIP' => $nipCabdin
              );
            } else {
              $data['info_sekolah'] = array(
                'message' => 'Data NIP KaTU Cabang Dinas tidak ditemukan',
                'NIP' => 0
              );
            }
            

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('master/pegawai_tpp/detail_pegawai', $data);

            // echo json_encode($data['jabatansample']);
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai');
        
        }
    }

    public function add(){
        $data_header['session'] = $this->session->all_userdata();
        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_tpp/add_pegawai');

    }

    public function add_S(){

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_tpp/detail_pegawai', $data);

    }

    public function add_(){
        // $real_id = decode_url($id);
        $cek = TRUE;
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

                // $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
                $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();
                $data['kodes2'] = $this->Pegawai_p3k_model->getDataKodes2();
                // echo json_encode($data);
                // die();

                $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
                $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
                $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
                $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
                $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
                $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
                $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
                $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
                $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
                $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
                $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
                $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');
                
                $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
                $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
                // $data['masterjfu'] = array();
                // $data['masterjft'] = array();
                // $data['mastersotk'] = array();
                // $data['mastersotk'] = array();
            
                $this->load->view('template/head');
                $this->load->view('template/header', $data_header);
                $this->load->view('master/pegawai_p3k/add', $data);

                // header('Content-Type: application/json');
                // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('masterpegawai_p3ks');
        }
    }

    public function data($id){
        $real_id = decode_url($id);
        $cek = cek_realparams('simgaji_pegawaip3ks', $real_id);
        if($cek){
            $this->lanjutData($real_id);
        } else {

            $cek_cpns = cek_realparams('simgaji_pegawais', $real_id);
            if($cek_cpns){
                $this->lanjutDataCpns($real_id);
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Invalid parameter'
                );

                $this->session->set_flashdata($array_msg);
                redirect('masterpegawai_p3ks');
            }
        }
    }

    private function lanjutData($real_id){
        $data_header['session'] = $this->session->all_userdata();

        $data['detail'] = $this->Pegawai_p3k_model->getDetailById($real_id);
        $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();

        $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
        $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
        $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
        $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
        $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
        $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
        $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
        $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
        $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
        $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
        $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

        $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
        $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
        // $data['masterjfu'] = array();
        // $data['masterjft'] = array();
        // $data['mastersotk'] = array();
        // $data['mastersotk'] = array();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_p3k/data_pegawai', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    private function lanjutDataCpns($real_id){
        $data_header['session'] = $this->session->all_userdata();

        $data['detail'] = $this->Pegawai_model->getDetailById($real_id);
        $data['kodes'] = $this->Pegawai_model->getDataKodes();

        $data['lokasi'] = $this->Pegawai_model->getDataDataMaster('simgaji_lokasis');
        $data['gender'] = $this->Pegawai_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Pegawai_model->getDataDataMaster('simgaji_agamas');
        $data['marital'] = $this->Pegawai_model->getDataDataMaster('simgaji_maritals');
        $data['bool'] = $this->Pegawai_model->getDataDataMaster('simgaji_bools');
        $data['statusp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_statusp3ks');
        $data['status'] = $this->Pegawai_model->getDataDataMaster('simgaji_statuses');
        $data['golongan'] = $this->Pegawai_model->getDataDataMaster('simgaji_golongans');
        $data['golonganp3k'] = $this->Pegawai_model->getDataDataMaster('simgaji_golonganp3ks');
        $data['eselon'] = $this->Pegawai_model->getDataDataMaster('simgaji_eselons');
        $data['pendidikan'] = $this->Pegawai_model->getDataDataMaster('simgaji_pendidikans');
        $data['kedudukan'] = $this->Pegawai_model->getDataDataMaster('simgaji_kedudukans');

        $data['fungsional'] = $this->Pegawai_model->getDataDataMaster('simgaji_fungsionals');
        $data['profesi'] = $this->Pegawai_model->getDataDataMaster('simgaji_profesis');
        // $data['masterjfu'] = array();
        // $data['masterjft'] = array();
        // $data['mastersotk'] = array();
        // $data['mastersotk'] = array();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/pegawai_p3k/data_pegawai', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    

   # fungsi untuk mengecek status username dari db
	function cek_nip(){
		# ambil username dari form
		$nip = $_POST['nip'];
						# select ke model member username yang diinput user
		$hasil_nip = $this->Tpp_masterpegawai_model->cek_nip($nip);
	
		if($hasil_nip == 1){
			echo "1";
		}else{
			echo "0";
		}

    }

    public function add_action(){

        foreach ($_POST as $key => $value) {
            $data[htmlspecialchars($key)] = htmlspecialchars($value); 
        }
        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);
        $x = $this->db->insert($this->tb_pegawai, $merge);

        $bulan = getDataPeriode()->bulan;
        $tahun = getDataPeriode()->tahun;

        $periode = $tahun.'-'.$bulan.'-01';
        
        $pegawai_id = $this->db->insert_id();
        if($x){
            $arr = array(
                'nip_verifikator' => $this->session->userdata('B_02B'),
                'ip_address' => $this->input->ip_address(),
                'log_by'  => $this->session->userdata('id'),
                'log_at'  => date('Y-m-d H:i:s'),
                'periode' => $periode,
                'perubahan_pada' => 'pegawai_p3k'
            );
            $merge2 = array_replace($arr, $merge);
            $this->db->insert('simgaji_pegawaip3ks_log', $merge2);
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Tambah Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Tambah Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action(){
        $user_id = $this->session->userdata('id');
        $role = $this->session->userdata('role');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
       
        $now = date('Y-m-d H:i:s');
        if(count($kode_id) > 0){

            $this->db->where('id', $pegawai_id);
            $pgw = $this->db->get($this->pegawais)->row();

            $nip = $pgw->nip;

            $datahistory = array();
            $idVerif = array();
            foreach ($kode_id as $key => $value) {

                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);
                $nilai_lama = $this->Tpp_pegawai_model->getDetailById($pegawai_id)->$field_name;
                $nilai_baru = $this->input->post($field_name, TRUE);

                // $datahistory[$key] = array(
                //     'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                //     'pegawai_id' => $pegawai_id,
                //     'kode_id' => $value,
                //     'lama' => $nilai_lama,
                //     'baru' => $nilai_baru,
                //     'verifikator' => $this->session->userdata('B_02B'),
                //     'created_at' => $now
                // );

                $datahistory[$key] = array(
                    'nip_verifikator' => $this->session->userdata('B_02B'),
                    'nip_ybs' => $nip,
                    'lama' => $nilai_lama,
                    'baru' => $nilai_baru,
                    'ip_address' => $this->input->ip_address(),
                    'created_by'  => $this->session->userdata('id'),
                    'created_at'  => date('Y-m-d H:i:s'),
                    'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                    'kode_id'   => $value,
                    'perubahan_pada' => 'pegawai_tpp',
                );

                $data = array(
                    $field_name => $nilai_baru,
                );

                $this->db->where('id', $pegawai_id);
                $this->db->update($this->pegawais, $data);

                if($field_name == 'lokasi_gaji'){

                    $a = $nilai_baru;
                    $x = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$a'")->row();

                    $data_x = array(
                        'lokasi' => $x->kode,
                        'lokasi_gaji' => $x->id,
                        'KOLOK' => $x->kolok_simpeg,
                        'A_01' => substr($x->kolok_simpeg,0,2),
                        'unor' => $x->kolok_simpeg,
                    );

                    $this->db->where('id', $pegawai_id);
                    $this->db->update($this->pegawais, $data_x);

                }

                if($field_name == 'I_05'){

                    $b = $nilai_baru;
                    $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$b'")->row();
                    
                    if(strlen($b) == 5){

                        $jenjang =  $this->input->post('I_07', TRUE);

                        $data_z = array(
                            'I_JB' => textFunsional($b, $jenjang),
                        );

                        // echo json_encode($data_z);
                        // die();
                    }else{
                        $data_z = array(
                            'I_JB' => textJabatan($b),
                        );
                    }
                    

                    $this->db->where('id', $pegawai_id);
                    $this->db->update($this->pegawais, $data_z);

                }

                if($field_name == 'lokasi_kerja'){

                    $y_ = $nilai_baru;
                    $y = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y_'")->row();

                    $data_y = array(
                        'lokasi_kerja' => $y->id,
                        'A_01' => substr($y->kolok_simpeg,0,2)
                    );

                    $this->db->where('id', $pegawai_id);
                    $this->db->update($this->pegawais, $data_y);

                }

                if($field_name == 'lokasi_kerja_plt'){

                    $p_ = $nilai_baru;
                    $p = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$p_'")->row();

                    $data_p = array(
                        'lokasi_gaji_plt' => $p->kode,
                        'unor_plt' => $p->kolok_simpeg
                    );

                    $this->db->where('id', $pegawai_id);
                    $this->db->update($this->pegawais, $data_p);

                }

                    

                    //get beban kerja
                    $this->dbeps->where('kelas', $pgw->kelasjab);
                    

                    if($pgw->kelasjab == 15){
                        $this->dbeps->where('kdjab', $pgw->I_05);
                    }

                    if(array_ka_waka($pgw->I_05) == true){
                        $this->dbeps->where('kdjab', $pgw->I_05);
                    }

                    if(strlen($pgw->I_05) == 5){
                        
                        if($pgw->I_05 == '00053'){
                            $this->dbeps->where('jenjang', $pgw->I_07);
                        }else if($pgw->I_05 == '00018'){
                            $this->dbeps->where('koord', $pgw->K_01);
                            $this->dbeps->where('kdjab', $pgw->I_05);
                        }else{
                            $this->dbeps->where('gol', pengelompokan_golongan($pgw->golongan_id,0,1));
                        }
                    }  
                    
                    if($pgw->K_01 == 1){
                        $this->dbeps->where('koord', 1);
                    }
                    
                    // if(strlen($pgw->I_05) == 10){
                    //     if($pgw->I_07 < 13){
                    //         $this->dbeps->where('gol', pengelompokan_golongan($pgw->golongan_id));
                    //     }
                    // }

                    $bb = $this->dbeps->get('TABREFTPPKELASJAB');
                    if($bb->num_rows() > 0){
                        $beban_kerja = $bb->row()->nominal;
                    }else{
                        $beban_kerja = 0;
                    }
                    //end beban kerja

                    //start beban kerja khusus
                    $beban_kerja_khusus = 0;
                    if(beban_kerja_khusus($pgw->KOLOK) == TRUE){
                        

                        if($pgw->A_01 == 'A2'){
                            $x = substr($pgw->KOLOK,0,7).'%';
                        }

                        if($pgw->A_01 == 'C1'){
                            $x = $pgw->A_01.'%';
                        }
                        $this->dbeps->like('unor',$x,'AFTER');
                        $this->dbeps->where('kelas', $pgw->kelasjab);
                        $bbk = $this->dbeps->get('TABREFTPPKELASJAB_BEBAN');
                        if($bbk->num_rows() > 0){
                            $beban_kerja_khusus = $bbk->row()->nominal;
                        }else{
                            $beban_kerja_khusus = 0;
                        }  

                        // echo $x;
                        // die();
                    }
                    //end beban kerja khusus

                    //start tempat kerja
                    $tempat_kerja = 0;
                    if(tempat_kerja($pgw->lokasi) == TRUE){

                        $this->dbeps->like('unor', $pgw->KOLOK);
                        $this->dbeps->where('kelas', $pgw->kelasjab);

                        // if($pgw->is_spesialis != '' || $pgw->is_spesialis != 0){
                        //     $this->dbeps->where('spesialis', $pgw->is_spesialis);
                        // }

                        if($pgw->A_01 == 'D0'){
                            $this->dbeps->where('kdjab', $pgw->I_05);
                        }
                       
                        $tk = $this->dbeps->get('TABREFTPPKELASJAB_TEMPAT');
                        if($tk->num_rows() > 0){
                            $tempat_kerja = $tk->row()->nominal;
                        }else{
                            $tempat_kerja = 0;
                        }  
                    }
                    //end tempat kerja


                    //start kondisi kerja
                    $kondisi_kerja = 0;
                    if(kondisi_kerja($pgw->lokasi) == TRUE){

                        $dua = substr($pgw->KOLOK, 0, 2);
                        if($dua == '83' || $dua == '84' | $dua == '85' | $dua == '86'){
                            $this->dbeps->like('unor', $dua, 'AFTER');
                        }

                        $enam = substr($pgw->KOLOK, 0, 6);
                        if($enam == 'A20024'){
                            $this->dbeps->like('unor', $enam, 'AFTER');
                        }

                        $enam = substr($pgw->KOLOK, 0, 6);
                        if($dua == 'D5'){
                            $this->dbeps->like('unor', $enam, 'AFTER');
                        }

                        $this->dbeps->where('kelas', $pgw->kelasjab);

                        $hit = strlen($pgw->I_05);
                        if($hit == 10 && $dua == 'D0'){
                            $this->dbeps->where('kdjab', $pgw->I_05);
                        }

                        if($pgw->I_05 == '00018'){
                            $this->dbeps->where('kdjab', $pgw->I_05);
                        }

                        $kk = $this->dbeps->get('TABREFTPPKELASJAB_KONDISI');
                        if($kk->num_rows() > 0){
                            $kondisi_kerja = $kk->row()->nominal;
                        }else{
                            $kondisi_kerja = 0;
                        } 
                        //belum masih proses
                          
                    }
                    //end kondisi kerja


                    //start IS PLT
                    $beban_kerja_plt = 0;
                    if($pgw->is_plt == 1){

                        $this->dbeps->where('kdjab', $pgw->unor_plt);
                        $plt = $this->dbeps->get('TABREFTPPKELASJAB');
                        if($plt->num_rows() > 0){
                            $beban_kerja_plt = (10/100)*$plt->row()->nominal;
                        }else{
                            $beban_kerja_plt = 0;
                        }
    
                    }
                    //end IS PLT

                    $data_tpp = array(
                        'beban_kerja' => $beban_kerja,
                        'beban_kerja_khusus' => $beban_kerja_khusus,
                        'tempat' => $tempat_kerja,
                        'kondisi' => $kondisi_kerja,
                        'nominal_plt' => $beban_kerja_plt
                    );

                    // echo json_encode($data_tpp);
                    // die();

                    $this->db->where('id', $pegawai_id);
                    $this->db->update($this->pegawais, $data_tpp);


            }

            // echo json_encode($datahistory);
            // die();

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_log_verifikator', $datahistory);

            $array_msg = array(
                'status' => 'success',
                'message' => 'Berhasil merubah data'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_masterpegawai/detail/'.encode_url($pegawai_id));

        }

    }

    public function update_action_old(){
        
        $pegawai_id = $this->input->post('id');
        foreach ($_POST as $key => $value) {
            $data[htmlspecialchars($key)] = htmlspecialchars($value); 
        }

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        echo json_encode($merge);
        die();

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_3(){


        $pegawai_id = $this->input->post('id');
        $kode_id = $this->input->post('kode_id', TRUE);
        $nip = $this->input->post('nip', TRUE);

        // echo json_encode($kode_id);
        // die();

        if(count($kode_id) > 0){
        $datahistory = array();

        $bulan = getDataPeriode()->bulan;
        $tahun = getDataPeriode()->tahun;
        $periode = $tahun.'-'.$bulan.'-01';

            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);
                $nilai_lama = $this->Pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;
                $nilai_baru = $this->input->post($field_name, TRUE);

                // $datahistory[$key] = array(
                //     'nip_verifikator' => $this->session->userdata('B_02B'),
                //     'nip_ybs' => $nip,
                //     'lama' => $nilai_lama,
                //     'baru' => $nilai_baru,
                //     'ip_address' => $this->input->ip_address(),
                //     'created_by'  => $this->session->userdata('id'),
                //     'created_at'  => date('Y-m-d H:i:s'),
                //     'periode'   => $periode,
                //     'kode_id'   => $kode_id
                // );

                $datahistory[] = array(
                    'nip_verifikator' => $this->session->userdata('B_02B'),
                    'nip_ybs' => $nip,
                    'lama' => $nilai_lama,
                    'baru' => $nilai_baru,
                    'ip_address' => $this->input->ip_address(),
                    'created_by'  => $this->session->userdata('id'),
                    'created_at'  => date('Y-m-d H:i:s'),
                    'periode'   => $periode,
                    'kode_id'   => $kode_id[$key],
                    'perubahan_pada' => 'pegawai_p3k',
                );

                // $this->db->insert('simgaji_log_verifikator', $datahistory);
            }
        }

        // echo json_encode($datahistory);
        // die();
        $ins = $this->Fungsi_model->tambah_bulk('simgaji_log_verifikator', $datahistory);

        $pegawai_id = $this->input->post('id');
        foreach ($_POST as $key => $value) {
           
                $data[htmlspecialchars($key)] = htmlspecialchars($value);  
           
        }

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode,
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('masterpegawai_p3ks/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_2(){

        error_reporting(0);
        $pegawai_id = $this->input->post('id');
        $kode_id = $this->input->post('kode_id', TRUE);
        $data = [];
        $data2 = [];
        $update = [];

        foreach ($_POST as $key => $value) {

            
            $asli = substr($key,0,5);
            if(substr($key,0,5) != 'asli_'){
                // $data[$key] = $value; 
                $data[htmlspecialchars($key)] = htmlspecialchars($value); 

            }else if(substr($key,0,5) == 'asli_'){
                $data2[$key] = $value; 
                $data2[htmlspecialchars($key)] = htmlspecialchars($value); 
                echo $data2[$key][$value];
            }
        }


        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_2_proses(){

        error_reporting(0);
        $pegawai_id = $this->input->post('id');
        $data = [];
        $data2 = [];
        $update = [];

        foreach ($_POST as $key => $value) {

            $x = cekDataPns('simgaji_pegawaip3ks', $pegawai_id);
            // echo $x->nip;
            // echo '<br/>';
            // echo $x->$key;
            if($x->$key != '1961020120212110wer'){
                // echo 'asli<br/>';
                // echo $x->$key.'<br/>';
                // echo 'perubahan<br/>';
                // echo $x->$key.'<br/>';

                // $update[$key] = $value;
                // break;
            }else{
                '';
            }
            //     echo $x->$value;
            // }
            // $asli = substr($key,0,5);
            // if(substr($key,0,5) != 'asli_'){
            //     $data[$key] = $value; 
                

            // }else if(substr($key,0,5) == 'asli_'){
            //     $data2[$key] = $value; 
            //     // $data2[htmlspecialchars($key)] = htmlspecialchars($value); 
            //     // echo $data2[$key][$value];
            // }
            // // echo $data2[$key][$value];
                
            //     // die();
            //     if($data2[$value] != $data[$value]){

            //         // echo $data[$key][$value];
            //         // echo '<br/>';
            //         // echo $data2[$key][$value];
            //         $update = 1;
            //         // break;
            //     }

              
            
            
        }
        // echo json_encode($data);

        // echo json_encode($update);
        die();

        $x =  $this->input->post('lokasi_kerja');
        $y =  $this->input->post('lokasi_gaji');
        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$x'")->row();
        $a = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$y'")->row();

        $add = array(
            'lokasikerja' => $z->kode,
            'lokasigaji' => $a->kode
            );
        $merge = array_replace($add, $data);

        $this->db->where('id', $pegawai_id);
        $x = $this->db->update($this->tb_pegawai, $merge);
        if($x){
            $array_msg = array(
                'status'=>'success',
                'message'=>'Berhasil Edit Data'
             );
        }else{
            $array_msg = array(
                'status'=>'danger',
                'message'=>'Gagal Edit Data'
            );
        }
        
        $this->session->set_flashdata($array_msg);
        redirect('pegawai_p3k/data/'.encode_url($pegawai_id));

        

    }

    public function update_action_0(){
        $user_id = $this->session->userdata('id');
        // $id_lokasis = $this->session->userdata('id_lokasis');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $now = date('Y-m-d H:i:s');

        if(count($kode_id) > 0){
            $datahistory = array();
            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);

                $nilai_lama = $this->Pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;

                $nilai_baru = $this->input->post($field_name, TRUE);

                $A_01 = $this->session->userdata('A_01');
                $A_02 = $this->session->userdata('A_02');

                if($A_01 == 'D0' && $A_02 != '00'){
                    //bukan SKPD induk, turunan disdik prov. jenjang here

                    // $kodeumpeg_atasan = getKodeCabdinSekolah($A_01, $A_02, $A_03, $A_04, $A_05);
					// $lokasi = getNalokFromKolok($kodeumpeg_atasan);

					// $nip_acc = getNipUmpegCabdinByLokasi($posisi_acc);

                    $nip = $this->input->post('nipe', TRUE);
                    $nama = $this->input->post('nama', TRUE);
                    $jabatan = $this->input->post('jabatan', TRUE);

                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
                    $myCabdinKolok = getKolokCabdin($myKolok);
                    $fix_myCabdinKolok = getKolokCabdin($myCabdinKolok); //fix perubahan tablokb08

                    $dataMyVerifikator = getMyVerifikator($fix_myCabdinKolok);
                    if($dataMyVerifikator != null){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => 'Data berhasil diusulkan',
                            'verifikator' => $dataMyVerifikator->id,
                            'status_ajuan' => 0,
                            'posisi_acc' => $nip,
                            'posted_by' => $user_id,
                            'created_at' => $now
                        );
                    } else {
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'Sekolah / Cabdin anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }

                } else {
                    //SKPD induk prov
                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

                    // $dataMyVerifikator = getMyVerifikator($myKolok);

                    $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
                    foreach ($dataAllMyVerifikator as $key => $valVerif) {
                        $idVerif[] = $valVerif->id;
                    }
                    $allIdVerifikator = implode(",", $idVerif);
                    if(count($dataAllMyVerifikator) > 0){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => 'Data berhasil diusulkan',
                            // 'verifikator' => $dataMyVerifikator->id,
                            'verifikator' => $allIdVerifikator,
                            'status_ajuan' => 1,
                            // 'posisi_acc' => $dataMyVerifikator->nip,
                            'posisi_acc' => getNipSekByKolok($myKolok),
                            'posted_by' => $user_id,
                            'created_at' => $now
                        );
                    } else {
                        //SKPD ybs blm pny verifikator
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('pegawai_p3k');
                    }
                }

            }

            // echo json_encode($datahistory);

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_historyp3ks', $datahistory);
            if($ins){
                //START add history acc ke verifikator
                // $data_acc = array(
                //     'pegawais_id' => $pegawai_id,
                //     'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                //     'nip' => $dataMyVerifikator->nip,
                //     'nama' => $dataMyVerifikator->nama,
                //     'jabatan' => 'Verifikator',
                //     'status_acc' => 0,
                //     'is_plt' => 0,
                //     'created_at' => $now
                // );
                // $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
                //END add history acc ke verifikator

                //add notif inbox2 for verifikator
                // $A_01 = $this->session->userdata('A_01');
                // $B_02B = $this->session->userdata('B_02B');
                // $B_03A = getDataMastfip($B_02B)->B_03A;
				// $B_03 = getDataMastfip($B_02B)->B_03;
				// $B_03B = getDataMastfip($B_02B)->B_03B;
                // tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Ada usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $dataMyVerifikator->nip, '0');

                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('history_p3k');
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('pegawai_p3k');
            }
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Pastikan ada data yang dipilih'
            );

            $this->session->set_flashdata($array_msg);
            redirect('pegawai_p3k/detail/' . encode_url($pegawai_id));
        }

    }

    public function hapus_action(){
        $id = $this->input->post('id', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $hps = $this->Fungsi_model->hapus($this->tb_pegawai, $id, $data);
        if($hps){
            $reponse = array(
                'status' => 1,
                'message' => 'Berhasil menghapus data'
            );
        } else {
            $reponse = array(
                'status' => 0,
                'message' => 'Gagal menghapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($reponse);
    }

   

}
?>