<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nominatif_tpp_pot_pegawai extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Nominatif_tpp_pot_pegawai_model');
		$this->load->model('Tpp_pot_pegawai_model');

        $role = $this->session->userdata('role');
        if( $role != 3 && $role != 8){
            echo'<script>alert("Anda tidak diperkenankan mengakses halaman ini. Hubungi Administrator !");window.location.href = "'.base_url('dashboard').'";</script>';       
        }
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();
        $A_01 = $this->session->userdata('A_01');
        $nip = $this->session->userdata('B_02B');
        $role = $this->session->userdata('role');
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;
        $kode_lokasis = getDetailLokasisById($lokasis_id)->kode;
        $kolok_simpeg = getDetailLokasisById($lokasis_id)->kolok_simpeg;
        
        // $periodetpp = getDataPeriodeTPP()->tahun . '-' . getDataPeriodeTPP()->bulan . '-01';
        // $tahun_tpp = getDataPeriodeTPP()->tahun;
        // $bulan_tpp = getDataPeriodeTPP()->bulan;

        
        $data['subunit_disdik'] = $this->Tpp_pot_pegawai_model->get_uptdisdik()->result();

        if(isset($_GET['bul']) && isset($_GET['thn'])){
            $kd = null;
            $data['filter2'] = array(
                'kodedrop' => null
            );
            if($A_01 == 'D0'){
                $kd = $this->input->get('kode', TRUE);
                $data['filter2'] = array(
                    'kodedrop' => $kd
                );
            }
            $bulan_tpp = $this->input->get('bul', TRUE);
            $tahun_tpp = $this->input->get('thn', TRUE);

            $get = $this->Nominatif_tpp_pot_pegawai_model->getDataNominatif($tahun_tpp, $bulan_tpp, $nip, $kd);

            $datatable = array();
            foreach ($get as $key => $value) {

                $fname = getDataMastfip($value->nip)->B_03A. ' ' .getDataMastfip($value->nip)->B_03. ' ' .getDataMastfip($value->nip)->B_03B;

                $datatable[$key] = array(
                    'nama_nip' => $value->nip.'</br>'.$fname.'</br>'.getDataPangkatGol($value->gol)->NAMAX,
                    'kelas_jab' => $value->kelas_jab,
                    'jabatan' => $value->jab,
                    'cs' => $value->sakit,
                    'tb' => $value->tb,
                    'skp' => $value->skp,
                    'pksp' => 0,
                    'alpha' => $value->hari,
                    'kwk' => $value->jam,
                    'hukdis' => $value->hukdis,
                    'atlit_pelatih' => '',
                    'keterangan' => $value->ket
                );
            }
            $data['datatable'] = $datatable;

            $data['filter'] = array(
                'bl' => $bulan_tpp,
                'th' => $tahun_tpp,
                'jml_record' => count($datatable)
            );
        } else {
            $data['datatable'] = array();

            $data['filter'] = array(
                'bl' => getDataPeriodeTPP()->bulan,
                'th' => getDataPeriodeTPP()->tahun,
                'jml_record' => count($data['datatable']),
            );

            $data['filter2'] = array(
                'kodedrop' => null
            );
        }

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('nominatif_tpp_pot_pegawai/nominatif_tpp_pot_pegawai_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($get);
    }

    public function cetak_usulan_tpp(){
        // $cetak100 = isset($_GET['c100']);
        // echo $cetak100;
        // die();
        
        $A_01 = $this->session->userdata('A_01');
        $nip = $this->session->userdata('B_02B');
        $bulan = $this->input->get('bulan', TRUE);
        $tahun = $this->input->get('tahun', TRUE);
        $kode = null;

        if(($bulan != null || $bulan != '') && ($tahun != null || $tahun != '')){
            $cetak100 = null;
            if(isset($_GET['c100'])){
                $cetak100 = isset($_GET['c100']);
            }

            $kode = null;
            if($A_01 == 'D0'){
                $kode = $this->input->get('kode', TRUE);
            }
            $get = $this->Nominatif_tpp_pot_pegawai_model->getDataNominatifCetak($tahun, $bulan, $nip, $cetak100, $kode);

            $datatable = array();
            foreach ($get as $key => $value) {

                $fname = getDataMastfip($value->nip)->B_03A. ' ' .getDataMastfip($value->nip)->B_03. ' ' .getDataMastfip($value->nip)->B_03B;

                $datatable[$key] = array(
                    'nip' => $value->nip,
                    'nama' => $fname,
                    'nalok' => $value->NALOK,
                    'pkt_gol' => getDataPangkatGol($value->gol)->NAMAX,
                    'kelas_jab' => $value->kelas_jab,
                    'jabatan' => $value->jab,
                    'cs' => $value->sakit,
                    'tb' => $value->tb,
                    'skp' => $value->skp,
                    'alpha' => $value->hari,
                    'kwk' => $value->jam,
                    'k6_k7' => $value->perilaku,
                    'hukdis' => $value->hukdis,
                    'atlit_pelatih' => '',
                    'keterangan' => $value->ket,
                    'waktu' => $value->waktu
                );
            }
            $data['datanya'] = $datatable;

            $data['filter'] = array(
                'bl' => $bulan,
                'th' => $tahun
            );

            $data['kepala'] = $this->Nominatif_tpp_pot_pegawai_model->getRowKepala($A_01);
            $data['unor'] = $this->Nominatif_tpp_pot_pegawai_model->getRowSKPDInduk($A_01)->NALOK;

            $data['subunor_disdik'] = null;
            if($kode != null){
                $data['subunor_disdik'] = $this->Nominatif_tpp_pot_pegawai_model->getUkDisdik($kode)->NALOK;
            }
            

            $this->load->view('nominatif_tpp_pot_pegawai/cetak_usulan_tpp', $data);
            // echo json_encode($data);
        } else {
            echo "Invalid parameter";
        }
    }

    public function cetak_instrumen_penilaian(){
        $A_01 = $this->session->userdata('A_01');
        $nip = $this->session->userdata('B_02B');
        $bulan = $this->input->get('bulan', TRUE);
        $tahun = $this->input->get('tahun', TRUE);

        if(($bulan != null || $bulan != '') && ($tahun != null || $tahun != '')){
            $cetak100 = null;
            if(isset($_GET['c100'])){
                $cetak100 = isset($_GET['c100']);
            }

            $kode = null;
            if($A_01 == 'D0'){
                $kode = $this->input->get('kode', TRUE);
            }
            $get = $this->Nominatif_tpp_pot_pegawai_model->getDataNominatifCetak($tahun, $bulan, $nip, $cetak100, $kode);

            $datatable = array();
            foreach ($get as $key => $value) {

                $fname = getDataMastfip($value->nip)->B_03A. ' ' .getDataMastfip($value->nip)->B_03. ' ' .getDataMastfip($value->nip)->B_03B;

                $datatable[$key] = array(
                    'unit_kerja' => getDataMastfip($value->nip)->A_01,
                    'nip' => $value->nip,
                    'nama' => $fname,
                    'nalok' => $value->NALOK,
                    'pkt_gol' => getDataPangkatGol($value->gol)->NAMAX,
                    'jabatan' => $value->jab,
                    'kelas_jab' => $value->kelas_jab,

                    'skp' => $value->skp,
                    'perilaku' => $value->perilaku,
                    'hukdis' => $value->hukdis,

                    'lhkpn' => $value->lhkpn,
                    'lhkasn' => $value->lhkasn,
                    'bmd' => $value->bmd,
                    'grat' => $value->grat,
                    'tptgr' => $value->tptgr,

                    'nominalskp' => $value->nominalskp,
                    'nominalperilaku' => $value->nominalperilaku,
                    'nominalkurang' => $value->nominalkurang,
                    'nominalkurangkepatuhan' => $value->nominalkurangkepatuhan,
                    'nominaltpp' => $value->nominaltpp,
                    'nominaldefault' => $value->nominal_bebankerja,

                    'nominal_bebankerjakhusus' => $value->nominal_bebankerjakhusus,
                    'nominal_tempat' => $value->nominal_tempat,
                    'nominal_kondisi' => $value->nominal_kondisi,
                    
                    'waktu' => $value->waktu
                );
            }
            $data['datanya'] = $datatable;

            $data['filter'] = array(
                'bl' => $bulan,
                'th' => $tahun
            );

            $data['skpd_induk'] = $this->Nominatif_tpp_pot_pegawai_model->getRowSKPDInduk($A_01);
            $data['kepala'] = $this->Nominatif_tpp_pot_pegawai_model->getRowKepala($A_01);

            $data['subunor_disdik'] = null;
            if($kode != null){
                $data['subunor_disdik'] = $this->Nominatif_tpp_pot_pegawai_model->getUkDisdik($kode)->NALOK;
            }

            $this->load->view('nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian', $data);
            // echo json_encode($data);
        } else {
            echo "Invalid parameter";
        }
    }

    public function cetak_rekap_tpp(){
        $A_01 = $this->session->userdata('A_01');
        $nip = $this->session->userdata('B_02B');
        $bulan = $this->input->get('bulan', TRUE);
        $tahun = $this->input->get('tahun', TRUE);

        if(($bulan != null || $bulan != '') && ($tahun != null || $tahun != '')){
            $cetak100 = null;
            if(isset($_GET['c100'])){
                $cetak100 = isset($_GET['c100']);
            }

            $kode = null;
            if($A_01 == 'D0'){
                $kode = $this->input->get('kode', TRUE);
            }

            $get = $this->Nominatif_tpp_pot_pegawai_model->getDataNominatifCetak($tahun, $bulan, $nip, $cetak100, $kode);

            $datatable = array();
            foreach ($get as $key => $value) {

                $fname = getDataMastfip($value->nip)->B_03A. ' ' .getDataMastfip($value->nip)->B_03. ' ' .getDataMastfip($value->nip)->B_03B;

                $datatable[$key] = array(
                    'unit_kerja' => getDataMastfip($value->nip)->A_01,
                    'nip' => $value->nip,
                    'nama' => $fname,
                    'nalok' => $value->NALOK,
                    'pkt_gol' => getDataPangkatGol($value->gol)->NAMAX,
                    'jabatan' => $value->jab,
                    'kelas_jab' => $value->kelas_jab,

                    'skp' => $value->skp,
                    'perilaku' => $value->perilaku,
                    'hukdis' => $value->hukdis,

                    'lhkpn' => $value->lhkpn,
                    'lhkasn' => $value->lhkasn,
                    'bmd' => $value->bmd,
                    'grat' => $value->grat,
                    'tptgr' => $value->tptgr,

                    'nominalskp' => $value->nominalskp,
                    'nominalperilaku' => $value->nominalperilaku,
                    'nominalkurang' => $value->nominalkurang,
                    'nominalkurangkepatuhan' => $value->nominalkurangkepatuhan,
                    'nominaltpp' => $value->nominaltpp,
                    'nominaldefault' => $value->nominal_bebankerja,
                    
                    'ket' => $value->ket,
                    'waktu' => $value->waktu
                );
            }
            $data['datanya'] = $datatable;

            $data['filter'] = array(
                'bl' => $bulan,
                'th' => $tahun
            );

            $data['kepala'] = $this->Nominatif_tpp_pot_pegawai_model->getRowKepala($A_01);
            $data['unor'] = $this->Nominatif_tpp_pot_pegawai_model->getRowSKPDInduk($A_01)->NALOK;

            $data['subunor_disdik'] = null;
            if($kode != null){
                $data['subunor_disdik'] = $this->Nominatif_tpp_pot_pegawai_model->getUkDisdik($kode)->NALOK;
            }

            $this->load->view('nominatif_tpp_pot_pegawai/cetak_rekap_tpp', $data);
            // echo json_encode($data);
        } else {
            echo "Invalid parameter";
        }
    }
}