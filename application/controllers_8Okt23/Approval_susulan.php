<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_susulan extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Approval_susulan_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('approval_susulan/approval_susulan_index');
    }

	public function read(){
		$postData = $this->input->post();
		$nip = $this->session->userdata('B_02B');
		$bulan = date('m');
		$tahun = date('Y');

        $data = $this->Approval_susulan_model->getData($postData, $nip, $bulan, $tahun);

        echo json_encode($data);
	}

	public function ajukan_action(){
		//sekolah
		$nip = $this->session->userdata('B_02B');
		$userid = $this->session->userdata('id');
		$id_lokasis = $this->session->userdata('id_lokasis');
		$bulan = date('m');
		$tahun = date('Y');
		$now = date('Y-m-d H:i:s');

		$A_01 = $this->session->userdata('A_01');
		$B_03A = getDataMastfip($nip)->B_03A;
		$B_03 = getDataMastfip($nip)->B_03;
		$B_03B = getDataMastfip($nip)->B_03B;

		$kodeKasubbagKeu = cari_kode_kasubbagkeu($A_01); //kode kasubbag keuangan disdik prov
		$nipDanPlt = getNipByKolokStruktural($kodeKasubbagKeu);
		$nipKasubbagKeu = $nipDanPlt['nip'];
		$is_plt = $nipDanPlt['is_plt'];
		$unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
		$B_03AKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03A;
		$B_03KasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03;
		$B_03BKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03B;
		$namaKasubbagKeu = $B_03AKasubbagKeu . ' ' . $B_03KasubbagKeu . ' ' . $B_03BKasubbagKeu;

		//add notif
		tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Susulan Gaji Pegawai', $nip, $B_03A, $B_03, $B_03B, $A_01, $nipKasubbagKeu, '0');

		$teksJabatan = 'KASUBBAG KEUANGAN SKPD';
		if($is_plt == 1){
			$teksJabatan = 'PLT KASUBBAG KEUANGAN SKPD';
		}

		//START add history acc
		$data_acc = array(
			'A_01' => $A_01,
			// 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
			// 'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
			'nip' => $nipKasubbagKeu,
			'nama' => $namaKasubbagKeu,
			'jabatan' => $teksJabatan,
			'status_acc' => 0,
			'is_plt' => $is_plt,
			'unit_kerja_plt' => $unit_kerja_plt,
			'posted_by' => $this->session->userdata('id'),
			'created_at' => $now
		);
		$this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
		//END add history acc

		$get = $this->Approval_susulan_model->getDataApp($nip, $bulan, $tahun);
		foreach ($get as $key => $value) {
			$updateArray[] = array(
				'id'=>$value->id,
				'posisi_acc' => $nipKasubbagKeu,
				'cabdin_by' => $userid,
				'cabdin_nip' => $nip,
				'updated_at' => $now
			);
		}

		$this->db->update_batch('simgaji_susulan',$updateArray,'id');
		// $this->Approval_susulan_model->edit_batch_bynip('simgaji_susulan', $updateArray, $nipKasubbagKeu);
		// echo json_encode($updateArray);

		$response = array(
			'status' => 1,
			'message' => 'Berhasil'
		);

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($response);
	}

	public function ajukancabdin_action(){
		//cabdin
		$bulan = date('m');
		$tahun = date('Y');
		$now = date('Y-m-d H:i:s');

		$user_id = $this->session->userdata('id');
		$A_01 = $this->session->userdata('A_01');
		$B_02B = $this->session->userdata('B_02B');
		$B_03A = getDataMastfip($B_02B)->B_03A;
		$B_03 = getDataMastfip($B_02B)->B_03;
		$B_03B = getDataMastfip($B_02B)->B_03B;

		$kodeKasubbagKeu = cari_kode_kasubbagkeu($A_01); //kode kasubbag keuangan disdik prov
		$nipDanPlt = getNipByKolokStruktural($kodeKasubbagKeu);
		$nipKasubbagKeu = $nipDanPlt['nip'];
		$is_plt = $nipDanPlt['is_plt'];
		$unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
		$B_03AKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03A;
		$B_03KasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03;
		$B_03BKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03B;
		$namaKasubbagKeu = $B_03AKasubbagKeu . ' ' . $B_03KasubbagKeu . ' ' . $B_03BKasubbagKeu;

		//add notif
		tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Susulan Gaji Pegawai', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKasubbagKeu, '0');

		$teksJabatan = 'KASUBBAG KEUANGAN SKPD';
		if($is_plt == 1){
			$teksJabatan = 'PLT KASUBBAG KEUANGAN SKPD';
		}

		//START add history acc
		$data_acc = array(
			'A_01' => $A_01,
			'kode_lokasis' => '34', //34 2 digit kode induk DIsdik Prov
			// 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
			'nip' => $nipKasubbagKeu,
			'nama' => $namaKasubbagKeu,
			'jabatan' => $teksJabatan,
			'status_acc' => 0,
			'is_plt' => $teksJabatan,
			'unit_kerja_plt' => $unit_kerja_plt,
			'posted_by' => $this->session->userdata('id'),
			'created_at' => $now
		);
		$this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
		//END add history acc

		// START UPDATE HISTORY ACC
		$data_upd = array(
			'status_acc' => 1,
			'updated_at' => $now
		);
		$this->Approval_susulan_model->editByNipByBulanTahun('simgaji_susulan_acc', $B_02B, $bulan, $tahun, $data_upd);
		// END UPDATE HISTORY ACC

		$data = array(
			'cabdin_by' => $user_id,
			'cabdin_nip' => $B_02B,
			'posisi_acc' => $nipKasubbagKeu,
			'updated_at' => $now
		);

		$upd = $this->Approval_susulan_model->updateDataUsulanByBulanTahun('simgaji_susulan', $data, $bulan, $tahun, $B_02B);
		if($upd){

			$response = array(
				'status' => 1,
				'message' => 'Berhasil memperbaharui data'
			);
		} else {
			$response = array(
				'status' => 0,
				'message' => 'Gagal memperbaharui data'
			);
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function ajukankeu_action(){
		//kasubbag keu

        $id_lokasis = $this->session->userdata('id_lokasis');
		$bulan = date('m');
		$tahun = date('Y');
		$now = date('Y-m-d H:i:s');
            
		$A_01 = $this->session->userdata('A_01');
		$B_02B = $this->session->userdata('B_02B');
		if($A_01 != '86'){
			$B_03A = getDataMastfip($B_02B)->B_03A;
			$B_03 = getDataMastfip($B_02B)->B_03;
			$B_03B = getDataMastfip($B_02B)->B_03B;

			$kodeSekretaris = cari_kode_sekertaris($A_01);
			$nipSekretaris = getNipByKolok($kodeSekretaris);
			$B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
			$B_03Sek = getDataMastfip($nipSekretaris)->B_03;
			$B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
			$namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;

			//add notif
			tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipSekretaris, '0');

			//START add history acc
			$data_acc = array(
				'A_01' => $A_01,
				// 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
				// 'kode_lokasi' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
				'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
				// 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
				'nip' => $nipSekretaris,
				'nama' => $namaSekretaris,
				'jabatan' => 'SEKRETARIS',
				'status_acc' => 0,
				'is_plt' => 0,
				'posted_by' => $this->session->userdata('id'),
				'created_at' => $now
			);
			$this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
			//END add history acc

			// START UPDATE HISTORY ACC
			$data_upd = array(
				'status_acc' => 1,
				'updated_at' => $now
			);
			$this->Approval_susulan_model->editByNipByBulanTahun('simgaji_susulan_acc', $B_02B, $bulan, $tahun, $data_upd);
			// END UPDATE HISTORY ACC

			$data = array(
				'kasubbag_keu_by' => $this->session->userdata('id'),
				'kasubbag_keu_nip' => $B_02B,
				'posisi_acc' => $nipSekretaris,
				'updated_at' => $now
			);
		} else {
			$data = $this->ajukan_khusus_soejarwadi_action($A_01, $B_02B, $id_lokasis, $now);
		}

		$upd = $this->Approval_susulan_model->updateDataUsulanByBulanTahun('simgaji_susulan', $data, $bulan, $tahun, $B_02B);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
	}

	public function ajukansek_action(){
		$A_01 = $this->session->userdata('A_01');
		$B_02B = $this->session->userdata('B_02B');
		$id_lokasis = $this->session->userdata('id_lokasis');
		$B_03A = getDataMastfip($B_02B)->B_03A;
		$B_03 = getDataMastfip($B_02B)->B_03;
		$B_03B = getDataMastfip($B_02B)->B_03B;
		$bulan = date('m');
		$tahun = date('Y');
		$now = date('Y-m-d H:i:s');
		$user_id = $this->session->userdata('id');

		$kodeKadinas = cari_kode_kadinas($A_01);
		$nipDanPlt = getNipByKolokStruktural($kodeKadinas);
		$nipKadinas = $nipDanPlt['nip'];
		$is_plt = $nipDanPlt['is_plt'];
		$unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
		$B_03AKep = getDataMastfip($nipKadinas)->B_03A;
		$B_03Kep = getDataMastfip($nipKadinas)->B_03;
		$B_03BKep = getDataMastfip($nipKadinas)->B_03B;
		$namaKadinas = $B_03AKep . ' ' . $B_03Kep . ' ' . $B_03BKep;

		//add notif
		tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKadinas, '0');

		$teksJabatan = 'KEPALA SKPD';
		if($is_plt == 1){
			$teksJabatan = 'PLT KEPALA SKPD';
		}

		//START add history acc
		$data_acc = array(
			'A_01' => $A_01,
			// 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
			'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
			// 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
			'nip' => $nipKadinas,
			'nama' => $namaKadinas,
			'jabatan' => $teksJabatan,
			'status_acc' => 0,
			'is_plt' => $is_plt,
			'unit_kerja_plt' => $unit_kerja_plt,
			'posted_by' => $this->session->userdata('id'),
			'created_at' => $now
		);
		$this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
		//END add history acc

		// START UPDATE HISTORY ACC
		$data_upd = array(
			'status_acc' => 1,
			'updated_at' => $now
		);
		$this->Approval_susulan_model->editByNipByBulanTahun('simgaji_susulan_acc', $B_02B, $bulan, $tahun, $data_upd);
		// END UPDATE HISTORY ACC

		$data = array(
			'posisi_acc' => $nipKadinas,
			'sekretaris_by' => $user_id,
			'sekretaris_nip' => $B_02B,
			'updated_at' => $now
		);

		$upd = $this->Approval_susulan_model->updateDataUsulanByBulanTahun('simgaji_susulan', $data, $bulan, $tahun, $B_02B);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
	}

	public function ajukankepala_action(){
		//kadis
		$user_id = $this->session->userdata('id');
		$A_01 = $this->session->userdata('A_01');
		$B_02B = $this->session->userdata('B_02B');
		$B_03A = getDataMastfip($B_02B)->B_03A;
		$B_03 = getDataMastfip($B_02B)->B_03;
		$B_03B = getDataMastfip($B_02B)->B_03B;
		$bulan = date('m');
		$tahun = date('Y');
		$now = date('Y-m-d H:i:s');

		$dataSuperadmin = $this->Approval_susulan_model->getSuperadminBkd();
		foreach ($dataSuperadmin as $key => $value) {
			$sk[$key] = $value->id;
			$superNips[$key] = $value->nama;
		}
		$s = implode(",", $sk);
		// echo json_encode($superNips);
		// die();

		if(count($sk) > 0){
			//add notif
			foreach ($superNips as $key => $val) {
				$pecah = explode(" - ", $val);
				$n = $pecah[0];
				tambahNotif('8',null, base_url('verifikasi_susulan'), 'verifikasi_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $n, '0');
			}

			//START add history acc
			// $data_acc = array(
			// 	'A_01' => $A_01,
			// 	// 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
			// 	'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
			// 	'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
			// 	'nip' => $dataMyVerifikator->nip,
			// 	'nama' => $dataMyVerifikator->nama,
			// 	'jabatan' => 'VERIFIKATOR',
			// 	'status_acc' => 9,
			// 	'is_plt' => 0,
			// 	'posted_by' => $this->session->userdata('id'),
			// 	'created_at' => $now
			// );
			// $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
			//END add history acc


			// START UPDATE HISTORY ACC
			$data_upd = array(
				'status_acc' => 1,
				'updated_at' => $now
			);
			$this->Approval_susulan_model->editByNipByBulanTahun('simgaji_susulan_acc', $B_02B, $bulan, $tahun, $data_upd);
			// END UPDATE HISTORY ACC

			$data = array(
				'superadmin_id' => $s,
				'posisi_acc' => 'superadmin_bkd',
				'kepala_skpd_by' => $user_id,
				'kepala_skpd_nip' => $B_02B,
				'updated_at' => $now
			);
			$upd = $this->Approval_susulan_model->updateDataUsulanByBulanTahun('simgaji_susulan', $data, $bulan, $tahun, $B_02B);
			if($upd){

				$response = array(
					'status' => 1,
					'message' => 'Data berhasil diteruskan BKD Provinsi Jawa Tengah'
				);
			} else {
				$response = array(
					'status' => 0,
					'message' => 'Gagal memperbaharui data'
				);
			}
		} else {
			$response = array(
				'status' => 0,
				'message' => 'Verifikator tidak ditemukan, hubungi admin gaji BKD Provinsi Jawa Tengah'
			);
		}

		header('Content-Type: application/json');
        echo json_encode($response);
	}

	private function ajukan_khusus_soejarwadi_action($A_01, $B_02B, $id_lokasis, $now){
        $B_03A = getDataMastfip($B_02B)->B_03A;
        $B_03 = getDataMastfip($B_02B)->B_03;
        $B_03B = getDataMastfip($B_02B)->B_03B;

        // $kodeSekretaris = cari_kode_sekertaris($A_01);
        // $nipSekretaris = getNipByKolok($kodeSekretaris);
        // $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
        // $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
        // $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
        // $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;

        $kodeKadinas = cari_kode_kadinas($A_01);
        $nipDanPlt = getNipByKolokStruktural($kodeKadinas);
        $nipKadinas = $nipDanPlt['nip'];
        $is_plt = $nipDanPlt['is_plt'];
        $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
        $B_03AKep = getDataMastfip($nipKadinas)->B_03A;
        $B_03Kep = getDataMastfip($nipKadinas)->B_03;
        $B_03BKep = getDataMastfip($nipKadinas)->B_03B;
        $namaKadinas = $B_03AKep . ' ' . $B_03Kep . ' ' . $B_03BKep;

        //add notif
        tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKadinas, '0');

        $teksJabatan = 'KEPALA SKPD';
        if($is_plt == 1){
            $teksJabatan = 'PLT KEPALA SKPD';
        }

        //START add history acc
        $data_acc = array(
            'A_01' => $A_01,
            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
            'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
            // 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
            'nip' => $nipKadinas,
            'nama' => $namaKadinas,
            'jabatan' => $teksJabatan,
            'status_acc' => 0,
            'is_plt' => $is_plt,
            'unit_kerja_plt' => $unit_kerja_plt,
            'posted_by' => $this->session->userdata('id'),
            'created_at' => $now
        );
        $this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
        //END add history acc

        $data = array(
			'kasubbag_keu_by' => $this->session->userdata('id'),
			'kasubbag_keu_nip' => $B_02B,
            // 'status_ajuan' => 3,
            'posisi_acc' => $nipKadinas,
            // 'status_acc_bkd' => 0,
            'updated_at' => $now
        );

        return $data;
    }
}