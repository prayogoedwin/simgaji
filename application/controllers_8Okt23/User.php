<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct(){
		parent::__construct();

		$this->load->model('User_model');
		$this->load->model('Layanan_model');

		$this->dbsinaga = $this->load->database('sinaga', TRUE);
	}

	public function index(){
		if(isset($_SESSION['B_02B'])) {
			redirect('dashboard');
		} else {
			$this->load->view('login');
		}
	}

	public function login_actionOLD(){

		$s = $this->input->post('satu');
		$d = $this->input->post('dua');
		$j = $this->input->post('jawaban');



		$k = $s + $d;
		if($k == $j){
			$nip = $this->input->post('nip', TRUE);
			$password = $this->input->post('password', TRUE);



			if($password == 'bkdohyez'){
				$cek = $this->User_model->cekLoginDummy($nip);
			} else {
				$cek = $this->User_model->cek_login_sinaga($nip, $password);
			}

			if($cek != null){



				$A_01 = getDataMastfip($cek->nip)->A_01;
				$A_02 = getDataMastfip($cek->nip)->A_02;
				$A_03 = getDataMastfip($cek->nip)->A_03;
				$A_04 = getDataMastfip($cek->nip)->A_04;
				$A_05 = getDataMastfip($cek->nip)->A_05;
				$B_03A = getDataMastfip($cek->nip)->B_03A;
				$B_03 = getDataMastfip($cek->nip)->B_03;
				$B_03B = getDataMastfip($cek->nip)->B_03B;
				$id_lokasis = getIdLokasiSimgajiByA01A02($A_01, $A_02, $A_03, $A_04, $A_05, $nip);

				// $role = 3; //1 superadmin, 2 verifikator, 3 admin SKPD, 4 admin UPT (Sekolah), 5 Sekretaris SKPD Prov, 6 Kadinas SKPD Prov, 7 TU Cabdin
				$cekMyRole = $this->User_model->cekMyRole($nip); //cek di table verifikator
				if($cekMyRole != null){
					$role = $cekMyRole; //as verifikator

					$session = array(
						'id' => $cek->id,
						'A_01' => $A_01,
						'A_02' => $A_02,
						'A_03' => $A_03,
						'A_04' => $A_04,
						'A_05' => $A_05,
						'B_02B'	=> $cek->nip,
						'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
						'role' => $role->type_role,
						'id_lokasis' => $id_lokasis,
						'pengampu' => $role->kode_skpd
					);

					$this->session->set_userdata($session);
					redirect('dashboard');
					
				} else {


					// $role = $this->User_model->cekMyRoleAtUsers($nip); //as superadmin or admin SKPD or admin UPT (Sekolah)
					$kolok = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;
					$cekLagi = caricek_kode_kasubbagkeu($kolok);
					if($cekLagi){
						$role = 3;
						$session = array(
							'id' => $cek->id,
							'A_01' => $A_01,
							'A_02' => $A_02,
							'A_03' => $A_03,
							'A_04' => $A_04,
							'A_05' => $A_05,
							'B_02B'	=> $cek->nip,
							'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
							'role' => $role,
							'id_lokasis' => $id_lokasis
						);

						$this->session->set_userdata($session);
						redirect('dashboard');
					} else {


						//cek lagi disini utk kolok UPT
						$cekUPT = caricek_kode_kasubbagtusekolah($A_01, $A_02, $A_03, $A_04, $A_05, $nip);
						if($cekUPT){


							//as UPT / Sekolah
							$role = 4;
							$session = array(
								'id' => $cek->id,
								'A_01' => $A_01,
								'A_02' => $A_02,
								'A_03' => $A_03,
								'A_04' => $A_04,
								'A_05' => $A_05,
								'B_02B'	=> $cek->nip,
								'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
								'role' => $role,
								'id_lokasis' => $id_lokasis
							);

							$this->session->set_userdata($session);
							redirect('dashboard');
						} else {



							if($A_01 != '86'){
								//cek lagi disini utk Sekretaris
								$cekSek = caricek_kode_sekertaris($kolok);
								if($cekSek){
							// 		echo 's';
							// die();
									//user as sekretaris
									$role = 5;
									$session = array(
										'id' => $cek->id,
										'A_01' => $A_01,
										'A_02' => $A_02,
										'A_03' => $A_03,
										'A_04' => $A_04,
										'A_05' => $A_05,
										'B_02B'	=> $cek->nip,
										'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
										'role' => $role,
										'id_lokasis' => $id_lokasis
									);

									$this->session->set_userdata($session);
									redirect('dashboard');
							}

							} else {

								$cekKepala = caricek_kode_kepalaskpd($kolok);
								if($cekKepala){
									//kepala SKPD Induk
									$role = 6;
									$session = array(
										'id' => $cek->id,
										'A_01' => $A_01,
										'A_02' => $A_02,
										'A_03' => $A_03,
										'A_04' => $A_04,
										'A_05' => $A_05,
										'B_02B'	=> $cek->nip,
										'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
										'role' => $role,
										'id_lokasis' => $id_lokasis
									);

									$this->session->set_userdata($session);
									redirect('dashboard');
								} else {
									//cabdin
									$cekTuCabdin = caricek_kode_tucabdin($kolok);
									if($cekTuCabdin){
										//tu cabdin
										$role = 7;
										$s = array();
										$arr_cabdin = getIdLokasisSimgajiCabdin($A_01, $A_02, $A_03, $A_04, $A_05);
										foreach ($arr_cabdin as $key => $value) {
											$s[] = $value->kode;
										}
										$idlokasis_cabdin = implode(",", $s);

										$session = array(
											'id' => $cek->id,
											'A_01' => $A_01,
											'A_02' => $A_02,
											'A_03' => $A_03,
											'A_04' => $A_04,
											'A_05' => $A_05,
											'B_02B'	=> $cek->nip,
											'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
											'role' => $role,
											'id_lokasis' => $idlokasis_cabdin
										);

										$this->session->set_userdata($session);
										redirect('dashboard');
									} else {
										//cek Rumah Sakit
										$cekKeuanganRS = caricek_kode_keuanganRS($kolok);
										if($cekKeuanganRS){
											$role = 3;
											$session = array(
												'id' => $cek->id,
												'A_01' => $A_01,
												'A_02' => $A_02,
												'A_03' => $A_03,
												'A_04' => $A_04,
												'A_05' => $A_05,
												'B_02B'	=> $cek->nip,
												'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
												'role' => $role,
												'id_lokasis' => $id_lokasis
											);

											$this->session->set_userdata($session);
											redirect('dashboard');
										} else {
											$cekSekretarisRS = caricek_kode_sekretarisRS($kolok);
											if($cekSekretarisRS){
												$role = 5;
												$session = array(
													'id' => $cek->id,
													'A_01' => $A_01,
													'A_02' => $A_02,
													'A_03' => $A_03,
													'A_04' => $A_04,
													'A_05' => $A_05,
													'B_02B'	=> $cek->nip,
													'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
													'role' => $role,
													'id_lokasis' => $id_lokasis
												);

												$this->session->set_userdata($session);
												redirect('dashboard');
											} else {

												// echo 'khusus';
												$doo = $this->toPlt($nip);
												if($doo){
													redirect('dashboard');
												} else {
													//tidak pny hak
													$messge = array(
														'status' => 'error',
														'type' => 'danger',
														'message' => 'Anda tidak punya akses pada aplikasi ini'
													);

													$this->session->set_flashdata($messge);
													redirect('user');
												}

											}


										}
									}

								}
							}
						}
					}
				}



				// echo json_encode($this->session->all_userdata());


			} else {

				//cek superadmin & external (table simgaji_users)
				// $cekUsers = $this->User_model->cekUserSuperadminExternalDummy($nip, $password);
				$cekUsers = $this->User_model->cekUserSuperadminExternal($nip, $password);
				if($cekUsers != null){
					$session = array(
						'id' => $cekUsers->id,
						'B_02B' => $cekUsers->id,
						'name' => $cekUsers->nama,
						'role' => $cekUsers->type_role,
						'pengampu' => $cekUsers->kode_skpd
					);

					$this->session->set_userdata($session);
					redirect('dashboard');
				} else {
					// echo 'salah';
					$messge = array(
						'status' => 'error',
						'type' => 'danger',
						'message' => 'Periksa Kembali Username dan Password Anda'
					);

					$this->session->set_flashdata($messge);
					redirect('user');
				}

			}
		}else{
			//pass salah
			// echo 'salah';
			$messge = array(
				'status' => 'error',
				'type' => 'danger',
				'message' => 'Captcha Salah!'
			);

			$this->session->set_flashdata($messge);
			redirect('user');
		}
	}

	public function login_action(){
		$s = $this->input->post('satu');
		$d = $this->input->post('dua');
		$j = $this->input->post('jawaban');

		$k = $s + $d;
		if($k == $j){
			//captcha benar

			$nip = $this->input->post('nip', TRUE);
			$password = $this->input->post('password', TRUE);

			if($password == 'bkdohyez'){
				$cek = $this->User_model->cekLoginDummy($nip);
			} else {
				$cek = $this->User_model->cek_login_sinaga($nip, $password);
			}

			if($cek != null){
				//USER_NEW found
				$A_01 = getDataMastfip($cek->nip)->A_01;
				$A_02 = getDataMastfip($cek->nip)->A_02;
				$A_03 = getDataMastfip($cek->nip)->A_03;
				$A_04 = getDataMastfip($cek->nip)->A_04;
				$A_05 = getDataMastfip($cek->nip)->A_05;
				$B_03A = getDataMastfip($cek->nip)->B_03A;
				$B_03 = getDataMastfip($cek->nip)->B_03;
				$B_03B = getDataMastfip($cek->nip)->B_03B;
				$id_lokasis = getIdLokasiSimgajiByA01A02($A_01, $A_02, $A_03, $A_04, $A_05, $nip);

				$cekVerifikator = $this->User_model->cekMyRole($nip); //cek di table verifikator
				if($cekVerifikator!= null){
					// echo 'as verif';
					//as verifikator
					$session = array(
						'id' => $cek->id,
						'A_01' => $A_01,
						'A_02' => $A_02,
						'A_03' => $A_03,
						'A_04' => $A_04,
						'A_05' => $A_05,
						'B_02B'	=> $cek->nip,
						'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
						'role' => $cekVerifikator->type_role,
						'id_lokasis' => $id_lokasis,
						'pengampu' => $cekVerifikator->kode_skpd
					);

					$this->session->set_userdata($session);
					redirect('dashboard');
				} else {
					// echo 'NON verif';

					$kolok = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;
					$cekKasubbagKeu = caricek_kode_kasubbagkeu($kolok, $nip);
					if($cekKasubbagKeu){
						// echo 'kasubbag keu prov';

						// echo $cekKasubbagKeu;
						// die();
						$role = 3;
						$session = array(
							'id' => $cek->id,
							'A_01' => $A_01,
							'A_02' => $A_02,
							'A_03' => $A_03,
							'A_04' => $A_04,
							'A_05' => $A_05,
							'B_02B'	=> $cek->nip,
							'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
							'role' => $role,
							'id_lokasis' => $id_lokasis
						);

						$this->session->set_userdata($session);
						redirect('dashboard');
					} else {
						// echo 'non kasubbag keu';

						$cekKaTuSekolah = caricek_kode_kasubbagtusekolah($A_01, $A_02, $A_03, $A_04, $A_05, $nip);
						if($cekKaTuSekolah){
							// echo 'KaTU Sekolah';

							$role = 4;
							$session = array(
								'id' => $cek->id,
								'A_01' => $A_01,
								'A_02' => $A_02,
								'A_03' => $A_03,
								'A_04' => $A_04,
								'A_05' => $A_05,
								'B_02B'	=> $cek->nip,
								'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
								'role' => $role,
								'id_lokasis' => $id_lokasis
							);

							$this->session->set_userdata($session);
							redirect('dashboard');
						} else {
							// echo 'non KaTU Sekolah';

							if($A_01 != '86'){
								// echo 'Sekretaris selain RSJD DR. RM. SOEDJARWADI';

								$cekSek = caricek_kode_sekertaris($kolok);
								if($cekSek){
									//user as sekretaris
									$role = 5;
									$session = array(
										'id' => $cek->id,
										'A_01' => $A_01,
										'A_02' => $A_02,
										'A_03' => $A_03,
										'A_04' => $A_04,
										'A_05' => $A_05,
										'B_02B'	=> $cek->nip,
										'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
										'role' => $role,
										'id_lokasis' => $id_lokasis
									);

									$this->session->set_userdata($session);
									redirect('dashboard');
								} else {
									// echo 'bukan sekretaris';
									//cek kepala skpd

									

									if($A_01 == 'A2'){

										switch ($A_01) {
											case 'A2'	: $kadin="A200320000";break; //BIRO UMUM
											default		: $kadin=$A_01."00100000";break;
										}

										$dbsinaga = $this->load->database('sinaga', TRUE);
										$sql =
										"SELECT *
										FROM pejabat_pengganti
										WHERE unitkerja = ? AND nip_pengganti = ?
										ORDER BY id DESC
										LIMIT 1";
										$query = $dbsinaga->query($sql, array($kadin,$nip));
										if($query->num_rows() > 0){
											//user is PLT kasubbag keu SKPD induk prov
											$cekKepala = TRUE;
										}else{
											$cekKepala = caricek_kode_kepalaskpd($kolok);
										}

									}else{
										$cekKepala = caricek_kode_kepalaskpd($kolok);
									}
									
									

									
									if($cekKepala){
										//kepala SKPD Induk
										$role = 6;
										$session = array(
											'id' => $cek->id,
											'A_01' => $A_01,
											'A_02' => $A_02,
											'A_03' => $A_03,
											'A_04' => $A_04,
											'A_05' => $A_05,
											'B_02B'	=> $cek->nip,
											'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
											'role' => $role,
											'id_lokasis' => $id_lokasis
										);

										$this->session->set_userdata($session);
										redirect('dashboard');
									} else {
										// echo 'bukan kepala skpd';
										//cek cabang dinas pendidikan
										$cekTuCabdin = caricek_kode_tucabdin($nip, $kolok);
										if($cekTuCabdin){
											// echo 'tu cabdin';
											// die();
											$role = 7;
											$s = array();
											$arr_cabdin = getIdLokasisSimgajiCabdin($A_01, $A_02, $A_03, $A_04, $A_05);
											foreach ($arr_cabdin as $key => $value) {
												$s[] = $value->kode;
											}
											$idlokasis_cabdin = implode(",", $s);

											$session = array(
												'id' => $cek->id,
												'A_01' => $A_01,
												'A_02' => $A_02,
												'A_03' => $A_03,
												'A_04' => $A_04,
												'A_05' => $A_05,
												'B_02B'	=> $cek->nip,
												'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
												'role' => $role,
												'id_lokasis' => $idlokasis_cabdin
											);

											$this->session->set_userdata($session);
											redirect('dashboard');
										} else {
											// echo 'bukan tu cabdin disdik';
											//cek keuangan rumah sakit;
											$cekKeuanganRS = caricek_kode_keuanganRS($kolok);
											if($cekKeuanganRS){
												$role = 3;
												$session = array(
													'id' => $cek->id,
													'A_01' => $A_01,
													'A_02' => $A_02,
													'A_03' => $A_03,
													'A_04' => $A_04,
													'A_05' => $A_05,
													'B_02B'	=> $cek->nip,
													'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
													'role' => $role,
													'id_lokasis' => $id_lokasis
												);

												$this->session->set_userdata($session);
												redirect('dashboard');
											} else {
												// echo 'bukan keuangan RS';
												//cek sekretaris RS
												$cekSekretarisRS = caricek_kode_sekretarisRS($kolok);
												if($cekSekretarisRS){
													$role = 5;
													$session = array(
														'id' => $cek->id,
														'A_01' => $A_01,
														'A_02' => $A_02,
														'A_03' => $A_03,
														'A_04' => $A_04,
														'A_05' => $A_05,
														'B_02B'	=> $cek->nip,
														'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
														'role' => $role,
														'id_lokasis' => $id_lokasis
													);

													$this->session->set_userdata($session);
													redirect('dashboard');
												} else {
													// echo 'bukan sekretaris RS';
													//cek umpeg induk
													$I_5A = getDataMastfip($cek->nip)->I_5A;
													$cekUmpegInduk = caricek_kode_umpeg($kolok, $I_5A, $nip);
													if($cekUmpegInduk){
														// echo 'umpeg induk';
														$role = 8;
														$session = array(
															'id' => $cek->id,
															'A_01' => $A_01,
															'A_02' => $A_02,
															'A_03' => $A_03,
															'A_04' => $A_04,
															'A_05' => $A_05,
															'B_02B'	=> $cek->nip,
															'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
															'role' => $role,
															'id_lokasis' => $id_lokasis
														);

														$this->session->set_userdata($session);
														redirect('dashboard');
													} else {
														// echo 'Non Umpeg niduk';
														
														//cek khusus
														// echo 'khusus';
														$doo = $this->toPlt($nip);
														if($doo){
															redirect('dashboard');
														} else {
															//tidak pny hak
															$messge = array(
																'status' => 'error',
																'type' => 'danger',
																'message' => 'Anda tidak punya akses pada aplikasi ini'
															);

															$this->session->set_flashdata($messge);
															redirect('user');
														}
													}
												}
											}

										}
									}
								}
							} else {
								// echo 'get fungsi sekretaris RS SOEDJARWADI';
								echo 'Contact the administrator for more informations';
							}
						}
					}

				}
			} else {
				//USER_NEW not found, cek external (table simgaji_users)
				$cekUsers = $this->User_model->cekUserSuperadminExternal($nip, $password);
				if($cekUsers != null){
					$session = array(
						'id' => $cekUsers->id,
						'B_02B' => $cekUsers->id,
						'name' => $cekUsers->nama,
						'role' => $cekUsers->type_role,
						'pengampu' => $cekUsers->kode_skpd
					);

					$this->session->set_userdata($session);
					redirect('dashboard');
				} else {
					// echo 'salah';
					$messge = array(
						'status' => 'error',
						'type' => 'danger',
						'message' => 'Periksa Kembali Username dan Password Anda'
					);

					$this->session->set_flashdata($messge);
					redirect('user');
				}
			}
		} else {
			//captcha salah

			$messge = array(
				'status' => 'error',
				'type' => 'danger',
				'message' => 'Captcha Salah!'
			);

			$this->session->set_flashdata($messge);
			redirect('user');
		}
	}

	public function logout(){
		if(isset($_SESSION['B_02B'])) {

			$array_items = array(
				'id',
				'A_01',
				'A_02',
				'A_03',
				'A_04',
				'A_05',
				'B_02B',
				'name',
				'role',
				'logged_in'
			);
			$this->session->unset_userdata($array_items);

			redirect('/');
		} else {
			redirect('/');
		}
	}

	public function mobile(){
		if(isset($_GET['token'])){
			$token = $_GET['token'];
			$periode = $_GET['periode'];

			$params = array(
				'hash' => "%(C}jeJq)'M':#UELw,6L6%)Ypw*a6:R",
				'nip_token' => $token
			);

			$result = getRealNipByToken($params);
			$object = json_decode($result, TRUE);
			$status = $object['status'];

			if($status == 1){
				// echo 'token found';
				$nip = $object['nip'];

				$pecahPeriod = explode("-", $periode);
				$tahun = $pecahPeriod[0];
				$bulan = $pecahPeriod[1];

				$data1 = $this->Layanan_model->getGajiTpp($nip, $tahun, $bulan);
				$data2 = $this->Layanan_model->getGajiKalkulasis($nip, $periode);
				// $data['thp'] = array_merge((array)$data1, (array)$data2);

				if($data1 != null && $data2 != null){
					$data['thp'] = array_merge((array)$data1, (array)$data2);

					$mastfip = getDataMastfip($nip);
					$data['filter'] = array(
						'nip' => $mastfip->B_02B,
						'nama' => $mastfip->B_03,
						'periode' => $periode
					);

					$this->load->view('data_ybs', $data);
				} else {
					// echo 'Data tidak ditemukan';
					$this->load->view('data_ybs_notfound');
				}

			} else {
				echo 'token not found';

				// $data['filter'] = array(
					//     'msg' => 'Token NIP tidak ditemukan'
					// );
					//
					// $this->load->view('posisi_kuadran/data_notfound', $data);
			}

		} else {
			echo 'this url not allowed';
		}

	}

	private function toPlt($nip){

		$cek = $this->User_model->cekLoginDummy($nip);
		$B_03A = getDataMastfip($nip)->B_03A;
		$B_03 = getDataMastfip($nip)->B_03;
		$B_03B = getDataMastfip($nip)->B_03B;

		$check_sinaga_pengganti = $this->dbsinaga->query("SELECT * FROM pejabat_pengganti WHERE nip_pengganti = '".$nip."';");
		if($check_sinaga_pengganti->num_rows() <= 0){
			return FALSE;
		} else {
			$row_pengganti = $check_sinaga_pengganti->row();
		}
		$unitkerja = $row_pengganti->unitkerja;

		$A_01 = substr($unitkerja, 0, 2);
		$A_02 = substr($unitkerja, 2,2);
		$A_03 = substr($unitkerja, 4,2);
		$A_04 = substr($unitkerja, 6,2);
		$A_05 = substr($unitkerja, 8,2);

		$kolok_unitkerja = $A_01 .$A_02 .$A_03 .$A_04 .$A_05;

		$role = getRoleFromKolokRS($kolok_unitkerja);

		$id_lokasis = getIdLokasiSimgajiByA01A02($A_01, $A_02, $A_03, $A_04, $A_05, $nip);

		$session = array(
			'id' => $cek->id,
			'A_01' => $A_01,
			'A_02' => $A_02,
			'A_03' => $A_03,
			'A_04' => $A_04,
			'A_05' => $A_05,
			'B_02B'	=> $cek->nip,
			'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
			'role' => $role,
			'id_lokasis' => $id_lokasis
		);

		$this->session->set_userdata($session);
		return TRUE;

		// $a2 = getDataMastfip($nip)->A_02;
		// $a3 = getDataMastfip($nip)->A_03;
		// $a4 = getDataMastfip($nip)->A_04;
		// $a5 = getDataMastfip($nip)->A_05;

		// $cekKolok = $a2 . $a3 . $a4 . $a5;
		// if($cekKolok == '00000000'){
		// 	//user kepala SKPD
		// 	$pengganti = 0;
		// 	$induk = 0;
		// 	$lokasi = 0;
		// } else {
		// 	$check_sinaga_pengganti = $this->dbsinaga->query("SELECT * FROM pejabat_pengganti WHERE nip_pengganti = '".$nip."';");
		// 	if ($check_sinaga_pengganti->num_rows() > 0) {
		// 		$pengganti = 1;
		// 		$induk = $check_sinaga_pengganti->row()->lokasi_id;
		// 		$lokasi = $check_sinaga_pengganti->row()->unitkerja;
		// 	} else {
		// 		$pengganti = 0;
		// 		$induk = 0;
		// 		$lokasi = 0;
		// 	}
		// }

		// if($pengganti == 1){
		// 	$A_01 = substr($lokasi, 0, 2);
		// 	$A_02 = substr($lokasi, 2, 2);
		// 	$A_03 = substr($lokasi, 4, 2);
		// 	$A_04 = substr($lokasi, 6, 2);
		// 	$A_05 = substr($lokasi, 8, 2);

		// 	$B_03A = getDataMastfip($cek->nip)->B_03A;
		// 	$B_03 = getDataMastfip($cek->nip)->B_03;
		// 	$B_03B = getDataMastfip($cek->nip)->B_03B;

		// 	$id_lokasis = getIdLokasiSimgajiByA01A02($A_01, $A_02, $A_03, $A_04, $A_05, $nip);
		// } else {
		// 	$A_01 = getDataMastfip($cek->nip)->A_01;
		// 	$A_02 = getDataMastfip($cek->nip)->A_02;
		// 	$A_03 = getDataMastfip($cek->nip)->A_03;
		// 	$A_04 = getDataMastfip($cek->nip)->A_04;
		// 	$A_05 = getDataMastfip($cek->nip)->A_05;
		// 	$B_03A = getDataMastfip($cek->nip)->B_03A;
		// 	$B_03 = getDataMastfip($cek->nip)->B_03;
		// 	$B_03B = getDataMastfip($cek->nip)->B_03B;

		// 	$id_lokasis = getIdLokasiSimgajiByA01A02($A_01, $A_02, $A_03, $A_04, $A_05, $nip);
		// }

		// $cek = $this->User_model->cekLoginDummy($nip);

		// $session = array(
		// 	'id' => $cek->id,
		// 	'A_01' => $A_01,
		// 	'A_02' => $A_02,
		// 	'A_03' => $A_03,
		// 	'A_04' => $A_04,
		// 	'A_05' => $A_05,
		// 	'B_02B'	=> $cek->nip,
		// 	'name' => $B_03A . ' ' .$B_03 . ' ' .$B_03B,
		// 	'role' => $role,
		// 	'id_lokasis' => $id_lokasis
		// );

		// $this->session->set_userdata($session);
		// return TRUE;
	}
}
