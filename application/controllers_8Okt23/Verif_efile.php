<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verif_efile extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Verif_efile_model');
    }

    public function index(){
        redirect('/');
    }

    public function data(){
        $history_id = decode_url($this->uri->segment(3)); //history_p3ks id

        $cek = cek_realparams('simgaji_historyp3ks', $history_id);
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

            $kode_efile = $this->Verif_efile_model->getDetailHistoryById($history_id)->kode_dokumen;
            $nip_ybs = $this->Verif_efile_model->getDetailHistoryById($history_id)->nip;

            $params = array(
                'token' => '!BKDoyee123',
                'kodes' => rtrim($kode_efile, ","),
                'nip' => $nip_ybs
            );

            $res = ClientPost($params);
            $result = json_decode($res, TRUE);
            $data['efilenya'] = $result['data'];

            $data['filter'] = array(
                'nip' => $nip_ybs
            );

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('verif_efile/verif_efile_index', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('verif_efile');

            // echo json_encode($this->session->all_userdata());
        }
    }

    public function verif_action(){
        $iddoks = $this->input->post('iddoks', TRUE);
        $rdr_id = $this->input->post('rdr_id', TRUE);

        foreach ($iddoks as $id) {
            // $this->delete_m->delete($id);
            $params = array(
                'token' => '!BKDoyee123',
                'id_dokumen' => $id,
                'nip_verify' => $this->session->userdata('B_02B')
            );

            $res = clientPostVerif($params);
        }

        if($res){
            $array_msg = array(
				'status'	=>	'success',
				'message'	=>	'Berhasil memverifikasi efile'
			);

			$this->session->set_flashdata($array_msg);
			redirect('verif_efile/data/' . $rdr_id);
        } else {
            $array_msg = array(
				'status'	=>	'error',
				'message'	=>	'Gagal memperbaharui data'
			);

			$this->session->set_flashdata($array_msg);
			redirect('verif_efile/data/' . $rdr_id);
        }

        // echo json_encode($iddoks);
    }
}
