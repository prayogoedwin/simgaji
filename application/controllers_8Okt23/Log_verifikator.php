<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Log_verifikator extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();

        $this->load->model('Log_verifikator_model');
        $this->load->model('Fungsi_model');
        $this->load->model('Pegawai_p3k_model');
        


        $this->pegawais = 'simgaji_pegawaip3ks';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }
    }




    public function index()
    {
        $nip =  $this->session->userdata('B_02B');
       
   
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Log Perubahan Oleh Verifikator';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Log Perubahan P3K</span>';


        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('log/index', $data);
    }

    //CRUD users START//
    public function get_log_pegawai()
    {

        $get = $this->Log_verifikator_model->get_logedit();
        $datatable = array();
        foreach ($get as $key => $value) {

            if($value->kode_id == ''){
                $lama = '';
                $baru = '';
            }else{
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value->kode_id);
                $lama = cekKodeEd($field_name, $value->lama);
                $baru = cekKodeEd($field_name, $value->baru);
            }
            

            $datatable[$key] = array(
                'ybs' => $value->nip_ybs,
                // 'lama' => $value->lama,
                // 'baru' => $value->baru,
                'lama' => $lama,
                'baru' => $baru,
                'verfikator' => $value->namever,
                'created_at' => $value->created_at,  
                'periode' => $value->periode,  
                'perubahan' => $value->perubahan_pada,  
            );
        }


        $output = array(
            "recordsTotal" => $this->Log_verifikator_model->logedit_count_all(),
            "recordsFiltered" => $this->Log_verifikator_model->logedit_count_filtered(),
            "data" => $datatable,

        );
        echo json_encode($output);

    }


    public function penambahan()
    {
        $nip =  $this->session->userdata('B_02B');
       
   
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Log Penambahan Oleh Verifikator';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Log Penambahan P3K</span>';


        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('log/penambahan', $data);
    }

    //CRUD users START//

    //CRUD users START//
    public function get_log_penambahan_pegawai()
    {

        $get = $this->Log_verifikator_model->get_logadd();
        $datatable = array();
        foreach ($get as $key => $value) {
            $n = '<a href="'.base_url('masterpegawai_p3ks/data/'.encode_url($value->id)).'">'.$value->nip.'</a>';
            $datatable[$key] = array(
                'ybs' => $n,
                'nama' => $value->name,
                'verfikator' => $value->namever,
                'created_at' => $value->created_at,  
            );
        }


        $output = array(
            "recordsTotal" => $this->Log_verifikator_model->logadd_count_all(),
            "recordsFiltered" => $this->Log_verifikator_model->logadd_count_filtered(),
            "data" => $datatable,

        );
        echo json_encode($output);

    }

    public function get_log_penambahan_pegawai_()
    {
       
        $this->db->select("a.*, b.nama as namever");
        if($this->input->post('nip_ybs') != ''){
            $this->db->where('a.nip_ybs', $this->input->post('nip_ybs'));
        }
        if($this->input->post('verifikator') != ''){
            $this->db->where('a.created_by', $this->input->post('verifikator'));
        }

        if($this->input->post('created_at') != ''){
            $this->db->like('a.created_at', $this->input->post('created_at'), 'AFTER');
        }
        
        $this->db->from('simgaji_pegawaip3ks_log a');
        $datatable = array();
        $this->db->join('simgaji_verifikator b', 'a.nip_verifikator = b.id');

        $a = $this->db->get();
        $get = $a->result();
        foreach ($get as $key => $value) {

            $n = '<a href="'.base_url('masterpegawai_p3ks/data/'.encode_url($value->id)).'">'.$value->nip.'</a>';

            $datatable[$key] = array(
                'ybs' => $n,
                'nama' => $value->name,
                'verfikator' => $value->namever,
                'created_at' => $value->created_at,  
            );
        }

        $countF = $a->num_rows();
        $output = array(
            // "recordsTotal" => $this->Masterpegawai_p3ks_model->pegawai_count_all(),
            "recordsFiltered" => $countF,
            "data" => $datatable,
        );
        echo json_encode($output);

    }

    public function usulan()
    {
        $nip =  $this->session->userdata('B_02B');
       
   
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Log Usulan Terverifikasi';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Log Perubahan P3K</span>';


        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('log/usulan', $data);
    }

    //CRUD users START//
    public function get_log_usulan($tipe = NULL)
    {
        if($tipe == 'historyp3k'){
            $db = 'simgaji_historyp3ks d';
            // $this->log_usul = 'simgaji_historyp3ks d';
        }else if($tipe == 'historyp3k_tpp'){
            $db  = 'simgaji_historyp3ks_tpp d';
        }else if($tipe == 'history_tpp'){
            $db  = 'simgaji_history_tpp d';
        }else{
            $db  = 'simgaji_history d';
        }
        $get = $this->Log_verifikator_model->get_logusul($db);
        $datatable = array();
        foreach ($get as $key => $value) {

            $field_name = $this->Fungsi_model->getFieldNameByKodeId($value->kode_id);

            $datatable[$key] = array(
                'ybs' => $value->nip,
                'lama' => cekKodeEd($field_name, $value->lama),
                'baru' => cekKodeEd($field_name, $value->baru),
                'pengusul' => $value->nip_by,
                'verfikator' => $value->namever,
                'created_at' => $value->created_at,  
                'periode' => $value->periode,  
            );
        }


        $output = array(
            "recordsTotal" => $this->Log_verifikator_model->logusul_count_all($db),
            "recordsFiltered" => $this->Log_verifikator_model->logusul_count_filtered($db),
            "data" => $datatable,

        );
        echo json_encode($output);

    }

}