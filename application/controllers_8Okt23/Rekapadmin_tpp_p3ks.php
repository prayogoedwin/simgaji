<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekapadmin_tpp_p3ks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->verifikasi = 'simgaji_historyp3ks_tpp_acc';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Rekapadmin_tpp_p3ks_model');
        $this->load->model('Verifikasi_tpp_p3ks_model');
        $this->load->model('Tpp_history_p3k_model');
        $this->load->model('Tpp_pegawai_p3k_model');
    }

    public function index()
    {

        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Periode Verifikasi TPP P3K';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Periode Verifikasi TPP P3K</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('rekapadmin_tpp_p3ks/periode', $data);
    }

    public function get_periode()
    {
        $get = $this->Verifikasi_tpp_p3ks_model->get_periode();
        $datatable = array();
        foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a href="' . base_url('verifikasi_p3ks/periode/') . $value->bulan . '/' . $value->tahun . '" >' . $value->tahun . '-' . $value->bulan . '-01</a>';
            $klik_kode = '<a href="' . base_url('rekapadmin_tpp_p3ks/periode/') . $value->periode . '">' . $value->periode . '</a>';
            $klik_kode_ = '<a href="' . base_url('rekapadmin_tpp_p3ks/periode_verif/') . $value->periode . '">' . $value->periode . '</a>';

            $no = 1;
            $datatable[$key] = array(

                'id' => $no++,
                'periode' => $klik_kode,
                'periode_' => $klik_kode_,
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$add,
            );
        }
        // $data['datatable'] = $datatable;

        $output = array(

            "recordsTotal" => $this->Verifikasi_tpp_p3ks_model->periode_count_all(),
            "recordsFiltered" => $this->Verifikasi_tpp_p3ks_model->periode_count_filtered(),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function periode()
        {
            $data_header['session'] = $this->session->all_userdata();
            $data['title'] = 'Rekap Histori';
            $data['breadcrumb'] =
                '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
            <span class="breadcrumb-item active">Rekap Histori</span>';

            $data['opd'] = $this->Fungsi_model->get_opd();

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('rekapadmin_tpp_p3ks/index', $data);


        }

        public function periode_verif()
        {
            // echo $this->session->userdata('B_02B');
            // die();
            $data_header['session'] = $this->session->all_userdata();
            $data['title'] = 'Rekap Histori TPP';
            $data['breadcrumb'] =
                '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
            <span class="breadcrumb-item active">Rekap Histori TPP</span>';

            $data['opd'] = $this->Fungsi_model->get_opd();

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);

            $this->load->view('rekapadmin_tpp_p3ks/index_verif', $data);
        }


       
    


        //CRUD users START//
    public function get_rekapp3ks()
    {
        $periode = $this->uri->segment(3);
        $get = $this->Rekapadmin_tpp_p3ks_model->get_rekapp3ks($periode);
        
        $datatable = array();
        foreach ($get as $key => $value) {
            $usulan = $value->tolak + $value->revisi + $value->setuju + $value->belum ;
            $proses = $value->tolak + $value->revisi + $value->setuju ;
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a target="BLANK" href="' . base_url('verifikasi_p3ks/detail/') . encode_url($value->pegawai_id) . '" >' . $value->B_02B . '</a>';
            
            $datatable[$key] = array(
                'lokasikerja' => $value->lokasi_kerja,
                'lokasigaji' => $value->lokasi_gaji,
                'usulan' =>  $usulan,
                'proses' => $proses,
                'belum' =>  $value->belum,
                // 'pengampu' => get_pengampu($value->id_lokasi),
            );   
           
        }
        // $data['datatable'] = $datatable;

        // $output = array(

        //     "recordsTotal" => $this->Rekapadmin_tpp_p3ks_model->verifikasi_count_all($periode),
        //     "recordsFiltered" => $this->Rekapadmin_tpp_p3ks_model->verifikasi_count_filtered($periode),
        //     "data" => $datatable,
        // );

        $output = array(

            "recordsTotal" => array(),
            "recordsFiltered" => array(),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }
    

         //CRUD users START//
         public function get_rekapp3ks_by_verifikator()
         {
             $periode = $this->uri->segment(3);
             $get = $this->Rekapadmin_tpp_p3ks_model->get_rekapp3ks_by_verifikator($periode);
             
             $datatable = array();
             foreach ($get as $key => $value) {
                 $usulan = $value->tolak + $value->revisi + $value->setuju + $value->belum ;
                 $proses = $value->tolak + $value->revisi + $value->setuju ;
                 //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
                 // $klik_kode = '<a target="BLANK" href="' . base_url('verifikasi_p3ks/detail/') . encode_url($value->pegawai_id) . '" >' . $value->B_02B . '</a>';
                 
                    $select = explode(',',$value->kode_skpd);
                    $datasel = '<ul>';

                    foreach ($select as $val){
                        $datasel .= '<li>'.getKodeByLokasis($val).' - '.$val.'</li>';
                    }

                    $datasel .= '</ul>';

                 $datatable[$key] = array(
                     'nip' => $value->nip,
                     'nama' => $value->nama,
                     'usulan' =>  $usulan,
                     'proses' => $value->belum,
                     'belum' =>  $proses,
                     'mngampu' =>  $datasel,
                 );   
                
             }
             // $data['datatable'] = $datatable;
     
             // $output = array(
     
             //     "recordsTotal" => $this->Rekapadmin_tpp_p3ks_model->verifikasi_count_all($periode),
             //     "recordsFiltered" => $this->Rekapadmin_tpp_p3ks_model->verifikasi_count_filtered($periode),
             //     "data" => $datatable,
             // );
     
             $output = array(
     
                 "recordsTotal" => array(),
                 "recordsFiltered" => array(),
                 "data" => $datatable,
             );
             // output to json format
             echo json_encode($output);
             // header('Content-Type: application/json');
             // echo json_encode($data);
         }



    public function log()
    {

        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Log Perubahan P3K Oleh Verifikator';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Log Perubahan P3K Oleh Verifikator</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('rekapadmin_tpp_p3ks/log', $data);
    }

    public function get_log()
    {
        $get = $this->Verifikasi_tpp_p3ks_model->get_periode();
        $datatable = array();
        foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a href="' . base_url('verifikasi_p3ks/periode/') . $value->bulan . '/' . $value->tahun . '" >' . $value->tahun . '-' . $value->bulan . '-01</a>';
            $klik_kode = '<a href="' . base_url('rekapadmin_tpp_p3ks/periode/') . $value->periode . '">' . $value->periode . '</a>';
            $klik_kode_ = '<a href="' . base_url('rekapadmin_tpp_p3ks/periode_verif/') . $value->periode . '">' . $value->periode . '</a>';

            $no = 1;
            $datatable[$key] = array(

                'id' => $no++,
                'periode' => $klik_kode,
                'periode_' => $klik_kode_,
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$add,
            );
        }
        // $data['datatable'] = $datatable;

        $output = array(

            "recordsTotal" => $this->Verifikasi_tpp_p3ks_model->periode_count_all(),
            "recordsFiltered" => $this->Verifikasi_tpp_p3ks_model->periode_count_filtered(),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

}
