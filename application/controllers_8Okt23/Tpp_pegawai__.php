<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pegawai extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_pegawai_model');
	}

    public function index_(){
        $data_header['session'] = $this->session->all_userdata();
        $role = $this->session->userdata('role');
        $lokasis_id = $this->session->userdata('id_lokasis');
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;
        $kode_lokasis = getDetailLokasisById($lokasis_id)->kode;

        $get = $this->Tpp_pegawai_model->getDataPegawai($kode_lokasis);

        $datatable = array();
        foreach ($get as $key => $value) {

            if($statusSkpd == '2'){
                $klik_nip = '<a href="'. base_url('tpp_pegawai/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
            } else {
                $klik_nip = $value->nip;
            }

            $datatable[$key] = array(
                'name' => $value->name,
                'nip' => $klik_nip,
                'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('tpp_pegawai/data_pegawai/') . encode_url($value->id) .'">Lihat Data</a>'
            );
        }
        $data['datatable'] = $datatable;

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('tpp_pegawai/tpp_pegawai_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function index(){

        // echo $this->session->userdata('id_lokasis');
        // die();
        $data_header['session'] = $this->session->all_userdata();
        $role = $this->session->userdata('role');
        if($role == 4){
            //kasubbag TU Sekolah

            $lokasis_id = $this->session->userdata('id_lokasis');
            $row_lokasi = $this->Tpp_pegawai_model->getKodeLokasisById($lokasis_id);
            // $kode6digit = substr($row_lokasi->kode, 0, 6);
            // $get = $this->Pegawai_p3k_model->getDataPegawaiP3kTuSekolah($row_lokasi->kode);
            if($row_lokasi != null){
              $get = $this->Tpp_pegawai_model->getDataPegawaiTuSekolah($row_lokasi->id);
            } else {
              $array_msg = array(
                  'status'=>'error',
                  'message'=>'Data mapping tidak ditemukan, silahkan hubungi BKD Provinsi Jawa Tengah'
              );

              $this->session->set_flashdata($array_msg);
              redirect('dashboard');
            }


            $datatable = array();
            foreach ($get as $key => $value) {
                $klik_nip = '<a href="'. base_url('tpp_pegawai/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';

                $datatable[$key] = array(
                    'nip' => $klik_nip,
                    'name' => $value->name,
                    'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('tpp_pegawai/data/') . encode_url($value->id) .'"><i class="icon-lasso2"></i></a>'
                );
            }
            $data['datatable'] = $datatable;

        } else if($role == 7){
            //TU Cabdin
            $lokasis_id = $this->session->userdata('id_lokasis');
            $data['dropd'] = $this->Tpp_pegawai_model->getDropCabdinIn($lokasis_id);

            if(isset($_GET['kode'])){

                $kode = $_GET['kode'];

                $get = $this->Tpp_pegawai_model->getDataPegawaiForCabdin($kode);

                $datatable = array();
                foreach ($get as $key => $value) {
                    $klik_nip = '<a href="'. base_url('tpp_pegawai/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';

                    $datatable[$key] = array(
                        'nip' => $klik_nip,
                        'name' => $value->name,
                        'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('tpp_pegawai/data/') . encode_url($value->id) .'"><i class="icon-lasso2"></i></a>'
                    );
                }
                $data['datatable'] = $datatable;

                $data['filter'] = array(
                    'kodefilter' => $kode
                );

            } else {

                $data['datatable'] = array();

                $data['filter'] = array(
                    'kodefilter' => null
                );
            }

        } else {
            $data['datatable'] = array();
        }

        // echo json_encode($data);
        // die();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('tpp_pegawai/tpp_pegawai_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getData(){
        $lokasis_id = $this->session->userdata('id_lokasis');
       
        $statusSkpd = getStatusSKPDByLokasisId($lokasis_id)->status_gaji;

        $query  =
        "SELECT p.`id`, p.`nip`, p.`name`
        FROM `simgaji_pegawai_tpp` p";

        $search = array('p.nip','p.`name`');
        // if()
        // $where  = array('lokasi_kerja' => $lokasis_id);
        $where  = array('lokasi_gaji' => $lokasis_id);
        // $where  = array('nama_kategori' => 'Tutorial');

        // jika memakai IS NULL pada where sql
        $isWhere = null;
        // $isWhere = 'artikel.deleted_at IS NULL';
        header('Content-Type: application/json');
        echo $this->Tpp_pegawai_model->getDataPegawai($query,$search,$where,$isWhere, $statusSkpd);
    }

    public function detail2($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawai_tpp', $real_id);
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_pegawai_model->getDetailById($real_id);

            $data['warning'] = array(
                'pesan' => 'NIP tidak ditemukan di SIMPEG, silahkan hubungi admin'
            );

            $nip_ybs = $data['detail']->nip;
            $cekNip = getDataMastfip($nip_ybs);
            if($cekNip != null){
                $data['warning'] = array(
                    'pesan' => ''
                );
            }

            $data['datasimpeg'] = getDataMastfip($data['detail']->nip);
            $nips = $data['detail']->nip;
            $kolok = $data['datasimpeg']->A_01 . $data['datasimpeg']->A_02 . $data['datasimpeg']->A_03 . $data['datasimpeg']->A_04 . $data['datasimpeg']->A_05;
            $data['unit_kerja'] = getUnitKerja($data['datasimpeg']->A_01)->NALOK;

            $data['unors'] = $this->Tpp_pegawai_model->getSKPDInduk();
            $getsample = $this->Tpp_pegawai_model->getJabatanBySKPD('B2');
            $samplepic = array();
            foreach ($getsample as $key => $value) {
                $najab = '--';
                if($value->NALOK != NULL){
                    $najab = $value->NALOK;
                } else {
                    if($value->NAMA != NULL){
                        $najab = $value->NAMA;
                    } else {
                        $najab = $value->NAJFU;
                    }
                }

                $samplepic[$key] = array(
                    'kode_jabatan' => $value->KD_FORMASI,
                    'teks_picker' => $najab
                );
            }
            $data['jabatansample'] = $samplepic;

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai/detail_pegawai', $data);

            // echo json_encode($data['jabatansample']);
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai');
        
        }
    }

    public function detail($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawai_tpp', $real_id);
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_pegawai_model->getDetailById($real_id);

            $data['warning'] = array(
                'pesan' => 'NIP tidak ditemukan di SIMPEG, silahkan hubungi admin'
            );

            $nip_ybs = $data['detail']->nip;
            $cekNip = getDataMastfip($nip_ybs);
            if($cekNip != null){
                $data['warning'] = array(
                    'pesan' => ''
                );
            }

            //notif for katu sekolah
            $n = $this->session->userdata('B_02B');
            $nipCabdin = $this->Tpp_pegawai_model->getNIPCabdinByNipKatuSekolah($n);
            if($nipCabdin != null){
              $data['info_sekolah'] = array(
                'message' => 'NIP KaTU Cabang Dinas',
                'NIP' => $nipCabdin
              );
            } else {
              $data['info_sekolah'] = array(
                'message' => 'Data NIP KaTU Cabang Dinas tidak ditemukan',
                'NIP' => 0
              );
            }
            

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai/detail_pegawai', $data);

            // echo json_encode($data['jabatansample']);
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai');
        
        }
    }

    public function detail_action(){
        $user_id = $this->session->userdata('id');
        $role = $this->session->userdata('role');
        // $id_lokasis = $this->session->userdata('id_lokasis');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $now = date('Y-m-d H:i:s');

        // echo json_encode($kode_id);
        // die();

        if(count($kode_id) > 0){
            $datahistory = array();
            $idVerif = array();
            foreach ($kode_id as $key => $value) {
                $field_name = $this->Fungsi_model->getFieldNameByKodeId($value);

                $nilai_lama = $this->Tpp_pegawai_model->getDetailById($pegawai_id)->$field_name;

                $nilai_baru = $this->input->post($field_name, TRUE);

                $A_01 = $this->session->userdata('A_01');
                $A_02 = $this->session->userdata('A_02');

                if($A_01 == 'D0' && $A_02 != '00'){
                    //bukan SKPD induk, turunan disdik prov. jenjang here

                    // $kodeumpeg_atasan = getKodeCabdinSekolah($A_01, $A_02, $A_03, $A_04, $A_05);
					// $lokasi = getNalokFromKolok($kodeumpeg_atasan);

					// $nip_acc = getNipUmpegCabdinByLokasi($posisi_acc);

                    $nip = $this->input->post('nipe', TRUE);
                    $nama = $this->input->post('nama', TRUE);
                    $jabatan = $this->input->post('jabatan', TRUE);

                    if($role == 4){
                        //UPT or sekolah
                        $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));
                    } else if($role == 7){
                        //cabdin pendidikan
                        $x = $this->session->userdata('id_lokasis');
                        $pecah = explode(",", $x);
                        $firstKode = $pecah[0];
                        $myKolok = getKolokSimpegByLokasisKode($firstKode);
                    }

                    if($role == 4){
                        //UPT or sekolah
                        $myCabdinKolok = getKolokCabdin($myKolok);
                        $fix_myCabdinKolok = getKolokCabdin($myCabdinKolok); //fix perubahan tablokb08
                    } else {
                        $fix_myCabdinKolok = getKolokCabdinNonAtasan($myKolok);
                    }


                    $dataMyVerifikator = getMyVerifikator($fix_myCabdinKolok);
                    if($dataMyVerifikator != null){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            'verifikator' => $dataMyVerifikator->id,
                            'status_ajuan' => 0,
                            'posisi_acc' => $nip,
                            'posted_by' => $user_id,
                            'nip_by' => $this->session->userdata('B_02B'),
                            'created_at' => $now
                        );
                    } else {
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'Sekolah / Cabdin anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('tpp_pegawai');
                    }

                } else {
                    //SKPD induk prov
                    $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

                    // $dataMyVerifikator = getMyVerifikator($myKolok);

                    $dataAllMyVerifikator = getAllMyVerifikator($myKolok); //get semua verifikator yang mengampu
                    foreach ($dataAllMyVerifikator as $ki => $valVerif) {
                        $idVerif[$ki] = $valVerif->id;
                    }
                    $allIdVerifikator = implode(",", $idVerif);
                    if(count($dataAllMyVerifikator) > 0){
                        $datahistory[$key] = array(
                            'tanggal' => date('Y-m-d'),
                            'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                            'pegawai_id' => $pegawai_id,
                            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                            'kode_id' => $value,
                            'lama' => $nilai_lama,
                            // 'field_name' => $field_name,
                            'baru' => $nilai_baru,
                            'description' => '-',
                            // 'verifikator' => $dataMyVerifikator->id,
                            'verifikator' => $allIdVerifikator,
                            'status_ajuan' => 1,
                            // 'posisi_acc' => $dataMyVerifikator->nip,
                            'posisi_acc' => getNipSekByKolok($myKolok),
                            'posted_by' => $user_id,
                            'nip_by' => $this->session->userdata('B_02B'),
                            'created_at' => $now
                        );
                    } else {
                        //SKPD ybs blm pny verifikator
                        $array_msg = array(
                            'status'=>'error',
                            'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                        );

                        $this->session->set_flashdata($array_msg);
                        redirect('tpp_pegawai');
                    }
                }

            }

            // echo json_encode($datahistory);
            // die();

            $ins = $this->Fungsi_model->tambah_bulk('simgaji_history_tpp', $datahistory);
            if($ins){
                //START add history acc ke verifikator
                // $data_acc = array(
                //     'pegawais_id' => $pegawai_id,
                //     'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                //     'nip' => $dataMyVerifikator->nip,
                //     'nama' => $dataMyVerifikator->nama,
                //     'jabatan' => 'Verifikator',
                //     'status_acc' => 0,
                //     'is_plt' => 0,
                //     'created_at' => $now
                // );
                // $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
                //END add history acc ke verifikator

                //add notif inbox2 for verifikator
                // $A_01 = $this->session->userdata('A_01');
                // $B_02B = $this->session->userdata('B_02B');
                // $B_03A = getDataMastfip($B_02B)->B_03A;
				// $B_03 = getDataMastfip($B_02B)->B_03;
				// $B_03B = getDataMastfip($B_02B)->B_03B;
                // tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Ada usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $dataMyVerifikator->nip, '0');

                $array_msg = array(
                    'status'=>'success',
                    'message'=>'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_history');
            } else {
                $array_msg = array(
                    'status'=>'error',
                    'message'=>'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('tpp_pegawai');
            }
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Pastikan ada data yang dipilih'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai/detail/' . encode_url($pegawai_id));
        }

    }

    public function data_pegawai($par){
        $real_id = decode_url($par);
        $cek = cek_realparams('simgaji_pegawai_tpp', $real_id);
        if($cek){
            // echo 'lanjut';

            $data_header['session'] = $this->session->all_userdata();

            $detail = $this->Tpp_pegawai_model->getDetailById($real_id);
            $data['detail'] = $detail;
            $data['kodes'] = $this->Tpp_pegawai_model->getDataKodes();
            $data['lokasi'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_kedudukans');
            $data['jabatans'] = $this->Tpp_pegawai_model->getJabatanBySKPDforData($detail->A_01);
            $data['jenis_jabatans'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_jenisjab');
            $data['fungsional'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_profesis');

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('tpp_pegawai/data_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {

            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('tpp_pegawai');
        }
    }


    public function get_petafngsapk(){
        $this->dbeps = $this->load->database('eps', TRUE);
        $i_5a = $this->input->post('i_5a');
        $i_05 = $this->input->post('i_05');

        $x = $this->Tpp_pegawai_model->geJabatanDetail($i_5a, $i_05);

        if($x){
            if($i_5a == 1){

                $y = $this->db->query("SELECT * FROM simgaji_eselons WHERE kode_simpeg = '$x->ESEL'");
                if($y->num_rows() > 0){
                    $u = $y->row();
                    $es_new = $u->id;
                    $es_str = $u->name;
                }else{
                    $es_new = '';
                    $es_str = '';
                }

                $data = array(
                    'i_05' => $x->KOLOK,
                    'i_07' => '',
                    'kelasjab' => $x->KELAS_1,
                    'esel' => $x->ESEL,
                    'esel_str' => $es_str
                );


            }elseif($i_5a == 2){
                
                $data = array(
                    'i_05' => $x->KFUNG,
                    'i_07' => $x->KJENJANG,
                    'kelasjab' => $x->KELAS,
                    'esel' => '',
                    'esel_str' => ''
                );

            }elseif($i_5a == 0){
                
                $data = array(
                    'i_05' => $x->KOJFU,
                    'i_07' => '',
                    'kelasjab' => $x->KELAS,
                    'esel' => '',
                    'esel_str' => ''
                );

            }else{
                $data = array();
            }
        }else{
            $data = array();
        }

       

        echo json_encode($data);
    }

    public function get_petafngsapk_(){
        $this->dbeps = $this->load->database('eps', TRUE);
        $i_05 = $this->input->post('i_05');
        $x = $this->Tpp_pegawai_model->petafngsapk($i_05);
        if($x != false){

            $y = $this->Tpp_pegawai_model->mastformasi18($x->CEPAT_KODE);

            $data = array(
                'i_05' => $x->KFUNG,
                'i_07' => $x->KJENJANG,
                'kelasjab' => $y->KELAS,
                'esel' => '',
                'esel_str' => ''
            );
        }else{
            $y = $this->Tpp_pegawai_model->mastformasi18($i_05);

            $get_esel = $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = '$y->KD_FORMASI'");
            if($get_esel->num_rows() > 0){
                $eselon = $get_esel->row()->ESEL;
            }else{
                $eselon = '';
            }
            

            $x = $this->db->query("SELECT * FROM simgaji_eselons WHERE kode_simpeg = '$eselon'");
            if($x->num_rows() > 0){
                $u = $x->row();
                $es_new = $u->id;
                $es_str = $u->name;
            }else{
                $es_new = '';
                $es_str = '';
            }
            $data = array(
                'i_05' => '',
                'i_07' => '',
                'kelasjab' => $y->KELAS,
                'esel' => $es_new,
                'esel_str' => $es_str
            );
        }

        echo json_encode($data);
    }

    public function cek_hidden_show_filed(){

        $lokasi_gaji = $this->input->post('lokasi_gaji');

        // echo $lokasi_gaji;
        // die();

        $this->db->where('id', $lokasi_gaji);
        $lokasis = $this->db->get('simgaji_lokasis');
        if($lokasis->num_rows() > 0){
            $lokasi = $lokasis->row()->kode;
        } else{
            $lokasi = '0';
        }  
        

        $bkk = 'hidden';
        if(beban_kerja_khusus($lokasi) == true){
            $bkk = '';
        }

        $tk = 'hidden';
        if(tempat_kerja($lokasi) == true){
            $tk = '';
        }

        $kk = 'hidden';
        if(kondisi_kerja($lokasi) == true){
            $kk = '';
        }

        $data = array(
            'bkk' => $bkk,
            'tk'  => $tk,
            'kk'  => $kk
        );

        echo json_encode($data);
        
    }

    // public function get_jbt(){

    //     $getsample = $this->Tpp_pegawai_model->geJabatan();
    //     echo json_encode(count($getsample));

    // }

    public function get_jabatan($jab_now = null, $jab_now_name = null){

         // Ambil data ID Provinsi yang dikirim via ajax post
        //  $lokasi_kerja = $this->input->post('lokasi_kerja');
         $jenis_jab = $this->input->post('i_5a');
 
         if($jab_now != NULL){
 
             $namajab = urldecode($jab_now_name);
             $lists = "<option value='$jab_now'>$namajab</option>";
             
         }else{
            $lists = "<option value=''>Silakan Pilih Jabatan</option>";
         }

         $getsample = $this->Tpp_pegawai_model->geJabatan($jenis_jab);
 
         foreach ($getsample as $key => $value) {

             $najab = '--';
             if($jenis_jab == 1){
                $najab = $value->NAJABP;
                $cepat_kode = $value->KOLOK;
             }elseif($jenis_jab == 2){
                $najab = $value->NAMA;
                $cepat_kode = $value->CEPAT_KODE;
             }elseif($jenis_jab == 0){
                $najab = $value->NAJFU;
                $cepat_kode = $value->KOJFU;
             }else{
                $najab = 'Tidak ditemukan';
                $cepat_kode = 'Tidak ditemukan';
             }
             
 
             // $lists .= "<option  value='".$value->KD_FORMASI."'>".$najab."</option>"; 
             $lists .= "<option  value='".$cepat_kode."'>".$najab.' - '.$cepat_kode."</option>"; 
 
             // $samplepic[$key] = array(
             //     'kode_jabatan' => $value->KD_FORMASI,
             //     'teks_picker' => $najab
             // );
         }
         
         $callback = array('list_jabatan'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
         echo json_encode($callback); // konversi varibael $callback menjadi JSON

    }

    public function get_jabatan_($jab_now = null, $jab_now_name = null){
        // Ambil data ID Provinsi yang dikirim via ajax post
        $lokasi_kerja = $this->input->post('lokasi_kerja');

        $this->db->where('id', $lokasi_kerja);
        $lokasis = $this->db->get('simgaji_lokasis');
        if($lokasis->num_rows() > 0){
            $lok = $lokasis->row()->kolok_simpeg;
        } else{
            $lok = 00;
        }  

        // if($jab_now != null){
        //     $A_01 = $lokasi_kerja;
        // }else{
        //     $A_01 = substr($lok,0,2);
        // }

        if(strlen($lokasi_kerja) == 2){
            $A_01 = $lokasi_kerja;
        }else{
            $A_01 = substr($lok,0,2);
        }

        // echo $A_01;
        // die();

        // $A_01 = substr($lok,0,2);

       

        $getsample = $this->Tpp_pegawai_model->getJabatanBySKPD($A_01);
        $samplepic = array();

        if($jab_now != NULL){

            $namajab = urldecode($jab_now_name);
            $lists = "<option value='$jab_now'>$namajab</option>";
            
        }else{
           $lists = "<option value=''>Silakan Pilih Jabatan</option>";
        }

        
        
        foreach ($getsample as $key => $value) {
            $najab = '--';
            if($value->NALOK != NULL){
                $najab = $value->NAJABP;
            } else {
                if($value->NAMA != NULL){
                    $najab = $value->NAMA;
                } else {
                    $najab = $value->NAJFU;
                }
            }

           
           

            // $lists .= "<option  value='".$value->KD_FORMASI."'>".$najab."</option>"; 
            $lists .= "<option  value='".$value->KD_FORMASI."'>".$najab.' - '.$value->KD_FORMASI."</option>"; 

            // $samplepic[$key] = array(
            //     'kode_jabatan' => $value->KD_FORMASI,
            //     'teks_picker' => $najab
            // );
        }
        
        $callback = array('list_jabatan'=>$lists); // Masukan variabel lists tadi ke dalam array $callback dengan index array : list_kota
        echo json_encode($callback); // konversi varibael $callback menjadi JSON
    }
}