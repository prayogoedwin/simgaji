<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji_susulan extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Gaji_susulan_model');
		$this->load->model('Pegawai_p3k_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        //notif for katu sekolah
        $n = $this->session->userdata('B_02B');
        $nipCabdin = $this->Pegawai_p3k_model->getNIPCabdinByNipKatuSekolah($n);
        if($nipCabdin != null){
            $data['info_sekolah'] = array(
                'message' => 'NIP KaTU Cabang Dinas',
                'NIP' => $nipCabdin
            );
        } else {
            $data['info_sekolah'] = array(
                'message' => 'Data NIP KaTU Cabang Dinas tidak ditemukan',
                'NIP' => 0
            );
        }

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('gaji_susulan/gaji_susulan_index', $data);

		// echo json_encode($data);
	}

    public function read(){
        $user_id = $this->session->userdata('id');
        $postData = $this->input->post();

        $data = $this->Gaji_susulan_model->getData($postData, $user_id);

        echo json_encode($data);
    }

    public function cari_pegawais(){
        $nip = $this->input->get('nipnya', TRUE);

        $data = $this->Gaji_susulan_model->getDataPegawai($nip);

        header('Content-Type: application/json');
		echo json_encode($data);
    }

    public function tambah_action(){
        $userid = $this->session->userdata('id');
        $nip = $this->input->post('nipz', TRUE);
        $pegawai_id = $this->input->post('nips', TRUE);
        $jenis_susulan = $this->input->post('jenis_susulan', TRUE);
        $kategori_gapok = $this->input->post('kategori_gapok', TRUE);
        if($jenis_susulan == '1'){
            //gapok
            if($kategori_gapok == '1'){
                //tanpa perubahan
                $nmtable = 'simgaji_susulan';
                $flaging = 2;
            } else {
                //dengan perubahan
                $nmtable = 'simgaji_susulan_gapokperubahan';
                $flaging = 1;
            }
        } else {
            //fungsional
            $nmtable = 'simgaji_susulan';
            $flaging = 2;
        }
        $nama = $this->input->post('nama', TRUE);
        $periode_start = $this->input->post('periode_start', TRUE);
        $periode_end = $this->input->post('periode_end', TRUE);
        $lokasi_kode = $this->input->post('lokasi_kode', TRUE);
        $gj_baru = $this->input->post('gj_baru', TRUE);
        $gj_lama = $this->input->post('gj_lama', TRUE);
        if($gj_lama == null || $gj_lama == ''){
            $gj_lama = 0;
        }
        $jml_bl = $this->input->post('jml_bl', TRUE);
        $keterangan = $this->input->post('keterangan', TRUE);
        $posisi_acc = $this->input->post('posisi_acc', TRUE);
        $now = date('Y-m-d H:i:s');

        $gaji_pokok = $this->input->post('gaji_pokok', TRUE);
        $tunjangan_istri = $this->input->post('tunjangan_istri', TRUE);
        $tunjangan_anak = $this->input->post('tunjangan_anak', TRUE);
        $jumlah_tunjangan_keluarga = $this->input->post('jumlah_tunjangan_keluarga', TRUE);
        $jumlah_penghasilan = $this->input->post('jumlah_penghasilan', TRUE);
        $tunjangan_umum = $this->input->post('tunjangan_umum', TRUE);
        $tunjangan_umum_tambahan = $this->input->post('tunjangan_umum_tambahan', TRUE);
        $tunjangan_struktural = $this->input->post('tunjangan_struktural', TRUE);
        $tunjangan_fungsional = $this->input->post('tunjangan_fungsional', TRUE);
        $jiwa = $this->input->post('jiwa', TRUE);
        $tunjangan_beras = $this->input->post('tunjangan_beras', TRUE);
        $tunjangan_pph = $this->input->post('tunjangan_pph', TRUE);
        $pembulatan = $this->input->post('pembulatan', TRUE);
        $jumlah_kotor = $this->input->post('jumlah_kotor', TRUE);
        $potongan_bpjs_kesehatan = $this->input->post('potongan_bpjs_kesehatan', TRUE);
        $potongan_pensiun = $this->input->post('potongan_pensiun', TRUE);
        $potongan_lain = $this->input->post('potongan_lain', TRUE);
        $potongan_cp = $this->input->post('potongan_cp', TRUE);
        $jumlah_potongan = $this->input->post('jumlah_potongan', TRUE);
        $jumlah_bersih_bayar = $this->input->post('jumlah_bersih_bayar', TRUE);
        if($gj_baru == null || $gj_baru == ''){
            $gj_baru = $jumlah_bersih_bayar;
        }

        if($flaging == 1){
            //tabel gapokperubahan

            $gaji_pokok_baru = $gaji_pokok;
            $tunjangan_istri_baru = $tunjangan_istri;
            $tunjangan_anak_baru = $tunjangan_anak;
            $jumlah_tunjangan_keluarga_baru = $jumlah_tunjangan_keluarga;
            $jumlah_penghasilan_baru = $jumlah_penghasilan;
            $tunjangan_umum_baru = $tunjangan_umum;
            $tunjangan_umum_tambahan_baru = $tunjangan_umum_tambahan;
            $tunjangan_struktural_baru = $tunjangan_struktural;
            $tunjangan_fungsional_baru = $tunjangan_fungsional;
            $jiwa_baru = $jiwa;
            $tunjangan_beras_baru = $tunjangan_beras;
            $tunjangan_pph_baru = $tunjangan_pph;
            $pembulatan_baru = $pembulatan;
            $jumlah_kotor_baru = $jumlah_kotor;
            $potongan_bpjs_kesehatan_baru = $potongan_bpjs_kesehatan;
            $potongan_pensiun_baru = $potongan_pensiun;
            $potongan_lain_baru = $potongan_lain;
            $potongan_cp_baru = $potongan_cp;
            $jumlah_potongan_baru = $jumlah_potongan;
            $jumlah_bersih_bayar_baru = $jumlah_bersih_bayar;

            $data = array(
                'jenis_susulan' => $jenis_susulan,
                'nip' => $nip,
                'nama' => $nama,
                'periode_start' => $periode_start,
                'periode_end' => $periode_end,
                'lama' => $gj_lama,
                'baru' => $gj_baru,
                'jumlah_bulan' => $jml_bl,
                'gaji_pokok_lama' => $gaji_pokok,
                'gaji_pokok_baru' => $gaji_pokok_baru,
                'tunjangan_istri_lama' => $tunjangan_istri,
                'tunjangan_istri_baru' => $tunjangan_istri_baru,
                'tunjangan_anak_lama' => $tunjangan_anak,
                'tunjangan_anak_baru' => $tunjangan_anak_baru,
                'jumlah_tunjangan_keluarga_lama' => $jumlah_tunjangan_keluarga,
                'jumlah_tunjangan_keluarga_baru' => $jumlah_tunjangan_keluarga_baru,
                'jumlah_penghasilan_lama' => $jumlah_penghasilan,
                'jumlah_penghasilan_baru' => $jumlah_penghasilan_baru,
                'tunjangan_umum_lama' => $tunjangan_umum,
                'tunjangan_umum_baru' => $tunjangan_umum_baru,
                'tunjangan_umum_tambahan_lama' => $tunjangan_umum_tambahan,
                'tunjangan_umum_tambahan_baru' => $tunjangan_umum_tambahan_baru,
                'tunjangan_struktural_lama' => $tunjangan_struktural,
                'tunjangan_struktural_baru' => $tunjangan_struktural_baru,
                'tunjangan_fungsional_lama' => $tunjangan_fungsional,
                'tunjangan_fungsional_baru' => $tunjangan_fungsional_baru,
                'tunjangan_beras_lama' => $tunjangan_beras,
                'tunjangan_beras_baru' => $tunjangan_beras_baru,
                'tunjangan_pph_lama' => $tunjangan_pph,
                'tunjangan_pph_baru' => $tunjangan_pph_baru,
                'pembulatan_lama' => $pembulatan,
                'pembulatan_baru' => $pembulatan_baru,
                'jiwa_lama' => $jiwa,
                'jiwa_baru' => $jiwa_baru,
                'jumlah_kotor_lama' => $jumlah_kotor,
                'jumlah_kotor_baru' => $jumlah_kotor_baru,
                'potongan_bpjs_kesehatan_lama' => $potongan_bpjs_kesehatan,
                'potongan_bpjs_kesehatan_baru' => $potongan_bpjs_kesehatan_baru,
                'potongan_pensiun_lama' => $potongan_pensiun,
                'potongan_pensiun_baru' => $potongan_pensiun_baru,
                'potongan_lain_lama' => $potongan_lain,
                'potongan_lain_baru' => $potongan_lain_baru,
                'potongan_cp_lama' => $potongan_cp,
                'potongan_cp_baru' => $potongan_cp_baru,
                'jumlah_potongan_lama' => $jumlah_potongan,
                'jumlah_potongan_baru' => $jumlah_potongan_baru,
                'jumlah_bersih_bayar_lama' => $jumlah_bersih_bayar,
                'jumlah_bersih_bayar_baru' => $jumlah_bersih_bayar_baru,
                'pegawai_id' => $pegawai_id,
                'keterangan' => $keterangan,
                'posisi_acc' => $posisi_acc,
                'status' => 0,
                'posted_by' => $userid,
                'created_at' => $now
            );
        } else {
            $data = array(
                'jenis_susulan' => $jenis_susulan,
                'nip' => $nip,
                'nama' => $nama,
                'periode_start' => $periode_start,
                'periode_end' => $periode_end,
                'lama' => $gj_lama,
                'baru' => $gj_baru,
                'jumlah_bulan' => $jml_bl,
                'gaji_pokok' => $gaji_pokok,
                // 'gaji_pokok_baru' => $gaji_pokok_baru,
                'tunjangan_istri' => $tunjangan_istri,
                // 'tunjangan_istri_baru' => $tunjangan_istri_baru,
                'tunjangan_anak' => $tunjangan_anak,
                // 'tunjangan_anak_baru' => $tunjangan_anak_baru,
                'jumlah_tunjangan_keluarga' => $jumlah_tunjangan_keluarga,
                // 'jumlah_tunjangan_keluarga_baru' => $jumlah_tunjangan_keluarga_baru,
                'jumlah_penghasilan' => $jumlah_penghasilan,
                // 'jumlah_penghasilan_baru' => $jumlah_penghasilan_baru,
                'tunjangan_umum' => $tunjangan_umum,
                // 'tunjangan_umum_baru' => $tunjangan_umum_baru,
                'tunjangan_umum_tambahan' => $tunjangan_umum_tambahan,
                // 'tunjangan_umum_tambahan_baru' => $tunjangan_umum_tambahan_baru,
                'tunjangan_struktural' => $tunjangan_struktural,
                // 'tunjangan_struktural_baru' => $tunjangan_struktural_baru,
                'tunjangan_fungsional' => $tunjangan_fungsional,
                // 'tunjangan_fungsional_baru' => $tunjangan_fungsional_baru,
                'tunjangan_beras' => $tunjangan_beras,
                // 'tunjangan_beras_baru' => $tunjangan_beras_baru,
                'tunjangan_pph' => $tunjangan_pph,
                // 'tunjangan_pph_baru' => $tunjangan_pph_baru,
                'pembulatan' => $pembulatan,
                // 'pembulatan_baru' => $pembulatan_baru,
                'jiwa' => $jiwa,
                // 'jiwa_baru' => $jiwa_baru,
                'jumlah_kotor' => $jumlah_kotor,
                // 'jumlah_kotor_baru' => $jumlah_kotor_baru,
                'potongan_bpjs_kesehatan' => $potongan_bpjs_kesehatan,
                // 'potongan_bpjs_kesehatan_baru' => $potongan_bpjs_kesehatan_baru,
                'potongan_pensiun' => $potongan_pensiun,
                // 'potongan_pensiun_baru' => $potongan_pensiun_baru,
                'potongan_lain' => $potongan_lain,
                // 'potongan_lain_baru' => $potongan_lain_baru,
                'potongan_cp' => $potongan_cp,
                // 'potongan_cp_baru' => $potongan_cp_baru,
                'jumlah_potongan' => $jumlah_potongan,
                // 'jumlah_potongan_baru' => $jumlah_potongan_baru,
                'jumlah_bersih_bayar' => $jumlah_bersih_bayar,
                // 'jumlah_bersih_bayar_baru' => $jumlah_bersih_bayar_baru,
                'pegawai_id' => $pegawai_id,
                'keterangan' => $keterangan,
                'posisi_acc' => $posisi_acc,
                'status' => 0,
                'posted_by' => $userid,
                'created_at' => $now
            );
        }

        // echo json_encode($data);
        // die();

        $upd = $this->Fungsi_model->tambah($nmtable, $data);
        if($upd){
            $response = array(
                'status' => 1,
                'message' => 'Berhasil mengusulkan data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal mengusulkan data'
            );
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    public function getgajilama(){
        $periode_start = $this->input->post('periode_start', TRUE);
        $nip = $this->input->post('nip', TRUE);
        $kolom = $this->input->post('kolom', TRUE);

        $dt = $this->Gaji_susulan_model->gajilama($periode_start, $nip, $kolom);
        if($dt != null){

            $x = array(
                'nip' => $nip,
                // 'nominal' => number_format($dt->nominal, 0),
                'nominal' => $dt->nominal,
                'lokasi_kode' => $dt->lokasi_kode
            );

            $response = array(
                'status' => 1,
                'message' => 'Success',
                'data' => $x
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Failed',
                'data' => $dt
            );
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    public function getgajiakhir(){
        // $periode_end = $this->input->post('periode_end', TRUE);
        $nip = $this->input->post('nip', TRUE);
        $kolom = $this->input->post('kolom', TRUE);

        // $data = array(
        //     'periode_end' => $periode_end,
        //     'nip' => $nip
        // );

        $g = $this->Gaji_susulan_model->gajiAkhir($nip, $kolom);

        if($g != null){

            $x = array(
                'nip' => $nip,
                // 'nominal' => number_format($g->nominal, 0),
                'nominal' => $g->nominal,
            );

            $response = array(
                'status' => 1,
                'message' => 'Success',
                'data' => $x
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Failed',
                'data' => $g
            );
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    public function getJumlahBersihBayar(){
        $nip = $this->input->post('nip', TRUE);

        $dt = $this->Gaji_susulan_model->getGaji($nip);
        if($dt != null){
            $response = array(
                'status' => 1,
                'message' => 'Success',
                'data' => $dt
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Failed',
                'data' => $dt
            );
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    public function getJumlahBersihBayarFungsional(){
        $nip = $this->input->post('nip', TRUE);

        $dt = $this->Gaji_susulan_model->getGajiUntukFungsional($nip);
        if($dt != null){
            $response = array(
                'status' => 1,
                'message' => 'Success',
                'data' => $dt
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Failed',
                'data' => $dt
            );
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($response);
    }

    public function ajukan_action(){
        $role = $this->session->userdata('role');
        $user_id = $this->session->userdata('id');
        $id_lokasis = $this->session->userdata('id_lokasis');
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');
        $bl = date('m');
        $tahun = date('Y');

        if($role == 3){
            //kasubbag keu
            
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            if($A_01 != '86'){
                $B_03A = getDataMastfip($B_02B)->B_03A;
                $B_03 = getDataMastfip($B_02B)->B_03;
                $B_03B = getDataMastfip($B_02B)->B_03B;
    
                $kodeSekretaris = cari_kode_sekertaris($A_01);
                $nipSekretaris = getNipByKolok($kodeSekretaris);
                $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
                $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
                $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
                $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;
    
                //add notif
                tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipSekretaris, '0');
    
                //START add history acc
                $data_acc = array(
                    'A_01' => $A_01,
                    // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                    // 'kode_lokasi' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                    'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                    // 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                    'nip' => $nipSekretaris,
                    'nama' => $namaSekretaris,
                    'jabatan' => 'SEKRETARIS',
                    'status_acc' => 0,
                    'is_plt' => 0,
                    'posted_by' => $this->session->userdata('id'),
                    'created_at' => $now
                );
                $this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
                //END add history acc
    
                $dt = array(
                    'status' => 1,
                    'posisi_acc' => $nipSekretaris,
                    'updated_at' => $now
                );
    
                $upd = $this->Gaji_susulan_model->updateByUserIdByMonth('simgaji_susulan', $dt, $user_id, $bl, $tahun);
                if($upd) {
                    $response = array(
                        'status' => 1,
                        'message' => 'Berhasil memperbaharui data'
                    );
                } else {
                    $response = array(
                        'status' => 0,
                        'message' => 'Gagal memperbaharui data'
                    );
                }
            } else {
                $data = $this->ajukan_khusus_soejarwadi_action($A_01, $B_02B, $id_lokasis, $now);
            }
            
        } else if($role == 4){
            // echo 'kasubag TU Sekolah';
            // die();

            $dt = array(
                'status' => 1,
                'created_at' => $now
            );

            $upd = $this->Gaji_susulan_model->updateByUserIdByMonth('simgaji_susulan', $dt, $user_id, $bl, $tahun);
            if($upd){
                $A_01 = $this->session->userdata('A_01');
                $B_02B = $this->session->userdata('B_02B');
                // $B_03A = getDataMastfip($B_02B)->B_03A;
                // $B_03 = getDataMastfip($B_02B)->B_03;
                // $B_03B = getDataMastfip($B_02B)->B_03B;
    
                $nip_cabdin = $this->Gaji_susulan_model->getNipAcc($user_id, $bl)->posisi_acc;
                $B_03A = getDataMastfip($nip_cabdin)->B_03A;
                $B_03 = getDataMastfip($nip_cabdin)->B_03;
                $B_03B = getDataMastfip($nip_cabdin)->B_03B;
                $nama_cabdin = $B_03A . ' ' . $B_03 . ' ' . $B_03B;
    
                $detailCabdin = $this->Gaji_susulan_model->getJabatanCabdinByNip($nip_cabdin);
    
                //add notif
                tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nip_cabdin, '0');
    
                //START add history acc ke verifikator
                $data_acc = array(
                    'A_01' => $A_01,
                    'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                    'periode' => $periode,
                    'nip' => $nip_cabdin,
                    'nama' => $nama_cabdin,
                    // 'jabatan' => 'Cabang Dinas Pendidikan',
                    'jabatan' => $detailCabdin,
                    'status_acc' => 0,
                    'is_plt' => 0,
                    'posted_by' => $this->session->userdata('id'),
                    'created_at' => $now
                );
                $this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
                //END add history acc ke verifikator
    
                $response = array(
                    'status' => 1,
                    'message' => 'Berhasil memperbaharui data'
                );
            } else {
                //gagal update table

                $response = array(
                    'status' => 0,
                    'message' => 'Gagal memperbaharui data'
                );
            }

            
        } else if($role == 7){
            //cabdin
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $kodeKasubbagKeu = cari_kode_kasubbagkeu($A_01); //kode kasubbag keuangan disdik prov
            $nipDanPlt = getNipByKolokStruktural($kodeKasubbagKeu);
            $nipKasubbagKeu = $nipDanPlt['nip'];
            $is_plt = $nipDanPlt['is_plt'];
            $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
            $B_03AKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03A;
            $B_03KasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03;
            $B_03BKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03B;
            $namaKasubbagKeu = $B_03AKasubbagKeu . ' ' . $B_03KasubbagKeu . ' ' . $B_03BKasubbagKeu;

            //add notif
            tambahNotif('8',null, base_url('approval_usulan_p3k'), 'approval_usulan_p3k', 'Pegawai P3K', 'Usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKasubbagKeu, '0');

            $teksJabatan = 'KASUBBAG KEUANGAN SKPD';
            if($is_plt == 1){
                $teksJabatan = 'PLT KASUBBAG KEUANGAN SKPD';
            }

            //START add history acc
            $data_acc = array(
                'A_01' => $A_01,
                // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                // 'kode_lokasi' => '34', //34 2 digit kode induk DIsdik Prov
                'kode_lokasis' => '34', //34 2 digit kode induk DIsdik Prov
                'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                'nip' => $nipKasubbagKeu,
                'nama' => $namaKasubbagKeu,
                'jabatan' => 'SEKRETARIS',
                'status_acc' => 0,
                'is_plt' => $teksJabatan,
                'unit_kerja_plt' => $unit_kerja_plt,
                'created_at' => $now
            );
            $this->Fungsi_model->tambah('simgaji_historyp3ks_acc', $data_acc);
            //END add history acc

            $data = array(
                'status_ajuan' => 0,
                // 'posisi_acc' => $namaKasubbagKeu,
                'posisi_acc' => $nipKasubbagKeu,
                'status_acc_bkd' => 0,
                'updated_at' => $now
            );
        }

        // $upd = $this->GajiSusulan_model->updateDataUsulanByBulan('simgaji_susulan_acc', $data, $periode, $user_id);
        // if($upd){

        //     $response = array(
        //         'status' => 1,
        //         'message' => 'Berhasil memperbaharui data'
        //     );
        // } else {
        //     $response = array(
        //         'status' => 0,
        //         'message' => 'Gagal memperbaharui data'
        //     );
        // }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hapus_action(){
        $id = $this->input->post('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $upd = $this->Fungsi_model->edit('simgaji_susulan', $id, $data);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil hapus data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal hapus data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    private function ajukan_khusus_soejarwadi_action($A_01, $B_02B, $id_lokasis, $now){
        $B_03A = getDataMastfip($B_02B)->B_03A;
        $B_03 = getDataMastfip($B_02B)->B_03;
        $B_03B = getDataMastfip($B_02B)->B_03B;

        $kodeKadinas = cari_kode_kadinas($A_01);
        $nipDanPlt = getNipByKolokStruktural($kodeKadinas);
        $nipKadinas = $nipDanPlt['nip'];
        $is_plt = $nipDanPlt['is_plt'];
        $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
        $B_03AKep = getDataMastfip($nipKadinas)->B_03A;
        $B_03Kep = getDataMastfip($nipKadinas)->B_03;
        $B_03BKep = getDataMastfip($nipKadinas)->B_03B;
        $namaKadinas = $B_03AKep . ' ' . $B_03Kep . ' ' . $B_03BKep;

        //add notif
        tambahNotif('8',null, base_url('approval_susulan'), 'approval_susulan', 'Gaji Susulan', 'Usulan gaji susulan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKadinas, '0');

        $teksJabatan = 'KEPALA SKPD';
        if($is_plt == 1){
            $teksJabatan = 'PLT KEPALA SKPD';
        }

        //START add history acc
        $data_acc = array(
            'A_01' => $A_01,
            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
            'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
            // 'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
            'nip' => $nipKadinas,
            'nama' => $namaKadinas,
            'jabatan' => $teksJabatan,
            'status_acc' => 0,
            'is_plt' => $is_plt,
            'unit_kerja_plt' => $unit_kerja_plt,
            'posted_by' => $this->session->userdata('id'),
            'created_at' => $now
        );
        $this->Fungsi_model->tambah('simgaji_susulan_acc', $data_acc);
        //END add history acc

        $data = array(
            'status' => 1,
            'posisi_acc' => $nipKadinas,
            'updated_at' => $now
        );

        return $data;
    }
}