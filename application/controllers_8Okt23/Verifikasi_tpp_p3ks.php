<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Verifikasi_tpp_p3ks extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->verifikasi = 'simgaji_historyp3ks_tpp_acc';
        $this->pegawai = 'simgaji_pegawaip3ks_tpp';
        $this->history = 'simgaji_historyp3ks_tpp';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
        $this->load->model('Verifikasi_tpp_p3ks_model');
        $this->load->model('Tpp_history_p3k_model');
        $this->load->model('Tpp_pegawai_p3k_model');
    }

    //CRUD users START//
    public function get_verifikasi()
    {
        $periode = $this->uri->segment(3);

        $get = $this->Verifikasi_tpp_p3ks_model->get_verifikasi($periode);
        // echo json_encode($get);
        // die();
        $datatable = array();
        foreach ($get as $key => $value) {
            $merah = $value->tolak + $value->revisi;
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a target="BLANK" href="' . base_url('verifikasi_p3ks/detail/') . encode_url($value->pegawai_id) . '" >' . $value->B_02B . '</a>';
            $klik_kode = '<a href="javascript:void(0)" onclick="detailData(' . $value->pegawai_id . ')">' . $value->B_02B . '</a>';
            $datatable[$key] = array(
                'nip' => $klik_kode,
                'nama' => $value->B_03,
                'lokasikerja' => $value->lokasi_kerja,
                'lokasigaji' => $value->lokasi_gaji,
                'belum' => $value->belum,
                'tolak' => $merah,
                'setuju' => $value->setuju,
                'id' => '<input type="checkbox" class="data-check" value="' . $value->id . '">',
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$add,
            );
        }
        // $data['datatable'] = $datatable;

        $output = array(

            "recordsTotal" => $this->Verifikasi_tpp_p3ks_model->verifikasi_count_all($periode),
            "recordsFiltered" => $this->Verifikasi_tpp_p3ks_model->verifikasi_count_filtered($periode),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function cek_verif(){

            $B_02B = $this->session->userdata('B_02B');
            // echo $B_02B;
            // die();
            $d = ifVerif2($B_02B);
            if($d != '');

            $select = explode(',',$d);
            $datasel = NULL;
            foreach ($select as $key => $val){
                $datasel[$key] = $val;
                // $this->db->or_where('c.i', $val);
            }

            $x = array_values($datasel);
            echo json_encode($x);
    }

    public function get_detail_verifikasi(){
        // $id = $this->input->post("id");    
        // $real_id = decode_url($id);

        $real_id = $this->input->post("id"); 
        $cek = cek_realparamsPegawaiId($this->history, $real_id);
        $get = $this->Tpp_history_p3k_model->getDataByPegawaiId($real_id);
        $i=1;
        $output = '';
        $output .= '<div class="table-responsive">
                    <table class="table table-bordered"> 
                    Data Pegawai:'.getNama($real_id).'

                    <div class="row col-md-12">
                    <div class="col-md-12">
                            <tr>
                                <td width="5%">No</td>  
                                <td hidden>Id</td>  
                                <td >Params</td>  
                                <td >Lama</td>
                                <td >Baru</td>    
                                <td >Ket</td>
                                <td >Posisi Ajuan</td>
                                <td >Status Acc</td>    
                                <td width="25%">Verifikasi BKD</td>
                                <td >Efile</td>
                                <td >Tandai</td>
                            </tr> 
                    </div>
                    <div class="col-md-12">';
                    
        foreach($get as $value)
        {

            $sts_ajuan = '';
                if($value->status_ajuan == 0){
                    $sts_ajuan = 'UPT / Sekolah';
                } else if($value->status_ajuan == 1){
                    $sts_ajuan = 'Kasubag Keuangan';
                } else if($value->status_ajuan == 2){
                    $sts_ajuan = 'Sekretaris';
                } else if($value->status_ajuan == 3){
                    $sts_ajuan = 'Kepala SKPD';
                }

                $output .= '<tr>
                            <td>'.$i++.'<input type="hidden" class="form-control" id="pegawai_id" name="pegawai_id[]" value="'.$value->pegawai_id.'"></td> 
                            <td hidden> <input id="id" name="id[]" value="'.$value->id.'">'.$value->id.'</td>
                            <td hidden> <input id="fieldz" name="fieldz[]" value="'.$value->fidel.'"></td>
                            <td>'.$value->params.'</td>
                            <td>'.cekKodeEd($value->fidel,$value->lama).'<input hidden class="form-control" id="lama" name="lama[]" value="'.$value->lama.'"></td>
                            <td>'.cekKodeEd($value->fidel,$value->baru).' <input hidden class="form-control" id="baru" name="baru[]" value="'.$value->baru.'"></td>
                            <td> <textarea  class="form-control" id="description" name="description[]">'.$value->description.'</textarea></td>
                            <td>'.$sts_ajuan.'<input type="hidden" class="form-control" id="status_ajuan" name="status_ajuan[]" value="'.$value->status_ajuan.'"></td>
                            <td>'.status_acc($value->status_acc).'<input type="hidden" class="form-control" id="status_acc" name="status_acc[]" value="'.$value->status_acc.'"></td>
                            <td>'.form_dropdown("status_acc_bkd[]", status_array2("belum diproses"), $value->status_acc_bkd, "id=\"status_acc_bkd\" class=\"form-control custom-select\"   required").'</td>
                            <td> <a href="'.base_url('verif_efile/data/').encode_url($value->id).'" target="_blank">Periksa Eile</a></td>
                            <td> <input id="id_del" name="id_del[]" type="checkbox" class="data-check checked" value="' . $value->id . '"></td>
                            </tr>';               
        }
        
        $output .= '</div>   
                    </div>
                    </table>
                    </div>';



       echo json_encode($output);
    }

    public function verifikator_bulk_update(){  
          
        $pegawai_id = $this->input->post('pegawai_id'); 
        $id = $this->input->post('id');
        $fieldz = $this->input->post('fieldz');
        $lama = $this->input->post('lama');
        $baru = $this->input->post('baru');
        $description = $this->input->post('description');
        $status_acc_bkd = $this->input->post('status_acc_bkd');
        $status_acc = $this->input->post('status_acc');
        $status_ajuan = $this->input->post('status_ajuan');
       
        
        // $result = array();
        // foreach ($id as $key => $val) {
        //    $result[] = array(
        //       'id' => $id[$key],
        //       'lama' => $lama[$key],
        //       'baru' => $baru[$key],
        //       'description' => $description[$key],
        //       'status_acc_bkd' => $status_acc_bkd[$key],
        //       'updated_at' => date('Y-m-d H:i:s')
               
        //    );
        // }         
        // $this->db->update_batch($this->history, $result, 'id');  
        
                $datahistory = array();
                for ($x = 0; $x < sizeof($id); $x++) {
                    
                    $datahistory[] = array(
                        'pegawai_id' => $pegawai_id[$x],
                        'id' => $id[$x],
                        'lama' => $lama[$x],
                        'baru' => $baru[$x],
                        'description' => $description[$x],
                        'status_ajuan' => $status_ajuan[$x],
                        'status_acc' => $status_acc[$x],
                        'status_acc_bkd' => $status_acc_bkd[$x],
                        'updated_at' => date('Y-m-d H:i:s')
                    );

                    if($status_ajuan[$x] == 3 AND $status_acc[$x] == 1 AND $status_acc_bkd[$x]  == 1){

                        $data = array(
                            $fieldz[$x] => $baru[$x],
                        );

                        $this->db->where('id', $pegawai_id[$x]);
                        $this->db->update($this->pegawai, $data);
                        // $this->db->update('simgaji_pegawais', $data);
                    }

                    if($fieldz[$x] == 'lokasi_gaji'){

                        $a = $baru[$x];
                        $z = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$a'")->row();

                        $data_z = array(
                            'lokasigaji' => $z->kode,
                        );

                        $this->db->where('id', $pegawai_id[$x]);
                        $this->db->update($this->pegawai, $data_z);

                    }

                    if($fieldz[$x] == 'lokasi_kerja'){

                        $b = $baru[$x];
                        $y = $this->db->query("SELECT * FROM simgaji_lokasis WHERE id='$b'")->row();

                        $data_y = array(
                            'lokasikerja' => $z->kode,
                        );

                        $this->db->where('id', $pegawai_id[$x]);
                        $this->db->update($this->pegawai, $data_y);

                    }
                    
                    


                }
            json_encode($datahistory);
            $ins = $this->db->update_batch($this->history, $datahistory, 'id');
        // $datahistory =   $this->input->post('data');
        // $ins = $this->db->update_batch($this->history, $datahistory, 'id');

            

        $output = array(
            'status' => 'success',
            'message' => 'Berhasil Update Data',
            'data' => $datahistory,
            ''
        );
        echo json_encode($output);

     }

     public function verifikator_bulk_delete(){  
          
        $list_id = $this->input->post('id_del');

        $pegawai_id = $this->input->post('pegawai_id');
        
        


        foreach ($list_id as $id) {

            $this->db->where('id', $id);
            $this->db->delete($this->history);
        }   

        $output = array(
            'status' => 'success',
            'message' => 'Berhasil Hapus Data',
            'data' => $pegawai_id
        );
        echo json_encode($output);

     }

    public function get_periode()
    {
        $get = $this->Verifikasi_tpp_p3ks_model->get_periode();
        $datatable = array();
        foreach ($get as $key => $value) {
            //<a href="'. base_url('mutasi_skpd/edit/') .  encode_url($value->id) .'" class="btn btn-outline bg-warning border-warning text-warning btn-icon rounded-round legitRipple"><i class="icon-pencil"></i></a>
            // $klik_kode = '<a href="' . base_url('verifikasi_p3ks/periode/') . $value->bulan . '/' . $value->tahun . '" >' . $value->tahun . '-' . $value->bulan . '-01</a>';
            $klik_kode = '<a href="' . base_url('verifikasi_tpp_p3ks/periode/') . $value->periode . '">' . $value->periode . '</a>';

            $no = 1;
            $datatable[$key] = array(

                'id' => $no++,
                'periode' => $klik_kode,
                // 'kode_skpd' => $value->kode_skpd.'&nbsp;'.$add,
            );
        }
        // $data['datatable'] = $datatable;

        $output = array(

            "recordsTotal" => $this->Verifikasi_tpp_p3ks_model->periode_count_all(),
            "recordsFiltered" => $this->Verifikasi_tpp_p3ks_model->periode_count_filtered(),
            "data" => $datatable,
        );
        // output to json format
        echo json_encode($output);
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function index()
    {

        $data_header['session'] = $this->session->all_userdata();
        $data['session'] = $this->session->all_userdata();
        $data['title'] = 'Periode Verifikasi TPP P3K';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Periode Verifikasi TPP P3K</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('verifikasi_tpp_p3ks/periode', $data);
    }

    public function periode()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['session'] = $this->session->all_userdata();
        $data['title'] = 'Verifikasi TPP P3K';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Verifikasi TPP P3K</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();
        $nip = $this->session->userdata('B_02B');
        $data['verifikator'] = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip ='$nip'");


        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('verifikasi_tpp_p3ks/index', $data);
    }

    public function detail($id)
    {
        $real_id = decode_url($id);

        $cek = cek_realparamsPegawaiId($this->history, $real_id);
        if ($cek) {
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_history_p3k_model->getDetailPegawaiById($real_id);

            $get = $this->Tpp_history_p3k_model->getDataByPegawaiId($real_id);

            $datatable = array();
            foreach ($get as $key => $value) {


                $datatable[$key] = array(
                    'kode_id' => $value->kode_id,
                    'params' => $value->params,
                    // 'lama' => $value->lama,
                    'lama' => cekKode($value->kode_id, $value->lama),
                    // 'baru' => $value->baru,
                    'baru' => cekKode($value->kode_id, $value->baru),
                    'ket' => $value->description,
                    'status' => $value->status_ajuan,
                    // 'jalteks_baru' => cekKode($value->kode_id, $value->baru)
                );
            }
            $data['datatable'] = $datatable;

            $data['data_usulan'] = $this->Tpp_history_p3k_model->getDataByPegawaiIdBandingkan($real_id);

            //start data existing
            $data['kodes'] = $this->Pegawai_p3k_model->getDataKodes();

            $data['lokasi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_bools');
            $data['statusp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statusp3ks');
            $data['status'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golongans');
            $data['golonganp3k'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_golonganp3ks');
            $data['eselon'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_kedudukans');

            $data['fungsional'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Pegawai_p3k_model->getDataDataMaster('simgaji_profesis');
            //end data existing

            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
            $this->load->view('verifikasi_tpp_p3ks/detail_usulan_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {
            $array_msg = array(
                'status' => 'error',
                'message' => 'Invalid parameter'
            );

            $this->session->set_flashdata($array_msg);
            redirect('verifikasi_p3ks/detail/' . $id);
        }
    }

    public function detail_action()
    {

        $uri = $this->input->post('uri', TRUE);
        $real_id = decode_url($uri);
        $user_id = $this->session->userdata('id');
        $id_history = $this->input->post('id_history');
        $field = $this->input->post('field');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');

        

            // $field_name = $this->Fungsi_model->getFieldNameByKodeId($field);

            // $nilai_lama = $this->Pegawai_p3k_model->getDetailById($pegawai_id)->$field_name;

            // $nilai_baru = $this->input->post($field_name, TRUE);

            $datahistory = array();
            for ($x = 0; $x < sizeof($id_history); $x++) {
                
                $datahistory[] = array(
                    'id' => $id_history[$x],
                    'kode_id' => $kode_id[$x],
                    // 'lama' => $nilai_lama,
                    // 'baru' => $nilai_baru,
                    'description' => 'Data telah diverifikasi',
                    'status_ajuan' => 3,
                    'posted_by' => $user_id,
                    'updated_at' => $now
                );
            }

            // json_encode($datahistory);
            $ins = $this->db->update_batch($this->history, $datahistory, 'id');
            // echo json_encode($datahistory);
            // $ins = $this->db->update_batch($this->history, $datahistory, 'id');
            // $ins = $this->Fungsi_model->tambah_bulk($this->history, $datahistory);
            if ($ins) {
                //add history acc
                $data_acc = array(
                    'pegawais_id' => $pegawai_id,
                    'status_acc' => 1,
                    'created_at' => $now
                );
                // $this->Fungsi_model->edit('simgaji_historyp3ks_tpp_acc', '', $data_acc);

                $this->db->where('pegawais_id', $pegawai_id);
                $this->db->where('periode', $periode);
                $update = $this->db->update('simgaji_historyp3ks_tpp_acc', $data_acc); 

                //add notif inbox2
                $A_01 = $this->session->userdata('A_01');
                $B_02B = $this->session->userdata('B_02B');
                $B_03A = getDataMastfip($B_02B)->B_03A;
                $B_03 = getDataMastfip($B_02B)->B_03;
                $B_03B = getDataMastfip($B_02B)->B_03B;
                tambahNotif('8', null, base_url('history_p3ks'), 'history_p3ks', 'Pegawai P3K', 'Hasil Ajuan', $B_02B, $B_03A, $B_03, $B_03B, $A_01, '196608071985112001', '0');

                $array_msg = array(
                    'status' => 'success',
                    'message' => 'Berhasil mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('verifikasi_p3ks/detail/'.$uri);
            } else {
                $array_msg = array(
                    'status' => 'error',
                    'message' => 'Gagal mengusulkan data'
                );

                $this->session->set_flashdata($array_msg);
                redirect('verifikasi_p3ks/detail/'.$uri);
            }
       
    }

    public function detail_action3(){
        $id_history = $this->input->post('id_history');
        $field = $this->session->userdata('field');
        $pegawai_id = $this->input->post('pegawai_id', TRUE);
        $kode_id = $this->input->post('kode_id', TRUE);

        echo json_encode($id_history);


    }
}
