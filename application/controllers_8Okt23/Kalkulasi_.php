<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalkulasi extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');

		// $this->load->model('Fungsi_model');
		

        $this->tb_lokasi = 'simgaji_lokasis';
        $this->tb_kalkulasi = 'simgaji_kalkulasis';
		$this->tb_kalkulasi_tpps = 'simgaji_kalkulasi_tpp';
		$this->tb_kalkulasip3k = 'simgaji_kalkulasip3ks';
		$this->tb_kalkulasip3k_tb = 'simgaji_kalkulasip3ks_tb';
		$this->tb_kalkulasip3k_eb = 'simgaji_kalkulasip3ks_eb';
		$this->tb_kalkulasip3k_tahunan = 'simgaji_kalkulasip3ks_tahunan';

        $this->tb_pegawai = 'simgaji_pegawais';
		$this->tb_pegawai_tpp = 'simgaji_pegawai_tpp';
		$this->tb_pegawai_tpp_kinerja = 'simgaji_masttpp_kinerja';
		$this->tb_pegawaip3k = 'simgaji_pegawaip3ks';
		$this->tb_pegawaip3k_13 = 'simgaji_pegawaip3k_13';
		$this->tb_pegawaip3k_14 = 'simgaji_pegawaip3k_14';
		// $this->tb_pegawaip3k_tahunan = 'simgaji_pegawaip3k_tahunan';
		$this->tb_pegawaip3k_tahunan = $this->tb_pegawaip3k;

		$this->tb_tjumum = 'simgaji_tjumums';
		$this->tb_tjumump3k = 'simgaji_tjumump3ks';
		$this->tb_tjfungsional = 'simgaji_tjfungsionals';
		$this->tb_fungsional = 'simgaji_fungsionals';
		$this->tb_eselon = 'simgaji_eselons';
		$this->tb_kedudukan = 'simgaji_kedudukans';
		$this->tb_golongan = 'simgaji_golongans';
		$this->tb_golonganp3k = 'simgaji_golonganp3ks';
		$this->tb_status = 'simgaji_statuses';
		$this->tb_statusp3k = 'simgaji_statusp3ks';
		$this->tb_marital = 'simgaji_maritals';
		$this->tb_beras = 'simgaji_berases';


		$this->potongans = 'simgaji_masttpp_kinerja';

	}

    public function index(){
		$data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Kalkulasi';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
        <span class="breadcrumb-item active">Kalkulasi</span>';

        $data['opd'] = $this->Fungsi_model->get_opd();

        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
		$this->load->view('kalkulasi/kalkulasi_index', $data);
    }

	function pph_old($istri, $anak, $pen_tht, $gaji_kotor_pembulatan,$nip){
		if($gaji_kotor_pembulatan < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
		} 
		else {
			$tambahan = 0;
		}
		
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
		
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12 ;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	////pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*50000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		$pph_bulan	 = floor($pph_tahun/12) ;
		
		return $pph_bulan ;
	}

	function pph($istri, $anak, $pen_tht, $gaji_kotor_pembulatan,$nip){
		if($gaji_kotor_pembulatan < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
		} 
		else {
			$tambahan = 0;
		}
		
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}
		
		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12 ;
		
		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);	////pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 	
		if(substr($nip,14,1)==2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);
	
		if($penghasilan_kpj < 0) {
			 $penghasilan_kpj = 0;
		}		
		$penghasilan_kpj = substr_replace($penghasilan_kpj,"000",-3);
		
		if($penghasilan_kpj <= 60000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		}
		elseif($penghasilan_kpj > 60000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		=(0.05 * 60000000) + (0.15 * ($penghasilan_kpj - 60000000));
		}
		elseif($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		=(0.05*60000000) + (0.15*200000000) + (0.25*($penghasilan_kpj-250000000));
		}
		elseif($penghasilan_kpj > 500000000) {
			$pph_tahun		=(0.05*60000000) + (0.15*200000000)+(0.25*250000000)+(0.3*($penghasilan_kpj-500000000));
		}
		$pph_bulan	 = floor($pph_tahun/12) ;
		
		return $pph_bulan ;
	}

	function pph_13($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip)
	{
		if ($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}

		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;

		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}

		$pen_tht_tahun = $pen_tht * 12;

		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;

		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}

		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}

		if ($penghasilan_kpj_13 < 0) {
			$penghasilan_kpj_13 = 0;
		}

		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13, "000", -3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		// PPH Biasa
		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}

		// PPH 13
		if ($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		} elseif ($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		} elseif ($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj_13 - 250000000));
		} elseif ($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj_13 - 500000000));
		}

		return $pph_tahun_13 - $pph_tahun;
	}

	function pph_14($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip)
	{
		if ($gaji_kotor_pembulatan < 1000000 || $gaji_kotor_pembulatan_13 < 1000000) {
			$tambahan = 1000000 - $gaji_kotor_pembulatan;
			$tambahan_13 = 1000000 - $gaji_kotor_pembulatan_13;
		} else {
			$tambahan = 0;
			$tambahan_13 = 0;
		}

		// Perhitungan Setahun + 13
		$gaji_kotor	= $gaji_kotor_pembulatan + $tambahan;
		$gaji_kotor_tahun = $gaji_kotor * 12;
		$gaji_kotor_bulan_13 = $gaji_kotor_pembulatan_13;
		$gaji_kotor_tahun_13 = $gaji_kotor_tahun + $gaji_kotor_bulan_13;

		$bea_jabatan_13 = 0.05 * $gaji_kotor_tahun_13;
		if ($bea_jabatan_13 > 6000000) {
			$bea_jabatan_13 = 6000000;
		}

		$pen_tht_tahun = $pen_tht * 12;
		$kurang_gaji_tahun = $pen_tht_tahun + $bea_jabatan_13;
		$penghasilan_tahun_13 = $gaji_kotor_tahun_13 - $kurang_gaji_tahun;

		// Perhitungan Setahun
		$bea_jabatan = 0.05 * $gaji_kotor;
		if ($bea_jabatan > 500000) {
			$bea_jabatan = 500000;
		}

		$kurang_gaji = $bea_jabatan + $pen_tht;
		$penghasilan_bulan	= $gaji_kotor - $kurang_gaji;
		$penghasilan_tahun	= $penghasilan_bulan * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj_13 = round($penghasilan_tahun_13 - $ptkp);
		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}

		if ($penghasilan_kpj_13 < 0) {
			$penghasilan_kpj_13 = 0;
		}

		$penghasilan_kpj_13 = substr_replace($penghasilan_kpj_13, "000", -3);
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		// PPH Biasa
		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}

		// PPH 13
		if ($penghasilan_kpj_13 <= 50000000) {
			$pph_tahun_13		= (0.05 * $penghasilan_kpj_13);
		} elseif ($penghasilan_kpj_13 > 50000000 && $penghasilan_kpj_13 <= 250000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj_13 - 50000000));
		} elseif ($penghasilan_kpj_13 > 250000000 && $penghasilan_kpj_13 <= 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj_13 - 250000000));
		} elseif ($penghasilan_kpj_13 > 500000000) {
			$pph_tahun_13		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj_13 - 500000000));
		}

		/*if($nip == '196002291986031004') {
			echo $bea_jabatan."=".$bea_jabatan_13."=";
			echo $kurang_gaji."=".$penghasilan_bulan."=@";
			echo $ptkp."=";
			echo $penghasilan_tahun."=".$penghasilan_kpj."=".$penghasilan_kpj_13."=#";
			echo $pph_tahun."=".$pph_tahun_13."=";
			echo $pph_tahun_13 - $pph_tahun;
			
			die();
		}*/

		return $pph_tahun_13 - $pph_tahun;
	}

	function pph_desember($istri, $anak, $pen_tht, $gaji_kotor_pembulatan, $nip, $gaji_13, $gaji_14, $jumlah_pph, $tahun, $gaji_pokok, $tunjangan_istri, $tunjangan_anak)
	{
		$sql_gaji_kotor =
			"SELECT SUM(gaji_pokok + tunjangan_istri + tunjangan_anak + tunjangan_struktural + tunjangan_beras + tunjangan_fungsional + tunjangan_umum + pembulatan) AS gaji_kotor
		FROM $this->tb_kalkulasip3k WHERE nip = '" . $nip . "' AND MONTH(periode) < 12 AND YEAR(periode) = " . $tahun;
		/*$sql_gaji_kotor = 
		"SELECT SUM(k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak + k.tunjangan_struktural + k.tunjangan_beras + k.tunjangan_fungsional 
+ k.tunjangan_umum + k.pembulatan) + f.jumlah_penghasilan_4 AS gaji_kotor
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND MONTH(k.periode) < 12 AND YEAR(k.periode) = ".$tahun;*/

		// $gaji_kotor = DB::query(Database::SELECT, $sql_gaji_kotor)->execute()->get('gaji_kotor', 0);
		$gaji_kotor = $this->db->query($sql_gaji_kotor)->row()->gaji_kotor;
		$bruto_12 = $gaji_kotor + $gaji_kotor_pembulatan + $gaji_13 + $gaji_14;

		$sql_pen_tht =
			"SELECT SUM(0.0475 * (gaji_pokok + tunjangan_istri + tunjangan_anak)) AS pen_tht_12
		FROM kalkulasis WHERE nip = '" . $nip . "' AND MONTH(periode) < 12 AND YEAR(periode) = " . $tahun;

		/*$sql_pen_tht = 
		"SELECT SUM(0.0475 * (k.gaji_pokok + k.tunjangan_istri + k.tunjangan_anak))+ (0.0475 * (f.gaji_pokok_4 + f.tunjangan_istri_4 + f.tunjangan_anak_4)) AS pen_tht_12
FROM kalkulasis k
JOIN kalkulasis_rapels_finals f ON f.nip=k.nip
WHERE k.nip = '".$nip."' AND MONTH(k.periode) < 12 AND YEAR(k.periode) = ".$tahun;*/

		// $pen_tht_12 = DB::query(Database::SELECT, $sql_pen_tht)->execute()->get('pen_tht_12', 0);
		$pen_tht_12 = $this->db->query($sql_pen_tht)->row()->pen_tht_12;

		$pen_tht_12 = $pen_tht_12 + (0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak));
		if ($pen_tht_12 > 2400000) {
			$pen_tht_12 = 2400000;
		}

		$bea_jabatan_12 = ceil(0.05 * $bruto_12);
		if ($bea_jabatan_12 > 6000000) {
			$bea_jabatan_12 = 6000000;
		}

		$kurang_gaji_12 = $bea_jabatan_12 + $pen_tht_12;
		$penghasilan_tahun	= $bruto_12 - $kurang_gaji_12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$penghasilan_kpj = round($penghasilan_tahun - $ptkp);

		if ($penghasilan_kpj < 0) {
			$penghasilan_kpj = 0;
		}
		$penghasilan_kpj = substr_replace($penghasilan_kpj, "000", -3);

		if ($penghasilan_kpj <= 50000000) {
			$pph_tahun		= (0.05 * $penghasilan_kpj);
		} elseif ($penghasilan_kpj > 50000000 && $penghasilan_kpj <= 250000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * ($penghasilan_kpj - 50000000));
		} elseif ($penghasilan_kpj > 250000000 && $penghasilan_kpj <= 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($penghasilan_kpj - 250000000));
		} elseif ($penghasilan_kpj > 500000000) {
			$pph_tahun		= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($penghasilan_kpj - 500000000));
		}
		//$pph_bulan	 = floor($pph_tahun/12) ;

		//dafiz
		/*$sql_pph_rapel = 
		"SELECT tunjangan_pph_4 AS pph_rapel
		FROM kalkulasis_rapels_finals WHERE nip = '".$nip."' AND YEAR(periode) = ".$tahun;
				
		$pph_rapel = DB::query(Database::SELECT, $sql_pph_rapel)->execute()->get('pph_rapel', 0);*/
		//dafiz end

		$pph_bulan = $pph_tahun - $jumlah_pph;
		if ($pph_bulan <= 0) {
			$pph_bulan = 0;
		}

		return $pph_bulan;
		//return $bruto_12."#".$bea_jabatan_12."#".$pen_tht_12."#".$kurang_gaji_12;
		//return $gaji_kotor_pembulatan."#".$gaji_kotor_pembulatan_12."#".$bruto_12."#".$bea_jabatan_12."#".$kurang_gaji_12."#".$penghasilan_tahun."#".$ptkp."#".$penghasilan_kpj;
		//return $gaji_kotor."#".$bruto_12."#".$gaji_kotor_pembulatan."#".$gaji_13."#".$gaji_14;
		//return $pph_tahun."#".$jumlah_pph;
		//return $bruto_12;
	}

	function pph_bebankerja($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $kespeg, $lainnya = 0, $nip)
	{
		// $harga_beras = ORM::factory('beras')
		// 	->where('bool_id', '=', 2)
		// 	->find();
		$this->db->where('bool_id', 2);
		$harga_beras = $this->db->get('simgaji_berases')->row();

		$jumlah_jiwa	= 1 + $istri + $anak;
		$tjistri		= 0.1 * $gaji_pokok * $istri;
		$tjanak			= 0.02 * $gaji_pokok * $anak;
		$tjberas		= 10 * $harga_beras->harga * $jumlah_jiwa;

		$gator = $gaji_pokok + $tjistri + $tjanak + $tunjangan_struktural + $tunjangan_fungsional + $tjberas + $tunjangan_umum + $pembulatan + $kespeg + $lainnya;


		if ($gator < 1000000) {
			$tmhumum = 1000000 - $gator;
		} else {
			$tmhumum = 0;
		}
		$gator 		= $gator + $tmhumum;
		$beajab		= 0.05 * $gator;
		if ($beajab > 500000) {
			$beajab = 500000;
		}
		$pentht		= 0.0475 * ($gaji_pokok + $tjistri + $tjanak);
		#edy
		if ($pentht > 200000) {
			$pentht = 200000;
		}
		$kurgaji	= $beajab + $pentht;
		$pengnet1	= $gator - $kurgaji;
		$pengnet12	= $pengnet1 * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);		//pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$pengkpj	= (int) round($pengnet12 - $ptkp);

		if ($pengkpj < 0) {
			$pengkpj = 0;
		}

		$pKPJ = substr_replace($pengkpj, "000", -3);

		$pphThn = 0;

		if ($pKPJ <= 60000000) {
			$pphThn	= (0.05 * $pKPJ);
		} elseif ($pKPJ > 60000000 && $pKPJ <= 250000000) {
			$pphThn	= (0.05 * 60000000) + (0.15 * ($pKPJ - 60000000));
		} elseif ($pKPJ > 250000000 && $pKPJ <= 500000000) {
			$pphThn	= (0.05 * 60000000) + (0.15 * 200000000) + (0.25 * ($pKPJ - 250000000));
		} elseif ($pKPJ > 500000000) {
			$pphThn	= (0.05 * 60000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($pKPJ - 500000000));
		}
		return floor($pphThn / 12);
	}

	function pph_kespeg($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $kespeg, $konker_jakarta, $nip)
	{
		// $harga_beras = ORM::factory('beras')
		// 	->where('bool_id', '=', 2)
		// 	->find();
		$this->db->where('bool_id', 2);
		$harga_beras = $this->db->get('beras')->row();

		$jumlah_jiwa	= 1 + $istri + $anak;
		$tjistri		= 0.1 * $gaji_pokok * $istri;
		$tjanak			= 0.02 * $gaji_pokok * $anak;
		$tjberas		= 10 * $harga_beras->harga * $jumlah_jiwa;

		$gator = $gaji_pokok + $tjistri + $tjanak + $tunjangan_struktural + $tunjangan_fungsional + $tjberas + $tunjangan_umum + $pembulatan + $kespeg;

		if ($konker_jakarta > 0) {
			$gator = $gator + $konker_jakarta;
		}

		if ($gator < 1000000) {
			$tmhumum = 1000000 - $gator;
		} else {
			$tmhumum = 0;
		}
		$gator 		= $gator + $tmhumum;
		$beajab		= 0.05 * $gator;
		if ($beajab > 500000) {
			$beajab = 500000;
		}
		$pentht		= 0.0475 * ($gaji_pokok + $tjistri + $tjanak);
		#edy
		if ($pentht > 200000) {
			$pentht = 200000;
		}
		$kurgaji	= $beajab + $pentht;
		$pengnet1	= $gator - $kurgaji;
		$pengnet12	= $pengnet1 * 12;

		$ptkp = 54000000 + ($istri * 4500000) + (4500000 * $anak);		//pns 36.000.000 anakistri 3000.000 berlaku jan 2015 diterapkan sep 2015, pns=54000000 anak istri4500000 berlaku jan 2016 diterapkan mulai sept 2016 
		if (substr($nip, 14, 1) == 2) {
			$ptkp = 54000000;
		}

		$pengkpj	= (int) round($pengnet12 - $ptkp);

		if ($pengkpj < 0) {
			$pengkpj = 0;
		}

		$pKPJ = substr_replace($pengkpj, "000", -3);

		$pphThn = 0;

		if ($pKPJ <= 50000000) {
			$pphThn	= (0.05 * $pKPJ);
		} elseif ($pKPJ > 50000000 && $pKPJ <= 250000000) {
			$pphThn	= (0.05 * 50000000) + (0.15 * ($pKPJ - 50000000));
		} elseif ($pKPJ > 250000000 && $pKPJ <= 500000000) {
			$pphThn	= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * ($pKPJ - 250000000));
		} elseif ($pKPJ > 500000000) {
			$pphThn	= (0.05 * 50000000) + (0.15 * 200000000) + (0.25 * 250000000) + (0.3 * ($pKPJ - 500000000));
		}
		return floor($pphThn / 12);
	}
    

    public function action(){

      
		
        $lokasi_start = $_POST['lokasi_start'];
		$lokasi_end =$_POST['lokasi_end'];

		

        if($_POST['jenis'] != "GAJI_SUSULAN") {
			$periode = $_POST['tahun'];
			
			if(isset($_POST['bulan_id'])) {	
				$periode .= "-".$_POST['bulan_id']."-01";
			}
			
			$arrLokasi = array();
			if($_POST['lokasi_end']) {

				

				// echo $lokasi_start;
				// echo "<br/>";
				// echo $lokasi_end;
				// die();

                if(substr($lokasi_end,-3) == "000"){
                    $this->db->where('LEFT(kode,2)',substr($lokasi_end,0,2));
					// $this->db->like('kode', substr($lokasi_start,0,2));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
					// echo $max->kode;
					// die();

                }else{
                    $this->db->where('LEFT(kode,6)',substr($lokasi_end,0,6));
					// $this->db->like('kode', substr($lokasi_start,0,6));
                    $this->db->order_by('kode','DESC');
                    $max = $this->db->get($this->tb_lokasi)->row();
                }

                $this->db->where('kode >=', $lokasi_start);
                $this->db->where('kode <=', $max->kode);
                $lokasis = $this->db->get($this->tb_lokasi)->result();
					
				foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

				// echo json_encode($arrLokasi);
				// die();	

			} else {

				

                if(substr($lokasi_start,-4) == "0000") {

                    if(substr($lokasi_start,0,2) == "34") {

                        $this->db->where('LEFT("kode",2) =', 34);
                        $this->db->where('LEFT("kode",4) !=', 3411);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
						

					} else {
			
						$this->db->where('LEFT(kode,2) =', substr($lokasi_start,0,2));
						// $this->db->where('LEFT(kode,2) =',substr($lokasi_start,0,2),true);
                        $lokasis = $this->db->get($this->tb_lokasi)->result();
						// echo json_encode($lokasis);
						// die();
					}


                }else{

                    $this->db->where('LEFT("kode",6) =', substr($lokasi_start,0,6));
                    $lokasis = $this->db->get($this->tb_lokasi)->result();
				

                }

                foreach($lokasis as $lokasi) {
					array_push($arrLokasi,$lokasi->id);
				}	

			
				// echo json_encode($arrLokasi);
				// die();
				
				
			}
		}

		



        if($_POST['jenis'] == "GAJI_BULANAN") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasi);
				
			//print_r($arrLokasi);
			//die();	
		  	$this->gaji_bulanan($periode,$arrLokasi);
		}

		elseif($_POST['jenis'] == "TPP") {

			// print_r($arrLokasi);
			// die();	
            $this->db->where_in('lokasi_gaji', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasi_tpps);
				
			
			$this->tpp($periode,$arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_BULANAN_PPPK") {

			// print_r($arrLokasi);
			// die();	
            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasi_tpps);
				
			$this->gajip3k_bulanan($periode,$arrLokasi);
		  	
		}

		elseif($_POST['jenis'] == "GAJI_13_PPPK") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k_tb);
				
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_13($periode, $arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_14_PPPK") {

            $this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('periode', $periode);
            $this->db->delete($this->tb_kalkulasip3k_eb);
				
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_14($periode, $arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_DES_PPPK") {
	
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_desember($arrLokasi);
		}

		elseif($_POST['jenis'] == "GAJI_TAHUNAN_PPPK") {

            
			$this->db->where_in('lokasi_id', $arrLokasi);
            $this->db->where('YEAR(periode)', $periode);
            $this->db->delete($this->tb_kalkulasip3k_tahunan);	
			// print_r($arrLokasi);
			// die();	
			$this->gajip3k_tahunan($periode,$arrLokasi);
		}


    }

	function tpp($periode, $arrLokasi)
	{

		// echo json_encode($arrLokasi);
		// die();

		// $this->auto_render = false;
		error_reporting(0);

		$i = 1;
		$xperiode = explode("-", $periode);
		$value = "";

		// // RSJ
		// $arrRSJ = array(50, 67, 68, 69);
		// $arrKodeRSJ = array();
		// // $lokasi_rsj = ORM::factory('lokasi')
		// // 	->where('LEFT("kode",2)', 'IN', $arrRSJ)
		// // 	->find_all();
		// $this->db->where_in('LEFT("kode",2)',$arrRSJ);
		// $lokasi_rsj = $this->db->get($this->tb_lokasi)->result();

		// foreach ($lokasi_rsj as $rsj) {
		// 	array_push($arrKodeRSJ, $rsj->id);
		// }

		// // Jakarta
		// $arrJakarta = array(43);
		// $arrKodeJakarta = array();
		// // $lokasi_jakarta = ORM::factory('lokasi')
		// // 	->where('LEFT("kode",2)', 'IN', $arrJakarta)
		// // 	->find_all();
		// $this->db->where_in('LEFT("kode",2)',$arrJakarta);
		// $lokasi_jakarta = $this->db->get($this->tb_lokasi)->result();

		// foreach ($lokasi_jakarta as $jakarta) {
		// 	array_push($arrKodeJakarta, $jakarta->id);
		// }

		// // Dinsos
		// $arrDinsos = array(21);
		// $arrKodeDinsos = array();
		// // $lokasi_dinsos = ORM::factory('lokasi')
		// // 	->where('LEFT("kode",2)', 'IN', $arrDinsos)
		// // 	->find_all();
		// $this->db->where_in('LEFT("kode",2)',$arrDinsos);
		// $lokasi_dinsos = $this->db->get($this->tb_lokasi)->result();

		// foreach ($lokasi_dinsos as $dinsos) {
		// 	array_push($arrKodeDinsos, $dinsos->id);
		// }

		// // APBJ
		// $arrAPBJ = array('02020250');
		// $arrKodeAPBJ = array();
		// // $lokasi_APBJ = ORM::factory('lokasi')
		// // 	->where('kode', 'IN', $arrAPBJ)
		// // 	->find_all();
		// $this->db->where_in('LEFT("kode",2)',$arrAPBJ);
		// $lokasi_APBJ = $this->db->get($this->tb_lokasi)->result();
		// foreach ($lokasi_APBJ as $APBJ) {
		// 	array_push($arrKodeAPBJ, $APBJ->id);
		// }

		// // $lokasi_kks = ORM::factory('lokasi')
		// // 	->where('status_tb_kk', '=', 1)
		// // 	->find_all();

		// // $lokasi_tbs = ORM::factory('lokasi')
		// // 	->where('status_tb_kk', '=', 2)
		// // 	->find_all();

		// // $arrKK = array();
		// // $arrTB = array();

		// // foreach ($lokasi_kks as $kk) {
		// // 	array_push($arrKK, $kk->id);
		// // }

		// // foreach ($lokasi_tbs as $tb) {
		// // 	array_push($arrTB, $tb->id);
		// // }

		// // $insentips = ORM::factory('insentip')
		// // 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// // 	->where('tpp_stop', '=', 1)
		// // 	->find_all();
		// $this->db->where('nip', '196305261995031002');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where('tpp_stop', 0);
		// $this->db->limit(10);
		$insentips = $this->db->get($this->tb_pegawai_tpp)->result();

		// header('Content-Type: application/json; charset=utf-8');
		// echo json_encode($insentips);
		// die();

		/*$insentips = ORM::factory('insentip')
			->where('nip','=','195912041991031002')
			->find_all();*/

		$kelompok = "";
		foreach ($insentips as $insentip) {
			// $kalkulasi = ORM::factory('kalkulasi')
			// 	->where('nip', '=', $insentip->nip)
			// 	->where('periode', '=', $periode)
			// 	->find();
			$this->db->where('nip', $insentip->nip);
			$this->db->where('periode', $periode);
			$kalkulasi = $this->db->get($this->tb_kalkulasi)->row();

			// $n_kalkulasi = ORM::factory('kalkulasi')
			// 	->where('nip', '=', $insentip->nip)
			// 	->where('periode', '=', $periode)
			// 	->count_all();
			$this->db->where('nip', $insentip->nip);
			$this->db->where('periode', $periode);
			$n_kalkulasi = $this->db->get($this->tb_kalkulasi)->num_rows();

			// echo json_encode($n_kalkulasi);
			// die();

			if ($n_kalkulasi == 0) {

				// $setting = ORM::factory('setting', 1);
				// if ($setting->tahun_tpp == $xperiode[0] && $setting->periode_tpp == $xperiode[1]) {
				// 	$table = "insentips";
				// } else {
				// 	$table = "insentip_" . $xperiode[0] . "_" . $xperiode[1];
				// }
				// DB::update($table)
				// 	->set(array('tpp_stop' => '2'))
				// 	->where('nip', '=', $insentip->nip)
				// 	->execute();

				// continue;

				$setting = $this->db->get_where('simgaji_periodeberkala',array('type' => 2))->row();
				// echo $setting->periode_tpp;
				// die();

				


				if ($setting->tahun == $xperiode[0] && $setting->bulan == $xperiode[1]) {
					// $table = "insentips";
					$this->tb_pegawai_tpp = $this->tb_pegawai_tpp;
				} else {
					$this->tb_pegawai_tpp = $this->tb_pegawai_tpp . $xperiode[0] . "_" . $xperiode[1];
					// $table = "insentips";
				}

				$this->db->where('nip', $insentip->nip);
				$this->db->update($this->tb_pegawai_tpp, array('tpp_stop' => '1'));
				// continue;

			}
			

			// $pegawai = ORM::factory('pegawai')
			// 	->where('nip', '=', $insentip->nip)
			// 	->find();

			$this->db->where('nip', $insentip->nip);
			$pegawai = $this->db->get($this->tb_pegawai)->row();

		

			$lokasi_id = $insentip->lokasi_gaji;
			// $lokasi = ORM::factory('lokasi', $lokasi_id);

			$nama = $kalkulasi->name;
			$nip = $kalkulasi->nip;
			// $golongan_id = $kalkulasi->golongan_id;
			// $golongan_string = $kalkulasi->golongan_string;
			// $kelas_jabatan = $insentip->kelasjab;

			$gaji_pokok = $kalkulasi->gaji_pokok;
			$istri = $kalkulasi->istri;
			$anak = $kalkulasi->anak;
			$tunjangan_umum = $kalkulasi->tunjangan_umum;
			$tunjangan_struktural = $kalkulasi->tunjangan_struktural;
			$tunjangan_fungsional = $kalkulasi->tunjangan_fungsional;
			$pembulatan = $kalkulasi->pembulatan;

			$bpjs_gaji = $kalkulasi->jumlah_penghasilan + $kalkulasi->tunjangan_umum + $kalkulasi->tunjangan_struktural + $kalkulasi->tunjangan_fungsional;
			$max_bpjs = 12000000 - $bpjs_gaji;

			$beban_kerja = 0;
			$pph_beban_kerja = 0;
			$potongan = 0;

			$beban_kerja_khusus = 0;
			$pph_beban_kerja_khusus = 0;

			$tempat = 0;
			$pph_tempat = 0;

			$kondisi = 0;
			$pph_kondisi = 0;

			$plt = 0;
			$pph_plt = 0;

			// $tjKonker = 0;
			// $pphBlnAll_KK = 0;
			// $tjJakarta = 0;
			// $pphBlnAll_TB = 0;
			// $pph_konker = 0;
			// $pph_jakarta = 0;
			// $bpjs_konker = 0;
			// $bpjs_jakarta = 0;
			// $askes_konker = 0;
			// $askes_jakarta = 0;
			// $konker_kotor = 0;
			// $jakarta_kotor = 0;

			$jabatan = $insentip->jabatan;
			$eselon_id = $insentip->eselon_id;
			$golongan_id = $insentip->golongan_id;
			// $golongan_string = $insentip->golongan_string;
			$golongan_string = 0;

			$this->db->where('nip', $pegawai->nip);
			$this->db->where('bulan', intval($xperiode[1]));
			$this->db->where('tahun', intval($xperiode[0]));
			$masttppk = $this->db->get($this->tb_pegawai_tpp_kinerja)->row();

			// $beban_kerja_kotor = (($insentip->beban_kerja * ((($masttppk->skp / 100) * (60 / 100)) + ((40 / 100) - ($masttppk->perilaku / 100)))) * ($masttppk->hukdis / 100)) -
			// 				((($masttppk->lhkpn / 100) + ($masttppk->lhkasn / 100) + ($masttppk->bmd / 100) + ($masttppk->grat / 100) + ($masttppk->tptgr / 100)));
			$beban_kerja_kotor =$insentip->beban_kerja;
			if ($beban_kerja_kotor <= $max_bpjs) {
				$potongan_beban_kerja_bpjs = ceil(0.01 * $beban_kerja_kotor);
				$potongan_beban_kerja_askes = ceil(0.04 * $beban_kerja_kotor);
			} else {
				$potongan_beban_kerja_bpjs = ceil(0.01 * $max_bpjs);
				$potongan_beban_kerja_askes = ceil(0.04 * $max_bpjs);
			}

			// $beban_kerja_khusus_kotor = (($insentip->baban_kerja_khusus * ((($masttppk->skp / 100) * (60 / 100)) + ((40 / 100) - ($masttppk->perilaku / 100)))) * ($masttppk->hukdis / 100)) -
			// 				((($masttppk->lhkpn / 100) + ($masttppk->lhkasn / 100) + ($masttppk->bmd / 100) + ($masttppk->grat / 100) + ($masttppk->tptgr / 100)));
			$beban_kerja_khusus_kotor =$insentip->beban_kerja_khusus;
			$max_bpjs_bkk = $max_bpjs - $beban_kerja_kotor;
			if($max_bpjs_bkk <= 0){
				$potongan_beban_kerja_khusus_bpjs = 0;
				$potongan_beban_kerja_khusus_askes = 0;

			}else{

				if ($beban_kerja_khusus_kotor <= $max_bpjs_bkk) {
					$potongan_beban_kerja_khusus_bpjs = ceil(0.01 * $beban_kerja_khusus_kotor);
					$potongan_beban_kerja_khusus_askes = ceil(0.04 * $beban_kerja_khusus_kotor);
				} else {
					$potongan_beban_kerja_khusus_bpjs = ceil(0.01 * $max_bpjs_bkk);
					$potongan_beban_kerja_khusus_askes = ceil(0.04 * $max_bpjs_bkk);
				}

			}
			

			// $tempat_kotor = (($insentip->tempat * ((($masttppk->skp / 100) * (60 / 100)) + ((40 / 100) - ($masttppk->perilaku / 100)))) * ($masttppk->hukdis / 100)) -
			// 				((($masttppk->lhkpn / 100) + ($masttppk->lhkasn / 100) + ($masttppk->bmd / 100) + ($masttppk->grat / 100) + ($masttppk->tptgr / 100)));
			$tempat_kotor =$insentip->tempat;
			$max_bpjs_tempat = $max_bpjs - $beban_kerja_kotor;
			if($max_bpjs_tempat <= 0){
				$potongan_tempat_bpjs = 0;
				$potongan_tempat_askes = 0;

			}else{
				if ($tempat_kotor <= $max_bpjs_tempat) {
					$potongan_tempat_bpjs = ceil(0.01 * $tempat_kotor);
					$potongan_tempat_askes = ceil(0.04 * $tempat_kotor);
				} else {
					$potongan_tempat_bpjs = ceil(0.01 * $max_bpjs_tempat);
					$potongan_tempat_askes = ceil(0.04 * $max_bpjs_tempat);
				}
			}

			// $kondisi_kotor = (($insentip->kondisi * ((($masttppk->skp / 100) * (60 / 100)) + ((40 / 100) - ($masttppk->perilaku / 100)))) * ($masttppk->hukdis / 100)) -
			// 				((($masttppk->lhkpn / 100) + ($masttppk->lhkasn / 100) + ($masttppk->bmd / 100) + ($masttppk->grat / 100) + ($masttppk->tptgr / 100)));
			$kondisi_kotor =$insentip->kondisi;
			$max_bpjs_kondisi = $max_bpjs - $beban_kerja_kotor;
			if($max_bpjs_kondisi <= 0){
				$potongan_kondisi_bpjs = 0;
				$potongan_kondisi_askes = 0;

			}else{
				if ($kondisi_kotor <= $max_bpjs_kondisi) {
					$potongan_kondisi_bpjs = ceil(0.01 * $kondisi_kotor);
					$potongan_kondisi_askes = ceil(0.04 * $kondisi_kotor);
				} else {
					$potongan_kondisi_bpjs = ceil(0.01 * $max_bpjs_kondisi);
					$potongan_kondisi_askes = ceil(0.04 * $max_bpjs_kondisi);
				}
			}

			// $kondisi_kotor = (($insentip->kondisi * ((($masttppk->skp / 100) * (60 / 100)) + ((40 / 100) - ($masttppk->perilaku / 100)))) * ($masttppk->hukdis / 100)) -
			// 				((($masttppk->lhkpn / 100) + ($masttppk->lhkasn / 100) + ($masttppk->bmd / 100) + ($masttppk->grat / 100) + ($masttppk->tptgr / 100)));
			$plt_kotor =$insentip->nominal_plt;
			$max_bpjs_plt = $max_bpjs - $beban_kerja_kotor;
			if($max_bpjs_plt <= 0){
				$potongan_plt_bpjs = 0;
				$potongan_plt_askes = 0;

			}else{
				if ($plt_kotor <= $max_bpjs_plt) {
					$potongan_plt_bpjs = ceil(0.01 * $plt_kotor);
					$potongan_plt_askes = ceil(0.04 * $plt_kotor);
				} else {
					$potongan_plt_bpjs = ceil(0.01 * $max_bpjs_plt);
					$potongan_plt_askes = ceil(0.04 * $max_bpjs_plt);
				}
			}

			$tunjangan_pph = $kalkulasi->tunjangan_pph;
			
			$beban_kerja = $insentip->beban_kerja; 
			$potongan = 0; //nanti di isi dari mast tpp ajuan simpeg
			$nominal_beban_kerja = $beban_kerja_kotor - $potongan_beban_kerja_bpjs - $potongan;
			// $nominal_beban_kerja = $beban_kerja_kotor - $potongan_beban_kerja_bpjs - $potongan - $pph_beban_kerja; // jika untuk pt3k pakai rumus ini

			$beban_kerja_khusus = $insentip->beban_kerja_khusus; 
			$nominal_beban_kerja_khusus = $beban_kerja_khusus_kotor - $potongan_beban_kerja_khusus_bpjs;

			$tempat = $insentip->tempat; 
			$nominal_tempat = $tempat_kotor - $potongan_tempat_bpjs;

			$kondisi = $insentip->kondisi; 
			$nominal_kondisi = $kondisi_kotor - $potongan_kondisi_bpjs;

			$plt = $insentip->nominal_plt; 
			$nominal_plt = $plt_kotor - $potongan_plt_bpjs;
			
			// $tjKespeg = $tppkotor - $potongan_tpp_bpjs;
			// $potongan = $eselon->finsentip - $tppkotor;
			// $tunjangan_insentip = $eselon->finsentip;

			if($beban_kerja != 0){
			$pph_beban_kerja_ = $this->pph_bebankerja($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $nominal_beban_kerja, 0 ,$nip);
			$pph_beban_kerja = $pph_beban_kerja_ - $tunjangan_pph;
			}

			if($beban_kerja_khusus != 0){
			$pph_beban_kerja_khusus_ = $this->pph_bebankerja($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $nominal_beban_kerja, $nominal_beban_kerja_khusus, $nip);
			$pph_beban_kerja_khusus = $pph_beban_kerja_khusus_ - $pph_beban_kerja - $tunjangan_pph;
			}

			if($tempat != 0){
			$pph_tempat_ = $this->pph_bebankerja($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $nominal_beban_kerja, $nominal_tempat, $nip);
			$pph_tempat = $pph_tempat_ - $pph_beban_kerja - $tunjangan_pph;
			}

			if($kondisi != 0){
				$pph_kondisi_ = $this->pph_bebankerja($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $nominal_beban_kerja, $nominal_kondisi, $nip);
				$pph_kondisi = $pph_kondisi_ - $pph_beban_kerja - $tunjangan_pph;
			}

			if($plt != 0){
				$pph_plt_ = $this->pph_bebankerja($gaji_pokok, $istri, $anak, $tunjangan_struktural, $tunjangan_fungsional, $tunjangan_umum, $pembulatan, $nominal_beban_kerja, $nominal_plt, $nip);
				$pph_plt = $pph_plt_ - $pph_beban_kerja - $tunjangan_pph;
			}

			$potongan = $this->potongan_beban_kerja($nip, $xperiode[1], $xperiode[0]);

			if($insentip->tpp_stop == 1){

				$beban_kerja = 0;
				$nominal_beban_kerja = 0;
				$pph_beban_kerja = 0;
				$nominal_beban_kerja = 0;
				$potongan = 0;
				$potongan_beban_kerja_bpjs = 0;

			}

			if($insentip->beban_kerja_khusus_stop == 1){

				$beban_kerja_khusus = 0;
				$nominal_beban_kerja_khusus = 0;
				$pph_beban_kerja_khusus = 0;
				$nominal_beban_kerja_khusus = 0;
				$potongan_beban_kerja_khusus_bpjs = 0;

			}

			if($insentip->tempat_stop == 1){

				$tempat = 0;
				$nominal_tempat = 0;
				$pph_tempat = 0;
				$nominal_tempat = 0;
				$potongan_tempat_bpjs = 0;

			}

			if($insentip->kondisi_stop == 1){

				$kondisi = 0;
				$nominal_kondisi = 0;
				$pph_kondisi = 0;
				$nominal_kondisi = 0;
				$potongan_kondisi_bpjs = 0;

			}

			if($insentip->kondisi_stop == 0){

				$plt = 0;
				$nominal_plt = 0;
				$pph_plt = 0;
				$nominal_plt = 0;
				$potongan_plt_bpjs = 0;

			}

			$arrFieldVal[] = array(
				'tanggal' => $this->db->escape_str(date("Y-m-d")), 
				'periode' => $this->db->escape_str($periode),
				'lokasi'  => $insentip->lokasi,
				'lokasi_string' => get_lokasi_new($insentip->lokasi),
				'lokasi_gaji'=> $insentip->lokasi_gaji,
				'lokasi_gaji_string' => get_lokasi_id_new($insentip->lokasi_gaji),
				'name' => $this->db->escape_str($nama),
				'nip' 				=> $nip,
				'golongan_id' 		=> $insentip->golongan_id,
				'golongan_string' 	=> golongan_nama($insentip->golongan_id),
				'jabatan' => $insentip->I_05,
				'jenjang' => $insentip->I_07,
				'jabatan_string' => $insentip->I_JB,
				'kelas_jabatan' => $insentip->kelasjab,
				'gaji_pokok' => $gaji_pokok,
				'tunjangan_pph'		=> $tunjangan_pph,
				'beban_kerja_stop'		=> $insentip->tpp_stop,
				'beban_kerja'		=> $beban_kerja,
				'beban_kerja_pot' => $potongan,
				'beban_kerja_pot_bpjs' => $potongan_beban_kerja_bpjs,
				'beban_kerja_nominal' => $nominal_beban_kerja,
				'beban_kerja_pph' => $pph_beban_kerja,
				'beban_kerja_khusus_stop'		=> $insentip->beban_kerja_khusus_stop,
				'beban_kerja_khusus' => $beban_kerja_khusus,
				'beban_kerja_khusus_pot_bpjs' => $potongan_beban_kerja_khusus_bpjs,
				'beban_kerja_khusus_nominal' => $nominal_beban_kerja_khusus,
				'beban_kerja_khusus_pph' => $pph_beban_kerja_khusus,
				'tempat_stop'		=> $insentip->tempat_stop,
				'tempat' => $tempat,
				'tempat_pot_bpjs' => $potongan_tempat_bpjs,
				'tempat_nominal' => $nominal_tempat,
				'tempat_pph' => $pph_tempat,
				'kondisi_stop'		=> $insentip->kondisi_stop,
				'kondisi' => $kondisi,
				'kondisi_pot_bpjs' => $potongan_kondisi_bpjs,
				'kondisi_nominal' => $nominal_kondisi,
				'kondisi_pph' =>  $pph_kondisi,
				'lokasi_gaji_plt' => $insentip->lokasi_gaji_plt,
				'lokasi_kerja_plt' => $insentip->lokasi_kerja_plt,
				'is_plt' => $insentip->is_plt,
				'plt' => $plt,
				'plt_nominal'  => $nominal_plt,
				'plt_pph' => $pph_plt,
				'status_id' =>  $pegawai->status_id,
				'created_at' => date("Y-m-d H:i:s"),
				'created_by' => $this->session->userdata('B_02B')
			);
			
			// $this->db->insert($this->tb_kalkulasi, $arrFieldVal);
		}

		// $array_msg = array(
		// 	'status'=>'success',
		// 	'message'=>'Berhasil melakukan Kalkulasi'. $periode
		// );
		
		// $this->session->set_flashdata($array_msg);
		// redirect('kalkulasi');

		header('Content-Type: application/json; charset=utf-8');
		echo json_encode($arrFieldVal);

		
	}

	function gaji_bulanan($periode, $arrLokasi){							
		error_reporting(0);
		// Parameter
		$i			= 1;		
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		
		$this->db->select($this->tb_pegawai.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golongan.'.kode as golongan_kode');
		$this->db->select($this->tb_status.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawai);
		$this->db->join($this->tb_eselon, $this->tb_pegawai.'.eselon_id ='.$this->tb_eselon.'.id');
        $this->db->join($this->tb_kedudukan, $this->tb_pegawai.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawai.'.fungsional_id ='.$this->tb_fungsional.'.id');
		$this->db->join($this->tb_golongan, $this->tb_pegawai.'.golongan_id ='.$this->tb_golongan.'.id');
		$this->db->join($this->tb_status, $this->tb_pegawai.'.status_id ='.$this->tb_status.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawai.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawai.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
        $this->db->where_in('lokasi_gaji', $arrLokasi);
        $this->db->where_in('status_id', array(1,2,7,8,9));
        $pegawais = $this->db->get()->result();
        
		
		foreach($pegawais as $pegawai) {
			$pensiun	= 0;
			
			$xperiode = explode("-",$periode);
			$xtgllahir = explode("-",$pegawai->tanggal_lahir);			
			$dd = $xperiode[2]-$xtgllahir[2];
			$mm = ($xperiode[1]-$xtgllahir[1])*30;
			$yy = ($xperiode[0]-$xtgllahir[0])*363;
			$selisih = $dd + $mm + $yy;

			// $this->db->where('id', $pegawai->eselon_id);
			// $eselon_ = $this->db->get($this->tb_eselon)->row();

			// $this->db->where('id', $pegawai->fungsional_id);
			// $fungsional_ = $this->db->get($this->tb_fungsional)->row();

			// $this->db->where('id', $pegawai->kedudukan_id);
			// $kedudukan_ = $this->db->get($this->tb_kedudukan)->row();
			
			if($pegawai->eselon_id > 1) {
				if($pegawai->eselon_id < 8) {
					if($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;					
					}	
				}
				$jabatan = $pegawai->kedudukan_name	;	
			}
			else {
				if($pegawai->fungsional_id > 1) {
					if($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;						
					}
					$jabatan = $pegawai->fungsional_name;
				}
				else {
					if($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name	;	
				}
			}
			
			if($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;
				
				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;
				
				if($pegawai->tunjangan_istri == 2) {	//punya istri dan tertanggung				
					if($pegawai->anak < 10) {   //
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "11".$jumlah_anak; // 1 PNS 1 istri tertanggung digit 3 dan 4 jml anak
					$jumlah_istri = 1;					
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				}
				else {
					if($pegawai->anak < 10) {  // digit kedua istri/suami 
						$jumlah_anak = "0".$pegawai->anak;
					}
					
					$jiwa_string = "10".$jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}
				
				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				
				// Kolom 5
				$tunjangan_umum = 0;
				if($pegawai->tunjangan_umum == 2) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				$tunjangan_umum_tambahan = 0;
				
				$tunjangan_fungsional = 0;
				if($pegawai->fungsional_id > 1) {

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

					
					//$tunjangan_fungsional = 0;
				} else {
					
					if($pegawai->kedudukan_id != 72) {

						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();

					}
				}
				
				if($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}
				
				// Tunjangan Struktural
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;
				
				if($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}
				
				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}
				
				// Kondisi Khusus
				// Radiologi
				if($pegawai->kedudukan_id >= 90 AND $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if($pegawai->kedudukan_id >= 43 AND $pegawai->kedudukan_id <= 46) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}
				
				// Ahli Sandi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) {

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumum)->row()->tunjangan;

				}

				
				// Kondisi Khusus untk Tunjangan Sandiman, Nominal Tunj Fungs dipindahkan ke Nominal $tunjangan_umum_tambahan di cetakan disebut sbg Tunj Kompensasi
				if($pegawai->fungsional_id >= 602 AND $pegawai->fungsional_id <= 609) { //Jika Ahli Sandi tingkat 1-6
					if($pegawai->kompensasi_id >= 879 AND $pegawai->kompensasi_id <= 891) { //Jika Sandiman ( tunjangan kompensasi pemula-madya) dan merupakan JFT
						//Diisi tunjangan funstionalnya sesuai nominal
						//Diisi tunjangan kompensasinya di tunj umum tambahan
						//Dinolkan tunjangan umumnya karena merupakan fungsional umum
					

						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						
						
					$tunjangan_umum = 0;
					} else { //Kalau yang ini fungsional tertentu (JFT) tapi bukan sandiman
						$tunjangan_fungsional = 0;

						$this->db->where("fungsional_id", $pegawai->kompensasi_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->kompensasi_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_umum_tambahan = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

						}
				}
				
				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}
				
				if($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}
				
				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural)); //Tunjangan BPJS 4% tidak ditampilkan di slip gaji
				
				//dafiz end
				
				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3]; 

				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();
					
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;
				
				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
					
					
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan
				
				if($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb,5,2);
					$year_tb = substr($pegawai->tmt_tb,0,4);
					
					$date_tb = mktime(0,0,0,$month_tb,0,$year_tb);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;			
					}
				}
				
				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal,5,2);
					$year_meninggal = substr($pegawai->tmt_meninggal,0,4);
					
					$date_meninggal = mktime(0,0,0,$month_meninggal,0,$year_meninggal);	
					$date_now = mktime(0,0,0,$xperiode[1],0,$xperiode[0]);
					
					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;
					
					if($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;		
					}
					else {
						continue;						
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END
				
				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
		
				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
					}
				
				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan ;
				//dafiz end
				
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				}
				else {
					$potongan_tpp = 0;
				}
				
				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if($pen_tht > 200000){
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// $jumlah_potongan = round($potongan_lain + $potongan_iwp);
				// $gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum + $tunjangan_umum_tambahan);
				// $gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				// $jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras + $tunjangan_umum_tambahan);
				// $jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
								
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);
				
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);
			
				$pembulatan	= 0;
				if(substr($gaji_bersih,-2)!="00") {
					$pembulatan	= 100 - substr($gaji_bersih,-2);
				}
				
				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan,$nip);
				
				
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan + $pph_bulan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;
				
				//echo $gaji_pokok;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasi, $arrFieldVal);

			}
			
		}

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi'. $periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');

		
	}

	function gajip3k_bulanan($periode, $arrLokasi)
	{

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		$month_search = substr($periode, 5, 2);


		if($this_period == $periode){
			$this->tb_pegawaip3k = $this->tb_pegawaip3k;
		}else{
			$this->tb_pegawaip3k = $this->tb_pegawaip3k.'_'.$year_search.'_'.$month_search;
		}

		// echo $month_search;
		// echo '<br/>';
		// echo $this_period;
		// echo '<br/>';
		// echo $periode;
		// echo '<br/>';
		// echo $this->tb_pegawaip3k;
		// die();


		if ( !$this->db->table_exists($this->tb_pegawaip3k) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}
		
	
		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','198102072010011014')
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		// $this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		$pegawaip3ks = $this->db->get()->result();
		// echo json_encode($arrLokasi);
		// echo json_encode($pegawaip3ks);
		// die();

		foreach ($pegawaip3ks as $key => $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			// $xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$xtgllahir = explode("-", $pegawai->tmt_akhir_kontrak);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			$th_akhir_kontrak = date('Y',strtotime($pegawai->tmt_akhir_kontrak));
			$bl_akhir_kontrak = date('m',strtotime($pegawai->tmt_akhir_kontrak))+1;
			$per = $year_period.'-'.$month_period;
			$per2 = $year_period.'-'.$month_period.'-01';
			$akon = $th_akhir_kontrak.'-'.$bl_akhir_kontrak;

			if($pegawai->tmt_akhir_kontrak < $per2){

				$pensi = array(
					'status_id' => 4
				);
				$this->db->where('nip', $pegawai->nip);
				$this->db->update($this->tb_pegawaip3k, $pensi);
				$pensiun = 1;

			}else{

				// $pensi = array(
				// 	'status_id' => 2
				// );
				// $this->db->where('nip', $pegawai->nip);
				// $this->db->update($this->tb_pegawaip3k, $pensi);
				$pensiun = 0;
			}
			

			// if ($pegawai->eselon_id > 1) {
			// 	if ($pegawai->eselon_id < 8) {
			// 		if ($pegawai->eselon_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 	}
			// 	$jabatan = $pegawai->kedudukan_name;
			// } else {
			// 	if ($pegawai->fungsional_id > 1) {
			// 		if ($pegawai->fungsional_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->fungsional_name;
			// 	} else {
			// 		if ($pegawai->kedudukan_usia <= $selisih) {
			// 			$pensiun = 1;
			// 		}
			// 		$jabatan = $pegawai->kedudukan_name;
			// 	}
			// }

			if ($pensiun == 0) {
				
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
						$this->db->where("fungsional_id", $pegawai->fungsional_id);
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
						// echo $tunjangan_fungsional; die();
						
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/


				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				//$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);
				$potongan_pensiun = 0;

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;

				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$pph_bulan = $this->pph($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				// echo '<br/>';
				// echo 'Gaji kotor pembulatan: '.$gaji_kotor_pembulatan;
				// echo '<br/>';
				// echo 'Jumlah Bersih: '.$jumlah_bersih;
				// echo '<br/>';
				// echo 'PPH: '.$pph_bulan;
				// echo '<br/>';
				// echo 'Jumlah: '.$jumlah_bersih_bayar;
				// die();


				//echo $gaji_pokok;
				//die();

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				/*$field = implode(",",$arrField);
				$value = implode(',',$arrValue);
				
				$sql = "INSERT INTO kalkulasip3ks (".$field.") VALUES (".$value.")";	
				$query = DB::query(Database::INSERT, $sql)->execute();*/

				//echo $month_tb."=".$year_tb."=".$month_diff;
				//die();

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);

			}
			
			// else if($pensiun == 1){

				
			// 	$pensi = array(
			// 		'status_id' => 4
			// 	);
			// 	$this->db->where('nip', $pegawai->nip);
			// 	$this->db->update($this->tb_pegawaip3k, $pensi);
			// }
		}

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}

	function gajip3k_13($periode, $arrLokasi)
	{
		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		// $month_search = substr($periode, 6, 1);
		$month_search = substr($periode, 5, 2);


		if($this_period == $periode){
			$this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13;
		}else{
			$this->tb_pegawaip3k_13 = $this->tb_pegawaip3k_13.'_'.$year_search.'_'.$month_search;
		}


		if ( !$this->db->table_exists($this->tb_pegawaip3k_13) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}

		// echo $this->tb_pegawaip3k_13;
		// die();


		
		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k_13.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k_13);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k_13.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k_13.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k_13.'.fungsional_id ='.$this->tb_fungsional.'.id', 'LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k_13.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k_13.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k_13.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k_13.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$pegawaip3ks = $this->db->get()->result();

		// $pegawaip3ks = ORM::factory('pegawaitbp3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	->find_all();

		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;

					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;

				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
						$this->db->where('golongan', $pegawai->golongan_id[0]);
						$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;


					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
						$this->db->where('golongan', $pegawai->golongan_id[0]);
						$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$askes = 0;

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// Beras
				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				$potongan_bpjs_kesehatan = 0;
				$potongan_pensiun = 0;
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;
				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if ($bea_jabatan > 500000) {
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;


				// Tanpa Beras				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				// Dengan Beras
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				$gaji_kotor_13	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				#$gaji_bersih_13 = round($gaji_kotor - $jumlah_potongan);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);
				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				// Tanpa Beras
				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				// Dengan Beras
				$pembulatan_13	= 0;
				if (substr($gaji_bersih_13, -2) != "00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;
				$pph_bulan = $this->pph_13($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);

				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;

				$pembulatan	= 0;
				if (substr($jumlah_k, -2) != "00") {
					$pembulatan = 100 - substr($jumlah_k, -2);
				}

				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k_tb, $arrFieldVal);
			}
		}

		

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasitbp3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
	

	$array_msg = array(
		'status'=>'success',
		'message'=>'Berhasil melakukan Kalkulasi 13 Periode '.$periode
	);
	
	$this->session->set_flashdata($array_msg);
	redirect('kalkulasi');

	}

	function gajip3k_14($periode, $arrLokasi)
	{
		// $this->auto_render = false;

		$month_period = getDataPeriode()->bulan;
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$year_search = substr($periode, 0, 4);
		// $month_search = substr($periode, 6, 1);
		$month_search = substr($periode, 5, 2);

		if($this_period == $periode){
			$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14;
		}else{
			$this->tb_pegawaip3k_14 = $this->tb_pegawaip3k_14.'_'.$year_search.'_'.$month_search;
		}

		if ( !$this->db->table_exists($this->tb_pegawaip3k_14) )
		{
			$array_msg = array(
				'status'=>'error',
				'message'=>'Table untuk periode '.$periode. ' tidak di temukan!'
			);
			
			$this->session->set_flashdata($array_msg);
			redirect('kalkulasi');
			die();
		}


		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";

		$this->db->select($this->tb_pegawaip3k_14.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k_14);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k_14.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k_14.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k_14.'.fungsional_id ='.$this->tb_fungsional.'.id', 'LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k_14.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k_14.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k_14.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k_14.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		$pegawaip3ks = $this->db->get()->result();

		// $pegawaip3ks = ORM::factory('pegawaiebp3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	->find_all();



		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan_name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;

					$tunjangan_fungsional = 0;
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$askes = 0;

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// Beras
				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras_ok = $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/

				$potongan_bpjs_kesehatan = 0;
				$potongan_pensiun = 0;
				$potongan_iwp = $potongan_bpjs_kesehatan + $potongan_pensiun;


				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}
				$potongan_tpp = 0;

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				#edy
				if ($bea_jabatan > 500000) {
					$bea_jabatan = 500000;
				}
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				// Biasa				
				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras_ok + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				// 13
				$potongan_lain = 0;
				$potongan_iwp = 0;
				$jumlah_potongan_13 = round($potongan_lain + $potongan_iwp);
				#edy
				#$gaji_kotor_13 = $gaji_pokok;
				#gaji ke 14 / THR th 2018 dibayarkan tj jab & tj keluarga
				$gaji_kotor_13 = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih_13 = round($gaji_kotor_13 - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				// Tanpa Beras
				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp;
					die();
				}*/

				// Dengan Beras
				$pembulatan_13	= 0;
				if (substr($gaji_bersih_13, -2) != "00") {
					$pembulatan_13 = 100 - substr($gaji_bersih_13, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				$gaji_kotor_pembulatan_13 = $gaji_kotor_13 + $pembulatan_13;

				/*if($nip == '196806141990011001') {
					echo $gaji_pokok."=".$tunjangan_struktural."=".$tunjangan_fungsional."=".$tunjangan_istri."=".$tunjangan_anak."=";
					echo $tunjangan_beras."=".$tunjangan_beras_ok."=".$gaji_kotor."=".$gaji_kotor_pembulatan."=".$gaji_bersih."=".$pembulatan."=".$potongan_iwp."=";
					echo $gaji_kotor_13;
					die();
				}*/

				$pph_bulan = $this->pph_14($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $gaji_kotor_pembulatan_13, $nip);
				#edy				
				#$jumlah_kotor = $gaji_pokok + $pph_bulan;
				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5;
				$jumlah_k = $jumlah_kotor;
				#$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan_13;
				#$jumlah_k = $jumlah_kolom_4 + $jumlah_kolom_5;
				$pembulatan	= 0;
				#if(substr($gaji_pokok,-2)!="00") {
				#	$pembulatan = 100 - substr($gaji_pokok,-2);
				#}
				if (substr($jumlah_k, -2) != "00") {
					$pembulatan = 100 - substr($jumlah_k, -2);
				}

				$jumlah_bersih = $jumlah_kotor = $jumlah_k + $pembulatan;
				$jumlah_bersih_bayar = $jumlah_kotor - $pph_bulan;

				#$tunjangan_anak = $tunjangan_istri = $tunjangan_beras = 0;
				#$tunjangan_umum = $tunjangan_fungsional = $tunjangan_struktural = 0;
				$potongan_iwp = $potongan_lain = $jumlah_potongan = 0;

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k_eb, $arrFieldVal);
			}
		}

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasiebp3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi 14 Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	
	}

	function gajip3k_desember($arrLokasi)
	{
		// $this->auto_render = false;
		$month_period = '12';
		$year_period = getDataPeriode()->tahun;

		$this_period = $year_period.'-'.$month_period.'-01';

		$this->db->where_in('lokasi_id', $arrLokasi);
		$this->db->where('periode', $this_period);
		$this->db->delete($this->tb_kalkulasip3k);

		// Parameter
		$i			= 1;
		$nPeg		= 0;
		$nIstri		= 0;
		$nAnak		= 0;
		$nJiwa	 	= 0;
		$jabatan	= "";
		$value 		= "";
		$periode 	= $this_period;

		// $pegawaip3ks = ORM::factory('pegawaip3k')
		// 	->where('lokasi_gaji', 'IN', $arrLokasi)
		// 	->where('status_id', 'IN', array(1, 2, 7, 8, 9))
		// 	//->where('nip','=','196806141990011001') // Arief Irwanto
		// 	//->where('nip','=','196708131989032005') // Widyowati
		// 	->find_all();

		$this->db->select($this->tb_pegawaip3k.'.*');
		// $this->db->select($this->tb_pegawaip3k.'.*');
		$this->db->select($this->tb_kedudukan.'.usia as kedudukan_usia,'.$this->tb_kedudukan.'.name as kedudukan_name');
		$this->db->select($this->tb_eselon.'.usia as eselon_usia,'. $this->tb_eselon.'.name as eselon_name');
		$this->db->select($this->tb_fungsional.'.usia as fungsional_usia,'. $this->tb_fungsional.'.name as fungsional_name');
		$this->db->select($this->tb_golonganp3k.'.kode as golongan_kode');
		$this->db->select($this->tb_statusp3k.'.name as status_name');
		$this->db->select($this->tb_marital.'.name as marital_name');
		$this->db->select($this->tb_lokasi.'.kode as lokasi_kode,'. $this->tb_lokasi.'.name as lokasi_name');
		$this->db->from($this->tb_pegawaip3k);
		$this->db->join($this->tb_kedudukan, $this->tb_pegawaip3k.'.kedudukan_id ='.$this->tb_kedudukan.'.id');
		$this->db->join($this->tb_eselon, $this->tb_pegawaip3k.'.eselon_id ='.$this->tb_eselon.'.id');
		$this->db->join($this->tb_fungsional, $this->tb_pegawaip3k.'.fungsional_id ='.$this->tb_fungsional.'.id','LEFT');
		$this->db->join($this->tb_golonganp3k, $this->tb_pegawaip3k.'.golongan_id ='.$this->tb_golonganp3k.'.id');
		$this->db->join($this->tb_statusp3k, $this->tb_pegawaip3k.'.status_id ='.$this->tb_statusp3k.'.id');
		$this->db->join($this->tb_marital, $this->tb_pegawaip3k.'.marital_id ='.$this->tb_marital.'.id');
		$this->db->join($this->tb_lokasi, $this->tb_pegawaip3k.'.lokasi_gaji ='.$this->tb_lokasi.'.id');
		$this->db->where_in('lokasi_gaji', $arrLokasi);
		$this->db->where_in('status_id', array(1,2,7,8,9));
		// $this->db->where($this->tb_pegawaip3k.'.nip', '197509252021211001');
		$pegawaip3ks = $this->db->get()->result();

		foreach ($pegawaip3ks as $pegawai) {
			$pensiun	= 0;

			$xperiode = explode("-", $periode);
			$xtgllahir = explode("-", $pegawai->tanggal_lahir);
			$dd = $xperiode[2] - $xtgllahir[2];
			$mm = ($xperiode[1] - $xtgllahir[1]) * 30;
			$yy = ($xperiode[0] - $xtgllahir[0]) * 363;
			$selisih = $dd + $mm + $yy;

			if ($pegawai->eselon_id > 1) {
				if ($pegawai->eselon_id < 8) {
					if ($pegawai->eselon_usia <= $selisih) {
						$pensiun = 1;
					}
				}
				$jabatan = $pegawai->kedudukan->name;
			} else {
				if ($pegawai->fungsional_id > 1) {
					if ($pegawai->fungsional_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->fungsional_name;
				} else {
					if ($pegawai->kedudukan_usia <= $selisih) {
						$pensiun = 1;
					}
					$jabatan = $pegawai->kedudukan_name;
				}
			}

			if ($pensiun == 0) {
				// KOLOM 1
				$nama = $pegawai->name;
				$tanggal_lahir = $pegawai->tanggal_lahir;
				$nip = $pegawai->nip;
				$golongan_string = $pegawai->golongan_kode;
				$status_string = $pegawai->status_name;

				// KOLOM 2
				$marital_string = $pegawai->marital_name;
				$jumlah_anak = $pegawai->anak;

				if ($pegawai->tunjangan_istri == 2) {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "11" . $jumlah_anak;
					$jumlah_istri = 1;
					$tunjangan_istri = 0.1 * $pegawai->gaji_pokok;
				} else {
					if ($pegawai->anak < 10) {
						$jumlah_anak = "0" . $pegawai->anak;
					}

					$jiwa_string = "10" . $jumlah_anak;
					$jumlah_istri = 0;
					$tunjangan_istri = 0;
				}

				// Kolom 4
				$gaji_pokok = $pegawai->gaji_pokok;
				$tunjangan_anak = floor((0.02 * $gaji_pokok) * $pegawai->anak);
				$jumlah_kolom_4	= round($gaji_pokok + $tunjangan_istri + $tunjangan_anak);

				// Kolom 5
				$tunjangan_umum = 0;
				if ($pegawai->tunjangan_umum == 2) {
					// $tunjangan_umum = ORM::factory('tjumump3k')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3k)->row()->tunjangan;
				}

				$tunjangan_umum_tambahan = 0;

				$tunjangan_fungsional = 0;
				if ($pegawai->fungsional_id > 1) {
					// $tunjangan_fungsional = ORM::factory('tjfungsional')
					// 	->where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->or_where_open()
					// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
					// 	->where('golongan', '=', $pegawai->golongan_id)
					// 	->or_where_close()
					// 	->find()
					// 	->tunjangan;
					$this->db->where("fungsional_id", $pegawai->fungsional_id);
					$this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
					$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
				} else {
					if ($pegawai->kedudukan_id != 72) {
						// $tunjangan_fungsional = ORM::factory('tjfungsional')
						// 	->where_open()
						// 	->where('kedudukan_id', '=', $pegawai->kedudukan_id)
						// 	->where('golongan', '=', $pegawai->golongan_id[0])
						// 	->where_close()
						// 	->or_where_open()
						// 	->or_where('fungsional_id', '=', $pegawai->fungsional_id)
						// 	->where('golongan', '=', $pegawai->golongan_id)
						// 	->or_where_close()
						// 	->find()
						// 	->tunjangan;
					
						$this->db->where("kedudukan_id", $pegawai->kedudukan_id);
						$this->db->where("golongan", $pegawai->golongan_id[0]);
						// $this->db->or_where("(fungsional_id='$pegawai->fungsional_id' OR golongan='$pegawai->golongan_id')");
						$this->db->or_where("(fungsional_id='$pegawai->fungsional_id'");
						$this->db->where("golongan='$pegawai->golongan_id')");
						$tunjangan_fungsional = $this->db->get($this->tb_tjfungsional)->row()->tunjangan;
					}
				}

				if ($tunjangan_fungsional == null) {
					$tunjangan_fungsional = 0;
				}

				// Tunjangan Struktural
				// $tunjangan_struktural = ORM::factory('eselon')
				// 	->where('id', '=', $pegawai->eselon_id)
				// 	->find()
				// 	->tunjangan;
				$this->db->where('id', $pegawai->eselon_id);
				$tunjangan_struktural = $this->db->get($this->tb_eselon)->row()->tunjangan;

				if ($tunjangan_struktural == null) {
					$tunjangan_struktural = 0;
				}

				if ($tunjangan_fungsional > 0) {
					$tunjangan_umum = 0;
				}

				// Kondisi Khusus
				// Radiologi
				if ($pegawai->kedudukan_id >= 90 and $pegawai->kedudukan_id <= 93) {
					$tunjangan_umum = 0;
				}
				if ($pegawai->kedudukan_id >= 43 and $pegawai->kedudukan_id <= 46) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;
					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
					$tunjangan_fungsional = 0;
				}

				// Ahli Sandi
				if ($pegawai->fungsional_id >= 602 and $pegawai->fungsional_id <= 609) {
					// $tunjangan_umum = ORM::factory('tjumum')
					// 	->where('golongan', '=', $pegawai->golongan_id[0])
					// 	->find()
					// 	->tunjangan;

					$this->db->where('golongan', $pegawai->golongan_id[0]);
					$tunjangan_umum = $this->db->get($this->tb_tjumump3ks)->row()->tunjangan;
					$tunjangan_fungsional = 0;
					
				}

				if ($tunjangan_struktural > 0) {
					$tunjangan_umum = 0;
				}

				if ($pegawai->bebas_tugas == 2) {
					$tunjangan_fungsional = 0;
					$tunjangan_struktural = 0;
				}

				//dafiz 2019-12-02
				$askes = round(0.04 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));

				//dafiz end

				$total_jiwa	= $jiwa_string[0] + $jiwa_string[1] + $jiwa_string[2] + $jiwa_string[3];

				// $beras = ORM::factory('beras')
				// 	->where('bool_id', '=', 2)
				// 	->find();
				$this->db->where('bool_id', 2);
				$beras = $this->db->get($this->tb_beras)->row();

				$tunjangan_beras 	= $beras->harga * $total_jiwa * $beras->kg;
				$tunjangan_lain		= 0;

				// Potongan
				$potongan_lain = 0;
				/*$potongan_lain = ORM::factory('potongan')
					->where('golongan','=',$pegawai->golongan_id[0])
					->find()
					->potongan;*/
				// TUGAS BELAJAR & MENINGGAL - HONO - START
				// Tb bulan ke 7 dari tmt tj jabatan (struktural,  fungsional umum, fungsional khusus) dihilangkan

				if ($pegawai->tmt_tb != "0000-00-00") {
					$month_tb = substr($pegawai->tmt_tb, 5, 2);
					$year_tb = substr($pegawai->tmt_tb, 0, 4);

					$date_tb = mktime(0, 0, 0, $month_tb, 0, $year_tb);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_tb) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 7) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					}
				}

				// tmt meninggal januari juni gaji dihentikan (tj jabatan selama jan sd mei dihentikan)
				if ($pegawai->tmt_meninggal != "0000-00-00") {
					$month_meninggal = substr($pegawai->tmt_meninggal, 5, 2);
					$year_meninggal = substr($pegawai->tmt_meninggal, 0, 4);

					$date_meninggal = mktime(0, 0, 0, $month_meninggal, 0, $year_meninggal);
					$date_now = mktime(0, 0, 0, $xperiode[1], 0, $xperiode[0]);

					$month_diff = round(($date_now - $date_meninggal) / 60 / 60 / 24 / 30) + 1;

					if ($month_diff >= 0 && $month_diff < 6) {
						$tunjangan_umum = 0;
						$tunjangan_fungsional = 0;
						$tunjangan_struktural = 0;
					} else {
						continue;
					}
				}
				// TUGAS BELAJAR & MENINGGAL - HONO - END

				//dafiz 2019-12-02
				$potongan_bpjs_kesehatan = ceil(0.01 * ($jumlah_kolom_4 + $tunjangan_umum + $tunjangan_fungsional + $tunjangan_struktural));
				$potongan_pensiun = ceil(0.08 * $jumlah_kolom_4);

				if ($pegawai->status_id == 8) {
					$potongan_pensiun = 0;
				}

				$potongan_iwp = $potongan_pensiun + $potongan_bpjs_kesehatan;
				//dafiz end

				$gaji_bruto = round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $tunjangan_fungsional + $tunjangan_umum);
				if ($gaji_bruto < 1000000) {
					$potongan_tpp = round(1000000 - $gaji_bruto);
				} else {
					$potongan_tpp = 0;
				}

				$gaji_bruto	= $gaji_bruto + $potongan_tpp;
				$bea_jabatan = 0.05 * $gaji_bruto;
				$pen_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				#edy
				if ($pen_tht > 200000) {
					$pen_tht = 200000;
				}
				$potongan_beras = 0;
				$potongan_cp = 0;

				$jumlah_potongan = round($potongan_lain + $potongan_iwp);
				$gaji_kotor	= round($jumlah_kolom_4 + $tunjangan_struktural + $tunjangan_beras + $potongan_tpp + $tunjangan_fungsional + $tunjangan_umum);
				$gaji_bersih = round($gaji_kotor - $jumlah_potongan);

				$jumlah_kolom_5	= round($tunjangan_umum + $tunjangan_struktural + $tunjangan_fungsional + $tunjangan_beras);
				$jumlah_kolom_7	= round($potongan_iwp + $potongan_lain + $potongan_beras);

				$pembulatan	= 0;
				if (substr($gaji_bersih, -2) != "00") {
					$pembulatan	= 100 - substr($gaji_bersih, -2);
				}

				$gaji_kotor_pembulatan = $gaji_kotor + $pembulatan;
				//$gaji_kotor_pembulatan = $gaji_kotor;

				// 13
				// $kalkulasitbs = ORM::factory('kalkulasitbp3k')
				// 	->where('nip', '=', $pegawai->nip)
				// 	->where('YEAR("periode")', '=', $periode)
				// 	->find();
				$this->db->where('nip', $pegawai->nip);
				$this->db->where('YEAR(periode)', $periode);
				$kalkulasitbs = $this->db->get($this->tb_kalkulasip3k_tb)->row();

				$gaji_13 = $kalkulasitbs->gaji_pokok + $kalkulasitbs->tunjangan_istri + $kalkulasitbs->tunjangan_anak + $kalkulasitbs->tunjangan_umum + $kalkulasitbs->tunjangan_fungsional + $kalkulasitbs->tunjangan_struktural + $kalkulasitbs->pembulatan;

				// 14
				// $kalkulasiebs = ORM::factory('kalkulasiebp3k')
				// 	->where('nip', '=', $pegawai->nip)
				// 	->where('YEAR("periode")', '=', $periode)
				// 	->find();
				$this->db->where('nip', $pegawai->nip);
				$this->db->where('YEAR(periode)', $periode);
				$kalkulasiebs = $this->db->get($this->tb_kalkulasip3k_eb)->row();

				$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->tunjangan_istri + $kalkulasiebs->tunjangan_anak + $kalkulasiebs->tunjangan_umum + $kalkulasiebs->tunjangan_fungsional + $kalkulasiebs->tunjangan_struktural + $kalkulasiebs->pembulatan;
				//$gaji_14 = $kalkulasiebs->gaji_pokok + $kalkulasiebs->pembulatan;

				// Gaji Jan sd Nov 
				// $sql_pph_tahun =
				// 	"SELECT SUM(tunjangan_pph) AS pph_tahun
				// FROM kalkulasip3ks WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];
				$sql_pph_tahun =
				"SELECT SUM(tunjangan_pph) AS pph_tahun
				FROM $this->tb_kalkulasip3k WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];

				// $pph_tahun = DB::query(Database::SELECT, $sql_pph_tahun)->execute()->get('pph_tahun', 0);
				$pph_tahun = $this->db->query($sql_pph_tahun)->row()->pph_tahun;

				// $sql_pph_13_tahun =
				// 	"SELECT SUM(tunjangan_pph) AS pph_13_tahun
				// FROM kalkulasitbp3ks WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];
				$sql_pph_13_tahun =
				"SELECT SUM(tunjangan_pph) AS pph_13_tahun
			    FROM $this->tb_kalkulasip3k_tb WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];

				// $pph_13_tahun = DB::query(Database::SELECT, $sql_pph_13_tahun)->execute()->get('pph_13_tahun', 0);
				$pph_13_tahun = $this->db->query($sql_pph_13_tahun)->row()->pph_13_tahun;

				// $sql_pph_14_tahun =
				// 	"SELECT SUM(tunjangan_pph) AS pph_14_tahun
				// FROM kalkulasiebp3ks WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];
				$sql_pph_14_tahun =
					"SELECT SUM(tunjangan_pph) AS pph_14_tahun
				FROM $this->tb_kalkulasip3k_eb WHERE nip = '" . $pegawai->nip . "' AND YEAR(periode) = " . $xperiode[0];

				// $pph_14_tahun = DB::query(Database::SELECT, $sql_pph_14_tahun)->execute()->get('pph_14_tahun', 0);
				$pph_14_tahun = $this->db->query($sql_pph_14_tahun)->row()->pph_14_tahun;

				$jumlah_pph = $pph_tahun + $pph_13_tahun + $pph_14_tahun;

				$pph_bulan = $this->pph_desember($jumlah_istri, $jumlah_anak, $pen_tht, $gaji_kotor_pembulatan, $nip, $gaji_13, $gaji_14, $jumlah_pph, $xperiode[0], $gaji_pokok, $tunjangan_istri, $tunjangan_anak);

				//echo $gaji_pokok."#".$tunjangan_istri."#".$tunjangan_anak;
				//echo $tunjangan_umum."#".$tunjangan_fungsional."#".$tunjangan_struktural."#".$tunjangan_beras;
				//echo $gaji_13."#".$gaji_14."<br>";

				//echo $pph_bulan."#".$pph_tahun."#".$pph_13_tahun."#".$pph_14_tahun;
				//echo $pph_bulan;


				$jumlah_kotor = $jumlah_kolom_4 + $jumlah_kolom_5 + $pembulatan;
				$jumlah_bersih = $jumlah_kotor - $jumlah_kolom_7;
				$jumlah_bersih_bayar = $jumlah_bersih - $pph_bulan;

				if ($pph_bulan < 0) {
					$pph_bulan = 0;
				}

				// $arrValue = array(
				// 	"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
				// 	"'" . mysql_real_escape_string($periode) . "'",
				// 	$pegawai->lokasi_gaji,
				// 	"'" . mysql_real_escape_string($pegawai->gaji->kode) . "'",
				// 	"'" . mysql_real_escape_string($pegawai->gaji->name) . "'",
				// 	"'" . mysql_real_escape_string($nama) . "'",
				// 	"'" . mysql_real_escape_string($tanggal_lahir) . "'",
				// 	$nip,
				// 	$pegawai->status_id,
				// 	"'" . mysql_real_escape_string($status_string) . "'",
				// 	$pegawai->golongan_id,
				// 	"'" . mysql_real_escape_string($golongan_string) . "'",
				// 	"'" . mysql_real_escape_string($jabatan) . "'",
				// 	$pegawai->marital_id,
				// 	"'" . mysql_real_escape_string($marital_string) . "'",
				// 	$jumlah_istri,
				// 	$jumlah_anak,
				// 	$total_jiwa,
				// 	"'" . mysql_real_escape_string($jiwa_string) . "'",
				// 	$gaji_pokok,
				// 	$tunjangan_istri,
				// 	$tunjangan_anak,
				// 	$tunjangan_istri + $tunjangan_anak,
				// 	$gaji_pokok + $tunjangan_istri + $tunjangan_anak,
				// 	$tunjangan_umum,
				// 	$tunjangan_umum_tambahan,
				// 	$tunjangan_struktural,
				// 	$tunjangan_fungsional,
				// 	$tunjangan_beras,
				// 	$pph_bulan,
				// 	$pembulatan,
				// 	$jumlah_kotor,
				// 	$potongan_bpjs_kesehatan,
				// 	$potongan_pensiun,
				// 	$potongan_iwp,
				// 	$potongan_lain,
				// 	$potongan_beras,
				// 	$potongan_cp,
				// 	$jumlah_potongan,
				// 	$jumlah_bersih,
				// 	$jumlah_bersih_bayar,
				// 	$askes,
				// 	"'" . mysql_real_escape_string($pegawai->kelompok_gaji) . "'"
				// );

				// $value .= "(" . implode(",", $arrValue) . "),";

				$arrFieldVal = array(
					'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
					'periode' 			=> $this->db->escape_str($periode),
					'lokasi_id' 		=> $pegawai->lokasi_gaji,
					'lokasi_kode' 		=> $this->db->escape_str($pegawai->lokasi_kode),
					'lokasi_string' 	=> $this->db->escape_str($pegawai->lokasi_name),
					'name' 				=> $this->db->escape_str($nama),
					'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
					'nip' 				=> $nip,
					'status_id' 		=> $pegawai->status_id,
					'status_string' 	=> $this->db->escape_str($status_string),
					'golongan_id' 		=> $pegawai->golongan_id,
					'golongan_string' 	=> $this->db->escape_str($golongan_string),
					'jabatan' 			=> $this->db->escape_str($jabatan),
					'marital_id' 		=> $pegawai->marital_id,
					'marital_string' 	=> $this->db->escape_str($marital_string),
					'istri' 			=> $jumlah_istri,
					'anak' 				=> $jumlah_anak,
					'jiwa' 				=> $total_jiwa,
					'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
					'gaji_pokok' 		=> $gaji_pokok,
					'tunjangan_istri' 	=> $tunjangan_istri,
					'tunjangan_anak' 	=> $tunjangan_anak,
					'jumlah_tunjangan_keluarga'=> $tunjangan_istri + $tunjangan_anak,
					'jumlah_penghasilan'=> $gaji_pokok + $tunjangan_istri + $tunjangan_anak,
					'tunjangan_umum' 	=> $tunjangan_umum,
					'tunjangan_umum_tambahan'=> $tunjangan_umum_tambahan,
					'tunjangan_struktural'=> $tunjangan_struktural,
					'tunjangan_fungsional'=> $tunjangan_fungsional,
					'tunjangan_beras' 	=> $tunjangan_beras,
					'tunjangan_pph' 	=> $pph_bulan,
					'pembulatan' 		=> $pembulatan,
					'jumlah_kotor' 		=> $jumlah_kotor,
					'potongan_bpjs_kesehatan'=> $potongan_bpjs_kesehatan,
					'potongan_pensiun' 	=> $potongan_pensiun,
					'potongan_iwp' 		=> $potongan_iwp,
					'potongan_lain' 	=> $potongan_lain,
					'potongan_beras' 	=> $potongan_beras,
					'potongan_cp' 		=> $potongan_cp,
					'jumlah_potongan' 	=> $jumlah_potongan,
					'jumlah_bersih' 	=> $jumlah_bersih,
					'jumlah_bersih_bayar'=> $jumlah_bersih_bayar,
					'askes' 			=> $askes,
					'kelompok_gaji' 	=> $this->db->escape_str($pegawai->kelompok_gaji)			
				);

				$this->db->insert($this->tb_kalkulasip3k, $arrFieldVal);
			}
		}

		//die();

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji'
		// );

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO kalkulasip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi 14 Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
		
	}

	function gajip3k_tahunan($periode, $arrLokasi)
	{
		// $kalkulasis = ORM::factory('kalkulasip3k')
		// 	->where('lokasi_id', 'IN', $arrLokasi)
		// 	->find_all();
		
		$this->db->where('YEAR(periode)', $periode);
		$this->db->where_in('lokasi_id', $arrLokasi);
		$kalkulasis = $this->db->get($this->tb_kalkulasip3k)->result();

		$arrNip = array();
		foreach ($kalkulasis as $k) {
			array_push($arrNip, $k->nip);
		}
		$arrNip = array_unique($arrNip);
		// $arrNip = array('196302162021211001');

		// echo json_encode($arrNip);

		$value = "";
		foreach ($arrNip as $key => $nip) {
			// $n = ORM::factory('kalkulasip3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->count_all();

			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$n = $this->db->get($this->tb_kalkulasip3k)->num_rows();


				
			if ($n == 0) {
				continue;
				
			}

			$gaji_pokok = 0;
			$tunjangan_istri = 0;
			$tunjangan_anak = 0;
			$jumlah_tunjangan_keluarga = 0;
			$tunjangan_umum = 0;
			$tunjangan_umum_tambahan = 0;
			$tunjangan_struktural = 0;
			$tunjangan_fungsional = 0;
			$tunjangan_beras = 0;
			$tunjangan_pph = 0;
			$pembulatan = 0;
			$jumlah_kotor = 0;
			$potongan_bpjs_kesehatan = 0;
			$potongan_pensiun = 0;
			$potongan_iwp = 0;
			$potongan_lain = 0;
			$potongan_cp = 0;
			$jumlah_potongan = 0;
			$jumlah_bersih = 0;
			$jumlah_bersih_bayar = 0;
			$askes = 0;
			$sgaji_pokok = 0;
			$ssgaji_pokok = 0;
			$stunjangan_istri = 0;
			$stunjangan_anak = 0;
			$sjumlah_tunjangan_keluarga = 0;
			$stunjangan_umum = 0;
			$stunjangan_umum_tambahan = 0;
			$stunjangan_struktural = 0;
			$stunjangan_fungsional = 0;
			$stunjangan_beras = 0;
			$stunjangan_pph = 0;
			$spembulatan = 0;
			$sjumlah_kotor = 0;
			$spotongan_bpjs_kesehatan = 0;
			$spotongan_pensiun = 0;
			$spotongan_iwp = 0;
			$spotongan_lain = 0;
			$spotongan_cp = 0;
			$sjumlah_potongan = 0;
			$sjumlah_bersih = 0;
			$sjumlah_bersih_bayar = 0;
			$saskes = 0;
			$n_bea_jabatan = 0;
			$n_tht = 0;
			$n_netto = 0;
			$n_ptkp = 0;
			$n_pkpj = 0;

			// SUM GAJI BULANAN
			$sql = "SELECT 
			lokasi_id, lokasi_kode, lokasi_string, name, tanggal_lahir, nip, 
			status_string, golongan_id, golongan_string, jabatan, marital_id,
			marital_string, istri, anak, jiwa, jiwa_string, status_id, kelompok_gaji,
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM $this->tb_kalkulasip3k		
			WHERE YEAR(periode) = ? AND nip = ?";

			// $query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			$query =  $this->db->query($sql, array($periode, $nip))->result();


			foreach ($query as $q) {
				$lokasi_id = $q->lokasi_id;
				$lokasi_kode = $q->lokasi_kode;
				$lokasi_string = $q->lokasi_string;
				$name = $q->name;
				$tanggal_lahir = $q->tanggal_lahir;
				$nip = $q->nip;
				$status_id = $q->status_id;
				$status_string = $q->status_string;
				$golongan_id = $q->golongan_id;
				$golongan_string = $q->golongan_string;
				$jabatan = $q->jabatan;
				$marital_id = $q->marital_id;
				$marital_string = $q->marital_string;
				$istri = $q->istri;
				$anak = $q->anak;
				$jiwa = $q->jiwa;
				$jiwa_string = $q->jiwa_string;
				$kelompok_gaji = $q->kelompok_gaji;

				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = 0;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$potongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$askes = $askes + $q->askes;
			}

			//echo $gaji_pokok;
			//die();

			// GAJI 13
			// $n = ORM::factory('kalkulasitbp3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->count_all();
			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$n = $this->db->get($this->tb_kalkulasip3k_tb)->num_rows();
			

			if ($n == 0) {
				$sgaji_pokok = $gaji_pokok + 0;
				$stunjangan_istri = $tunjangan_istri + 0;
				$stunjangan_anak = $tunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $tunjangan_umum + 0;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $tunjangan_struktural + 0;
				$stunjangan_fungsional = $tunjangan_fungsional + 0;
				$stunjangan_beras = $tunjangan_beras + 0;
				$stunjangan_pph = 0;
				$spembulatan = $pembulatan + 0;
				$sjumlah_kotor = $jumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $potongan_pensiun + 0;
				$spotongan_iwp = $potongan_iwp + 0;
				$spotongan_lain = $potongan_lain + 0;
				$spotongan_cp = $potongan_cp + 0;
				$sjumlah_potongan = $jumlah_potongan + 0;
				$sjumlah_bersih = $jumlah_bersih + 0;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + 0;
				$saskes = $askes + 0;
			}

			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM $this->tb_kalkulasip3k_tb		
			WHERE YEAR(periode) = ? AND nip = ?";

			// $query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			$query =  $this->db->query($sql, array($periode, $nip))->result();

			foreach ($query as $q) {
				$sgaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;
				$stunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = 0;
				$spembulatan = $pembulatan + $q->pembulatan;
				$sjumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $potongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $potongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $potongan_lain + $q->potongan_lain;
				$spotongan_cp = $potongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $askes + $q->askes;
			}

			// RAPEL GAJI
			//$n = ORM::factory('kalkulasis_rapels_final')
			//	->where('YEAR("periode")','=',$periode)
			//	->where('nip','=',$nip)
			//	->count_all();

			//if($n == 0) {
			//	continue;
			//}

			/*$sql = "SELECT 
			SUM(gaji_pokok_4) as gaji_pokok,
			SUM(tunjangan_istri_4) as tunjangan_istri,	
			SUM(tunjangan_anak_4) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga_4) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum_4) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan_4) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural_4) as tunjangan_struktural,
			SUM(tunjangan_fungsional_4) as tunjangan_fungsional,
			SUM(tunjangan_beras_4) as tunjangan_beras,
			SUM(tunjangan_pph_4) as tunjangan_pph,
			SUM(pembulatan_4) as pembulatan,
			SUM(jumlah_kotor_4) as jumlah_kotor,
			SUM(potongan_iwp_4) as potongan_iwp,
			SUM(potongan_lain_4) as potongan_lain,
			SUM(potongan_cp_4) as potongan_cp,
			SUM(jumlah_potongan_4) as jumlah_potongan,
			SUM(jumlah_bersih_4) as jumlah_bersih,
			SUM(jumlah_bersih_bayar_4) as jumlah_bersih_bayar
			FROM kalkulasis_rapels_finals		
			WHERE YEAR(periode) = ".$periode." AND nip = '".$nip."'";
			
			$query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			
			foreach($query as $q) {
				$gaji_pokok = $gaji_pokok + $q->gaji_pokok;
				$tunjangan_istri = $tunjangan_istri + $q->tunjangan_istri;	
				$tunjangan_anak = $tunjangan_anak + $q->tunjangan_anak;
				$jumlah_tunjangan_keluarga = $jumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$tunjangan_umum = $tunjangan_umum + $q->tunjangan_umum;
				$tunjangan_umum_tambahan = $tunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$tunjangan_struktural = $tunjangan_struktural + $q->tunjangan_struktural;
				$tunjangan_fungsional = $tunjangan_fungsional + $q->tunjangan_fungsional;
				$tunjangan_beras = $tunjangan_beras + $q->tunjangan_beras;
				$tunjangan_pph = $tunjangan_pph + $q->tunjangan_pph;
				$pembulatan = $pembulatan + $q->pembulatan;
				$jumlah_kotor = $jumlah_kotor + $q->jumlah_kotor;
				$potongan_iwp = $potongan_iwp + $q->potongan_iwp;
				$potongan_lain = $potongan_lain + $q->potongan_lain;
				$potongan_cp = $potongan_cp + $q->potongan_cp;
				$jumlah_potongan = $jumlah_potongan + $q->jumlah_potongan;
				$jumlah_bersih = $jumlah_bersih + $q->jumlah_bersih;
				$jumlah_bersih_bayar = $jumlah_bersih_bayar + $q->jumlah_bersih_bayar;
			}*/

			// GAJI 14
			// $n = ORM::factory('kalkulasiebp3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->count_all();
			
			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$n = $this->db->get($this->tb_kalkulasip3k_eb)->num_rows();

			if ($n == 0) {
				$sgaji_pokok = $sgaji_pokok + 0;
				$stunjangan_istri = $stunjangan_istri + 0;
				$stunjangan_anak = $stunjangan_anak + 0;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + 0;
				$stunjangan_umum = $stunjangan_umum + 0;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + 0;
				$stunjangan_struktural = $stunjangan_struktural + 0;
				$stunjangan_fungsional = $stunjangan_fungsional + 0;
				$stunjangan_beras = $stunjangan_beras + 0;
				$stunjangan_pph = $stunjangan_pph + 0;
				$spembulatan = 0;
				$sjumlah_kotor = $sjumlah_kotor + 0;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + 0;
				$spotongan_pensiun = $spotongan_pensiun + 0;
				$spotongan_iwp = $spotongan_iwp + 0;
				$spotongan_lain = $spotongan_lain + 0;
				$spotongan_cp = $spotongan_cp + 0;
				$sjumlah_potongan = $sjumlah_potongan + 0;
				$sjumlah_bersih = $sjumlah_bersih + 0;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + 0;
				$saskes = $saskes + 0;
			}

			$sql = "SELECT 
			SUM(gaji_pokok) as gaji_pokok,
			SUM(tunjangan_istri) as tunjangan_istri,	
			SUM(tunjangan_anak) as tunjangan_anak,
			SUM(jumlah_tunjangan_keluarga) as jumlah_tunjangan_keluarga,
			SUM(tunjangan_umum) as tunjangan_umum,
			SUM(tunjangan_umum_tambahan) as tunjangan_umum_tambahan,
			SUM(tunjangan_struktural) as tunjangan_struktural,
			SUM(tunjangan_fungsional) as tunjangan_fungsional,
			SUM(tunjangan_beras) as tunjangan_beras,
			SUM(tunjangan_pph) as tunjangan_pph,
			SUM(pembulatan) as pembulatan,
			SUM(jumlah_kotor) as jumlah_kotor,
			SUM(potongan_bpjs_kesehatan) as potongan_bpjs_kesehatan,
			SUM(potongan_pensiun) as potongan_pensiun,
			SUM(potongan_iwp) as potongan_iwp,
			SUM(potongan_lain) as potongan_lain,
			SUM(potongan_cp) as potongan_cp,
			SUM(jumlah_potongan) as jumlah_potongan,
			SUM(jumlah_bersih) as jumlah_bersih,
			SUM(jumlah_bersih_bayar) as jumlah_bersih_bayar,
			SUM(askes) as askes
			FROM $this->tb_kalkulasip3k_eb		
			WHERE YEAR(periode) = ? AND nip = ?";

			// $query = DB::query(Database::SELECT, $sql)->as_object()->execute();
			$query =  $this->db->query($sql, array($periode, $nip))->result();

			foreach ($query as $q) {
				$ssgaji_pokok = $sgaji_pokok + $q->gaji_pokok;
				$stunjangan_istri = $stunjangan_istri + $q->tunjangan_istri;
				$stunjangan_anak = $stunjangan_anak + $q->tunjangan_anak;
				$sjumlah_tunjangan_keluarga = $sjumlah_tunjangan_keluarga + $q->jumlah_tunjangan_keluarga;
				$stunjangan_umum = $stunjangan_umum + $q->tunjangan_umum;
				$stunjangan_umum_tambahan = $stunjangan_umum_tambahan + $q->tunjangan_umum_tambahan;
				$stunjangan_struktural = $stunjangan_struktural + $q->tunjangan_struktural;
				$stunjangan_fungsional = $stunjangan_fungsional + $q->tunjangan_fungsional;
				$stunjangan_beras = $stunjangan_beras + $q->tunjangan_beras;
				$stunjangan_pph = 0;
				$spembulatan = $spembulatan + $q->pembulatan;
				$sjumlah_kotor = $sjumlah_kotor + $q->jumlah_kotor;
				$spotongan_bpjs_kesehatan = $spotongan_bpjs_kesehatan + $q->potongan_bpjs_kesehatan;
				$spotongan_pensiun = $spotongan_pensiun + $q->potongan_pensiun;
				$spotongan_iwp = $spotongan_iwp + $q->potongan_iwp;
				$spotongan_lain = $spotongan_lain + $q->potongan_lain;
				$spotongan_cp = $spotongan_cp + $q->potongan_cp;
				$sjumlah_potongan = $sjumlah_potongan + $q->jumlah_potongan;
				$sjumlah_bersih = $sjumlah_bersih + $q->jumlah_bersih;
				$sjumlah_bersih_bayar = $sjumlah_bersih_bayar + $q->jumlah_bersih_bayar;
				$saskes = $saskes + $q->askes;
			}

			// $pertama = ORM::factory('kalkulasip3k')
			// 	->where('YEAR("periode")', '=', $periode)
			// 	->where('nip', '=', $nip)
			// 	->order_by('periode', 'DESC')
			// 	->find();
			$this->db->where('YEAR(periode)', $periode);
			$this->db->where('nip', $nip);
			$this->db->order_by('periode', 'DESC');
			$c = $this->db->get($this->tb_kalkulasip3k);

			if($c->num_rows()>0){
				$pertama = $c->row();
				$n_bea_jabatan = 0.05 * ($sjumlah_kotor - $stunjangan_pph);
				$n_tht = 0.0475 * ($gaji_pokok + $tunjangan_istri + $tunjangan_anak);
				if ($n_tht > 2400000) {
					$n_tht = 2400000;
				}
				$n_netto = $sjumlah_kotor - ($stunjangan_pph + $n_bea_jabatan + $n_tht);
				$n_ptkp = 54000000 + ((4500000 * $pertama->istri) + (4500000 * $pertama->anak));
				if (substr($nip, 14, 1) == 2) {
					$n_ptkp = 54000000;
				}

				$n_pkpj = $n_netto - $n_ptkp;
				if ($n_pkpj < 0) {
					$n_pkpj = 0;
				}
			}else{
				$n_bea_jabatan = 0;
				$n_tht = 0;
				$n_netto = 0;
				$n_ptkp = 99;
				$n_pkpj = 0;
			}

			

		// 	$arrValue = array(
		// 		"'" . mysql_real_escape_string(date("Y-m-d")) . "'",
		// 		"'" . mysql_real_escape_string($periode) . "'",
		// 		$lokasi_id,
		// 		"'" . mysql_real_escape_string($lokasi_kode) . "'",
		// 		"'" . mysql_real_escape_string($lokasi_string) . "'",
		// 		"'" . mysql_real_escape_string($name) . "'",
		// 		"'" . mysql_real_escape_string($tanggal_lahir) . "'",
		// 		$nip,
		// 		$status_id,
		// 		"'" . mysql_real_escape_string($status_string) . "'",
		// 		$golongan_id,
		// 		"'" . mysql_real_escape_string($golongan_string) . "'",
		// 		"'" . mysql_real_escape_string($pertama->jabatan) . "'",
		// 		$marital_id,
		// 		"'" . mysql_real_escape_string($marital_string) . "'",
		// 		$istri,
		// 		$anak,
		// 		$jiwa,
		// 		"'" . mysql_real_escape_string($jiwa_string) . "'",
		// 		$sgaji_pokok,
		// 		$stunjangan_istri,
		// 		$stunjangan_anak,
		// 		$stunjangan_istri + $stunjangan_anak,
		// 		$sgaji_pokok + $stunjangan_istri + $stunjangan_anak,
		// 		$stunjangan_umum,
		// 		$stunjangan_umum_tambahan,
		// 		$stunjangan_struktural,
		// 		$stunjangan_fungsional,
		// 		$stunjangan_beras,
		// 		0,
		// 		$spembulatan,
		// 		$sjumlah_kotor,
		// 		$spotongan_bpjs_kesehatan,
		// 		$spotongan_pensiun,
		// 		$spotongan_iwp,
		// 		$spotongan_lain,
		// 		$stunjangan_beras,
		// 		$spotongan_cp,
		// 		$sjumlah_potongan,
		// 		$sjumlah_bersih,
		// 		$sjumlah_bersih_bayar,
		// 		$saskes,
		// 		"'" . mysql_real_escape_string($kelompok_gaji) . "'",
		// 		$n_bea_jabatan,
		// 		$n_tht,
		// 		$n_netto,
		// 		$n_ptkp,
		// 		$n_pkpj
		// 	);

		// 	$value .= "(" . implode(",", $arrValue) . "),";
		// }

		// $arrField = array(
		// 	'tanggal',
		// 	'periode',
		// 	'lokasi_id',
		// 	'lokasi_kode',
		// 	'lokasi_string',
		// 	'name',
		// 	'tanggal_lahir',
		// 	'nip',
		// 	'status_id',
		// 	'status_string',
		// 	'golongan_id',
		// 	'golongan_string',
		// 	'jabatan',
		// 	'marital_id',
		// 	'marital_string',
		// 	'istri',
		// 	'anak',
		// 	'jiwa',
		// 	'jiwa_string',
		// 	'gaji_pokok',
		// 	'tunjangan_istri',
		// 	'tunjangan_anak',
		// 	'jumlah_tunjangan_keluarga',
		// 	'jumlah_penghasilan',
		// 	'tunjangan_umum',
		// 	'tunjangan_umum_tambahan',
		// 	'tunjangan_struktural',
		// 	'tunjangan_fungsional',
		// 	'tunjangan_beras',
		// 	'tunjangan_pph',
		// 	'pembulatan',
		// 	'jumlah_kotor',
		// 	'potongan_bpjs_kesehatan',
		// 	'potongan_pensiun',
		// 	'potongan_iwp',
		// 	'potongan_lain',
		// 	'potongan_beras',
		// 	'potongan_cp',
		// 	'jumlah_potongan',
		// 	'jumlah_bersih',
		// 	'jumlah_bersih_bayar',
		// 	'askes',
		// 	'kelompok_gaji',
		// 	'bea_jabatan',
		// 	'tht',
		// 	'penghasilan_netto',
		// 	'ptkp',
		// 	'pkpj'
		// );


		$arrFieldVal = array(
			'tanggal' 			=> $this->db->escape_str(date("Y-m-d")), 
			'periode' 			=> $this->db->escape_str($periode),
			'lokasi_id' 		=> $lokasi_id,
			'lokasi_kode' 		=> $this->db->escape_str($lokasi_kode),
			'lokasi_string' 	=> $this->db->escape_str($lokasi_string),
			'name' 				=> $this->db->escape_str($name),
			'tanggal_lahir' 	=> $this->db->escape_str($tanggal_lahir),
			'nip' 				=> $nip,
			'status_id' 		=> $status_id,
			'status_string' 	=> $this->db->escape_str($status_string),
			'golongan_id' 		=> $golongan_id,
			'golongan_string' 	=> $this->db->escape_str($golongan_string),
			'jabatan' 			=> $this->db->escape_str($jabatan),
			'marital_id' 		=> $marital_id,
			'marital_string' 	=> $this->db->escape_str($marital_string),
			'istri' 			=> $istri,
			'anak' 				=> $anak,
			'jiwa' 				=> $jiwa,
			'jiwa_string' 		=> $this->db->escape_str($jiwa_string),
			'gaji_pokok' 		=> $ssgaji_pokok, 
			'tunjangan_istri' 	=> $stunjangan_istri,
			'tunjangan_anak' 	=> $stunjangan_anak,
			'jumlah_tunjangan_keluarga'=> $stunjangan_istri + $stunjangan_anak,
			'jumlah_penghasilan'=> $sgaji_pokok + $stunjangan_istri + $stunjangan_anak,
			'tunjangan_umum' 	=> $stunjangan_umum,
			'tunjangan_umum_tambahan'=> $stunjangan_umum_tambahan,
			'tunjangan_struktural'=> $stunjangan_struktural,
			'tunjangan_fungsional'=> $stunjangan_fungsional,
			'tunjangan_beras' 	=> $stunjangan_beras,
			'tunjangan_pph' 	=> 0,
			'pembulatan' 		=> $spembulatan,
			'jumlah_kotor' 		=> $sjumlah_kotor,
			'potongan_bpjs_kesehatan'=> $spotongan_bpjs_kesehatan,
			'potongan_pensiun' 	=> $spotongan_pensiun,
			'potongan_iwp' 		=> $spotongan_iwp,
			'potongan_lain' 	=> $spotongan_lain,
			'potongan_beras' 	=> $stunjangan_beras,
			'potongan_cp' 		=> $potongan_cp,
			'jumlah_potongan' 	=> $sjumlah_potongan,
			'jumlah_bersih' 	=> $sjumlah_bersih,
			'jumlah_bersih_bayar'=> $sjumlah_bersih_bayar,
			'askes' 			=> $saskes,
			'kelompok_gaji' 	=> $this->db->escape_str($kelompok_gaji),
			'bea_jabatan'		=> $n_bea_jabatan,
			'tht'				=> $n_tht,
			'penghasilan_netto' => $n_netto,
			'ptkp'				=> $n_ptkp,
			'pkpj'				=> $n_pkpj,			
		);

		$this->db->insert($this->tb_kalkulasip3k_tahunan, $arrFieldVal);

		
		

		// $field = implode(",", $arrField);
		// $value = substr_replace($value, "", -1);

		// $sql = "INSERT INTO tahunangajip3ks (" . $field . ") VALUES " . $value;
		// $query = DB::query(Database::INSERT, $sql)->execute();
		}

		// echo json_encode($arrX);
		// die();

		

		$array_msg = array(
			'status'=>'success',
			'message'=>'Berhasil melakukan Kalkulasi Gaji Tahunan PPPK Periode '.$periode
		);
		
		$this->session->set_flashdata($array_msg);
		redirect('kalkulasi');
	}

	private function potongan_beban_kerja($nip, $bl, $th){
		$potongan = 0;
		$this->db->where('nip', $nip);
		$this->db->where('bulan', $bl);
		$this->db->where('tahun', $th);
		$q = $this->db->get($this->potongans);

		// echo json_encode($q);
		// die();
		if($q->num_rows() > 0){
			$var = $q->row();

			$pksp_default = 0;
            // $jmlinsentip = intval($basetpps[$key]) + intval($tpp_bebankerjakhusus[$key]) + intval($tpp_tempat[$key]) + intval($tpp_kondisi[$key]);
            $jmlinsentip = intval($var->nominal_bebankerja);
			$jmlinsentip_3kategori = intval($var->nominal_bebankerjakhusus) + intval($var->nominal_tempat)  + intval($var->nominal_kondisi) ;
            

			//------ Prosentase K6-K7 -------
            $dt_perilaku = (intval($var->hari)*2) + (floor($var->jam/7.5)*2);
            if ($dt_perilaku>40) $dt_perilaku=40;
			
			//------ Nominal SKP
			$dt_nominalskp = $jmlinsentip * ((60/100) * ($var->skp/100));

			//------ Nominal Perilaku
			$dt_nominalperilaku = $jmlinsentip * ((40 - $dt_perilaku - $pksp_default)/100);

			//------ Total Nominal sebelum pengurangan
			$nomtppsebelum = $dt_nominalskp + $dt_nominalperilaku;

			//------ Hukdis
			$potonganhukdis = $nomtppsebelum * ((100-$var->hukdis)/100);

			$potlhkpn = $jmlinsentip * $var->lhkpn/100;
            $potlhkasn = $jmlinsentip * $var->lhkasn/100;
            $pottptgr = $jmlinsentip * $var->tptgr/100;
            $potgrat = $jmlinsentip * $var->grat/100;
            $potbmd = $jmlinsentip * $var->bmd/100;

            $dt_nominalkurangkepatuhan = $potlhkpn + $potlhkasn + $pottptgr + $potgrat + $potbmd;

			$dt_nominalkurang = $potonganhukdis;
            $dt_nominaltpp = $nomtppsebelum + $jmlinsentip_3kategori - $dt_nominalkurang - $dt_nominalkurangkepatuhan;

			$potongan =  $dt_nominaltpp - $jmlinsentip;
		}

		return $potongan;
	}

	





}