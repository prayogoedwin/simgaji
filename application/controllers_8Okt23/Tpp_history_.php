<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_history extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Tpp_history_model');
		$this->load->model('Tpp_pegawai_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		// $this->load->view('tpp_history/history_index');
		$this->load->view('tpp_history/history_periode');

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function getData(){
        $user_id = $this->session->userdata('id');
        $periode = $this->input->post('period', TRUE);

        // $query  = 
        // "SELECT p.`id`, p.`nip`, p.`name` nama
        // FROM `simgaji_history_tpp` h
        // JOIN `simgaji_pegawais` p ON h.`pegawai_id` = p.`id`";

        // $search = array('p.nip','p.`name`', 'h.`status_ajuan`');
        // $where  = array(
        //     'h.posted_by' => $user_id,
        //     'periode' => $periode
        // );
        
        // // jika memakai IS NULL pada where sql
        // // $isWhere = null;
        // $isWhere = 'h.deleted_at IS NULL';
        // $isGroupBy = 'p.`id`';


        // header('Content-Type: application/json');
        // echo $this->Tpp_history_model->getDataHistory($query,$search,$where,$isWhere, $isGroupBy);
    }

    public function getDataPeriode(){
        $user_id = $this->session->userdata('id');

        $query  = 
        "SELECT DISTINCT(periode)
        FROM `simgaji_history_tpp`";

        $search = array('periode');
        $where  = null;
        
        // jika memakai IS NULL pada where sql
        $isWhere = null;
        $isGroupBy = null;


        header('Content-Type: application/json');
        echo $this->Tpp_history_model->getDataHistoryPeriode($query,$search,$where,$isWhere, $isGroupBy);
    }

    public function periode(){
        $data_header['session'] = $this->session->all_userdata();

        $A_01 = $this->session->userdata('A_01');
        $user_id = $this->session->userdata('id');

        $periode = $this->uri->segment(3);
        $data['detail_byperiode'] = $this->Tpp_history_model->getStatusAllDataByPeriode($periode, $user_id);

        $get = $this->Tpp_history_model->getDataHistory_($A_01, $periode, $user_id);

        $datatable = array();
		foreach ($get as $key => $value) {

            $sts_acc = '-';
            if($value->status_acc == 0){
                $sts_acc = '<span class="badge badge-warning">Menunggu</span>';
            } else if($value->status_acc == 1){
                $sts_acc = '<span class="badge badge-success">Disetujui</span>';
            } else if($value->status_acc == 9) {
                $sts_acc = '<span class="badge badge-success">Proses Data Pada BKD</span>';
            } else {
                $sts_acc = '<span class="badge badge-danger">Direvisi: '.$value->keterangan.'</span>';
            }

            $datatable[$key] = array(
                'no' => $key + 1,
				'periode' => $value->periode,
				'nip_nama_jabatan' => $value->nip . '</br>' . $value->nama . '</br>' . $value->jabatan,
                'status' => $sts_acc,
                'waktu' => $value->updated_at
			);
        }
        $data['datatable'] = $datatable;


        //data pegawai top
        $get = $this->Tpp_history_model->getDataByPostedBy($periode, $user_id);
        $get2 = $this->Tpp_history_model->getDataByCabdinBy($periode, $user_id);
        $get3 = $this->Tpp_history_model->getDataByKasubbagKeu($periode, $user_id);

        $getall = array_merge($get, $get2, $get3);

        $datatable_top = array();
		foreach ($getall as $key => $value) {

            
            if($value->updated_at == null){
                $ac = '<a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="hapusAjuan('. $value->id .')"><i class="icon-trash"></i></a>';
            } else {
                $ac = '-';
            }
            
            $klik_nip = '<a href="'. base_url('tpp_history/detail/') . encode_url($value->id) .'">'. $value->nip .'</a>';
            
			$datatable_top[$key] = array(
                'no' => $key+1,
                'nip' => $klik_nip,
                'nama' => $value->nama,
                'act' => $ac
			);
		}
        $data['datatable_top'] = $datatable_top;

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
        $this->load->view('tpp_history/history_index', $data);

        
        // echo json_encode($data);
    }

    public function detail($id){
        $real_id = decode_url($id);

        $cek = cek_realparamsPegawaiId('simgaji_history_tpp', $real_id);
        if($cek){
            $data_header['session'] = $this->session->all_userdata();

            $data['detail'] = $this->Tpp_history_model->getDetailPegawaiById($real_id);

            $get = $this->Tpp_history_model->getDataByPegawaiId($real_id);

            $datatable = array();
            foreach ($get as $key => $value) {
                
                // $sts_ajuan = '';
                // if($value->status_ajuan == 0){
                //     $sts_ajuan = 'UPT / Sekolah';
                // } else if($value->status_ajuan == 1){
                //     $sts_ajuan = 'Kasubag Keuangan';
                // } else if($value->status_ajuan == 2){
                //     $sts_ajuan = 'Sekretaris';
                // } else if($value->status_ajuan == 3){
                //     $sts_ajuan = 'Kepala SKPD';
                // }
                $act = '';

                if($value->status_acc_bkd == '0'){
                    $sts_bkd = '<span class="badge badge-warning">Menunggu</span>';
                } else if($value->status_acc_bkd == '1'){
                    $sts_bkd = '<span class="badge badge-success">Acc</span>';
                } else if($value->status_acc_bkd == '2'){
                    $sts_bkd = '<span class="badge badge-warning">Revisi</span>';
                    $act = '<div>
                        <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="revisiModal('. $value->id .')"><i class="icon-pencil"></i></a>
                        </div>';
                } else if($value->status_acc_bkd == '3'){
                    $sts_bkd = '<span class="badge badge-danger">Ditolak</span>';
                }

                $datatable[$key] = array(
                    'kode_id' => $value->kode_id,
                    'params' => $value->params,
                    // 'lama' => $value->lama,
                    'lama' => cekKode($value->kode_id, $value->lama),
                    // 'baru' => $value->baru,
                    'baru' => cekKode($value->kode_id, $value->baru),
                    'ket' => $value->description,
                    'sts_bkd' => $sts_bkd,
                    'aksi' => $act
                    // 'status' => $sts_ajuan,
                );
            }
            $data['datatable'] = $datatable;

            // $data['data_usulan'] = $this->Tpp_history_model->getDataByPegawaiIdBandingkan($real_id);

            //start data existing
            $data['kodes'] = $this->Tpp_pegawai_model->getDataKodes();

            $data['lokasi'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_lokasis');
            $data['gender'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_genders');
            $data['agama'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_agamas');
            $data['marital'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_maritals');
            $data['bool'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_bools');
            $data['status'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_status');
            $data['status'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_statuses');
            $data['golongan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_golongans');
            // $data['golonganp3k'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_golongan');
            $data['eselon'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_eselons');
            $data['pendidikan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_pendidikans');
            $data['kedudukan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_kedudukans');
            
            $data['fungsional'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_fungsionals');
            $data['profesi'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_profesis');
            //end data existing
        
            $this->load->view('template/head');
            $this->load->view('template/header', $data_header);
		    $this->load->view('tpp_history/detail_usulan_pegawai', $data);

            // header('Content-Type: application/json');
            // echo json_encode($data);
        } else {
            $array_msg = array(
                'status'=>'error',
                'message'=>'Invalid parameter'
            );
            
            $this->session->set_flashdata($array_msg);
            redirect('history');
        }
    }

    public function ajukan_action(){
        $role = $this->session->userdata('role');
        $user_id = $this->session->userdata('id');
        $id_lokasis = $this->session->userdata('id_lokasis');
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');

        if($role == 3){
            //kasubbag keu
            
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            if($A_01 != '86'){
                $B_03A = getDataMastfip($B_02B)->B_03A;
                $B_03 = getDataMastfip($B_02B)->B_03;
                $B_03B = getDataMastfip($B_02B)->B_03B;
    
                $kodeSekretaris = cari_kode_sekertaris($A_01);
                $nipSekretaris = getNipByKolok($kodeSekretaris);
                $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
                $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
                $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
                $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;
    
                //add notif
                tambahNotif('8',null, base_url('approval_usulan_tpp'), 'approval_usulan_tpp', 'Pegawai TPP', 'Usulan data pegawai TPP', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipSekretaris, '0');
    
                //START add history acc
                $data_acc = array(
                    'A_01' => $A_01,
                    // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                    // 'kode_lokasi' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                    'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                    'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                    'nip' => $nipSekretaris,
                    'nama' => $namaSekretaris,
                    'jabatan' => 'SEKRETARIS',
                    'status_acc' => 0,
                    'is_plt' => 0,
                    'created_at' => $now
                );
                $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
                //END add history acc
    
                $data = array(
                    'status_ajuan' => 2,
                    'posisi_acc' => $nipSekretaris,
                    'status_acc_bkd' => 0,
                    'updated_at' => $now
                );
            } else {
                $data = $this->ajukan_khusus_soejarwadi_action($A_01, $B_02B, $id_lokasis, $now);
            }
            
        } else if($role == 4){
            //kasubag TU Sekolah
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $nip_cabdin = $this->Tpp_history_model->getNipAtas($user_id, $periode)->posisi_acc;
            $B_03A = getDataMastfip($nip_cabdin)->B_03A;
            $B_03 = getDataMastfip($nip_cabdin)->B_03;
            $B_03B = getDataMastfip($nip_cabdin)->B_03B;
            $nama_cabdin = $B_03A . ' ' . $B_03 . ' ' . $B_03B;

            $detailCabdin = $this->Tpp_history_model->getJabatanCabdinByNip($nip_cabdin);

            // $kodeSekretaris = cari_kode_sekertaris($A_01);
            // $nipSekretaris = getNipByKolok($kodeSekretaris);
            // $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
            // $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
            // $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
            // $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;

            //add notif
            tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nip_cabdin, '0');

            //START add history acc ke verifikator
            $data_acc = array(
                'A_01' => $A_01,
                // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                // 'kode_lokasi' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                'periode' => $periode,
                'nip' => $nip_cabdin,
                'nama' => $nama_cabdin,
                // 'jabatan' => 'Cabang Dinas Pendidikan',
                'jabatan' => $detailCabdin,
                'status_acc' => 0,
                'is_plt' => 0,
                'posted_by' => $this->session->userdata('id'),
                'created_at' => $now
            );
            $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
            //END add history acc ke verifikator

            $data = array(
                'status_ajuan' => 0,
                'updated_at' => $now
            );
        } else if($role == 7){
            //cabdin
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $kodeKasubbagKeu = cari_kode_kasubbagkeu($A_01); //kode kasubbag keuangan disdik prov
            $nipDanPlt = getNipByKolokStruktural($kodeKasubbagKeu);
            $nipKasubbagKeu = $nipDanPlt['nip'];
            $is_plt = $nipDanPlt['is_plt'];
            $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
            $B_03AKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03A;
            $B_03KasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03;
            $B_03BKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03B;
            $namaKasubbagKeu = $B_03AKasubbagKeu . ' ' . $B_03KasubbagKeu . ' ' . $B_03BKasubbagKeu;

            //add notif
            tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai P3K', 'Usulan data pegawai P3K', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKasubbagKeu, '0');

            $teksJabatan = 'KASUBBAG KEUANGAN SKPD';
            if($is_plt == 1){
                $teksJabatan = 'PLT KASUBBAG KEUANGAN SKPD';
            }

            //START add history acc
            $data_acc = array(
                'A_01' => $A_01,
                // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                // 'kode_lokasi' => '34', //34 2 digit kode induk DIsdik Prov
                'kode_lokasis' => '34', //34 2 digit kode induk DIsdik Prov
                'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                'nip' => $nipKasubbagKeu,
                'nama' => $namaKasubbagKeu,
                'jabatan' => 'SEKRETARIS',
                'status_acc' => 0,
                'is_plt' => $teksJabatan,
                'unit_kerja_plt' => $unit_kerja_plt,
                'created_at' => $now
            );
            $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
            //END add history acc

            $data = array(
                'status_ajuan' => 0,
                // 'posisi_acc' => $namaKasubbagKeu,
                'posisi_acc' => $nipKasubbagKeu,
                'status_acc_bkd' => 0,
                'updated_at' => $now
            );
        }

        $upd = $this->Tpp_history_model->updateDataUsulanByPeriode('simgaji_history_tpp', $data, $periode, $user_id);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function hapus_ajuan_action(){
        $id = $this->input->post('id');
        $now = date('Y-m-d H:i:s');

        $data = array(
            'deleted_at' => $now
        );

        $upd = $this->Fungsi_model->hapusByPegawaiId('simgaji_history_tpp', $id, $data);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }
    
    public function revisiajuan(){
        $id_history = $this->input->get('id_history');

        $detail_history = $this->Tpp_history_model->getDataById($id_history);

        $data['kodes'] = $this->Tpp_history_model->getKodesById($detail_history->kode_id);


        // $data['lokasi'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_lokasis');
        // $data['gender'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_genders');
        $data['agama'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_agamas');
        // $data['marital'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_maritals');
        // $data['bool'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_bools');
        // $data['statusp3k'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_status');
        // $data['status'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_statuses');
        // $data['golongan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_golongans');
        // $data['golonganp3k'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_golongan');
        // $data['eselon'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_eselons');
        // $data['pendidikan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_pendidikans');
        // $data['kedudukan'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_kedudukans');
        
        // $data['fungsional'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_fungsionals');
        // $data['profesi'] = $this->Tpp_pegawai_model->getDataDataMaster('simgaji_profesis');

        $data['sekmen'] = $id_history;

        $this->load->view('tpp_history/revisi_ajuan', $data);
       
        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

    public function revisi_action(){
        $id_history = $this->input->post('id_history', TRUE);
        $nilai_baru = $this->input->post('val_baru', TRUE);
        $now = date('Y-m-d H:i:s');

        $data = array(
            'baru' => $nilai_baru,
            'description' => 'Data berhasil diusulkan',
            'status_acc_bkd' => 0,
            'updated_at' => $now
        );

        $upd = $this->Fungsi_model->edit('simgaji_history_tpp', $id_history, $data);
        if($upd){
            //KIRIM NOTIF ULANG VERIFIKATOR
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $user_id_verifikator = $this->Tpp_history_model->getDataById($id_history)->verifikator;//userid verifikator (table verifikator) data id ini
            $nip_verifikator = $this->Tpp_history_model->getDetailVerifikatorById($user_id_verifikator)->nip;

            $pegawaiId = $this->Tpp_history_model->getDataById($id_history)->pegawai_id;
            $nipYangDirevisi = $this->Tpp_history_model->getDataPegawaiP3kById($pegawaiId)->nip;
            $aplikasiurl = base_url('verifikasis/detail_periode/') . getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '/' . $nipYangDirevisi;

            $mySKPD = getMyLokasi($this->session->userdata('A_01'), getDataMastfip($B_02B)->A_02, getDataMastfip($B_02B)->A_03, getDataMastfip($B_02B)->A_04, getDataMastfip($B_02B)->A_05);

            tambahNotif('8',null, $aplikasiurl, 'verifikasis', 'Pegawai ', 'Ada Pembaruan Ajuan Ulang (' . $mySKPD . ')', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nip_verifikator, '0');

            $response = array(
                'status' => 1,
                'message' => 'Berhasil mengajukan ulang'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal mengajukan ulang'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    private function ajukan_khusus_soejarwadi_action($A_01, $B_02B, $id_lokasis, $now){
        $B_03A = getDataMastfip($B_02B)->B_03A;
        $B_03 = getDataMastfip($B_02B)->B_03;
        $B_03B = getDataMastfip($B_02B)->B_03B;

        // $kodeSekretaris = cari_kode_sekertaris($A_01);
        // $nipSekretaris = getNipByKolok($kodeSekretaris);
        // $B_03ASek = getDataMastfip($nipSekretaris)->B_03A;
        // $B_03Sek = getDataMastfip($nipSekretaris)->B_03;
        // $B_03BSek = getDataMastfip($nipSekretaris)->B_03B;
        // $namaSekretaris = $B_03ASek . ' ' . $B_03Sek . ' ' . $B_03BSek;

        $kodeKadinas = cari_kode_kadinas($A_01);
        $nipDanPlt = getNipByKolokStruktural($kodeKadinas);
        $nipKadinas = $nipDanPlt['nip'];
        $is_plt = $nipDanPlt['is_plt'];
        $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
        $B_03AKep = getDataMastfip($nipKadinas)->B_03A;
        $B_03Kep = getDataMastfip($nipKadinas)->B_03;
        $B_03BKep = getDataMastfip($nipKadinas)->B_03B;
        $namaKadinas = $B_03AKep . ' ' . $B_03Kep . ' ' . $B_03BKep;

        //add notif
        tambahNotif('8',null, base_url('approval_usulan'), 'approval_usulan', 'Pegawai ', 'Usulan data pegawai ', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKadinas, '0');

        $teksJabatan = 'KEPALA SKPD';
        if($is_plt == 1){
            $teksJabatan = 'PLT KEPALA SKPD';
        }

        //START add history acc
        $data_acc = array(
            'A_01' => $A_01,
            // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
            'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
            'nip' => $nipKadinas,
            'nama' => $namaKadinas,
            'jabatan' => $teksJabatan,
            'status_acc' => 0,
            'is_plt' => $is_plt,
            'unit_kerja_plt' => $unit_kerja_plt,
            'posted_by' => $this->session->userdata('id'),
            'created_at' => $now
        );
        $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
        //END add history acc

        $data = array(
            'status_ajuan' => 3,
            'posisi_acc' => $nipKadinas,
            'status_acc_bkd' => 0,
            'updated_at' => $now
        );

        return $data;
    }
}