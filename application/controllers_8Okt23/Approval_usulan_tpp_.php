<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_usulan_tpp extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Approval_usulan_tpp_model');
    }

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

        $get = $this->Approval_usulan_tpp_model->getDataPeriode();

        $datatable = array();
        foreach ($get as $key => $value) {
            $per = getDataPeriodeTPP()->tahun . '-' . getDataPeriodeTPP()->bulan . '-01';
            if($value->periode != $per){
                $period = $value->periode;
            } else {
                $period = '<a href="'. base_url('approval_usulan_tpp/periode/') . $value->periode .'">'. $value->periode .'</a>';
            }

            $datatable[$key] = array(
                'periode' => $period
            );
        }
        $data['datatable'] = $datatable;

		$this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('approval_usulan_tpp/approval_usulan_tpp_index', $data);

		// echo json_encode($this->session->all_userdata());
	}

    public function periode(){
        $data_header['session'] = $this->session->all_userdata();

        $A_01 = $this->session->userdata('A_01');
        $B_02B = $this->session->userdata('B_02B');
        $user_id = $this->session->userdata('id');

        $periode = $this->uri->segment(3);
        $data['detail_byperiode'] = $this->Approval_usulan_tpp_model->getStatusAllDataByPeriode($periode, $B_02B);

        $A_02 = getDataMastfip($B_02B)->A_02;
        $A_03 = getDataMastfip($B_02B)->A_03;
        $A_04 = getDataMastfip($B_02B)->A_04;
        $A_05 = getDataMastfip($B_02B)->A_05;
        $kolok = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;
        
        $get = $this->Approval_usulan_tpp_model->getDataHistory($kolok, $periode, $B_02B);

        $datatable = array();
		foreach ($get as $key => $value) {

            $sts_acc = '-';
            if($value->status_acc == 0){
                $sts_acc = '<span class="badge badge-warning">Menunggu</span>';
            } else if($value->status_acc == 1){
                $sts_acc = '<span class="badge badge-success">Disetujui</span>';
            } else if($value->status_acc == 9) {
                $sts_acc = '<span class="badge badge-success">Proses Data Pada BKD</span>';
            } else {
                $sts_acc = '<span class="badge badge-danger">Direvisi</span>';
            }

            $datatable[$key] = array(
                'no' => $key + 1,
				'periode' => $value->periode,
				'nip_nama_jabatan' => $value->nip . '</br>' . $value->nama . '</br>' . $value->jabatan,
                'status' => $sts_acc,
                'waktu' => $value->updated_at
			);
        }
        $data['datatable'] = $datatable;

        $role = $this->session->userdata('role');
        $getDataAtas = $this->Approval_usulan_tpp_model->getDataAtas($periode, $B_02B, $user_id, $role);

        $datatableAtas = array();
		foreach ($getDataAtas as $key => $value) {
            $datatableAtas[$key] = array(
                'no' => $key + 1,
                'nip' => $value->nip,
                'nama' => $value->nama,
                'detailYbs' => $this->detailPegawaisTpp($value->id)
			);
        }
        $data['datatableAtas'] = $datatableAtas;

        // echo json_encode($datatableAtas);

        //sekretaris, kadis
        $role = $this->session->userdata('role');
        if($role == 5 || $role == 6){
            $getKumulatif = $this->Approval_usulan_tpp_model->getForKumulatifSekKadinas($role, $periode, $user_id);

            $sum_belumdiproses = 0;
            $sum_ditolak = 0;
            $sum_disetujui = 0;
            foreach($getKumulatif as $i=>$valkum){
                $rev_tolak = $valkum->revisi + $valkum->tolak;

                $sum_belumdiproses += $valkum->belum;
                $sum_ditolak += $rev_tolak;
                $sum_disetujui += $valkum->setuju;
            }

            $data['sum_kumulatif'] = array(
                'blm' => $sum_belumdiproses,
                'tlk' => $sum_ditolak,
                'stj' => $sum_disetujui
            );
        } else {
            $data['sum_kumulatif'] = array(
                'blm' => 0,
                'tlk' => 0,
                'stj' => 0
            );
        }
        


        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
        $this->load->view('approval_usulan_tpp/approval_usulan_tpp_periode', $data);

        // echo json_encode($data);
    }

    public function ajukan_action(){
        $role = $this->session->userdata('role');
        $user_id = $this->session->userdata('id');
        $mynip = $this->session->userdata('B_02B');
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');

        $id_lokasis = $this->session->userdata('id_lokasis');
        if(strpos($id_lokasis, ',') !== false) {
            // explodable
            $id_lokasis = substr($id_lokasis, 0, 4);
        }

        if($role == 5){
            //sekretaris

            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $kodeKadinas = cari_kode_kadinas($A_01);
            $nipDanPlt = getNipByKolokStruktural($kodeKadinas);
            $nipKadinas = $nipDanPlt['nip'];
            $is_plt = $nipDanPlt['is_plt'];
            $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
            $B_03AKep = getDataMastfip($nipKadinas)->B_03A;
            $B_03Kep = getDataMastfip($nipKadinas)->B_03;
            $B_03BKep = getDataMastfip($nipKadinas)->B_03B;
            $namaKadinas = $B_03AKep . ' ' . $B_03Kep . ' ' . $B_03BKep;

            //add notif
            tambahNotif('8',null, base_url('approval_usulan_tpp'), 'approval_usulan_tpp', 'TPP Pegawai ', 'Usulan data TPP pegawai ', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKadinas, '0');

            $teksJabatan = 'KEPALA SKPD';
            if($is_plt == 1){
                $teksJabatan = 'PLT KEPALA SKPD';
            }

            //START add history acc
            $data_acc = array(
                'A_01' => $A_01,
                // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                'nip' => $nipKadinas,
                'nama' => $namaKadinas,
                'jabatan' => $teksJabatan,
                'status_acc' => 0,
                'is_plt' => $is_plt,
                'unit_kerja_plt' => $unit_kerja_plt,
                'posted_by' => $this->session->userdata('id'),
                'created_at' => $now
            );
            $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
            //END add history acc


            // START UPDATE history acc
            $upd_acc = array(
                'status_acc' => 1,
                'updated_at' => $now,
            );
            $myHistoryLastId = $this->Approval_usulan_tpp_model->getHistoryAccLastId($mynip, $periode, $A_01)->id;
            $this->Fungsi_model->edit('simgaji_history_tpp_acc', $myHistoryLastId, $upd_acc);
            // END UPDATE history acc

            $data = array(
                'status_ajuan' => 3,
                'posisi_acc' => $nipKadinas,
                'sekretaris_by' => $user_id,
                'updated_at' => $now
            );

            $hsl = true;
        } else if($role == 6){
            //kepala dinas prov
            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $myKolok = getKolokSimpegByLokasis($this->session->userdata('id_lokasis'));

            $dataMyVerifikator = getMyVerifikator($myKolok);
            if($dataMyVerifikator != null){
                // $kodeKadinas = cari_kode_kadinas($A_01);
                // $nipKadinas = getNipByKolokStruktural($kodeKadinas);
                // $B_03AKep = getDataMastfip($nipKadinas)->B_03A;
                // $B_03Kep = getDataMastfip($nipKadinas)->B_03;
                // $B_03BKep = getDataMastfip($nipKadinas)->B_03B;
                // $namaKadinas = $B_03AKep . ' ' . $B_03Kep . ' ' . $B_03BKep;

                //add notif
                tambahNotif('8',null, base_url('verifikasi_tpp'), 'verifikasi_tpp', 'TPP Pegawai ', 'Usulan data TPP pegawai ', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $dataMyVerifikator->nip, '0');

                //START add history acc
                $data_acc = array(
                    'A_01' => $A_01,
                    // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                    'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                    'periode' => getDataPeriodeTPP()->tahun .'-'. getDataPeriodeTPP()->bulan . '-01',
                    'nip' => $dataMyVerifikator->nip,
                    'nama' => $dataMyVerifikator->nama,
                    'jabatan' => 'VERIFIKATOR',
                    'status_acc' => 9,
                    'is_plt' => 0,
                    'posted_by' => $this->session->userdata('id'),
                    'created_at' => $now
                );
                $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
                //END add history acc


                // START UPDATE history acc
                $upd_acc = array(
                    'status_acc' => 1,
                    'updated_at' => $now,
                );
                $myHistoryLastId = $this->Approval_usulan_tpp_model->getHistoryAccLastId($mynip, $periode, $A_01)->id;
                $this->Fungsi_model->edit('simgaji_history_tpp_acc', $myHistoryLastId, $upd_acc);
                // END UPDATE history acc

                $data = array(
                    'status_ajuan' => 3,
                    'status_acc' => 1,
                    'posisi_acc' => $dataMyVerifikator->nip,
                    'kepala_skpd_by' => $user_id,
                    'updated_at' => $now
                );

                $hsl = true;
            } else {
                //SKPD ybs blm pny verifikator
                // $array_msg = array(
                //     'status'=>'error',
                //     'message'=>'SKPD anda belum memiliki verifikator, silahkan hubungi BKD Provinsi Jawa Tengah'
                // );

                // $this->session->set_flashdata($array_msg);
                // redirect('approval_usulan/periode/' . $periode);
                $hsl = false;
            }
        } else if ($role == 7){
            //tu cabdin

            $A_01 = $this->session->userdata('A_01');
            $B_02B = $this->session->userdata('B_02B');
            $B_03A = getDataMastfip($B_02B)->B_03A;
            $B_03 = getDataMastfip($B_02B)->B_03;
            $B_03B = getDataMastfip($B_02B)->B_03B;

            $kodeKasubbagKeu = cari_kode_kasubbagkeu($A_01); //kode kasubbag keuangan disdik prov
            $nipDanPlt = getNipByKolokStruktural($kodeKasubbagKeu);
            $nipKasubbagKeu = $nipDanPlt['nip'];
            $is_plt = $nipDanPlt['is_plt'];
            $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
            $B_03AKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03A;
            $B_03KasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03;
            $B_03BKasubbagKeu = getDataMastfip($nipKasubbagKeu)->B_03B;
            $namaKasubbagKeu = $B_03AKasubbagKeu . ' ' . $B_03KasubbagKeu . ' ' . $B_03BKasubbagKeu;

            //add notif
            tambahNotif('8',null, base_url('approval_usulan_tpp'), 'approval_usulan_tpp', 'Pegawai TPP', 'Usulan data pegawai TPP', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipKasubbagKeu, '0');

            $teksJabatan = 'KASUBBAG KEUANGAN SKPD';
            if($is_plt == 1){
                $teksJabatan = 'PLT KASUBBAG KEUANGAN SKPD';
            }

            //START add history acc
            $data_acc = array(
                'A_01' => $A_01,
                // 'kode_lokasi' => getDetailLokasisById($id_lokasis)->kode,
                // 'kode_lokasi' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
                'kode_lokasis' => '34', //34 2 digit kode induk DIsdik Prov
                'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
                'nip' => $nipKasubbagKeu,
                'nama' => $namaKasubbagKeu,
                'jabatan' => $teksJabatan,
                'status_acc' => 0,
                'is_plt' => $is_plt,
                'unit_kerja_plt' => $unit_kerja_plt,
                'posted_by' => $this->session->userdata('id'),
                'created_at' => $now
            );
            $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
            //END add history acc


            // START UPDATE history acc
            $upd_acc = array(
                'status_acc' => 1,
                'updated_at' => $now,
            );
            $this->Fungsi_model->editByNipByPeriodeByA01('simgaji_history_tpp_acc', $mynip, $periode, $A_01, $upd_acc);
            // END UPDATE history acc

            $data = array(
                'status_ajuan' => 1,
                'posisi_acc' => $nipKasubbagKeu,
                'cabdin_by' => $user_id,
                'updated_at' => $now
            );

            $hsl = true;
        }


        if($hsl){
            $upd = $this->Approval_usulan_tpp_model->updateDataUsulanByPeriode('simgaji_history_tpp', $data, $periode, $mynip);
            if($upd){

                //kepala dinas, cek jika sudah di verif BKD maka data lgsg update ke master
                if($role == 6){
                    $dataKadis = $this->Approval_usulan_tpp_model->getDataKadisForUpdate($periode, $mynip, $user_id);
                    // echo json_encode($getDataAtas);
                    // die();

                    foreach ($dataKadis as $kii => $valkadis) {
                        if($valkadis->status_acc_bkd == 1){

                            $data = array(
                                $valkadis->field_kode => $valkadis->baru,
                            );

                            $this->db->where('id', $valkadis->id);
                            $this->db->update('simgaji_pegawai_tpp', $data);
                        }
                    }
                }

                $response = array(
                    'status' => 1,
                    'message' => 'Berhasil memperbaharui data'
                );
            } else {
                $response = array(
                    'status' => 0,
                    'message' => 'Gagal memperbaharui data'
                );
            }
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Anda belum memiliki verifikator'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function ajukan_action_kasubbagkeudisdik(){
        // $role = $this->session->userdata('role');
        $user_id = $this->session->userdata('id');
        $mynip = $this->session->userdata('B_02B');
        $periode = $this->input->post('periode', TRUE);
        $now = date('Y-m-d H:i:s');

        $id_lokasis = $this->session->userdata('id_lokasis');
        if(strpos($id_lokasis, ' ') !== false) {
            // explodable
            $id_lokasis = substr($id_lokasis, 0, 4);
        }

        $A_01 = $this->session->userdata('A_01');
        $B_02B = $this->session->userdata('B_02B');
        $B_03A = getDataMastfip($B_02B)->B_03A;
        $B_03 = getDataMastfip($B_02B)->B_03;
        $B_03B = getDataMastfip($B_02B)->B_03B;

        $kodeSekDisdik = cari_kode_sekertaris($A_01);
        $nipDanPlt = getNipByKolokStruktural($kodeSekDisdik);
        $nipSekDisdik = $nipDanPlt['nip'];
        $is_plt = $nipDanPlt['is_plt'];
        $unit_kerja_plt = $nipDanPlt['unit_kerja_plt'];
        $B_03ASekDisdik = getDataMastfip($nipSekDisdik)->B_03A;
        $B_03SekDisdik = getDataMastfip($nipSekDisdik)->B_03;
        $B_03BSekDisdik = getDataMastfip($nipSekDisdik)->B_03B;
        $namaSekDisdik = $B_03ASekDisdik . ' ' . $B_03SekDisdik . ' ' . $B_03BSekDisdik;

        //add notif
        tambahNotif('8',null, base_url('approval_usulan_tpp'), 'approval_usulan_tpp', 'Pegawai TPP', 'Usulan data pegawai TPP', $B_02B, $B_03A, $B_03, $B_03B, $A_01, $nipSekDisdik, '0');

        $teksJabatan = 'SEKRETARIS';
        if($is_plt == 1){
            $teksJabatan = 'PLT SEKRETARIS';
        }

        //START add history acc
        $data_acc = array(
            'A_01' => $A_01,
            'kode_lokasis' => substr(getDetailLokasisById($id_lokasis)->kode, 0, 2),
            'periode' => getDataPeriode()->tahun .'-'. getDataPeriode()->bulan . '-01',
            'nip' => $nipSekDisdik,
            'nama' => $namaSekDisdik,
            'jabatan' => $teksJabatan,
            'status_acc' => 0,
            'is_plt' => $is_plt,
            'unit_kerja_plt' => $unit_kerja_plt,
            'posted_by' => $this->session->userdata('id'),
            'created_at' => $now
        );
        $this->Fungsi_model->tambah('simgaji_history_tpp_acc', $data_acc);
        //END add history acc


        // START UPDATE history acc
        $upd_acc = array(
            'status_acc' => 1,
            'updated_at' => $now,
        );
        $this->Fungsi_model->editByNipByPeriodeByA01('simgaji_history_tpp_acc', $mynip, $periode, $A_01, $upd_acc);
        // END UPDATE history acc

        $data = array(
            'status_ajuan' => 2,
            'posisi_acc' => $nipSekDisdik,
            'kasubbag_keu_by' => $user_id,
            'updated_at' => $now
        );

        $upd = $this->Approval_usulan_tpp_model->updateDataUsulanByPeriode('simgaji_history_tpp', $data, $periode, $mynip);
        if($upd){

            $response = array(
                'status' => 1,
                'message' => 'Berhasil memperbaharui data'
            );
        } else {
            $response = array(
                'status' => 0,
                'message' => 'Gagal memperbaharui data'
            );
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    private function detailPegawaisTpp($pegawai_id){
        // return 'pegawai id ne : ' . $pegawai_id;
        //cekKode($value->kode_id, $value->lama)
        $data = array();
        $dataHistory = $this->Approval_usulan_tpp_model->getDataHistoryByPegawaiId($pegawai_id);

        foreach ($dataHistory as $key => $value) {
            $data[$key] = array(
                'id' => $value->id,
                'periode' => $value->periode,
                'params' => $value->params,
                'lama' => cekKode($value->kode_id, $value->lama),
                'baru' => cekKode($value->kode_id, $value->baru)
            );
        }

        return $data;
    }
}