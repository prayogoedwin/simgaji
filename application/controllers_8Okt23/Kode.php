<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kode extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $lgn = $this->session->userdata();
        $this->kodes = 'simgaji_kodes';

        if (!isset($lgn['B_02B'])) {
            redirect('/', 'refresh');
        }

        $this->load->model('Fungsi_model');
    }

    public function index()
    {
        $data_header['session'] = $this->session->all_userdata();
        $data['title'] = 'Kode Mutasi';
        $data['breadcrumb'] =
            '<a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
             <span class="breadcrumb-item active">Kode Mutasi</span>';
        $this->load->view('template/head');
        $this->load->view('template/header', $data_header);
        $this->load->view('master/kode/index', $data);
    }

    public function get_kode()
    {
        if (isset($_POST["action"])) {
            if ($_POST["action"] == 'fetch_data') {
                $query = "
                SELECT * FROM simgaji_kodes 
                ORDER BY sort ASC
                ";
                $data = $this->db->query($query)->result();
                echo json_encode($data);
            }

            // if($_POST['action'] == 'update'){
            //     $test_name = $_POST["page_id_array"];
            //     $count = count($test_name); 
            //     for ($i = 0; $i < $count; $i++) { 
            //         $data = array('id' => $count,'sort' => $i);
            //         // $this->db->update('simgaji_kodes', $data); 
            //         $this->db->update_batch('simgaji_kodes',$data, 'id'); 
            //     }

            // }

            if ($_POST['action'] == 'update') {
                for ($count = 0; $count < count($_POST["page_id_array"]); $count++) {
                    // $query = "
                    // UPDATE simgaji_kodes 
                    // SET sort = '" . ($count + 1) . "' 
                    // WHERE id = '" . $_POST["page_id_array"][$count] . "'
                    // ";
                    $this->db->set('sort',$count + 1);
                    $this->db->where('id', $_POST["page_id_array"][$count]);
                    $this->db->update('simgaji_kodes', $data); 
                    // // $statement = $connect->prepare($query);
                    // // $statement->execute();
                    // echo json_encode($query);
                }
            }
        }
    }

    public function kode_change(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('bool_id', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

    public function kode_change_p3k(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('bool_id_p3k', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

    public function kode_change_tpp(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('bool_id_tpp', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

    public function kode_editable(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('status', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

    public function kode_editable_p3k(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('status_p3k', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }


    public function kode_editable_tpp(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('status_tpp', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

    public function kode_editable_verifikator(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 2){
            $bool_id = 1;
        }elseif($val == 1){
            $bool_id = 2;
        }else{
            $bool_id = 0;
        }

        $this->db->set('status_verifikator', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

    public function kode_required(){

        

        $id = $this->input->post('id', TRUE);
        $val = $this->input->post('val', TRUE);

        if($val == 0){
            $bool_id = 1;
        }else{
            $bool_id = 0;
        }

        $this->db->set('status_required', $bool_id);
        $this->db->where('id', $id);
        $this->db->update('simgaji_kodes');

    }

}
