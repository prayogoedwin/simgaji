<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends CI_Controller {
    public function __construct(){
		parent::__construct();

		$lgn = $this->session->userdata();

        if(!isset($lgn['B_02B'])) {
			redirect('/', 'refresh');
		}

		$this->load->model('Fungsi_model');
		$this->load->model('Setting_model');
	}

    public function index(){
        $data_header['session'] = $this->session->all_userdata();

		$data['detail'] = $this->Setting_model->getData();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('setting/setting_index', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

	public function simpan_action(){
		$bulan_lama = $this->Setting_model->getData()->bulan;
		$bulan_gaji = $this->input->post('bulan_gaji', TRUE);
		$tahun_gaji = $this->input->post('tahun_gaji', TRUE);

		$data = array(
			'bulan' => $bulan_gaji,
			'tahun' => $tahun_gaji
		);

		$upd = $this->Setting_model->editByBulan('simgaji_periodeberkala', $bulan_lama, $data);
		if($upd){
			$array_msg = array(
				'status'	=>	'success',
				'message'	=>	'Berhasil memperbaharui data'
			);

			$this->session->set_flashdata($array_msg);
			redirect('setting');
		} else {
			$array_msg = array(
				'status'	=>	'error',
				'message'	=>	'Gagal memperbaharui data'
			);

			$this->session->set_flashdata($array_msg);
			redirect('setting');
		}
	}

	public function ganti_password(){
        $data_header['session'] = $this->session->all_userdata();

		$data['detail'] = $this->Setting_model->getData();

        $this->load->view('template/head');
		$this->load->view('template/header', $data_header);
		$this->load->view('setting/update_password', $data);

        // header('Content-Type: application/json');
        // echo json_encode($data);
    }

	public function ganti_password_action(){
		$user = $this->session->userdata('B_02B');
		$role = $this->session->userdata('role');
		$lama = $this->input->post('lama', TRUE);
		$baru = $this->input->post('baru', TRUE);
		$rebaru = $this->input->post('rebaru', TRUE);

		
		
		$pass = md5($lama);

        $sql =
        "SELECT *
        FROM `simgaji_verifikator`
        WHERE id = ?
        AND `password` = ?";

        $query_cek = $this->db->query($sql, array($user, $pass));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
			// echo json_encode($data);
			// die();
			if($baru != $rebaru){

				$array_msg = array(
					'status'	=>	'error',
					'message'	=>	'Gagal memperbaharui password'
				);
	
				$this->session->set_flashdata($array_msg);
				redirect('setting/ganti_password');

			}else{
				$data = array(
					'password' => md5($baru)
				);
				$this->db->update('simgaji_verifikator', $data);
				$array_msg = array(
					'status'	=>	'success',
					'message'	=>	'Berhasil memperbaharui password'
				);
	
				$this->session->set_flashdata($array_msg);
				redirect('setting/ganti_password');
			}
        } else {
            $array_msg = array(
				'status'	=>	'error',
				'message'	=>	'Gagal memperbaharui password'
			);

			$this->session->set_flashdata($array_msg);
			redirect('setting/ganti_password');
        }

       
	}
}