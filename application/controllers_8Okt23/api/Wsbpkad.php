<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbpkad extends CI_Controller {
    public $dbgaji = NULL;

    public function __construct(){
		parent::__construct();

        $this->load->model('Wsbpkad_model');
        $this->dbgaji = $this->load->database('gaji', TRUE);
    }

    public function action_auth(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $params = $_REQUEST;

            if(isset($_POST['username']) && isset($_POST['password'])){

                $username = $params['username'];
                $pass = $params['password'];

                if($username == 'bpkad' && $pass == 'W3z5^*kQ@J!6'){
                    $statusHeader = array(
                        'status' => 200,
                    );

                    // $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) );
                    $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($this->randomss(16)), 4) );
                    $upd_uuid = array(
                        'token' => $UUID,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Wsbpkad_model->update_token('simgaji_token', '4', $upd_uuid);

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Authentication success',
                        'auth_token' => $UUID
                    );
                } else {
                    $statusHeader = array(
                        'status' => 401,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Username & Password not found'
                    );
                }
                
            } else {
                $statusHeader = array(
                    'status' => 403,
                );

                $response = array(
                    'status' => $statusHeader['status'],
                    'message' => 'Required parameter'
                );
            }

            json_output($statusHeader['status'],$response);
        }
    }

    public function gaji_asn(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpkad_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['service_code']) && isset($_POST['page']) && isset($_POST['item_per_page'])){
                    // $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];
		            $page = $params['page'];
		            $item_per_page = $params['item_per_page'];

                    $page = filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT);
                    if(false === $page) {
                        $page = 1;
                    } else if($page < 1){
                        $page = 1;
                    }

                    if($service_code == 'pns'){
                        $data = $this->Wsbpkad_model->getGajiPns($periode, $page, $item_per_page);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsbpkad_model->getGajiP3k($periode, $page, $item_per_page);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_asn_14(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbpkad_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['service_code']) && isset($_POST['page']) && isset($_POST['item_per_page'])){
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];
                    $page = $params['page'];
		            $item_per_page = $params['item_per_page'];

                    $page = filter_input(INPUT_POST, 'page', FILTER_VALIDATE_INT);
                    if(false === $page) {
                        $page = 1;
                    } else if($page < 1){
                        $page = 1;
                    }

                    $pecahPeriode = explode('-', $periode);
                    $xtahun = $pecahPeriode[0];
                    $xbulan = $pecahPeriode[1];

                    if(substr($xbulan, 0, 1) === '0'){
                        $xbulan = substr($xbulan, -1);
                    }
                    

                    if($service_code == 'pns'){
                        // $tablePegawai = 'pegawai_'. $xtahun .'_' . $xbulan . '_'; //bawaan BPKAD
                        $tablePegawai = 'pegawai_14_'. $xtahun .'_' . $xbulan . '_'; //bawaan BPKAD
                        if(!$this->dbgaji->table_exists($tablePegawai)){
                            $resp = array(
                                'status' => 200,
                                'message' => 'Periode table pegawai not found',
                                'data' => array()
                            );
                            header('Content-Type: application/json; charset=utf-8');
                            echo json_encode($resp);
                            die();
                        }

                        $data = $this->Wsbpkad_model->getGajiPns14($periode, $page, $item_per_page);
                    } else if($service_code == 'p3k'){
                        $tablePegawai = 'pegawaip3k_14_'. $xtahun .'_' . $xbulan . '_';
                        if(!$this->dbgaji->table_exists($tablePegawai)){
                            $resp = array(
                                'status' => 200,
                                'message' => 'Periode table pegawai not found',
                                'data' => array()
                            );
                            header('Content-Type: application/json; charset=utf-8');
                            echo json_encode($resp);
                            die();
                        }

                        $data = $this->Wsbpkad_model->getGajiP3k14($periode, $page, $item_per_page);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    private function randomss($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }
}