<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_gaji extends CI_Controller {
  public function __construct(){
		parent::__construct();

		  $this->load->model('Layanan_model');
  }

  public function gaji_takehomepay(){
    $token = $this->input->post('token', TRUE);
    $nip = $this->input->post('nip', TRUE);
    $periode = $this->input->post('periode', TRUE);

    $tk = 'xCuvya3=vJ]L';

    if( (isset($_POST['token'])) && (isset($_POST['nip'])) && (isset($_POST['periode'])) ){

      if($token != $tk){
        $response = array(
          'status' => 0,
          'message' => 'Token tidak match',
          'data' => null
        );
      } else {
        $pecahPeriod = explode("-", $periode);
        $tahun = $pecahPeriod[0];
        $bulan = $pecahPeriod[1];

        $data = $this->Layanan_model->getGajiTpp($nip, $tahun, $bulan);

        if($data != null){
          $data_kalkulasis = $this->Layanan_model->getGajiKalkulasis($nip, $periode);
          // $data->take_home_pay = '123';
          $data_all = array_merge((array)$data, (array)$data_kalkulasis);

          $response = array(
            'status' => 1,
            'message' => 'Data berhasil ditemukan',
            'data' => $data_all
          );
        } else {
          $response = array(
            'status' => 0,
            'message' => 'Data tidak ditemukan',
            'data' => $data
          );
        }
      }

    } else {
      $response = array(
        'status' => 0,
        'message' => 'Pastikan semua parameter terisi',
        'data' => null
      );
    }



    header('Content-Type: application/json;');
    echo json_encode($response);
  }
}
