<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbankjtg extends CI_Controller {
    public function __construct(){
		parent::__construct();

        $this->load->model('Wsbankjtg_model');
    }

    public function action_auth(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $params = $_REQUEST;

            if(isset($_POST['username']) && isset($_POST['password'])){

                $username = $params['username'];
                $pass = $params['password'];

                if($username == 'bankjateng' && $pass == 'Mp$97R&haj%3'){
                    $statusHeader = array(
                        'status' => 200,
                    );

                    // $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex(random_bytes(16)), 4) );
                    $UUID = vsprintf( '%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($this->randomss(16)), 4) );
                    $upd_uuid = array(
                        'token' => $UUID,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->Wsbankjtg_model->update_token('simgaji_token', '1', $upd_uuid);

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Authentication success',
                        'auth_token' => $UUID
                    );
                } else {
                    $statusHeader = array(
                        'status' => 401,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Username & Password not found'
                    );
                }
                
            } else {
                $statusHeader = array(
                    'status' => 403,
                );

                $response = array(
                    'status' => $statusHeader['status'],
                    'message' => 'Required parameter'
                );
            }

            json_output($statusHeader['status'],$response);
        }
    }

    public function gaji_asn(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbankjtg_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'pns'){
                        $data = $this->Wsbankjtg_model->getGajiPnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsbankjtg_model->getGajiP3kByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_asn_13(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbankjtg_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'pns'){
                        $data = $this->Wsbankjtg_model->getGaji13PnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsbankjtg_model->getGaji13P3kByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_susulan(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbankjtg_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'cpns'){
                        $data = $this->Wsbankjtg_model->getGajiSusulanCpnsByUk($periode, $unit_kerja);
                    } else if($service_code == 'p3k'){
                        $data = $this->Wsbankjtg_model->getGajiSusulanP3kByUk($periode, $unit_kerja);
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_tpp(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbankjtg_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'pns'){
                        $d = $this->Wsbankjtg_model->getGajiTppPnsByUk($periode, $unit_kerja);
                        foreach ($d as $k => $v) {
                            $data[$k] = array(
                                'no' => $k + 1 . "",
                                'periode_tpp' => $v->periode_tpp,
                                'kode_lokasi' => $v->kode_lokasi,
                                'lokasi_gaji' => $v->lokasi_gaji,
                                'nip' => $v->nip,
                                'nama' => $v->nama,
                                'golongan' => $v->golongan,
                                'jabatan' => $v->jabatan,
                                'kelas_jabatan' => $v->kelas_jabatan,
                                'TPP_BEBAN_KERJA' => $v->TPP_BEBAN_KERJA,
                                'TPP_BEBAN_KERJA_KHUSUS' => $v->TPP_BEBAN_KERJA_KHUSUS,
                                'TPP_TEMPAT' => $v->TPP_TEMPAT,
                                'TPP_KONDISI' => $v->TPP_KONDISI,
                                'TPP_PLT' => $v->TPP_PLT,
                            );
                        }
                    } else if($service_code == 'pns13'){
                        // $data = $this->Wsbankjtg_model->getGajiTppPns13ByUk($periode, $unit_kerja);
                        $this->output->set_status_header(202);
                        $rex = array(
                            'status' => 202,
                            'message' => 'TPP PNS 13 Under maintenance'
                        );
                        header('Content-Type: application/json; charset=utf-8');
                        echo json_encode($rex);
                        die();
                    } else if($service_code == 'p3k'){
                        $p = $this->Wsbankjtg_model->getGajiTppP3kByUk($periode, $unit_kerja);
                        foreach ($p as $k => $v) {
                            $j = '-';
                            $kls_j = '-';

                            if($v->jabatan != null){
                                $j = $v->jabatan;
                            }

                            if($v->kelas_jabatan != null){
                                $kls_j = $v->kelas_jabatan;
                            }

                            $data[$k] = array(
                                'no' => $k + 1 . "",
                                'periode_tpp' => $v->periode_tpp,
                                'kode_lokasi' => $v->kode_lokasi,
                                'lokasi_gaji' => $v->lokasi_gaji,
                                'nip' => $v->nip,
                                'nama' => $v->nama,
                                'golongan' => $v->golongan,
                                'jabatan' => $j,
                                'kelas_jabatan' => $kls_j,
                                'TPP_BEBAN_KERJA' => $v->TPP_BEBAN_KERJA,
                                'TPP_BEBAN_KERJA_KHUSUS' => $v->TPP_BEBAN_KERJA_KHUSUS,
                                'TPP_TEMPAT' => $v->TPP_TEMPAT,
                                'TPP_KONDISI' => $v->TPP_KONDISI,
                                'TPP_PLT' => $v->TPP_PLT,
                            );
                        }
                    }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    public function gaji_asn_14(){
        $method = $_SERVER['REQUEST_METHOD'];

        if($method != 'POST'){
            json_output(400,array('status' => 400,'message' => 'Bad request'));
        } else {
            $check_auth_client = $this->Wsbankjtg_model->check_auth_client();

            if($check_auth_client == true){
                $params = $_REQUEST;
                $data = array();

                if(isset($_POST['periode']) && isset($_POST['unit_kerja']) && isset($_POST['service_code'])){
                    $unit_kerja = $params['unit_kerja'];
		            $periode = $params['periode'];
		            $service_code = $params['service_code'];

                    if($service_code == 'pns'){
                        $data = $this->Wsbankjtg_model->getGajiPns14ByUk($periode, $unit_kerja);
                    } 
                    // else if($service_code == 'p3k'){
                    //     $data = $this->Wsbankjtg_model->getGaji13P3kByUk($periode, $unit_kerja);
                    // }
                    
                    if(count($data) > 0){
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Success',
                            'data' => $data
                        );
                    } else {
                        $statusHeader = array(
                            'status' => 200,
                        );

                        $response = array(
                            'status' => $statusHeader['status'],
                            'message' => 'Data not found',
                            'data' => array()
                        );
                    }
                        
                } else {
                    $statusHeader = array(
                        'status' => 403,
                    );

                    $response = array(
                        'status' => $statusHeader['status'],
                        'message' => 'Required parameter'
                    );
                }

				json_output($statusHeader['status'],$response);
            }
        }
    }

    private function randomss($length){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $characters_length = strlen($characters);
        $output = '';
        for ($i = 0; $i < $length; $i++)
            $output .= $characters[rand(0, $characters_length - 1)];

        return $output;
    }
}