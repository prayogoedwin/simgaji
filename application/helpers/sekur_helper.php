<?php
    function encode_url($url){
        $random1 = substr(sha1(rand()), 0, 40);
        $random2 = substr(md5(rand()), 0, 20);
        $ret = base64_encode($random1.$url.$random2);

        return strtr(
            $ret,
            array(
                '+' => '.',
                '=' => '-',
                '/' => '~'
            )
        );

    }

    function decode_url($url){
        $a = base64_decode($url);
        $hitung = strlen($a);
        $x = $hitung - 60;
        $y = $x + 20;
        $c = substr($a,-$y);
        return substr($c, 0, $x);
    }

    function cek_realparams($table, $id){
		$ci = get_instance();
		$query = $ci->db->query(
				"SELECT tablenya.id
				FROM $table tablenya
				WHERE tablenya.id = ".$ci->db->escape($id)." ;");
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
	}

    function cek_realparamsPegawaiId($table, $id){
		$ci = get_instance();
		$query = $ci->db->query(
				"SELECT tablenya.pegawai_id
				FROM $table tablenya
				WHERE tablenya.pegawai_id = ".$ci->db->escape($id)." ;");
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
	}

    function ClientPost($_param){
		$url = "https://efile.bkd.jatengprov.go.id/api/getefilebykodes";
		$postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v)
        {
          $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_ENCODING, '');

        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		// 	'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXRhaWwiOnsidXNlcl9pZCI6ImphdGVuZyIsInBhc3N3b3JkIjoiIiwibmFtYSI6IkRpbmFzIFRlbmFnYSBLZXJqYSBkYW4gVHJhbnNtaWdyYXNpIFByb3ZpbnNpIEphd2EgVGVuZ2FoIiwiZW1haWwiOiJidXJzYWtlcmphamF0ZW5nQGdtYWlsLmNvbSIsImlkX2dyb3VwIjoiQURNSU5fUFJPViIsImFrdGlmIjoiMSIsInVwZGF0ZV9ieSI6IjEiLCJ1cGRhdGVfZGF0ZSI6IjIwMTktMTItMTkiLCJpZCI6IiJ9LCJrYW50b3IiOmZhbHNlLCJsb2dpbiI6dHJ1ZSwidGltZXN0YW1wIjoxNTgxNTcyNzg5fQ.jM1HYW3C90LBtWK5Tkydjb-0cY6fVMVodmaqNZYnIK4'
		// ));

		$response = curl_exec($ch);
		$err = curl_error($ch);

		curl_close($ch);

		if ($err) {
			// echo "cURL Error #:" . $err;
			return $err;
		} else {
			$hsl = json_decode($response, TRUE);
			$status = $hsl['status'];
			if($status == 1){
				// return TRUE;
				return $response;
			} else {
				//return FALSE;
				return $hsl;
			}
		}
    }

    function clientPostVerif($_param){
        $url = "https://efile.bkd.jatengprov.go.id/api/verif_efile";
		$postData = '';
        //create name value pairs seperated by &
        foreach($_param as $k => $v)
        {
            $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_ENCODING, '');
		// curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		// 	'Authorization: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkZXRhaWwiOnsidXNlcl9pZCI6ImphdGVuZyIsInBhc3N3b3JkIjoiIiwibmFtYSI6IkRpbmFzIFRlbmFnYSBLZXJqYSBkYW4gVHJhbnNtaWdyYXNpIFByb3ZpbnNpIEphd2EgVGVuZ2FoIiwiZW1haWwiOiJidXJzYWtlcmphamF0ZW5nQGdtYWlsLmNvbSIsImlkX2dyb3VwIjoiQURNSU5fUFJPViIsImFrdGlmIjoiMSIsInVwZGF0ZV9ieSI6IjEiLCJ1cGRhdGVfZGF0ZSI6IjIwMTktMTItMTkiLCJpZCI6IiJ9LCJrYW50b3IiOmZhbHNlLCJsb2dpbiI6dHJ1ZSwidGltZXN0YW1wIjoxNTgxNTcyNzg5fQ.jM1HYW3C90LBtWK5Tkydjb-0cY6fVMVodmaqNZYnIK4'
		// ));

		$response = curl_exec($ch);
		$err = curl_error($ch);

		curl_close($ch);

		if ($err) {
			// echo "cURL Error #:" . $err;
			return $err;
		} else {
			$hsl = json_decode($response, TRUE);
			$status = $hsl['status'];
			if($status == 1){
				// return TRUE;
				return $response;
			} else {
				//return FALSE;
				return $hsl;
			}
		}
    }

    function json_output($statusHeader,$response){
		$ci =& get_instance();
		$ci->output->set_content_type('application/json');
		$ci->output->set_status_header($statusHeader);
		$ci->output->set_output(json_encode($response));
	}
?>
