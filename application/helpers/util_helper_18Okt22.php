<?php
    function cekKode($kode, $value){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);


        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE id = ?";

        $query = $db->query($sql, array($kode));
        $dataName = $query->row()->name;

        if($dataName == 'Status Kawin'){
            $dt = textStatuKawin($value);
        } else if($dataName == 'Nama'){
            $dt = $value;
        } else if($dataName == 'Agama'){
            $dt = textAgama($value);
        } else if($dataName == 'Lokasi Induk'){
            $dt = textLokasi($value);
        } else if($dataName == 'Alamat'){
            $dt = $value;
        } else if($dataName == 'Tempat Lahir'){
            $dt = $value;
        } else if($dataName == 'Jumlah Anak Tertunjang'){
            $dt = $value;
        } else if($dataName == 'Tanggal Lahir'){
            $dt = $value;
        } else if($dataName == 'Jumlah Anak'){
            $dt = $value;
        } else if($dataName == 'NIP Baru'){
            $dt = $value;
        } else if($dataName == 'Status Pegawai'){
            $dt = textStatuses($value);
        } else if($dataName == 'Lokasi Gaji'){
            $dt = textLokasGaji($value);
        } else if($dataName == 'TMT Akhir Kontrak'){
            $dt = $value;
        } else if($dataName == 'Jenis Kelamin'){
            $dt = textGender($value);
        } else if($dataName == 'Gaji Pokok'){
            $dt = number_format($value);
        } else if($dataName == 'Tunjangan Istri/suami'){
          $dt = textBool($value);
        } else if($dataName == 'Status PPPK'){
          $dt = textStatusP3K($value);
        } else if($dataName == 'TMT Tugas Belajar'){
          $dt = $value;
        } else if($dataName == 'Keterangan'){
            $dt = $value;
        }

        else {
            $dt = '-';
        }

        return $dt;
    }

    function cekKodeEd($kode, $value){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE field = ?";

        $query = $db->query($sql, array($kode));
        if($query->num_rows() > 0){
            $dataName = $query->row()->name;
        }else{
            $dataName = '-';
        }
       

        if($dataName == 'Status Kawin'){
            $dt = textStatuKawin($value);
        } else if($dataName == 'Nama'){
            $dt = $value;
        } else if($dataName == 'Agama'){
            $dt = textAgama($value);
        } else if($dataName == 'Lokasi Induk'){
            $dt = textLokasi($value);
        } else if($dataName == 'Alamat'){
            $dt = $value;
        } else if($dataName == 'Tempat Lahir'){
            $dt = $value;
        } else if($dataName == 'Jumlah Anak Tertunjang'){
            $dt = $value;
        } else if($dataName == 'Tanggal Lahir'){
            $dt = $value;
        } else if($dataName == 'Jumlah Anak'){
            $dt = $value;
        } else if($dataName == 'NIP Baru'){
            $dt = $value;
        } else if($dataName == 'Status Pegawai'){
            $dt = textStatuses($value);
        } else if($dataName == 'Lokasi Gaji'){
            $dt = textLokasGaji($value);
        } else if($dataName == 'TMT Akhir Kontrak'){
            $dt = $value;
        } else if($dataName == 'Jenis Kelamin'){
            $dt = textGender($value);
        } else if($dataName == 'Gaji Pokok'){
            $dt = number_format($value);
        } else if($dataName == 'Tunjangan Istri/suami'){
          $dt = textBool($value);
        } else if($dataName == 'Status PPPK'){
          $dt = textStatusP3K($value);
        } else if($dataName == 'TMT Tugas Belajar'){
          $dt = $value;
        } else if($dataName == 'Keterangan'){
            $dt = $value;
        }

        else {
            $dt = $value;
        }

        return $dt;
    }

    function textStatusP3K($identifier){
      $ci = get_instance();

      $db = $ci->load->database('default', TRUE);

      $sql =
      "SELECT *
      FROM `simgaji_statusp3ks`
      WHERE id = ?";

      $query = $db->query($sql, array($identifier));
      return $query->row()->name;
    }

    function textBool($identifier){
      if($identifier == '2'){
        $dt = 'Ya';
      } else if($identifier == '1'){
        $dt = 'Tidak';
      }else{
        $dt = '';
      }

      return $dt;
    }

    function textLokasi($identifier){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE id = ?";

        $query = $db->query($sql, array($identifier));
        return $query->row()->name;
    }

    function textAgama($identifier){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_agamas`
        WHERE id = ?";

        $query = $db->query($sql, array($identifier));
        if($query->num_rows() > 0){
            return $query->row()->name;
        } else {
            return '-';
        }
    }

    function textStatuses($identifier){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_statuses`
        WHERE id = ?";

        $query = $db->query($sql, array($identifier));
        return $query->row()->name;
    }

    function textLokasGaji($identifier){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE id = ?";

        $query = $db->query($sql, array($identifier));
        return $query->row()->name;
    }

    function textStatuKawin($identifier){
        $teksnya = '';

        switch ($identifier) {
            case 1:
                $teksnya = 'TK';
                break;
            case 2:
                $teksnya = 'K';
                break;
            case 3:
                $teksnya = 'DD';
                break;
            case 4:
                $teksnya = 'JD';
                break;
            default:
                $teksnya = '-';
                break;
        }

        return $teksnya;
    }

    function textGender($identifier){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_genders`
        WHERE id = ?";

        $query = $db->query($sql, array($identifier));
        if($query->num_rows() > 0){
            return $query->row()->name;
        } else {
            return '-';
        }
    }

    function getMyVerifikator($kolok){
        $x = '%'.$kolok.'%';
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_verifikator`
        WHERE kode_skpd_simpeg LIKE ?";

        $query = $db->query($sql, array($x));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    function getAllMyVerifikator($kolok){
        $x = '%'.$kolok.'%';
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_verifikator`
        WHERE kode_skpd_simpeg LIKE ?";

        $query = $db->query($sql, array($x));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    function getKolokSimpegByLokasis($lokasis_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE id = ?";

        $query = $db->query($sql, array($lokasis_id));

        return $query->row()->kolok_simpeg;
    }

    function getKolokSimpegByLokasisKode($lokasis_kode){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);
        
        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kode = ?";

        $query = $db->query($sql, array($lokasis_kode));

        return $query->row()->kolok_simpeg;
    }

    function getKolokCabdin($kolok_sekolah){
        $ci = get_instance();

        $dbeps = $ci->load->database('eps', TRUE);

        $sql =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";

        $query = $dbeps->query($sql, array($kolok_sekolah));

        return $query->row()->ATASAN;
    }

    function getKolokCabdinNonAtasan($kolok_sekolah){
        $ci = get_instance();

        $dbeps = $ci->load->database('eps', TRUE);

        $sql =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";

        $query = $dbeps->query($sql, array($kolok_sekolah));

        return $query->row()->KOLOK;
    }

    function getKolokByLokasis($lokasis_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kode = ?";

        $query = $db->query($sql, array($lokasis_id));

        return $query->row()->kolok_simpeg;
    }

    function getIdLokasisByLokasis($lokasis_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kode = ?";

        $query = $db->query($sql, array($lokasis_id));

        return $query->row()->id;
    }



    function getKodeByLokasis($lokasis_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kode = ?";

        $query = $db->query($sql, array($lokasis_id));

        return $query->row()->name;
    }

    function getKolokSimpegByLokasisSimpeg($lokasis_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kolok_simpeg = ?";

        $query = $db->query($sql, array($lokasis_id));

        return $query->row()->name;
    }

    function tambahNotif($jenis_notif_id, $aplikasi_id, $aplikasi_url, $web_url, $judul, $isi, $B_02B, $B_03A, $B_03, $B_03B, $A_01, $for_nip, $is_general){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $data = array(
            'id_jenis_notif' => $jenis_notif_id,
            'id_aplikasi' => $aplikasi_id,
            'url_aplikasi' => $aplikasi_url,
            'url_web' => $web_url,
            'judul' => $judul,
            'isi' => $isi,
            'B_02B' => $B_02B,
            'B_03A' => $B_03A,
            'B_03' => $B_03,
            'B_03B' => $B_03B,
            'A_01' => $A_01,
            'for_nip' => $for_nip,
            'is_open' => 0,
            'is_general' => $is_general,
            'created_at' => date('Y-m-d H:i:s')
        );

        $insert = $db->insert('inbox2', $data);
		if ($insert){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    function countingNotif($nip){
        $ci = get_instance();
        $db = $ci->load->database('default', TRUE);

        return $db->query("SELECT count(id) as hit FROM inbox2 WHERE for_nip = $nip AND `is_open` = 0")->row()->hit;
    }

    function getMyNotif($nip){
        $ci = get_instance();
        $db = $ci->load->database('default', TRUE);

        return $db->query("SELECT * FROM inbox2 WHERE for_nip = $nip AND `is_open` = 0 LIMIT 5")->result();
    }

    function getDataPeriode(){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_periodeberkala`";

        $query = $db->query($sql);

        return $query->row();
    }

    function getDetailLokasisById($id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE id = ?";

        $query = $db->query($sql, array($id));

        return $query->row();
    }

    function getTotalDataHistoryByIdByPeriode($periode, $pegawai_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_historyp3ks`
        WHERE periode = ?
        AND pegawai_id = ?";

        $query = $db->query($sql, array($periode, $pegawai_id));

        return $query->num_rows();
    }

    function getMyLokasi($A_01, $A_02, $A_03, $A_04, $A_05){
        $ci = get_instance();

        $dbeps = $ci->load->database('eps', TRUE);

        $kolok = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;

        if($A_01 == 'D0' && $A_02 <> '00'){
            //sekolah dan cabdin
            $sql =
            "SELECT *
            FROM TABLOKB08
            WHERE KOLOK = ?";

            $query = $dbeps->query($sql, array($kolok))->row();

            $data = $query->NALOK;

        } else if($A_02 =='00'){
            //induk
            $kol_induk = $A_01 . '00000000';

            $sql =
            "SELECT *
            FROM TABLOKB08
            WHERE KOLOK = ?";

            $query = $dbeps->query($sql, array($kol_induk))->row();

            $data = $query->NALOK;
        }

        return $data;
    }

    function bulanAngkaToHuruf($bulan){
        $bln = '';
        switch ($bulan) {
            case '01':
                $bln = 'Januari';
                break;
            case '02':
                $bln = 'Februari';
                break;
            case '03':
                $bln = 'Maret';
                break;
            case '04':
                $bln = 'April';
                break;
            case '05':
                $bln = 'Mei';
                break;
            case '06':
                $bln = 'Juni';
                break;
            case '07':
                $bln = 'Juli';
                break;
            case '08':
                $bln = 'Agustus';
                break;
            case '09':
                $bln = 'September';
                break;
            case '10':
                $bln = 'Oktober';
                break;
            case '11':
                $bln = 'November';
                break;
            case '12':
                $bln = 'Desember';
                break;
            default:
                $bln = '-';
                break;
        }

        return $bln;
    }

    function textStatusDataStatusGaji($kode){
        $teksnya = '';
        if($kode == 1){
            $teksnya = '<span class="badge badge-info">Kalkulasi</span>';
        } else if($kode == 2){

            $teksnya = '<span class="badge badge-info">Buka / Revisi</span>';
        } else if($kode == 3){

            $teksnya = '<span class="badge badge-info">Fixed</span>';
        } else if($kode == 4){

            $teksnya = '<span class="badge badge-info">Cetak</span>';
        }

        return $teksnya;
    }

    function getStatusSKPDByLokasisId($lokasis_id){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE id = ?";

        $query = $db->query($sql, array($lokasis_id));

        return $query->row();
    }
?>
