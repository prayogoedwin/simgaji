<?php
    function getDataMastfip($nip){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);


        $sql =
        "SELECT *
        FROM `MASTFIP08`
        WHERE B_02B = ?";

        $query = $dbeps->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    function getDataPangkatGol($v){
        $ci = get_instance();

        $dbeps = $ci->load->database('eps', TRUE);


        $sql =
        "SELECT *
        FROM `TABPKT`
        WHERE KODE = ?";

        $query = $dbeps->query($sql, array($v));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    function getDataTablok08($kolok){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);


        $sql =
        "SELECT *
        FROM `TABLOKB08`
        WHERE KOLOK = ?";

        $query = $dbeps->query($sql, array($kolok));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    function getUnitKerja($A_01){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);


        $sql =
        "SELECT *
        FROM `TABLOKB08`
        WHERE A_01 = ?";

        $query = $dbeps->query($sql, array($A_01));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    function getIdLokasiSimgajiByA01A02($A_01, $A_02, $A_03, $A_04, $A_05){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);

        $firstA_04 = substr($A_04, 0, 1);

        if($A_02 == '00'){
            //SKPD Prov
            $lok = $A_01 . $A_02;

            $sql =
            "SELECT *
            FROM `simgaji_lokasis`
            WHERE kolok_simpeg LIKE '$lok%'
            ORDER BY id ASC
            LIMIT 1";
        } else if($A_01 == 'D0' && $A_02 != '00' && $A_04 > '30'){
            //sekolah

            // $loksekolah = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;
            $loksekolah = $A_01 . $A_02 . $A_03 . $A_04;

            $sql =
            "SELECT *
            FROM `simgaji_lokasis`
            WHERE kolok_simpeg LIKE '$loksekolah%'
            ORDER BY id ASC
            LIMIT 1";
        } else if($A_02 != '00' && $A_04 <= 30) {
            //cabdin
            $lok = $A_01 . $A_02 . $A_03 . $A_04;

            $lok_tablok = $lok . $A_05;

            $sql_tablok08 =
            "SELECT *
            FROM TABLOKB08
            WHERE KOLOK = ?";

            $query_tablok08 = $dbeps->query($sql_tablok08, array($lok_tablok));
            $row_tablok08_atasan = $query_tablok08->row()->ATASAN;


            $sql =
            "SELECT *
            FROM `simgaji_lokasis`
            WHERE kolok_simpeg = '$row_tablok08_atasan'";
        }


        // $sql =
        // "SELECT *
        // FROM `simgaji_lokasis`
        // WHERE kolok_simpeg LIKE '$lok%'
        // ORDER BY id DESC";

        $query = $db->query($sql);
        if($query->num_rows() > 0){
            $data = $query->row()->id;
        } else {
            $data = null;
        }

        return $data;
    }

    function getIdLokasisSimgajiCabdin($A_01, $A_02, $A_03, $A_04, $A_05){
        $ci = get_instance();

        $db = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);

        $lok_tablok = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;

        $sql_tablok08 =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";

        $query_tablok08 = $dbeps->query($sql_tablok08, array($lok_tablok));
        $row_tablok08_atasan = $query_tablok08->row()->ATASAN;

        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kolok_simpeg = '$row_tablok08_atasan'";

        $query = $db->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    function getNipByUserNewId($userid){
        $ci = get_instance();

        // $dbeps = NULL;
        // $dbdefault = NULL;

        // $dbdefault = $ci->load->database('default', TRUE);
        $dbeps = $ci->load->database('eps', TRUE);


        $sql =
        "SELECT *
        FROM `USER_NEW`
        WHERE id = ?";

        $query = $dbeps->query($sql, array($userid));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data->nip;
    }

    function getRealNipByToken($params){

        // $url = "http://srv.bkd.jatengprov.go.id/bkd_asigam3/api/token_mobile/check";
        $url = "http://103.9.227.162/bkd_asigam3/api/token_mobile/check";
    		$postData = '';
            //create name value pairs seperated by &
            foreach($params as $k => $v)
            {
              $postData .= $k . '='.$v.'&';
            }
            rtrim($postData, '&');


            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_HEADER, false);
            // curl_setopt($ch, CURLOPT_POST, count($postData));
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    		curl_setopt($ch, CURLOPT_ENCODING, '');

    		$response = curl_exec($ch);
    		$err = curl_error($ch);

    		curl_close($ch);

    		if ($err) {
    			// echo "cURL Error #:" . $err;
    			return $err;
    		} else {
    			$hsl = json_decode($response, TRUE);
    			$status = $hsl['status'];
    			if($status){
    				// return TRUE;
    				return $response;
    			} else {
    				// return FALSE;
    				return $response;
    			}
    		}
  	}

    function getTakeHomePay($params){

        $url = "https://dev.bkd.jatengprov.go.id/simgaji/api/get_takehomepay";
    		$postData = '';
        //create name value pairs seperated by &
        foreach($params as $k => $v)
        {
          $postData .= $k . '='.$v.'&';
        }
        rtrim($postData, '&');


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, false);
        // curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt ($ch, CURLOPT_PORT , 80);
    		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    		curl_setopt($ch, CURLOPT_ENCODING, '');

    		$response = curl_exec($ch);
    		$err = curl_error($ch);

    		curl_close($ch);

    		if ($err) {
    			// echo "cURL Error #:" . $err;
    			return $err;
    		} else {
    			$hsl = json_decode($response, TRUE);
    			$status = $hsl['status'];
    			if($status){
    				// return TRUE;
    				return $response;
    			} else {
    				// return FALSE;
    				return $response;
    			}
    		}
  	}
?>
