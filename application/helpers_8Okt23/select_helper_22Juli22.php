<?php

//OPD
function opd_array($placeholder)
{
    $ci = get_instance();
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select("NALOK, KOLOK");
    $ci->dbeps->where('A_02','00');
    $ci->dbeps->where('A_03','00');
    $ci->dbeps->where('A_04','00');
    $ci->dbeps->where('A_05','00');
    $ci->dbeps->where('AKTIF',1);
    $data = $ci->dbeps->get("TABLOKB08")->result();

    foreach ($data as $dt) {
        $select[$dt->KOLOK] = ($dt->NALOK);
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

//OPD + CABDIN
function opd_cabdin_array($placeholder)
{
    $ci = get_instance();

    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select("NALOK, KOLOK");
    $ci->dbeps->where('A_01','D0');
    $ci->dbeps->where('A_02 >= 21');
    $ci->dbeps->where('A_02 <= 33');
    $ci->dbeps->where('A_03','00');
    $ci->dbeps->where('A_04','00');
    $ci->dbeps->where('A_05','00');
    $ci->dbeps->where('AKTIF',1);
    $data_c = $ci->dbeps->get("TABLOKB08")->result();

    //
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select("NALOK, KOLOK");
    $ci->dbeps->where('A_02','00');
    $ci->dbeps->where('A_02','00');
    $ci->dbeps->where('A_03','00');
    $ci->dbeps->where('A_04','00');
    $ci->dbeps->where('A_05','00');
    $ci->dbeps->where('AKTIF',1);
    $data = $ci->dbeps->get("TABLOKB08")->result();

    foreach ($data as $dt) {
        $select[$dt->KOLOK] = ($dt->NALOK);
    }

    foreach ($data_c as $dtc) {
        $select_c[$dtc->KOLOK] = ($dtc->NALOK);
    }

    // return $select;

    if($placeholder == NULL){
        $merge = array_replace($select, $select_c);
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge2 = array_replace($placeholder_arr, $select);
        $merge = array_replace($merge2, $select_c);
    }

    return $merge;
}

//GET SIMGAJI LOKASIS
function opd_gaji_array_($placeholder, $show_id, $status_gaji, $lokasi = NULL)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    if($status_gaji == 2){
        $ci->db->where('status_gaji',$status_gaji);
    }

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }



    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function opd_gaji_array($placeholder, $show_id, $status_gaji, $lokasi = NULL)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    // if($status_gaji == 2){
    //     $ci->db->where('status_gaji',$status_gaji);
    // }

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

    //GET SIMGAJI LOKASIS
    function opd_gaji_v2_array($placeholder, $show_id, $status_gaji, $lokasi)
    {
        $ci = get_instance();
        $lokasis = explode(',',$lokasi);

        $ci->db->select("id, kode, name");

        $datasel = array();
        foreach ($lokasis as $key => $val){
            // $ci->db->where('id', $val);
            $datasel[$key] = $val;
        }

        if($status_gaji == 2){
            $ci->db->where('status_gaji',$status_gaji);
        }


        $ci->db->where_in('id',  $datasel);

        $data = $ci->db->get("simgaji_lokasis")->result();


        if($show_id == 'id'){
            foreach ($data as $dt) {
                // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
                $select[$dt->id] = ($dt->name.' - '.$dt->kode.' - '.$dt->id);
            }
        }else{
            foreach ($data as $dt) {
                // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
                $select[$dt->kode] = ($dt->name.' - '.$dt->kode.' - '.$dt->id);
            }
        }
        $select = '';
        if($placeholder == NULL){
            $merge = $select;
        }else{
            $placeholder_arr = array('' => $placeholder);
            $merge = array_replace($placeholder_arr, $select);
        }

        return $merge;



    }


//GET SIMGAJI LOKASIS
function opd_gaji_v3_array($placeholder, $show_id, $status_gaji)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    $ci->db->where('bool_id',2);

    // if($status_gaji == 2){
    //     $ci->db->where('status_gaji',$status_gaji);
    // }

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }



    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

//GET SIMGAJI LOKASIS
function opd_gaji_v4_array($placeholder, $show_id)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    $ci->db->where('bool_id',2);


    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }



    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function opd_array_fix_array($placeholder, $show_id, $role, $lokasi){

    $ci = get_instance();
    $ci->db->select("id, kode, name");


    if($lokasi != NULL){
        if($role == 2){

            $lokasi_arr = explode(',',$lokasi);

            foreach ($lokasi_arr as $val2){
                $ci->db->or_where('kode',$val2);
            }


        }else if($role == 99){

            //untuk external

        }else{
            $ci->db->where('kode',$lokasi);
        }

    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    $select = array();
    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->id);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;

}

function status_acc($sts){

    switch ($sts) {

        case '0':
            $icon = '<i  class="icon-cog"></i>';
            break;
        case '2':
            $icon = '<i style="color:orange" class="icon-backward">Buka / Revisi</i>';
            break;
        case '1':
            $icon = '<i style="color:green" class="icon-checkmark-circle">Acc</i>';
            break;
        case '3':
            $icon = '<i style="color:red" class="icon-cancel-circle2">Tolak</i>';
            break;
        default:
            $icon = "-";
            break;


    }

    return $icon;


}

function status_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'setuju',
        '2' => 'buka/revisi',
        '3' => 'tolak'
    );
    return $arr;
}


function status_array2($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'setuju',

        '3' => 'tolak'
    );
    return $arr;
}

function statusverif_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'buka',
        '2' => 'tutup',
    );
    return $arr;
}

function statusverif_nama($sts){

    switch ($sts) {

        case '2':
            $icon = 'Tutup';
            break;
        case '1':
            $icon = 'Buka';
            break;
        default:
            $icon = "-";
            break;
    }

    return $icon;


}

function statusdata_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'Kalkulasi',
        '2' => 'Buka/Revisi',
        '3' => 'Fixed',
        '4' => 'Cetak',
    );
    return $arr;
}

function statusdata_nama($sts){

    switch ($sts) {

        case '1':
            $icon = 'Kalkulasi';
            break;
        case '2':
            $icon = 'Buka/Revisi';
            break;
        case '3':
            $icon = 'Fixed';
            break;
        case '4':
            $icon = 'Cetak';
            break;
        default:
            $icon = "-";
            break;
    }

    return $icon;

}

function statusdata_show($sts){

    switch ($sts) {

        case '1':
            $icon = 'd-none';
            break;
        case '2':
            $icon = '-';
            break;
        case '3':
            $icon = 'd-none';
            break;
        case '4':
            $icon = 'd-none';
            break;
        default:
            $icon = "d-none";
            break;
    }

    return $icon;

}

function statusdata_redirect($sts){

    switch ($sts) {

        case '1':
            $icon = 'false';
            break;
        case '2':
            $icon = 'true';
            break;
        case '3':
            $icon = 'false';
            break;
        case '4':
            $icon = 'false';
            break;
        default:
            $icon = "false";
            break;
    }

    return $icon;

}

function jeniskalulasi_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        // ''  => $placeholder,
        // 'GAJI_BULANAN' => 'GAJI BULANAN',
        // 'TPP' => 'TPP',
        // 'GAJI_13' => 'GAJI 13',
        // 'GAJI_14' => 'GAJI 14',
        // 'TPP_13' => 'TPP 13',
        // 'GAJI_TAHUNAN' => 'GAJI TAHUNAN',
        // 'TPP_TAHUNAN' => 'TPP TAHUNAN',
        'GAJI_BULANAN_PPPK' => 'GAJI BULANAN PPPK',
        'GAJI_13_PPPK' => 'GAJI 13 PPPK',
    );
    return $arr;
}

function jeniscetak_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'DAFTAR GAJI',
        '2' => 'REKAP GAJI PER LOKASI INDUK',
        '3' => 'REKAP GAJI PER LOKASI',

    );
    return $arr;
}

function jenistahunan_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI' => 'DAFTAR GAJI',
            'REKAP GAJI PER LOKASI INDUK' => 'REKAP GAJI PER LOKASI INDUK',
            'REKAP GAJI PER LOKASI' => 'REKAP GAJI PER LOKASI',
            'REKAP GAJI KIRI KANAN' => 'REKAP GAJI KIRI KANAN',
            'REKAP GAJI LOKASI DETAIL' => 'REKAP GAJI LOKASI DETAIL',
            // 'REKAP GAJI SELURUH SKPD' => 'REKAP GAJI SELURUH SKPD',
            'REKAP BPJS KESEHATAN' => 'REKAP BPJS KESEHATAN',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            // 'REKAP BPJS JKK' => 'REKAP BPJS JKK',
            // 'REKAP BPJS JKM' => 'REKAP BPJS JKM',
            // 'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI' => 'DAFTAR GAJI',
            'REKAP GAJI PER LOKASI INDUK' => 'REKAP GAJI PER LOKASI INDUK',
            'REKAP GAJI PER LOKASI' => 'REKAP GAJI PER LOKASI',
            'REKAP GAJI KIRI KANAN' => 'REKAP GAJI KIRI KANAN',
            'REKAP GAJI LOKASI DETAIL' => 'REKAP GAJI LOKASI DETAIL',
            'REKAP GAJI SELURUH SKPD' => 'REKAP GAJI SELURUH SKPD',
            'REKAP BPJS KESEHATAN' => 'REKAP BPJS KESEHATAN',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            'REKAP BPJS JKK' => 'REKAP BPJS JKK',
            'REKAP BPJS JKM' => 'REKAP BPJS JKM',
            'GAJI PERSONAL' => 'GAJI PERSONAL',
            'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    }

    return $arrJenis;
}

function jenisbulananp3k_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI PPPK' => 'DAFTAR GAJI PPPK',
            'REKAP GAJI PPPK PER LOKASI INDUK' => 'REKAP GAJI PPPK PER LOKASI INDUK',
            'REKAP GAJI PPPK PER LOKASI' => 'REKAP GAJI PPPK PER LOKASI',
            'REKAP GAJI PPPK KIRI KANAN' => 'REKAP GAJI PPPK KIRI KANAN',
            'REKAP GAJI PPPK LOKASI DETAIL' => 'REKAP GAJI PPPK LOKASI DETAIL',
            'REKAP GAJI PPPK SELURUH SKPD' => 'REKAP GAJI PPPK SELURUH SKPD',
            'REKAP BPJS KESEHATAN PPPK' => 'REKAP BPJS KESEHATAN PPPK',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            'REKAP BPJS JKK PPPK' => 'REKAP BPJS JKK PPPK',
            'REKAP BPJS JKM PPPK' => 'REKAP BPJS JKM PPPK',
            'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI PPPK' => 'DAFTAR GAJI PPPK',
            'REKAP GAJI PPPK PER LOKASI INDUK' => 'REKAP GAJI PPPK PER LOKASI INDUK',
            'REKAP GAJI PPPK PER LOKASI' => 'REKAP GAJI PPPK PER LOKASI',
            'REKAP GAJI PPPK KIRI KANAN' => 'REKAP GAJI PPPK KIRI KANAN',
            'REKAP GAJI PPPK LOKASI DETAIL' => 'REKAP GAJI PPPK LOKASI DETAIL',
            'REKAP GAJI PPPK SELURUH SKPD' => 'REKAP GAJI PPPK SELURUH SKPD',
            'REKAP BPJS KESEHATAN PPPK' => 'REKAP BPJS KESEHATAN PPPK',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            'REKAP BPJS JKK PPPK' => 'REKAP BPJS JKK PPPK',
            'REKAP BPJS JKM PPPK' => 'REKAP BPJS JKM PPPK',
            'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    }

    return $arrJenis;
}

function jenisgaji13_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13' => 'DAFTAR GAJI 13',
			'REKAP GAJI 13 PER LOKASI INDUK' => 'REKAP GAJI 13 PER LOKASI INDUK',
			'REKAP GAJI 13 PER LOKASI' => 'REKAP GAJI 13 PER LOKASI',
			'REKAP GAJI 13 KIRI KANAN' => 'REKAP GAJI 13 KIRI KANAN',
			// 'REKAP GAJI 13 SELURUH SKPD' => 'REKAP GAJI 13 SELURUH SKPD',
			'DAFTAR BPJS KESEHATAN 13' => 'DAFTAR BPJS KESEHATAN 13',
			#'KEKURANGAN ASKES 13' => 'KEKURANGAN ASKES 13',
			#'REKAP BPJS KK 13' => 'REKAP BPJS KK 13',
			// 'REKAP BPJS JKK 13' => 'REKAP BPJS JKK 13',
			// 'REKAP BPJS JKM 13' => 'REKAP BPJS JKM 13',
			// 'GAJI PERSONAL 13' => 'GAJI PERSONAL 13',
			// 'REKAP PEGAWAI 13' => 'REKAP PEGAWAI 13',
			// 'REKAP GAJI 13 UMUM' => 'REKAP GAJI 13 UMUM',
			// 'REKAP GAJI 13 UMUM DETAIL' => 'REKAP GAJI 13 UMUM DETAIL',
			// 'REKAP GAJI 13 FUNGSIONAL' => 'REKAP GAJI 13 FUNGSIONAL',
			// 'REKAP GAJI 13 FUNGSIONAL DETAIL' => 'REKAP GAJI 13 FUNGSIONAL DETAIL',
			// 'REKAP GAJI 13 STRUKTURAL' => 'REKAP GAJI 13 STRUKTURAL',
			// 'REKAP GAJI 13 STRUKTURAL DETAIL' => 'REKAP GAJI 13 STRUKTURAL DETAIL',
			// 'REKAP GAJI 13 LOKASI DETAIL' => 'REKAP GAJI 13 LOKASI DETAIL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13' => 'DAFTAR GAJI 13',
			'REKAP GAJI 13 PER LOKASI INDUK' => 'REKAP GAJI 13 PER LOKASI INDUK',
			'REKAP GAJI 13 PER LOKASI' => 'REKAP GAJI 13 PER LOKASI',
			'REKAP GAJI 13 KIRI KANAN' => 'REKAP GAJI 13 KIRI KANAN',
			'REKAP GAJI 13 SELURUH SKPD' => 'REKAP GAJI 13 SELURUH SKPD',
			'DAFTAR BPJS KESEHATAN 13' => 'DAFTAR BPJS KESEHATAN 13',
			#'KEKURANGAN ASKES 13' => 'KEKURANGAN ASKES 13',
			#'REKAP BPJS KK 13' => 'REKAP BPJS KK 13',
			'REKAP BPJS JKK 13' => 'REKAP BPJS JKK 13',
			'REKAP BPJS JKM 13' => 'REKAP BPJS JKM 13',
			'GAJI PERSONAL 13' => 'GAJI PERSONAL 13',
			'REKAP PEGAWAI 13' => 'REKAP PEGAWAI 13',
			'REKAP GAJI 13 UMUM' => 'REKAP GAJI 13 UMUM',
			'REKAP GAJI 13 UMUM DETAIL' => 'REKAP GAJI 13 UMUM DETAIL',
			'REKAP GAJI 13 FUNGSIONAL' => 'REKAP GAJI 13 FUNGSIONAL',
			'REKAP GAJI 13 FUNGSIONAL DETAIL' => 'REKAP GAJI 13 FUNGSIONAL DETAIL',
			'REKAP GAJI 13 STRUKTURAL' => 'REKAP GAJI 13 STRUKTURAL',
			'REKAP GAJI 13 STRUKTURAL DETAIL' => 'REKAP GAJI 13 STRUKTURAL DETAIL',
			'REKAP GAJI 13 LOKASI DETAIL' => 'REKAP GAJI 13 LOKASI DETAIL',
        );
    }

    return $arrJenis;
}

function jenisgaji13p3k_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13 PPPK' => 'DAFTAR GAJI 13 PPPK',
			'REKAP GAJI 13 PPPK PER LOKASI INDUK' => 'REKAP GAJI 13 PPPK PER LOKASI INDUK',
			'REKAP GAJI 13 PPPK PER LOKASI' => 'REKAP GAJI 13 PPPK PER LOKASI',
			'REKAP GAJI 13 PPPK KIRI KANAN' => 'REKAP GAJI 13 PPPK KIRI KANAN',
			'REKAP GAJI 13 PPPK SELURUH SKPD' => 'REKAP GAJI 13 PPPK SELURUH SKPD',
			'REKAP GAJI 13 PPPK LOKASI DETAIL' => 'REKAP GAJI 13 PPPK LOKASI DETAIL',
      'GAJI PERSONAL' => 'GAJI PERSONAL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13 PPPK' => 'DAFTAR GAJI 13 PPPK',
			'REKAP GAJI 13 PPPK PER LOKASI INDUK' => 'REKAP GAJI 13 PPPK PER LOKASI INDUK',
			'REKAP GAJI 13 PPPK PER LOKASI' => 'REKAP GAJI 13 PPPK PER LOKASI',
			'REKAP GAJI 13 PPPK KIRI KANAN' => 'REKAP GAJI 13 PPPK KIRI KANAN',
			'REKAP GAJI 13 PPPK SELURUH SKPD' => 'REKAP GAJI 13 PPPK SELURUH SKPD',
			'REKAP GAJI 13 PPPK LOKASI DETAIL' => 'REKAP GAJI 13 PPPK LOKASI DETAIL',
      'GAJI PERSONAL' => 'GAJI PERSONAL',
        );
    }

    return $arrJenis;
}


function role_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'SUPERADMIN',
        '2' => 'VERIVIKATOR',
        '3' => 'EXTERNAL',


    );
    return $arr;
}

function role_data($sts){

    switch ($sts) {

        case '1':
            $icon = 'SUPERADMIN';
            break;
        case '2':
            $icon = 'VERIVIKATOR';
            break;

        default:
            $icon = "false";
            break;
    }

    return $icon;

}







?>
