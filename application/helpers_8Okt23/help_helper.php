<?php
function cari_kode_kabiro($lok){
    switch ($lok) {
            // case 'B4'	: $kumpeg="B400001000";break;
            // case 'B5'	: $kumpeg="B500001000";break;
            // case 'D0'	: $kumpeg="D000001000";break;
            // case 'D2'	: $kumpeg="D200001000";break;
            // case 'D3'	: $kumpeg="D300001000";break;
            // case 'D5'	: $kumpeg="D500001000";break;
            // case 'D6'	: $kumpeg="D600103000";break;
            // case 'D7'	: $kumpeg="D700001000";break;
            // case 'D9'	: $kumpeg="D900001000";break;
            // case 'E1'	: $kumpeg="E100001000";break;
            // case 'E2'	: $kumpeg="E200001000";break;
            // case 'E3'	: $kumpeg="E300001000";break;
            // case 'E4'	: $kumpeg="E400001000";break;
            // case 'E5'	: $kumpeg="E500001000";break;
            // case 'E6'	: $kumpeg="E600001000";break;
            // case 'E7'	: $kumpeg="E700001000";break;
            // case 'F1'	: $kumpeg="F100001000";break;
        case '24':
            $kumpeg = "A200240000";
            break;
        case '34':
            $kumpeg = "A200340000";
            break;
        case '21':
            $kumpeg = "A200210000";
            break;
        case '22':
            $kumpeg = "A200220000";
            break;
        case '32':
            $kumpeg = "A200320000";
            break;
        case '13':
            $kumpeg = "A200130000";
            break;
            // case '31'	: $kumpeg="A200240000";break; //BIRO ORGANISASI like kepala dinas/badan, selain ini like sekertaris di dinas
        case '11':
            $kumpeg = "A200110000";
            break;
        case '12':
            $kumpeg = "A200120000";
            break;
        default:
            $kumpeg = "0000000000";
            break;
    }
    return $kumpeg;
}

function cari_kode_kadinas($lok){
    switch ($lok) {
            // case 'B4'	: $kumpeg="B400001000";break;
            // case 'B5'	: $kumpeg="B500001000";break;
            // case 'D0'	: $kumpeg="D000001000";break;
            // case 'D2'	: $kumpeg="D200001000";break;
            // case 'D3'	: $kumpeg="D300001000";break;
            // case 'D5'	: $kumpeg="D500001000";break;
            // case 'D6'	: $kumpeg="D600103000";break;
            // case 'D7'	: $kumpeg="D700001000";break;
            // case 'D9'	: $kumpeg="D900001000";break;
            // case 'E1'	: $kumpeg="E100001000";break;
            // case 'E2'	: $kumpeg="E200001000";break;
            // case 'E3'	: $kumpeg="E300001000";break;
            // case 'E4'	: $kumpeg="E400001000";break;
            // case 'E5'	: $kumpeg="E500001000";break;
            // case 'E6'	: $kumpeg="E600001000";break;
            // case 'E7'	: $kumpeg="E700001000";break;
            // case 'F1'	: $kumpeg="F100001000";break;
        case 'A2':
            $ka = "A200310000";
            break; //KEPALA BIRO ORGANISASI
            // case 'A3'	: $sek="A300000000";break;
            // case '29'	: $sek="2900000000";break;
            // case 'C1'	: $sek="C100000000";break;
            // case '80'	: $sek="8000000000";break;
            // case '81'	: $sek="8100000000";break;
            // case '82'	: $sek="8200000000";break;
            // case '83'	: $sek="8300300000";break;
            // case '84'	: $sek="8400230000";break;
            // case '85'	: $sek="8500230000";break;
            // case '86'	: $sek="8600100000";break;
        default:
            $ka = $lok . "00000000";
            break;
    }
    return $ka;
}

function cari_kode_kasubbagkeu($A_01){
        switch ($A_01) {
            // case 'B4'	: $kumpeg="B400001000";break;
            // case 'B5'	: $kumpeg="B500001000";break;
            // case 'D0'	: $kumpeg="D000001000";break;
            // case 'D2'	: $kumpeg="D200001000";break;
            // case 'D3'	: $kumpeg="D300001000";break;
            // case 'D5'	: $kumpeg="D500001000";break;
            // case 'D6'	: $kumpeg="D600103000";break;
            // case 'D7'	: $kumpeg="D700001000";break;
            // case 'D9'	: $kumpeg="D900001000";break;
            // case 'E1'	: $kumpeg="E100001000";break;
            // case 'E2'	: $kumpeg="E200001000";break;
            // case 'E3'	: $kumpeg="E300001000";break;
            // case 'E4'	: $kumpeg="E400001000";break;
            // case 'E5'	: $kumpeg="E500001000";break;
            // case 'E6'	: $kumpeg="E600001000";break;
            // case 'E7'	: $kumpeg="E700001000";break;
            // case 'F1'	: $kumpeg="F100001000";break;
        case 'A2':
            $ka = "A200310000";
            break; //KEPALA BIRO ORGANISASI
            // case 'A3'	: $sek="A300000000";break;
            // case '29'	: $sek="2900000000";break;
            // case 'C1'	: $sek="C100000000";break;
            // case '80'	: $sek="8000000000";break;
            // case '81'	: $sek="8100000000";break;
            // case '82'	: $sek="8200000000";break;
            // case '83'	: $sek="8300300000";break;
            // case '84'	: $sek="8400230000";break;
            // case '85'	: $sek="8500230000";break;
            // case '86'	: $sek="8600100000";break;
        default:
            $ka = $A_01 . "00102000";
            break;
    }
    return $ka;
}

function cari_kode_sekertaris($A_01){
    switch ($A_01) {
        // case 'B4'	: $kumpeg="B400001000";break;
        // case 'B5'	: $kumpeg="B500001000";break;
        // case 'D0'	: $kumpeg="D000001000";break;
        // case 'D2'	: $kumpeg="D200001000";break;
        // case 'D3'	: $kumpeg="D300001000";break;
        // case 'D5'	: $kumpeg="D500001000";break;
        // case 'D6'	: $kumpeg="D600103000";break;
        // case 'D7'	: $kumpeg="D700001000";break;
        // case 'D9'	: $kumpeg="D900001000";break;
        // case 'E1'	: $kumpeg="E100001000";break;
        // case 'E2'	: $kumpeg="E200001000";break;
        // case 'E3'	: $kumpeg="E300001000";break;
        // case 'E4'	: $kumpeg="E400001000";break;
        // case 'E5'	: $kumpeg="E500001000";break;
        // case 'E6'	: $kumpeg="E600001000";break;
        // case 'E7'	: $kumpeg="E700001000";break;
        // case 'F1'	: $kumpeg="F100001000";break;
        // case 'A2'	: $sek="A200321000";break;
        case 'A2'	: $sek="A203130000";break; //KEPALA BAGIAN AKUNTABILITAS KINERJA DAN REFORMASI BIROKRASI
        case 'A3'	: $sek="A300100000";break;
        case '29'	: $sek="2900100000";break;
        case 'C1'	: $sek="C100001000";break;
        // case '80'	: $sek="8000330000";break;
        case '80'	: $sek="8000210000";break;
        case '81'	: $sek="8100330000";break;
        // case '82'	: $sek="8200230000";break;
        case '83'	: $sek="8300300000";break;
        case '84'	: $sek="8400230000";break;
        case '85'	: $sek="8500230000";break;
        case '86'	: $sek="8600100000";break;
        default		: $sek=$A_01."00100000";break;
    }

    return $sek;
}

function status_ajuan($lok){
    switch ($lok) {
        case '3':
            return "Acc";
            break;
        default:
        return "Belum Acc";
        break;
        }
}

function create_log($id_users, $id_ref, $act){

    $ci = get_instance();

    $now = date('Y-m-d H:i:s');
    $ip = $ci->input->ip_address();

    $data = array(
        'id_mcusers'    => $id_users,
        'id_reflog'     => $id_ref,
        'activity'      => $act,
        'created_at'    => $now,
        'ip_address'    => $ip,
    );

    return $ci->db->insert('simgaji_log', $data);
}

function caricek_kode_kasubbagkeu($kolok, $n){
    $A_01 = substr($kolok, 0, 2);

    if($kolok == 'A200321300'){
        $kolok_kasubbagkeu = $A_01 . "00321300";
    }else{

        switch ($A_01) {
            case '86':
                $kolok_kasubbagkeu = $A_01 . "00100000";
                break;
    
            default:
                $kolok_kasubbagkeu = $A_01 . "00102000";
                break;
        }

    }

    

    // return $kolok_kasubbagkeu;
    if($kolok_kasubbagkeu == $kolok){
        //user is kasubbag keu SKPD induk prov
        return TRUE;

    } else {

        $ci = get_instance();

        $dbsinaga = $ci->load->database('sinaga', TRUE);

        // $sql =
        // "SELECT *
        // FROM pejabat_pengganti
        // WHERE unitkerja = ?
        // AND deleted_at IS NULL
        // ORDER BY id DESC
        // LIMIT 1";
        $sql =
        "SELECT *
        FROM pejabat_pengganti
        WHERE nip_pengganti = ?
        AND deleted_at IS NULL
        ORDER BY id DESC
        LIMIT 1";

        // $query = $dbsinaga->query($sql, array($kolok_kasubbagkeu));
        $query = $dbsinaga->query($sql, array($n));
        if($query->num_rows() > 0){
            //user is PLT kasubbag keu SKPD induk prov
            // return TRUE;

            //handling set false login umpeg
            $uk_pengganti = $query->row()->unitkerja;
            if($uk_pengganti == '8000331000' || $uk_pengganti == '8100332000' || $uk_pengganti == '8200232000' || $uk_pengganti == '8300302000' || $uk_pengganti == '8400231000' || $uk_pengganti == '8500231000' || $uk_pengganti == '8600100000'){
                return FALSE;
            } else {
                return TRUE;
            }
        }else{
            return FALSE;
        }

       
    }
}

function caricek_kode_kasubbagtusekolah($A_01, $A_02, $A_03, $A_04, $A_05, $nip){

    $firstA_04 = substr($A_04, 0, 1);

    if($A_01 == 'D0' && $A_02 != '00' && ($firstA_04 > 3 || $firstA_04 == 'A' || $A_04 == '9F' || $A_04 == '9G') && $A_05 == '10'){
        //TU SEKOLAH
        return TRUE;
    } else {
        //cek PLT pejabat_pengganti
        $kolok = $A_01 . $A_02 . $A_03 . $A_04 . '10';
        $ci = get_instance();

        $dbsinaga = $ci->load->database('sinaga', TRUE);

        $sql =
        "SELECT *
        FROM pejabat_pengganti
        WHERE nip_pengganti = ?
        AND unitkerja = ?
        ORDER BY id DESC
        LIMIT 1";

        $query = $dbsinaga->query($sql, array($nip, $kolok));
        if($query->num_rows() > 0){
            return TRUE;
        } else {
            return FALSE;
        }

    }
}

function caricek_kode_sekertaris($kolok){
    $A_01 = substr($kolok, 0, 2);

    switch ($A_01) {
        // case 'B4'	: $kumpeg="B400001000";break;
        // case 'B5'	: $kumpeg="B500001000";break;
        // case 'D0'	: $kumpeg="D000001000";break;
        // case 'D2'	: $kumpeg="D200001000";break;
        // case 'D3'	: $kumpeg="D300001000";break;
        // case 'D5'	: $kumpeg="D500001000";break;
        // case 'D6'	: $kumpeg="D600103000";break;
        // case 'D7'	: $kumpeg="D700001000";break;
        // case 'D9'	: $kumpeg="D900001000";break;
        // case 'E1'	: $kumpeg="E100001000";break;
        // case 'E2'	: $kumpeg="E200001000";break;
        // case 'E3'	: $kumpeg="E300001000";break;
        // case 'E4'	: $kumpeg="E400001000";break;
        // case 'E5'	: $kumpeg="E500001000";break;
        // case 'E6'	: $kumpeg="E600001000";break;
        // case 'E7'	: $kumpeg="E700001000";break;
        // case 'F1'	: $kumpeg="F100001000";break;
        // case 'A2'	: $sek="A200321000";break;
        case 'A2'	: $sek="A203130000";break; //KEPALA BAGIAN AKUNTABILITAS KINERJA DAN REFORMASI BIROKRASI
        case 'A3'	: $sek="A300100000";break;
        case '29'	: $sek="2900100000";break;
        case 'C1'	: $sek="C100001000";break;
        // case '80'	: $sek="8000330000";break;
        // case '81'	: $sek="8100330000";break;
        case '82'	: $sek="8200000000";break;
        // case '83'	: $sek="8300300000";break;
        // case '84'	: $sek="8400230000";break;
        // case '85'	: $sek="8500230000";break;
        // case '86'	: $sek="8600100000";break;
        default		: $sek=$A_01."00100000";break;
    }

    // return $sek;
    if($sek == $kolok){
        //user is sek SKPD induk prov
        return TRUE;
    } else {
        return FALSE;
    }
}

function caricek_kode_kepalaskpd($kolok){
    $A_01 = substr($kolok, 0, 2);
    $A_02 = substr($kolok, 2,2);
    $A_03 = substr($kolok, 4,2);

    // if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '11'){
    //     //KEPALA BIRO Pemotdaks
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '21'){
    //     //KEPALA BIRO Perekonomian
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '31'){
    //     //KEPALA BIRO Organisasi
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '12'){
    //     //KEPALA BIRO hukum
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '22'){
    //     //KEPALA BIRO Infrastruktur SDA
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '32'){
    //     //KEPALA BIRO Umum
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '13'){
    //     //KEPALA BIRO kesra
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '24'){
    //     //KEPALA BIRO APBJ
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '34'){
    //     //KEPALA BIRO ADMINISTRASI PEMBANGUNAN DAERAH
    //     $ka = $A_01 . $A_02 . $A_03 . '0000';
    // } else {
    //     $ka = $A_01 . "00000000";
    // }

    if($A_01 == 'A2' && $A_02 == '00' && $A_03 == '32'){
        //KEPALA BIRO Umum (setda)
        $ka = $A_01 . $A_02 . $A_03 . '0000';
    }else {
        $ka = $A_01 . "00000000";
    }

    // switch ($A_01) {
    //         // case 'B4'	: $kumpeg="B400001000";break;
    //         // case 'B5'	: $kumpeg="B500001000";break;
    //         // case 'D0'	: $kumpeg="D000001000";break;
    //         // case 'D2'	: $kumpeg="D200001000";break;
    //         // case 'D3'	: $kumpeg="D300001000";break;
    //         // case 'D5'	: $kumpeg="D500001000";break;
    //         // case 'D6'	: $kumpeg="D600103000";break;
    //         // case 'D7'	: $kumpeg="D700001000";break;
    //         // case 'D9'	: $kumpeg="D900001000";break;
    //         // case 'E1'	: $kumpeg="E100001000";break;
    //         // case 'E2'	: $kumpeg="E200001000";break;
    //         // case 'E3'	: $kumpeg="E300001000";break;
    //         // case 'E4'	: $kumpeg="E400001000";break;
    //         // case 'E5'	: $kumpeg="E500001000";break;
    //         // case 'E6'	: $kumpeg="E600001000";break;
    //         // case 'E7'	: $kumpeg="E700001000";break;
    //         // case 'F1'	: $kumpeg="F100001000";break;
    //     case 'A2':
    //         $ka = "A200310000";
    //         break; //KEPALA BIRO ORGANISASI
    //     case 'A2':
    //         $ka = "A200240000"; //KEPALA BIRO APBJ
    //         break;
    //     case 'A2':
    //         $ka = "A200110000"; //KEPALA BIRO Pemotdaks
    //         break;
    //         // case 'A3'	: $sek="A300000000";break;
    //         // case '29'	: $sek="2900000000";break;
    //         // case 'C1'	: $sek="C100000000";break;
    //         // case '80'	: $sek="8000000000";break;
    //         // case '81'	: $sek="8100000000";break;
    //         // case '82'	: $sek="8200000000";break;
    //         // case '83'	: $sek="8300300000";break;
    //         // case '84'	: $sek="8400230000";break;
    //         // case '85'	: $sek="8500230000";break;
    //         // case '86'	: $sek="8600100000";break;
    //     default:
    //         $ka = $A_01 . "00000000";
    //         break;
    // }
    // return $ka;

    if($ka == $kolok){
        //user is kepala SKPD induk prov
        return TRUE;
    } else {
        return FALSE;
    }
}

function getNipSekByKolok($kolok){
    $A_01 = substr($kolok, 0, 2);

    switch ($A_01) {
        // case 'B4'	: $kumpeg="B400001000";break;
        // case 'B5'	: $kumpeg="B500001000";break;
        // case 'D0'	: $kumpeg="D000001000";break;
        // case 'D2'	: $kumpeg="D200001000";break;
        // case 'D3'	: $kumpeg="D300001000";break;
        // case 'D5'	: $kumpeg="D500001000";break;
        // case 'D6'	: $kumpeg="D600103000";break;
        // case 'D7'	: $kumpeg="D700001000";break;
        // case 'D9'	: $kumpeg="D900001000";break;
        // case 'E1'	: $kumpeg="E100001000";break;
        // case 'E2'	: $kumpeg="E200001000";break;
        // case 'E3'	: $kumpeg="E300001000";break;
        // case 'E4'	: $kumpeg="E400001000";break;
        // case 'E5'	: $kumpeg="E500001000";break;
        // case 'E6'	: $kumpeg="E600001000";break;
        // case 'E7'	: $kumpeg="E700001000";break;
        // case 'F1'	: $kumpeg="F100001000";break;
        // case 'A2'	: $sek="A200321000";break;
        case 'A2'	: $sek="A203130000";break; //KEPALA BAGIAN AKUNTABILITAS KINERJA DAN REFORMASI BIROKRASI
        case 'A3'	: $sek="A300100000";break;
        case '29'	: $sek="2900100000";break;
        case 'C1'	: $sek="C100001000";break;
        // case '80'	: $sek="8000330000";break;
        case '80'	: $sek="8000210000";break; //MOEWARDI, KABID ANGGARAN
        case '81'	: $sek="8100320000";break; //MARGONO, KABAG KEUANGAN
        case '82'	: $sek="8200220000";break; //TUGUREJO, KABAG KEUANGAN
        case '83'	: $sek="8300300000";break; //KELET, KABAG TU RS UMUM KELET
        // case '84'	: $sek="8400230000";break;
        case '84'	: $sek="8400220000";break; //AMINO, KABAG KEUANGAN
        case '85'	: $sek="8500220000";break; //SURAKARTA, KABAG KEUANGAN
        case '86'	: $sek="8600100000";break;
        default		: $sek=$A_01."00100000";break;
    }

    $kolok_sekretaris = $sek;

    $sqlSek =
    "SELECT *
    FROM MASTFIP08
    WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = '$kolok_sekretaris'
    AND I_5A = '1'";

    $ci = get_instance();

    // $db = $ci->load->database('default', TRUE);
    $dbeps = $ci->load->database('eps', TRUE);

    $data = $dbeps->query($sqlSek)->row();

    return $data->B_02B;
}

function getNipByKolok($kolok){
    $ci = get_instance();

    $db = $ci->load->database('default', TRUE);
    $dbeps = $ci->load->database('eps', TRUE);

    $sql =
    "SELECT *
    FROM MASTFIP08
    WHERE CONCAT(A_01, A_02, A_03, A_04, A_05) = ?
    AND I_5A = '1'";

    $query = $dbeps->query($sql, array($kolok));

    if($query->num_rows() > 0){
        return $query->row()->B_02B;
    }else{

        $dbsinaga = $ci->load->database('sinaga', TRUE);
        $sql_pengganti =
        "SELECT *
        FROM pejabat_pengganti
        WHERE unitkerja = ?";
        $query_pengganti = $dbsinaga->query($sql_pengganti, array($kolok));
        if($query_pengganti->num_rows() > 0){
            return $query_pengganti->row()->nip_pengganti;
        }else{
            return 0;
        }

    }

    
}

function getNipByKolokStruktural($kolok){
    $ci = get_instance();

    $db = $ci->load->database('default', TRUE);
    $dbeps = $ci->load->database('eps', TRUE);
    $dbsinaga = $ci->load->database('sinaga', TRUE);

    $sql =
    "SELECT *
    FROM MASTFIP08
    WHERE CONCAT(A_01, A_02, A_03, A_04, A_05) = ?
    AND I_5A = '1'";

    $query = $dbeps->query($sql, array($kolok));
    if ($query->num_rows() > 0) {
        $data = $query->row()->B_02B;
        $is_plt = 0;
        $unit_kerja_plt = NULL;
    } else {
        //cari pejabat pengganti
        $sql_pengganti =
        "SELECT *
        FROM pejabat_pengganti
        WHERE unitkerja = ?";

        $query_pengganti = $dbsinaga->query($sql_pengganti, array($kolok));

        $data = $query_pengganti->row()->nip_pengganti;
        $is_plt = 1;
        $unit_kerja_plt = $query_pengganti->row()->unitkerja;;
    }
    $kembalian = array(
        'nip' => $data,
        'is_plt' => $is_plt,
        'unit_kerja_plt' => $unit_kerja_plt
    );

    return $kembalian;
}

function caricek_kode_tucabdin($nip, $kolok){
    //D021001000 tu cabdin
    $A_01 = substr($kolok, 0, 2);
    $A_02 = substr($kolok, 2, 2);
    $A_03 = substr($kolok, 4, 2);
    $A_04 = substr($kolok, 6, 2);

    if($A_01 == 'D0' && $A_02 <> '00' && $A_04 == '10'){
        //TU CABDIN
        return TRUE;
    } else {
        // return FALSE;

        //cek PLT pejabat_pengganti
        $kolok = $A_01 . $A_02 . $A_03 . '10' . '00';
        $ci = get_instance();

        $dbsinaga = $ci->load->database('sinaga', TRUE);

        $sql =
        "SELECT *
        FROM pejabat_pengganti
        WHERE nip_pengganti = ?
        ORDER BY id DESC
        LIMIT 1";

        $query = $dbsinaga->query($sql, array($nip));
        if($query->num_rows() > 0){
            $r = $query->row();
            $A_01pengganti = substr($r->unitkerja, 0, 2);
            if($A_01pengganti == 'D0'){
                //sementara boss
                if($nip == '197101221993032005'){
                    return FALSE;
                } else {
                    return TRUE;
                }

                
                // return TRUE;
            } else {
                return FALSE;
            }
            
        } else {
            return FALSE;
        }
    }
}

function namaBulan($input)
{
    if ($input == '1') {
        $output = 'Januari';
    }
    if ($input == '2') {
        $output = 'Februari';
    }
    if ($input == '3') {
        $output = 'Maret';
    }
    if ($input == '4') {
        $output = 'April';
    }
    if ($input == '5') {
        $output = 'Mei';
    }
    if ($input == '6') {
        $output = 'Juni';
    }
    if ($input == '7') {
        $output = 'Juli';
    }
    if ($input == '8') {
        $output = 'Agustus';
    }
    if ($input == '9') {
        $output = 'September';
    }
    if ($input == '10') {
        $output = 'Oktober';
    }
    if ($input == '11') {
        $output = 'November';
    }
    if ($input == '12') {
        $output = 'Desember';
    }
    return $output;
}

function get_pengampu($kode_lokasi){
    error_reporting(0);
    $ci = get_instance();

    $b = $ci->db->query("SELECT * FROM simgaji_verifikator WHERE id_skpd LIKE '%$kode_lokasi%'");

    // if($b->num_rows > 0){
    //     $x = $b->row()->nama;
    // }else{
    //     $x = '';
    // }

    $x = $b->row()->nama;

    return $x;
}

 function ifVerif($B_02B)
    {
        error_reporting(0);
        $ci = get_instance();
        // $B_02B = $ci->session->userdata('nip');
        $x = $ci->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$B_02B'");
        // if ($x->num_rows > 0) {
        //     $a = $x->row()->id_skpd;
        // } else {
        //     $a = '';
        // }

        $a = $x->row()->id_skpd;

        return $a;
    }


    function ifVerif2($B_02B)
    {
        error_reporting(0);
        $ci = get_instance();
        // $B_02B = $ci->session->userdata('nip');
        $x = $ci->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$B_02B' AND deleted_at IS NULL");
        // if ($x->num_rows > 0) {
        //     $a = $x->row()->id_skpd;
        // } else {
        //     $a = '';
        // }

        $a = $x->row()->kode_skpd;

        return $a;
    }


    function get_lokasi($kode_lokasi){
        // error_reporting(0);

        $ci = get_instance();
        $query_lokasi = "SELECT * FROM `simgaji_lokasis` WHERE kode = ?";
            $get_lokasi = $ci->db->query($query_lokasi, array($kode_lokasi));
            // if ($get_lokasi->num_rows() > 0) {
            //     $lokasi = $get_lokasi->row();
            // } else {
            //     $lokasi = array();
            // }
            $lokasi = $get_lokasi->row();

        return $lokasi;
    }


    function get_lokasi_id($kode_lokasi){
        // error_reporting(0);

        $ci = get_instance();
        $query_lokasi = "SELECT * FROM `simgaji_lokasis` WHERE id = ?";
            $get_lokasi = $ci->db->query($query_lokasi, array($kode_lokasi));
            // if ($get_lokasi->num_rows() > 0) {
            //     $lokasi = $get_lokasi->row();
            // } else {
            //     $lokasi = array();
            // }
            $lokasi = $get_lokasi->row();

        return $lokasi;
    }

    function get_lokasi_new($kode_lokasi){
        // error_reporting(0);

        $ci = get_instance();
        $query_lokasi = "SELECT * FROM `simgaji_lokasis` WHERE kode = ?";
            $get_lokasi = $ci->db->query($query_lokasi, array($kode_lokasi));
            if ($get_lokasi->num_rows() > 0) {
                $lokasi = $get_lokasi->row()->name;
            } else {
                $lokasi = '-';
            }
            // $lokasi = $get_lokasi->row();

        return $lokasi;
    }


    function get_lokasi_id_new($kode_lokasi){
        // error_reporting(0);

        $ci = get_instance();
        $query_lokasi = "SELECT * FROM `simgaji_lokasis` WHERE id = ?";
            $get_lokasi = $ci->db->query($query_lokasi, array($kode_lokasi));
            if ($get_lokasi->num_rows() > 0) {
                $lokasi = $get_lokasi->row()->name;
            } else {
                $lokasi = '-';
            }
            // $lokasi = $get_lokasi->row();

        return $lokasi;
    }

    function get_bulan($bulan){
        // error_reporting(0);

        $ci = get_instance();
        $query_bulan = "SELECT * FROM `bulans` WHERE id = ?";
            $get_lokasi = $ci->db->query($query_bulan, array($bulan));
            if ($get_lokasi->num_rows() > 0) {
                $lokasi = $get_lokasi->row();
            } else {
                $lokasi = array();
            }

        return $lokasi;
    }

    function get_lokasiSKPD($lokasi_id){
        // error_reporting(0);

        $ci = get_instance();
        $query_lokasi = "SELECT * FROM `simgaji_lokasis` WHERE id = ?";
            $get_lokasi = $ci->db->query($query_lokasi, array($lokasi_id));
            // if ($get_lokasi->num_rows() > 0) {
            //     $lokasi = $get_lokasi->row();
            // } else {
            //     $lokasi = array();
            // }
            $lokasi = $get_lokasi->row();

        return $lokasi;
    }

    function numFormat($number) {
        return number_format($number,0,",",".");
    }

    function cekDataPns($tabel, $id)
    {
        $ci = get_instance();
        // $B_02B = $ci->session->userdata('nip');
        $x = $ci->db->query("SELECT * FROM $tabel WHERE id = '$id'");
        // if ($x->num_rows > 0) {
        //     $a = $x->row()->id_skpd;
        // } else {
        //     $a = '';
        // }

        $a = $x->row();

        return $a;
    }

    function caricek_kode_keuanganRS($kolok){

        if($kolok == '8300301000'){
            //keuangan RS KELET
            $dt = TRUE;
        } else if($kolok == '8000212000'){
            //DR. MOEWARDI
            $dt = TRUE;
        } else if($kolok == '8100323000'){
            //MARGONO
            $dt = TRUE;
        } else if($kolok == '8200223000'){
            //TUGUREJO
            $dt = TRUE;
        } else if($kolok == '8400222000'){
            //AMINO
            $dt = TRUE;
        } else if($kolok == '8500222000'){
            //SURAKARTA
            $dt = TRUE;
        } else if($kolok == '8600100000'){
            //soedjarwadi
            $dt = TRUE;
        } else {
            $dt = FALSE;
        }

        return $dt;
    }

    function caricek_kode_sekretarisRS($kolok){
        if($kolok == '8300300000'){
            //RS KELET
            $dt = TRUE;
        } else if($kolok == '8000210000'){
            //moewardi
            $dt = TRUE;
        } else if($kolok == '8100320000'){
            //margono
            $dt = TRUE;
        } else if($kolok == '8200220000'){
            //tugurejo
            $dt = TRUE;
        } else if($kolok == '8400220000'){
            //amino
            $dt = TRUE;
        } else if($kolok == '8500220000'){
            //surakarta
            $dt = TRUE;
        } else {
            $dt = FALSE;
        }

        return $dt;
    }

    function caricek_kode_direkturRS($kolok){
        if($kolok == ''){
            //RS KELET
            $dt = TRUE;
        } else if($kolok == ''){
            //moewardi
            $dt = TRUE;
        } else if($kolok == ''){
            //margono
            $dt = TRUE;
        } else if($kolok == ''){
            //tugurejo
            $dt = TRUE;
        } else if($kolok == ''){
            //amino
            $dt = TRUE;
        } else if($kolok == ''){
            //surakarta
            $dt = TRUE;
        } else {
            $dt = FALSE;
        }

        return $dt;
    }

    function getRoleFromKolokRS($kolok){
        //cek for PLT
      if($kolok == '8300301000'){
        //keuangan RS KELET
        $dt = 3;
      } else if($kolok == '8300300000'){
        //sekretaris RS KELET
        $dt = 5;
      } else if($kolok == '8300000000'){
        //kepala RS KELET
        $dt = 6;
      } else if($kolok == '8100000000'){
        //kepala margono
        $dt = 6;
      } else if($kolok == '8200000000'){
          //kepala tugurejo
          $dt = 6;
      }
      // else if($kolok == 'D022006A10'){
      //   //KATU SMA NEGERI 1 WEDUNG
      //   $dt = 4;
      // } else if($kolok == 'D023006710'){
      //   //KATU SMA NEGERI 1 JEKULO KUDUS
      //   $dt = 4;
      // } else if($kolok == 'D023004D10'){
      //   //KATU SMK N 4 PATI
      //   $dt = 4;
      // } else if($kolok == 'D021004S10'){
      //   //KATU SMK NEGERI JAWA TENGAH
      //   $dt = 4;
      // }
      // else if($kolok == 'D023004B10'){
      //   //KATU SMKN 1 CLUWAK
      //   $dt = 4;
      // }

      else {
            $dt = 99;
      }

      return $dt;
    }

    function getNama($real_id)
    {
        error_reporting(0);
        $ci = get_instance();
        // $B_02B = $ci->session->userdata('nip');
        $x = $ci->db->query("SELECT * FROM simgaji_pegawai_tpp WHERE id = '$real_id'");
        // if ($x->num_rows > 0) {
        //     $a = $x->row()->nip.' - '.$x->row()->nip;
        // } else {
        //     $a = 'A';
        // }

        $a = $x->row()->nip.' - '.$x->row()->name;

        // $a = $x->row()->id_skpd;

        return $a;
    }

    function caricek_kode_umpeg($kolok, $I_5A, $nip){
        if($I_5A != 1){
            // return FALSE;
            $ci = get_instance();

            $dbsinaga = $ci->load->database('sinaga', TRUE);

            $sql =
            "SELECT *
            FROM pejabat_pengganti
            WHERE nip_pengganti = ?
            ORDER BY id DESC
            LIMIT 1";

            $query = $dbsinaga->query($sql, array($nip));
            if($query->num_rows() > 0){
                $A_01_plt = substr($query->row()->unitkerja, 0, 2);
    
                switch ($A_01_plt) {
                    case '80'	: $umpegPLT="8000331000";break; //RS MOEWARDI, KASUBBAG ORGANISASI & ADM. PEGAWAI
                    case '81'	: $umpegPLT="8100332000";break; //MARGONO SOE, KASUBBAG ORGANISASI  & KEPEGAWAIAN
                    case '82'	: $umpegPLT="8200232000";break; //TUGUREJO, KASUBBAG ORG & KEPEGAWAIAN
                    case '83'	: $umpegPLT="8300302000";break; //KELET, KASUBBAG UMUM & KEPEGAWAIAN
                    case '84'	: $umpegPLT="8400231000";break; //AMINO, KASUBBAG KEPEGAWAIAN, TU & HUKUM
                    case '85'	: $umpegPLT="8500231000";break; //SURAKARTA, KASUBBAG KEPEGAWAIAN, TU & HUKUM
                    case '86'	: $umpegPLT="8600100000";break; //SOEDJARWADI, KASUBBAG TATA USAHA
                    default		: $umpegPLT = $A_01."00103000";break;
                }
            
                // return $sek;
                if($umpegPLT == $query->row()->unitkerja){
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                return FALSE;
            }
        } else {
            $A_01 = substr($kolok, 0, 2);
    
            switch ($A_01) {
                case 'A2'	: $umpeg="A200312300";break; //KEPALA SUB BAGIAN TATA USAHA BIRO
                // case 'A3'	: $sek="A300100000";break;
                // case '29'	: $sek="2900100000";break;
                // case 'C1'	: $sek="C100001000";break;
                case '80'	: $umpeg="8000331000";break; //RS MOEWARDI, KASUBBAG ORGANISASI & ADM. PEGAWAI
                case '81'	: $umpeg="8100332000";break; //MARGONO SOE, KASUBBAG ORGANISASI  & KEPEGAWAIAN
                case '82'	: $umpeg="8200232000";break; //TUGUREJO, KASUBBAG ORG & KEPEGAWAIAN
                case '83'	: $umpeg="8300302000";break; //KELET, KASUBBAG UMUM & KEPEGAWAIAN
                case '84'	: $umpeg="8400231000";break; //AMINO, KASUBBAG KEPEGAWAIAN, TU & HUKUM
                case '85'	: $umpeg="8500231000";break; //SURAKARTA, KASUBBAG KEPEGAWAIAN, TU & HUKUM
                case '86'	: $umpeg="8600100000";break; //SOEDJARWADI, KASUBBAG TATA USAHA
                default		: $umpeg = $A_01."00103000";break;
            }
        
            // return $sek;
            if($umpeg == $kolok){
                //user is sek SKPD induk prov
                return TRUE;
            } else {
                // return FALSE;

                //cek PLT pejabat_pengganti
                // $kolok = $A_01 . $A_02 . $A_03 . '10' . '00';
                $ci = get_instance();

                $dbsinaga = $ci->load->database('sinaga', TRUE);

                $sql =
                "SELECT *
                FROM pejabat_pengganti
                WHERE nip_pengganti = ?
                ORDER BY id DESC
                LIMIT 1";

                $query = $dbsinaga->query($sql, array($nip));
                if($query->num_rows() > 0){
                    // return TRUE;
                    if($kolok == '8200100000'){
                        return FALSE;
                    } else {
                        return TRUE;
                    }
                } else {
                    return FALSE;
                }
            }
        }
    }

    function getStatusPeriodeTppSimpeg($A_01, $bulan, $tahun){

        $ci = get_instance();

        $dbeps = $ci->load->database('eps', TRUE);

        $sql =
        "SELECT *
        FROM periodetpp
        WHERE bulan = ?
        AND tahun =?";

        $query = $dbeps->query($sql, array($bulan, $tahun));

        if($query->num_rows() > 0){
            // $data = $query->row();
            $r = $query->row();
            $uk = $r->unit_kerja;

            $aktif = $r->active;

            $data = FALSE;
            if($aktif == '1' || $aktif == '2'){
                $units = explode(';', $uk);
                foreach ($units as $key => $value) {
                    if($value == $A_01){
                        $data = TRUE;
                        break;
                    }
                }
            }
            
        }else{
            $data = FALSE;
        }

        return $data;
        // return TRUE;
    }