<?php

//verifikator
function verifikator_array($placeholder)
{
    $ci = get_instance();
    $ci->db->where('deleted_at IS NULL');
    $data = $ci->db->get("simgaji_verifikator")->result();

    foreach ($data as $dt) {
        $select[$dt->id] = ($dt->nama);
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

//OPD
function opd_array($placeholder)
{
    $ci = get_instance();
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select("NALOK, KOLOK");
    $ci->dbeps->where('A_02','00');
    $ci->dbeps->where('A_03','00');
    $ci->dbeps->where('A_04','00');
    $ci->dbeps->where('A_05','00');
    $ci->dbeps->where('AKTIF',1);
    $data = $ci->dbeps->get("TABLOKB08")->result();

    foreach ($data as $dt) {
        $select[$dt->KOLOK] = ($dt->NALOK);
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

//OPD + CABDIN
function opd_cabdin_array($placeholder)
{
    $ci = get_instance();

    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select("NALOK, KOLOK");
    $ci->dbeps->where('A_01','D0');
    $ci->dbeps->where('A_02 >= 21');
    $ci->dbeps->where('A_02 <= 33');
    $ci->dbeps->where('A_03','00');
    $ci->dbeps->where('A_04','00');
    $ci->dbeps->where('A_05','00');
    $ci->dbeps->where('AKTIF',1);
    $data_c = $ci->dbeps->get("TABLOKB08")->result();

    //
    $ci->dbeps = $ci->load->database('eps', TRUE);
    $ci->dbeps->select("NALOK, KOLOK");
    $ci->dbeps->where('A_02','00');
    $ci->dbeps->where('A_02','00');
    $ci->dbeps->where('A_03','00');
    $ci->dbeps->where('A_04','00');
    $ci->dbeps->where('A_05','00');
    $ci->dbeps->where('AKTIF',1);
    $data = $ci->dbeps->get("TABLOKB08")->result();

    foreach ($data as $dt) {
        $select[$dt->KOLOK] = ($dt->NALOK);
    }

    foreach ($data_c as $dtc) {
        $select_c[$dtc->KOLOK] = ($dtc->NALOK);
    }

    // return $select;

    if($placeholder == NULL){
        $merge = array_replace($select, $select_c);
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge2 = array_replace($placeholder_arr, $select);
        $merge = array_replace($merge2, $select_c);
    }

    return $merge;
}

//GET SIMGAJI LOKASIS
function opd_gaji_array_($placeholder, $show_id, $status_gaji, $lokasi = NULL)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    if($status_gaji == 2){
        $ci->db->where('status_gaji',$status_gaji);
    }

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }



    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function opd_gaji_array($placeholder, $show_id, $status_gaji, $lokasi = NULL)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    // if($status_gaji == 2){
    //     $ci->db->where('status_gaji',$status_gaji);
    // }

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function opd_gaji_result($lokasi = NULL)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    return $data;
}

function opd_tanpa_cabdin_gaji_array($placeholder, $show_id, $status_gaji, $lokasi = NULL)
{
    $ci = get_instance();
    
    $ci->db->select("id, kode, name");
    $ci->db->not_like('kode', '3411');

    // if($status_gaji == 2){
    //     $ci->db->where('status_gaji',$status_gaji);
    // }

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}


function cabdin_gaji_array($placeholder, $show_id, $status_gaji, $lokasi = NULL)
{
    $ci = get_instance();
    
    $ci->db->select("id, kode, name");
    $ci->db->like('kode', '3411');

    // if($status_gaji == 2){
    //     $ci->db->where('status_gaji',$status_gaji);
    // }

    if($lokasi != NULL){
        $ci->db->where('kode',$lokasi);
    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}
    //GET SIMGAJI LOKASIS
    function opd_gaji_v2_array($placeholder, $show_id, $status_gaji, $lokasi)
    {
        $ci = get_instance();
        $lokasis = explode(',',$lokasi);

        $ci->db->select("id, kode, name");

        $datasel = array();
        foreach ($lokasis as $key => $val){
            // $ci->db->where('id', $val);
            $datasel[$key] = $val;
        }

        if($status_gaji == 2){
            $ci->db->where('status_gaji',$status_gaji);
        }


        $ci->db->where_in('id',  $datasel);

        $data = $ci->db->get("simgaji_lokasis")->result();


        if($show_id == 'id'){
            foreach ($data as $dt) {
                // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
                $select[$dt->id] = ($dt->name.' - '.$dt->kode.' - '.$dt->id);
            }
        }else{
            foreach ($data as $dt) {
                // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
                $select[$dt->kode] = ($dt->name.' - '.$dt->kode.' - '.$dt->id);
            }
        }
        $select = '';
        if($placeholder == NULL){
            $merge = $select;
        }else{
            $placeholder_arr = array('' => $placeholder);
            $merge = array_replace($placeholder_arr, $select);
        }

        return $merge;



    }


//GET SIMGAJI LOKASIS
function opd_gaji_v3_array($placeholder, $show_id, $status_gaji)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    $ci->db->where('bool_id',2);

    // if($status_gaji == 2){
    //     $ci->db->where('status_gaji',$status_gaji);
    // }

    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }



    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

//GET SIMGAJI LOKASIS
function opd_gaji_v4_array($placeholder, $show_id)
{
    $ci = get_instance();
    $ci->db->select("id, kode, name");

    $ci->db->where('bool_id',2);


    $data = $ci->db->get("simgaji_lokasis")->result();

    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name);
        }
    }



    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function opd_array_fix_array($placeholder, $show_id, $role, $lokasi){

    $ci = get_instance();
    $ci->db->select("id, kode, name");


    if($lokasi != NULL){
        if($role == 2){

            $lokasi_arr = explode(',',$lokasi);

            foreach ($lokasi_arr as $val2){
                $ci->db->or_where('kode',$val2);
            }


        }else if($role == 99){

            //untuk external

        } else if($role == 3){
          if($lokasi == '34000000'){
            $lok_disdik = substr($lokasi, 0,2);
            $ci->db->like('kode',$lok_disdik,'AFTER');
          } else {
            $ci->db->where('kode',$lokasi);
          }

        } else{
            $ci->db->where('kode',$lokasi);
        }

    }

    $ci->db->where('kode !=', '');

    $data = $ci->db->get("simgaji_lokasis")->result();

    $select = array();
    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;

}

// SELECT * FROM simgaji_lokasis WHERE kode LIKE '%000000';

function opd_induk_array_tpp($placeholder, $show_id){

    $ci = get_instance();
    // $ci->db->select("id, kode, name");
    // $ci->db->like('kode','%000000');
    // $ci->db->where('bool_id',2);
    // $ci->db->where('kode !=', '');
    // $data = $ci->db->get("simgaji_lokasis")->result();

    $data =$ci->db->query("SELECT * FROM simgaji_lokasis WHERE kode LIKE '%000000'")->result();
    $select = array();
    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function opd_array_tpp($placeholder, $show_id){

    $ci = get_instance();
    $ci->db->select("id, kode, name");
    $ci->db->where('bool_id',2);
    $ci->db->where('kode !=', '');
    $data = $ci->db->get("simgaji_lokasis")->result();
    $select = array();
    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function golongan_id_array($placeholder){

    $ci = get_instance();
    $data = $ci->db->get("simgaji_golongans")->result();
    $select = array();
    foreach ($data as $dt) {
        // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
        // $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        $select[$dt->id] = ($dt->name.' ('.$dt->kode.')');
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function golongan_nama($placeholder){

    $ci = get_instance();
    $data = $ci->db->get("simgaji_golongans WHERE id = '$placeholder'");
    if($data->num_rows() > 0){
         $row = $data->row();
          $echo = $row->kode.'/'.$row->name;
        //  $echo = $row->name;
    }else{
        $echo = '-';
    }

    return $echo;
}

function eselon_nama($placeholder){

    $ci = get_instance();
    $data = $ci->db->get("simgaji_eselons WHERE id = '$placeholder'");
    if($data->num_rows() > 0){
         $row = $data->row();
        //  $echo = $row->name.'-'.$row->kode;
         $echo = $row->name;
    }else{
        $echo = '-';
    }

    return $echo;
}

function eselon_id_array($placeholder){

    $ci = get_instance();
    $data = $ci->db->get("simgaji_eselons")->result();
    $select = array();
    foreach ($data as $dt) {
        // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
        // $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        $select[$dt->id] = ($dt->name);
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function unit_array_tpp($placeholder, $show_id){

    $ci = get_instance();
    $ci->db->select("id, kode, name");
    $ci->db->where('kode !=', '');
    $data = $ci->db->get("simgaji_lokasis")->result();
    $select = array();
    if($show_id == 'id'){
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->id] = ($dt->name.' - '.$dt->kode);
        }
    }else{
        foreach ($data as $dt) {
            // $select[$dt->kode] = ($dt->name .'-'.$dt->kode);
            $select[$dt->kode] = ($dt->name.' - '.$dt->kode);
        }
    }

    // return $select;

    if($placeholder == NULL){
        $merge = $select;
    }else{
        $placeholder_arr = array('' => $placeholder);
        $merge = array_replace($placeholder_arr, $select);
    }

    return $merge;
}

function status_acc($sts){

    switch ($sts) {

        case '0':
            $icon = '<i  class="icon-cog"></i>';
            break;
        case '2':
            $icon = '<i style="color:orange" class="icon-backward">Buka / Revisi</i>';
            break;
        case '1':
            $icon = '<i style="color:green" class="icon-checkmark-circle">Acc</i>';
            break;
        case '3':
            $icon = '<i style="color:red" class="icon-cancel-circle2">Tolak</i>';
            break;
        default:
            $icon = "-";
            break;


    }

    return $icon;


}

function status_array($placeholder)
{
    // 1=Ah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'setuju',
        '2' => 'buka/revisi',
        '3' => 'tolak'
    );
    return $arr;
}


function status_array2($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'setuju',

        '3' => 'tolak'
    );
    return $arr;
}

function statusverif_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'buka',
        '2' => 'tutup',
    );
    return $arr;
}

function ya_tidak_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        // ''  => $placeholder,
        '' => 'Tidak',
        '1' => 'Ya',
        '0' => 'Tidak',
    );
    return $arr;
}

function ya_tidak_lama_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        // ''  => $placeholder,
        '' => 'Tidak',
        '1' => 'Ya',
        '2' => 'Tidak',
    );
    return $arr;
}

function ya_tidak_nama($sts){

    switch ($sts) {
        case '':
            $icon = 'Tidak';
            break;
        case '0':
            $icon = 'Tidak';
            break;
        case '1':
            $icon = 'Ya';
            break;
        default:
            $icon = "-";
            break;
    }

    return $icon;


}

function stop_tidak_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '2' => 'Ya',
        '1' => 'Tidak',
    );
    return $arr;
}

function stop_tidak_nama($sts){
    switch ($sts) {
        case '2':
            $icon = 'Ya';
            break;
        case '1':
            $icon = 'Tidak';
            break;
        default:
            $icon = "-";
            break;
    }
    return $icon;
}

function statusverif_nama($sts){

    switch ($sts) {

        case '2':
            $icon = 'Tutup';
            break;
        case '1':
            $icon = 'Buka';
            break;
        default:
            $icon = "-";
            break;
    }

    return $icon;


}

function statusdata_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'Kalkulasi',
        '2' => 'Buka/Revisi',
        '3' => 'Fixed',
        '4' => 'Cetak',
    );
    return $arr;
}

function statusdata_nama($sts){

    switch ($sts) {

        case '1':
            $icon = 'Kalkulasi';
            break;
        case '2':
            $icon = 'Buka/Revisi';
            break;
        case '3':
            $icon = 'Fixed';
            break;
        case '4':
            $icon = 'Cetak';
            break;
        default:
            $icon = "-";
            break;
    }

    return $icon;

}

function statusdata_show($sts){

    switch ($sts) {

        case '1':
            $icon = 'd-none';
            break;
        case '2':
            $icon = '-';
            break;
        case '3':
            $icon = 'd-none';
            break;
        case '4':
            $icon = 'd-none';
            break;
        default:
            $icon = "d-none";
            break;
    }

    return $icon;

}

function statusdata_redirect($sts){

    switch ($sts) {

        case '1':
            $icon = 'false';
            break;
        case '2':
            $icon = 'true';
            break;
        case '3':
            $icon = 'false';
            break;
        case '4':
            $icon = 'false';
            break;
        default:
            $icon = "false";
            break;
    }

    return $icon;

}

function jeniskalulasi_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        // ''  => $placeholder,
        // 'GAJI_BULANAN' => 'GAJI BULANAN',
        // 'TPP' => 'TPP',
        // 'GAJI_13' => 'GAJI 13',
        // 'GAJI_14' => 'GAJI 14',
        // 'TPP_13' => 'TPP 13',
        // 'GAJI_TAHUNAN' => 'GAJI TAHUNAN',
        // 'TPP_TAHUNAN' => 'TPP TAHUNAN',
        'GAJI_DES' => 'GAJI DES',
        'GAJI_TAHUNAN' => 'GAJI TAHUNAN',
        'TPP' => 'TPP',
        '0' => '---------- || ---------- || ---------- || ---------- || ---------- || ----------',
        'GAJI_BULANAN_PPPK' => 'GAJI BULANAN PPPK',
        'GAJI_13_PPPK' => 'GAJI 13 PPPK',
        'GAJI_14_PPPK' => 'GAJI 14 PPPK',
        'GAJI_DES_PPPK' => 'GAJI DES PPPK',
        'GAJI_TAHUNAN_PPPK' => 'GAJI TAHUNAN PPPK',
        
    );
    return $arr;
}

function jeniscetak_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'DAFTAR GAJI',
        '2' => 'REKAP GAJI PER LOKASI INDUK',
        '3' => 'REKAP GAJI PER LOKASI',

    );
    return $arr;
}

function jenissusulan_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            // 'DAFTAR GAJI' => 'DAFTAR GAJI',
            // 'REKAP GAJI PER LOKASI INDUK' => 'REKAP GAJI PER LOKASI INDUK',
            // 'REKAP GAJI PER LOKASI' => 'REKAP GAJI PER LOKASI',
            // 'REKAP GAJI KIRI KANAN' => 'REKAP GAJI KIRI KANAN',
            // 'REKAP GAJI LOKASI DETAIL' => 'REKAP GAJI LOKASI DETAIL',
            // 'REKAP GAJI SELURUH SKPD' => 'REKAP GAJI SELURUH SKPD',
            // 'REKAP BPJS KESEHATAN' => 'REKAP BPJS KESEHATAN',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            // 'REKAP BPJS JKK' => 'REKAP BPJS JKK',
            // 'REKAP BPJS JKM' => 'REKAP BPJS JKM',
            // 'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
            'DAFTAR GAJI SUSULAN' => 'DAFTAR GAJI SUSULAN'
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            // 'DAFTAR GAJI' => 'DAFTAR GAJI',
            // 'REKAP GAJI PER LOKASI INDUK' => 'REKAP GAJI PER LOKASI INDUK',
            // 'REKAP GAJI PER LOKASI' => 'REKAP GAJI PER LOKASI',
            // 'REKAP GAJI KIRI KANAN' => 'REKAP GAJI KIRI KANAN',
            // 'REKAP GAJI LOKASI DETAIL' => 'REKAP GAJI LOKASI DETAIL',
            // 'REKAP GAJI SELURUH SKPD' => 'REKAP GAJI SELURUH SKPD',
            // 'REKAP BPJS KESEHATAN' => 'REKAP BPJS KESEHATAN',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            // 'REKAP BPJS JKK' => 'REKAP BPJS JKK',
            // 'REKAP BPJS JKM' => 'REKAP BPJS JKM',
            // 'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
            'DAFTAR GAJI SUSULAN' => 'DAFTAR GAJI SUSULAN'
        );
    }

    return $arrJenis;
}

function jenistahunan_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI' => 'DAFTAR GAJI',
            'REKAP GAJI PER LOKASI INDUK' => 'REKAP GAJI PER LOKASI INDUK',
            'REKAP GAJI PER LOKASI' => 'REKAP GAJI PER LOKASI',
            'REKAP GAJI KIRI KANAN' => 'REKAP GAJI KIRI KANAN',
            'REKAP GAJI LOKASI DETAIL' => 'REKAP GAJI LOKASI DETAIL',
            // 'REKAP GAJI SELURUH SKPD' => 'REKAP GAJI SELURUH SKPD',
            'REKAP BPJS KESEHATAN' => 'REKAP BPJS KESEHATAN',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            // 'REKAP BPJS JKK' => 'REKAP BPJS JKK',
            // 'REKAP BPJS JKM' => 'REKAP BPJS JKM',
            // 'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI' => 'DAFTAR GAJI',
            'REKAP GAJI PER LOKASI INDUK' => 'REKAP GAJI PER LOKASI INDUK',
            'REKAP GAJI PER LOKASI' => 'REKAP GAJI PER LOKASI',
            'REKAP GAJI KIRI KANAN' => 'REKAP GAJI KIRI KANAN',
            'REKAP GAJI LOKASI DETAIL' => 'REKAP GAJI LOKASI DETAIL',
            'REKAP GAJI SELURUH SKPD' => 'REKAP GAJI SELURUH SKPD',
            'REKAP BPJS KESEHATAN' => 'REKAP BPJS KESEHATAN',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            'REKAP BPJS JKK' => 'REKAP BPJS JKK',
            'REKAP BPJS JKM' => 'REKAP BPJS JKM',
            'GAJI PERSONAL' => 'GAJI PERSONAL',
            'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    }

    return $arrJenis;
}

function jenisbulananp3k_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI PPPK' => 'DAFTAR GAJI PPPK',
            'REKAP GAJI PPPK PER LOKASI INDUK' => 'REKAP GAJI PPPK PER LOKASI INDUK',
            'REKAP GAJI PPPK PER LOKASI' => 'REKAP GAJI PPPK PER LOKASI',
            'REKAP GAJI PPPK KIRI KANAN' => 'REKAP GAJI PPPK KIRI KANAN',
            'REKAP GAJI PPPK LOKASI DETAIL' => 'REKAP GAJI PPPK LOKASI DETAIL',
            'REKAP GAJI PPPK SELURUH SKPD' => 'REKAP GAJI PPPK SELURUH SKPD',
            'REKAP BPJS KESEHATAN PPPK' => 'REKAP BPJS KESEHATAN PPPK',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            'REKAP BPJS JKK PPPK' => 'REKAP BPJS JKK PPPK',
            'REKAP BPJS JKM PPPK' => 'REKAP BPJS JKM PPPK',
            'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI PPPK' => 'DAFTAR GAJI PPPK',
            'REKAP GAJI PPPK PER LOKASI INDUK' => 'REKAP GAJI PPPK PER LOKASI INDUK',
            'REKAP GAJI PPPK PER LOKASI' => 'REKAP GAJI PPPK PER LOKASI',
            'REKAP GAJI PPPK KIRI KANAN' => 'REKAP GAJI PPPK KIRI KANAN',
            'REKAP GAJI PPPK LOKASI DETAIL' => 'REKAP GAJI PPPK LOKASI DETAIL',
            'REKAP GAJI PPPK SELURUH SKPD' => 'REKAP GAJI PPPK SELURUH SKPD',
            'REKAP BPJS KESEHATAN PPPK' => 'REKAP BPJS KESEHATAN PPPK',
            #'KEKURANGAN ASKES' => 'KEKURANGAN ASKES',
            #'REKAP BPJS KK' => 'REKAP BPJS KK',
            'REKAP BPJS JKK PPPK' => 'REKAP BPJS JKK PPPK',
            'REKAP BPJS JKM PPPK' => 'REKAP BPJS JKM PPPK',
            'GAJI PERSONAL' => 'GAJI PERSONAL',
            // 'REKAP PEGAWAI' => 'REKAP PEGAWAI',
            // 'REKAP TUNJANGAN UMUM' => 'REKAP TUNJANGAN UMUM',
            // 'REKAP TUNJANGAN UMUM DETAIL' => 'REKAP TUNJANGAN UMUM DETAIL',
            // 'REKAP TUNJANGAN FUNGSIONAL' => 'REKAP TUNJANGAN FUNGSIONAL',
            // 'REKAP TUNJANGAN FUNGSIONAL DETAIL' => 'REKAP TUNJANGAN FUNGSIONAL DETAIL',
            // 'REKAP TUNJANGAN STRUKTURAL' => 'REKAP TUNJANGAN STRUKTURAL',
            // 'REKAP TUNJANGAN STRUKTURAL DETAIL' => 'REKAP TUNJANGAN STRUKTURAL DETAIL',
        );
    }

    return $arrJenis;
}

function jenisgaji13_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13' => 'DAFTAR GAJI 13',
			'REKAP GAJI 13 PER LOKASI INDUK' => 'REKAP GAJI 13 PER LOKASI INDUK',
			'REKAP GAJI 13 PER LOKASI' => 'REKAP GAJI 13 PER LOKASI',
			'REKAP GAJI 13 KIRI KANAN' => 'REKAP GAJI 13 KIRI KANAN',
			// 'REKAP GAJI 13 SELURUH SKPD' => 'REKAP GAJI 13 SELURUH SKPD',
			'DAFTAR BPJS KESEHATAN 13' => 'DAFTAR BPJS KESEHATAN 13',
			#'KEKURANGAN ASKES 13' => 'KEKURANGAN ASKES 13',
			#'REKAP BPJS KK 13' => 'REKAP BPJS KK 13',
			// 'REKAP BPJS JKK 13' => 'REKAP BPJS JKK 13',
			// 'REKAP BPJS JKM 13' => 'REKAP BPJS JKM 13',
			// 'GAJI PERSONAL 13' => 'GAJI PERSONAL 13',
			// 'REKAP PEGAWAI 13' => 'REKAP PEGAWAI 13',
			// 'REKAP GAJI 13 UMUM' => 'REKAP GAJI 13 UMUM',
			// 'REKAP GAJI 13 UMUM DETAIL' => 'REKAP GAJI 13 UMUM DETAIL',
			// 'REKAP GAJI 13 FUNGSIONAL' => 'REKAP GAJI 13 FUNGSIONAL',
			// 'REKAP GAJI 13 FUNGSIONAL DETAIL' => 'REKAP GAJI 13 FUNGSIONAL DETAIL',
			// 'REKAP GAJI 13 STRUKTURAL' => 'REKAP GAJI 13 STRUKTURAL',
			// 'REKAP GAJI 13 STRUKTURAL DETAIL' => 'REKAP GAJI 13 STRUKTURAL DETAIL',
			// 'REKAP GAJI 13 LOKASI DETAIL' => 'REKAP GAJI 13 LOKASI DETAIL',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13' => 'DAFTAR GAJI 13',
			'REKAP GAJI 13 PER LOKASI INDUK' => 'REKAP GAJI 13 PER LOKASI INDUK',
			'REKAP GAJI 13 PER LOKASI' => 'REKAP GAJI 13 PER LOKASI',
			'REKAP GAJI 13 KIRI KANAN' => 'REKAP GAJI 13 KIRI KANAN',
			'REKAP GAJI 13 SELURUH SKPD' => 'REKAP GAJI 13 SELURUH SKPD',
			'DAFTAR BPJS KESEHATAN 13' => 'DAFTAR BPJS KESEHATAN 13',
			#'KEKURANGAN ASKES 13' => 'KEKURANGAN ASKES 13',
			#'REKAP BPJS KK 13' => 'REKAP BPJS KK 13',
			'REKAP BPJS JKK 13' => 'REKAP BPJS JKK 13',
			'REKAP BPJS JKM 13' => 'REKAP BPJS JKM 13',
			'GAJI PERSONAL 13' => 'GAJI PERSONAL 13',
			'REKAP PEGAWAI 13' => 'REKAP PEGAWAI 13',
			'REKAP GAJI 13 UMUM' => 'REKAP GAJI 13 UMUM',
			'REKAP GAJI 13 UMUM DETAIL' => 'REKAP GAJI 13 UMUM DETAIL',
			'REKAP GAJI 13 FUNGSIONAL' => 'REKAP GAJI 13 FUNGSIONAL',
			'REKAP GAJI 13 FUNGSIONAL DETAIL' => 'REKAP GAJI 13 FUNGSIONAL DETAIL',
			'REKAP GAJI 13 STRUKTURAL' => 'REKAP GAJI 13 STRUKTURAL',
			'REKAP GAJI 13 STRUKTURAL DETAIL' => 'REKAP GAJI 13 STRUKTURAL DETAIL',
			'REKAP GAJI 13 LOKASI DETAIL' => 'REKAP GAJI 13 LOKASI DETAIL',
        );
    }

    return $arrJenis;
}

function jenisgaji13p3k_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13 PPPK' => 'DAFTAR GAJI 13 PPPK',
			'REKAP GAJI 13 PPPK PER LOKASI INDUK' => 'REKAP GAJI 13 PPPK PER LOKASI INDUK',
			'REKAP GAJI 13 PPPK PER LOKASI' => 'REKAP GAJI 13 PPPK PER LOKASI',
			'REKAP GAJI 13 PPPK KIRI KANAN' => 'REKAP GAJI 13 PPPK KIRI KANAN',
			'REKAP GAJI 13 PPPK SELURUH SKPD' => 'REKAP GAJI 13 PPPK SELURUH SKPD',
			'REKAP GAJI 13 PPPK LOKASI DETAIL' => 'REKAP GAJI 13 PPPK LOKASI DETAIL',
            'GAJI PERSONAL 13' => 'GAJI PERSONAL 13',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 13 PPPK' => 'DAFTAR GAJI 13 PPPK',
			'REKAP GAJI 13 PPPK PER LOKASI INDUK' => 'REKAP GAJI 13 PPPK PER LOKASI INDUK',
			'REKAP GAJI 13 PPPK PER LOKASI' => 'REKAP GAJI 13 PPPK PER LOKASI',
			'REKAP GAJI 13 PPPK KIRI KANAN' => 'REKAP GAJI 13 PPPK KIRI KANAN',
			'REKAP GAJI 13 PPPK SELURUH SKPD' => 'REKAP GAJI 13 PPPK SELURUH SKPD',
			'REKAP GAJI 13 PPPK LOKASI DETAIL' => 'REKAP GAJI 13 PPPK LOKASI DETAIL',
            'GAJI PERSONAL 13' => 'GAJI PERSONAL 13',
        );
    }

    return $arrJenis;
}

function jenisgaji14p3k_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 14 PPPK' => 'DAFTAR GAJI 14 PPPK',
			'REKAP GAJI 14 PPPK PER LOKASI INDUK' => 'REKAP GAJI 14 PPPK PER LOKASI INDUK',
			'REKAP GAJI 14 PPPK PER LOKASI' => 'REKAP GAJI 14 PPPK PER LOKASI',
			'REKAP GAJI 14 PPPK KIRI KANAN' => 'REKAP GAJI 14 PPPK KIRI KANAN',
			'REKAP GAJI 14 PPPK SELURUH SKPD' => 'REKAP GAJI 14 PPPK SELURUH SKPD',
			'REKAP GAJI 14 PPPK LOKASI DETAIL' => 'REKAP GAJI 14 PPPK LOKASI DETAIL',
            'GAJI PERSONAL 14' => 'GAJI PERSONAL 14',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI 14 PPPK' => 'DAFTAR GAJI 14 PPPK',
			'REKAP GAJI 14 PPPK PER LOKASI INDUK' => 'REKAP GAJI 14 PPPK PER LOKASI INDUK',
			'REKAP GAJI 14 PPPK PER LOKASI' => 'REKAP GAJI 14 PPPK PER LOKASI',
			'REKAP GAJI 14 PPPK KIRI KANAN' => 'REKAP GAJI 14 PPPK KIRI KANAN',
			'REKAP GAJI 14 PPPK SELURUH SKPD' => 'REKAP GAJI 14 PPPK SELURUH SKPD',
			'REKAP GAJI 14 PPPK LOKASI DETAIL' => 'REKAP GAJI 14 PPPK LOKASI DETAIL',
            'GAJI PERSONAL 14' => 'GAJI PERSONAL 14',
        );
    }

    return $arrJenis;
}

function jenisgajiTahunanp3k_array($placeholder, $role = null){
    if($role == 'ADMIN SKPD'){
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI PPPK TAHUNAN' => 'DAFTAR GAJI PPPK TAHUNAN',
			'REKAP GAJI PPPK PER LOKASI INDUK TAHUNAN' => 'REKAP GAJI PPPK PER LOKASI INDUK TAHUNAN',
			'REKAP GAJI PPPK PER LOKASI TAHUNAN' => 'REKAP GAJI PPPK PER LOKASI TAHUNAN',
			'REKAP GAJI PPPK KIRI KANAN TAHUNAN' => 'REKAP GAJI PPPK KIRI KANAN TAHUNAN',
			'REKAP GAJI PPPK SELURUH SKPD TAHUNAN' => 'REKAP GAJI PPPK SELURUH SKPD TAHUNAN',
			'REKAP GAJI PPPK LOKASI DETAIL TAHUNAN' => 'REKAP GAJI PPPK LOKASI DETAIL TAHUNAN',
            // 'GAJI PERSONAL TAHUNAN' => 'GAJI PERSONAL TAHUNAN',
        );
    } else {
        $arrJenis = array(
            ''  => $placeholder,
            'DAFTAR GAJI PPPK TAHUNAN' => 'DAFTAR GAJI PPPK TAHUNAN',
			'REKAP GAJI PPPK PER LOKASI INDUK TAHUNAN' => 'REKAP GAJI PPPK PER LOKASI INDUK TAHUNAN',
			'REKAP GAJI PPPK PER LOKASI TAHUNAN' => 'REKAP GAJI PPPK PER LOKASI TAHUNAN',
			'REKAP GAJI PPPK KIRI KANAN TAHUNAN' => 'REKAP GAJI PPPK KIRI KANAN TAHUNAN',
			'REKAP GAJI PPPK SELURUH SKPD TAHUNAN' => 'REKAP GAJI PPPK SELURUH SKPD TAHUNAN',
			'REKAP GAJI PPPK LOKASI DETAIL TAHUNAN' => 'REKAP GAJI PPPK LOKASI DETAIL TAHUNAN',
            // 'GAJI PERSONAL TAHUNAN' => 'GAJI PERSONAL TAHUNAN',
        );
    }

    return $arrJenis;
}


function role_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'SUPERADMIN',
        '2' => 'VERIFIKATOR',
        '3' => 'EXTERNAL',


    );
    return $arr;
}

function status_pegawai_tpp_array($placeholder)
{
    // 1=Ayah, 2=Ibu, 3=Suami, 4=Istri, 5=Anak
    $arr = array(
        ''  => $placeholder,
        '1' => 'PNS',
        '2' => 'CPNS',
        '3' => 'PNS DIPERBANTUKAN',


    );
    return $arr;
}

function jenis_jabatan()
{
    $arr = array(
        'id' => '1',
        'name'=> 'STRUKTURAL'
    );

    $arr .= array(
        'id' => '2',
        'name'=> 'JFT'
    );

    $arr .= array(
        'id' => '2',
        'name'=> 'JF0'
    ); 

    $arr['data'] = $arr;
    return $arr;
}

function role_data($sts){

    switch ($sts) {

        case '1':
            $icon = 'SUPERADMIN';
            break;
        case '2':
            $icon = 'VERIFIKATOR';
            break;

        default:
            $icon = "false";
            break;
    }

    return $icon;

}

function jenis_susulan($sts){

    switch ($sts) {

        case '1':
            $icon = 'GAJI_POKOK';
            break;
        case '2':
            $icon = 'FUNGSIONAL';
            break;

        default:
            $icon = "-";
            break;
    }

    return $icon;

}

function array_ka_waka($val){
    $arr = array(
        '1300000000',
        '2500000000',
        '8000000000',
        '8000100000',
        '8000200000',
        '8000300000',
        '8100000000',
        '8100100000',
        '8100200000',
        '8100300000',
        '8200000000',
        '8200100000',
        '8200200000',
        '8300000000',
        '8400000000',
        '8400100000',
        '8400200000',
        '8500000000',
        '8500100000',
        '8500200000',
        '8600000000',
        'A100300000',
        'A100600000',
        'A100800000',
        'A200000000',
        'A200100000',
        'A200110000',
        'A200120000',
        'A200130000',
        'A200200000',
        'A200210000',
        'A200220000',
        'A200240000',
        'A200300000',
        'A200310000',
        'A200320000',
        'A200340000'
    );

    if (in_array($val, $arr)){
        return true;
    }else{
        return false;
    }

}

function beban_kerja_khusus($val){
    $arr = array(
        '43000000', //badan penghubung
        '02040390', //kabag tu dan pimpinan
        'C1', //kode simpeg badan penghubung
        'A200323000', //kode simpeg kabag tu dan pimpinan
        'A200323' //kode simpeg kabag tu dan pimpinan
    );
    if (in_array($val, $arr)){
        return true;
    }else{
        return false;
    }
}

function tempat_kerja($val){
    $arr = array(
        '34112610', //sma kampung laut
        '3996',
        '34110917', //sma karimun jawa
        '3678',
        '30020000', //upt pelabuhan perikanan karimun jawa
        '2885'
    );
    if (in_array($val, $arr)){
        return true;
    }else{
        return false;
    }
}

function kondisi_kerja($val){
    $arr = array(
        // '50676869', //RSJ
        '50000000', //RSJ
        '67000000', //RSJ
        '68000000', //RSJ
        '69000000', //RSJ
        '02020250', //Biro APBJ
        '3411%41', //slb,
        //dinsos panti2
        '21010%', '21010100','21010200','21010300','21010400',
        '21020%', '21020100','21020200',
        '21040%', '21040100','21040200','21040300','21040400',
        '21060%', '21060100',
        '21080%', '21080100','21080200',
        '21090%', '21090100','21090200',
        '21100%', '21100100','21100200','21100300',
        '21110%', '21110100',
        '21120%', '21120400',
        '21130%', '21130200',
        '21150%', '21150100',
        '21160%', '21150100',
        '21170%', '21150100',
        '21210%', '21150100','21150300','21150400',
        '21250%', '21250100',
        '21260%', '21260100','21260200',
        '21280%', '21280100',
        '21290%', '21290100',
        '21320%', '21320200','21320300','21320400',
        //dinoso panti2 end

    );

    if (in_array($val, $arr)){
        return true;
    }else{
        return false;
    }
}


function pengelompokan_golongan($gol){

    if($gol >= 11 && $gol <= 14){
        $g = 1;
    }else if($gol >= 21 && $gol <= 24){
        $g = 2;
    }else if($gol >= 31 && $gol <= 34){
        $g = 3;
    }else if($gol >= 41 && $gol <= 45){
        $g = 4;
    }else{
        $g = 1;
    }

    return $g;

}







?>
