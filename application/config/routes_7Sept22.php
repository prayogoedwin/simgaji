<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'user';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['api/get_takehomepay'] = "api/layanan_gaji/gaji_takehomepay";

//bank jateng
$route['api/v1/bankjateng/auth'] = "api/Wsbankjtg/action_auth";
$route['api/v1/bankjateng/gajiasn'] = "api/Wsbankjtg/gaji_asn";
$route['api/v1/bankjateng/gaji13'] = "api/Wsbankjtg/gaji_asn_13";
$route['api/v1/bankjateng/gajisusulan'] = "api/Wsbankjtg/gaji_susulan";
$route['api/v1/bankjateng/gajitpp'] = "api/Wsbankjtg/gaji_tpp";
$route['api/v1/bankjateng/gaji14'] = "api/Wsbankjtg/gaji_asn_14";

//disdik
$route['api/v1/disdik/gajiasn'] = "api/Wsdisdik/gaji_asn";
$route['api/v1/disdik/gajitpp'] = "api/Wsdisdik/gaji_tpp";

//bpjs
$route['api/v1/bpjs/auth'] = "api/Wsbpjs/action_auth";
$route['api/v1/bpjs/gajiasn'] = "api/Wsbpjs/gaji_asn";
$route['api/v1/bpjs/gajisusulan'] = "api/Wsbpjs/gaji_susulan";
$route['api/v1/bpjs/gajitpp'] = "api/Wsbpjs/gaji_tpp";

//bpkad
$route['api/v1/bpkad/auth'] = "api/Wsbpkad/action_auth";
$route['api/v1/bpkad/gajiasn'] = "api/Wsbpkad/gaji_asn";
$route['api/v1/bpkad/gaji14'] = "api/Wsbpkad/gaji_asn_14";