<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pot_pegawai_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        // $this->tb_gajipokok = 'simgaji_gajip3ks';

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDataPegawaiByKode($kode_lokasis, $kolok_simpeg, $periodetpp){
        $kode4digit = substr($kode_lokasis, 0, 4) . '%';
        $kolok2digit = substr($kolok_simpeg, 0, 2);

        $pecahkan = explode('-',$periodetpp);
        $bul2blkg = $pecahkan[1] - 2;
        $bul = $pecahkan[1];
        $thn = $pecahkan[0];
        $par_tgl = $thn . '-' . str_pad($bul2blkg,2,"0",STR_PAD_LEFT) . '-01';
        $par_tgl_asli = $thn . '-' . $bul . '-01';
        $blth2blkg = $thn . str_pad($bul2blkg,2,"0",STR_PAD_LEFT);

        $tahunlhk = $thn;
        $bulanlhk = $bul - 1;
        if($bul == '1'){
            $tahunlhk = $thn - 1;
            $bulanlhk = '12';
        }

        $r = explode('-',$periodetpp);
        
        $sql = 
        "SELECT 
        CONCAT(B_03A,IF (B_03A!='','. ',''),B_03,IF(B_03B!='',', ',''),B_03B) nama,mm.B_02B,mm.F_03,mm.I_5A,mm.I_05,mm.I_JB,mm.K_01,mm.kelasjab,mm.spesialis,mm.tendik,mm.RUMPUN,
        CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) unor, hd.*, IF(tb.NIP IS NULL, 0, 1) tb, pres.alpha, FORMAT(FLOOR(pres.kwk/60)+(MOD(kwk,60)/60),2) kwk,
        TIMESTAMPDIFF(MONTH,CONCAT(LEFT(DATE_ADD(k.TMTHKM,INTERVAL 2 MONTH),7),'-01'),'$par_tgl') <=12 AND YEAR(TMTHKM)>='2018'
        AND COUNT(ik.nip) < CASE WHEN hd.tingkat=3 THEN 12 WHEN hd.tingkat=2 THEN 6 WHEN hd.tingkat=1 THEN 3 ELSE 0 END kenahk, 
        if(lhkpn.nip is null,0,10) as lhkpn,
        if(lhkasn.nip is null,0,10) as lhkasn,
        if(tptgr.nip is null,0,10) as tptgr,
        bmd.lokasi is not null AND COUNT(ib.nip) < 3 kenabmd,
		grat.nip is not null AND COUNT(ig.nip) < 3 kenagrat,
        COUNT(ib.nip) xbmd,
		COUNT(ig.nip) xgrat,

        CASE WHEN p3kdr.nominal IS NOT NULL THEN p3kdr.nominal
        WHEN p3kkes.nominal IS NOT NULL THEN p3kkes.nominal
        when p3k.nominal is not null then p3k.nominal
        WHEN kasek.nominal IS NOT NULL THEN kasek.nominal
        WHEN tusek.nominal IS NOT NULL THEN tusek.nominal
        WHEN pws.nominal IS NOT NULL THEN pws.nominal
        WHEN jpt.nominal IS NOT NULL THEN jpt.nominal
        WHEN tendik.nominal IS NOT NULL THEN tendik.nominal
        WHEN str.nominal IS NOT NULL THEN str.nominal
        WHEN setara.nominal IS NOT NULL THEN setara.nominal
        ELSE gol.nominal END tpp, beban.nominal beban,
        CASE WHEN gurutmp.nominal IS NOT NULL THEN gurutmp.nominal
        WHEN tusektmp.nominal IS NOT NULL THEN tusektmp.nominal
        WHEN tendiktmp.nominal IS NOT NULL THEN tendiktmp.nominal
        ELSE tempat.nominal END tempat, CASE WHEN gurukon.nominal IS NOT NULL
        THEN gurukon.nominal WHEN tusekkon.nominal IS NOT NULL THEN tusekkon.nominal
        WHEN tendikkon.nominal IS NOT NULL THEN tendikkon.nominal ELSE kondisi.nominal END kondisi
        FROM bkd_gaji_dev.simgaji_pegawai_tpp ptpp, ( SELECT m.*, CASE WHEN I_5A='1' THEN l.KELAS_1 WHEN I_5A='2' THEN f2.KELAS ELSE u.KELAS END kelasjab, sp.nip, IF (sp.nip IS NULL,0,1) spesialis, IF ((m.A_01='D0' AND m.A_02!='00' AND m.A_04>'30') OR (I_5A='2' AND I_05 IN ('00018','00053')),2,0) tendik, f.RUMPUN
        FROM MASTFIP08 m 
        LEFT JOIN TABFNG1 f ON m.I_05=f.KFUNG 
        LEFT JOIN TABLOKB08 l ON m.I_05=l.KOLOK
        LEFT JOIN petafngsapk p ON m.I_05=p.KFUNG AND m.I_07=p.KJENJANG
        LEFT JOIN TABFNG2 f2 ON p.CEPAT_KODE=f2.CEPAT_KODE
        LEFT JOIN TABJFU15 u ON m.I_05=u.KOJFU
        LEFT JOIN presensi2021.spesialis sp ON m.B_02B=sp.nip
        WHERE m.A_01 NOT IN (98,99) ) mm

        left join TABREFTPPKELASJAB p3kdr on p3kdr.jenisasn=mm.B_09 and p3kdr.rumpun=mm.RUMPUN and p3kdr.kdjab=mm.I_05 and p3kdr.spesialis=mm.spesialis
        left join TABREFTPPKELASJAB p3kkes on p3kkes.jenisasn=mm.B_09 and p3kkes.rumpun=mm.RUMPUN and p3kkes.kdjab=0
        left join TABREFTPPKELASJAB p3k on p3k.jenisasn=mm.B_09 and p3k.rumpun=mm.RUMPUN and p3k.rumpun!=1
        LEFT JOIN TABREFTPPKELASJAB kasek ON kasek.kdjab=mm.I_05 AND mm.tendik=2 AND kasek.koord=mm.K_01
        AND mm.I_05='00018' AND mm.I_5A=2 AND mm.K_01=1 LEFT JOIN TABREFTPPKELASJAB tusek ON mm.I_05 LIKE tusek.kdjab
        AND mm.tendik=2 AND I_5A=1 LEFT JOIN TABREFTPPKELASJAB pws ON pws.kdjab=mm.I_05 AND pws.jenjang=mm.I_07
        AND mm.I_05='00053' AND mm.I_5A=2 LEFT JOIN TABREFTPPKELASJAB tendik ON tendik.spesialis=mm.tendik
        AND tendik.kelas=kelasjab LEFT JOIN TABREFTPPKELASJAB jpt ON jpt.kelas=kelasjab AND jpt.kdjab=mm.I_05
        LEFT JOIN TABREFTPPKELASJAB str ON str.kelas=kelasjab AND str.esel=LEFT(mm.I_06,1) AND mm.tendik=0
        LEFT JOIN TABREFTPPKELASJAB setara ON setara.kelas=kelasjab AND setara.koord=mm.K_01 AND mm.tendik=0
        LEFT JOIN TABREFTPPKELASJAB gol ON gol.kelas=kelasjab AND gol.gol=LEFT(F_03,1) AND mm.tendik=0
        LEFT JOIN TABREFTPPKELASJAB_BEBAN beban ON beban.kelas=kelasjab
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE beban.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT gurutmp ON gurutmp.kdjab=mm.I_05 AND mm.tendik=2
        AND gurutmp.koord=IFNULL(mm.K_01,0) AND mm.I_05='00018' AND mm.I_5A=2
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE gurutmp.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT tusektmp ON mm.I_05 LIKE tusektmp.kdjab AND mm.tendik=2 AND I_5A=1
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tusektmp.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT tendiktmp ON tendiktmp.spesialis=mm.tendik AND mm.tendik=2
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tendiktmp.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT tempat ON tempat.kelas=kelasjab
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tempat.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI gurukon ON gurukon.kdjab=mm.I_05 AND mm.tendik=2
        AND gurukon.koord=IFNULL(mm.K_01,0) AND mm.I_05='00018' AND mm.I_5A=2
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE gurukon.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI tusekkon ON mm.I_05 LIKE tusekkon.kdjab AND mm.tendik=2 AND I_5A=1
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tusekkon.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI tendikkon ON tendikkon.spesialis=mm.tendik AND mm.tendik=2 AND
        CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tendikkon.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI kondisi ON kondisi.kelas=kelasjab
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE kondisi.unor
        LEFT JOIN KOMP_KASUS k ON mm.B_02=k.NIP
        AND TMTHKM=(SELECT MAX(TMTHKM) FROM KOMP_KASUS k,gaji.masttpp_kinerja tpp
        WHERE k.NIP=mm.B_02 AND mm.B_02B=tpp.nip HAVING MIN(STR_TO_DATE(CONCAT('01,',bulan,',',tahun),'%d,%m,%Y')) < k.TMTHKM)
        AND k.TMTHKM <= '$par_tgl_asli'
        LEFT JOIN TABHKDIS hd ON k.JENISHKM=hd.hkdis
        LEFT JOIN MSTTGSBLJR tb ON mm.B_02=tb.nip AND tb.TGL_MULAI <= '$par_tgl'
        AND CASE WHEN IFNULL(TG_TMT_BERHENTI,'0000-00-00')<>'0000-00-00' THEN TG_TMT_BERHENTI
        WHEN IFNULL(TGL_SELESAI_PANJANG_2,'0000-00-00')<>'0000-00-00' THEN TGL_SELESAI_PANJANG_2
        WHEN IFNULL(TGL_SELESAI_PANJANG_1,'0000-00-00')<>'0000-00-00' THEN TGL_SELESAI_PANJANG_1
        ELSE TGL_SELESAI END >= LAST_DAY('$par_tgl')
        LEFT JOIN presensi2021.presensi pres ON pres.nip = mm.B_02B AND tahun_bulan = '$blth2blkg'
        LEFT JOIN gaji.masttpp_kinerja ik ON mm.B_02B=ik.nip AND CONCAT(ik.tahun,'-',ik.bulan,'-01')>(SELECT COALESCE(MAX(TMTHKM),'9999-12-31') FROM KOMP_KASUS WHERE NIP=mm.B_02 AND YEAR(TMTHKM)>='2018') AND ik.hukdis<'100'
        
        LEFT JOIN masttpp_lhkpn lhkpn on lhkpn.nip=mm.B_02B and mm.I_5A=1 and date(lhkpn.waktu)>='".$tahunlhk."-".$bulanlhk."-01' and date(lhkpn.waktu)<='".$tahunlhk."-".$bulanlhk."-08'
        LEFT JOIN masttpp_lhkasn lhkasn on lhkasn.nip=mm.B_02B and mm.I_5A<>1 and date(lhkasn.waktu)>='".$tahunlhk."-".$bulanlhk."-01' and date(lhkasn.waktu)<='".$tahunlhk."-".$bulanlhk."-08'
        LEFT JOIN masttpp_tptgr tptgr on tptgr.nip=mm.B_02B and date_add(tptgr.tgl_berlaku,INTERVAL 2 YEAR)<'".$r[0]."-".$r[1]."-01' and status=1
        LEFT JOIN masttpp_gratifikasi grat on grat.nip=mm.B_02B and concat(grat.tahun,'-',grat.bulan,'-01')<'".$r[0]."-".$r[1]."-01'
        LEFT JOIN masttpp_bmd bmd on bmd.lokasi=mm.I_05 and mm.I_5A='1' and mm.A_01<>'99' and year(bmd.tgl_berlaku)='".$r[0]."' and '3'<'".intval($r[1])."' and bmd.status=1
        left join gaji.masttpp_kinerja ib ON mm.B_02B=ib.nip AND ib.tahun='".$r[0]."' AND ib.bmd<>'0'
		left join gaji.masttpp_kinerja ig ON mm.B_02B=ig.nip AND ig.tahun='".$r[0]."' AND ig.grat<>'0'

        WHERE mm.A_01 NOT IN (98,99)
        AND mm.A_01= ?
        AND ptpp.nip = mm.B_02B
        AND ptpp.tpp_stop != '2'
        AND mm.B_09 != '3'
        GROUP BY mm.B_02
        ORDER BY mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05,I_06,F_03 DESC";

        $query = $this->dbeps->query($sql, array($kolok2digit));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataPegawaiKhususDisdikByKode($kd, $periodetpp){
        
        $qkolok = "";
        $panjang = strlen($kd);
        if($panjang == 6){
            $A_01 = substr($kd, 0, 2);
            $A_02 = substr($kd, 2, 2);
            $A_03 = substr($kd, 4, 2);
            $qkolok = "AND mm.A_01 = '$A_01' AND mm.A_02 = '$A_02' AND mm.A_03 = '$A_03'";

        } else if($panjang == 8){
            $A_01 = substr($kd, 0, 2);
            $A_02 = substr($kd, 2, 2);
            $A_03 = substr($kd, 4, 2);
            $A_04 = substr($kd, 6, 2);
            $qkolok = "AND mm.A_01 = '$A_01' AND mm.A_02 = '$A_02' AND mm.A_03 = '$A_03' AND mm.A_04 = '$A_04'";
        }


        $pecahkan = explode('-',$periodetpp);
        $bul2blkg = $pecahkan[1] - 2;
        $bul = $pecahkan[1];
        $thn = $pecahkan[0];
        $par_tgl = $thn . '-' . str_pad($bul2blkg,2,"0",STR_PAD_LEFT) . '-01';
        $par_tgl_asli = $thn . '-' . $bul . '-01';
        $blth2blkg = $thn . str_pad($bul2blkg,2,"0",STR_PAD_LEFT);

        $tahunlhk = $thn;
        $bulanlhk = $bul - 1;
        if($bul == '1'){
            $tahunlhk = $thn - 1;
            $bulanlhk = '12';
        }

        $r = explode('-',$periodetpp);
        
        $sql = 
        "SELECT 
        CONCAT(B_03A,IF (B_03A!='','. ',''),B_03,IF(B_03B!='',', ',''),B_03B) nama,mm.B_02B,mm.F_03,mm.I_5A,mm.I_05,mm.I_JB,mm.K_01,mm.kelasjab,mm.spesialis,mm.tendik,mm.RUMPUN,
        CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) unor, hd.*, IF(tb.NIP IS NULL, 0, 1) tb, pres.alpha, FORMAT(FLOOR(pres.kwk/60)+(MOD(kwk,60)/60),2) kwk,
        TIMESTAMPDIFF(MONTH,CONCAT(LEFT(DATE_ADD(k.TMTHKM,INTERVAL 2 MONTH),7),'-01'),'$par_tgl') <=12 AND YEAR(TMTHKM)>='2018'
        AND COUNT(ik.nip) < CASE WHEN hd.tingkat=3 THEN 12 WHEN hd.tingkat=2 THEN 6 WHEN hd.tingkat=1 THEN 3 ELSE 0 END kenahk, 
        if(lhkpn.nip is null,0,10) as lhkpn,
        if(lhkasn.nip is null,0,10) as lhkasn,
        if(tptgr.nip is null,0,10) as tptgr,
        bmd.lokasi is not null AND COUNT(ib.nip) < 3 kenabmd,
		grat.nip is not null AND COUNT(ig.nip) < 3 kenagrat,
        COUNT(ib.nip) xbmd,
		COUNT(ig.nip) xgrat,

        CASE WHEN p3kdr.nominal IS NOT NULL THEN p3kdr.nominal
        WHEN p3kkes.nominal IS NOT NULL THEN p3kkes.nominal
        when p3k.nominal is not null then p3k.nominal
        WHEN kasek.nominal IS NOT NULL THEN kasek.nominal
        WHEN tusek.nominal IS NOT NULL THEN tusek.nominal
        WHEN pws.nominal IS NOT NULL THEN pws.nominal
        WHEN jpt.nominal IS NOT NULL THEN jpt.nominal
        WHEN tendik.nominal IS NOT NULL THEN tendik.nominal
        WHEN str.nominal IS NOT NULL THEN str.nominal
        WHEN setara.nominal IS NOT NULL THEN setara.nominal
        ELSE gol.nominal END tpp, beban.nominal beban,
        CASE WHEN gurutmp.nominal IS NOT NULL THEN gurutmp.nominal
        WHEN tusektmp.nominal IS NOT NULL THEN tusektmp.nominal
        WHEN tendiktmp.nominal IS NOT NULL THEN tendiktmp.nominal
        ELSE tempat.nominal END tempat, CASE WHEN gurukon.nominal IS NOT NULL
        THEN gurukon.nominal WHEN tusekkon.nominal IS NOT NULL THEN tusekkon.nominal
        WHEN tendikkon.nominal IS NOT NULL THEN tendikkon.nominal ELSE kondisi.nominal END kondisi
        FROM bkd_gaji_dev.simgaji_pegawai_tpp ptpp, ( SELECT m.*, CASE WHEN I_5A='1' THEN l.KELAS_1 WHEN I_5A='2' THEN f2.KELAS ELSE u.KELAS END kelasjab, sp.nip, IF (sp.nip IS NULL,0,1) spesialis, IF ((m.A_01='D0' AND m.A_02!='00' AND m.A_04>'30') OR (I_5A='2' AND I_05 IN ('00018','00053')),2,0) tendik, f.RUMPUN
        FROM MASTFIP08 m 
        LEFT JOIN TABFNG1 f ON m.I_05=f.KFUNG 
        LEFT JOIN TABLOKB08 l ON m.I_05=l.KOLOK
        LEFT JOIN petafngsapk p ON m.I_05=p.KFUNG AND m.I_07=p.KJENJANG
        LEFT JOIN TABFNG2 f2 ON p.CEPAT_KODE=f2.CEPAT_KODE
        LEFT JOIN TABJFU15 u ON m.I_05=u.KOJFU
        LEFT JOIN presensi2021.spesialis sp ON m.B_02B=sp.nip
        WHERE m.A_01 NOT IN (98,99) ) mm
        left join TABREFTPPKELASJAB p3kdr on p3kdr.jenisasn=mm.B_09 and p3kdr.rumpun=mm.RUMPUN and p3kdr.kdjab=mm.I_05 and p3kdr.spesialis=mm.spesialis
        left join TABREFTPPKELASJAB p3kkes on p3kkes.jenisasn=mm.B_09 and p3kkes.rumpun=mm.RUMPUN and p3kkes.kdjab=0
        left join TABREFTPPKELASJAB p3k on p3k.jenisasn=mm.B_09 and p3k.rumpun=mm.RUMPUN and p3k.rumpun!=1
        LEFT JOIN TABREFTPPKELASJAB kasek ON kasek.kdjab=mm.I_05 AND mm.tendik=2 AND kasek.koord=mm.K_01
        AND mm.I_05='00018' AND mm.I_5A=2 AND mm.K_01=1 LEFT JOIN TABREFTPPKELASJAB tusek ON mm.I_05 LIKE tusek.kdjab
        AND mm.tendik=2 AND I_5A=1 LEFT JOIN TABREFTPPKELASJAB pws ON pws.kdjab=mm.I_05 AND pws.jenjang=mm.I_07
        AND mm.I_05='00053' AND mm.I_5A=2 LEFT JOIN TABREFTPPKELASJAB tendik ON tendik.spesialis=mm.tendik
        AND tendik.kelas=kelasjab LEFT JOIN TABREFTPPKELASJAB jpt ON jpt.kelas=kelasjab AND jpt.kdjab=mm.I_05
        LEFT JOIN TABREFTPPKELASJAB str ON str.kelas=kelasjab AND str.esel=LEFT(mm.I_06,1) AND mm.tendik=0
        LEFT JOIN TABREFTPPKELASJAB setara ON setara.kelas=kelasjab AND setara.koord=mm.K_01 AND mm.tendik=0
        LEFT JOIN TABREFTPPKELASJAB gol ON gol.kelas=kelasjab AND gol.gol=LEFT(F_03,1) AND mm.tendik=0
        LEFT JOIN TABREFTPPKELASJAB_BEBAN beban ON beban.kelas=kelasjab
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE beban.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT gurutmp ON gurutmp.kdjab=mm.I_05 AND mm.tendik=2
        AND gurutmp.koord=IFNULL(mm.K_01,0) AND mm.I_05='00018' AND mm.I_5A=2
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE gurutmp.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT tusektmp ON mm.I_05 LIKE tusektmp.kdjab AND mm.tendik=2 AND I_5A=1
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tusektmp.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT tendiktmp ON tendiktmp.spesialis=mm.tendik AND mm.tendik=2
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tendiktmp.unor
        LEFT JOIN TABREFTPPKELASJAB_TEMPAT tempat ON tempat.kelas=kelasjab
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tempat.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI gurukon ON gurukon.kdjab=mm.I_05 AND mm.tendik=2
        AND gurukon.koord=IFNULL(mm.K_01,0) AND mm.I_05='00018' AND mm.I_5A=2
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE gurukon.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI tusekkon ON mm.I_05 LIKE tusekkon.kdjab AND mm.tendik=2 AND I_5A=1
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tusekkon.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI tendikkon ON tendikkon.spesialis=mm.tendik AND mm.tendik=2 AND
        CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE tendikkon.unor
        LEFT JOIN TABREFTPPKELASJAB_KONDISI kondisi ON kondisi.kelas=kelasjab
        AND CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) LIKE kondisi.unor
        LEFT JOIN KOMP_KASUS k ON mm.B_02=k.NIP
        AND TMTHKM=(SELECT MAX(TMTHKM) FROM KOMP_KASUS k,gaji.masttpp_kinerja tpp
        WHERE k.NIP=mm.B_02 AND mm.B_02B=tpp.nip HAVING MIN(STR_TO_DATE(CONCAT('01,',bulan,',',tahun),'%d,%m,%Y')) < k.TMTHKM)
        AND k.TMTHKM <= '$par_tgl_asli'
        LEFT JOIN TABHKDIS hd ON k.JENISHKM=hd.hkdis
        LEFT JOIN MSTTGSBLJR tb ON mm.B_02=tb.nip AND tb.TGL_MULAI <= '$par_tgl'
        AND CASE WHEN IFNULL(TG_TMT_BERHENTI,'0000-00-00')<>'0000-00-00' THEN TG_TMT_BERHENTI
        WHEN IFNULL(TGL_SELESAI_PANJANG_2,'0000-00-00')<>'0000-00-00' THEN TGL_SELESAI_PANJANG_2
        WHEN IFNULL(TGL_SELESAI_PANJANG_1,'0000-00-00')<>'0000-00-00' THEN TGL_SELESAI_PANJANG_1
        ELSE TGL_SELESAI END >= LAST_DAY('$par_tgl')
        LEFT JOIN presensi2021.presensi pres ON pres.nip = mm.B_02B AND tahun_bulan = '$blth2blkg'
        LEFT JOIN gaji.masttpp_kinerja ik ON mm.B_02B=ik.nip AND CONCAT(ik.tahun,'-',ik.bulan,'-01')>(SELECT COALESCE(MAX(TMTHKM),'9999-12-31') FROM KOMP_KASUS WHERE NIP=mm.B_02 AND YEAR(TMTHKM)>='2018') AND ik.hukdis<'100'
        
        LEFT JOIN masttpp_lhkpn lhkpn on lhkpn.nip=mm.B_02B and mm.I_5A=1 and date(lhkpn.waktu)>='".$tahunlhk."-".$bulanlhk."-01' and date(lhkpn.waktu)<='".$tahunlhk."-".$bulanlhk."-08'
        LEFT JOIN masttpp_lhkasn lhkasn on lhkasn.nip=mm.B_02B and mm.I_5A<>1 and date(lhkasn.waktu)>='".$tahunlhk."-".$bulanlhk."-01' and date(lhkasn.waktu)<='".$tahunlhk."-".$bulanlhk."-08'
        LEFT JOIN masttpp_tptgr tptgr on tptgr.nip=mm.B_02B and date_add(tptgr.tgl_berlaku,INTERVAL 2 YEAR)<'".$r[0]."-".$r[1]."-01' and status=1
        LEFT JOIN masttpp_gratifikasi grat on grat.nip=mm.B_02B and concat(grat.tahun,'-',grat.bulan,'-01')<'".$r[0]."-".$r[1]."-01'
        LEFT JOIN masttpp_bmd bmd on bmd.lokasi=mm.I_05 and mm.I_5A='1' and mm.A_01<>'99' and year(bmd.tgl_berlaku)='".$r[0]."' and '3'<'".intval($r[1])."' and bmd.status=1
        left join gaji.masttpp_kinerja ib ON mm.B_02B=ib.nip AND ib.tahun='".$r[0]."' AND ib.bmd<>'0'
		left join gaji.masttpp_kinerja ig ON mm.B_02B=ig.nip AND ig.tahun='".$r[0]."' AND ig.grat<>'0'

        WHERE mm.A_01 NOT IN (98,99)
        $qkolok
        AND ptpp.nip = mm.B_02B
        AND ptpp.tpp_stop != '2'
        AND mm.B_09 != '3'
        GROUP BY mm.B_02
        ORDER BY mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05,I_06,F_03 DESC";

        $query = $this->dbeps->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    function get_uptdisdik(){
        $unit_kerja = 'D0';

		$this->dbeps->order_by('KOLOK','ASC');
		$this->dbeps->order_by('ESEL','ASC');
		$this->dbeps->where('AKTIF','1');
		
		$this->dbeps->like('KOLOK',$unit_kerja,'after');
		if($unit_kerja == 'D0')
		{
			$this->dbeps->select('if (A_02<>"00" and A_04>"30",CONCAT(A_01,A_02,A_03,A_04),CONCAT(A_01,A_02,A_03)) as kode,NALOK as nama');
			$in_array = array('31','32','88');
			$this->dbeps->where_in('ESEL',$in_array);
		}
        return $this->dbeps->get('TABLOKB08');
	}

    public function getRowPegawaiTpp($nip){
        $sql = 
        "SELECT *
        FROM simgaji_pegawai_tpp
        WHERE nip = ?";

        $query = $this->db->query($sql, array($nip));

        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function cekRow($nip, $bulan, $tahun){
        $sql = 
        "SELECT *
        FROM simgaji_masttpp_kinerja
        WHERE nip = ?
        AND bulan = ?
        AND tahun = ?";

        $query = $this->db->query($sql, array($nip, $bulan, $tahun));

        if($query->num_rows() > 0){
            $data = TRUE;
        } else {
            $data = FALSE;
        }

        return $data;
    }
}