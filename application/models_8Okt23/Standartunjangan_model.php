<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standartunjangan_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->eselons = 'eselons a';
        $this->umums = 'tjumums a';
        $this->umump3ks = 'tjumump3ks a';
        $this->fungsionals = 'simgaji_fungsionals a';
        $this->tj_fungsionals = 'simgaji_tjfungsionals tj';

    }


    //CRUD ESELON START//
    var $eselon_column_order = array('a.id', 'a.kode', 'a.name','a.tunjangan','a.usia'); //set column field database for datatable orderable
    var $eselon_column_search = array('a.kode', 'a.name','a.tunjangan','a.usia'); //set column field database for datatable searchable
    var $eselon_order = array('a.id' => 'asc'); // default order

    private function _get_eselon_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->eselons);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->eselon_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->eselon_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->eselon_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->eselon_order)) {
            $order = $this->eselon_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_eselon()
    {
        $this->_get_eselon_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function eselon_count_filtered()
    {
        $this->_get_eselon_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function eselon_count_all()
    {
        $this->db->from($this->eselons);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getEselonById($id){
        $sql =
        "SELECT *
        FROM $this->eselons
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
     //CRUD ESELON END//



     //CRUD TUNJANGAN UMUM START//
    var $umum_column_order = array('a.id', 'a.golongan', 'a.tunjangan'); //set column field database for datatable orderable
    var $umum_column_search = array('a.golongan','a.tunjangan'); //set column field database for datatable searchable
    var $umum_order = array('a.id' => 'asc'); // default order

    private function _get_umum_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->umums);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->umum_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->umum_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->umum_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->umum_order)) {
            $order = $this->umum_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_umum()
    {
        $this->_get_umum_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function umum_count_filtered()
    {
        $this->_get_umum_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function umum_count_all()
    {
        $this->db->from($this->umums);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getUmumById($id){
        $sql =
        "SELECT *
        FROM $this->umums
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
     //CRUD UMUM END//


     //CRUD TUNJANGAN UMUM P3K START//
    var $umump3k_column_order = array('a.id', 'a.golongan', 'a.tunjangan'); //set column field database for datatable orderable
    var $umump3k_column_search = array('a.golongan','a.tunjangan'); //set column field database for datatable searchable
    var $umump3k_order = array('a.id' => 'asc'); // default order

    private function _get_umump3k_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->umump3ks);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->umump3k_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->umump3k_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->umump3k_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->umump3k_order)) {
            $order = $this->umump3k_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_umump3k()
    {
        $this->_get_umump3k_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function umump3k_count_filtered()
    {
        $this->_get_umump3k_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function umump3k_count_all()
    {
        $this->db->from($this->umump3ks);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getUmump3kById($id){
        $sql =
        "SELECT *
        FROM $this->umump3ks
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
     //CRUD UMUM P3K END//


     var $fungsional_column_order = array('tj.id, tj.fungsional_kode, tj.tunjangan, tj.fungsional_id, a.name, a.usia'); //set column field database for datatable orderable
     var $fungsional_column_search = array('tj.fungsional_kode, tj.tunjangan, tj.fungsional_id, a.name, a.usia'); //set column field database for datatable searchable
     var $fungsional_order = array('tj.id' => 'asc'); // default order

    public function get_fungsional(){
      $this->_get_fungsional_datatables_query();
      if (@$_POST['length'] != -1)
          $this->db->limit(@$_POST['length'], @$_POST['start']);
      $query = $this->db->get();
      return $query->result();
    }

    private function _get_fungsional_datatables_query(){
//       SELECT tj.id id, tj.fungsional_kode, tj.tunjangan, tj.fungsional_id, f.`name`, f.usia
// FROM simgaji_tjfungsionals tj
// JOIN simgaji_fungsionals f ON tj.fungsional_id = f.id
// WHERE deleted_at IS NULL;

        $this->db->select('tj.id, tj.fungsional_kode, tj.tunjangan, tj.fungsional_id, a.*');
        $this->db->from($this->fungsionals);
        $this->db->join($this->tj_fungsionals,'a.id = tj.fungsional_id');
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->fungsional_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->fungsional_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->fungsional_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->fungsional_order)) {
            $order = $this->fungsional_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function fungsional_count_filtered()
    {
        $this->_get_fungsional_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function fungsional_count_all()
    {
        $this->_get_fungsional_datatables_query();
        return $this->db->count_all_results();
    }

    public function getFungsionalById($id){
      $sql =
      "SELECT tj.id id, tj.fungsional_kode, tj.tunjangan, tj.fungsional_id, f.`name`, f.usia
      FROM simgaji_tjfungsionals tj
      JOIN simgaji_fungsionals f ON tj.fungsional_id = f.id
      WHERE deleted_at IS NULL
      AND tj.id = ?";

      $query = $this->db->query($sql, array($id));

      if ($query->num_rows() > 0) {
          $data = $query->row();
      } else {
          $data = null;
      }

      return $data;
    }

    public function getDataKedudukan($postData = null){
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start']; //0
        $rowperpage = $postData['length']; // Rows display per page , 10
        $columnIndex = $postData['order'][0]['column']; // Column index
        // $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        // $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        
        ## Search
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = "AND (tj.`kedudukan_id` like '%".$searchValue."%' or tj.`golongan` like '%".$searchValue."%' or tj.`tunjangan` like '%".$searchValue."%' or ked.`name` like '%".$searchValue."%' or ked.`usia` like '%".$searchValue."%') ";
        }
        
        $sql =
        "SELECT tj.*, ked.`kode`, ked.`name`, ked.`usia`, ked.`deleted_at`
        FROM simgaji_tjfungsionals tj
        JOIN simgaji_kedudukans ked ON tj.`kedudukan_id` = ked.`id`
        WHERE ked.`deleted_at` IS NULL";
        
        $query = $this->db->query($sql);
        
        ## Total number of records without filtering
        $totalRecords = $query->num_rows();
        
        ## Total number of record with filtering
        if($searchQuery != ''){
            $sql =
            "SELECT tj.*, ked.`kode`, ked.`name`, ked.`usia`, ked.`deleted_at`
            FROM simgaji_tjfungsionals tj
            JOIN simgaji_kedudukans ked ON tj.`kedudukan_id` = ked.`id`
            WHERE ked.`deleted_at` IS NULL
            $searchQuery";
            
            $query = $this->db->query($sql);
        }
        
        $totalRecordwithFilter = $query->num_rows();
        
        ## Fetch records
        $sql =
        "SELECT tj.*, ked.`kode`, ked.`name`, ked.`usia`, ked.`deleted_at`
        FROM simgaji_tjfungsionals tj
        JOIN simgaji_kedudukans ked ON tj.`kedudukan_id` = ked.`id`
        WHERE ked.`deleted_at` IS NULL
        $searchQuery
        LIMIT $start, $rowperpage";
        
        $query = $this->db->query($sql);
        $records = $query->result();
        
        $data = array();
        
        foreach($records as $key => $value){
            
            // $action =
            // '<div>
            // <a class="btn btn-warning btn-rounded" onclick="detailData(' . $value->id . ')" href="javascript:void(0)" ><i class="mdi mdi-border-color"></i></a>
            // <a class="btn btn-danger btn-rounded" onclick="deleteData(' . $value->id . ')" href="javascript:void(0)" ><i class="mdi mdi-delete"></i></a>
            // </div>';
            $klik_tj_id = '<a href="javascript:void(0)" onclick="detailData('.$value->id.')">'. $value->name .'</a>';
            
            $data[$key] = array(
                'kedudukan_id' => $value->kedudukan_id,
                'golongan' => $value->golongan,
                'tunjangan' => number_format($value->tunjangan),
                'nama' => $klik_tj_id,
                'usia' => $value->usia,
                // 'aksi' => $action
            );
        }
        
        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );
        
        return $response;
    }

    public function getKedudukanById($id){
        $sql =
        "SELECT tj.*, ked.`kode`, ked.`name`, ked.`usia`, ked.`deleted_at`
        FROM simgaji_tjfungsionals tj
        JOIN simgaji_kedudukans ked ON tj.`kedudukan_id` = ked.`id`
        WHERE ked.`deleted_at` IS NULL
		AND tj.`id` = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getIdTableKedudukan($tj_id){
        $sql = 
        "SELECT ked.`id`
        FROM simgaji_tjfungsionals tj
        JOIN simgaji_kedudukans ked ON tj.`kedudukan_id` = ked.`id`
        WHERE ked.`deleted_at` IS NULL
		AND tj.`id` = ?";

        $query = $this->db->query($sql, array($tj_id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function tambahKembaliId($table, $data){
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
    
        return  $insert_id;
    }
}
