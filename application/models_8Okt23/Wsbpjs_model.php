<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbpjs_model extends CI_Model{
    var $client_apikey = "BKDJATENG";
    // var $auth       = "d6bee00c-76cf-47e8-93c2-1fc1cf34106e";

    public $dbgaji = NULL;

    public function __construct(){
        parent::__construct();

        $this->dbgaji = $this->load->database('gaji', TRUE);
    }

    public function check_auth_client(){
        $auth  = $this->input->get_request_header('Authorization', TRUE);
        $client_apikey = $this->input->get_request_header('x-api-key', TRUE);

		$tgl = date('Y-m-d');
        $auth_generate = $this->validate_token('2', $auth, $tgl);

        if($auth_generate == null){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized token'));
        } else

        if($client_apikey == $this->client_apikey && $auth == $auth_generate->token){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function getGajiPnsByUk($service_code, $periode, $uk){
        if($service_code == 'pns' && $uk == 'all'){
            $this->dbgaji->select('kalkulasis.periode AS tgl gaji,
			kalkulasis.name AS NAMA,kalkulasis.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,
			kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais', 'kalkulasis.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->where('kalkulasis.lokasi_kode !=', '77000000');
			//$this->dbgaji->where('kalkulasis.lokasi_kode!=', '01000000');
			$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			$this->dbgaji->order_by('kalkulasis.golongan_id', 'DESC');
			//$this->dbgaji->limit(20);
			$data = $this->dbgaji->get()->result();

            // $data = 'all';
        } else if($service_code == 'pns' && $uk !== 'all'){
            $this->dbgaji->select('kalkulasis.periode AS PERIODE GAJI,
			pegawais.name AS NAMA,
			pegawais.nip AS NIP,pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais', 'kalkulasis.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('kalkulasis.lokasi_kode', $uk, 'after');
			$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			$this->dbgaji->order_by('kalkulasis.golongan_id', 'DESC');
			$data = $this->dbgaji->get()->result();

            // $data = 'by uk';
        } else if($service_code == 'p3k' && $uk !== 'all'){
            $this->dbgaji->select('kalkulasip3ks.periode AS PERIODE GAJI,
			pegawaip3ks.name AS NAMA,
			pegawaip3ks.nip AS NIP,
			pegawaip3ks.nik AS NIK,
			pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			kalkulasip3ks.lokasi_string AS UNIT_KERJA,
			kalkulasip3ks.golongan_string AS GOLONGAN,
			kalkulasip3ks.jabatan AS JABATAN,
			kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
			kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
			kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
			kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasip3ks.tunjangan_umum AS TJ_UMUM,0
			AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasip3ks');
			$this->dbgaji->join('pegawaip3ks', 'kalkulasip3ks.nip=pegawaip3ks.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawaip3ks.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasip3ks.periode', $periode);
			$this->dbgaji->like('kalkulasip3ks.lokasi_kode', $uk, 'after');
			$this->dbgaji->order_by('kalkulasip3ks.lokasi_kode');
			$this->dbgaji->order_by('kalkulasip3ks.golongan_id', 'DESC');
			$data = $this->dbgaji->get()->result();
        } else if($service_code == 'p3k' && $uk == 'all'){
            $this->dbgaji->select('kalkulasip3ks.periode AS PERIODE GAJI,
			pegawaip3ks.name AS NAMA,
			pegawaip3ks.nip AS NIP,
			pegawaip3ks.nik AS NIK,
			pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			kalkulasip3ks.lokasi_string AS UNIT_KERJA,
			kalkulasip3ks.golongan_string AS GOLONGAN,
			kalkulasip3ks.jabatan AS JABATAN,
			kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
			kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
			kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
			kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasip3ks.tunjangan_umum AS TJ_UMUM,0
			AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasip3ks');
			$this->dbgaji->join('pegawaip3ks', 'kalkulasip3ks.nip=pegawaip3ks.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawaip3ks.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasip3ks.periode', $periode);
			$this->dbgaji->order_by('kalkulasip3ks.lokasi_kode');
			$this->dbgaji->order_by('kalkulasip3ks.golongan_id', 'DESC');
			$data = $this->dbgaji->get()->result();
        }

        return $data;
    }

    public function getGajiSusulanP3k($periode, $uk){
        if($uk == 'all'){
            $this->dbgaji->select('kalkulasip3ksusulans.periode AS PERIODE GAJI SUSULAN,
			pegawaip3ks.name AS NAMA,
			pegawaip3ks.nip AS NIP,
			pegawaip3ks.nik AS NIK,
			pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			kalkulasip3ksusulans.lokasi_string AS UNIT_KERJA,
			kalkulasip3ksusulans.golongan_string AS GOLONGAN,
			kalkulasip3ksusulans.jabatan AS JABATAN,
			kalkulasip3ksusulans.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasip3ksusulans.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasip3ksusulans.gaji_pokok AS GAJI_POKOK,
			kalkulasip3ksusulans.tunjangan_istri AS TJ_ISTRI,
			kalkulasip3ksusulans.tunjangan_anak AS TJ_ANAK,
			kalkulasip3ksusulans.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasip3ksusulans.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasip3ksusulans.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasip3ksusulans.tunjangan_umum AS TJ_UMUM,0
			AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasip3ksusulans.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasip3ksusulans.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasip3ksusulans');
			$this->dbgaji->join('pegawaip3ks', 'kalkulasip3ksusulans.nip=pegawaip3ks.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawaip3ks.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasip3ksusulans.periode', $periode);
			$this->dbgaji->order_by('kalkulasip3ksusulans.lokasi_kode');
			$this->dbgaji->order_by('kalkulasip3ksusulans.golongan_id', 'DESC');
			$data = $this->dbgaji->get()->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiTppByPeriodeUk($periode, $uk){
        if($uk !== 'all'){
            $this->dbgaji->select('tpps.periode AS periode tpp,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,tpps.name AS NAMA,
			tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis', 'kalkulasis.nip=tpps.nip', 'left');
			$this->dbgaji->join('pegawais', 'tpps.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('tpps.lokasi_kode', $uk, 'after');
			$this->dbgaji->order_by('tpps.lokasi_kode');
			$this->dbgaji->order_by('tpps.golongan_id', 'DESC');

			$data = $this->dbgaji->get()->result();
        } else if($uk == 'all'){
            $this->dbgaji->select('tpps.periode AS periode tpp,
			tpps.name AS NAMA,tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis', 'kalkulasis.nip=tpps.nip', 'left');
			$this->dbgaji->join('pegawais', 'tpps.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->order_by('tpps.lokasi_kode');
			$this->dbgaji->order_by('tpps.golongan_id', 'DESC');
			// $this->dbgaji->limit(5);

			$data = $this->dbgaji->get()->result();
        }

        return $data;
    }

	public function update_token($table, $stakeholder_code, $data){
        $this->db->where('stakeholder_code', $stakeholder_code);
		$delete = $this->db->update($table, $data); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    private function validate_token($stakeholder_code, $token, $tgl){
        $sql = 
        "SELECT *
        FROM simgaji_token
        WHERE `stakeholder_code` = ?
        AND token = ?
        AND date(created_at) = ?";

        $query = $this->db->query($sql, array($stakeholder_code, $token, $tgl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}