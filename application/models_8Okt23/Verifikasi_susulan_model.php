<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_susulan_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->verifikasi = 'simgaji_susulan a';
        $this->verifikasi_acc = 'simgaji_susulan b';
        $this->pegawaip3ks = 'simgaji_pegawais b';
        
    }

    var $verifikasi_column_order = array('a.id', 'a.jenis_susulan', 'a.nip', 'a.nama', 'a.periode_start', 'a.periode_end', 'a.lama', 'a.baru'); //set column field database for datatable orderable
    var $verifikasi_column_search = array('a.id', 'a.jenis_susulan', 'a.nip', 'a.nama', 'a.periode_start', 'a.periode_end', 'a.lama', 'a.baru'); //set column field database for datatable searchable
    var $verifikasi_order = array('id' => 'DESC'); // default order

    private function _get_verifikasi_datatables_query($periode)
    {

        $role = $this->session->userdata('role');
        $B_02B = $this->session->userdata('B_02B');
        $this->db->select('a.*');
        $this->db->select('c.name as lokasi_kerja');
        $this->db->select('d.name as lokasi_gaji');

        if($periode == 'riwayat'){
            $this->db->where_in('a.verifikasi_status', ['1','2'] );
        }else{
            $this->db->where('a.verifikasi_status', 0);
        }
        
        $this->db->where('a.deleted_at IS NULL');
        $this->db->from($this->verifikasi);
        $this->db->join($this->pegawaip3ks, 'b.id = a.pegawai_id');
        $this->db->join('simgaji_lokasis c', 'c.id = b.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji');

        if($this->input->post('lokasi_kerja'))
        {
            $lokasi_kerja = $this->input->post('lokasi_kerja');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('b.lokasikerja', $lk, 'AFTER');
        }
    
        
        if($this->input->post('lokasi_gaji'))
        {
            $lokasi_kerja = $this->input->post('lokasi_gaji');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('b.lokasigaji', $lk, 'AFTER');
        }

        $i = 0;
        foreach ($this->verifikasi_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->verifikasi_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->verifikasi_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->verifikasi_order)) {
            $order = $this->verifikasi_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
        // $this->db->group_by('a.pegawai_id');

    }

    function get_verifikasi($periode)
    {
        $this->_get_verifikasi_datatables_query($periode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function verifikasi_count_filtered($periode)
    {
        $this->_get_verifikasi_datatables_query($periode);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function verifikasi_count_all($periode)
    {
        // $periode = $tahun.'-'.$bulan.'-01';
        $this->db->from($this->verifikasi);
        $this->db->where('verifikasi_status', $periode);
        $this->db->where('deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    function getById($id)
    {
        // $periode = $tahun.'-'.$bulan.'-01';
        $this->db->from($this->verifikasi);
        $this->db->where('id', $id);
        $this->db->where('deleted_at IS NULL');
        return $this->db->get();
    }

}