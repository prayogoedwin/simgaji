<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;
    public $dbsinaga = NULL;

    public function __construct(){
        parent::__construct();

        $this->tb_gajipokok = 'simgaji_gajip3ks';

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->dbsinaga = $this->load->database('sinaga', TRUE);
    }

    public function getDataPegawaiP3K($query,$cari,$where,$iswhere, $statusSkpd){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}");

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);

            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';

            if($statusSkpd == '2'){
              $klik_nip = '<a href="'. base_url('pegawai_p3k/detail/') . encode_url($value['id']) .'">'. $value['nip'] .'</a>';
            } else {
              $klik_nip = $value['nip'];
            }

			$datatable[$key] = array(
                'name' => $value['name'],
                'nip' => $klik_nip,
                'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('pegawai_p3k/data/') . encode_url($value['id']) .'">Lihat Data</a>'
			);
		}

        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_count,
            'recordsFiltered'=>$sql_filter_count,
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getDetailById($id){
        $sql =
        "SELECT *
        FROM simgaji_pegawaip3ks
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getGolonganYbs($gol_id){
        $sql =
        "SELECT *
        FROM simgaji_golonganp3ks
        WHERE id = ?";

        $query = $this->db->query($sql, array($gol_id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataKodes(){
        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_p3k = '2'
        ORDER BY sort";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataKodes2(){
        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_p3k = '2' AND kode_simpeg != '0'
        ORDER BY sort";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataKodesByJenis($jenis){
        $jns = '%' . $jenis . '%';

        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_p3k = '2'
        AND group_id LIKE ?
        ORDER BY sort";

        $query = $this->db->query($sql, array($jns));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataDataMaster($table){
        $sql =
        "SELECT *
        FROM $table";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataRekomenGaji($gol_id, $masa){
        $sql =
        "SELECT *
        FROM $this->tb_gajipokok
        WHERE golongan_id = ?
        AND masa = ?";

        $query = $this->db->query($sql, array($gol_id, $masa));
        if ($query->num_rows() > 0) {
            // $data = $query->row();
            $data = $query->row_array();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataKeluarga($nip){
        $sql =
        "SELECT *
        FROM MASTKEL1
        WHERE KF_01 = ?";

        $query = $this->dbeps->query($sql, array($nip));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataPegawaiP3kTuSekolah($lokasis_id){
        $sql =
        "SELECT *
        FROM `simgaji_pegawaip3ks`
        WHERE lokasi_gaji = ?";
        // $x = $kode . '%';

        // $sql =
        // "SELECT *
        // FROM `simgaji_pegawaip3ks`
        // WHERE lokasigaji LIKE ?";

        $query = $this->db->query($sql, array($lokasis_id));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDropCabdinIn($kode){
        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kode in($kode)";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataPegawaiForCabdin($lokasigaji){
        $enamdigitkodelokasi = substr($lokasigaji, 0, 6);
        $parentkodelokasi = $enamdigitkodelokasi . '00';

        $sql =
        "SELECT *
        FROM `simgaji_pegawaip3ks`
        WHERE `lokasigaji` LIKE '$enamdigitkodelokasi%'
        ";
        // AND lokasigaji != '$parentkodelokasi'

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function ekasus($nip){
        $sql =
        "SELECT COUNT(NIP) AS hot
        FROM KOMP_KASUS
        WHERE NIP = ?
        AND SELESAIHKM";

        $query = $this->dbeps->query($sql, array($nip));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getKodeLokasisById($lokasis_id){
      $sql =
      "SELECT *
      FROM simgaji_lokasis
      WHERE id = ?";

      $query = $this->db->query($sql, array($lokasis_id));
      if ($query->num_rows() > 0) {
          $data = $query->row();
      } else {
          $data = null;
      }

      return $data;
    }

    public function getNIPCabdinByNipKatuSekolah($nip){
      $sql =
      "SELECT tb.KOLOK, tb.NALOKP
      FROM TABLOKB08 tb, MASTFIP08 m
      WHERE m.B_02B = ?
      AND m.A_01 = tb.A_01
      AND m.A_02 = tb.A_02
      AND m.A_03 = tb.A_03
      AND tb.A_04 = '10'
      AND tb.A_05 = '00'";

      $query = $this->dbeps->query($sql, array($nip));
      $kolok = $query->row()->KOLOK;

      $sql2 =
      "SELECT B_02B
      FROM MASTFIP08
      WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = ?
      AND I_5A = 1";

      $query2 = $this->dbeps->query($sql2, array($kolok));
      if($query2->num_rows() > 0){
        return $query2->row()->B_02B;
      } else {
        // return null;

        $sql_pengganti =
        "SELECT *
        FROM pejabat_pengganti
        WHERE unitkerja = ?";

        $query_pengganti = $this->dbsinaga->query($sql_pengganti, array($kolok));
        if($query_pengganti->num_rows() > 0){
          return 'PLT/PLH ' . $query_pengganti->row()->nip_pengganti;
        } else {
          return null;
        }
      }
    }
}
