<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getData(){
        $sql = 
        "SELECT *
        FROM `simgaji_periodeberkala` WHERE `type` = 1";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    public function getDataTpp(){
        $sql = 
        "SELECT *
        FROM `simgaji_periodeberkala` WHERE `type` = 2";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    public function editByBulan($table, $bulan_lama, $data){
        $this->db->where('bulan', $bulan_lama);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }
}