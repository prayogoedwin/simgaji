<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gajipokok_p3ks_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getData(){
        $sql = 
        "SELECT a.*, b.name as gol
        FROM `simgaji_gajip3ks` a
        JOIN simgaji_golonganp3ks b ON a.golongan_id = b.id";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        
        return $data;
    }

    public function getDataById($id){
        $sql = 
        "SELECT a.*, b.name as gol
        FROM `simgaji_gajip3ks` a
        JOIN simgaji_golonganp3ks b ON a.golongan_id = b.id
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
}