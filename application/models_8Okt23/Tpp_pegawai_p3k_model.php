<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pegawai_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->tb_gajipokok = 'simgaji_gajip3ks';

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDataPegawaiByKode($kode_lokasis, $kolok_simpeg){
        $kode4digit = substr($kode_lokasis, 0, 4) . '%';
        $kolok2digit = substr($kolok_simpeg, 0, 2);
        
        // $sql = 
        // "SELECT *
        // FROM `simgaji_pegawaip3kss`
        // WHERE deleted_at IS NULL
        // AND lokasi_kerja = ?";

        // $sql = 
        // "SELECT *
        // FROM `simgaji_pegawaip3ks`
        // WHERE deleted_at IS NULL
        // AND lokasigaji LIKE ?";

        //query P3K
        $sql = 
        "SELECT CONCAT(B_03A,IF (B_03A!='','. ',''),B_03,IF(B_03B!='',', ',''),B_03B) nama,B_02B,F_03,I_5A,I_05,I_JB,K_01,kelasjab,mm.spesialis,mm.tendik,mm.RUMPUN,
        CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) unor,
        case when p3kdr.nominal is not null then p3kdr.nominal
        when p3kkes.nominal is not null then p3kkes.nominal
        when kasek.nominal is not null then kasek.nominal
        when tusek.nominal is not null then tusek.nominal
        when pws.nominal is not null then pws.nominal
        when jpt.nominal is not null then jpt.nominal
        when tendik.nominal is not null then tendik.nominal
        when str.nominal is not null then str.nominal
        when setara.nominal is not null then setara.nominal
        else gol.nominal end tpp,
        beban.nominal beban,
        case when gurutmp.nominal is not null then gurutmp.nominal
        when tusektmp.nominal is not null then tusektmp.nominal 
        when tendiktmp.nominal is not null then tendiktmp.nominal
        else tempat.nominal end tempat,
        case when gurukon.nominal is not null then gurukon.nominal
        when tusekkon.nominal is not null then tusekkon.nominal 
        when tendikkon.nominal is not null then tendikkon.nominal
        else kondisi.nominal end kondisi
        from (
        SELECT m.*,
        CASE WHEN I_5A='1' then l.KELAS_1 when I_5A='2' then f2.KELAS else u.KELAS end kelasjab,
        sp.nip,
        if (sp.nip is null,0,1) spesialis,
        if ((m.A_01='D0' and m.A_02!='00' and m.A_04>'30') or (I_5A='2' and I_05 in ('00018','00053')),2,0) tendik,
        f.RUMPUN from MASTFIP08 m
        LEFT JOIN TABFNG1 f on m.I_05=f.KFUNG
        left join TABLOKB08 l on m.I_05=l.KOLOK
        LEFT JOIN petafngsapk p on m.I_05=p.KFUNG and m.I_07=p.KJENJANG
        left JOIN TABFNG2 f2 on p.CEPAT_KODE=f2.CEPAT_KODE
        left join TABJFU15 u on m.I_05=u.KOJFU
        left join presensi.spesialis sp on m.B_02B=sp.nip
        where m.A_01 not in (98,99)
        ) mm
        left join TABREFTPPKELASJAB p3kdr on p3kdr.jenisasn=mm.B_09 and p3kdr.rumpun=mm.RUMPUN and p3kdr.kdjab=mm.I_05 and p3kdr.spesialis=mm.spesialis
        left join TABREFTPPKELASJAB p3kkes on p3kkes.jenisasn=mm.B_09 and p3kkes.rumpun=mm.RUMPUN and p3kkes.kdjab=0 and mm.tendik=0
        left join TABREFTPPKELASJAB kasek on kasek.kdjab=mm.I_05 and mm.tendik=2 and kasek.koord=mm.K_01 and mm.I_05='00018' and mm.I_5A=2 and mm.K_01=1
        left join TABREFTPPKELASJAB tusek on mm.I_05 like tusek.kdjab and mm.tendik=2 and I_5A=1
        left join TABREFTPPKELASJAB pws on pws.kdjab=mm.I_05 and pws.jenjang=mm.I_07 and mm.I_05='00053' and mm.I_5A=2
        left join TABREFTPPKELASJAB tendik on tendik.spesialis=mm.tendik and tendik.kelas=kelasjab
        left join TABREFTPPKELASJAB jpt on jpt.kelas=kelasjab and jpt.kdjab=mm.I_05
        left join TABREFTPPKELASJAB str on str.kelas=kelasjab and str.esel=left(mm.I_06,1) and mm.tendik=0
        left join TABREFTPPKELASJAB setara on setara.kelas=kelasjab and setara.koord=mm.K_01 and mm.tendik=0
        left join TABREFTPPKELASJAB gol on gol.kelas=kelasjab and gol.gol=left(F_03,1) and mm.tendik=0
        left join TABREFTPPKELASJAB_BEBAN beban on beban.kelas=kelasjab and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like beban.unor
        left join TABREFTPPKELASJAB_TEMPAT gurutmp on gurutmp.kdjab=mm.I_05 and mm.tendik=2 and gurutmp.koord=ifnull(mm.K_01,0) and mm.I_05='00018' and mm.I_5A=2 and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like gurutmp.unor
        left join TABREFTPPKELASJAB_TEMPAT tusektmp on mm.I_05 like tusektmp.kdjab and mm.tendik=2 and I_5A=1 and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like tusektmp.unor
        left join TABREFTPPKELASJAB_TEMPAT tendiktmp on tendiktmp.spesialis=mm.tendik and mm.tendik=2 and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like tendiktmp.unor
        left join TABREFTPPKELASJAB_TEMPAT tempat on tempat.kelas=kelasjab and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like tempat.unor
        left join TABREFTPPKELASJAB_KONDISI gurukon on gurukon.kdjab=mm.I_05 and mm.tendik=2 and gurukon.koord=ifnull(mm.K_01,0) and mm.I_05='00018' and mm.I_5A=2 and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like gurukon.unor
        left join TABREFTPPKELASJAB_KONDISI tusekkon on mm.I_05 like tusekkon.kdjab and mm.tendik=2 and I_5A=1 and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like tusekkon.unor
        left join TABREFTPPKELASJAB_KONDISI tendikkon on tendikkon.spesialis=mm.tendik and mm.tendik=2 and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like tendikkon.unor
        left join TABREFTPPKELASJAB_KONDISI kondisi on kondisi.kelas=kelasjab and CONCAT(mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05) like kondisi.unor
        WHERE mm.A_01 not in (98,99) and mm.A_01= ?
        AND mm.B_09 != '3'
        ORDER BY mm.A_01,mm.A_02,mm.A_03,mm.A_04,mm.A_05,I_06,F_03 desc;";

        $query = $this->dbeps->query($sql, array($kolok2digit));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataPegawaiByKodeAsKasubbagKeu($kode_lokasis){
        $sql = 
        "SELECT p.`id`, p.`nip`, p.`name`
        FROM `simgaji_pegawaip3ks_tpp` p
        WHERE lokasikerja = ?;";

        $query = $this->db->query($sql, array($kode_lokasis));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDetailById($id){
        $sql =
        "SELECT *
        FROM simgaji_pegawaip3ks_tpp
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataDataMaster($table){
        $sql =
        "SELECT *
        FROM $table";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGolonganYbs($gol_id){
        $sql =
        "SELECT *
        FROM simgaji_golonganp3ks
        WHERE id = ?";

        $query = $this->db->query($sql, array($gol_id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataKodes(){
        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_tpp = '2'
        ORDER BY sort";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataKodes2(){
        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_p3k = '2' AND kode_simpeg != '0'
        ORDER BY sort";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataKodesByJenis($jenis){
        $jns = '%' . $jenis . '%';

        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_tpp = '2'
        AND group_id LIKE ?
        ORDER BY sort";

        $query = $this->db->query($sql, array($jns));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataRekomenGaji($gol_id, $masa){
        $sql =
        "SELECT *
        FROM $this->tb_gajipokok
        WHERE golongan_id = ?
        AND masa = ?";

        $query = $this->db->query($sql, array($gol_id, $masa));
        if ($query->num_rows() > 0) {
            // $data = $query->row();
            $data = $query->row_array();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataKeluarga($nip){
        $sql =
        "SELECT *
        FROM MASTKEL1
        WHERE KF_01 = ?";

        $query = $this->dbeps->query($sql, array($nip));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getNIPCabdinByNipKatuSekolah($nip){
        $sql =
        "SELECT tb.KOLOK, tb.NALOKP
        FROM TABLOKB08 tb, MASTFIP08 m
        WHERE m.B_02B = ?
        AND m.A_01 = tb.A_01
        AND m.A_02 = tb.A_02
        AND m.A_03 = tb.A_03
        AND tb.A_04 = '10'
        AND tb.A_05 = '00'";
  
        $query = $this->dbeps->query($sql, array($nip));
        $kolok = $query->row()->KOLOK;
  
        $sql2 =
        "SELECT B_02B
        FROM MASTFIP08
        WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = ?
        AND I_5A = 1";
  
        $query2 = $this->dbeps->query($sql2, array($kolok));
        if($query2->num_rows() > 0){
          return $query2->row()->B_02B;
        } else {
          // return null;
  
          $sql_pengganti =
          "SELECT *
          FROM pejabat_pengganti
          WHERE unitkerja = ?";
  
          $query_pengganti = $this->dbsinaga->query($sql_pengganti, array($kolok));
          if($query_pengganti->num_rows() > 0){
            return 'PLT/PLH ' . $query_pengganti->row()->nip_pengganti;
          } else {
            return null;
          }
        }
    }

    public function ekasus($nip){
        $sql =
        "SELECT COUNT(NIP) AS hot
        FROM KOMP_KASUS
        WHERE NIP = ?
        AND SELESAIHKM";

        $query = $this->dbeps->query($sql, array($nip));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getJabatanSimpeg($nip){
        $sql =
        "SELECT f2.NAMA nama_jabatan, KELAS kelas_jabatan
        from MASTFIP08 m
        left join petafngsapk p on I_05=p.KFUNG and m.I_07=p.KJENJANG
        left join TABFNG2 f2 on p.CEPAT_KODE=f2.CEPAT_KODE
        where m.A_01 not in (98,99) and I_5A=2
        and m.B_02B = ?";

        $query = $this->dbeps->query($sql, array($nip));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}