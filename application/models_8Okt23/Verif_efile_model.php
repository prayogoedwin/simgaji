<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verif_efile_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDetailHistoryById($history_id){
        $sql = 
        "SELECT h.*, k.`kode_dokumen`, p.`nip`
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_kodes` k ON h.`kode_id` = k.`id`
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`id` = ?";

        $query = $this->db->query($sql, array($history_id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
}