<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_tpp_pot_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->usulantpp_pot = 'simgaji_masttpp_kinerja';   
    }

    public function getRowVerifikator($nip){
        $sql = 
        "SELECT *
        FROM simgaji_verifikator
        WHERE deleted_at IS NULL
        AND nip = ?";

        $query = $this->db->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getRowTablokB08($kolok){
        $sql = 
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";

        $query = $this->dbeps->query($sql, array($kolok));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataUsulanPerUnor($unor, $bln, $thn, $nokelas = null){

        $q_add = "";
        if($nokelas != null){
            $q_add = "AND (kelas_jab = 0 OR kelas_jab = null OR kelas_jab = '')";
        }

        $A_01 = substr($unor, 0, 2);
        $A_02 = substr($unor, 2, 2);
        $A_03 = substr($unor, 4, 2);
        $A_04 = substr($unor, 6, 2);

        $kol4 = $A_01 . $A_02;
        $kol2 = $A_01;

        $b = $this->db->escape($bln);
        $t = $this->db->escape($thn);
        $k4 = $this->db->escape($kol4);
        $k2 = $this->db->escape($kol2);
        
        if($A_01 == 'D0' && $A_02 <> '00'){
            //selain disdik
            $sql = 
            "SELECT *
            FROM simgaji_masttpp_kinerja
            WHERE LEFT(KOLOK,4) = $k4
            AND bulan = $b
            AND tahun = $t
            $q_add
            ORDER BY KOLOK";
        } else if($A_01 == 'D0' && $A_02 == '00'){
            //disdik induk
            $sql = 
            "SELECT *
            FROM simgaji_masttpp_kinerja
            WHERE LEFT(KOLOK,2) = '$A_01'
            AND MID(KOLOK,3,2) = '00'
            AND bulan = $b
            AND tahun = $t
            $q_add
            ORDER BY KOLOK";
        } else {
            //turunan disdik
            $sql = 
            "SELECT *
            FROM simgaji_masttpp_kinerja
            WHERE LEFT(KOLOK,2) = $k2
            AND bulan = $b
            AND tahun = $t
            $q_add
            ORDER BY KOLOK";
        }
        

        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDetailData($nip, $bln, $thn){
        $sql = 
        "SELECT *
        FROM `simgaji_masttpp_kinerja`
        WHERE nip = ?
        AND bulan = ?
        AND tahun = ?";

        $query = $this->db->query($sql, array($nip, $bln, $thn));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataNominatifCetak($tahun, $bulan, $cetak100 = null, $kode = null){
        $qc = "";
        if($cetak100 != null){
            // $qc = "AND skp = '100'";
            $qc="AND (kin.`skp`<100 OR kin.`perilaku`>0 OR kin.`hukdis`<100 OR kin.`nominalkurang`>0 OR kin.`lhkpn` > 0 OR kin.`lhkasn` > 0 OR kin.`bmd` > 0 OR kin.`grat` > 0 OR kin.`tptgr` > 0) ";
        }

        $qd = "";
        if($kode != null){
            $panjang = strlen($kode);
            if($panjang == 6){
                $A_01 = substr($kode, 0, 2);
                $A_02 = substr($kode, 2, 2);
                $A_03 = substr($kode, 4, 2);
                $kolg = $A_01 . $A_02 . $A_03;
                $qd = "AND LEFT(kin.`KOLOK`,6) = '$kolg'";

            } else if($panjang == 8){
                $A_01 = substr($kode, 0, 2);
                $A_02 = substr($kode, 2, 2);
                $A_03 = substr($kode, 4, 2);
                $A_04 = substr($kode, 6, 2);
                $kolg = $A_01 . $A_02 . $A_03 . $A_04;
                $qd = "AND LEFT(kin.`KOLOK`,8) = '$kolg'";
            } else if($panjang == 10){
                $A_01 = substr($kode, 0, 2);
                // $A_02 = substr($kode, 2, 2);
                // $A_03 = substr($kode, 4, 2);
                // $A_04 = substr($kode, 6, 2);
                // $A_05 = substr($kode, 8, 2);
                // $kolg = $A_01 . $A_02 . $A_03 . $A_04 . $A_05;
                $qd = "AND LEFT(kin.`KOLOK`,2) = '$A_01'";
            }
        }

        $sql = 
        "SELECT kin.*, tab.`NALOK`
        FROM simgaji_masttpp_kinerja kin
        JOIN eps.`TABLOKB08` tab ON kin.`KOLOK` = tab.`KOLOK`
        WHERE kin.`tahun` = ?
        AND kin.`bulan` = ?
        $qc
        $qd
        ORDER BY kin.`KOLOK`,kin.`kelas_jab` DESC";

        $query_cek = $this->db->query($sql, array($tahun, $bulan));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getRowKepala($uk){
        $subuk = '';

        if ($uk=='A2' && ($subuk=='' || $subuk=='all')){
            $qk="SELECT t.A_01,t.KOLOK,t.NAJABP,'SEKRETARIAT DAERAH' NALOKP,a.B_02,a.B_02B,a.B_03A,a.B_03,a.B_03B,a.F_03,a.I_JB, pkt.NAMAY
            FROM TABLOKB08 t
            LEFT JOIN MASTFIP08 a ON t.KOLOK=a.I_05
            LEFT JOIN TABPKT pkt ON a.F_03 = pkt.KODE
            WHERE t.KOLOK='A200310000'
            AND t.A_01<>'99'";
        } else {
            $qk="SELECT t.A_01,t.KOLOK,t.NAJABP,t.NALOKP,a.B_02,a.B_02B,a.B_03A,a.B_03,a.B_03B,a.F_03,a.I_JB,pkt.NAMAY
            FROM TABLOKB08 t
            LEFT JOIN MASTFIP08 a ON t.KOLOK=a.I_05
            LEFT JOIN TABPKT pkt ON a.F_03 = pkt.KODE
            WHERE t.KOLOK=RPAD(t.A_01,10,'0')
            and t.A_01='$uk'
            AND t.A_01<>'99'";
        }
		$rk=$this->dbeps->query($qk);
        if($rk->row()->B_02B != null){
            $dt = $rk->row();
            $isplt = 0;
        } else {
            // $dt = null;
            $x = $uk . '00000000';
            $q_p = 
            "SELECT *
            FROM bkd_sinaga_new.pejabat_pengganti
            WHERE unitkerja = '$x'";
            $rP = $this->dbeps->query($q_p)->row();
            $nip_plt = $rP->nip_pengganti;

            $q_plt = 
            "SELECT t.A_01,t.KOLOK,t.NAJABP,t.NALOKP,a.B_02,a.B_02B,a.B_03A,a.B_03,a.B_03B,a.F_03,a.I_JB,pkt.NAMAY
            FROM TABLOKB08 t
            LEFT JOIN MASTFIP08 a ON t.KOLOK=a.I_05
            LEFT JOIN TABPKT pkt ON a.F_03 = pkt.KODE
            WHERE a.B_02B = '$nip_plt'
            AND t.A_01<>'99'";

            $rplt = $this->dbeps->query($q_plt);

            $dt = $rplt->row();
            $isplt = 1;
        }

        $y = array(
            'rPeg' => $dt,
            'isplt' => $isplt
        );
        
		return $y;
    }

    public function format_nip($nip) {
		$formatted = substr($nip,0,8).' '.substr($nip,8,6).' '.substr($nip,14,1).' '.substr($nip,15,3);
		return $formatted;
	}

    public function getRowSKPDInduk($uk){
        $qk="SELECT *
            FROM TABLOKB08 t
            WHERE t.A_01=?
            AND t.A_01<>'99'";

        $rk=$this->dbeps->query($qk, array($uk));

        return $rk->row();
    }

    public function getUkDisdik($kd = null){
        
        if($kd != null){
            $panjang = strlen($kd);
            if($panjang == 6){
                $qkolok = 
                "SELECT *
                FROM TABLOKB08
                WHERE LEFT(KOLOK,6) = ?
                LIMIT 1";

            } else if($panjang == 8){
                $qkolok =
                "SELECT *
                FROM TABLOKB08
                WHERE LEFT(KOLOK,8) = ?
                LIMIT 1";
            }
        }

        $quer = $this->dbeps->query($qkolok, array($kd));
        if($quer->num_rows() > 0){
            $data = $quer->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function editbaru($table, $data, $nip, $bulan, $tahun){
        // $this->db->update_batch('po_order_details',$updateArray, 'poid');
        // $upd = $this->db->update_batch($table, $updateArray);
        // if($upd){
        //     return TRUE;
        // } else {
        //     return FALSE;
        // }

        $this->db->where('nip', $nip);
        $this->db->where('bulan', $bulan);
        $this->db->where('tahun', $tahun);
		$delete = $this->db->update($table, $data); 
		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }
}