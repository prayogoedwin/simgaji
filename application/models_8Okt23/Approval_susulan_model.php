<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_susulan_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;
    public $dbgaji = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->dbgaji = $this->load->database('gaji', TRUE);
    }

    public function getData($postData = null, $nip, $bulan, $tahun){
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start']; //0
        $rowperpage = $postData['length']; // Rows display per page , 10
        $columnIndex = $postData['order'][0]['column']; // Column index
        // $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        // $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        
        ## Search
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = "AND (nip LIKE '%".$searchValue."%' OR nama LIKE '%".$searchValue."%') ";
        }
        
        $sql =
        "SELECT *
        FROM `simgaji_susulan`
        WHERE deleted_at IS NULL
        AND `posisi_acc` = ?
        AND MONTH(created_at) = ?
        AND YEAR(created_at) = ?
        AND status = '1'";
        
        $query = $this->db->query($sql, array($nip, $bulan, $tahun));
        
        ## Total number of records without filtering
        $totalRecords = $query->num_rows();
        
        ## Total number of record with filtering
        if($searchQuery != ''){
            $sql =
            "SELECT *
            FROM `simgaji_susulan`
            WHERE deleted_at IS NULL
            AND `posisi_acc` = ?
            AND MONTH(created_at) = ?
            AND YEAR(created_at) = ?
            AND status = '1'
            $searchQuery";
            
            $query = $this->db->query($sql, array($nip, $bulan, $tahun));
        }
        
        $totalRecordwithFilter = $query->num_rows();
        
        ## Fetch records
        $sql =
        "SELECT *
        FROM `simgaji_susulan`
        WHERE deleted_at IS NULL
        AND `posisi_acc` = ?
        AND MONTH(created_at) = ?
        AND YEAR(created_at) = ?
        AND status = '1'
        $searchQuery
        LIMIT $start, $rowperpage";
        
        $query = $this->db->query($sql, array($nip, $bulan, $tahun));
        $records = $query->result();
        
        $data = array();
        
        foreach($records as $key => $value){
            $jns = '-';
            if($value->jenis_susulan == '1'){
                $jns = '<span class="badge badge-warning">Gaji Pokok</span>';
            } else if($value->jenis_susulan == '2'){
                $jns = '<span class="badge badge-warning">Fungsional</span>';
            }

            // $aks = 
            //     '<div>
            //         <a class="btn btn-primary btn-sm" href="javascript:void(0)" onclick="del('.$value->id.')"><i class="icon-eye"></i></a>
            //     </div>';
            
            $data[$key] = array(
                'jenis_susulan' => $jns,
                'nip' => $value->nip,
                'nama' => $value->nama,
                'lama' => $value->lama,
                'baru' => $value->baru,
                'bulan' => $value->jumlah_bulan,
                'start' => $value->periode_start,
                'end' => $value->periode_end,
                // 'action' => $aks
            );
        }
        
        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );
        
        return $response;
    }

    public function getDataApp($nip, $bulan, $tahun){
        $sql = 
        "SELECT *
        FROM `simgaji_susulan`
        WHERE deleted_at IS NULL
        AND `posisi_acc` = ?
        AND MONTH(created_at) = ?
        AND YEAR(created_at) = ?
        AND status = '1'";

        $query = $this->db->query($sql, array($nip, $bulan, $tahun));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function edit_batch_bynip($table, $data, $nip){
		// $insert = $this->db->insert_batch($table, $data);
        // $this->db->where('posisi_acc', $nip);
        $upd = $this->db->update_batch($table,$data, 'posisi_acc' ); 
		if ($upd){
			return TRUE;
		} else {
			return FALSE;
		}
	}

    public function updateDataUsulanByBulanTahun($table, $data, $bulan, $tahun, $nip){
        $this->db->where('MONTH(created_at)', $bulan);
        $this->db->where('YEAR(created_at)', $tahun);
        $this->db->where('posisi_acc', $nip);
		$update = $this->db->update($table, $data);

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function editByNipByBulanTahun($table, $nip, $bulan, $tahun, $data){
        $this->db->where('nip', $nip);
        $this->db->where('MONTH(created_at)', $bulan);
        $this->db->where('YEAR(created_at)', $tahun);
        // $this->db->where('A_01', $A_01);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function getSuperadminBkd(){
        $sql = 
        "SELECT id, nip, nama
        FROM simgaji_verifikator
        WHERE deleted_at IS NULL
        AND type_role = '1'";

        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
}