<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);

        $this->history_acc = 'bkd_gaji_dev.simgaji_historyp3ks_acc';
    }

    public function getDataHistoryP3K($query,$cari,$where,$iswhere,$isGroupBy){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}");

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);

            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")" . " GROUP BY " . $isGroupBy .$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")". " GROUP BY " . $isGroupBy . $order ." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }

            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';
            $klik_nip = '<a href="'. base_url('history_p3k/detail/') . encode_url($value['id']) .'">'. $value['nip'] .'</a>';

			$datatable[$key] = array(
                'name' => $value['nama'],
                'nip' => $klik_nip
			);
		}

        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_count,
            'recordsFiltered'=>$sql_filter_count,
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getDataHistoryP3KPeriode($query,$cari,$where,$iswhere,$isGroupBy){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}");

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);

            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")" . " GROUP BY " . $isGroupBy .$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")". " GROUP BY " . $isGroupBy . $order ." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }

            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';
            // $klik_periode = '<a href="'. base_url('history_p3k/periode/') . $value['periode'] .'">'. $value['periode'] .'</a>';

            $per = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
            if($value['periode'] != $per){
                $period = $value['periode'];
            } else {
                $period = '<a href="'. base_url('history_p3k/periode/') . $value['periode'] .'">'. $value['periode'] .'</a>';
            }



			$datatable[$key] = array(
                'periode' => $period
			);
		}

        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_count,
            'recordsFiltered'=>$sql_filter_count,
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getDataByPegawaiId($pegawai_id){
        $sql =
        "SELECT h.*, k.`name` params, k.field as fidel
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_kodes` k ON h.`kode_id` = k.`id`
        WHERE h.deleted_at IS NULL
        AND h.pegawai_id = ?";

        $query = $this->db->query($sql, array($pegawai_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataByPegawaiIdForAdmin($pegawai_id, $peride){
        $sql =
        "SELECT h.*, k.`name` params, k.field as fidel
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_kodes` k ON h.`kode_id` = k.`id`
        WHERE h.deleted_at IS NULL
        AND h.pegawai_id = ? AND h.periode = ?";

        $query = $this->db->query($sql, array($pegawai_id, $peride));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataByPegawaiIdBandingkan($pegawai_id){
        // $sql =
        // "SELECT a.*, b.`pegawai_id`
        // FROM simgaji_kodes a
        // LEFT JOIN simgaji_historyp3ks b ON b.`kode_id` = a.`id` AND pegawai_id = ? AND b.`status_ajuan` < 3
        // WHERE a.`bool_id` = '2'
        // ORDER BY sort ASC";

        // $sql =
        // "SELECT a.*, b.`pegawai_id`, b.`baru`, b.`kode_id`, b.`status_ajuan`, b.id as id_history
        // FROM simgaji_kodes a
        // LEFT JOIN simgaji_historyp3ks b ON b.`kode_id` = a.`id` AND b.`pegawai_id` = ? AND b.`status_ajuan` < 3
        // WHERE a.`bool_id` = '2'
        // ORDER BY sort ASC;";

        $sql =
        "SELECT a.*, b.`pegawai_id`, b.`baru`, b.`kode_id`, b.`status_ajuan`, b.id as id_history, b.periode
        FROM simgaji_kodes a
        LEFT JOIN simgaji_historyp3ks b ON b.`kode_id` = a.`id` AND b.`pegawai_id` = ?
        WHERE a.`bool_id` = '2'
        ORDER BY sort ASC;";

        $query = $this->db->query($sql, array($pegawai_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDetailPegawaiById($pegawai_id){
        $sql =
        "SELECT *
        FROM `simgaji_pegawaip3ks`
        WHERE id = ?";

        $query = $this->db->query($sql, array($pegawai_id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function updateDataUsulanByPeriode($table, $data, $periode, $user_id){
        $this->db->where('periode', $periode);
        $this->db->where('posted_by', $user_id);
		$update = $this->db->update($table, $data);

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function getStatusAllDataByPeriode($periode, $user_id){
        $sql =
        "SELECT p.`id`, h.`status_ajuan`, h.`updated_at`
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`periode` = ?
        AND h.`posted_by` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataHistory($A_01, $periode, $user_id){
        // $sql =
        // "SELECT *
        // FROM simgaji_historyp3ks_acc
        // WHERE periode = ?
        // AND (A_01 = ? OR posted_by = ?)
        // GROUP BY nip
        // ORDER BY id ASC";

        $sql =
        "SELECT h.*
        FROM $this->history_acc h
        JOIN eps.USER_NEW u ON h.posted_by = u.id
        JOIN eps.MASTFIP08 m ON u.username = m.B_02B
        WHERE h.posted_by = ?
        AND h.periode = ?
        AND (SELECT A_02 FROM eps.MASTFIP08 WHERE B_02B = u.username) = (SELECT A_02 FROM eps.MASTFIP08 WHERE B_02B = h.nip);";

        $query = $this->db->query($sql, array($user_id, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getNipAtas($posted_by, $periode){
        $sql =
        "SELECT *
        FROM `simgaji_historyp3ks`
        WHERE posted_by = ?
        AND periode = ?";

        $query = $this->db->query($sql, array($posted_by, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataByPostedBy($periode, $user_id){
        $sql =
        "SELECT h.`id` id_hist, p.`id`, p.`nip`, p.`name` nama, h.`updated_at`
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        AND h.`periode` = ?
        AND h.`posted_by` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataByCabdinBy($periode, $user_id){
        $sql =
        "SELECT h.`id` id_hist, p.`id`, p.`nip`, p.`name` nama, h.`updated_at`
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        AND h.`periode` = ?
        AND h.`cabdin_by` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataByKasubbagKeu($periode, $user_id){
        $sql =
        "SELECT h.`id` id_hist, p.`id`, p.`nip`, p.`name` nama
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        AND h.`periode` = ?
        AND h.`kasubbag_keu_by` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataById($id_history){
        $sql =
        "SELECT *
        FROM simgaji_historyp3ks
        WHERE id = ?";

        $query = $this->db->query($sql, array($id_history));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getKodesById($id){
        $sql =
        "SELECT *
        FROM simgaji_kodes
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDetailVerifikatorById($id){
        $sql =
        "SELECT *
        FROM simgaji_verifikator
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataPegawaiP3kById($id){
        $sql =
        "SELECT *
        FROM simgaji_pegawaip3ks
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getJabatanCabdinByNip($nip){
        $sql =
        "SELECT *
        FROM MASTFIP08
        WHERE B_02B = ?";

        $query = $this->dbeps->query($sql, array($nip));
        $data = $query->row();

        $kolok = $data->A_01 . $data->A_02 . $data->A_03 . $data->A_04 . $data->A_05;

        $sqlTablok =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";
        $queryTablok = $this->dbeps->query($sqlTablok, array($kolok));
        $dataTablok = $queryTablok->row();
        $kolokAtasan = $dataTablok->ATASAN;

        $sqlAtasan =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";

        $queryAtasan = $this->dbeps->query($sqlAtasan, array($kolokAtasan));
        $dataAtasan = $queryAtasan->row();

        return $dataAtasan->NALOK;
    }
}
