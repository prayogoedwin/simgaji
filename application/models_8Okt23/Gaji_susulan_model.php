<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji_susulan_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;
    public $dbgaji = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->dbgaji = $this->load->database('gaji', TRUE);
    }

    public function getData($postData = null, $user_id){
        ## Read value
        $draw = $postData['draw'];
        $start = $postData['start']; //0
        $rowperpage = $postData['length']; // Rows display per page , 10
        $columnIndex = $postData['order'][0]['column']; // Column index
        // $columnName = $postData['columns'][$columnIndex]['data']; // Column name
        // $columnSortOrder = $postData['order'][0]['dir']; // asc or desc
        $searchValue = $postData['search']['value']; // Search value
        
        ## Search
        $searchQuery = "";
        if($searchValue != ''){
            $searchQuery = "AND (nip LIKE '%".$searchValue."%' OR nama LIKE '%".$searchValue."%') ";
        }
        
        $sql =
        "SELECT *
        FROM `simgaji_susulan`
        WHERE deleted_at IS NULL
        AND posted_by = ?";
        
        $query = $this->db->query($sql, array($user_id));
        
        ## Total number of records without filtering
        $totalRecords = $query->num_rows();
        
        ## Total number of record with filtering
        if($searchQuery != ''){
            $sql =
            "SELECT *
            FROM `simgaji_susulan`
            WHERE deleted_at IS NULL
            AND posted_by = ?
            $searchQuery";
            
            $query = $this->db->query($sql, array($user_id));
        }
        
        $totalRecordwithFilter = $query->num_rows();
        
        ## Fetch records
        $sql =
        "SELECT *
        FROM `simgaji_susulan`
        WHERE deleted_at IS NULL
        AND posted_by = ?
        $searchQuery
        LIMIT $start, $rowperpage";
        
        $query = $this->db->query($sql, array($user_id));
        $records = $query->result();
        
        $data = array();
        
        foreach($records as $key => $value){

            if($value->status == 0){
                $aks = 
                '<div>
                    <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="del('.$value->id.')"><i class="icon-trash"></i></a>
                </div>';
            } else {
                $aks = '-';
            }
            
            $data[$key] = array(
                'nip' => $value->nip,
                'nama' => $value->nama,
                'lama' => $value->lama,
                'baru' => $value->baru,
                'bulan' => $value->jumlah_bulan,
                'start' => $value->periode_start,
                'end' => $value->periode_end,
                'action' => $aks
            );
        }
        
        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );
        
        return $response;
    }

    public function getDataPegawai($nip){
        $sql =
        "SELECT p.`id`, p.`nip` id_nip, p.`name`, lok.`name` nama_lokasi
        FROM simgaji_pegawais p
        JOIN `simgaji_lokasis` lok ON p.`lokasigaji` = lok.`kode`
        WHERE p.`nip` = ?";

        $query = $this->db->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function gajilama($periode, $nip, $kolom){
        $pecah = explode('-', $periode);
        $per = $pecah[0].'-'.$pecah[1].'-01';

        if($kolom == 1){
            //struktural
            $pilih = 'id, nip, tanggal, periode, name, tunjangan_struktural nominal, lokasi_kode';
        } else if ($kolom == 2){
            //jft
            $pilih = 'id, nip, tanggal, periode, name, tunjangan_fungsional nominal, lokasi_kode';
        } else if($kolom == 3){
            //pelaksana 
            $pilih = 'id, nip, tanggal, periode, name, tunjangan_umum nominal, lokasi_kode';
        }

        $sql =
        "SELECT $pilih
        FROM kalkulasis
        WHERE periode = ?
        AND nip = ?";

        $query = $this->dbgaji->query($sql, array($per, $nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function gajiAkhir($nip, $kolom){
        // $pecah = explode('-', $periode);
        // $per = $pecah[0].'-'.$pecah[1].'-01';

        if($kolom == 1){
            //struktural
            $pilih = 'id, nip, tanggal, periode, name, tunjangan_struktural nominal, lokasi_kode';
        } else if ($kolom == 2){
            //jft
            $pilih = 'id, nip, tanggal, periode, name, tunjangan_fungsional nominal, lokasi_kode';
        } else if($kolom == 3){
            //pelaksana 
            $pilih = 'id, nip, tanggal, periode, name, tunjangan_umum nominal, lokasi_kode';
        }

        $sql =
        "SELECT $pilih
        FROM kalkulasis
        WHERE nip = ?
        ORDER BY id DESC";

        $query = $this->dbgaji->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function updateByUserIdByMonth($table, $data, $userid, $bl, $th){
        $this->db->where('posted_by', $userid);
        $this->db->where('MONTH(created_at)', $bl);
        $this->db->where('YEAR(created_at)', $th);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function getNipAcc($userid, $bl){
        $sql = 
        "SELECT *
        FROM simgaji_susulan
        WHERE deleted_at IS NULL
        AND posted_by = ?
        AND MONTH(created_at) = ?";

        $query = $this->db->query($sql, array($userid,$bl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getJabatanCabdinByNip($nip){
        $sql =
        "SELECT *
        FROM MASTFIP08
        WHERE B_02B = ?";

        $query = $this->dbeps->query($sql, array($nip));
        $data = $query->row();

        $kolok = $data->A_01 . $data->A_02 . $data->A_03 . $data->A_04 . $data->A_05;

        $sqlTablok =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";
        $queryTablok = $this->dbeps->query($sqlTablok, array($kolok));
        $dataTablok = $queryTablok->row();
        $kolokAtasan = $dataTablok->ATASAN;

        $sqlAtasan =
        "SELECT *
        FROM TABLOKB08
        WHERE KOLOK = ?";

        $queryAtasan = $this->dbeps->query($sqlAtasan, array($kolokAtasan));
        $dataAtasan = $queryAtasan->row();

        return $dataAtasan->NALOK;
    }

    public function updateDataUsulanByBulan($table, $data, $bl, $user_id){
        $this->db->where('MONTH(created_at)', $bl);
        $this->db->where('posted_by', $user_id);
		$update = $this->db->update($table, $data);

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function getGaji($nip){
        $sql =
        "SELECT *
        FROM kalkulasis
        WHERE nip = ?
        ORDER BY id ASC
        LIMIT 1";

        $query = $this->dbgaji->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getGajiUntukFungsional($nip){
        $sql =
        "SELECT *
        FROM kalkulasis
        WHERE nip = ?
        ORDER BY id DESC
        LIMIT 1";

        $query = $this->dbgaji->query($sql, array($nip));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}