<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbpkad_model extends CI_Model{
    var $client_apikey = "BKDJATENG";

    public $dbgaji = NULL;

    public function __construct(){
        parent::__construct();

        $this->dbgaji = $this->load->database('gaji', TRUE);
    }

    public function check_auth_client(){
        $auth  = $this->input->get_request_header('Authorization', TRUE);
        $client_apikey = $this->input->get_request_header('x-api-key', TRUE);

        $tgl = date('Y-m-d');
        $auth_generate = $this->validate_token('4', $auth, $tgl);

        if($auth_generate == null){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized token'));
        } else

        if($client_apikey == $this->client_apikey && $auth == $auth_generate->token){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function getGajiPns($periode, $page, $item_per_page){
        $pecahPeriode = explode('-', $periode);
        $xtahun = $pecahPeriode[0];
        $xbulan = $pecahPeriode[1];

        $offset = ($page - 1) * $item_per_page;

        $sql = 
        "SELECT '1',YEAR(periode) AS tahunGaji,SUBSTR(periode,6,2)AS bulanGaji,'11' AS kodeProvinsi,'00' AS kodeKabkota,
        0 AS jenisData,a.`lokasi_id` AS kodeSKPD,b.name AS uraianSKPD,a.lokasi_kode AS kodeSatkerSKPD,
        a.lokasi_string AS namaSatkerSKPD,a.nip AS nipPegawai,a.name AS namaPegawai,c.npwp AS npwp,
        a.status_id AS kodeStatusPegawai,a.status_string AS uraianStatusPegawai,LEFT(c.masa_kerja,2) AS masaKerja,
        IF(a.marital_string='TK','TIDAK KAWIN',IF(a.marital_string='K','KAWIN','JANDA DUDA')) AS namaStatusKawin,
        c.gender_id AS jenisKelamin,c.tanggal_lahir AS tanggalLahir,a.golongan_id AS kodeGolongan,d.name AS uraianGolongan,
        c.kedudukan_id AS kodeJabatanStruktural,e.name AS uraianjabatanStruktural,'00' AS kodeKelompokFungsional,
        '00' AS uraianKelompokFungsional,c.fungsional_id AS kodeJabatanFungsional, f.name AS uraianJabatanFungsional,
        '000' AS kodeJabatanKhusus,'-' AS uraianJabatanKhusus,'000' AS kodeGuru,'-' AS uraianGuru,'000' AS kodeSertifikasi,
        '-' AS uraianSertifikasi,istri AS jumlahIstriSuami,a.anak AS jumlahAnak,a.gaji_pokok AS gajiPokok,
        0 AS persenGaji,a.tunjangan_istri AS tunjanganIstriSuami,a.tunjangan_anak AS tunjanganAnak,
        0 AS tunjanganPerbaikanPenghasilan,a.tunjangan_struktural AS tunjanganStruktural,a.tunjangan_fungsional AS tunjanganFungsional,
        0 AS tunjanganJabatanKhusus,a.tunjangan_umum AS tunjanganUmum,0 AS tunjanganKemahalan,
        0 AS tunjanganTerpencil,a.askes AS tunjanganAskes,a.tunjangan_pph AS tunjanganPajak,
        a.pembulatan AS tunjanganPembulatan,a.tunjangan_beras AS tunjanganBeras,0 AS tunjanganPendidikan,
        g.tunjangan AS tunjanganEselon,0 AS tunjanganGuru,0 AS tunjanganKelangkaan, 0 AS tunjanganKhusus,
        (a.gaji_pokok * 0.0024) AS tunjanganJKK,(a.gaji_pokok * 0.0072) AS tunjanganJKM,jumlah_kotor AS jumlahKotor,
        potongan_iwp AS potonganIWP10,(potongan_iwp * 0.08) AS potonganIWP8,(potongan_iwp * 0.02) AS potonganIWP2, 
        askes AS potonganAskes,0 AS potonganBulog,
        potongan_lain AS potonganTaperum,tunjangan_pph AS potonganPajak,0 AS potonganSewaRumah,0 AS potonganHutang,
        (a.gaji_pokok * 0.0024) AS potonganJKK,(a.gaji_pokok * 0.0072) AS potonganJKM,jumlah_potongan AS jumlahPotongan,
        ((i.insentip+i.pph-i.potongan)+(i.konker_bruto+i.pph_konker)+(i.jakarta_bruto+i.pph_jakarta)) AS nilaiTambahanPenghasilan,jumlah_bersih AS jumlahBersih,'-' AS nomorRekening,'-' AS namaBank,'-' AS nomorTelepon,
        '-' AS alamatEmail,g.name AS kodeEselon,1 AS kodeBayarTP
        FROM kalkulasis a 
        LEFT JOIN lokasis b ON LEFT(a.lokasi_kode,2)=LEFT(b.kode,2) AND RIGHT(b.kode,6) LIKE '000000'
        LEFT JOIN pegawais c ON a.nip=c.nip 
        LEFT JOIN golongans d ON a.golongan_id=d.id 
        LEFT JOIN kedudukans e ON c.kedudukan_id=e.kode 
        LEFT JOIN fungsionals f ON c.fungsional_id=f.id
        LEFT JOIN eselons g ON c.eselon_id=g.id 
        LEFT JOIN statuses h ON c.status_id=h.id 
        LEFT JOIN (SELECT nip,insentip,pph,potongan,konker_bruto,pph_konker,jakarta_bruto,pph_jakarta FROM tpps 
        WHERE YEAR(periode)= ?
        AND MONTH(periode)= ?) i ON a.nip=i.nip
        LIMIT $offset, $item_per_page";

        $query = $this->dbgaji->query($sql, array($xtahun, $xbulan));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiP3k($periode, $page, $item_per_page){
        $pecahPeriode = explode('-', $periode);
        $xtahun = $pecahPeriode[0];
        $xbulan = $pecahPeriode[1];

        $offset = ($page - 1) * $item_per_page;

        $sql = 
        "SELECT '1',YEAR(periode) AS tahunGaji,SUBSTR(periode,6,2)AS bulanGaji,'11' AS kodeProvinsi,'00' AS kodeKabkota,
        0 AS jenisData,a.lokasi_id AS kodeSKPD,b.name AS uraianSKPD,a.lokasi_kode AS kodeSatkerSKPD,
        a.lokasi_string AS namaSatkerSKPD,a.nip AS nipPegawai,a.name AS namaPegawai,c.npwp AS npwp,
        a.status_id AS kodeStatusPegawai,a.status_string AS uraianStatusPegawai,LEFT(c.masa_kerja,2) AS masaKerja,
        IF(a.marital_string='TK','TIDAK KAWIN',IF(a.marital_string='K','KAWIN','JANDA DUDA')) AS namaStatusKawin,
        c.gender_id AS jenisKelamin,c.tanggal_lahir AS tanggalLahir,a.golongan_id AS kodeGolongan,d.name AS uraianGolongan,
        c.kedudukan_id AS kodeJabatanStruktural,e.name AS uraianjabatanStruktural,'00' AS kodeKelompokFungsional,
        '00' AS uraianKelompokFungsional,c.fungsional_id AS kodeJabatanFungsional, f.name AS uraianJabatanFungsional,
        '000' AS kodeJabatanKhusus,'-' AS uraianJabatanKhusus,'000' AS kodeGuru,'-' AS uraianGuru,'000' AS kodeSertifikasi,
        '-' AS uraianSertifikasi,istri AS jumlahIstriSuami,a.anak AS jumlahAnak,a.gaji_pokok AS gajiPokok,
        0 AS persenGaji,a.tunjangan_istri AS tunjanganIstriSuami,a.tunjangan_anak AS tunjanganAnak,
        0 AS tunjanganPerbaikanPenghasilan,a.tunjangan_struktural AS tunjanganStruktural,a.tunjangan_fungsional AS tunjanganFungsional,
        0 AS tunjanganJabatanKhusus,a.tunjangan_umum AS tunjanganUmum,0 AS tunjanganKemahalan,
        0 AS tunjanganTerpencil,a.askes AS tunjanganAskes,a.tunjangan_pph AS tunjanganPajak,
        a.pembulatan AS tunjanganPembulatan,a.tunjangan_beras AS tunjanganBeras,0 AS tunjanganPendidikan,
        g.tunjangan AS tunjanganEselon,0 AS tunjanganGuru,0 AS tunjanganKelangkaan, 0 AS tunjanganKhusus,
        (a.gaji_pokok * 0.0024) AS tunjanganJKK,(a.gaji_pokok * 0.0072) AS tunjanganJKM,jumlah_kotor AS jumlahKotor,
        potongan_iwp AS potonganIWP10,(potongan_iwp * 0.08) AS potonganIWP8,(potongan_iwp * 0.02) AS potonganIWP2, 
        askes AS potonganAskes,0 AS potonganBulog,
        potongan_lain AS potonganTaperum,tunjangan_pph AS potonganPajak,0 AS potonganSewaRumah,0 AS potonganHutang,
        (a.gaji_pokok * 0.0024) AS potonganJKK,(a.gaji_pokok * 0.0072) AS potonganJKM,jumlah_potongan AS jumlahPotongan,
        ((i.insentip+i.pph-i.potongan)+(i.konker_bruto+i.pph_konker)+(i.jakarta_bruto+i.pph_jakarta)) AS nilaiTambahanPenghasilan,jumlah_bersih AS jumlahBersih,'-' AS nomorRekening,'-' AS namaBank,'-' AS nomorTelepon,
        '-' AS alamatEmail,g.name AS kodeEselon,1 AS kodeBayarTP
        FROM kalkulasip3ks a 
        LEFT JOIN lokasis b ON LEFT(a.lokasi_kode,2)=LEFT(b.kode,2) AND RIGHT(b.kode,6) LIKE '000000'
        LEFT JOIN pegawaip3ks c ON a.nip=c.nip 
        LEFT JOIN golonganp3ks d ON a.golongan_id=d.id 
        LEFT JOIN kedudukans e ON c.kedudukan_id=e.kode 
        LEFT JOIN fungsionals f ON c.fungsional_id=f.id
        LEFT JOIN eselons g ON c.eselon_id=g.id 
        LEFT JOIN statuses h ON c.status_id=h.id 
        LEFT JOIN (SELECT nip,insentip,pph,potongan,konker_bruto,pph_konker,jakarta_bruto,pph_jakarta FROM tppp3ks 
        WHERE YEAR(periode)= ?
        AND MONTH(periode)=?) i ON a.nip=i.nip
        LIMIT $offset, $item_per_page";

        $query = $this->dbgaji->query($sql, array($xtahun, $xbulan));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiPns14($periode, $page, $item_per_page){
        $pecahPeriode = explode('-', $periode);
        $xtahun = $pecahPeriode[0];
        $xbulan = $pecahPeriode[1];

        if(substr($xbulan, 0, 1) === '0'){
            $xbulan = substr($xbulan, -1);
        }
        // $tablePegawai = 'pegawai_'. $xtahun .'_' . $xbulan . '_'; //bawaan BPKAD
        $tablePegawai = 'pegawai_14_'. $xtahun .'_' . $xbulan . '_';

        $offset = ($page - 1) * $item_per_page;

        $sql = 
        "SELECT '1',YEAR(periode) AS tahunGaji,SUBSTR(periode,6,2)AS bulanGaji,'11' AS kodeProvinsi,'00' AS kodeKabkota,
        0 AS jenisData,b.kode AS kodeSKPD,b.name AS uraianSKPD,a.lokasi_kode AS kodeSatkerSKPD,
        a.lokasi_string AS namaSatkerSKPD,a.nip AS nipPegawai,a.name AS namaPegawai,REPLACE(REPLACE(c.npwp,'.',''),'-','') AS npwp,
        a.status_id AS kodeStatusPegawai,a.status_string AS uraianStatusPegawai,LEFT(c.masa_kerja,2) AS masaKerja,
        IF(a.marital_string='TK','TIDAK KAWIN',IF(a.marital_string='K','KAWIN','JANDA DUDA')) AS namaStatusKawin,
        c.gender_id AS jenisKelamin,c.tanggal_lahir AS tanggalLahir,a.golongan_id AS kodeGolongan,d.name AS uraianGolongan,
        c.kedudukan_id AS kodeJabatanStruktural,e.name AS uraianjabatanStruktural,'00' AS kodeKelompokFungsional,
        '00' AS uraianKelompokFungsional,c.fungsional_id AS kodeJabatanFungsional, f.name AS uraianJabatanFungsional,
        '000' AS kodeJabatanKhusus,'-' AS uraianJabatanKhusus,'000' AS kodeGuru,'-' AS uraianGuru,'000' AS kodeSertifikasi,
        '-' AS uraianSertifikasi,istri AS jumlahIstriSuami,a.anak AS jumlahAnak,a.gaji_pokok AS gajiPokok,
        0 AS persenGaji,a.tunjangan_istri AS tunjanganIstriSuami,a.tunjangan_anak AS tunjanganAnak,
        0 AS tunjanganPerbaikanPenghasilan,a.tunjangan_struktural AS tunjanganStruktural,a.tunjangan_fungsional AS tunjanganFungsional,
        0 AS tunjanganJabatanKhusus,a.tunjangan_umum AS tunjanganUmum,0 AS tunjanganKemahalan,
        0 AS tunjanganTerpencil,a.askes AS tunjanganAskes,a.tunjangan_pph AS tunjanganPajak,
        a.pembulatan AS tunjanganPembulatan,a.tunjangan_beras AS tunjanganBeras,0 AS tunjanganPendidikan,
        g.tunjangan AS tunjanganEselon,0 AS tunjanganGuru,0 AS tunjanganKelangkaan, 0 AS tunjanganKhusus,
        (a.gaji_pokok * 0.0024) AS tunjanganJKK,(a.gaji_pokok * 0.0072) AS tunjanganJKM,jumlah_kotor AS jumlahKotor,
        potongan_iwp AS potonganIWP10,(potongan_iwp * 0.08) AS potonganIWP8,(potongan_iwp * 0.02) AS potonganIWP2, 
        askes AS potonganAskes,0 AS potonganBulog,
        potongan_lain AS potonganTaperum,tunjangan_pph AS potonganPajak,0 AS potonganSewaRumah,0 AS potonganHutang,
        0 AS potonganJKK,0 AS potonganJKM,jumlah_potongan AS jumlahPotongan,
        0 AS nilaiTambahanPenghasilan,jumlah_bersih AS jumlahBersih,'-' AS nomorRekening,'-' AS namaBank,'-' AS nomorTelepon,
        '-' AS alamatEmail,g.name AS kodeEselon,1 AS kodeBayarTP
        FROM kalkulasiebs a 
        LEFT JOIN lokasis b ON LEFT(a.lokasi_kode,2)=LEFT(b.kode,2) AND RIGHT(b.kode,6) LIKE '000000'
        LEFT JOIN $tablePegawai c ON a.nip=c.nip 
        LEFT JOIN golongans d ON a.golongan_id=d.id 
        LEFT JOIN kedudukans e ON c.kedudukan_id=e.kode 
        LEFT JOIN fungsionals f ON c.fungsional_id=f.id
        LEFT JOIN eselons g ON c.eselon_id=g.id 
        LEFT JOIN statuses h ON c.status_id=h.id
        -- WHERE a.`periode` = '$periode'
        LIMIT $offset, $item_per_page";

        $query = $this->dbgaji->query($sql, array($xtahun, $xbulan));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiP3k14($periode, $page, $item_per_page){
        $pecahPeriode = explode('-', $periode);
        $xtahun = $pecahPeriode[0];
        $xbulan = $pecahPeriode[1];

        if(substr($xbulan, 0, 1) === '0'){
            $xbulan = substr($xbulan, -1);
        }
        $tablePegawai = 'pegawaip3k_14_'. $xtahun .'_' . $xbulan . '_';

        $offset = ($page - 1) * $item_per_page;

        $sql = 
        "SELECT '1',YEAR(periode) AS tahunGaji,SUBSTR(periode,6,2)AS bulanGaji,'11' AS kodeProvinsi,'00' AS kodeKabkota,
        0 AS jenisData,b.kode AS kodeSKPD,b.name AS uraianSKPD,a.lokasi_kode AS kodeSatkerSKPD,
        a.lokasi_string AS namaSatkerSKPD,a.nip AS nipPegawai,a.name AS namaPegawai,REPLACE(REPLACE(c.npwp,'.',''),'-','') AS npwp,
        a.status_id AS kodeStatusPegawai,a.status_string AS uraianStatusPegawai,LEFT(c.masa_kerja,2) AS masaKerja,
        IF(a.marital_string='TK','TIDAK KAWIN',IF(a.marital_string='K','KAWIN','JANDA DUDA')) AS namaStatusKawin,
        c.gender_id AS jenisKelamin,c.tanggal_lahir AS tanggalLahir,a.golongan_id AS kodeGolongan,d.name AS uraianGolongan,
        c.kedudukan_id AS kodeJabatanStruktural,e.name AS uraianjabatanStruktural,'00' AS kodeKelompokFungsional,
        '00' AS uraianKelompokFungsional,c.fungsional_id AS kodeJabatanFungsional, f.name AS uraianJabatanFungsional,
        '000' AS kodeJabatanKhusus,'-' AS uraianJabatanKhusus,'000' AS kodeGuru,'-' AS uraianGuru,'000' AS kodeSertifikasi,
        '-' AS uraianSertifikasi,istri AS jumlahIstriSuami,a.anak AS jumlahAnak,a.gaji_pokok AS gajiPokok,
        0 AS persenGaji,a.tunjangan_istri AS tunjanganIstriSuami,a.tunjangan_anak AS tunjanganAnak,
        0 AS tunjanganPerbaikanPenghasilan,a.tunjangan_struktural AS tunjanganStruktural,a.tunjangan_fungsional AS tunjanganFungsional,
        0 AS tunjanganJabatanKhusus,a.tunjangan_umum AS tunjanganUmum,0 AS tunjanganKemahalan,
        0 AS tunjanganTerpencil,a.askes AS tunjanganAskes,a.tunjangan_pph AS tunjanganPajak,
        a.pembulatan AS tunjanganPembulatan,a.tunjangan_beras AS tunjanganBeras,0 AS tunjanganPendidikan,
        g.tunjangan AS tunjanganEselon,0 AS tunjanganGuru,0 AS tunjanganKelangkaan, 0 AS tunjanganKhusus,
        (a.gaji_pokok * 0.0024) AS tunjanganJKK,(a.gaji_pokok * 0.0072) AS tunjanganJKM,jumlah_kotor AS jumlahKotor,
        potongan_iwp AS potonganIWP10,(potongan_iwp * 0.08) AS potonganIWP8,(potongan_iwp * 0.02) AS potonganIWP2, 
        askes AS potonganAskes,0 AS potonganBulog,
        potongan_lain AS potonganTaperum,tunjangan_pph AS potonganPajak,0 AS potonganSewaRumah,0 AS potonganHutang,
        0 AS potonganJKK,0 AS potonganJKM,jumlah_potongan AS jumlahPotongan,
        0 AS nilaiTambahanPenghasilan,jumlah_bersih AS jumlahBersih,'-' AS nomorRekening,'-' AS namaBank,'-' AS nomorTelepon,
        '-' AS alamatEmail,g.name AS kodeEselon,1 AS kodeBayarTP
        FROM kalkulasiebp3ks a 
        LEFT JOIN lokasis b ON LEFT(a.lokasi_kode,2)=LEFT(b.kode,2) AND RIGHT(b.kode,6) LIKE '000000'
        LEFT JOIN $tablePegawai c ON a.nip=c.nip 
        LEFT JOIN golonganp3ks d ON a.golongan_id=d.id 
        LEFT JOIN kedudukans e ON c.kedudukan_id=e.kode 
        LEFT JOIN fungsionals f ON c.fungsional_id=f.id
        LEFT JOIN eselons g ON c.eselon_id=g.id 
        LEFT JOIN statusp3ks h ON c.status_id=h.id
        WHERE a.`periode` = '$periode'
        LIMIT $offset, $item_per_page";

        $query = $this->dbgaji->query($sql, array($xtahun, $xbulan));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function update_token($table, $stakeholder_code, $data){
        $this->db->where('stakeholder_code', $stakeholder_code);
		$delete = $this->db->update($table, $data); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    private function validate_token($stakeholder_code, $token, $tgl){
        $sql = 
        "SELECT *
        FROM simgaji_token
        WHERE `stakeholder_code` = ?
        AND token = ?
        AND date(created_at) = ?";

        $query = $this->db->query($sql, array($stakeholder_code, $token, $tgl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}