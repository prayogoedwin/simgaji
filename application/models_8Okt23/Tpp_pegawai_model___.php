<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_pegawai_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->tb_pegawai = 'simgaji_pegawai_tpp';

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->dbsinaga = $this->load->database('sinaga', TRUE);
    }

    public function getDataPegawai_($kode_lokasis){
        $duadigit = substr($kode_lokasis, 0, 2);
        if($duadigit != '34'){
            $par = $duadigit.'%';
        }

        $sql = 
        "SELECT *
        FROM $this->tb_pegawai
        WHERE lokasi LIKE ?";

        $query = $this->db->query($sql, array($par));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }
        return $data;
    }

    public function getDataPegawai($query,$cari,$where,$iswhere, $statusSkpd){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}");

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);

            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';

            if($statusSkpd == '2'){
              $klik_nip = '<a href="'. base_url('tpp_pegawai/detail/') . encode_url($value['id']) .'">'. $value['nip'] .'</a>';
            } else {
              $klik_nip = $value['nip'];
            }

			$datatable[$key] = array(
                'name' => $value['name'],
                'nip' => $klik_nip,
                'aksi' => '<a class="btn btn-dark btn-sm" href="'. base_url('tpp_pegawai/data/') . encode_url($value['id']) .'">Lihat Data</a>'
			);
		}

        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_count,
            'recordsFiltered'=>$sql_filter_count,
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getDetailById($id){
        $sql =
        "SELECT *
        FROM $this->tb_pegawai
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataKodes(){
        $sql =
        "SELECT *
        FROM `simgaji_kodes`
        WHERE bool_id_tpp = '2'
        ORDER BY sort";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataDataMaster($table){
        $sql =
        "SELECT *
        FROM $table";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getSKPDInduk(){
        $sql = 
        "SELECT *
        FROM TABLOK08
        WHERE aktif = '1'
        AND kd != '99'
        AND urutuk IS NOT NULL
        ORDER BY urutuk ASC";

        $query = $this->dbeps->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataPegawaiForCabdin($lokasigaji){
        $enamdigitkodelokasi = substr($lokasigaji, 0, 6);
        $parentkodelokasi = $enamdigitkodelokasi . '00';

        $sql =
        "SELECT a.*
        FROM simgaji_pegawai_tpp a
        INNER JOIN simgaji_lokasis b ON a.lokasi_gaji = b.id
        WHERE b.kode LIKE '$enamdigitkodelokasi%'
        ";
        // AND lokasigaji != '$parentkodelokasi'

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getKodeLokasisById($lokasis_id){
        $sql =
        "SELECT *
        FROM simgaji_lokasis
        WHERE id = ?";
  
        $query = $this->db->query($sql, array($lokasis_id));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
  
        return $data;
      }

      public function getDataPegawaiTuSekolah($lokasis_id){
        $sql =
        "SELECT *
        FROM `simgaji_pegawai_tpp`
        WHERE lokasi_gaji = ?";
        // $x = $kode . '%';

        // $sql =
        // "SELECT *
        // FROM `simgaji_pegawaip3ks`
        // WHERE lokasigaji LIKE ?";

        $query = $this->db->query($sql, array($lokasis_id));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getNIPCabdinByNipKatuSekolah($nip){
        $sql =
        "SELECT tb.KOLOK, tb.NALOKP
        FROM TABLOKB08 tb, MASTFIP08 m
        WHERE m.B_02B = ?
        AND m.A_01 = tb.A_01
        AND m.A_02 = tb.A_02
        AND m.A_03 = tb.A_03
        AND tb.A_04 = '10'
        AND tb.A_05 = '00'";
  
        $query = $this->dbeps->query($sql, array($nip));
        if($query->num_rows() > 0){
            $kolok = $query->row()->KOLOK;
        }else{
            $kolok = '%';
        }
       
  
        $sql2 =
        "SELECT B_02B
        FROM MASTFIP08
        WHERE CONCAT(A_01,A_02,A_03,A_04,A_05) = ?
        AND I_5A = 1";
  
        $query2 = $this->dbeps->query($sql2, array($kolok));
        if($query2->num_rows() > 0){
          return $query2->row()->B_02B;
        } else {
          // return null;
  
          $sql_pengganti =
          "SELECT *
          FROM pejabat_pengganti
          WHERE unitkerja = ?";
  
          $query_pengganti = $this->dbsinaga->query($sql_pengganti, array($kolok));
          if($query_pengganti->num_rows() > 0){
            return 'PLT/PLH ' . $query_pengganti->row()->nip_pengganti;
          } else {
            return null;
          }
        }
      }

    public function getDropCabdinIn($kode){
        $sql =
        "SELECT *
        FROM `simgaji_lokasis`
        WHERE kode in($kode)";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function geJabatan($i_5A){

        if($i_5A == 1){

            $struktural = $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE AKTIF = 1");
            if ($struktural->num_rows() > 0) {
                $data = $struktural->result();
            } else {
                $data = array();
            }

        }else if($i_5A == 2){

            $fungsional = $this->dbeps->query("SELECT a.*, b.* FROM TABFNG2 a INNER JOIN petafngsapk b ON a.CEPAT_KODE = b.CEPAT_KODE ");
            if ($fungsional->num_rows() > 0) {
                $data = $fungsional->result();
            } else {
                $data = array();
            }

        }else if($i_5A == 0){

            $staff = $this->dbeps->query("SELECT * FROM TABJFU15");
            if ($staff->num_rows() > 0) {
                $data = $staff->result();
            } else {
                $data = array();
            }

        }else{
            $data = array();
        }

        return $data;

    }

    public function geJabatanDetail($i_5A, $i_05){

        if($i_5A == 1){

            $struktural = $this->dbeps->query("SELECT * FROM TABLOKB08 WHERE KOLOK = '$i_05';");
            if ($struktural->num_rows() > 0) {
                $data = $struktural->row();
            } else {
                $data = array();
            }

        }else if($i_5A == 2){

            $fungsional = $this->dbeps->query("SELECT a.*, b.* FROM TABFNG2 a INNER JOIN petafngsapk b ON a.CEPAT_KODE = b.CEPAT_KODE WHERE a.CEPAT_KODE = '$i_05';");
            if ($fungsional->num_rows() > 0) {
                $data = $fungsional->row();
            } else {
                $data = array();
            }

        }else if($i_5A == 0){

            $staff = $this->dbeps->query("SELECT * FROM TABJFU15 WHERE KOJFU = '$i_05';");
            if ($staff->num_rows() > 0) {
                $data = $staff->row();
            } else {
                $data = array();
            }

        }else{
            $data = array();
        }

        return $data;

    }

    public function getJabatanBySKPD($A_01){

        $x = $A_01;

        $sql = 
        "SELECT
        m.*,
        s.*,
        u.*,
        f.*,
        if(KD_JAB = 1, m.KELAS, if(KD_JAB = 2, f.KELAS, u.KELAS)) KELASJAB,
            count(p.ID_FORMASI) AS bez
        FROM
            MASTFORMASI18 m
        LEFT JOIN TABLOKB08 s ON m.KD_FORMASI = s.KOLOK
        LEFT JOIN TABFNG2 f ON m.KD_FORMASI = f.CEPAT_KODE
        LEFT JOIN TABJFU15 u ON m.KD_FORMASI = u.KOJFU
        LEFT JOIN PEGAWAI_JFU18 p ON m.ID_FORMASI = p.ID_FORMASI
    WHERE
        LEFT(UNIT_KERJA,2) = ?
    GROUP BY
        m.KD_FORMASI
    ORDER BY
        UNIT_KERJA,
        field(KD_JAB, 1, 2, 0),
        KELASJAB DESC,
        f.NAMA,
        u.NAJFU";

        $query = $this->dbeps->query($sql, array($x));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function petafngsapk($cepat_kode){


        $sql = 
        "SELECT * FROM petafngsapk WHERE CEPAT_KODE = ?";

        $query = $this->dbeps->query($sql, array($cepat_kode));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = FALSE;
        }

        return $data;
    }

    public function mastformasi18($cepat_kode){
        $sql = 
        "SELECT * FROM MASTFORMASI18 WHERE KD_FORMASI = ?";

        $query = $this->dbeps->query($sql, array($cepat_kode));
        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = FALSE;
        }

        return $data;
    }

    public function getJabatanBySKPDforData($A_01){

        $x = $A_01.'%';

        $sql = 
        "SELECT
        m.KD_FORMASI as id,
        CASE
            WHEN KD_JAB = 2 THEN f.NAMA
            WHEN KD_JAB = 1 THEN s.NALOK
            WHEN KD_JAB = 0 THEN u.NAJFU
        END  as `name`,
        m.*,
        s.*,
        u.*,
        f.*,
        if(KD_JAB = 1, m.KELAS, if(KD_JAB = 2, f.KELAS, u.KELAS)) KELASJAB,
            count(p.ID_FORMASI) AS bez
        FROM
            MASTFORMASI18 m
        LEFT JOIN TABLOKB08 s ON m.KD_FORMASI = s.KOLOK
        LEFT JOIN TABFNG2 f ON m.KD_FORMASI = f.CEPAT_KODE
        LEFT JOIN TABJFU15 u ON m.KD_FORMASI = u.KOJFU
        LEFT JOIN PEGAWAI_JFU18 p ON m.ID_FORMASI = p.ID_FORMASI
    WHERE
        UNIT_KERJA LIKE ?
    GROUP BY
        m.ID_FORMASI
    ORDER BY
        UNIT_KERJA,
        field(KD_JAB, 1, 2, 0),
        KELASJAB DESC,
        f.NAMA,
        u.NAJFU";

        $query = $this->dbeps->query($sql, array($x));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
}