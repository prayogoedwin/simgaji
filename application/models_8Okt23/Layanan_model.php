<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layanan_model extends CI_Model{
  public $dbeps = NULL;
  public $dbgaji = NULL;

  public function __construct(){
	   parent::__construct();

      $this->dbeps = $this->load->database('eps', TRUE);
      $this->dbgaji = $this->load->database('gaji', TRUE);
  }

  public function getGajiTpp($nip, $tahun, $bulan){
    $sql =
    "SELECT nip, tahun, bulan, nominalskp, nominalperilaku, nominaltpp, nominalkurang, jam, hukdis, ket
    FROM masttpp_kinerja
    WHERE nip = ?
    AND tahun = ?
    AND bulan = ?";

    $query = $this->dbgaji->query($sql, array($nip, $tahun, $bulan));
    if($query->num_rows() > 0){
      $data = $query->row();
    } else {
      $data = null;
    }

    return $data;
  }

  public function getGajiKalkulasis($nip, $periode){
    $sql =
    "SELECT nip nip_kalkulasis, periode periode_kalkulasis, jumlah_bersih_bayar gaji, gaji_pokok, tunjangan_anak, tunjangan_istri, tunjangan_umum, tunjangan_umum_tambahan, tunjangan_struktural, tunjangan_fungsional, tunjangan_beras, tunjangan_pph, pembulatan, potongan_iwp, potongan_lain, potongan_beras, potongan_cp, jumlah_bersih
    FROM kalkulasis
    WHERE nip = ?
    AND periode = ?";

    $query = $this->dbgaji->query($sql, array($nip, $periode));
    if($query->num_rows() > 0){
      $data = $query->row();
    } else {
      $data = null;
    }

    return $data;
  }
}
