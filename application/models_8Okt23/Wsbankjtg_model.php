<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbankjtg_model extends CI_Model{

    var $client_apikey = "BKDJATENG";
    // var $auth       = "bb0b37ad-d57b-44da-8b8b-db0bf32727d1";

    public $dbgaji = NULL;
    public $dbgaji_prod = NULL;
    public $dbgaji_pns = NULL;

    public function __construct(){
        parent::__construct();

        $this->dbgaji = $this->load->database('gaji', TRUE);
        $this->dbgaji_prod = $this->load->database('gaji_prod', TRUE);
        $this->dbgaji_pns = $this->load->database('gaji_pns', TRUE);
    }

    public function check_auth_client(){
        $auth  = $this->input->get_request_header('Authorization', TRUE);
        $client_apikey = $this->input->get_request_header('x-api-key', TRUE);

        $tgl = date('Y-m-d');
        $auth_generate = $this->validate_token('1', $auth, $tgl);

        if($auth_generate == null){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized token'));
        } else

        if($client_apikey == $this->client_apikey && $auth == $auth_generate->token){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function getGajiPnsByUk($periode, $uk){
        $this->dbgaji->select('pegawais.nik as NIK, periode as periode_gaji, lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,kalkulasis.nip AS NIP,kalkulasis.NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->join('pegawais', 'pegawais.nip = kalkulasis.nip');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('kalkulasis.golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kalkulasis.kelompok_gaji', 'DESC');
        $data = $this->dbgaji->get('kalkulasis')->result();

        return $data;
    }

    public function getGajiP3kByUk($periode, $uk){
        $this->dbgaji_prod->select('periode as periode_gajip3k,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji, nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji_prod->where('periode', $periode);
        $this->dbgaji_prod->like('lokasi_kode', $uk, 'after');
        $this->dbgaji_prod->order_by('lokasi_kode');
        $this->dbgaji_prod->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji_prod->order_by('golongan_id', 'DESC');
        $this->dbgaji_prod->order_by('nip', 'ASC');
        $this->dbgaji_prod->order_by('kelompok_gaji', 'DESC');
        // return $this->dbgaji->get('kalkulasip3ks')->result();
        return $this->dbgaji_prod->get('simgaji_kalkulasip3ks')->result();
    }

    public function getGaji13PnsByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gaji13,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasitbs')->result();
    }

    public function getGaji13P3kByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gajip3k13,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasitbp3ks')->result();
    }

    public function getGajiSusulanCpnsByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gajisusulan,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,status_string as status, golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasisusulans')->result();
    }

    public function getGajiSusulanP3kByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gaji_susulanp3k,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama, status_string as status,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasip3ksusulans')->result();
    }

    public function getGajiTppPnsByUk($periode, $uk){
        // $this->dbgaji->select('periode as periode_tpp,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,tpp AS TPP,konker AS TPP_KONDISI_KERJA,jakarta AS TPP_TEMPAT_BEKERJA');
        // $this->dbgaji->where('periode', $periode);
        // $this->dbgaji->like('lokasi_kode', $uk, 'after');
        // $this->dbgaji->order_by('lokasi_kode');
        // $this->dbgaji->order_by('eselon_id', 'DESC');
        // $this->dbgaji->order_by('golongan_id', 'DESC');
        // $this->dbgaji->order_by('nip', 'ASC');
        // $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        // return $this->dbgaji->get('tpps')->result();

        $s = $uk . "%";

        $sql = 
        "SELECT
        tpp.`periode` AS `periode_tpp`,
        tpp.`lokasi` AS `kode_lokasi`,
        tpp.`lokasi_string` AS `lokasi_gaji`,
        tpp.`nip` AS `nip`,
        tpp.`NAME` AS `nama`,
        tpp.`golongan_string` AS `golongan`,
        tpp.`jabatan_string` AS `jabatan`,
        tpp.`kelas_jabatan`,
        tpp.`beban_kerja_nominal` AS `TPP_BEBAN_KERJA`,
        tpp.`beban_kerja_khusus_nominal` AS `TPP_BEBAN_KERJA_KHUSUS`,
        tpp.`tempat_nominal` AS `TPP_TEMPAT`,
        tpp.`kondisi_nominal` AS `TPP_KONDISI`,
        tpp.`plt_nominal` AS `TPP_PLT`
        FROM
            `simgaji_kalkulasi_tpp` tpp
        WHERE
            tpp.`periode` = ?
            AND tpp.`lokasi` LIKE ? ESCAPE '!' 
        ORDER BY
            tpp.`beban_kerja` DESC,
            tpp.`eselon_id` DESC,
            tpp.`golongan_id` DESC,
            tpp.`nip` ASC";

        $query = $this->dbgaji_pns->query($sql, array($periode, $s));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiTppP3kByUk($periode, $uk){

        $s = $uk . "%";

        $sql = 
        "SELECT
        tpp.`periode` AS `periode_tpp`,
        tpp.`lokasi` AS `kode_lokasi`,
        tpp.`lokasi_string` AS `lokasi_gaji`,
        tpp.`nip` AS `nip`,
        tpp.`NAME` AS `nama`,
        tpp.`golongan_string` AS `golongan`,
        tpp.`jabatan_string` AS `jabatan`,
        tpp.`kelas_jabatan`,
        tpp.`beban_kerja_nominal` AS `TPP_BEBAN_KERJA`,
        tpp.`beban_kerja_khusus_nominal` AS `TPP_BEBAN_KERJA_KHUSUS`,
        tpp.`tempat_nominal` AS `TPP_TEMPAT`,
        tpp.`kondisi_nominal` AS `TPP_KONDISI`,
        tpp.`plt_nominal` AS `TPP_PLT`
        FROM
            gaji_prod.`simgaji_kalkulasi_tpp` tpp
        JOIN
            gaji_prod.`simgaji_pegawai_tppp3ks` peg ON tpp.`nip` = peg.`nip`
        WHERE
            tpp.`periode` = ?
            AND tpp.`lokasi` LIKE ? ESCAPE '!' 
        ORDER BY
            peg.`KOLOK`,
            tpp.`beban_kerja` DESC,
            tpp.`eselon_id` DESC,
            tpp.`golongan_id` DESC,
            tpp.`nip` ASC";

        $query = $this->dbgaji_pns->query($sql, array($periode, $s));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiTppPns13ByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_tpp_THR,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,tpp AS TPP');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('eselon_id', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('tpptbs')->result();
    }

    public function getGajiPns14ByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gaji14,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasiebs')->result();
    }

    public function update_token($table, $stakeholder_code, $data){
        $this->db->where('stakeholder_code', $stakeholder_code);
		$delete = $this->db->update($table, $data); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    private function validate_token($stakeholder_code, $token, $tgl){
        $sql = 
        "SELECT *
        FROM simgaji_token
        WHERE `stakeholder_code` = ?
        AND token = ?
        AND date(created_at) = ?";

        $query = $this->db->query($sql, array($stakeholder_code, $token, $tgl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}
