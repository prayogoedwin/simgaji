<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eselon_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->eselons = 'eselons a';

        
    }

    //CRUD SOTK START//
    var $eselon_column_order = array('a.id', 'a.kode', 'a.name','a.tunjangan','a.usia'); //set column field database for datatable orderable
    var $eselon_column_search = array('a.kode', 'a.name','a.tunjangan','a.usia'); //set column field database for datatable searchable
    var $eselon_order = array('a.id' => 'asc'); // default order 

    private function _get_eselon_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->eselons);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->eselon_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->eselon_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->eselon_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->eselon_order)) {
            $order = $this->eselon_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_eselon()
    {
        $this->_get_eselon_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function eselon_count_filtered()
    {
        $this->_get_eselon_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function eselon_count_all()
    {
        $this->db->from($this->eselons);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getStrukturalById($id){
        $sql = 
        "SELECT *
        FROM $this->eselons
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
     //CRUD SOTK END//


    
}