<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{
    public $dbeps = NULL;
    public $db = NULL;

    public function __construct(){
		parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function cek_login_sinaga($nip, $password) {

        $md5_pass = md5($password);

        $sql =
        "SELECT *
        FROM USER_NEW
        WHERE username = ?
        AND password = ?";

        $query_cek = $this->dbeps->query($sql, array($nip, $md5_pass));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();

        } else {
            $data = null;
        }

        return $data;
    }

    public function cekLoginDummy($nip){
        $sql =
        "SELECT *
        FROM USER_NEW
        WHERE username = ?";

        $query_cek = $this->dbeps->query($sql, array($nip));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function cekMyRole($nip){
        $sql =
        "SELECT *
        FROM `simgaji_verifikator`
        WHERE nip = ? AND deleted_at IS NULL";

        $query_cek = $this->db->query($sql, array($nip));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function cekMyRoleAtUsers($nip){
        $sql =
        "SELECT *
        FROM `simgaji_users`
        WHERE nip = ?";

        $query_cek = $this->db->query($sql, array($nip));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row()->type_role;
        } else {
            $data = null;
        }

        return $data;
    }

    public function cekUserSuperadminExternal($username, $password){
        $pass = md5($password);

        $sql =
        "SELECT *
        FROM `simgaji_verifikator`
        WHERE nip = ?
        AND password = ?";

        $query_cek = $this->db->query($sql, array($username, $pass));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
        } else {
            $data = null;
        }

        return $data;
    }


    public function cekUserSuperadminExternalDummy($username, $password){
        $pass = md5($password);

        $sql =
        "SELECT *
        FROM `simgaji_verifikator`
        WHERE nip = ?";

        $query_cek = $this->db->query($sql, array($username));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row();
        } else {
            $data = null;
        }

        return $data;
    }
}
