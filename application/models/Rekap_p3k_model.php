<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekap_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDataPeriode($query,$cari,$where,$iswhere,$isGroupBy){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}"); 

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);
                
            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";
            
            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column']; 

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir']; 
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")" . " GROUP BY " . $isGroupBy .$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")". " GROUP BY " . $isGroupBy . $order ." LIMIT ".$limit." OFFSET ".$start);
            }
            
            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";
            
            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column']; 

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir']; 
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {                
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {     
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }

            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';
            // $klik_periode = '<a href="'. base_url('history_p3k/periode/') . $value['periode'] .'">'. $value['periode'] .'</a>';
            
            $per = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
            if($value['periode'] != $per){
                $period = $value['periode'];
            } else {
                $period = '<a href="'. base_url('rekap_p3k/periode/') . $value['periode'] .'">'. $value['periode'] .'</a>';
            }

			$datatable[$key] = array(
                'periode' => $period
			);
		}
        
        $callback = array(    
            'draw' => $_POST['draw'], // Ini dari datatablenya    
            'recordsTotal' => $sql_count,    
            'recordsFiltered'=>$sql_filter_count,    
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getDataByPeriode($user_id, $periode){
        // $sql = 
        // "SELECT *
        // FROM `simgaji_historyp3ks`
        // WHERE deleted_at IS NULL 
        // AND posted_by = ?
        // AND periode = ?
        // GROUP BY `pegawai_id`";

        $sql = 
        "SELECT h.* , p.`nip`, p.`name`, k.`name` mutasi
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        JOIN `simgaji_kodes` k ON h.`kode_id` = k.`id`
        WHERE h.`deleted_at` IS NULL 
        AND h.`posted_by` = ?
        AND h.`periode` = ?
        ORDER BY h.`pegawai_id`";

        // GROUP BY h.`pegawai_id`

        $query = $this->db->query($sql, array($user_id, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataKumulatifByPeriode($user_id, $periode){
        $sql = 
        "SELECT p.`id`, p.nip, p.name, SUM(IF(h.status_acc_bkd = 0, 1, 0)) as belum, SUM(IF(h.status_acc_bkd = 2, 1, 0)) as revisi, SUM(IF(h.status_acc_bkd = 3, 1, 0)) as tolak, SUM(IF(h.status_acc_bkd = 1, 1, 0)) as setuju
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        AND h.`posted_by` = ?
        AND h.`periode` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($user_id, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
}