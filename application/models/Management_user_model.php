<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Management_user_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

   

    public function getDataUsers($query,$cari,$where,$iswhere){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}"); 

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);
                
            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";
            
            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column']; 

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir']; 
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }
            
            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";
            
            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column']; 

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir']; 
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {                
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {     
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';
            $klik_nip = '<a href="'. base_url('management_user/detail/') . encode_url($value['id']) .'">'. $value['nip'] .'</a>';

            $teksRole = '';
            switch ($value['type_role']) {
                case 1:
                    $teksRole = 'Superadmin';
                    break;
                case 3:
                    $teksRole = 'Admin SKPD';
                    break;
                case 4:
                    $teksRole = 'Admin UPT / Sekolah';
                    break;
                default:
                    $teksRole = '-';
                    break;
            }

			$datatable[$key] = array(
                'nip_nama' => $klik_nip . '</br>' . $value['nama'],
                'skpd' => $value['skpd'],
                'role' => $teksRole
			);
		}
        
        $callback = array(    
            'draw' => $_POST['draw'], // Ini dari datatablenya    
            'recordsTotal' => $sql_count,    
            'recordsFiltered'=>$sql_filter_count,    
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getUnitOrganisasi(){
        $sql = 
        "SELECT KOLOK,NALOK
        FROM TABLOKB08
        WHERE (KOLOK LIKE '%00000000' OR (A_01='A2' AND ESEL='22'))
        AND AKTIF=1
        ORDER BY KOLOK";
        
        $query = $this->dbeps->query($sql);
        
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        
        return $data;
    }

    public function getSubUnitByUnor($unor_id){
        $sql = 
        "SELECT KOLOK,NALOK
        FROM TABLOKB08
        WHERE AKTIF = 1
        AND (ATASAN = '$unor_id' OR KOLOK = '$unor_id')
        ORDER BY KOLOK";
        
        $query = $this->dbeps->query($sql);
        
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }
        
        return $data;
    }

    public function getDataPns($nip){
        $x = '%'.$nip.'%';
        $sql =
        "SELECT CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.`B_02B` id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
        FROM MASTFIP08 mastfip
        JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
        JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
        JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
        WHERE 
        (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
        AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";

        $query = $this->dbeps->query($sql, array($nip, $x));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
}