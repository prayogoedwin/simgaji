<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_usulan_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDataHistoryP3KPeriode($query,$cari,$where,$iswhere,$isGroupBy){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}");

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);

            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")" . " GROUP BY " . $isGroupBy .$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")". " GROUP BY " . $isGroupBy . $order ." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }

            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';
            // $klik_periode = '<a href="'. base_url('history_p3k/periode/') . $value['periode'] .'">'. $value['periode'] .'</a>';

            $per = getDataPeriode()->tahun . '-' . getDataPeriode()->bulan . '-01';
            if($value['periode'] != $per){
                $period = $value['periode'];
            } else {
                $period = '<a href="'. base_url('approval_usulan_p3k/periode/') . $value['periode'] .'">'. $value['periode'] .'</a>';
            }



			$datatable[$key] = array(
                'periode' => $period
			);
		}

        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_count,
            'recordsFiltered'=>$sql_filter_count,
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function getStatusAllDataByPeriode($periode, $mynip){
        $sql =
        "SELECT p.`id`, h.*
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`periode` = ?
        AND h.`posisi_acc` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $mynip));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataHistory($A_01, $periode, $B_02B){
        // $sql =
        // "SELECT *
        // FROM simgaji_historyp3ks_acc
        // WHERE A_01 = ?
        // AND periode = ?
        // AND nip = ?
        // GROUP BY nip";

        // $sql =
        // "SELECT *
        // FROM simgaji_historyp3ks_acc
        // WHERE periode = ?
        // AND (nip = ? OR A_01 = ?)
        // GROUP BY nip
        // ORDER BY id ASC";

        $sql =
        "SELECT h.*
        FROM gaji_prod.simgaji_historyp3ks_acc h
        WHERE h.periode = ?
        AND (h.nip = ? OR h.nip = (SELECT B_02B FROM eps.MASTFIP08 WHERE A_01 = ? AND A_02 = '00' AND A_03 = '10' AND A_04 = '20' AND A_05 = '00' AND I_5A = '1'))
        AND deleted_at IS NULL";

        $query = $this->db->query($sql, array($periode, $B_02B, $A_01));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataHistoryP3K($query,$cari,$where,$iswhere,$isGroupBy){
        // Ambil data yang di ketik user pada textbox pencarian
        $search = htmlspecialchars($_POST['search']['value']);
        // Ambil data limit per page
        $limit = preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['length']}");
        // Ambil data start
        $start =preg_replace("/[^a-zA-Z0-9.]/", '', "{$_POST['start']}");

        if($where != null){
            $setWhere = array();
            foreach ($where as $key => $value)
            {
                $setWhere[] = $key."='".$value."'";
            }
            $fwhere = implode(' AND ', $setWhere);

            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere AND ".$fwhere);

            }else{
                $sql = $this->db->query($query." WHERE ".$fwhere);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")" . " GROUP BY " . $isGroupBy .$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")". " GROUP BY " . $isGroupBy . $order ." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND ".$fwhere." AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE ".$fwhere." AND (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere AND ".$fwhere);
                }else{
                    $sql_filter = $this->db->query($query." WHERE ".$fwhere);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }
            $data = $sql_data->result_array();

        }else{
            if(!empty($iswhere))
            {
                $sql = $this->db->query($query." WHERE  $iswhere ");
            }else{
                $sql = $this->db->query($query);
            }
            $sql_count = $sql->num_rows();

            $cari = implode(" LIKE '%".$search."%' OR ", $cari)." LIKE '%".$search."%'";

            // Untuk mengambil nama field yg menjadi acuan untuk sorting
            $order_field = $_POST['order'][0]['column'];

            // Untuk menentukan order by "ASC" atau "DESC"
            $order_ascdesc = $_POST['order'][0]['dir'];
            $order = " ORDER BY ".$_POST['columns'][$order_field]['data']." ".$order_ascdesc;

            if(!empty($iswhere))
            {
                $sql_data = $this->db->query($query." WHERE $iswhere AND (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }else{
                $sql_data = $this->db->query($query." WHERE (".$cari.")".$order." LIMIT ".$limit." OFFSET ".$start);
            }

            if(isset($search))
            {
                if(!empty($iswhere))
                {
                    $sql_cari =  $this->db->query($query." WHERE $iswhere AND (".$cari.")");
                }else{
                    $sql_cari =  $this->db->query($query." WHERE (".$cari.")");
                }
                $sql_filter_count = $sql_cari->num_rows();
            }else{
                if(!empty($iswhere))
                {
                    $sql_filter = $this->db->query($query." WHERE $iswhere");
                }else{
                    $sql_filter = $this->db->query($query);
                }
                $sql_filter_count = $sql_filter->num_rows();
            }

            $data = $sql_data->result_array();
        }

        $datatable = array();
		foreach ($data as $key => $value) {
            // $klik_nip = '<a href="javascript:void(0)" onclick="detailData('.$value['id'].')">'. $value['nip'] .'</a>';
            $klik_nip = '<a href="'. base_url('history_p3k/detail/') . encode_url($value['id']) .'">'. $value['nip'] .'</a>';

			$datatable[$key] = array(
                'name' => $value['nama'],
                'nip' => $klik_nip
			);
		}

        $callback = array(
            'draw' => $_POST['draw'], // Ini dari datatablenya
            'recordsTotal' => $sql_count,
            'recordsFiltered'=>$sql_filter_count,
            'data'=>$datatable
        );
        return json_encode($callback); // Convert array $callback ke json
    }

    public function updateDataUsulanByPeriode($table, $data, $periode, $mynip){
        $this->db->where('periode', $periode);
        $this->db->where('posisi_acc', $mynip);
		$update = $this->db->update($table, $data);

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function getDataAtas($periode, $B_02B, $user_id, $role){
        if($role == 5){
            //sekretaris
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks` h
            JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`sekretaris_by` = ?)
            GROUP BY p.`id`";

        } else if($role == 6){
            //kepala SKPD
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks` h
            JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`kepala_skpd_by` = ?)
            GROUP BY p.`id`";
        } else if($role == 7){
            //tu cabdin
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks` h
            JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`cabdin_by` = ?)
            GROUP BY p.`id`";
        } else if($role == 3){
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks` h
            JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`kasubbag_keu_by` = ?)
            GROUP BY p.`id`";
        }


        $query = $this->db->query($sql, array($periode, $B_02B, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataHistoryByPegawaiId($pegawai_id){
        $sql =
        "SELECT h.`id`, h.`periode`, h.`kode_id`, h.`lama`, h.`baru`, k.`name` params
        FROM simgaji_historyp3ks h
        JOIN simgaji_kodes k ON h.`kode_id` = k.`id`
        WHERE h.`pegawai_id` = ?";

        $query = $this->db->query($sql, array($pegawai_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataAjuanByNipPosisiAcc($nip){
        $sql =
        "SELECT *
        FROM `simgaji_historyp3ks`
        WHERE deleted_at IS NULL
        AND `posisi_acc` = ?
        LIMIT 1";

        $query = $this->db->query($sql, array($nip));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getHistoryP3ksAccLastId($nip, $periode){
        $sql =
        "SELECT *
        FROM `simgaji_historyp3ks_acc`
        WHERE nip = ?
        AND periode = ?
        ORDER BY id DESC
        LIMIT 1";

        $query = $this->db->query($sql, array($nip, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataKadisForUpdate($periode, $B_02B, $user_id){
        //get data kadis untuk update data master jika verif bkd sudh acc
        $sql =
        "SELECT p.`id`, p.`nip`, p.`name` nama, h.`status_acc_bkd`, h.`lama`, h.`baru`, k.`field` field_kode
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        JOIN `simgaji_kodes` k ON h.`kode_id` = k.`id`
        WHERE h.`deleted_at` IS NULL
        AND h.`periode` = ?
        AND (h.`posisi_acc` = ? OR h.`kepala_skpd_by` = ?)";

        $query = $this->db->query($sql, array($periode, $B_02B, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getForKumulatifSekKadinas($role, $periode, $user_id){

        if($role == 5){
            //sekretaris
            $posted = "AND h.`sekretaris_by` = '$user_id' ";
        } else if($role == 6){
            //kadis
            $posted = "AND h.`kepala_skpd_by` = '$user_id' ";
        }

        $sql =
        "SELECT p.`id`, p.nip, p.name, SUM(IF(h.status_acc_bkd = 0, 1, 0)) as belum, SUM(IF(h.status_acc_bkd = 2, 1, 0)) as revisi, SUM(IF(h.status_acc_bkd = 3, 1, 0)) as tolak, SUM(IF(h.status_acc_bkd = 1, 1, 0)) as setuju
        FROM `simgaji_historyp3ks` h
        JOIN `simgaji_pegawaip3ks` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        $posted
        AND h.`periode` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
}
