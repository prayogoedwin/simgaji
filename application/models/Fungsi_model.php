<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fungsi_model extends CI_Model{
    public $dbeps = NULL;
    public $db = NULL;

    public function __construct(){
		parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function tambah($table, $data){
        $insert = $this->db->insert($table, $data);
		if ($insert){
			return TRUE;
		} else {
			return FALSE;
		}
    }

	public function tambah_bulk($table, $data){
		$ins = $this->db->insert_batch($table, $data);
		if($ins){
			return TRUE;
		} else {
			return FALSE;
		}
	}

    public function edit($table, $id, $data){
        $this->db->where('id', $id);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

	public function edit_v2($table, $where, $id, $data){
        $this->db->where($where, $id);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

	function soft_deleted($table, $where, $where_value, $data){
        $this->db->set($data);
        $this->db->where($where, $where_value);
        return $this->db->update($table);
    }

    public function hapus($table, $id, $data){
        $this->db->where('id', $id);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

	public function hapusByPegawaiId($table, $pegawai_id, $data){
        $this->db->where('pegawai_id', $pegawai_id);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

	public function getFieldNameByKodeId($kode_id){
        $sql = 
        "SELECT *
        FROM simgaji_kodes
        WHERE id = ?";

        $query_cek = $this->db->query($sql, array($kode_id));

        if($query_cek->num_rows() > 0){
            $data = $query_cek->row()->field;
        } else {
            $data = null;
        }

        return $data;
    }

	public function get_opd(){
		$this->dbeps->select("NALOKP, KOLOK");
        $this->dbeps->where('A_01');
        $this->dbeps->where('AKTIF',1);
		return $this->dbeps->get("TABLOKB08")->result();
	}

	public function editByNipByPeriodeByA01($table, $nip, $periode, $A_01, $data){
        $this->db->where('nip', $nip);
        $this->db->where('periode', $periode);
        $this->db->where('A_01', $A_01);
		$update = $this->db->update($table, $data); 

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }
}