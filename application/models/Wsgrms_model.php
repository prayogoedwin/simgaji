<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsgrms_model extends CI_Model{
    var $client_apikey = "GRMSJATENG";
    // var $auth       = "bb0b37ad-d57b-44da-8b8b-db0bf32727d1";

    public $dbgaji = NULL;
    public $dbgaji_prod = NULL;

    public function __construct(){
        parent::__construct();

        $this->dbgaji = $this->load->database('gaji', TRUE);
        $this->dbgaji_prod = $this->load->database('gaji_prod', TRUE);
        $this->dbgaji_pns = $this->load->database('gaji_pns', TRUE);
    }

    public function check_auth_client(){
        $auth  = $this->input->get_request_header('Authorization', TRUE);
        $client_apikey = $this->input->get_request_header('x-api-key', TRUE);

        $tgl = date('Y-m-d');
        $auth_generate = $this->validate_token('5', $auth, $tgl);

        if($auth_generate == null){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized token'));
        } else

        if($client_apikey == $this->client_apikey && $auth == $auth_generate->token){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function getGajiPnsByUk($periode, $uk){
        $this->dbgaji->select('pegawais.nik as NIK, pegawais.status_id, periode as periode_gaji, lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,kalkulasis.nip AS NIP,kalkulasis.NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji, kalkulasis.gaji_pokok, kalkulasis.jumlah_tunjangan_keluarga, kalkulasis.tunjangan_istri, kalkulasis.tunjangan_anak, kalkulasis.tunjangan_umum, kalkulasis.tunjangan_fungsional, kalkulasis.tunjangan_struktural, kalkulasis.tunjangan_umum_tambahan, kalkulasis.tunjangan_beras, kalkulasis.tunjangan_pph, kalkulasis.pembulatan, kalkulasis.jumlah_kotor, kalkulasis.potongan_bpjs_kesehatan, kalkulasis.potongan_pensiun, kalkulasis.jumlah_potongan, kalkulasis.tunjangan_pph, kalkulasis.askes');
        $this->dbgaji->join('pegawais', 'pegawais.nip = kalkulasis.nip');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('kalkulasis.golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kalkulasis.kelompok_gaji', 'DESC');
        $data = $this->dbgaji->get('kalkulasis')->result();

        return $data;
    }

    public function getGajiP3kByUk($periode, $uk){
        $this->dbgaji_prod->select('simgaji_pegawaip3ks.nik AS NIK, simgaji_pegawaip3ks.status_id,periode as periode_gaji,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji, simgaji_kalkulasip3ks.nip AS NIP,simgaji_kalkulasip3ks.NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji, simgaji_kalkulasip3ks.gaji_pokok, simgaji_kalkulasip3ks.jumlah_tunjangan_keluarga, simgaji_kalkulasip3ks.tunjangan_istri, simgaji_kalkulasip3ks.tunjangan_anak, simgaji_kalkulasip3ks.tunjangan_umum, simgaji_kalkulasip3ks.tunjangan_fungsional, simgaji_kalkulasip3ks.tunjangan_struktural, simgaji_kalkulasip3ks.tunjangan_umum_tambahan, simgaji_kalkulasip3ks.tunjangan_beras, simgaji_kalkulasip3ks.tunjangan_pph, simgaji_kalkulasip3ks.pembulatan, simgaji_kalkulasip3ks.jumlah_kotor, simgaji_kalkulasip3ks.potongan_bpjs_kesehatan, simgaji_kalkulasip3ks.potongan_pensiun, simgaji_kalkulasip3ks.jumlah_potongan, simgaji_kalkulasip3ks.tunjangan_pph, simgaji_kalkulasip3ks.askes');
        $this->dbgaji_prod->join('simgaji_pegawaip3ks', 'simgaji_pegawaip3ks.nip = simgaji_kalkulasip3ks.nip');
        $this->dbgaji_prod->where('periode', $periode);
        $this->dbgaji_prod->like('lokasi_kode', $uk, 'after');
        $this->dbgaji_prod->order_by('lokasi_kode');
        $this->dbgaji_prod->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.golongan_id', 'DESC');
        $this->dbgaji_prod->order_by('nip', 'ASC');
        $this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.kelompok_gaji', 'DESC');
        return $this->dbgaji_prod->get('simgaji_kalkulasip3ks')->result();

    }

    public function getGajiSusulanCpnsByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gajisusulan,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,status_string as status, golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasisusulans')->result();
    }

    public function getGajiSusulanP3kByUk($periode, $uk){
        // $this->dbgaji->select('periode as periode_gaji_susulanp3k,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama, status_string as status,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        // $this->dbgaji->where('periode', $periode);
        // $this->dbgaji->like('lokasi_kode', $uk, 'after');
        // $this->dbgaji->order_by('lokasi_kode');
        // $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        // $this->dbgaji->order_by('golongan_id', 'DESC');
        // $this->dbgaji->order_by('nip', 'ASC');
        // $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        // return $this->dbgaji->get('kalkulasip3ksusulans')->result();
        $sql = 
        "SELECT
            sus.*,
            peg.nik
        FROM
            `kalkulasip3ksusulans` sus
        JOIN gaji_prod.simgaji_pegawaip3ks peg ON sus.nip = peg.nip
        WHERE
            sus.`periode` = ?
            AND sus.`lokasi_kode` LIKE ? ESCAPE '!' 
        ORDER BY
            sus.`lokasi_kode`,
            sus.`tunjangan_struktural` DESC,
            sus.`golongan_id` DESC,
            sus.`nip` ASC,
            sus.`kelompok_gaji` DESC";
        
        $query = $this->dbgaji->query($sql, array($periode, '%'.$this->db->escape_like_str($uk).'%'));
        return $query->result();
    }

    public function getGajiTppPnsByUk($periode, $uk){
        // $this->dbgaji_pns->select('`periode` AS `periode_tpp`,
        // `lokasi` AS `kode_lokasi`,
        // `lokasi_string` AS `lokasi_gaji`,
        // `nip`,
        // `NAME` AS `nama`,
        // `golongan_string` AS `golongan`,
        // `jabatan_string` AS `jabatan`,
        // `beban_kerja` AS `tpp`,
        // `kondisi` AS `tpp_kondisi_kerja`,
        // `tempat` AS `tpp_tempat_bekerja`');
        // $this->dbgaji_pns->where('periode', $periode);
        // $this->dbgaji_pns->like('lokasi', $uk, 'after');
        // $this->dbgaji_pns->order_by('lokasi');
        // $this->dbgaji_pns->order_by('eselon_id', 'DESC');
        // $this->dbgaji_pns->order_by('golongan_id', 'DESC');
        // $this->dbgaji_pns->order_by('nip', 'ASC');
        // $this->dbgaji_pns->order_by('lokasi', 'DESC');
        // return $this->dbgaji_pns->get('simgaji_kalkulasi_tpp')->result();

        // $this->dbgaji_pns->select('simgaji_pegawai_tpp.nik as NIK, periode as periode_gaji, lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,simgaji_kalkulasi_tpp.nip AS NIP,simgaji_kalkulasi_tpp.NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji, simgaji_kalkulasi_tpp.gaji_pokok, simgaji_kalkulasi_tpp.jumlah_tunjangan_keluarga, simgaji_kalkulasi_tpp.tunjangan_istri, simgaji_kalkulasi_tpp.tunjangan_anak, simgaji_kalkulasi_tpp.tunjangan_umum, simgaji_kalkulasi_tpp.tunjangan_fungsional, simgaji_kalkulasi_tpp.tunjangan_struktural, simgaji_kalkulasi_tpp.tunjangan_umum_tambahan, simgaji_kalkulasi_tpp.tunjangan_beras, simgaji_kalkulasi_tpp.tunjangan_pph, simgaji_kalkulasi_tpp.pembulatan, simgaji_kalkulasi_tpp.jumlah_kotor, simgaji_kalkulasi_tpp.potongan_bpjs_kesehatan, simgaji_kalkulasi_tpp.potongan_pensiun, simgaji_kalkulasi_tpp.jumlah_potongan, simgaji_kalkulasi_tpp.tunjangan_pph, simgaji_kalkulasi_tpp.askes');
        // $this->dbgaji_pns->join('simgaji_pegawai_tpp', 'simgaji_pegawai_tpp.nip = simgaji_kalkulasi_tpp.nip');
        // $this->dbgaji_pns->where('periode', $periode);
        // $this->dbgaji_pns->like('lokasi_kode', $uk, 'after');
        // $this->dbgaji_pns->order_by('lokasi_kode');
        // $this->dbgaji_pns->order_by('tunjangan_struktural', 'DESC');
        // $this->dbgaji_pns->order_by('simgaji_kalkulasi_tpp.golongan_id', 'DESC');
        // $this->dbgaji_pns->order_by('nip', 'ASC');
        // $this->dbgaji_pns->order_by('simgaji_kalkulasi_tpp.kelompok_gaji', 'DESC');
        // $data = $this->dbgaji_pns->get('simgaji_kalkulasi_tpp')->result();
        
        // return $data;

        $sql =
        "SELECT tpp.*, p.`nik`
        FROM simgaji_kalkulasi_tpp tpp
        LEFT JOIN gaji.`pegawais` p ON tpp.`nip` = p.`nip`
        WHERE periode = ?
        AND tpp.`lokasi` LIKE ?
        ORDER BY lokasi, beban_kerja DESC, golongan_id DESC, nip ASC";

        $query = $this->dbgaji_pns->query($sql, array($periode, $this->db->escape_like_str($uk).'%'));
        if($query->num_rows()>0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiTppP3kByUk($periode, $uk){

        $sql =
        "SELECT tpp.*, p.`nik`
        FROM simgaji_kalkulasi_tpp tpp
        JOIN simgaji_pegawaip3ks p ON tpp.`nip` = p.`nip`
        WHERE periode = ?
        AND tpp.`lokasi` LIKE ?
        ORDER BY lokasi, beban_kerja DESC, golongan_id DESC, nip ASC";

        $query = $this->db->query($sql, array($periode, $this->db->escape_like_str($uk).'%'));
        if($query->num_rows()>0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
    
    public function getGaji13PnsByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gaji13,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasitbs')->result();
    }

    public function getGaji13P3kByUk($periode, $uk){
        $this->dbgaji_prod->select('periode as periode_gajip3k13,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji_prod->where('periode', $periode);
        $this->dbgaji_prod->like('lokasi_kode', $uk, 'after');
        $this->dbgaji_prod->order_by('lokasi_kode');
        $this->dbgaji_prod->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji_prod->order_by('golongan_id', 'DESC');
        $this->dbgaji_prod->order_by('nip', 'ASC');
        $this->dbgaji_prod->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji_prod->get('simgaji_kalkulasip3ks_tb')->result();
    }

    public function getGajiPns14ByUk($periode, $uk){
        $this->dbgaji->select('periode as periode_gaji14,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji->where('periode', $periode);
        $this->dbgaji->like('lokasi_kode', $uk, 'after');
        $this->dbgaji->order_by('lokasi_kode');
        $this->dbgaji->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji->order_by('golongan_id', 'DESC');
        $this->dbgaji->order_by('nip', 'ASC');
        $this->dbgaji->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji->get('kalkulasiebs')->result();
    }

    public function getGajiP3k14ByUk($periode, $uk){
        $this->dbgaji_prod->select('periode as periode_gaji14,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,jumlah_bersih_bayar AS Gaji');
        $this->dbgaji_prod->where('periode', $periode);
        $this->dbgaji_prod->like('lokasi_kode', $uk, 'after');
        $this->dbgaji_prod->order_by('lokasi_kode');
        $this->dbgaji_prod->order_by('tunjangan_struktural', 'DESC');
        $this->dbgaji_prod->order_by('golongan_id', 'DESC');
        $this->dbgaji_prod->order_by('nip', 'ASC');
        $this->dbgaji_prod->order_by('kelompok_gaji', 'DESC');
        return $this->dbgaji_prod->get('simgaji_kalkulasip3ks_eb')->result();
    }
    // public function getGajiTppPns13ByUk($periode, $uk){
    //     $this->dbgaji_pns->select('periode as periode_tpp_13,lokasi_kode as kode_lokasi, lokasi_string as lokasi_gaji,nip AS NIP,NAME AS Nama,golongan_string AS Golongan,jabatan AS Jabatan,beban_kerja_nominal AS TPP');
    //     $this->dbgaji_pns->where('periode', $periode);
    //     $this->dbgaji_pns->like('lokasi_kode', $uk, 'after');
    //     $this->dbgaji_pns->order_by('lokasi_kode');
    //     $this->dbgaji_pns->order_by('eselon_id', 'DESC');
    //     $this->dbgaji_pns->order_by('golongan_id', 'DESC');
    //     $this->dbgaji_pns->order_by('nip', 'ASC');
    //     $this->dbgaji_pns->order_by('kelompok_gaji', 'DESC');
    //     return $this->dbgaji_pns->get('simgaji_kalkulasi_tpp_13')->result();
    // }

    public function update_token($table, $stakeholder_code, $data){
        $this->db->where('stakeholder_code', $stakeholder_code);
		$delete = $this->db->update($table, $data);

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    private function validate_token($stakeholder_code, $token, $tgl){
        $sql =
        "SELECT *
        FROM simgaji_token
        WHERE `stakeholder_code` = ?
        AND token = ?
        AND date(created_at) = ?";

        $query = $this->db->query($sql, array($stakeholder_code, $token, $tgl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}