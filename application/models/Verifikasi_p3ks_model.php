<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_p3ks_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->historyp3ks_acc = 'simgaji_historyp3ks_acc a';
        $this->historyp3ks = 'simgaji_historyp3ks a';
        $this->pegawaip3ks = 'simgaji_pegawaip3ks b';


    }

     //CRUD JFT START//
    var $verifikasi_column_order = array('a.id', 'a.pegawai_id', 'b.nip', 'b.name', 'a.periode', 'belum', 'tolak', 'setuju'); //set column field database for datatable orderable
    var $verifikasi_column_search = array('a.id', 'a.pegawai_id', 'b.nip', 'b.name',  'a.periode', 'c.name', 'd.name'); //set column field database for datatable searchable
    var $verifikasi_order = array('belum' => 'ASC'); // default order

    var $periode_column_order = array('periode'); //set column field database for datatable orderable
    var $periode_column_search = array('periode'); //set column field database for datatable searchable
    var $periode_order = array('periode' => 'DESC','tolak' => 'DESC'); // default order

    private function get_lokasi_verif(){
        $B_02B = $this->session->userdata('B_02B');
        $this->db->select('a.*');
        $this->db->from($this->pegawaip3ks);
        $this->db->where('a.nip',$B_02B);
        $query = $this->db->get();
        return $query->row();
    }

    private function _get_verifikasi_datatables_query($periode)
    {

       $role = $this->session->userdata('role');
        // SELECT `a`.*, `b`.`nip` , `b`.`name` , SUM(IF(status_acc_bkd = 0, 1, 0)) as belum, SUM(IF(status_acc_bkd = 2, 1, 0)) as tolak, SUM(IF(status_acc_bkd = 1, 1, 0)) as setuju  FROM `simgaji_historyp3ks` `a`
        // JOIN `simgaji_pegawaip3ks` `b` ON `b`.`id` = `a`.`pegawai_id`
        // WHERE `a`.`periode` = '2022-01-01' AND `a`.`deleted_at` IS NULL GROUP BY pegawai_id

        // $periode = $tahun.'-'.$bulan.'-01';
        $B_02B = $this->session->userdata('B_02B');
        $verifikator = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$B_02B'")->row();

        // status ajuan = 3, status acc = 1, kepala_skpd_by IS NOT NULL
        $this->db->select('a.*');
        $this->db->select('b.nip as B_02B');
        $this->db->select('b.name as B_03');
        $this->db->select('c.name as lokasi_kerja');
        $this->db->select('d.name as lokasi_gaji');
        $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju,');
        $this->db->from($this->historyp3ks);
        $this->db->join($this->pegawaip3ks, 'b.id = a.pegawai_id');
        $this->db->join('simgaji_lokasis c', 'c.id = b.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji');
        $this->db->where('a.periode',$periode);

        // if ($role == 2) {
        //     if($this->input->post('lokasi_kerja') == '' && $this->input->post('lokasi_gaji') == '')
        //     {
        //         $B_02B = $this->session->userdata('B_02B');
        //         $d = ifVerif2($B_02B);
        //         if($d != '');

        //         $select = explode(',',$d);
        //         $datasel = NULL;
        //         foreach ($select as $key => $val){
        //             $datasel[$key] = $val;
        //             $v = substr($val,0,6);
        //             $this->db->like('b.lokasigaji', $lv);
        //         }

        //         // $x = array_values($datasel);
        //         // $this->db->where_in('b.lokasigaji', $x);
        //     }
        // }

        if($this->input->post('lokasi_kerja'))
        {
            $lokasi_kerja = $this->input->post('lokasi_kerja');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('b.lokasikerja', $lk, 'AFTER');
        }
    
        
        if($this->input->post('lokasi_gaji'))
        {
            $lokasi_kerja = $this->input->post('lokasi_gaji');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('b.lokasigaji', $lk, 'AFTER');
        }


        
        if ($role == 2) {
        $B_02B = $this->session->userdata('B_02B');
        $lokasi = ifVerif2($B_02B);
        if($this->input->post('lokasi_gaji') == '' && $this->input->post('lokasi_kerja') == ''){   
            $lokasi_arr = explode(',',$lokasi);
            $frst = stristr($lokasi,",",true);
            $this->db->group_start(); //
            foreach ($lokasi_arr as $val2){
            $lk = substr($val2,0,6);
            if($val2 == $frst){
                $this->db->like('b.lokasigaji',$lk,'AFTER');
            }else{
                
                $this->db->or_like('b.lokasigaji',$lk,'AFTER');
            }
            
           
            }
            $this->db->group_end();
        }
        }


        $this->db->where('a.deleted_at IS NULL');

        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->verifikasi_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->verifikasi_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->verifikasi_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->verifikasi_order)) {
            $order = $this->verifikasi_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->group_by('a.pegawai_id');

    }

    function get_verifikasi($periode)
    {
        $this->_get_verifikasi_datatables_query($periode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function verifikasi_count_filtered($periode)
    {
        $this->_get_verifikasi_datatables_query($periode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function verifikasi_count_all($periode)
    {
        // $periode = $tahun.'-'.$bulan.'-01';
        $this->db->from($this->verifikasi);
        $this->db->where('periode', $periode);
        $this->db->where('deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    //get periode
    private function _get_periode_datatables_query()
    {
        $this->db->distinct();
        $this->db->select('periode');
        $this->db->order_by("id", "desc");
        $this->db->from('simgaji_historyp3ks');

        $i = 0;

        foreach ($this->periode_column_search as $data) { // loop column
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->periode_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->periode_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->periode_order)) {
            $order = $this->periode_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

    }

    function get_periode()
    {
        $this->_get_periode_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function periode_count_filtered()
    {
        $this->_get_periode_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function periode_count_all()
    {
        $this->db->from('simgaji_periodeberkala');

        return $this->db->count_all_results();
    }

    public function getverifikasiById($id){
        $sql =
        "SELECT *
        FROM $this->verifikasi
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataPns($nip, $A_01){
        $x = '%'.$nip.'%';
        $sql =
        "SELECT CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.B_02B id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
        FROM MASTFIP08 mastfip
        JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
        JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
        JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
        WHERE
        (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
        AND mastfip.`A_01` = ?
        AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";

        $query = $this->dbeps->query($sql, array($nip, $x, $A_01));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

     //CRUD JFT END//




}
