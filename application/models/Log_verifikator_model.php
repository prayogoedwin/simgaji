<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_verifikator_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->log_edit = 'simgaji_log_verifikator a';
        $this->log_add = 'simgaji_pegawaip3ks_log c';
        $this->log_usul = 'simgaji_historyp3ks d';
    
        $this->pegawai = 'simgaji_pegawaip3ks e';
        $this->verifikator = 'simgaji_verifikator b';


        
    }

    //CRUD LOGEDIT P3K START//
    var $logedit_column_order = array('a.id', 'a.nip_ybs', 'b.nama','a.created_at'); //set column field database for datatable orderable
    var $logedit_column_search = array('a.nip_ybs', 'b.nama','a.created_at'); //set column field database for datatable searchable
    var $logedit_order = array('a.nip_ybs' => 'DESC'); // default order 


    //CRUD LOGADD P3K START//
    var $logadd_column_order = array('c.id', 'c.nip', 'b.nama','c.created_at'); //set column field database for datatable orderable
    var $logadd_column_search = array('c.nip', 'b.nama','c.created_at'); //set column field database for datatable searchable
    var $logadd_order = array('c.nip' => 'DESC'); // default order


    //CRUD LOG USULAN P3K START//
    var $logusul_column_order = array('d.id', 'e.nip', 'e.nama','d.created_at'); //set column field database for datatable orderable
    var $logusul_column_search = array('e.nip', 'e.nama','d.created_at'); //set column field database for datatable searchable
    var $logusul_order = array('e.nip' => 'DESC'); // default order


    private function _get_logedit_datatables_query()
    {

        $this->db->select("a.*, b.nama as namever");
        if($this->input->post('nip_ybs') != ''){
            $this->db->where('a.nip_ybs', $this->input->post('nip_ybs'));
        }
        if($this->input->post('verifikator') != ''){
            $this->db->where('a.created_by', $this->input->post('verifikator'));
        }

        $role = $this->session->userdata('role');
        $B_02B = $this->session->userdata('B_02B');
        $id = cari_id_verifikator_gaji($B_02B);
        if($role == 2 && $id != 0){
            $this->db->where('a.created_by', $id);
        }

        if($this->input->post('created_at') != ''){
            $this->db->like('a.created_at', $this->input->post('created_at'), 'AFTER');
        }
        if($this->input->post('periode') != ''){
            $this->db->like('a.periode', $this->input->post('periode'), 'AFTER');
        }
        $this->db->from($this->log_edit);
        $this->db->join($this->verifikator, 'a.nip_verifikator = b.id');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->logedit_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->logedit_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->logedit_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->logedit_order)) {
            $order = $this->logedit_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        // $this->db->group_by('a.id');
        
    }

    function get_logedit()
    {
        $this->_get_logedit_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function logedit_count_filtered()
    {
        $this->_get_logedit_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function logedit_count_all()
    {
        $this->db->from($this->log_edit);
        return $this->db->count_all_results();
    }


    private function _get_logadd_datatables_query()
    {

        $this->db->select("c.*, b.nama as namever");
        if($this->input->post('nip_ybs') != ''){
            $this->db->where('c.nip', $this->input->post('nip_ybs'));
        }
        if($this->input->post('verifikator') != ''){
            $this->db->where('c.log_by', $this->input->post('verifikator'));
        }

        $role = $this->session->userdata('role');
        $B_02B = $this->session->userdata('B_02B');
        $id = cari_id_verifikator_gaji($B_02B);
        if($role == 2 && $id != 0){
            $this->db->where('c.log_by', $id);
        }

        if($this->input->post('created_at') != ''){
            $this->db->like('c.created_at', $this->input->post('created_at'), 'AFTER');
        }

        
        $this->db->from($this->log_add);
        $this->db->join($this->verifikator, 'c.log_by = b.id','LEFT');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->logadd_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->logadd_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->logadd_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->logadd_order)) {
            $order = $this->logadd_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        // $this->db->group_by('a.id');
        
    }

    function get_logadd()
    {
        $this->_get_logadd_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function logadd_count_filtered()
    {
        $this->_get_logadd_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function logadd_count_all()
    {
        $this->db->from($this->log_add);
        return $this->db->count_all_results();
    }



    private function _get_logusul_datatables_query()
    {

        $this->db->where('d.status_acc_bkd', 1);
        $this->db->where('d.kepala_skpd_by IS NOT NULL');

        $this->db->select("d.*, e.nip, e.name, b.nama as namever");
        if($this->input->post('nip_ybs') != ''){
            $this->db->where('e.nip', $this->input->post('nip_ybs'));
        }
        if($this->input->post('verifikator') != ''){
            $this->db->where('d.verifikator', $this->input->post('verifikator'));
        }

        $role = $this->session->userdata('role');
        $B_02B = $this->session->userdata('B_02B');
        $id = cari_id_verifikator_gaji($B_02B);
        if($role == 2 && $id != 0){
            $this->db->where('d.verifikator', $id);
        }

        if($this->input->post('created_at') != ''){
            $this->db->like('d.created_at', $this->input->post('created_at'), 'AFTER');
        }
        if($this->input->post('periode') != ''){
            $this->db->like('d.periode', $this->input->post('periode'), 'AFTER');
        }
        $this->db->from($this->log_usul);
        $this->db->join($this->verifikator, 'd.verifikator = b.id');
        $this->db->join($this->pegawai, 'd.pegawai_id = e.id');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->logusul_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->logusul_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->logusul_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->logusul_order)) {
            $order = $this->logusul_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        // $this->db->group_by('a.id');
        
    }

    function get_logusul()
    {
        $this->_get_logusul_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function logusul_count_filtered()
    {
        $this->_get_logusul_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function logusul_count_all()
    {
        $this->db->from($this->log_edit);
        return $this->db->count_all_results();
    }

    
}