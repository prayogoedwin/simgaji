<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsbpjs_model extends CI_Model{
    var $client_apikey = "BKDJATENG";
    // var $auth       = "d6bee00c-76cf-47e8-93c2-1fc1cf34106e";

    public $dbgaji = NULL;

    public function __construct(){
        parent::__construct();

        $this->dbgaji = $this->load->database('gaji', TRUE);
		$this->dbgajip3k = $this->load->database('gaji_prod', TRUE);
    }

    public function check_auth_client(){
        $auth  = $this->input->get_request_header('Authorization', TRUE);
        $client_apikey = $this->input->get_request_header('x-api-key', TRUE);

		$tgl = date('Y-m-d');
        $auth_generate = $this->validate_token('2', $auth, $tgl);

        if($auth_generate == null){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized token'));
        } else

        if($client_apikey == $this->client_apikey && $auth == $auth_generate->token){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function getGajiPnsByUk($service_code, $periode, $uk){
        if($service_code == 'pns' && $uk == 'all'){
            $this->dbgaji->select('kalkulasis.periode AS tgl gaji,
			kalkulasis.name AS NAMA,kalkulasis.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,
			kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais', 'kalkulasis.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->where('kalkulasis.lokasi_kode !=', '77000000');
			//$this->dbgaji->where('kalkulasis.lokasi_kode!=', '01000000');
			$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			$this->dbgaji->order_by('kalkulasis.golongan_id', 'DESC');
			//$this->dbgaji->limit(20);
			$data = $this->dbgaji->get()->result();

            // $data = 'all';
        } else if($service_code == 'pns' && $uk !== 'all'){
            $this->dbgaji->select('kalkulasis.periode AS PERIODE GAJI,
			pegawais.name AS NAMA,
			pegawais.nip AS NIP,pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais', 'kalkulasis.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('kalkulasis.lokasi_kode', $uk, 'after');
			$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			$this->dbgaji->order_by('kalkulasis.golongan_id', 'DESC');
			$data = $this->dbgaji->get()->result();

            // $data = 'by uk';
        } else if($service_code == 'p3k' && $uk !== 'all'){
            $this->dbgajip3k->select('simgaji_kalkulasip3ks.periode AS PERIODE GAJI,
			simgaji_pegawaip3ks.name AS NAMA,
			simgaji_pegawaip3ks.nip AS NIP,
			simgaji_pegawaip3ks.nik AS NIK,
			simgaji_pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			simgaji_lokasis.name AS SKPD,
			simgaji_kalkulasip3ks.lokasi_string AS UNIT_KERJA,
			simgaji_kalkulasip3ks.golongan_string AS GOLONGAN,
			simgaji_kalkulasip3ks.jabatan AS JABATAN,
			simgaji_kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
			simgaji_kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
			simgaji_kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
			simgaji_kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
			simgaji_kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
			simgaji_kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			simgaji_kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			simgaji_kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
			simgaji_kalkulasip3ks.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			simgaji_kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			simgaji_kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgajip3k->from('simgaji_kalkulasip3ks');
			$this->dbgajip3k->join('simgaji_pegawaip3ks', 'simgaji_kalkulasip3ks.nip=simgaji_pegawaip3ks.nip', 'left');
			$this->dbgajip3k->join('simgaji_lokasis', 'simgaji_pegawaip3ks.lokasikerja=simgaji_lokasis.kode');
			$this->dbgajip3k->where('simgaji_kalkulasip3ks.periode', $periode);
			$this->dbgajip3k->like('simgaji_kalkulasip3ks.lokasi_kode', $uk, 'after');
			$this->dbgajip3k->order_by('simgaji_kalkulasip3ks.lokasi_kode');
			$this->dbgajip3k->order_by('simgaji_kalkulasip3ks.golongan_id', 'DESC');
			$data = $this->dbgajip3k->get()->result();
        } else if($service_code == 'p3k' && $uk == 'all'){
            $this->dbgajip3k->select('simgaji_kalkulasip3ks.periode AS PERIODE GAJI,
			simgaji_pegawaip3ks.name AS NAMA,
			simgaji_pegawaip3ks.nip AS NIP,
			simgaji_pegawaip3ks.nik AS NIK,
			simgaji_pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			simgaji_lokasis.name AS SKPD,
			simgaji_kalkulasip3ks.lokasi_string AS UNIT_KERJA,
			simgaji_kalkulasip3ks.golongan_string AS GOLONGAN,
			simgaji_kalkulasip3ks.jabatan AS JABATAN,
			simgaji_kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
			simgaji_kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
			simgaji_kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
			simgaji_kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
			simgaji_kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
			simgaji_kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			simgaji_kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			simgaji_kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
			simgaji_kalkulasip3ks.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			simgaji_kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			simgaji_kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgajip3k->from('simgaji_kalkulasip3ks');
			$this->dbgajip3k->join('simgaji_pegawaip3ks', 'simgaji_kalkulasip3ks.nip=simgaji_pegawaip3ks.nip', 'left');
			$this->dbgajip3k->join('simgaji_lokasis', 'simgaji_pegawaip3ks.lokasikerja=simgaji_lokasis.kode');
			#$this->dbgajip3k->join('simgaji_lokasis', 'simgaji_pegawaip3ks.lokasikerja=simgaji_lokasis.kode');
			$this->dbgajip3k->where('simgaji_kalkulasip3ks.periode', $periode);
			$this->dbgajip3k->order_by('simgaji_kalkulasip3ks.lokasi_kode');
			$this->dbgajip3k->order_by('simgaji_kalkulasip3ks.golongan_id', 'DESC');
			$data = $this->dbgajip3k->get()->result();
        }

        return $data;
    }

    public function getGajiSusulanP3k($periode, $uk){
        if($uk == 'all'){
            $this->dbgaji->select('kalkulasip3ksusulans.periode AS PERIODE GAJI SUSULAN,
			pegawaip3ks.name AS NAMA,
			pegawaip3ks.nip AS NIP,
			pegawaip3ks.nik AS NIK,
			pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			kalkulasip3ksusulans.lokasi_string AS UNIT_KERJA,
			kalkulasip3ksusulans.golongan_string AS GOLONGAN,
			kalkulasip3ksusulans.jabatan AS JABATAN,
			kalkulasip3ksusulans.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasip3ksusulans.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasip3ksusulans.gaji_pokok AS GAJI_POKOK,
			kalkulasip3ksusulans.tunjangan_istri AS TJ_ISTRI,
			kalkulasip3ksusulans.tunjangan_anak AS TJ_ANAK,
			kalkulasip3ksusulans.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasip3ksusulans.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasip3ksusulans.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasip3ksusulans.tunjangan_umum AS TJ_UMUM,0
			AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasip3ksusulans.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasip3ksusulans.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasip3ksusulans');
			$this->dbgaji->join('pegawaip3ks', 'kalkulasip3ksusulans.nip=pegawaip3ks.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawaip3ks.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasip3ksusulans.periode', $periode);
			$this->dbgaji->order_by('kalkulasip3ksusulans.lokasi_kode');
			$this->dbgaji->order_by('kalkulasip3ksusulans.golongan_id', 'DESC');
			$data = $this->dbgaji->get()->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiTppByPeriodeUk($periode, $uk){
        if($uk !== 'all'){
            $this->dbgaji->select('tpps.periode AS periode tpp,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,tpps.name AS NAMA,
			tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis', 'kalkulasis.nip=tpps.nip', 'left');
			$this->dbgaji->join('pegawais', 'tpps.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('tpps.lokasi_kode', $uk, 'after');
			$this->dbgaji->order_by('tpps.lokasi_kode');
			$this->dbgaji->order_by('tpps.golongan_id', 'DESC');

			$data = $this->dbgaji->get()->result();
        } else if($uk == 'all'){
            $this->dbgaji->select('tpps.periode AS periode tpp,
			tpps.name AS NAMA,tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis', 'kalkulasis.nip=tpps.nip', 'left');
			$this->dbgaji->join('pegawais', 'tpps.nip=pegawais.nip', 'left');
			$this->dbgaji->join('lokasis', 'pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->order_by('tpps.lokasi_kode');
			$this->dbgaji->order_by('tpps.golongan_id', 'DESC');
			// $this->dbgaji->limit(5);

			$data = $this->dbgaji->get()->result();
        }

        return $data;
    }

	public function getGajiTppKeuByPeriodeUk($periode, $uk){
		// $x = "AND LEFT ( t.lokasi, 2 )= '$uk'";

		$x = "";
		if($uk !== 'all'){
			$z = $uk . '%';
			$x = "AND t.lokasi LIKE '$z'";
		}
		
		$sql = 
		"SELECT
			t.periode periode_TPP,
			t.NAME AS 'NAMA',
			t.`nip` AS 'NIP',
			t.golongan_string AS 'GOLONGAN',
			k.istri AS 'JUMLAH_ISTRI/SUAMI',
			k.anak AS 'JUMLAH_ANAK',
			k.gaji_pokok AS 'GAJI_POKOK',
			k.tunjangan_istri AS 'TUNJANGAN_ISTRI/SUAMI',
			k.tunjangan_anak AS 'TUNJANGAN_ANAK',
			k.tunjangan_struktural AS 'TUNJANGAN_STRUKTURAL/ESELON',
			k.tunjangan_fungsional AS 'TUNJANGAN_FUNGSIONAL',
			k.tunjangan_umum AS 'TUNJANGAN_UMUM',
			0 'TUNJANGAN_SERTIFIKASI_GURU',
			0 'TUNJANGAN_SERTIFIKASI_LAYANAN_MEDIS',
			t.beban_kerja_nominal + t.beban_kerja_pot_bpjs AS 'TPP',
			t.beban_kerja_khusus_nominal + t.beban_kerja_khusus_pot_bpjs AS 'TPP_BEBAN_KERJA_KHUSUS',
			t.kondisi_nominal + t.kondisi_pot_bpjs AS 'TPP_KONDISI_KERJA',
			t.tempat_nominal + t.tempat_pot_bpjs AS 'TPP_TEMPAT_BERTUGAS',
			t.plt_nominal + t.plt_pot_bpjs AS 'TPP_PLT',
			k.potongan_bpjs_kesehatan AS 'BPJS_1%_GAJI',
			t.beban_kerja_pot_bpjs AS 'BPJS_1%_TPP',
			t.beban_kerja_khusus_pot_bpjs AS 'BPJS_1%_TPP_BEBAN_KERJA_KHUSUS',
			t.kondisi_pot_bpjs AS 'BPJS_1%_TPP_KONDISI_KERJA',
			t.tempat_pot_bpjs AS 'BPJS_1%_TPP_TEMPAT_BEKERJA',
			t.plt_pot_bpjs AS 'BPJS_1%_TPP_PLT',
			k.askes AS 'BPJS_4%_GAJI',
			t.beban_kerja_tun_bpjs AS 'BPJS_4%_TPP',
			t.beban_kerja_khusus_tun_bpjs AS 'BPJS_4%_TPP_BEBAN_KERJA_KHUSUS',
			t.kondisi_tun_bpjs AS 'BPJS_4%_TPP_KONDISI_KERJA',
			t.tempat_tun_bpjs AS 'BPJS_4%_TPP_TEMPAT_BEKERJA',
			t.plt_tun_bpjs AS 'BPJS_4%_TPP_PLT',
			CONCAT( LEFT ( t.lokasi, 2 ), '000000' ) AS 'KODE_SKPD',
			b.NAME AS 'SKPD',
			t.lokasi_string AS 'LOKASI_UNIT' 
		FROM
			gaji_pns.simgaji_kalkulasi_tpp t
			LEFT JOIN gaji.kalkulasis k ON t.nip = k.nip 
			JOIN gaji.lokasis b ON LEFT ( t.lokasi, 2 )= LEFT ( b.kode, 2 ) 
			AND RIGHT ( b.kode, 6 ) LIKE '000000' 
		WHERE
			t.periode = ?
			AND k.`periode` = ?
			$x
		ORDER BY
			t.lokasi ASC,
			t.golongan_id DESC";
		
		$query = $this->db->query($sql, array($periode, $periode));
		if($query->num_rows() > 0){
			$data = $query->result();
		} else {
			$data = array();
		}

		return $data;
	}

	public function getGajiKeuByPeriodeUk($periode, $uk){
		// $x = "AND LEFT ( t.lokasi, 2 )= '$uk'";

		$x = "";
		if($uk !== 'all'){
			$z = $uk . '%';
			$x = "AND k.lokasi_kode LIKE '$z'";
		}
		
		$sql = 
		"SELECT
			k.periode AS 'tgl gaji',
			k.nip AS 'nip',
			k.NAME AS 'nama',
			p.tempat_lahir AS 'tempatlhr',
			CONCAT (
				LEFT ( k.nip, 4 ),
				'-',
				MID( k.`nip`, 5, 2 ),
				'-',
			MID( k.`nip`, 7, 2 )) AS 'tgllhr',
			CONCAT( LEFT ( k.lokasi_kode, 2 ), '000000' ) AS 'kdskpd',
			k.lokasi_kode AS 'kdsatker',
			'' AS 'kddati2',
			'' AS 'kddati1',
			k.istri AS 'jistri',
			k.anak AS 'janak',
			k.golongan_id AS 'kdpangkat',
			e.NAME AS 'kdeselon',
			k.gaji_pokok AS 'gapok',
			k.tunjangan_istri AS 'tjistri',
			k.tunjangan_anak AS 'tjanak',
			k.tunjangan_struktural AS 'tjeselon',
			k.tunjangan_fungsional AS 'tjfungsi',
			0 AS 'tjstruk=tjeselon',
			0 'tjlangka',
			0 'tjterpencil',
			0 'tjkhusus',
			k.tunjangan_umum AS 'tjumum',
			k.tunjangan_umum AS 'tunjangan umum',
			k.potongan_bpjs_kesehatan AS 'iwp_beban_pegawai',
			k.askes AS 'iwp_beban_pembkerja',
			b.NAME AS 'nmskpd',
			k.lokasi_string AS 'nmsatker',
			REPLACE ( p.nik, '.', '' ) AS 'no_ktp' 
		FROM
			kalkulasis k
			LEFT JOIN pegawais p ON k.nip = p.nip
			LEFT JOIN eselons e ON p.eselon_id = e.id
			JOIN lokasis b ON LEFT ( k.lokasi_kode, 2 )= LEFT ( b.kode, 2 ) 
			AND RIGHT ( b.kode, 6 ) LIKE '000000' 
		WHERE
			k.periode = ?
			AND LEFT ( k.lokasi_kode, 2 ) NOT IN ( '77' )
			$x
		ORDER BY
			k.lokasi_kode ASC,
			k.golongan_id DESC;";
		
		$query = $this->dbgaji->query($sql, array($periode));
		if($query->num_rows() > 0){
			$data = $query->result();
		} else {
			$data = array();
		}

		return $data;
	}

	public function update_token($table, $stakeholder_code, $data){
        $this->db->where('stakeholder_code', $stakeholder_code);
		$delete = $this->db->update($table, $data); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    private function validate_token($stakeholder_code, $token, $tgl){
        $sql = 
        "SELECT *
        FROM simgaji_token
        WHERE `stakeholder_code` = ?
        AND token = ?
        AND date(created_at) = ?";

        $query = $this->db->query($sql, array($stakeholder_code, $token, $tgl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}