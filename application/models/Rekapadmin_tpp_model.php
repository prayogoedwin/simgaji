<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekapadmin_tpp_model extends CI_Model
{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->historyp3ks_acc = 'simgaji_history_tpp_acc a';
        $this->historyp3ks = 'simgaji_history_tpp a';
        $this->pegawaip3ks = 'simgaji_pegawai_tpp b';
    }

    //CRUD REKAP P#KS START//
    var $rekapp3ks_column_order = array('d.kode', 'belum', 'tolak', 'setuju'); //set column field database for datatable orderable
    var $rekapp3ks_column_search = array('d.name'); //set column field database for datatable searchable
    var $rekapp3ks_order = array('d.kode' => 'DESC'); // default order 

    //CRUD REKAP VERIFIKATOR P#KS START//
    var $rekapp3ks_column_orderby_verifikator = array('c.id', 'belum', 'tolak', 'setuju'); //set column field database for datatable orderable
    var $rekapp3ks_column_searchby_verifikator = array('c.nip', 'c.nama'); //set column field database for datatable searchable
    var $rekapp3ks_orderby_verifikator = array('c.id' => 'DESC'); // default order 


    

    private function _get_rekapp3ks_datatables_query_old($periode)
    {
        $role = $this->session->userdata('role');

        // $this->db->select('c.name as lokasi_kerja, a.pegawai_id, c.id as id_lokasi');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju');
        // $this->db->from($this->historyp3ks);
        // $this->db->join($this->pegawaip3ks, "b.id = a.pegawai_id AND a.periode = '$periode'  AND a.deleted_at IS NULL");
        // $this->db->join('simgaji_lokasis c', "c.kode = b.lokasikerja","RIGHT");

        // $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji');
        // $this->db->where('c.bool_id', 2);

        $this->db->select('a.*');
        $this->db->select('b.nip as B_02B');
        $this->db->select('b.name as B_03');
        // $this->db->select('c.name as lokasi_kerja, c.id as id_lokasi');
        $this->db->select('d.name as lokasi_gaji, , d.id as id_lokasi');
        $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju,');
        $this->db->from($this->historyp3ks);
        $this->db->join($this->pegawaip3ks, "b.id = a.pegawai_id AND a.periode = '$periode'  AND a.deleted_at IS NULL");
        // $this->db->join($this->pegawaip3ks, 'b.id = a.pegawai_id');
        // $this->db->join('simgaji_lokasis c', 'c.id = b.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji');
        // $this->db->where('a.periode',$periode);


        
        if ($role == 2) {
            $B_02B = $this->session->userdata('B_02B');
            $d = ifVerif2($B_02B);
            if($d != '');
            
            $select = explode(',',$d);
            $datasel = NULL;
            foreach ($select as $key => $val){
                $datasel[$key] = $val;
                // $this->db->or_where('c.i', $val);
            }


            $x = array_values($datasel);

            $this->db->where_in('d.id', $x);
            // $this->db->where_in('c.id', ['2812','3419']);

        }




        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->rekapp3ks_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->rekapp3ks_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->rekapp3ks_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->rekapp3ks_order)) {
            $order = $this->rekapp3ks_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->group_by('d.kode');
    }

    private function _get_rekapp3ks_datatables_query($periode)
    {
        $role = $this->session->userdata('role');


        // $this->db->select('a.*');
        // $this->db->select('b.name as B_03');
        // $this->db->select('d.name as lokasi_gaji, , d.id as id_lokasi');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum,');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi,');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak,');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju,');
        // $this->db->from($this->historyp3ks);
        // $this->db->join($this->pegawaip3ks, "b.id = a.pegawai_id AND a.periode = '$periode'  AND a.deleted_at IS NULL");
        // $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji AND d.bool_id = 2');
        // $this->db->where('a.periode',$periode);

        $this->db->select('a.*');
        $this->db->select('b.nip as B_02B');
        $this->db->select('b.name as B_03');
        $this->db->select('c.name as lokasi_kerja');
        $this->db->select('d.name as lokasi_gaji');
        $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju,');
        $this->db->from($this->historyp3ks);
        $this->db->join($this->pegawaip3ks, 'b.id = a.pegawai_id');
        $this->db->join('simgaji_lokasis c', 'c.id = b.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji');
        $this->db->where('a.periode',$periode);

        
        
        if ($role == 2) {
            $B_02B = $this->session->userdata('B_02B');
            $d = ifVerif($B_02B);
            if($d != '');
            
            $select = explode(',',$d);
            $datasel = NULL;
            foreach ($select as $key => $val){
                // $datasel[$key] = $val;
                $this->db->or_where('b.lokasi_gaji', $val);
            }


            // $x = array_values($datasel);
            // $this->db->where_in('d.id', $x);
            // $this->db->where_in('c.id', ['2812','3419']);

        }




        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->rekapp3ks_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->rekapp3ks_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->rekapp3ks_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->rekapp3ks_order)) {
            $order = $this->rekapp3ks_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->group_by('d.id');
    }

    private function _get_rekapp3ks_datatables_query_old2($periode)
    {
        $role = $this->session->userdata('role');

        // $this->db->select('c.name as lokasi_kerja, a.pegawai_id, c.id as id_lokasi');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak');
        // $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju');
        // $this->db->from($this->historyp3ks);
        // $this->db->join($this->pegawaip3ks, "b.id = a.pegawai_id AND a.periode = '$periode'  AND a.deleted_at IS NULL");
        // $this->db->join('simgaji_lokasis c', "c.kode = b.lokasikerja","RIGHT");

        // $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji');
        // $this->db->where('c.bool_id', 2);


        $this->db->select('a.*');
        // $this->db->select('b.nip as B_02B');
        $this->db->select('b.name as B_03');
        // $this->db->select('c.name as lokasi_kerja, c.id as id_lokasi');
        $this->db->select('d.name as lokasi_gaji, , d.id as id_lokasi');
        $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak,');
        $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju,');
        $this->db->from($this->historyp3ks);
        $this->db->join($this->pegawaip3ks, "b.id = a.pegawai_id AND a.periode = '$periode'  AND a.deleted_at IS NULL");
        // $this->db->join($this->pegawaip3ks, 'b.id = a.pegawai_id');
        // $this->db->join('simgaji_lokasis c', 'c.id = b.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = b.lokasi_gaji AND d.bool_id = 2');
        // $this->db->where('a.periode',$periode);


        
        if ($role == 2) {
            $B_02B = $this->session->userdata('B_02B');
            $d = ifVerif2($B_02B);
            if($d != '');
            
            $select = explode(',',$d);
            $datasel = NULL;
            foreach ($select as $key => $val){
                $datasel[$key] = $val;
                // $this->db->or_where('c.i', $val);
            }


            $x = array_values($datasel);

            $this->db->where_in('d.id', $x);
            // $this->db->where_in('c.id', ['2812','3419']);

        }




        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->rekapp3ks_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->rekapp3ks_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->rekapp3ks_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->rekapp3ks_order)) {
            $order = $this->rekapp3ks_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->group_by('d.id');
    }

    function get_rekapp3ks($periode)
    {
        $this->_get_rekapp3ks_datatables_query($periode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function rekapp3ks_count_filtered($periode)
    {
        $this->_get_rekapp3ks_datatables_query($periode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function rekapp3ks_count_all($periode)
    {
        // $periode = $tahun.'-'.$bulan.'-01';
        $this->db->from($this->rekapp3ks);
        $this->db->where('periode', $periode);
        $this->db->where('deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    private function _get_rekapp3ks_datatables_query_by_verifikator($periode)
    {
        $role = $this->session->userdata('role');
        $id_ver = $this->session->userdata('id');
        $pengampu = $this->session->userdata('pengampu');
        

        $this->db->select('`c`.`nip` as `nip`, `c`.`nama` as `nama`,`c`.`id` as `id_verifikator`, c.kode_skpd');
        $this->db->select('SUM(IF(a.status_acc_bkd = 0, 1, 0)) as belum');
        $this->db->select('SUM(IF(a.status_acc_bkd = 2, 1, 0)) as revisi');
        $this->db->select('SUM(IF(a.status_acc_bkd = 3, 1, 0)) as tolak');
        $this->db->select('SUM(IF(a.status_acc_bkd = 1, 1, 0)) as setuju');
        $this->db->from($this->historyp3ks);
        // $this->db->join($this->pegawaip3ks, "b.id = a.pegawai_id AND a.periode = '$periode'  AND a.deleted_at IS NULL");
       
        if($role == 2){
            $this->db->join('simgaji_verifikator c', "c.id = a.verifikator AND `a`.`periode` = '$periode' AND c.kode_skpd = '$pengampu' AND `a`.`deleted_at` IS NULL ");
        }else{
            $this->db->join('simgaji_verifikator c', "c.id = a.verifikator AND `a`.`periode` = '$periode' AND `a`.`deleted_at` IS NULL ", 'RIGHT');
        }
       
        
        // $this->db->where('c.bool_id', 2);

       
        
        // if ($role == 2) {
        //     $d = ifVerif($this->session->userdata('B_02B'));
        //     if($d != '');
            
        //     $select = explode(',',$d);
        //     $datasel = NULL;
        //     foreach ($select as $key => $val){
        //         $datasel[$key] = $val;
        //         // $this->db->or_where('c.i', $val);
        //     }


        //     $x = array_values($datasel);

        //     $this->db->where_in('c.id', $x);
        //     // $this->db->where_in('c.id', ['2812','3419']);

        // }

        // $this->db->where_in('c.id', ['2812','3419']);

        




        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->rekapp3ks_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->rekapp3ks_column_searchby_verifikator) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->rekapp3ks_column_orderby_verifikator[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->rekapp3ks_orderby_verifikator)) {
            $order = $this->rekapp3ks_orderby_verifikator;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        $this->db->group_by('c.id');
    }

    function get_rekapp3ks_by_verifikator($periode)
    {
        $this->_get_rekapp3ks_datatables_query_by_verifikator($periode);
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function rekapp3ks_count_filtered_by_verifikator($periode)
    {
        $this->_get_rekapp3ks_datatables_query($periode);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function rekapp3ks_count_all_by_verifikator($periode)
    {
        // $periode = $tahun.'-'.$bulan.'-01';
        $this->db->from($this->rekapp3ks);
        $this->db->where('periode', $periode);
        $this->db->where('deleted_at IS NULL');
        return $this->db->count_all_results();
    }




    public function getrekapp3ksById($id)
    {
        $sql =
            "SELECT *
        FROM $this->rekapp3ks
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataPns($nip, $A_01)
    {
        $x = '%' . $nip . '%';
        $sql =
            "SELECT CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.B_02B id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
        FROM MASTFIP08 mastfip
        JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
        JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
        JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
        WHERE 
        (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
        AND mastfip.`A_01` = ?
        AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";

        $query = $this->dbeps->query($sql, array($nip, $x, $A_01));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    //CRUD JFT END//




}
