<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikator_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->verifikators = 'simgaji_verifikator a';

        
    }

     //CRUD JFT START//
    var $verifikator_column_order = array('a.id', 'a.nip', 'a.type_role'); //set column field database for datatable orderable
    var $verifikator_column_search = array('a.nip', 'a.type_role'); //set column field database for datatable searchable
    var $verifikator_order = array('a.id' => 'asc'); // default order 

    private function _get_verifikator_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->verifikators);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->verifikator_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->verifikator_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->verifikator_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->verifikator_order)) {
            $order = $this->verifikator_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_verifikator()
    {
        $this->_get_verifikator_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function verifikator_count_filtered()
    {
        $this->_get_verifikator_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function verifikator_count_all()
    {
        $this->db->from($this->verifikator);
        $this->db->where('deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getVerifikatorById($id){
        $sql = 
        "SELECT *
        FROM $this->verifikator
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

    public function getDataPns($nip, $A_01){
        $x = '%'.$nip.'%';
        $sql =
        "SELECT  CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.B_02B id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
        FROM MASTFIP08 mastfip
        JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
        JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
        JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
        WHERE 
        (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
        AND mastfip.`A_01` = ?
        AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";

        $query = $this->dbeps->query($sql, array($nip, $x, $A_01));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataPnsAllDinas($nip){
        $x = '%'.$nip.'%';
        $sql =
        "SELECT *, CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`) full_A, mastfip.B_02B id, CONCAT(mastfip.`B_03A`, ' ', mastfip.`B_03`, ' ', mastfip.`B_03B`) name, mastfip.`I_5A`, mastfip.`I_05`, pangkat.`KODE` kode_gol, pangkat.`NAMAY` pkt, pangkat.`NAMAX` gol, tablok.`NALOKP` lokasi, tablok8.`nm` unit_kerja, mastfip.`I_06` esel
        FROM MASTFIP08 mastfip
        JOIN `TABPKT` pangkat ON mastfip.`F_03` = pangkat.`KODE`
        JOIN `TABLOKB08` tablok ON mastfip.`A_01` = tablok.`A_01`
        JOIN `TABLOK08` tablok8 ON mastfip.`A_01` = tablok8.`kd`
        WHERE 
        (mastfip.`B_02B` = ? OR mastfip.`B_03` LIKE ?)
        AND tablok.`KOLOK` = CONCAT(mastfip.`A_01`, mastfip.`A_02`, mastfip.`A_03`, mastfip.`A_04`, mastfip.`A_05`)";

        $query = $this->dbeps->query($sql, array($nip, $x));
        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

     //CRUD JFT END//



    
}