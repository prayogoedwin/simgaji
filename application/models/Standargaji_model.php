<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Standargaji_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->mastersotks = 'mastersotks a';
        $this->masterjfts = 'masterjfts a';
        $this->masterjfus = 'masterjfus a';

        
    }


    //CRUD SOTK START//
    var $struktural_column_order = array('a.id', 'a.nama','a.kelas','a.nominal'); //set column field database for datatable orderable
    var $struktural_column_search = array( 'a.nama','a.kelas','a.nominal'); //set column field database for datatable searchable
    var $struktural_order = array('a.id' => 'asc'); // default order 

    private function _get_struktural_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->mastersotks);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->struktural_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->struktural_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->struktural_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->struktural_order)) {
            $order = $this->struktural_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_struktural()
    {
        $this->_get_struktural_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function struktural_count_filtered()
    {
        $this->_get_struktural_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function struktural_count_all()
    {
        $this->db->from($this->mastersotks);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getStrukturalById($id){
        $sql = 
        "SELECT *
        FROM $this->mastersotks
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
     //CRUD SOTK END//


     //CRUD JFT START//
    var $jft_column_order = array('a.id', 'a.nama','a.kelas','a.nominal'); //set column field database for datatable orderable
    var $jft_column_search = array( 'a.nama','a.kelas','a.nominal'); //set column field database for datatable searchable
    var $jft_order = array('a.id' => 'asc'); // default order 

    private function _get_jft_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->masterjfts);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->jft_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->jft_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->jft_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->jft_order)) {
            $order = $this->jft_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_jft()
    {
        $this->_get_jft_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function jft_count_filtered()
    {
        $this->_get_jft_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function jft_count_all()
    {
        $this->db->from($this->masterjfts);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getJftById($id){
        $sql = 
        "SELECT *
        FROM $this->masterjfts
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

     //CRUD JFT END//



      //CRUD JFU START//
    var $jfu_column_order = array('a.id', 'a.nama','a.kelas','a.nominal'); //set column field database for datatable orderable
    var $jfu_column_search = array( 'a.nama','a.kelas','a.nominal'); //set column field database for datatable searchable
    var $jfu_order = array('a.id' => 'asc'); // default order 

    private function _get_jfu_datatables_query()
    {
        $this->db->select('a.*');
        $this->db->from($this->masterjfus);
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->jfu_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->jfu_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->jfu_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->jfu_order)) {
            $order = $this->jfu_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_jfu()
    {
        $this->_get_jfu_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function jfu_count_filtered()
    {
        $this->_get_jfu_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function jfu_count_all()
    {
        $this->db->from($this->masterjfus);
        $this->db->where('a.deleted_at IS NULL');
        return $this->db->count_all_results();
    }

    public function getJfuById($id){
        $sql = 
        "SELECT *
        FROM $this->masterjfus
        WHERE a.id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }

     //CRUD JFU END//
}