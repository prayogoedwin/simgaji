<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wsdisdik_model extends CI_Model{

    var $client_apikey = "BKDJATENG";
    // var $auth       = "8c261532-116f-11ed-861d-0242ac120002";

   // public $dbgaji = NULL;

   // public function __construct(){
     //   parent::__construct();

     //   $this->dbgaji = $this->load->database('gaji', TRUE);
   // }

	public $dbgaji = NULL;
    public $dbgaji_prod = NULL;

    public function __construct(){
        parent::__construct();

        $this->dbgaji = $this->load->database('gaji', TRUE);
        $this->dbgaji_prod = $this->load->database('gaji_prod', TRUE);
    }

    public function check_auth_client(){
        $auth  = $this->input->get_request_header('Authorization', TRUE);
        $client_apikey = $this->input->get_request_header('x-api-key', TRUE);

        $tgl = date('Y-m-d');
        $auth_generate = $this->validate_token('3', $auth, $tgl);

        if($auth_generate == null){
            return json_output(401,array('status' => 401,'message' => 'Unauthorized token'));
        } else

        if($client_apikey == $this->client_apikey && $auth == $auth_generate->token){
            return true;
        } else {
            return json_output(401,array('status' => 401,'message' => 'Unauthorized'));
        }
    }

    public function getGajiPnsByPeriodeUkSuk($periode, $uk, $sbuk){
        if($uk == '34' && $sbuk !== 'all'){

			$fixuk = '34' . $sbuk;

            $this->dbgaji->select('kalkulasis.periode AS PERIODE GAJI,
			pegawais.name AS NAMA,
			pegawais.nip AS NIP,pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais','kalkulasis.nip=pegawais.nip','left');
			$this->dbgaji->join('lokasis','pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('kalkulasis.lokasi_kode', $fixuk, 'after');
			// $this->dbgaji->where('kalkulasis.nip',$sbuk);
			$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			$this->dbgaji->order_by('kalkulasis.golongan_id','DESC');
			#$this->dbgaji->limit(20);
			$data = $this->dbgaji->get()->result();
        } else if($uk == '34' && $sbuk == 'all'){
            $this->dbgaji->select('kalkulasis.periode AS PERIODE GAJI,
			pegawais.name AS NAMA,
			pegawais.nip AS NIP,pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais','kalkulasis.nip=pegawais.nip','left');
			$this->dbgaji->join('lokasis','pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('kalkulasis.lokasi_kode', $uk, 'after');
			// $this->dbgaji->where('kalkulasis.nip',$sbuk);
			//$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			//$this->dbgaji->order_by('kalkulasis.golongan_id','DESC');
			#$this->dbgaji->limit(20);
			$data = $this->dbgaji->get()->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getGajiPnsByPeriodeNip($periode, $nip){
        $this->dbgaji->select('kalkulasis.periode AS PERIODE GAJI,
			pegawais.name AS NAMA,
			pegawais.nip AS NIP,pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			lokasis.name AS SKPD,kalkulasis.lokasi_string AS UNIT_KERJA,
			kalkulasis.golongan_string AS GOLONGAN,kalkulasis.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			kalkulasis.askes AS BPJS_4%_GAJI');
			$this->dbgaji->from('kalkulasis');
			$this->dbgaji->join('pegawais','kalkulasis.nip=pegawais.nip','left');
			$this->dbgaji->join('lokasis','pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->where('kalkulasis.nip', $nip);
			// $this->dbgaji->where('kalkulasis.nip',$sbuk);
			//$this->dbgaji->order_by('kalkulasis.lokasi_kode');
			//$this->dbgaji->order_by('kalkulasis.golongan_id','DESC');
			#$this->dbgaji->limit(20);
			return $this->dbgaji->get()->result();
    }

	public function getGajiP3kByPeriodeNip($periode, $nip){
		$this->dbgaji_prod->select('simgaji_kalkulasip3ks.periode AS PERIODE GAJI_PPPK,
		simgaji_pegawaip3ks.name AS NAMA,
		simgaji_pegawaip3ks.nip AS NIP,
		simgaji_pegawaip3ks.nik AS NIK,
		simgaji_pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
		simgaji_lokasis.name AS SKPD,
		simgaji_kalkulasip3ks.lokasi_string AS UNIT_KERJA,
		simgaji_kalkulasip3ks.golongan_string AS GOLONGAN,
		simgaji_kalkulasip3ks.jabatan AS JABATAN,
		simgaji_kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
		simgaji_kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
		simgaji_kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
		simgaji_kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
		simgaji_kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
		simgaji_kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
		simgaji_kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
		simgaji_kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
		simgaji_kalkulasip3ks.tunjangan_umum AS TJ_UMUM,
		0 AS SERTIFIKASI_GURU,
		0 AS JASPEL,
		simgaji_kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
		simgaji_kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgaji_prod->from('simgaji_kalkulasip3ks');
			$this->dbgaji_prod->join('simgaji_pegawaip3ks','simgaji_kalkulasip3ks.nip=simgaji_pegawaip3ks.nip','left');
			$this->dbgaji_prod->join('simgaji_lokasis','simgaji_pegawaip3ks.lokasikerja=simgaji_lokasis.kode');
			$this->dbgaji_prod->where('simgaji_kalkulasip3ks.periode', $periode);
			$this->dbgaji_prod->where('simgaji_kalkulasip3ks.nip', $nip);
			// $this->dbgaji->like('kalkulasip3ks.lokasi_kode', $uk, 'after');
			$this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.lokasi_kode');
			$this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.golongan_id','DESC');
			return $this->dbgaji_prod->get()->result();
	}

	public function getGajiP3kByPeriodeUkSuk($periode, $uk, $sbuk){
		// if($uk == '34' && $sbuk !== 'all'){
		// 	$fixuk = '34' . $sbuk;

		// 	$this->dbgaji->select('kalkulasip3ks.periode AS PERIODE GAJI,
		// 	pegawaip3ks.names AS NAMA,
		// 	pegawaip3ks.nip AS NIP,
		// 	pegawaip3ks.nik AS NIK,
		// 	pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
		// 	lokasis.name AS SKPD,
		// 	kalkulasip3ks.lokasi_string AS UNIT_KERJA,
		// 	kalkulasip3ks.golongan_string AS GOLONGAN,
		// 	kalkulasip3ks.jabatan AS JABATAN,
		// 	kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
		// 	kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
		// 	kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
		// 	kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
		// 	kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
		// 	kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
		// 	kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
		// 	kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
		// 	kalkulasip3ks.tunjangan_umum AS TJ_UMUM,0
		// 	AS SERTIFIKASI_GURU,
		// 	0 AS JASPEL,
		// 	kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
		// 	kalkulasip3ks.askes AS BPJS_4%_GAJI');
		// 	$this->dbgaji->from('kalkulasip3ks');
		// 	$this->dbgaji->join('pegawaip3ks','kalkulasip3ks.nip=pegawaip3ks.nip','left');
		// 	$this->dbgaji->join('lokasis','pegawaip3ks.lokasikerja=lokasis.kode');
		// 	$this->dbgaji->where('kalkulasip3ks.periode', $periode);
		// 	$this->dbgaji->like('kalkulasip3ks.lokasi_kode', $fixuk, 'after');
		// 	$this->dbgaji->order_by('kalkulasip3ks.lokasi_kode');
		// 	$this->dbgaji->order_by('kalkulasip3ks.golongan_id','DESC');
		// 	$data = $this->dbgaji->get()->result();
		// } else
		if($uk == '34' && $sbuk == 'all'){
			$this->dbgaji_prod->select('simgaji_kalkulasip3ks.periode AS PERIODE GAJI,
			simgaji_pegawaip3ks.name AS NAMA,
			simgaji_pegawaip3ks.nip AS NIP,
			simgaji_pegawaip3ks.nik AS NIK,
			simgaji_pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			simgaji_lokasis.name AS SKPD,
			simgaji_kalkulasip3ks.lokasi_string AS UNIT_KERJA,
			simgaji_kalkulasip3ks.golongan_string AS GOLONGAN,
			simgaji_kalkulasip3ks.jabatan AS JABATAN,
			simgaji_kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
			simgaji_kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
			simgaji_kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
			simgaji_kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
			simgaji_kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
			simgaji_kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			simgaji_kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			simgaji_kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
			simgaji_kalkulasip3ks.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			simgaji_kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			simgaji_kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgaji_prod->from('simgaji_kalkulasip3ks');
			$this->dbgaji_prod->join('simgaji_pegawaip3ks','simgaji_kalkulasip3ks.nip=simgaji_pegawaip3ks.nip','left');
			$this->dbgaji_prod->join('simgaji_lokasis','simgaji_pegawaip3ks.lokasikerja=simgaji_lokasis.kode');
			$this->dbgaji_prod->where('simgaji_kalkulasip3ks.periode', $periode);
			$this->dbgaji_prod->like('simgaji_kalkulasip3ks.lokasi_kode', $uk, 'after');
			$this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.lokasi_kode');
			$this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.golongan_id','DESC');
			$data = $this->dbgaji_prod->get()->result();
		} else {
			// $data = array();

			$this->dbgaji_prod->select('simgaji_kalkulasip3ks.periode AS PERIODE GAJI,
			simgaji_pegawaip3ks.name AS NAMA,
			simgaji_pegawaip3ks.nip AS NIP,
			simgaji_pegawaip3ks.nik AS NIK,
			simgaji_pegawaip3ks.no_bpjskes AS NO_BPJS_KES,
			simgaji_lokasis.name AS SKPD,
			simgaji_kalkulasip3ks.lokasi_string AS UNIT_KERJA,
			simgaji_kalkulasip3ks.golongan_string AS GOLONGAN,
			simgaji_kalkulasip3ks.jabatan AS JABATAN,
			simgaji_kalkulasip3ks.istri AS JUMLAH_ISTRI/SUAMI,
			simgaji_kalkulasip3ks.anak AS JUMLAH_ANAK TERTANGGUNG,
			simgaji_kalkulasip3ks.gaji_pokok AS GAJI_POKOK,
			simgaji_kalkulasip3ks.tunjangan_istri AS TJ_ISTRI,
			simgaji_kalkulasip3ks.tunjangan_anak AS TJ_ANAK,
			simgaji_kalkulasip3ks.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			simgaji_kalkulasip3ks.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			simgaji_kalkulasip3ks.tunjangan_fungsional AS TJ_FUNGSIONAL,
			simgaji_kalkulasip3ks.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			simgaji_kalkulasip3ks.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			simgaji_kalkulasip3ks.askes AS BPJS_4%_GAJI');
			$this->dbgaji_prod->from('simgaji_kalkulasip3ks');
			$this->dbgaji_prod->join('simgaji_pegawaip3ks','simgaji_kalkulasip3ks.nip=simgaji_pegawaip3ks.nip','left');
			$this->dbgaji_prod->join('simgaji_lokasis','simgaji_pegawaip3ks.lokasikerja=simgaji_lokasis.kode');
			$this->dbgaji_prod->where('simgaji_kalkulasip3ks.periode', $periode);
			$this->dbgaji_prod->like('simgaji_kalkulasip3ks.lokasi_kode', $uk, 'after');
			$this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.lokasi_kode');
			$this->dbgaji_prod->order_by('simgaji_kalkulasip3ks.golongan_id','DESC');
			$data = $this->dbgaji_prod->get()->result();
		}

		return $data;
	}

	public function getTppPnsByPeriodeUkSuk($periode, $uk, $sbuk){

		if($sbuk !== 'all'){
			$fixuk = '34' . $sbuk;
			$this->dbgaji->select('tpps.periode AS periode tpp,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,tpps.name AS NAMA,
			tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis','kalkulasis.nip=tpps.nip','left');
			$this->dbgaji->join('pegawais','tpps.nip=pegawais.nip','left');
			$this->dbgaji->join('lokasis','pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('tpps.lokasi_kode', $fixuk, 'after');
			$this->dbgaji->order_by('tpps.lokasi_kode');
			$this->dbgaji->order_by('tpps.golongan_id','DESC');
			$data = $this->dbgaji->get()->result();
		} else if ($sbuk == 'all'){
			$this->dbgaji->select('tpps.periode AS periode tpp,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,tpps.name AS NAMA,
			tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis','kalkulasis.nip=tpps.nip','left');
			$this->dbgaji->join('pegawais','tpps.nip=pegawais.nip','left');
			$this->dbgaji->join('lokasis','pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->like('tpps.lokasi_kode', $uk, 'after');
			// $this->dbgaji->where('tpps.nip', $sbuk);
			//$this->dbgaji->order_by('tpps.lokasi_kode');
			//$this->dbgaji->order_by('tpps.golongan_id','DESC');
			$data = $this->dbgaji->get()->result();
		} else {
			$data = array();
		}

		return $data;
		
	}

	public function getTppPnsByPeriodeNip($periode, $nip){
		$this->dbgaji->select('tpps.periode AS periode tpp,
			lokasis.name AS SKPD,
			tpps.lokasi_string AS UNIT_KERJA,tpps.name AS NAMA,
			tpps.nip AS NIP,
			pegawais.nik AS NIK,
			pegawais.no_bpjskes AS NO_BPJS_KES,
			tpps.golongan_string AS GOLONGAN,
			tpps.jabatan AS JABATAN,
			kalkulasis.istri AS JUMLAH_ISTRI/SUAMI,
			kalkulasis.anak AS JUMLAH_ANAK TERTANGGUNG,
			kalkulasis.gaji_pokok AS GAJI_POKOK,
			kalkulasis.tunjangan_istri AS TJ_ISTRI,
			kalkulasis.tunjangan_anak AS TJ_ANAK,
			kalkulasis.jumlah_tunjangan_keluarga AS TJ_KELUARGA,
			kalkulasis.tunjangan_struktural AS TJ_STRUKTURAL/ESELON,
			kalkulasis.tunjangan_fungsional AS TJ_FUNGSIONAL,
			kalkulasis.tunjangan_umum AS TJ_UMUM,
			0 AS SERTIFIKASI_GURU,
			0 AS JASPEL,
			(tpps.tpp+tpps.potongan_tpp_bpjs) AS TPP,
			(tpps.konker+tpps.bpjs_konker) AS TPP_KONDISI_KERJA,
			(tpps.jakarta+tpps.bpjs_jakarta) AS TPP_TEMPAT_BERTUGAS,
			kalkulasis.potongan_bpjs_kesehatan AS BPJS_1%_GAJI,
			tpps.potongan_tpp_bpjs AS BPJS_1%_TPP,
			tpps.bpjs_konker AS BPJS_1%_TPP_KONDISI_KERJA,
			tpps.bpjs_jakarta AS BPJS_1%_TPP_TEMPAT_BEKERJA,
			kalkulasis.askes AS BPJS_4%_GAJI,
			tpps.askes_tpp AS BPJS_4%_TPP,
			tpps.askes_konker AS BPJS_4%_TPP_KONDISI_KERJA,
			tpps.askes_jakarta AS BPJS_4%_TPP_TEMPAT_BEKERJA');
			$this->dbgaji->from('tpps');
			$this->dbgaji->join('kalkulasis','kalkulasis.nip=tpps.nip','left');
			$this->dbgaji->join('pegawais','tpps.nip=pegawais.nip','left');
			$this->dbgaji->join('lokasis','pegawais.lokasikerja=lokasis.kode');
			$this->dbgaji->where('tpps.periode', $periode);
			$this->dbgaji->where('kalkulasis.periode', $periode);
			$this->dbgaji->where('tpps.nip', $nip);
			$this->dbgaji->order_by('tpps.lokasi_kode');
			$this->dbgaji->order_by('tpps.golongan_id','DESC');
			return $this->dbgaji->get()->result();	
	}

	public function update_token($table, $stakeholder_code, $data){
        $this->db->where('stakeholder_code', $stakeholder_code);
		$delete = $this->db->update($table, $data); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
    }

	private function validate_token($stakeholder_code, $token, $tgl){
        $sql = 
        "SELECT *
        FROM simgaji_token
        WHERE `stakeholder_code` = ?
        AND token = ?
        AND date(created_at) = ?";

        $query = $this->db->query($sql, array($stakeholder_code, $token, $tgl));
        if($query->num_rows() > 0){
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }
}