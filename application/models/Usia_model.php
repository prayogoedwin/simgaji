<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usia_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    //START KEDUDUKAN
    var $kedudukan_column_order = array('a.id','a.kode','a.name', 'a.usia'); //set column field database for datatable orderable
    var $kedudukan_column_search = array('a.kode','a.name', 'a.usia'); //set column field database for datatable searchable
    var $kedudukan_order = array('a.id' => 'asc'); // default order 

    private function getKedudukanDatatablesQuery(){
        $this->db->select('a.*');
        $this->db->from('simgaji_kedudukans a');
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->kedudukan_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->kedudukan_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->kedudukan_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->kedudukan_order)) {
            $order = $this->kedudukan_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDataKedudukan(){
        $this->getKedudukanDatatablesQuery();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataKedudukanCount(){

        $sql = 
        "SELECT COUNT(id) jml
        FROM `simgaji_kedudukans`
        WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row()->jml;
        } else {
            $data = null;
        }
        
        return $data;
    }

    public function getDataKedudukanCountFiltered(){
        $this->getKedudukanDatatablesQuery();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getDataKedudukanById($id){
        $sql = 
        "SELECT *
        FROM `simgaji_kedudukans`
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
    //END KEDUDUKAN

    //START FUNGSIONAL
    var $fungsional_column_order = array('a.id','a.kode','a.name', 'a.usia'); //set column field database for datatable orderable
    var $fungsional_column_search = array('a.kode','a.name', 'a.usia'); //set column field database for datatable searchable
    var $fungsional_order = array('a.id' => 'asc'); // default order 

    private function getFungsionalDatatablesQuery(){
        $this->db->select('a.*');
        $this->db->from('simgaji_fungsionals a');
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->fungsional_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->fungsional_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->fungsional_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->fungsional_order)) {
            $order = $this->fungsional_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDataFungsional(){
        $this->getFungsionalDatatablesQuery();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataFungsionalCount(){

        $sql = 
        "SELECT COUNT(id) jml
        FROM `simgaji_fungsionals`
        WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row()->jml;
        } else {
            $data = null;
        }
        
        return $data;
    }

    public function getDataFungsionalCountFiltered(){
        $this->getFungsionalDatatablesQuery();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getDataFungsionalById($id){
        $sql = 
        "SELECT *
        FROM `simgaji_fungsionals`
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
    //END FUNGSIONAL
}