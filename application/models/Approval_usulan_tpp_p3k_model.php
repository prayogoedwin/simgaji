<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval_usulan_tpp_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDataPeriode(){
        $sql  =
        "SELECT DISTINCT(periode)
        FROM `simgaji_historyp3ks_tpp`";

        $query = $this->db->query($sql);
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getStatusAllDataByPeriode($periode, $mynip){
        $sql =
        "SELECT p.`id`, h.*
        FROM `simgaji_historyp3ks_tpp` h
        JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
        WHERE h.`periode` = ?
        AND h.`posisi_acc` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $mynip));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function getDataHistory($kolok, $periode, $B_02B){
        // $sql =
        // "SELECT *
        // FROM simgaji_historyp3ks_acc
        // WHERE A_01 = ?
        // AND periode = ?
        // AND nip = ?
        // GROUP BY nip";

        // $sql =
        // "SELECT *
        // FROM simgaji_historyp3ks_acc
        // WHERE periode = ?
        // AND (nip = ? OR A_01 = ?)
        // GROUP BY nip
        // ORDER BY id ASC";
        $A_01 = substr($kolok, 0, 2);
        

        $sql =
        "SELECT h.*
        FROM bkd_gaji_dev.simgaji_historyp3ks_tpp_acc h
        WHERE h.periode = ?
        AND (h.nip = ? OR h.nip = (SELECT B_02B FROM eps.MASTFIP08 WHERE A_01 = ? AND A_02 = '00' AND A_03 = '00' AND A_04 = '00' AND A_05 = '00' AND I_5A = '1') OR (h.`A_01` = ?))
        AND deleted_at IS NULL";

        $query = $this->db->query($sql, array($periode, $B_02B, $A_01, $A_01));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataAtas($periode, $B_02B, $user_id, $role){
        if($role == 5){
            //sekretaris
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks_tpp` h
            JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`sekretaris_by` = ?)
            GROUP BY p.`id`";

        } else if($role == 6){
            //kepala SKPD
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks_tpp` h
            JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`kepala_skpd_by` = ?)
            GROUP BY p.`id`";
        } else if($role == 3){
            $sql =
            "SELECT p.`id`, p.`nip`, p.`name` nama
            FROM `simgaji_historyp3ks_tpp` h
            JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
            WHERE h.`deleted_at` IS NULL
            AND h.`periode` = ?
            AND (h.`posisi_acc` = ? OR h.`kasubbag_keu_by` = ?)
            GROUP BY p.`id`";
        }


        $query = $this->db->query($sql, array($periode, $B_02B, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getForKumulatifSekKadinas($role, $periode, $user_id){

        if($role == 5){
            //sekretaris
            $posted = "AND h.`sekretaris_by` = '$user_id' ";
        } else if($role == 6){
            //kadis
            $posted = "AND h.`kepala_skpd_by` = '$user_id' ";
        }

        $sql =
        "SELECT p.`id`, p.nip, p.name, SUM(IF(h.status_acc_bkd = 0, 1, 0)) as belum, SUM(IF(h.status_acc_bkd = 2, 1, 0)) as revisi, SUM(IF(h.status_acc_bkd = 3, 1, 0)) as tolak, SUM(IF(h.status_acc_bkd = 1, 1, 0)) as setuju
        FROM `simgaji_historyp3ks_tpp` h
        JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        $posted
        AND h.`periode` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getHistoryP3ksAccLastId($nip, $periode){
        $sql =
        "SELECT *
        FROM `simgaji_historyp3ks_tpp_acc`
        WHERE nip = ?
        AND periode = ?
        ORDER BY id DESC
        LIMIT 1";

        $query = $this->db->query($sql, array($nip, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }

        return $data;
    }

    public function updateDataUsulanByPeriode($table, $data, $periode, $mynip){
        $this->db->where('periode', $periode);
        $this->db->where('posisi_acc', $mynip);
		$update = $this->db->update($table, $data);

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }

    public function getDataKadisForUpdate($periode, $B_02B, $user_id){
        //get data kadis untuk update data master jika verif bkd sudh acc
        $sql =
        "SELECT p.`id`, p.`nip`, p.`name` nama, h.`status_acc_bkd`
        FROM `simgaji_historyp3ks_tpp` h
        JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL AND h.`periode` = ?
        AND (h.`posisi_acc` = ? OR h.`kepala_skpd_by` = ?)";

        $query = $this->db->query($sql, array($periode, $B_02B, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }
}