<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterpegawai_p3ks_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
        $this->pegawaip3ks = 'simgaji_pegawaip3ks a';

        
    }

    function cek_nip($nip){
        $this->db->select('nip');
        $this->db->where('nip', $nip);
        $hasil = $this->db->get('simgaji_pegawaip3ks');
        $tot_rows = $hasil->num_rows();
            if($tot_rows > 0){
                    return 1;
            } else {
                    return 0;
            }
    
    }

    //CRUD PEGAWAI P3K START//
    var $pegawais_column_order = array('a.id',  'a.nip', 'a.name'); //set column field database for datatable orderable
    var $pegawais_column_search = array('a.id', 'a.nip', 'a.name'); //set column field database for datatable searchable
    var $pegawais_order = array('a.nip' => 'ASC'); // default order 


    private function _get_pegawais_datatables_query()
    {

        // SELECT `a`.*, `b`.`nip` , `b`.`name` , SUM(IF(status_acc_bkd = 0, 1, 0)) as belum, SUM(IF(status_acc_bkd = 2, 1, 0)) as tolak, SUM(IF(status_acc_bkd = 1, 1, 0)) as setuju  FROM `simgaji_historyp3ks` `a` 
        // JOIN `simgaji_pegawaip3ks` `b` ON `b`.`id` = `a`.`pegawai_id` 
        // WHERE `a`.`periode` = '2022-01-01' AND `a`.`deleted_at` IS NULL GROUP BY pegawai_id

        // $periode = $tahun.'-'.$bulan.'-01';

        // status ajuan = 3, status acc = 1, kepala_skpd_by IS NOT NULL
        $this->db->select('a.*');
        $this->db->select('c.name as lokasi_kerja');
        $this->db->select('d.name as lokasi_gaji');
        $this->db->from($this->pegawaip3ks);
        $this->db->join('simgaji_lokasis c', 'c.id = a.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = a.lokasi_gaji');

        if($this->input->post('lokasi_kerja'))
        {
            $lokasi_kerja = $this->input->post('lokasi_kerja');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('a.lokasikerja', $lk);
        }
    
        
        if($this->input->post('lokasi_gaji'))
        {
            $lokasi_kerja = $this->input->post('lokasi_gaji');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('a.lokasigaji', $lk);
        }

        

        // if($this->input->post('lokasi_kerja'))
        // {
        //     $lokasi_kerja = $this->input->post('lokasi_kerja');
        //     $lk = substr($lokasi_kerja,0,6);
        //     $this->db->like('a.lokasi_kerja', $lk);
        // }
        
        // if($this->input->post('lokasi_gaji'))
        // {
        //     $this->db->like('a.lokasi_gaji', $this->input->post('lokasi_gaji'));
        // }

        // if($this->input->post('nip_s'))
        // {
        //     $this->db->like('a.nip', $this->input->post('nip_s'));
        // }

        // if($this->input->post('nama_s'))
        // {
        //     $this->db->like('a.name', $this->input->post('nama_s'));
        // }

        // if($this->input->post('lokasi_kerja_s'))
        // {
        //     $lokasi_kerja2 = $this->input->post('lokasi_kerja_s');
        //     $lk2 = substr($lokasi_kerja2,0,6);
        //     $this->db->like('a.lokasi_kerja', $lk2);
        // }

        // if($this->input->post('lokasi_gaji_s'))
        // {
        //     $lokasi_kerja2 = $this->input->post('lokasi_gaji_s');
        //     $lk2 = substr($lokasi_kerja2,0,6);
        //     $this->db->like('a.lokasi_gaji', $lk2);
        // }

        $this->db->where('a.deleted_at IS NULL');
        
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->pegawais_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->pegawais_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->pegawais_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->pegawais_order)) {
            $order = $this->pegawais_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        // $this->db->group_by('a.id');
        
    }

    private function _get_pegawais_datatables_query_where($lokasi)
    {

        $this->db->select('a.*');
        $this->db->select('c.name as lokasi_kerja');
        $this->db->select('d.name as lokasi_gaji');
        $this->db->from($this->pegawaip3ks);
        $this->db->join('simgaji_lokasis c', 'c.id = a.lokasi_kerja');
        $this->db->join('simgaji_lokasis d', 'd.id = a.lokasi_gaji');


        // $lokasi_arr = explode(',',$lokasi);
        // foreach ($lokasi_arr as $val2){
        // $lk = substr($val2,0,6);
        // $this->db->or_like('a.lokasi_kerja',$lk);
        // }

        if($this->input->post('lokasi_kerja') == '' && $this->input->post('lokasi_gaji') == '')
        {
                $lokasi_arr = explode(',',$lokasi);
                foreach ($lokasi_arr as $val2){
                $lk = substr($val2,0,6);
                $this->db->or_like('a.lokasigaji',$lk);
                }
        }
        

        if($this->input->post('lokasi_kerja'))
        {
            $lokasi_kerja = $this->input->post('lokasi_kerja');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('a.lokasikerja', $lk);
        }
    
        
        if($this->input->post('lokasi_gaji'))
        {
            $lokasi_kerja = $this->input->post('lokasi_gaji');
            $lk = substr($lokasi_kerja,0,6);
            $this->db->like('a.lokasigaji', $lk);
        }

        // if($this->input->post('nip_s'))
        // {
        //     $this->db->like('a.nip', $this->input->post('nip_s'));
        // }

        // if($this->input->post('nama_s'))
        // {
        //     $this->db->like('a.name', $this->input->post('nama_s'));
        // }

        // if($this->input->post('lokasi_kerja_s'))
        // {
        //     $lokasi_kerja2 = $this->input->post('lokasi_kerja_s');
        //     $lk2 = substr($lokasi_kerja2,0,6);
        //     $this->db->like('a.lokasi_kerja', $lk2);
        // }

        // if($this->input->post('lokasi_gaji_s'))
        // {
        //     $lokasi_kerja2 = $this->input->post('lokasi_gaji_s');
        //     $lk2 = substr($lokasi_kerja2,0,6);
        //     $this->db->like('a.lokasi_gaji', $lk2);
        // }


        $this->db->where('a.deleted_at IS NULL');
        
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->pegawais_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->pegawais_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->pegawais_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->pegawais_order)) {
            $order = $this->pegawais_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }

        // $this->db->group_by('a.id');
        
    }

    function get_pegawai()
    {
        $this->_get_pegawais_datatables_query();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function get_pegawai_where($lokasi)
    {
        $this->_get_pegawais_datatables_query_where($lokasi);

        

        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function pegawai_count_filtered()
    {
        $this->_get_pegawais_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function pegawai_count_all()
    {
        $this->db->where('a.deleted_at IS NULL');
        $this->db->from($this->pegawaip3ks);
        return $this->db->count_all_results();
    }

    function pegawai_count_filtered_skpd($lokasi)
    {
        $this->_get_pegawais_datatables_query_where($lokasi);
        $query = $this->db->get();
        return $query->num_rows();
    }
    function pegawai_count_all_skpd($lokasi)
    {
        // $this->db->where('a.deleted_at IS NULL');
        // $this->db->from($this->pegawaip3ks);
        $this->_get_pegawais_datatables_query_where($lokasi);
        return $this->db->count_all_results();
    }

    
}