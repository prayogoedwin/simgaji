<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Potongan_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();
        
        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    var $potongan_column_order = array('a.id','a.golongan','a.potongan'); //set column field database for datatable orderable
    var $potongan_column_search = array('a.golongan','a.potongan'); //set column field database for datatable searchable
    var $potongan_order = array('a.id' => 'asc'); // default order 

    private function getPotonganDatatablesQuery(){
        $this->db->select('a.*');
        $this->db->from('simgaji_potongans a');
        $this->db->where('a.deleted_at IS NULL');
        // $this->db->order_by('a.id','desc');
        $i = 0;

        foreach ($this->potongan_column_search as $data) { // loop column 
            if (@$_POST['search']['value']) { // if datatable send POST for search
                if ($i === 0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($data, $_POST['search']['value']);
                } else {
                    $this->db->or_like($data, $_POST['search']['value']);
                }
                if (count($this->potongan_column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->potongan_column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->potongan_order)) {
            $order = $this->potongan_order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getData(){
        // $sql = 
        // "SELECT *
        // FROM `simgaji_potongans`
        // WHERE deleted_at IS NULL";

        // $query = $this->db->query($sql);

        // if ($query->num_rows() > 0) {
        //     $data = $query->result();
        // } else {
        //     $data = array();
        // }
        
        // return $data;

        $this->getPotonganDatatablesQuery();
        if (@$_POST['length'] != -1)
            $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataCount(){
        // $this->db->from('mastersotks');
        // $this->db->where('deleted_at IS NULL');
        // return $this->db->count_all_results();

        $sql = 
        "SELECT COUNT(id) jml
        FROM `simgaji_potongans`
        WHERE deleted_at IS NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            $data = $query->row()->jml;
        } else {
            $data = null;
        }
        
        return $data;
    }

    public function getDataCountFiltered(){
        $this->getPotonganDatatablesQuery();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getDataById($id){
        $sql = 
        "SELECT *
        FROM `simgaji_potongans`
        WHERE id = ?";

        $query = $this->db->query($sql, array($id));

        if ($query->num_rows() > 0) {
            $data = $query->row();
        } else {
            $data = null;
        }
        
        return $data;
    }
}