<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tpp_history_p3k_model extends CI_Model{
    public $db = NULL;
    public $dbeps = NULL;

    public function __construct(){
        parent::__construct();

        $this->tb_gajipokok = 'simgaji_gajip3ks';

        $this->db = $this->load->database('default', TRUE);
        $this->dbeps = $this->load->database('eps', TRUE);
    }

    public function getDataPeriode($bulan, $tahun){
        

        $sql = 
        "SELECT DISTINCT(CONCAT(tahun, '-',bulan, '-01')) periode
        FROM `simgaji_masttpp_kinerja`
        WHERE bulan = ?
        AND tahun = ?";

        // $sql = 
        // "SELECT DISTINCT(periode)
        // FROM `simgaji_historyp3ks_tpp`";

        $query = $this->db->query($sql, array($bulan, $tahun));
        if($query->num_rows() > 0){
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getStatusAllDataByPeriode($periode, $user_id){
        $sql =
        "SELECT p.`id`, h.`status_ajuan`, h.`updated_at`
        FROM `simgaji_historyp3ks_tpp` h
        JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
        WHERE h.`periode` = ?
        AND h.`posted_by` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataByKasubbagKeu($periode, $user_id){
        $sql =
        "SELECT h.`id` id_hist, p.`id`, p.`nip`, p.`name` nama, h.`updated_at`
        FROM `simgaji_historyp3ks_tpp` h
        JOIN `simgaji_pegawaip3ks_tpp` p ON h.`pegawai_id` = p.`id`
        WHERE h.`deleted_at` IS NULL
        AND h.`periode` = ?
        AND h.`posted_by` = ?
        GROUP BY p.`id`";

        $query = $this->db->query($sql, array($periode, $user_id));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function getDataHistory($A_01, $periode, $user_id){
        // $sql =
        // "SELECT *
        // FROM simgaji_historyp3ks_acc
        // WHERE periode = ?
        // AND (A_01 = ? OR posted_by = ?)
        // GROUP BY nip
        // ORDER BY id ASC";

        $sql =
        "SELECT h.*
        FROM bkd_gaji_dev.simgaji_historyp3ks_tpp_acc h
        JOIN eps.USER_NEW u ON h.posted_by = u.id
        JOIN eps.MASTFIP08 m ON u.username = m.B_02B
        WHERE h.posted_by = ?
        AND h.periode = ?
        AND (SELECT A_02 FROM eps.MASTFIP08 WHERE B_02B = u.username) = (SELECT A_02 FROM eps.MASTFIP08 WHERE B_02B = h.nip);";

        $query = $this->db->query($sql, array($user_id, $periode));

        if ($query->num_rows() > 0) {
            $data = $query->result();
        } else {
            $data = array();
        }

        return $data;
    }

    public function updateDataUsulanByPeriode($table, $data, $periode, $user_id){
        $this->db->where('periode', $periode);
        $this->db->where('posted_by', $user_id);
		$update = $this->db->update($table, $data);

		if ($update){
			return TRUE;
		} else {
			return FALSE;
		}
    }
}