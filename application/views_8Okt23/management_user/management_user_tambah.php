<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="<?=base_url('management_user')?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Management User</a>
                    <!-- <a href="<?=base_url('pengumuman')?>" class="breadcrumb-item">Pengumuman</a> -->
                    <span class="breadcrumb-item active">Tambah</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Form layouts -->
        <div class="row">

            <div class="col-md-6 offset-md-3">

                <!-- Vertical form -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data</h5>
                        <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="<?=base_url('management_user/tambah_action')?>" method="post">

                            <div class="form-group">
                                <label>NIP</label>
                                <select class="form-control" name="nips" id="nips" style="width:100%!important;" required></select>

                                <input type="hidden" name="nip" id="nip">
                                <input type="hidden" name="nama" id="nama">
                            </div>

                            <div class="form-group">
                                <label>Role</label>
                                <select class="form-control" name="role" id="role" required>
                                    <option value="">Pilih</option>
                                    <option value="1">Superadmin</option>
                                    <option value="3">Admin SKPD</option>
                                    <option value="4">Admin UPT / Sekolah</option>
                                </select>
                            </div>

                            <!-- <div class="form-group">
                                <label>Unit Organisasi</label>
                                <select class="form-control" name="unit_organisasi" id="unit_organisasi" required>
                                    <option value=""></option>
                                    <?php foreach ($unit_organisasi as $key => $val) :?>
                                    <option value="<?=$val->KOLOK;?>"><?=$val->NALOK;?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group" id="dSub_unit">
                                <label>Sub Unit</label>
                                <select name="sub_unit" id="sub_unit" class="form-control" required>
                                    <option value=""></option>
                                </select>
                            </div>

                            <div class="form-group" id="dSubSub_unit">
                                <label>Sub Sub Unit</label>
                                <select name="sub_sub_unit" id="sub_sub_unit" class="form-control" required>
                                    <option value=""></option>
                                </select>
                            </div> -->

                            <!-- <div class="form-group">
                                <label>Pengumuman</label>
                                <textarea rows="4" cols="4" class="form-control" placeholder="Isi pengumuman" name="pengumuman"></textarea>
                            </div> -->

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /vertical form -->

            </div>
        </div>
        <!-- /form layouts -->

    </div>
    <!-- /content area -->




<?php $this->load->view('template/footer');?>
</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $( document ).ready(function() {
        // console.log( "ready!" );
        // $('#dSub_unit').hide()
        // $("#sub_unit").prop('required',false);

        // $('#dSubSub_unit').hide()
        // $("#sub_sub_unit").prop('required',false);
    });
</script>

<script>
    var fixurl = '<?=base_url('management_user/cari_pns/');?>';
    $('#nips').select2({
        ajax: {
            url: fixurl,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    nipnya: params.term
                }
                
                // Query parameters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: "Masukkan NIP atau Nama PNS",
        minimumInputLength: 5,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });
    
    function formatRepo (repo) {
        if (repo.loading) {
            return repo.text;
        }
        
        var markup = "<div>" + repo.name + "</div><div>" + repo.id + "</div><div>" + repo.lokasi + "</div><div>" + repo.unit_kerja + "</div>";
        
        return markup;
    }
    
    function formatRepoSelection (repo) {
        return repo.id;
    }
    
    $('#nips').change(function(){
        var nip = $('#nips').select2('data')[0].id;
        var name = $('#nips').select2('data')[0].name;

        $('#nip').val(nip);
        $('#nama').val(name);
    });
</script>

<script>
    $('#unit_organisasi').select2({
        theme: "bootstrap",
        placeholder: "Unit Organisasi ..."
    });

    $('#sub_unit').select2({
        theme: "bootstrap",
        placeholder: "Sub Unit Organisasi ..."
    });

    $('#sub_sub_unit').select2({
        theme: "bootstrap",
        placeholder: "Sub Sub Unit Organisasi ..."
    });


    $('#unit_organisasi').on("change", function (e) {
        var unor_id = $(this).val();
        $('#unor_dipilih').val(unor_id);
        // alert(unor_id);

        $.get("<?=base_url('management_user/getSubByUnor/?unit_organisasi_id=');?>" + unor_id, function(data, status) {
            
            options = '<option val=""></option>';
            data.sub_unit.forEach(function(e) {
                options += '<option value="'+e.id+'" style="display: block;">'+e.name+'</option>';
            });
            
            $('#sub_unit').empty().append(options);
            $("#sub_unit").select2("destroy").prop("disabled", false)
            .select2({
                theme: "bootstrap",
                placeholder: "Sub Unit ..."
            });
            
            $('#sub_sub_unit')
            .empty()
            .append('<option val=""></option>')
            .select2("destroy")
            .select2({
                theme: "bootstrap",
                placeholder: "Pilih sub unit terlebih dahulu..."
            }).prop("disabled", true);
            
            $('#formasi_jabatan')
            .empty()
            .append('<option val=""></option>')
            .select2("destroy")
            .select2({
                theme: "bootstrap",
                placeholder: "Pilih sub unit terlebih dahulu..."
            }).prop("disabled", true);
            
        });

        // if(unor_id == 'D000000000'){
        //     // alert('dinas dikbud')
        //     $('#dSub_unit').show()
        //     $('#dSubSub_unit').show()
        //     $("#sub_unit").prop('required',true)
        //     $("#sub_sub_unit").prop('required',true)

        //     $.get("<?=base_url('management_user/getSubByUnor/?unit_organisasi_id=');?>" + unor_id, function(data, status) {
            
        //         options = '<option val=""></option>';
        //         data.sub_unit.forEach(function(e) {
        //             options += '<option value="'+e.id+'" style="display: block;">'+e.name+'</option>';
        //         });
                
        //         $('#sub_unit').empty().append(options);
        //         $("#sub_unit").select2("destroy").prop("disabled", false)
        //         .select2({
        //             theme: "bootstrap",
        //             placeholder: "Sub Unit ..."
        //         });
                
        //         $('#sub_sub_unit')
        //         .empty()
        //         .append('<option val=""></option>')
        //         .select2("destroy")
        //         .select2({
        //             theme: "bootstrap",
        //             placeholder: "Pilih sub unit terlebih dahulu..."
        //         }).prop("disabled", true);
                
        //         $('#formasi_jabatan')
        //         .empty()
        //         .append('<option val=""></option>')
        //         .select2("destroy")
        //         .select2({
        //             theme: "bootstrap",
        //             placeholder: "Pilih sub unit terlebih dahulu..."
        //         }).prop("disabled", true);
                
        //     });

        // } else {
        //     // alert('Laine')
        //     $('#dSub_unit').hide()
        //     $('#dSubSub_unit').hide()
        //     $("#sub_unit").prop('required',false);
        //     $("#sub_sub_unit").prop('required',false);
        // }
        
    });

    $('#sub_unit').on("change", function (e) {
        var sub_unit_id = $(this).val();
        // alert(unor_id);
        $.get("<?=base_url('management_user/getSubByUnor/?unit_organisasi_id=');?>" + sub_unit_id, function(data, status) {
            
            options = '<option val=""></option>';
            data.sub_unit.forEach(function(e) {
                options += '<option value="'+e.id+'" style="display: block;">'+e.name+'</option>';
            });
            
            $('#sub_sub_unit').empty().append(options);
            $("#sub_sub_unit").select2("destroy").prop("disabled", false)
            .select2({
                theme: "bootstrap",
                placeholder: "Sub Sub Unit ..."
            });
            
            $('#formasi_jabatan')
            .empty()
            .append('<option val=""></option>')
            .select2("destroy")
            .select2({
                theme: "bootstrap",
                placeholder: "Pilih sub sub unit terlebih dahulu..."
            }).prop("disabled", true);
        });
        
    });
</script>

</body>
</html>