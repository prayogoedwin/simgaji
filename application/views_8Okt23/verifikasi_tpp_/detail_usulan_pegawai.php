<?php error_reporting(0); ?>
<!-- Page content -->
<div class="page-content">

    <!-- Main content -->
    <div class="content-wrapper">

        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">

            </div>

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                        <a href="<?= base_url('history_p3k') ?>" class="breadcrumb-item">History P3K</a>
                        <span class="breadcrumb-item active">Detail</span>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content">

            <!-- Basic table -->
            <!-- <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data / <?= $detail->nip; ?> - <?= $detail->name; ?></h5>
                <div class="header-elements">
                    <a href="<?= base_url('potongan/tambah') ?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                </div>
            </div>

            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered table-striped" cellspacing="0" width="100%" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Params</th>
                            <th>Lama</th>
                            <th>Baru</th>
                            <th>Ket</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div> -->
            <!-- /basic table -->

            <div class="row">
                <div class="card col-md-12">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?= $detail->nip; ?> - <?= $detail->name; ?></h5>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="card col-md-6">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data Existing</h5>
                    </div>

                    <div class="form-group">
                        <?php foreach ($kodes as $key => $value) : ?>
                            <?php
                            // $cb_index = '<input type="checkbox" name="kode_id[]" id="kode_id" value="'.$value->id.'">';
                            $optionEl = '';
                            $labelform = $value->name;
                            $feld = $value->field;
                            if ($value->input == '1') {
                                $elementForm = '<input type="' . $value->source . '" class="form-control" name="' . $value->field . '" value="' . $detail->$feld . '" autocomplete="off">';
                            } else if ($value->input == '2') {
                                $elementForm = '<textarea rows="4" cols="4" class="form-control" name="' . $value->field . '">' . $detail->$feld . '</textarea>';
                            } else {
                                $dataSource = $value->source;
                                $elementFormSelOpen = '<select class="form-control select-search" name="' . $value->field . '" id="' . $value->field . '">';
                                foreach ($$dataSource as $ki => $val) {
                                    $kon = '';
                                    if ($val->id == $detail->$feld) {
                                        $kon = "selected";
                                    }

                                    $optionEl .= '<option value="' . $val->id . '"' . $kon . '>' . $val->name . '</option>';
                                }
                                $elementFormSelClose = '</select>';

                                $elementForm = $elementFormSelOpen . $optionEl . $elementFormSelClose;
                            }
                            ?>

                            <div class="form-group">
                                <label for=""><?= $labelform; ?></label>
                                <?= $elementForm; ?>
                            </div>

                        <?php endforeach; ?>
                    </div>
                </div>

                <div class="card col-md-6">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data Usulan</h5>
                    </div>

                    <div class="form-group">
                        <form action="<?= base_url('verifikasi/detail_action') ?>" method="post" onsubmit="return confirm('Apakah anda sudah yakin data yang akan diproses sudah benar?');">
                            
                        <!-- <input type="text" name="post_by" value="<?=$detail->posted_by;?>"> -->
                        <input type="hidden" name="pegawai_id" value="<?=$detail->id;?>">
                        <input type="hidden" name="uri" value="<?=$this->uri->segment(3)?>">
                      
                        
                        
                        <?php foreach ($data_usulan as $key => $value) : ?>

                            <?php if($value->pegawai_id != ''){ ?>

                                <input type="hidden" name="id_history[]" value="<?=$value->id_history ?>">
                                <input type="hidden" name="fieild" value="<?=$value->kode_id ?>">
                                <input type="hidden" name="fieild" value="<?=$value->periode ?>">

                            <?php } ?>
                            
                       
                                <?php
                                $flag_read = 'readonly';
                                $flag_disabled = 'disabled';
                                $cb_index = '';
                                if ($value->pegawai_id != null) {
                                    $flag_read = '';
                                    $flag_disabled = '';
                                    $cb_index = '<input type="checkbox" name="kode_id[]" id="kode_id" value="' . $value->id . '">';
                                }

                                $optionEl = '';
                                $labelform = $value->name;
                                $feld = $value->field;
                                if ($value->input == '1') {
                                    $elementForm = '<input type="' . $value->source . '" class="form-control" name="' . $value->field . '" value="' . $detail->$feld . '" autocomplete="off"' . $flag_read . '>';
                                } else if ($value->input == '2') {
                                    $elementForm = '<textarea rows="4" cols="4" class="form-control" name="' . $value->field . '"' . $flag_disabled . '>' . $detail->$feld . '</textarea>';
                                } else {
                                    $dataSource = $value->source;
                                    $elementFormSelOpen = '<select class="form-control select-search" name="' . $value->field . '" id="' . $value->field . '"' . $flag_read . '>';
                                    foreach ($dataSource as $ki => $val) {
                                        $kon = '';
                                        if ($val->id == $detail->$feld) {
                                            $kon = "selected";
                                        }

                                        $optionEl .= '<option value="' . $val->id . '"' . $kon . '>' . $val->name . '</option>';
                                    }
                                    $elementFormSelClose = '</select>';

                                    $elementForm = $elementFormSelOpen . $optionEl . $elementFormSelClose;
                                }
                                ?>

                                <div class="form-group">
                                    <label for=""><?= $labelform; ?></label>
                                    <?= $cb_index; ?> <?= $elementForm; ?>
                                </div>

                            <?php endforeach; ?>
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
        <!-- /content area -->




        <?php $this->load->view('template/footer'); ?>

        <script type="text/javascript" src="<?= base_url('assets/plugin/datatables/') ?>datatables.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function() {
        var tabel = $('#dataTable').DataTable({

            'destroy': true,
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            'data': <?= json_encode($datatable); ?>,
            'columns': [{
                    data: null,
                    sortable: false,
                    searceable: false
                },
                {
                    data: 'params'
                },
                {
                    data: 'lama'
                },
                {
                    data: 'baru'
                },
                {
                    data: 'ket'
                },
                {
                    data: 'status',
                    sortable: false,
                    searceable: false
                }
            ]
        });

        tabel.on('order.dt search.dt', function() {
            tabel.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

    });
</script>

</body>

</html>