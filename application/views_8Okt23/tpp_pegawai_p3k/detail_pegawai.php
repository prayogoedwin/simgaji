<?php
$A_01 = $this->session->userdata('A_01');
$A_02 = $this->session->userdata('A_02');
$A_03 = $this->session->userdata('A_03');
$A_04 = $this->session->userdata('A_04');
$A_05 = $this->session->userdata('A_05');

$jenis = $this->input->get('jns', TRUE);
$firstA_04 = substr($A_04, 0, 1);
$role = $this->session->userdata('role');
?>

<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <a href="<?=base_url('pegawai_p3k')?>" class="breadcrumb-item">TPP Pegawai P3K</a>
                    <span class="breadcrumb-item active">Detail  (<?=$detail->name;?> - <?=$detail->nip;?>)</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
      <?php if($warning['pesan'] != '') :?>
        <div class="alert bg-warning text-white alert-styled-left alert-dismissible">
  				<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
  				<span class="font-weight-semibold">Perhatian!</span> <?=$warning['pesan']?>
		    </div>
      <?php endif; ?>

        <!-- Form layouts -->
        <div class="row">

            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Jenis Usulan</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <!-- <a class="list-icons-item" data-action="collapse"></a> -->
                                    </div>
                                </div>
                            </div>

                            <form action="" method="get">
                                <div class="card-body">
                                    <div class="form-group">
                                        <select name="jns" id="jns" class="form-control" required>
                                            <option value="">Pilih</option>
                                            <option value="1" <?php if($filter['params'] == 1){echo "selected";} ?> >KGB</option>
                                            <option value="2" <?php if($filter['params'] == 2){echo "selected";} ?> >KP</option>
                                            <!-- <option value="3" <?php if($filter['params'] == 3){echo "selected";} ?> >Pensiun</option> -->
                                            <option value="4" <?php if($filter['params'] == 4){echo "selected";} ?> >Meninggal</option>
                                            <option value="5" <?php if($filter['params'] == 5){echo "selected";} ?> >Keluarga</option>
                                            <option value="6" <?php if($filter['params'] == 6){echo "selected";} ?> >Lainnya</option>
                                            <option value="7" <?php if($filter['params'] == 7){echo "selected";} ?> >TB (Tugas Belajar)</option>
                                            <option value="8" <?php if($filter['params'] == 8){echo "selected";} ?> >Perkawinan</option>
                                        </select>
                                    </div>

                                      <?php if(isset($_GET['jns']) == '4') :?>
                                          <div class="form-group">
                                            <span style="color: red;">Jika TMT meninggal <= 4 bulan, maka status pegawai diganti gaji terusan. Jika TMT meninggal > 4 bulan maka status pegawai meninggal (gaji dihentikan)</span>
                                          </div>
                                      <?php endif; ?>

                                    <div class="float-right" style="margin-bottom: 8px;">
                                        <?php if($warning['pesan'] != '') :?>
                                          <button type="submit" class="btn btn-sm btn-info" disabled>Cari</button>
                                        <?php else :?>
                                          <button type="submit" class="btn btn-sm btn-info">Cari</button>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                    <?php if(isset($_GET['jns'])) :?>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Jabatan SIMPEG</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <!-- <a class="list-icons-item" data-action="collapse"></a> -->
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <table class="table-striped table-bordered" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Jabatan</th>
                                            <th>Kelas Jabatan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?=$jabatan_pegawai->nama_jabatan?></td>
                                            <td><?=$jabatan_pegawai->kelas_jabatan?></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                    <?php endif; ?>

                    <?php if($jenis == '1' || $jenis == '2') :?>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">e - Kasus</h5>
                                <div class="header-elements">
                                </div>
                            </div>

                            <div class="card-body">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <td>Kode</td>
                                        <td>Keterangan</td>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?=$ekasus['kode_hot']?></td>
                                            <td><?=$ekasus['psn']?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php endif ;?>

                </div>


                <div class="row">
                    <!-- Vertical form -->
                    <?php if(isset($_GET['jns'])) :?>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Data</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <form action="<?=base_url('tpp_pegawai_p3k/detail_action')?>" method="post" onsubmit="return confirm('Apakah anda sudah yakin data yang akan diproses sudah benar?');">
                                    <input type="hidden" name="pegawai_id" value="<?=$detail->id;?>">

                                    <?php foreach ($kodes as $key => $value) :?>
                                        <?php
                                        $cb_index = '<input type="checkbox" name="kode_id[]" id="kode_id" value="'.$value->id.'">';
                                        $optionEl = '';
                                        $labelform = $value->name;
                                        $feld = $value->field;
                                            if($value->input == '1'){
                                                $elementForm = '<input type="'. $value->source .'" class="form-control" name="'. $value->field .'" value="' . $detail->$feld .'" autocomplete="off" id="'. $value->field .'">';
                                            } else if($value->input == '2'){
                                                $elementForm = '<textarea rows="4" cols="4" class="form-control" name="'. $value->field .'">'. $detail->$feld .'</textarea>';
                                            } else {
                                                $dataSource = $value->source;
                                                $elementFormSelOpen = '<select class="form-control select-search" name="'. $value->field .'" id="'. $value->field .'">';
                                                                foreach ($$dataSource as $ki => $val) {
                                                                    $kon = '';
                                                                    if($val->id == $detail->$feld){
                                                                        $kon = "selected";
                                                                    }

                                                                    $optionEl .= '<option value="'. $val->id .'"'. $kon .'>' . $val->name . '</option>';
                                                                }
                                                $elementFormSelClose = '</select>';

                                                $elementForm = $elementFormSelOpen . $optionEl . $elementFormSelClose;
                                            }
                                        ?>

                                        <div class="form-group">
                                            <label for=""><?=$labelform;?></label>
                                            <?=$cb_index?> <?=$elementForm;?>
                                        </div>

                                    <?php endforeach; ?>

                                    <?php if($A_01 == 'D0' && $A_02 != '00' && ($firstA_04 > '3' || $A_04 == '9F' || $A_04 == '9G')) :?>
                                    <!-- for sekolahan -->
                                    <div class="form-group">
                                        <label>NIP / Nama TU Cabang Dinas</label>
                                        <select class="form-control select2" name="nips" id="nips" style="width:100%!important;" required oninvalid="this.setCustomValidity('Silahkan input NIP')" oninput="this.setCustomValidity('')"></select>
                                        <strong><span style="color:red">Wajib Diisi NIP yang tampil dibawah</span></strong>

                                        <input type="hidden" name="nipe" id="nipe">
                                        <input type="hidden" name="nama" id="nama">
                                        <input type="hidden" name="jabatan" id="jabatan">
                                    </div>
                                    <?php endif; ?>

                                    <!-- START INPUTAN JIKA HUKDIS -->
                                    <?php if($ekasus['kode_hot'] > 0) :?>
                                    <div class="form-group">
                                        <label>Pemotongan Gaji</label>
                                        <input type="text" class="form-control" name="potongan_gaji_hukdis" id="potongan_gaji_hukdis" required>
                                    </div>
                                    <?php endif ;?>
                                    <!-- END INPUTAN JIKA HUKDIS -->

                                    <div class="text-right">
                                        <button type="submit" class="btn btn-info">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                        <?php if($jenis == '1' || $jenis == '2') :?>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Rekomendasi Gaji Pegawai</h5>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item" data-action="collapse"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <input type="hidden" name="rekom_gaji" id="rekom_gaji" value="<?=$rekomen_gaji['baru']?>">
                                    <table class="table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Gaji Pokok</th>
                                                <th>Masa  Kerja</th>
                                                <th>Golongan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Rp. <?=number_format($rekomen_gaji['baru'])?></td>
                                                <td><?=$rekomen_gaji['masa']?> Tahun</td>
                                                <td><?=$gol_ybs;?> - <?=$pangkat_ybs;?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                        <!-- KELUARGA START -->
                        <?php if($jenis == '5'):?>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Data Keluarga Pegawai</h5>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item" data-action="collapse"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Status</th>
                                                <th>Tertunjang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($family as $ki => $vamily) :?>
                                            <?php
                                            $sts = '';
                                            $act = '';
                                            if($vamily->KF_02 == '1'){
                                                $sts = 'Suami / Istri';
                                                // $act = '-';
                                            } else {
                                                $sts = 'Anak';
                                                // $act = '<input type="checkbox" name="" id="">';
                                            }

                                            $sts_tertanggung = 'belum diisi';
                                            if($vamily->KF_07 == 'T'){
                                                $sts_tertanggung = 'Tidak dapat';
                                            } else {
                                                $sts_tertanggung = 'Dapat';
                                            }
                                            ?>
                                            <tr>
                                                <td><?=$ki+1;?></td>
                                                <td><?=$vamily->KF_04;?></td>
                                                <td align="center"><?=$sts?></td>
                                                <td align="center"><?=$sts_tertanggung?></td>
                                            </tr>
                                            <?php endforeach ;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php endif ;?>
                        <!-- KELUARGA END -->

                        <!-- START EFILE -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">e - File</h5>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item" data-action="collapse"></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <table class="table-striped table-bordered" width="100%">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Status</th>
                                                <th>Verifikasi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          <?php if($efilenya != null) :?>

                                            <?php foreach ($efilenya as $kifi => $valefile) :?>
                                            <?php
                                                $stsfile = 'not found';
                                                if($valefile['efile'] != null){
                                                    $stsfile = '<a href="'. $valefile['efile'] .'" target="_blank">Lihat</a>';
                                                }
                                            ?>
                                            <tr>
                                                <td><?=$kifi+1;?></td>
                                                <td><?=$valefile['nama_jenis'];?></td>
                                                <td><?=$stsfile?></td>
                                                <?php if($valefile['status_verif'] != null) :?>
                                                    <td><i class="icon-stack-check mr-1"></i> <?=$valefile['status_verif']?></td>
                                                <?php else :?>
                                                    <td><i class="icon-stack-cancel mr-1"></i> Belum Terverifikasi</td>
                                                <?php endif ;?>
                                            </tr>
                                            <?php endforeach ;?>

                                          <?php else: ?>
                                            <tr>
                                              <td colspan="4" style="text-align: center;">Data tidak ditemukan</td>
                                            </tr>
                                          <?php endif; ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- END EFILE -->

                        <!-- START MSG SEKOLAH -->
                        <?php if($role == 4) :?>
                        <div class="col-md-6">
                          <div class="alert bg-info text-white alert-dismissible">
          									<button type="button" class="close" data-dismiss="alert"><span>×</span></button>
          									<span class="font-weight-semibold"><?=$info_sekolah['NIP']?> -- </span><?=$info_sekolah['message']?></a>.
        							    </div>
                        </div>
                        <?php endif; ?>
                        <!-- END MSG SEKOLAH -->

                    <?php endif ;?>
                    <!-- /vertical form -->
                </div>

            </div>
        </div>
        <!-- /form layouts -->

    </div>
    <!-- /content area -->




<?php $this->load->view('template/footer_custom');?>


    <script src="<?=base_url('assets/')?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/demo_pages/form_select2.js"></script>
</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    var fixurl = '<?=base_url('management_user/cari_pns/');?>';
    $('#nips').select2({
        ajax: {
            url: fixurl,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    nipnya: params.term
                }

                // Query parameters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: "Masukkan NIP atau Nama PNS",
        minimumInputLength: 5,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });

    function formatRepo (repo) {
        if (repo.loading) {
            return repo.text;
        }

        var markup = "<div>" + repo.name + "</div><div>" + repo.id + "</div><div>" + repo.lokasi + "</div><div>" + repo.unit_kerja + "</div>";

        return markup;
    }

    function formatRepoSelection (repo) {
        return repo.id;
    }

    $('#nips').change(function(){
        var nip = $('#nips').select2('data')[0].id;
        var name = $('#nips').select2('data')[0].name;
        var jab = $('#nips').select2('data')[0].lokasi;

        $('#nipe').val(nip);
        $('#nama').val(name);
        $('#jabatan').val(jab);
    });
</script>

<script>

    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        // timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });

    $("#gaji_pokok").keyup(function(){
        // console.log(this.value)

        if(this.value.length > 6){
            var rekomGaji = $('#rekom_gaji').val()

            if(this.value != rekomGaji){
                // alert('gaji tidak sesuai dengan rekomendasi')
                new Noty({
                    text: 'gaji tidak sesuai dengan rekomendasi',
                    type: 'info'
                }).show();
            }
        }


    });
</script>

</body>
</html>
