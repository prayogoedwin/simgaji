<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item">Usia</span>
                    <span class="breadcrumb-item active">Fungsional</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data</h5>
                <div class="header-elements">
                    <a href="javascript:void(0)" onclick="tambahData()" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Kode</th>
                            <th>Name</th>
                            <th>Usia</th>
                            <th>Rumpun</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

        <!-- START MODAL DETAIL -->
        <div id="mdlDetail" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="idne" id="idne">
                            <!-- <hr> -->
                            <div class="form-group row col-md-12">
                                <label>Kode</label>
                                <input type="text" class="form-control" name="kode" id="kode" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Name</label>
                                <input type="text" class="form-control" name="name" id="name" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Usia</label>
                                <input type="number" class="form-control" name="usia" id="usia" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Rumpun</label>
                                <input type="number" class="form-control" name="rumpun" id="rumpun" required autocomplete="off">
                            </div>
                        </div>
                    

                        <div class="modal-footer">
                            <button id="btn_update" type="submit" class="btn btn-md btn-success">Update</button>
                            <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->

        <!-- START MODAL TAMBAH -->
        <div id="mdlTambah" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h6 class="modal-title">Tambah Data</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div class="modal-body">
                        <!-- <hr> -->
                        <div class="form-group row col-md-12">
                            <label>Kode</label>
                            <input type="text" class="form-control" name="kodeadd" id="kodeadd" required autocomplete="off">
                        </div>
                        <div class="form-group row col-md-12">
                            <label>Name</label>
                            <input type="text" class="form-control" name="nameadd" id="nameadd" required autocomplete="off">
                        </div>
                        <div class="form-group row col-md-12">
                            <label>Usia</label>
                            <input type="number" class="form-control" name="usiaadd" id="usiaadd" required autocomplete="off">
                        </div>
                        <div class="form-group row col-md-12">
                            <label>Rumpun</label>
                            <input type="number" class="form-control" name="rumpun" id="rumpunadd" required autocomplete="off">
                        </div>
                    </div>
                

                    <div class="modal-footer">
                        <button id="btn_tambah" type="submit" class="btn btn-md btn-primary">Simpan</button>
                        <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- END MODAL TAMBAH -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        $.extend( $.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
            }
        });
        
        var tabel = $('#tabledt').DataTable({

            'lengthMenu' : [
                        [ 10, 25, 50, 100,  -1 ],
                        [ '10', '25', '50', '100', 'Semua' ]
                ],   
            buttons: {            
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                ]
            },

            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            "serverSide"  : true,
            "ajax": {
                "url": "<?=base_url('usia/getFungsional') ?>",
                "type": "POST"
            },
            'columns': [
                { data: 'num', sortable : false, searceable : false },
                { data: 'kode' },
                { data: 'name' },
                { data: 'usia' },
                { data: 'rumpun' },
                // { data: 'aksi', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });
</script>

<script>
    function tambahData(){
        $('#mdlTambah').modal('show');
    }

    $('#btn_tambah').on('click',function(){
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var kode = $('#kodeadd').val();
        var name = $('#nameadd').val();
        var usia = $('#usiaadd').val();
        var rumpun = $('#rumpunadd').val();

        if(kode == '' || name == '' || usia == '' || rumpun == ''){
            swalInit.fire({
                title: 'Perhatian',
                text: 'Pastikan semua kolom terisi!',
                type: 'info'
            });
        } else {
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('usia/tambah_fungsional_action')?>",
                data : {
                    kode : kode,
                    name : name,
                    usia : usia,
                    rumpun : rumpun
                },
                dataType : "JSON",
                success: function(data){

                    var sts = data['status'];
                    var msg = data['message'];

                    if(sts == 1){
                        $('[name="kodeadd"]').val("");
                        $('[name="nameadd"]').val("");
                        $('[name="usiaadd"]').val("");
                        $('[name="rumpunadd"]').val("");
                        $('#mdlTambah').modal('hide');

                        $('#tabledt').DataTable().ajax.reload(null, false);
                        swalInit.fire({
                            title: 'Yeay',
                            text: msg,
                            type: 'success'
                        });
                    } else {
                        swalInit.fire({
                            title: 'Ops',
                            text: msg,
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }

                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#tabledt').DataTable().ajax.reload(null, false);
                                location.reload();
                            }
                        });
                }
            });
            // return false;   
        }
        
    });
</script>

<script>
    $('#btn_update').on('click',function(){
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var id = $('#idne').val();
        var kode = $('#kode').val();
        var name = $('#name').val();
        var usia = $('#usia').val();
        var rumpun = $('#rumpun').val();

        $.ajax({
            type : "POST",
            url  : "<?php echo base_url('usia/update_fungsional_action')?>",
            data : {
                id : id,
                kode : kode,
                name : name,
                usia : usia,
                rumpun : rumpun
            },
            dataType : "JSON",
            success: function(data){

                var sts = data['status'];
                var msg = data['message'];

                if(sts == 1){
                    $('[name="idne"]').val("");
                    $('[name="kode"]').val("");
                    $('[name="name"]').val("");
                    $('[name="usia"]').val("");
                    $('[name="rumpun"]').val("");
                    $('#mdlDetail').modal('hide');

                    $('#tabledt').DataTable().ajax.reload(null, false);
                    swalInit.fire({
                        title: 'Yeay',
                        text: msg,
                        type: 'success'
                    });
                } else {
                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            // alert('Notification is closed.');
                            location.reload();
                        }
                    });
                }

                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swalInit.fire({
                        title: 'Oppss',
                        text: 'Terjadi kesalahan nih',
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
            }
        });
        return false;
    });
</script>

<script>
    function hapusData(){
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var id = $('#idne').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        
        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax');
                
                $.ajax({
                    url: "<?=base_url('usia/hapus_fungsional_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            $('[name="idne"]').val("");
                            $('[name="kode"]').val("");
                            $('[name="name"]').val("");
                            $('[name="usia"]').val("");
                            $('[name="rumpun"]').val("");
                            $('#mdlDetail').modal('hide');

                            $('#tabledt').DataTable().ajax.reload(null, false);
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success'
                            });
                            // swalInit.fire({
                            //     title: 'Yeay',
                            //     text: msg,
                            //     type: 'success',
                            //     onClose: function() {
                            //         // alert('Notification is closed.');
                            //         location.reload();
                            //     }
                            // });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
    function detailData(id){
        // $('#mdlDetail').modal('show');
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        $.ajax({
            url: "<?=base_url('usia/getDetailFungsional')?>",
            type: "POST",
            data: {
                id : id
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                
                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];
                
                if(sts == 1){
                    $('#mdlDetail').modal('show');

                    $('#idne').val(id)
                    $('#kode').val(dt['kode'])
                    $('#name').val(dt['name'])
                    $('#usia').val(dt['usia'])

                    $('#rumpun').val(dt['rumpun'])

                } else {
                    
                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            // alert('Notification is closed.');
                            location.reload();
                        }
                    });
                }
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        // alert('Notification is closed.');
                        location.reload();
                    }
                });
            }
        });
    }
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>