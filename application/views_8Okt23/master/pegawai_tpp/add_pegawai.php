<?php
$A_01 = $this->session->userdata('A_01');
$A_02 = $this->session->userdata('A_02');
$A_03 = $this->session->userdata('A_03');
$A_04 = $this->session->userdata('A_04');
$A_05 = $this->session->userdata('A_05');

$jenis = $this->input->get('jns', TRUE);
$firstA_04 = substr($A_04, 0, 1);
$role = $this->session->userdata('role');
?>

<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <a href="<?=base_url('tpp_pegawai')?>" class="breadcrumb-item">TPP Pegawai</a>
                    <span class="breadcrumb-item active">Tambah</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">
       

        <!-- Form layouts -->
        <div class="row">
   

           
            <div class="col-md-6 ">
           
            <form action="<?=base_url('tpp_masterpegawai/update_action')?>" method="post" onsubmit="return confirm('Apakah anda sudah yakin data yang akan diupdate sudah benar?');">
            <input type="hidden" name="pegawai_id" value="">   
            <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Tambah Pegawai TPP</h5>
                        <!-- <p style="color:red">centang yang akan dirubah</p> -->
                        <!-- <div class="header-elements">
                            <div class="list-icons">
                                <a class="list-icons-item" data-action="collapse"></a>
                            </div>
                        </div> -->
                    </div>
                    
                    <div class="card-body">

                    <div class="form-group row">
                            <label class="col-form-label col-lg-2">NIP</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id" value="3">
                            <div class="col-lg-8">
                                
                                <input type="text" name="nip" class="form-control" value="" >
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Nama</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id" value="4">
                            <div class="col-lg-8">
                                
                                <input type="text"  name="name" class="form-control" value="" >
                            </div>
                        </div>
                        
                        
                    <div class="form-group row">
                            <label class="col-form-label col-lg-2">Lokasi Induk SIMPEG</label>
                            <input type="checkbox"  onclick="return false;" >
                            <div class="col-lg-8">
                                
                                <input type="text" class="form-control" value="" readonly>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Lokasi Induk </label>
                            <input type="checkbox" name="kode_id[]" id="kode_id" value="2">
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('lokasi_kerja', opd_induk_array_tpp('Lokasi Kerja', 'id'), '', ' id="lokasi_kerja" class="form-control custom-select select2"   required');
                            ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Lokasi Gaji</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id" value="24">
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('lokasi_gaji', unit_array_tpp('Lokasi Gaji', 'id'), '', ' id="lokasi_gaji" class="form-control custom-select select2"   required');
                            ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Tmt Mutasi</label>
                            <input type="checkbox"  onclick="return false;" >
                            <div class="col-lg-8">
                            <input type="text" value="" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Jabatan</label>
                            <input type="hidden" name="jab_now" id="jab_now" value="">
                            <input type="hidden" name="jab_now_name" id="jab_now_name" value="">
                            <input type="checkbox"  id="kode_id105asli" value="" disabled>
                            <div class="col-lg-8">
                                <select class="form-control custom-select select2"  id="i_05">
                                <option value="">Pilih Jabatan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Jenis Jabatan</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id104" value="104" onclick="return false;">
                            <div class="col-lg-8">
                            <input type="text" name="I_5A" id="i_5a" value="" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Kode Jabatan</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id105" value="105" onclick="return false;">
                            <div class="col-lg-8">
                            <input type="text" name="I_05" id="i_05_string" value="" readonly>
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Kode Jenjang</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id106" value="106" onclick="return false;">
                            <div class="col-lg-8">
                            <input type="text" name="I_07" id="i_07" value="" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Kelas Jabatan</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id107" value="107" onclick="return false;">
                            <div class="col-lg-8">
                            <input type="text" name="kelasjab" id="kelasjab" value="" readonly>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Eselon</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id32" value="32"  onclick="return false;">
                            <div class="col-lg-8">
                            <input type="hidden" name="eselon_id" id="eselon_id" value="" readonly>
                            <input type="text" id="eselon_text" value="" readonly>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Pkt/Gol</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id14" value="14" >
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('golongan_id', golongan_id_array('Gologan'), '', ' id="golongan_id" class="form-control custom-select select2"   ');
                            ?>
                            </div>
                        </div>

                       
                        <div class="form-group row" hidden>
                            <label class="col-form-label col-lg-2">Eselon</label>
                            <!-- <input type="checkbox" name="kode_id[]" id="kode_id32" value="32" > -->
                            <div class="col-lg-8">
                            <?php 
                            // echo form_dropdown('eselon_id', eselon_id_array('Eselon'), $detail->eselon_id, ' id="eselon_id" class="form-control custom-select select2"   ');
                            ?>
                            </div>
                        </div>

                       

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">PLT</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id109" value="109" >
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('is_plt', ya_tidak_array('PLT'), '', ' id="is_plt" class="form-control custom-select select2"   ');
                            ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">LOKASI INDUK PLT </label>
                            <input type="checkbox" name="kode_id[]" id="kode_id110" value="110" >
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('lokasi_kerja_plt', opd_induk_array_tpp('Lokasi Kerja PLT', 'id'), '', ' id="lokasi_kerja_plt" class="form-control custom-select select2"   ');
                            ?>
                            <small style="color:red">HANYA PLT JPT PRATAMA / MADYA</small>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">TB</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id111" value="111" > 
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('tb', ya_tidak_array('Pilih Status TB'), '', ' id="tb" class="form-control custom-select select2"');
                            ?>
                            <!-- <input type="text" class="form-control" value="" disabled> -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">Penyetaraan</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id115" value="115"> 
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('penyetaraan', ya_tidak_array('Pilih Status Penyataraan'), '', ' id="penyetaraan" class="form-control custom-select select2"');
                            ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-form-label col-lg-2">TPP Beban Kerja Stop</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id47" value="47" > 
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('tpp_stop', ya_tidak_array('Pilih Status TPP Beban Kerja'), '', ' id="tpp_stop" class="form-control custom-select select2"');
                            ?>
                            </div>
                        </div>

                       
                        <div class="form-group row bkk" >
                            <label class="col-form-label col-lg-2">TPP Beban Kerja Khusus Stop</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id112" value="112"> 
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('beban_kerja_khusus_stop', ya_tidak_array('Pilih Status TPP Beban Kerja Khusus'), '', ' id="beban_kerja_khusus_stop" class="form-control custom-select select2"');
                            ?>
                            </div>
                        </div>
                       

                        
                       

                        <div class="form-group row tk">
                            <label class="col-form-label col-lg-2">TPP Tempat Stop</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id113" value="113"> 
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('tempat_stop', ya_tidak_array('Pilih Status TPP Tempat'), '', ' id="tempat_stop" class="form-control custom-select select2"');
                            ?>
                            </div>
                        </div>

                       

                      

                        <div class="form-group row kk" >
                            <label class="col-form-label col-lg-2">TPP Kondisi Stop</label>
                            <input type="checkbox" name="kode_id[]" id="kode_id114" value="114"> 
                            <div class="col-lg-8">
                            <?php 
                            echo form_dropdown('kondisi_stop', ya_tidak_array('Pilih Status TPP Kondisi'), '', ' id="kondisi_stop" class="form-control custom-select select2"');
                            ?>
                            </div>
                        </div>


                        <div class="text-right" >
                            <button type="submit" class="btn btn-info">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                        </div>
                        
                    </div>
                    
                </div>
            </form>
            </div>
        </div>
        <!-- /form layouts -->

    </div>
    <!-- /content area -->




<?php $this->load->view('template/footer_custom');?>
<script src="<?=base_url('assets/')?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
<script src="<?=base_url('assets/')?>global_assets/js/demo_pages/form_select2.js"></script>
</div>
<!-- /main content -->
</div>
<!-- /page content -->



<script>

    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        // timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

<script type="text/javascript">

    $(window).on('load', function() {
        var lokasi_kerja = $('#a_01').val();
        var lokasi = $('#lokasi').val();
        // alert(lokasi_kerja)
        var jab_now = $('#jab_now').val();
        var jab_now_name = $('#jab_now_name').val();
        // alert($(this).val());
        $.ajax({
            url: "<?php echo site_url('tpp_pegawai/get_jabatan/'); ?>"+"/"+jab_now+"/"+jab_now_name,
            method: "POST",
            data: {
                lokasi_kerja: lokasi_kerja,
            '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
            },
            async: true,
            dataType: 'json',
            success: function(data) {

            //   var html = '';
            //   var i;
            //   for (i = 0; i < data.length; i++) {
            //     html += '<option value=' + data[i].code + '>' + data[i].name + '</option>';
            //   }
            $("#i_05").html(data.list_jabatan).show();

            }
        });
        return false;

    });

    $(window).on('load', function() {
        var lokasi_gaji = $('#lokasi_gaji').val();
        // alert($(this).val());
        $.ajax({
            url: "<?php echo site_url('tpp_pegawai/cek_hidden_show_filed/'); ?>",
            method: "POST",
            data: {
                lokasi_gaji: lokasi_gaji,
            '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                if(data.bkk == 'hidden'){
                    $(".bkk").attr("hidden",true);
                }else{
                    $(".bkk").attr("hidden",false);
                }

                if(data.tk == 'hidden'){
                    $(".tk").attr("hidden",true);
                }else{
                    $(".tk").attr("hidden",false);
                }

                if(data.kk == 'hidden'){
                    $(".kk").attr("hidden",true);
                }else{
                    $(".kk").attr("hidden",false);
                }
                

            }
        });
        return false;

    });
    
  $(document).ready(function() {
    $('#lokasi_kerja').change(function() {
      var lokasi_kerja = $(this).val();
      // alert($(this).val());
      $.ajax({
        url: "<?php echo site_url('tpp_pegawai/get_jabatan'); ?>",
        method: "POST",
        data: {
            lokasi_kerja: lokasi_kerja,
         '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
        },
        async: true,
        dataType: 'json',
        success: function(data) {

        //   var html = '';
        //   var i;
        //   for (i = 0; i < data.length; i++) {
        //     html += '<option value=' + data[i].code + '>' + data[i].name + '</option>';
        //   }
          $("#i_05").html(data.list_jabatan).show();

        }
      });
      return false;
    });


    $('#lokasi_gaji').change(function() {
      var lokasi_gaji = $(this).val();

        $.ajax({
            url: "<?php echo site_url('tpp_pegawai/cek_hidden_show_filed/'); ?>",
            method: "POST",
            data: {
                lokasi_gaji: lokasi_gaji,
            '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
            },
            async: true,
            dataType: 'json',
            success: function(data) {

                if(data.bkk == 'hidden'){
                    $(".bkk").attr("hidden",true);
                }else{
                    $(".bkk").attr("hidden",false);
                }

                if(data.tk == 'hidden'){
                    $(".tk").attr("hidden",true);
                }else{
                    $(".tk").attr("hidden",false);
                }

                if(data.kk == 'hidden'){
                    $(".kk").attr("hidden",true);
                }else{
                    $(".kk").attr("hidden",false);
                }
                

            }
        });
      return false;
    });

    $('#i_05').change(function() {
      var i_05 = $(this).val();
      // alert($(this).val());
      $.ajax({
        url: "<?php echo site_url('tpp_pegawai/get_petafngsapk'); ?>",
        method: "POST",
        data: {
            i_05: i_05,
         '<?php echo $this->security->get_csrf_token_name(); ?>':'<?php echo $this->security->get_csrf_hash(); ?>',
        },
        async: true,
        dataType: 'json',
        success: function(data) {

            var leng = i_05.length;
            if(leng == 10){
                $("#i_5a").val(1);
                $("#kode_id32").prop("checked", true);
                    $("#eselon_id").val(data.eselun);
                    $("#eselon_text").val(data.esel_str);
               
            }else if(leng == 5){
                $("#i_5a").val(2);
                $("#kode_id32").prop("checked", false);
                    $("#eselon_id").val('');
                    $("#eselon_text").val('');
            }else{
                $("#i_5a").val(0);
                $("#kode_id32").prop("checked", false);
                    $("#eselon_id").val('');
                    $("#eselon_text").val('');
            }

            if(data.i_05 != ''){

                $("#kode_id105asli").prop("checked", false);
                $("#kode_id104").prop("checked", true);
                $("#kode_id105").prop("checked", true);
                $("#kode_id106").prop("checked", true);
                $("#kode_id107").prop("checked", true);
                $("#i_05_string").val(data.i_05);
                $("#i_07").val(data.i_07);
                $("#kelasjab").val(data.kelasjab);
                // $("#i_5a").val(2);

            }else{

               
                $("#kode_id104").prop("checked", true);
                $("#kode_id105asli").prop("checked", false);
                $("#kode_id105").prop("checked", true);
                $("#kode_id106").prop("checked", false);
                $("#kode_id107").prop("checked", true);
                $("#i_05_string").val(i_05);
                $("#i_07").val(data.i_07);
                $("#kelasjab").val(data.kelasjab);

                
            }
           

        }
      });
      return false;
    });

  });
</script>

<script>
    var fixurl = '<?=base_url('management_user/cari_pns/');?>';
    $('#nips').select2({
        ajax: {
            url: fixurl,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                var query = {
                    nipnya: params.term
                }

                // Query parameters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        },
        placeholder: "Masukkan NIP atau Nama PNS",
        minimumInputLength: 5,
        escapeMarkup: function (markup) { return markup; },
        templateResult: formatRepo,
        templateSelection: formatRepoSelection
    });

    function formatRepo (repo) {
        if (repo.loading) {
            return repo.text;
        }

        var markup = "<div>" + repo.name + "</div><div>" + repo.id + "</div><div>" + repo.lokasi + "</div><div>" + repo.unit_kerja + "</div>";

        return markup;
    }

    function formatRepoSelection (repo) {
        return repo.id;
    }

    $('#nips').change(function(){
        var nip = $('#nips').select2('data')[0].id;
        var name = $('#nips').select2('data')[0].name;
        var jab = $('#nips').select2('data')[0].lokasi;

        $('#nipe').val(nip);
        $('#nama').val(name);
        $('#jabatan').val(jab);
    });
</script>

<script>
  $(document).ready(function() {
    $('#lokasi_kerja').select2();
    $('#lokasi_gaji').select2();
    $('#lokasi_kerja_plt').select2();
    $('#i_05').select2();
    
    
  });
</script>

</body>
</html>
