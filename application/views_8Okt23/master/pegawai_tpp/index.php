<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">
    

    <!-- Page header -->
    <div class="page-header page-header-light">
        <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                    <?=$breadcrumb?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->



    



    <!-- Content area -->
    <div class="content">
   

     <!-- Form inputs -->
     
     <div class=" ">


<div class="">
    <!-- <form action="#"> -->
    <form id="form-filter" class="form-horizontal">


    

    <div class="form-group row">
       

    <?php 
    $nip =  $this->session->userdata('B_02B');
    $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$nip' AND deleted_at IS NULL");
    if($x->num_rows() > 0){
        $x = $x->row();
        $role = $x->type_role;
        $skpd = $x->kode_skpd;
        // echo $skpd;
    }else{
        $role = '';
        $skpd = '';
        
    }

    ?>
    
        <div class="col-lg-3">
            <?php 
            echo form_dropdown('lokasi_kerja', opd_induk_array_tpp('Lokasi Induk', 'id', $role, $skpd), '', ' id="lokasi_kerja" class="form-control custom-select select2"   required');
            ?>
        </div>

        <div class="col-lg-3" >
        <?php 
            echo form_dropdown('lokasi_gaji', opd_array_fix_array('Lokasi Gaji', 'id', $role, $skpd), '', ' id="lokasi_gaji" class="form-control custom-select select2"   required');
            ?>

        </div>

        <div class="col-lg-3">
        <div class="text-left">
        <button type="button" id="btn-filter" class="btn btn-primary pull-left"> <i class="fa fa-save"></i> Cari </button>
        </div>

        </div>

    </div>

    </form>
  
</div>

</div>
<!-- /form inputs -->
        
        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><?=$title?></h5>
                <div class="header-elements">
                    <a href="<?=base_url('tpp_masterpegawai/add')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                    <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#mdlAdd" >Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                    <!-- <table class="table">
                        <tr>
                            <th><i  class="icon-cog"></i> = Belum diproses</th>
                            <th><i style="color:red" class="icon-cancel-circle2"></i> = Ditolak</th>
                            <th><i style="color:green" class="icon-checkmark-circle"></i> = Disetujui</th>
                        </tr>
                    </table> -->
                    
                </div>
            </div>

           

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead >
                        <tr>
                            
                            <th >NIP</th>
                            <th >Nama</th>
                            <th >Lokasi Kerja</th>
                            <th >Lokasi Gaji</th>
                            <th >BK</th>
                            <th >BKK</th>
                            <th >TK</th>
                            <th >KK</th>
                            

                        </tr>

                       
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <form id="form-filter2" class="form-horizontal">
                    <!-- <tfoot >
                        <tr>  
                            <th ><input type="text" class="form-control" name="nip_s" id="nip_s" placeholder="NIP"></th>
                            <th ><input type="text" class="form-control" name="nama_s" id="nama_s" placeholder="Nama"></th>
                            <th ><?php echo form_dropdown('lokasi_kerja_s', opd_gaji_array('Lokasi Kerja', 'kode', '2'), '', ' id="lokasi_kerja_s" class="form-control custom-select select2"   required'); ?></th>
                            <th ><?php echo form_dropdown('lokasi_gaji_s', opd_gaji_array('Lokasi Gaji', 'kode', '2'), '', ' id="lokasi_gaji_s" class="form-control custom-select select2"   required'); ?></th>
                            
                           

                        </tr>
                    </tfoot> -->
                    </form>

                    

                   
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
   


</div>
<!-- /main content -->


<!-- START MODAL DETAIL -->
<div id="mdlDetail" class="modal fade" >
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail Verifikasi</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <div id="scs" class="alert alert-success d-none">
                    <strong>Success!</strong> Berhasil Update Data!
                    </div>

                    <div id="dgr" class="alert alert-danger d-none" >
                    <strong>Danger!</strong> Gagal Update Data!
                    </div>

                    <form id="msform" method="POST">

                    <input type="hidden" id="pegawai_id" name="pegawai_id" value="">


                    <div class="modal-body" id="your_modal_detail">  
                    </div>  
                        
                        <div class="modal-footer">
                        
                        <table class="table pull-left text-left">
                        <tr>
                            <th><i class="icon-cog"></i> = Belum diproses</th>
                            <th><i style="color:orange" class="icon-backward"></i> = Direvisi</th>
                            <th><i style="color:red" class="icon-cancel-circle2"></i> = Ditolak</th>
                            <th width="60%"><i style="color:green" class="icon-checkmark-circle"></i> = Disetujui</th>
                            <th ><button type="submit" id="btn_update" class="btn btn-md btn-success">Update</button></th>
                        </tr>
                        </table>
                        
                            <!-- <a href="javascript:void(0)" onclick="hapusData()" id="btn_del" class="btn btn-md btn-danger" style="display:none">Hapus</a> -->
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                           
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->

        


</div>
<!-- /page content -->

<?php $this->load->view('master/pegawai_tpp/js');?>

<script>
//   $('#lokasi_gaji').on('change', function() {
//     var lok = $("#lokasi_gaji").val();
    
//     alert(this.value)
//   });

</script>







      

</body>
</html>