<!-- Page content -->
<div class="page-content">
    
    <!-- Main content -->
    <div class="content-wrapper">
        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <!-- <div class="page-header-content header-elements-md-inline">
                <div class="page-title d-flex">
                    <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                
            </div> -->
            
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                            <span class="breadcrumb-item active">Pengumuman</span> -->
                            <?=$breadcrumb?>
                        </div>
                        
                        <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                    </div>
                    
                    
                </div>
            </div>
            <!-- /page header -->
            
            
            <!-- Content area -->
            <div class="content">
                
                <!-- Basic table -->
                <div class="card">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title"><?=$title?></h5>
                        <div class="header-elements">
                            <a href="javascript:void(0)" onclick="add()" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                            <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#modal_default">Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                        </div>
                    </div>
                    
                    <div class="table-responsive">
                        <table class="table table-stripe" id="tabledt">
                            <thead>
                                <tr >
                                    <th>No</th>
                                    <th>Kedudukan ID</th>
                                    <th>Golongan</th>
                                    <th>Tunjangan</th>
                                    <th>Nama</th>
                                    <th>Usia (hari)</th>
                                    <!-- <th class="text-center">#</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /basic table -->
                
            </div>
            <!-- /content area -->
            
            
            
            
            <?php $this->load->view('template/footer');?>
            <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
            <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
            <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
            <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
            <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
            <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
            <script src="<?php echo base_url('assets/plugin/jquery-validation/jquery.validate.min.js') ?>"></script>
            
            
            
        </div>
        <!-- /main content -->
        

        <!-- START MODAL ADD -->
        <div id="mdlADD" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Tambah Data</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <form id="addData">
                        <div class="modal-body">
                            <div class="form-group row col-md-12">
                                <label>Kode</label>
                                <input type="text" class="form-control" name="kode_add" id="kode_add" required autocomplete="off">
                                <small style="color: red;">Pastikan kode belum pernah terinput</small>
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Golongan</label>
                                <input type="text" class="form-control" name="golongan_add" id="golongan_add" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="namane_add" id="namane_add" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Tunjangan</label>
                                <input type="text"  class="form-control" name="tunjangan_add" id="tunjangan_add" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12 dropinduk">
                                <label>Usia(Hari)</label>
                                <input type="text" class="form-control" name="usia_hari_add" id="usia_hari_add" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12 dropinduk">
                                <label>Jenis</label>
                                <select class="form-control" name="jenis" id="jenis">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        <div class="modal-footer">
                            <!-- <button type="submit" class="btn btn-md btn-success">Simpan</button> -->
                            <button type="button" id="btn_add" class="btn btn-md btn-success">Simpan</button>
                            <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL ADD -->
        
        <!-- START MODAL DETAIL -->
        <div id="mdlDetail" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    
                    <form action="" method="POST">
                        <div class="modal-body">
                            <input type="hidden" readonly class="form-control" name="id" id="id" required autocomplete="off">
                            <div class="form-group row col-md-12">
                                <label>Nama</label>
                                <input type="text" class="form-control" name="namane" id="namane" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Tunjangan</label>
                                <input type="text"  class="form-control" name="tunjangan" id="tunjangan" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12 dropinduk">
                                <label>Usia(Hari)</label>
                                <input type="text" class="form-control" name="usia_hari" id="usia_hari" required autocomplete="off">
                            </div>
                        </div>
                        
                        
                        <div class="modal-footer">
                            <button type="button" id="btn_update" class="btn btn-md btn-success">Update</button>
                            <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->
        
        
    </div>
    <!-- /page content -->

    <script>
        var readDataStdTunjanganKedudukanUrl = '<?php echo site_url('standartunjangan/get_kedudukan') ?>';
    </script>
    
    <?php $this->load->view('master/standartunjangan/kedudukan/js');?>
    
    
    
    
    
    
    
    
</body>
</html>
