<script>
    $(function () {
        $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
             lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
        }
        });

        let yuerel, tableContent = $("#tabledt").DataTable({
            'lengthMenu' : [
                        [ 10, 25, 50, 100,  -1 ],
                        [ '10', '25', '50', '100', 'Semua' ]
                ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                ]
            },
            processing: true,
            serverSide: true,
            serverMethod: 'post',
            ajax: readDataStdTunjanganKedudukanUrl,
            // columnDefs: [{
            // 	searcable: !1,
            // 	orderable: !1,
            // 	targets: 0
            // }],
            order: [
                [5, "desc"]
            ],
            columns: [
                { "data": null,"sortable": false, 
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },{
                data: "kedudukan_id"
            },{
                data: "golongan"
            },{
                data: "tunjangan"
            },{
                data: "nama"
            },{
                data: "usia"
            }
        ]
        });
    });
    
</script>

<script>
    function detailData(id){
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        $.ajax({
            url: "<?=base_url('standartunjangan/get_detail_kedudukan')?>",
            type: "POST",
            data: {
                id : id
            },
            dataType: "json",
            success: function (data) {
                // console.log(data);

                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];

                if(sts == 1){
                    $('#mdlDetail').modal('show');

                    $('#id').val(id)
                    $('#namane').val(dt['name'])
                    $('#fungsional_kode').val(dt['fungsional_kode'])
                    $('#tunjangan').val(dt['tunjangan'])
                    $('#usia_hari').val(dt['usia'])

                } else {

                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });
    }
</script>

<script>
    $('#btn_update').on('click',function(){

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var id=$('#id').val();
        var nama = $('#namane').val()
        var tunjangan = $('#tunjangan').val()
        var usia = $('#usia_hari').val()

        $.ajax({
            type : "POST",
            url  : "<?php echo base_url('standartunjangan/kedudukan_update_action')?>",
            dataType : "json",
            data : {
                id:id,
                nama:nama,
                tunjangan:tunjangan,
                usia:usia
            },
            success: function(data){
            console.log(data)

            var sts = data['status'];
            var msg = data['message'];
            var dt = data['data'];

            if(sts == 1){
                $('[name="id"]').val("");
                $('[name="nama"]').val("");
                // $('[name="fungsional_kode"]').val("");
                $('[name="tunjangan"]').val("");
                $('[name="usia_hari"]').val("");

                $('#mdlDetail').modal('hide');
                $('#tabledt').DataTable().ajax.reload(null, false);
                    swalInit.fire(
                    'Berhasil',
                    'Berhasil Update Data',
                    'success'
                );
            } else {
                swalInit.fire({
                    title: 'Oppss',
                    text: msg,
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swalInit.fire({
                        title: 'Oppss',
                        text: 'Terjadi kesalahan nih',
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
            }
        });
        return false;
    });
</script>

<script>
    function hapusData(id){

        var id = $('#id').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax');
                $.ajax({
                    url: "<?=base_url('standartunjangan/kedudukan_hapus_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if(sts == 1){
                            $('#mdlDetail').modal('hide');
                            $('#tabledt').DataTable().ajax.reload(null, false);
                            swalInit.fire(
                                'Berhasil',
                                msg,
                                'success'
                            );

                            // swalInit.fire({
                            //     title: 'Yeay',
                            //     text: msg,
                            //     type: 'success',
                            //     onClose: function() {
                            //         $('#tabledt').DataTable().ajax.reload(null, false);
                            //     }
                            // });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#tabledt').DataTable().ajax.reload(null, false);
                            }
                        });
                    }
                });
            }
            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     // swalInit.fire(
            //     //     'Cancelled',
            //     //     'Your imaginary file is safe :)',
            //     //     'error'
            //     // );
            // }
        });
    }
</script>

<script>
    function add(){
        $('#mdlADD').modal('show');
        // $('#mdlDetail').modal('show');
    }

    $('#btn_add').on('click',function(){

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var kd = $('#kode_add').val()
        var gol = $('#golongan_add').val()
        var nm = $('#namane_add').val()
        var tunjangan = $('#tunjangan_add').val()
        var usia = $('#usia_hari_add').val()
        var jns = $('#jenis').val()

        if(gol == '' || nm == '' || tunjangan == '' || usia == '' || jns == ''){
            alert('pastikan semua kolom terisi')
        } else {
            $.ajax({
                type : "POST",
                url  : "<?php echo base_url('standartunjangan/kedudukan_add_action')?>",
                dataType : "json",
                data : {
                    kode: kd,
                    golongan:gol,
                    nama:nm,
                    tunjangan:tunjangan,
                    usia:usia,
                    jenis:jns
                },
                success: function(data){
                console.log(data)

                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];

                if(sts == 1){
                    $('[name="id"]').val("");
                    $('[name="nama"]').val("");
                    // $('[name="fungsional_kode"]').val("");
                    $('[name="tunjangan"]').val("");
                    $('[name="usia_hari"]').val("");

                    $('#mdlADD').modal('hide');
                    $('#tabledt').DataTable().ajax.reload(null, false);
                        swalInit.fire(
                        'Berhasil',
                        'Berhasil Menambah Data',
                        'success'
                    );
                } else {
                    swalInit.fire({
                        title: 'Oppss',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
                }


                },
                error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#tabledt').DataTable().ajax.reload(null, false);
                            }
                        });
                }
            });
            return false;
        }

        
    });
</script>