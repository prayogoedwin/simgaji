<?php
$A_01 = $this->session->userdata('A_01');
$A_02 = $this->session->userdata('A_02');
$A_03 = $this->session->userdata('A_03');
$A_04 = $this->session->userdata('A_04');
$A_05 = $this->session->userdata('A_05');

$jenis = $this->input->get('jns', TRUE);
?>

<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master Pegawai P3K</a>
                    <a href="<?=base_url('pegawai_p3k')?>" class="breadcrumb-item">Add Pegawai P3K</a>
                
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Form layouts -->
        <div class="row">

            <div class="col-md-12">
                <div class="row">
                    <!-- Vertical form -->
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header header-elements-inline">
                                <h5 class="card-title">Data</h5>
                                <div class="header-elements">
                                    <div class="list-icons">
                                        <a class="list-icons-item" data-action="collapse"></a>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <?php echo form_open('masterpegawai_tpp_p3ks/add_action'); ?>
                                <!-- <form action="<?=base_url('pegawai_p3k/detail_action')?>" method="post" onsubmit="return confirm('Apakah anda sudah yakin data yang akan diproses sudah benar?');"> -->

                                <div class="form-group row col-md-12" id="nama_div">
                                <label>Nama/NIP</label>
                                <select class="form-control" id="nipbos" style="width:100%!important;" required data-fouc></select>
                                <!-- <input type="text" id="nip" name="nip"> -->
                                </div>


                                    <?php foreach ($kodes as $key => $value) :?>
                                        <?php
                                        // $cb_index = '<input type="checkbox" name="kode_id[]" id="kode_id" value="'.$value->id.'">';
                                        $required =  '';
                                        $optionEl = '';
                                        $labelform = $value->name;
                                        $feld = $value->field;

                                        if($value->status_required == 1){
                                            $required = 'required';
                                        }
                                            if($value->input == '1'){
                                                if($value->field == 'nip'){
                                                    $elementForm = '<input type="'. $value->source .'" class="form-control" id="'. $value->field .'" name="'. $value->field .'" value="" onkeyup="cek_nip()" autocomplete="off" '.$required.'> <span id="pesan_nip"></span>';
                                                }else{
                                                    $elementForm = '<input type="'. $value->source .'" class="form-control" id="'. $value->field .'" name="'. $value->field .'" value="" autocomplete="off" '.$required.'> ';
                                                }
                                            } else if($value->input == '2'){
                                                $elementForm = '<textarea rows="4" cols="4" class="form-control" id="'. $value->field .'" name="'. $value->field .'" '.$required.'></textarea>';
                                            } else {
                                                $dataSource = $value->source;
                                                $elementFormSelOpen = '<select class="form-control select-search" name="'. $value->field .'" id="'. $value->field .'" '.$required.'>';
                                                                foreach ($$dataSource as $ki => $val) {
                                                                    
                                                                    

                                                                    $optionEl .= '<option value="'. $val->id .'">' . $val->name . '</option>';
                                                                }
                                                $elementFormSelClose = '</select>';

                                                $elementForm = $elementFormSelOpen . $optionEl . $elementFormSelClose;
                                            }
                                        ?>
                                    
                                        <div class="form-group">
                                            <label for=""><?=$labelform;?></label>
                                            
                                            <?=$elementForm;?>
                                        </div>

                                    <?php endforeach; ?>

                                     <div class="text-right">
                                        <button type="submit" class="btn btn-info" id="x">Tambah <i class="icon-checkmark2 ml-1"></i></button>
                                    </div> 
                                <!-- </form> -->
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
                    <!-- /vertical form -->
                </div>

            </div>
        </div>
        <!-- /form layouts -->

    </div>
    <!-- /content area -->




<?php $this->load->view('template/footer_custom');?>


    <script src="<?=base_url('assets/')?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/demo_pages/form_select2.js"></script>
</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }
</script>

<script type='text/javascript'>

    function cek_nip(){
        $("#pesan_nip").hide();
 
        var nip = $("#nip").val();
       
 
        if(nip != ""){
            $.ajax({
                url: "<?php echo site_url() . 'masterpegawai_p3ks/cek_nip'; ?>", //arahkan pada proses_tambah di controller member
                data: 'nip='+nip,
                type: "POST",
                success: function(msg){
                    if(msg==1){
                        $("#pesan_nip").css("color","#fc5d32");
                        $("#nip").css("border-color","#fc5d32");
                        $("#pesan_nip").html("&nbsp; Maaf NIP sudah dipakai.");
                        $("#x").hide();
                        error = 1;
                    }else{
                        $("#pesan_nip").css("color","#59c113");
                        $("#nip").css("border-color","#59c113");
                        $("#pesan_nip").html("&nbsp; NIP bisa di daftarkan");
                        $("#x").show();
                        
                        error = 0;
                    }
                    console.log(msg);
 
                    $("#pesan_nip").fadeIn(1000);
                }
            });
        } 
    }      
         
    

</script>


<script>
    $(function () {
        var fixurl = '<?=base_url('verifikator/cari_pns_all_dinas/');?>';
        $('#nipbos').select2({
            ajax: {
                url: fixurl,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        nipnya: params.term
                    }
                    
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            placeholder: "Masukkan NIP / Nama",
            minimumInputLength: 5,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        
        function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }
            
            var markup = "<div>" + repo.name + "</div><div>" + repo.id + "</div>";
            
            return markup;
        }
        
        function formatRepoSelection (repo) {
            return repo.id;
        }
        
        $('#nipbos').change(function(){
           

            <?php foreach ($kodes2 as $key2 => $value2) :?>

                var <?=$value2->field?>  = $('#nipbos').select2('data')[0].<?=$value2->kode_simpeg?>;
                $('#<?=$value2->field?>').val(<?=$value2->field?>)

            <?php endforeach; ?>

            
        });

        
    });

   
</script>

<!-- // var nip = $('#nipbos').select2('data')[0].B_02B;
            // $('#nip').val(nip); -->

</body>
</html>