<script>
    $(function() {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                },
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
            }
        });

        var tabel = $('#tabledt').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'Semua']
            ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                    // {
					// 	text: 'Hapus',
					// 	className: 'btn',
					// 	action: function(e, dt, node, config) {
					// 		bulk_delete();
					// 	}
					// },
                ]
            },
            "oLanguage": {
                "sEmptyTable": "Data Tidak Ditemukan",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
            },
            "pageLength": 10,
            'destroy': true,
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': true,
            'autoWidth': false,
            "serverSide": true,
            "ajax": {
                "url": "<?= base_url('masterpegawai_p3ks/get_pegawai') ?>",
                "type": "POST",
                "data": function ( data ) {
                data.lokasi_kerja = $('#lokasi_kerja').val();
                data.lokasi_gaji = $('#lokasi_gaji').val();
                data.nip_s = $('#nip_s').val();
                data.nama_s = $('#nama_s').val();
                data.lokasi_kerja_s = $('#lokasi_kerja_s').val();
                data.lokasi_gaji_s = $('#lokasi_gaji_s').val();
                }
            },
            'columns': [
                {
                    data: 'nip'
                },
                {
                    data: 'nama'
                },
                {
                    data: 'lokasikerja'
                },
                {
                    data: 'lokasigaji'
                },
                // {
                //     data: 'id'
                // }
            ],

            columnDefs: [{}],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });

    $('#btn-filter').click(function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        // alert($('#lokasi_gaji').val());
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#nip_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#nama_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });
    

    $('#lokasi_kerja_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#lokasi_gaji_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        alert(this);
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

   
</script>


