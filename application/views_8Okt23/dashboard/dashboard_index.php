<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold">Dashboard</span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            <!-- <div class="header-elements d-none">
                <a href="#" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
            </div> -->

           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <span class="breadcrumb-item active"><i class="icon-home2 mr-2"></i> Dashboard</span>
                    <!-- <a href="index.html" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Home</a> -->
                    <!-- <a href="#" class="breadcrumb-item">Link</a> -->
                    <!-- <span class="breadcrumb-item active">Current</span> -->
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic card -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Pengumuman</h5>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <table class="table">

                <?php 
                $no = 1;
                foreach($pengumuman as $peng): ?>
                    <tr>
                        <td><?=$no++;?></td>
                        <td><?=$peng->pengumuman;?></td>
                    </tr>
                    
                    <?php endforeach; ?>
                </table>
                
               
               

               
            </div>
        </div>
        <!-- /basic card -->

    </div>
    <!-- /content area -->



<?php $this->load->view('template/footer');?>
</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?= $this->session->userdata("status"); ?>' == 'error') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'error'
        }).show();
    } else if ('<?= $this->session->userdata("status"); ?>' == 'success') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'success'
        }).show();
    }
</script>

</body>
</html>