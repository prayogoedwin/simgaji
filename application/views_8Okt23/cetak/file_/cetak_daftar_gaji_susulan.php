<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?

$tb_kalkulasi = 'simgaji_susulan';
$tb_lokasi = 'simgaji_lokasis';

$pageBreak = "<tr><td>&nbsp;</td></tr></table>
			  <div style=\"page-break-after:always\"></div>
			  <table style=\"width:33.5cm\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";		 


function showPageHeader($strhal,$strkodelokasi,$strlokasigaji,$strperiode) {
	$xPeriode = explode("-",$strperiode);
	// $bulan = ORM::factory('bulan',$xPeriode[1]);
    $bulan = get_bulan($xPeriode[1]);
	
	return "<tr align='center' >
				<td colspan='11' class='borBottom'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>Hal. ".$strhal."</td>
							<td width='43%' align='center' valign='top'>PEMERINTAH PROVINSI JAWA TENGAH<br />
								DAFTAR GAJI SUSULAN DSB UNTUK PARA PEGAWAI / PEKERJA<br />
								KODE LOKASI : ".$strkodelokasi." &nbsp;&nbsp;&nbsp;".$strlokasigaji."<br />
								BAGIAN BULAN : ".strtoupper($bulan->name)." ".$xPeriode[0]."</td>
							<td width='13%' align='right' valign='bottom'>&nbsp;</td>
						</tr>
						<tr>
							<td colspan='3' height='8'></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td style=\"width:1cm\" valign='middle' align='center' rowspan='2'>No</td>
				<td style=\"width:7cm\" rowspan='2' valign='top' class='borLeft'>
					<table width='100%' cellspacing='0' cellpadding='0'>
						<tr>
							<td colspan='2'>NAMA</td>
						</tr>
						<tr>
							<td colspan='2'>TGL. LAHIR</td>
						</tr>
						<tr>
							<td colspan='2'>NIP</td>
						</tr>
						<tr>
							<td>STAPEG</td>
							<td align='right'>GOL/RUANG</td>
						</tr>
						<tr>
							<td colspan='2'>JABATAN</td>
						</tr>
					</table></td>
				<td style=\"width:1.7cm\" valign='middle' align='center' rowspan='2' class='borLeft'>STWIN<br />J.JIWA</td>
				<td align='center' valign='middle' colspan='3' class='borBothBottom'>PENGHASILAN</td>
				<td style=\"width:3.7cm\" align='center' valign='middle' class='borBottom'>POTONGAN</td>
				<td style=\"width:3cm\" valign='middle' align='right' rowspan='2' class='borBoth'>JML BERSIH</td>
				<td style=\"width:2cm\" valign='middle' align='right' rowspan='2'>POTONGAN<br />PPH</td>
				<td style=\"width:3.8cm\" valign='middle' align='right' rowspan='2' class='borBoth'>JUMLAH<br />PENGHASILAN<br />BERSIH YANG<br />DIBAYARKAN</td>
				<td style=\"width:2.8cm\" valign='middle' align='center' rowspan='2'>ASKES<br />BPJS</td>
			</tr>
			<tr>
				<td width='7%' align='right' valign='top' class='borBoth'>GAJI POKOK<br />TJ. ISTRI<br />TJ. ANAK<br />JUMLAH</td>
				<td width='10%' align='right' valign='top'>TUNJ. UMUM<br />TMB.TJ.UMUM<br />TJ.STRUK.<br />TJ.FUNGS.<br />TJ.BERAS<br />TJ.PPH</td>
				<td width='10%' align='right' valign='top' class='borBoth'>PEMBULATAN<br />JUMLAH KOTOR</td>
				<td width='11%' align='right' valign='top'>IWP (10%)<br />LAIN-LAIN<br />POT.BERAS<br />CP.<br />JML.POTONGAN</td>
			</tr>
			<tr align='center' >
				<td class='borTopBottom' >1</td>
				<td class='borTopBottom'>2</td>
				<td class='borTopBottom'>3</td>
				<td class='borTopBottom'>4</td>
				<td class='borTopBottom'>5</td>
				<td class='borTopBottom'>6</td>
				<td class='borTopBottom'>7</td>
				<td class='borTopBottom'>8</td>
				<td class='borTopBottom'>9</td>
				<td class='borTopBottom'>10 (8-9)</td>
				<td class='borTopBottom'>11</td>
			</tr>
			<tr>
				<td colspan='11' height='8'></td>
			</tr>";	
}

function showPageData($urut,$nama,$lahir,$nip,$status,$golongan,$jabatan,$marital,$jiwa,$gajipokok,$tjistri,$tjanak,$j4,$tjumum,$tjumum_tambahan,$tjstruktural,$tjfungsional,$tjberas,$tjpph,$pembulatan,$jmlkotor,$potiwp,$potlain,$potberas,$potcp,$jmlpot,$jmlbersih,$jmlbersihbayar,$askes,$bpjs) {
	return "<tr valign='top'>
				<td style=\"width:1cm\"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>".$urut."</td>
					  </tr>
					</table></td>
				<td style=\"width:7cm\" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'>".$nama."</td>
						</tr>
						<tr>
							<td colspan='2'>".$lahir."</td>
						</tr>
						<tr>
							<td colspan='2'>".$nip."</td>
						</tr>
						<tr>
							<td width='70%'>".$status."</td>
							<td width='30%' align='right'>".$golongan."</td>
						</tr>
						<tr>
							<td colspan='2'>".$jabatan."</td>
						</tr>
					</table></td>
				<td style=\"width:1.7cm\" align='center'>".$marital."<br />".$jiwa."</td>
				<td style=\"width:2.4cm\" align='right' valign='top'>".$gajipokok."<br />".$tjistri."<br />".$tjanak."<br />".$j4."</td>
				<td style=\"width:3.4cm\" align='right' valign='top'>".$tjumum."<br />".$tjumum_tambahan."<br />".$tjstruktural."<br />".$tjfungsional."<br />".$tjberas."<br />".$tjpph."</td>
				<td style=\"width:3.3cm\" align='right' valign='top'>".$pembulatan."<br />".$jmlkotor."</td>
				<td style=\"width:3.7cm\" align='right' valign='top'>".$potiwp."<br />".$potlain."<br />".$potberas."<br />".$potcp."<br />".$jmlpot."</td>
				<td style=\"width:3cm\" align='right'>".$jmlbersih."</td>
				<td style=\"width:2.8cm\" align='right'>".$tjpph."</td>
				<td style=\"width:3.8cm\" align='right'>".$jmlbersihbayar."</td>
				<td style=\"width:2cm\" align='right'>".$askes."<br>".$bpjs."</td>
			</tr>";			
}

function showPageDataKekurangan($urut,$nama,$lahir,$nip,$status,$golongan,$jabatan,$marital,$jiwa,$gajipokok,$tjistri,$tjanak,$j4,$tjumum,$tjumum_tambahan,$tjstruktural,$tjfungsional,$tjberas,$tjpph,$pembulatan,$jmlkotor,$potiwp,$potlain,$potberas,$potcp,$jmlpot,$jmlbersih,$jmlbersihbayar,$askes,$bpjs) {
	return "<tr valign='top' style='border-top:solid'>
				<td style=\"width:1cm\"' align='center' valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
					  <tr>
						<td width='100%' colspan='2' align='center'>#</td>
					  </tr>
					</table></td>
				<td style=\"width:7cm\" valign='top'>
					<table width='100%' cellspacing='0' cellpadding='0' class='text1'>
						<tr>
							<td colspan='2'><b>Kekurangan Gaji 1x Bulan</b></td>
						</tr>
						<tr>
							<td colspan='2'>".$nama."</td>
						</tr>
						<tr>
							<td colspan='2'>".$lahir."</td>
						</tr>
						<tr>
							<td colspan='2'>".$nip."</td>
						</tr>
						<tr>
							<td width='70%'>".$status."</td>
							<td width='30%' align='right'>".$golongan."</td>
						</tr>
						<tr>
							<td colspan='2'>".$jabatan."</td>
						</tr>
					</table></td>
				<td style=\"width:1.7cm\" align='center'>".$marital."<br />".$jiwa."</td>
				<td style=\"width:2.4cm\" align='right' valign='top'>".$gajipokok."<br />".$tjistri."<br />".$tjanak."<br />".$j4."</td>
				<td style=\"width:3.4cm\" align='right' valign='top'>".$tjumum."<br />".$tjumum_tambahan."<br />".$tjstruktural."<br />".$tjfungsional."<br />".$tjberas."<br />".$tjpph."</td>
				<td style=\"width:3.3cm\" align='right' valign='top'>".$pembulatan."<br />".$jmlkotor."</td>
				<td style=\"width:3.7cm\" align='right' valign='top'>".$potiwp."<br />".$potlain."<br />".$potberas."<br />".$potcp."<br />".$jmlpot."</td>
				<td style=\"width:3cm\" align='right'>".$jmlbersih."</td>
				<td style=\"width:2.8cm\" align='right'>".$tjpph."</td>
				<td style=\"width:3.8cm\" align='right'>".$jmlbersihbayar."</td>
				<td style=\"width:2cm\" align='right'>".$askes."<br>".$bpjs."</td>
			</tr>";			
}

// Check Data Exist
// $n_kalkulasis = ORM::factory('kalkulasi')
// 	->where('periode','=',$periode)
// 	->where('nip','=',$nip)
// 	->count_all();

// $this->db->where('periode', $periode);
$this->db->where('nip', $nip);
$this->db->where('jenis_susulan', 1);
$this->db->from($tb_kalkulasi);
$n_kalkulasis = $this->db->count_all_results();

if($n_kalkulasis == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk NIP ini tidak tersedia<br>Silakan pilih NIP lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

// CSS Block
echo "<div class='cetak'>";
echo "<table style=\"width:33.5cm\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";	
?>
<table style="width:33.5cm" border="0" cellpadding="3" cellspacing="0">
<?
// $kalkulasis = ORM::factory('kalkulasi')
// 	->where('periode','=',$periode)
// 	->where('nip','=',$nip);

// $this->db->where('periode', $periode);
$this->db->where('nip', $nip);
$this->db->where('jenis_susulan', 1);
$this->db->from($tb_kalkulasi);
$kalkulasis = $this->db->get();
	
// $nPeg = $kalkulasis->reset(FALSE)->count_all();	
// $this->db->where('periode', $periode);
$this->db->where('nip', $nip);
$this->db->where('jenis_susulan', 1);
$this->db->from($tb_kalkulasi);
$nPeg = $this->db->count_all_results();

if($nPeg) {
	$z = 0;
	$no = 1;
	$j = 1;
	
	$kalkulasis = $kalkulasis->result();

	foreach($kalkulasis as $kalkulasi) {
		$tanggal_lahir = new DateTime($kalkulasi->tanggal_lahir);
		
		// $lokasi_gaji = ORM::factory('lokasi', $kalkulasi->lokasi_id);

        $this->db->from($tb_lokasi);
        $this->db->where('id', $kalkulasi->lokasi_id);
        $lokasi_gaji = $this->db->get()->row();
		
		$sNama = $kalkulasi->nama;
		$sTglLahir = $tanggal_lahir->format("Y-m-d");
		$sNipLama = $kalkulasi->nip;
		$sNipBaru = $kalkulasi->nip;
		$sStatus = $kalkulasi->status_string;
		$sGolongan = $kalkulasi->golongan_id;
		$sStrGolongan = $kalkulasi->golongan_string;
		$sJabatan = $kalkulasi->jabatan;
		$sMarital = $kalkulasi->marital_string;
		$sJmlJiwa = $kalkulasi->jiwa_lama;
		$sGajiPokok = numFormat($kalkulasi->gaji_pokok);
		$sTjIstri = numFormat($kalkulasi->tunjangan_istri);
		$sTjAnak = numFormat($kalkulasi->tunjangan_anak);
		$sJ4 = numFormat($kalkulasi->jumlah_penghasilan);
		$sTjUmum = numFormat($kalkulasi->tunjangan_umum);
		$sTjUmumPlus = numFormat($kalkulasi->tunjangan_umum_tambahan);
		$sTjStruktural = numFormat($kalkulasi->tunjangan_struktural);
		$sTjFungsional = numFormat($kalkulasi->tunjangan_fungsional);
		$sTjBeras = numFormat($kalkulasi->tunjangan_beras);
		$sTjPph = numFormat($kalkulasi->tunjangan_pph);
		$sPembulatan = numFormat($kalkulasi->pembulatan);
		$sJmlKotor = numFormat($kalkulasi->jumlah_kotor);
		$sPotIwp = numFormat($kalkulasi->potongan_iwp);
		$sPotLain = numFormat($kalkulasi->potongan_lain);
		$sPotBeras = numFormat($kalkulasi->potongan_beras);
		$sPotCp = numFormat($kalkulasi->potongan_cp);
		$sJmlPot = numFormat($kalkulasi->jumlah_potongan);
		$sJmlBersih = numFormat($kalkulasi->jumlah_bersih);
		$sPotPph = numFormat($kalkulasi->tunjangan_pph);
		$sJmlBersihBayar = numFormat($kalkulasi->jumlah_bersih_bayar);
		
		$askes = numFormat(0.03 * $kalkulasi->jumlah_penghasilan);
		$bpjs = numFormat(0.0054 * $kalkulasi->gaji_pokok);	
		
		// echo showPageHeader($j,$lokasi_gaji->kode,$lokasi_gaji->name,$periode);		
		echo showPageHeader($j,'','',$periode);		
		echo showPageData($no,$sNama,$tanggal_lahir->format('d-m-Y'),$sNipBaru,$sStatus,$sStrGolongan,$sJabatan,$sMarital,$sJmlJiwa,$sGajiPokok,$sTjIstri,$sTjAnak,$sJ4,$sTjUmum,$sTjUmumPlus,$sTjStruktural,$sTjFungsional,$sTjBeras,$sTjPph,$sPembulatan,$sJmlKotor,$sPotIwp,$sPotLain,$sPotBeras,$sPotCp,$sJmlPot,$sJmlBersih,$sJmlBersihBayar,$askes,$bpjs);
		echo showPageDataKekurangan($no,$sNama,$tanggal_lahir->format('d-m-Y'),$sNipBaru,$sStatus,$sStrGolongan,$sJabatan,$sMarital,$sJmlJiwa,$sGajiPokok,$sTjIstri,$sTjAnak,$sJ4,$sTjUmum,$sTjUmumPlus,$sTjStruktural,$sTjFungsional,$sTjBeras,$sTjPph,$sPembulatan,$sJmlKotor,$sPotIwp,$sPotLain,$sPotBeras,$sPotCp,$sJmlPot,$sJmlBersih,$sJmlBersihBayar,$askes,$bpjs);
	}
}
?>
</table>
</div>
</body>
</html>