<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="cetak">

<?
$tb_kalkulasi = 'simgaji_kalkulasi_tpp';
$tb_lokasi = 'simgaji_lokasis';
$tb_golongan = 'simgaji_golongans';
?>

<?
$page = 1;

// echo json_encode($arrLokasiInduk);
// die();
foreach($arrLokasiInduk as $lokasi) {
	// $lokasi = ORM::factory('lokasi',$lokasi);
    $this->db->where('id', $lokasi);
	$lokasi = $this->db->get($tb_lokasi)->row();
	
	// $xPeriode = explode("-",$periode);    	
	// $bulan = ORM::factory('bulan',intval($xPeriode[1]));
    $xPeriode = explode("-",$periode);
	$bulan = get_bulan($xPeriode[1]);   

	
	// $tpps = ORM::factory('tpp')
	// 	->where('periode','=',$periode)
	// 	->where('lokasi_id','=',$lokasi->id);
		
	// if(substr($lokasi->name,0,4) == "BIRO") {
	// 	$kode = 	"lokasi LIKE '".substr($lokasi->kode,0,6)."%'";
	// }
	// else {
	// 	$kode = 	"LEFT(lokasi,2)='".substr($lokasi->kode,0,2)."'";
	// }
	
	// $nPeg = $tpps->reset(FALSE)->count_all();

    // echo $lokasi;
    // die();

    $this->db->where('periode', $periode);
    $this->db->where('lokasi_gaji', $lokasi->id);
    if(substr($lokasi->name,0,4) == "BIRO") {
		$kode = 	"lokasi_gaji_kode LIKE '".substr($lokasi->kode,0,6)."%'";
	}
	else {
		$kode = 	"LEFT(lokasi_gaji_kode,2)='".substr($lokasi->kode,0,2)."'";
	}

    $tpps = $this->db->get($tb_kalkulasi);

    // $this->db->where('periode', $periode);
    // $this->db->where('lokasi_gaji', $lokasi->id);
    // $this->db->from($tb_kalkulasi);
    $nPeg = $tpps->num_rows();

	if($nPeg > 0) {		
		?>	
		<table style="width:32.5cm" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td align="center" valign="top">REKAPITULASI TAMBAHAN PENGHASILAN BERDASARKAN BEBAN KERJA<br />
															BAGIAN  BULAN : <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?><br>
			  												BIRO / INSTANSI : <? echo $lokasi->name; ?></td>
			</tr>
            <tr>
                <td align="left"><? echo "LOKASI : ".$lokasi->kode; ?></td>
            </tr>
		</table>
		<table border="0" cellspacing="0" cellpadding="0" style="width:32.5cm">
          <tr>
				<td colspan="9"><? echo str_repeat("=",160); ?></td>
		  <tr>	
		  <tr>
				<td style="width:6.6cm">GOLONGAN</td>
				<td style="width:2.8cm" align="center">JML PEG</td>
				<td style="width:3.3cm" align="right">TAMBAHAN<br>PENGHASILAN</td>
				<td style="width:3.3cm" align="right">POTONGAN<br>TAMBAHAN<br>PENGHASILAN</td>
				<td style="width:3.3cm" align="right">TUNJANGAN<br>PPH 21</td>
				<td style="width:3.3cm" align="right">PENERIMAAN<br>KOTOR</td>
				<td style="width:3.3cm" align="right">POTONGAN<br>PPH 21</td>                
                <td style="width:3.3cm" align="right">POTONGAN<br>BPJS</td>
				<td style="width:3.3cm" align="right">PENERIMAAN<br>BERSIH</td>
		  </tr>
		  <tr>
				<td colspan="9"><? echo str_repeat("-",160); ?></td>
		  <tr>	
		<?
		// $golongan_prefixs = ORM::factory('golongan')->group_by('prefix')->find_all();
        $this->db->group_by('prefix');
        $golongan_prefixs = $this->db->get($tb_golongan)->result();
       
		foreach($golongan_prefixs as $golongan_prefix) {
			// $golongans = ORM::factory('golongan')
			// 	->where('LEFT("id",1)','=',substr($golongan_prefix->id,0,1))
			// 	->find_all();

            // $this->db->group_by('prefix');
            // $this->db->where('LEFT("id",1)', substr($golongan_prefix->id,0,1));
            $this->db->like('id', substr($golongan_prefix->id,0,1), 'AFTER');
            $golongans = $this->db->get($tb_golongan)->result();

            // echo json_encode($golongans);
            // die();
				
			foreach($golongans as $golongan) {
				// $sql = 
				// "SELECT count(nip) AS pegawai,sum(tpp) AS tpp, sum(insentip) AS insentip, sum(potongan) AS potongan, sum(potongan_tpp_bpjs) AS potongan_bpjs, sum(pph) AS pph, sum(konker) AS konker, sum(pph_konker) AS pph_konker, 
				// sum(jakarta) AS jakarta, sum(pph_jakarta) AS pph_jakarta
				// FROM $tb_kalkulasi WHERE ".$kode." AND periode = '".$periode."' AND golongan_id ='".$golongan->id."'";	

                $sql = 
                "SELECT count(nip) AS pegawai, sum(beban_kerja) AS insentip, sum(beban_kerja_nominal) AS tpp, sum(beban_kerja_pot) AS potongan,sum(beban_kerja_pot_bpjs) AS potongan_bpjs, sum(beban_kerja_pph) AS pph
                FROM $tb_kalkulasi WHERE ".$kode." AND periode = '".$periode."' AND golongan_id ='".$golongan->id."'";	

				
				// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
                $sqls = $this->db->query($sql)->result();
                	
				foreach($sqls as $row) {	
					?>
					<tr>
						<td valign="top"><? echo "GOL. ".$golongan->kode; ?></td>
						<td align="center" valign="top"><? echo round($row->pegawai); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->insentip); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->potongan); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->pph); ?></td>
						<td align="right" valign="top"><? echo numFormat(($row->insentip - $row->potongan) + $row->pph); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->pph); ?></td>
                        <td align="right" valign="top"><? echo numFormat($row->potongan_bpjs); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->insentip - $row->potongan - $row->potongan_bpjs); ?></td>
					</tr>					
				<?			
				}
			}
			
            // $sql = 
			// "SELECT count(nip) AS pegawai,sum(tpp) AS tpp, sum(insentip) AS insentip, sum(potongan) AS potongan, sum(potongan_tpp_bpjs) AS potongan_bpjs, sum(pph) AS pph, sum(konker) AS konker, sum(pph_konker) AS pph_konker, 
			// sum(jakarta) AS jakarta, sum(pph_jakarta) AS pph_jakarta 
			// FROM tpps WHERE ".$kode." AND LEFT(golongan_id,1)='".substr($golongan_prefix->id,0,1)."' AND periode = '".$periode."'";	

            $sql = 
            "SELECT count(nip) AS pegawai, sum(beban_kerja) AS insentip, sum(beban_kerja_nominal) AS tpp, sum(beban_kerja_pot) AS potongan,sum(beban_kerja_pot_bpjs) AS potongan_bpjs, sum(beban_kerja_pph) AS pph
            FROM $tb_kalkulasi WHERE ".$kode." AND LEFT(golongan_id,1)='".substr($golongan_prefix->id,0,1)."' AND periode = '".$periode."'";	
			
			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
            $sqls = $this->db->query($sql)->result();
			
			foreach($sqls as $row) {
				?>
                <tr>
                    <td colspan="9"><hr /></td>
                </tr> 
				<tr>
					<td valign="top">GOLONGAN <? echo $golongan_prefix->prefix; ?></td>
					<td align="center" valign="top"><? echo round($row->pegawai); ?></td>
					<td align="right" valign="top"><? echo numFormat($row->insentip); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->potongan); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->pph); ?></td>
						<td align="right" valign="top"><? echo numFormat(($row->insentip - $row->potongan) + $row->pph); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->pph); ?></td>
                        <td align="right" valign="top"><? echo numFormat($row->potongan_bpjs); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->insentip - $row->potongan - $row->potongan_bpjs); ?></td>
				</tr>
				<tr>
					<td colspan="9"><hr /></td>
				</tr>		
				<?
			}
		}
		// $sql = 
		// "SELECT count(nip) AS pegawai,sum(tpp) AS tpp, sum(insentip) AS insentip, sum(potongan) AS potongan, sum(potongan_tpp_bpjs) AS potongan_bpjs, sum(pph) AS pph, sum(konker) AS konker, sum(pph_konker) AS pph_konker, 
		// sum(jakarta) AS jakarta, sum(pph_jakarta) AS pph_jakarta 
		// FROM tpps WHERE ".$kode." AND periode = '".$periode."'";	
         $sql = 
         "SELECT count(nip) AS pegawai, sum(beban_kerja) AS insentip, sum(beban_kerja_nominal) AS tpp, sum(beban_kerja_pot) AS potongan,sum(beban_kerja_pot_bpjs) AS potongan_bpjs, sum(beban_kerja_pph) AS pph	
         FROM $tb_kalkulasi WHERE ".$kode." AND periode = '".$periode."'";	
		
		// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
        $sqls = $this->db->query($sql)->result();		
		foreach($sqls as $row) {
			?>
			<tr>
				<td valign="top">TOTAL PER LOKASI</td>
				<td align="center" valign="top"><? echo round($row->pegawai); ?></td>
				<td align="right" valign="top"><? echo numFormat($row->insentip); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->potongan); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->pph); ?></td>
						<td align="right" valign="top"><? echo numFormat(($row->insentip - $row->potongan) + $row->pph); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->pph); ?></td>
                        <td align="right" valign="top"><? echo numFormat($row->potongan_bpjs); ?></td>
						<td align="right" valign="top"><? echo numFormat($row->insentip - $row->potongan - $row->potongan_bpjs); ?></td>
			</tr>
			<tr>
				<td colspan="9"><hr /></td>
			</tr>		
			<?
		}
		?>
		</table>
		<?	
		echo "<div style=\"page-break-after:always\"></div>";			
		$page++;
	}
}
?>
</div>
</body>
</html>