<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php
//constract
$tb_kalkulasi = 'simgaji_kalkulasi_tpp';
$tb_lokasi = 'simgaji_lokasis';
?>

<?
$pageBreak = "<tr><td>&nbsp;</td></tr></table>
			  <div style=\"page-break-after:always\"></div>
			  <table style=\"width:33.5cm\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";		 

function showPageHeader($periode,$lokasi,$kelompok,$halaman) {
    $ci =& get_instance();
    $tb_kalkulasi = 'simgaji_kalkulasi_tpp';
    $tb_lokasi = 'simgaji_lokasis';
    $xPeriode = explode("-",$periode);
	// $bulan = ORM::factory('bulan',$xPeriode[1]);
	$bulan = get_bulan($xPeriode[1]);

	// $xPeriode = explode("-",$periode);
	// $bulan = ORM::factory('bulan',$xPeriode[1]);
	
	// $lokasi = ORM::factory('lokasi',$lokasi);
    $ci->db->where('id', $lokasi);
	$lokasi = $ci->db->get($tb_lokasi)->row();

    // echo json_encode($lokasi);
    // die();

	if($kelompok) {
		$kelompok = " / ".$kelompok;
	}
	
	return "<tr align='center' >
				<td colspan='11'>
					<table style=\"width:33.5cm\" cellspacing='0' cellpadding='0'>
						<tr>
							<td width='13%' align='left' valign='bottom'>PEMERINTAH PROVINSI<br>&nbsp;&nbsp;&nbsp;&nbsp;JAWA TENGAH<br>*******************</td>
						</tr>
						<tr>
							<td width='100%' align='center' valign='top'>DAFTAR TAMBAHAN PENGHASILAN BERDASARKAN BEBAN KERJA KHUSUS<br />
								BAGIAN  BULAN : ".strtoupper($bulan->name)." ".$xPeriode[0]."<br>
								BIRO / INSTANSI : ".$lokasi->name."</td>
						</tr>
						<tr>
							<td align='left'>
								<table width='100%'>
									<tr>
										<td align='left'>LOKASI : ".$lokasi->kode.$kelompok."</td>
										<td align='right'>HALAMAN : ".$halaman."</td>
									</tr>
								</table></td>
						</tr>
					</table></td>
			</tr>
			<tr>
				<td colspan='11'>".str_repeat("=",160)."</td>
			<tr>
			<tr>
				<td style=\"width:1cm\" valign='middle' align='center'>No</td>
				<td style=\"width:4.4cm\" align='center' valign='middle'>NIP<br>GOLONGAN</td>
				<td style=\"width:7.5cm\" valign='middle' align='center'>NAMA<br>JABATAN<br/>KELAS</td>
				<td style=\"width:2.8cm\" align='center' valign='middle'>TAMBAHAN<br>PENGHASILAN</td>

				<td style=\"width:2.7cm\" align='center' valign='middle'>TUNJANGAN<br>PPH 21</td>
				<td style=\"width:2.8cm\" valign='middle' align='center'>PENERIMAAN<br>KOTOR</td>
				<td style=\"width:2.7cm\" valign='middle' align='center'>POTONGAN<br>PPH 21</td>
				<td style=\"width:2.2cm\" align='center' valign='middle'>POTONGAN<br>BPJS</td>
				<td style=\"width:2.8cm\" valign='middle' align='center'>PENERIMAAN<br>BERSIH</td>
				<td style=\"width:1.9cm\" valign='middle' align='left'>TANDA<br>TANGAN</td>
			</tr>
			<tr>
				<td colspan='11'>".str_repeat("=",160)."</td>
			<tr>";	
}

function showPageData($no,$nip,$golongan_string,$name,$jabatan,$kelas,$tpp,$potongan_bpjs,$pph,$tpp_diterima) {
	return "<tr valign='top'>
				<td align='center' valign='top'>".$no.".</td>
				<td valign='top' align='center'>".$nip."<br>".$golongan_string."</td>
				<td align='center'>".$name."<br>".$jabatan."<br>".$kelas."</td>
				<td align='right' valign='top'>".numFormat($tpp)."</td>

				<td align='right' valign='top'>".numFormat($pph)."</td>
				<td valign='top' align='right'>".numFormat($tpp_diterima + $potongan_bpjs + $pph)."</td>
				<td valign='top' align='right'>".numFormat($pph)."</td>	
				<td align='right' valign='top'>".numFormat($potongan_bpjs)."</td>
				<td valign='top' align='right'>".numFormat($tpp_diterima)."</td>
				<td valign='top' align='left'>".$no.".....</td>
			</tr>
			<tr>
				<td colspan='12' height='3'></td>
			<tr>";
}

function showPageJumlahLokasi($pegawai,$tpp,$potongan_bpjs,$pph,$tpp_diterima) {
	return "<tr>
				<td colspan='11'>".str_repeat("=",160)."</td>
			<tr>
			<tr valign='top'>
				<td align='center' valign='top' colspan='3'>SUB TOTAL PER LOKASI (".$pegawai." ORANG)</td>
				<td align='right' valign='top'>".numFormat($tpp)."</td>
				<td align='right' valign='top'>".numFormat($pph)."</td>
				<td valign='top' align='right'>".numFormat($tpp_diterima + $potongan_bpjs + $pph)."</td>
				<td valign='top' align='right'>".numFormat($pph)."</td>
				<td align='right' valign='top'>".numFormat($potongan_bpjs)."</td>
				<td valign='top' align='right'>".numFormat($tpp_diterima)."</td>
				<td valign='middle' align='center'>&nbsp;</td>
			</tr>		
			<tr>
				<td colspan='11'>".str_repeat("=",160)."</td>
			<tr>";
}

// Check Data Exist
// $n_tpps = ORM::factory('tpp')
// 	->where('periode','=',$periode)
// 	->where('lokasi_id','IN',$arrLokasi)
// 	->count_all();

// echo $periode;
// die();

$this->db->where('periode', $periode);
$this->db->where_in('lokasi_gaji', $arrLokasi);
$this->db->where('plt_stop', 0);
$this->db->from($tb_kalkulasi);
$n_tpps = $this->db->count_all_results();

if($n_tpps == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk Lokasi dan atau Periode ini tidak tersedia<br>Silakan pilih Lokasi atau Periode lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

// CSS Block
echo "<div class='cetak'>";
echo "<table style=\"width:33.5cm\" border=\"0\" cellpadding=\"3\" cellspacing=\"0\">";	

$data_per_page = 14;
foreach($arrLokasi as $lokasi) {	
	// $kelompoks = ORM::factory('tpp')
	// 	->where('lokasi_id','=',$lokasi)
	// 	->group_by('kelompok_gaji')
	// 	->order_by('kelompok_gaji','ASC')
	// 	->find_all();

    $this->db->where('lokasi_gaji', $lokasi);
    // $this->db->where('plt_stop', 0);
    // $this->db->group_by('kelompok_gaji');
    // $this->db->order_by('kelompok_gaji','ASC');
    $kelompoks = $this->db->get($tb_kalkulasi)->result();
			
	foreach($kelompoks as $kelompok) {	
		// $tpps = ORM::factory('tpp')
		// 	->where('periode','=',$periode)
		// 	->where('lokasi_id','=',$lokasi)
		// 	->where('kelompok_gaji','=',$kelompok->kelompok_gaji);

        $this->db->where('periode', $periode);
        $this->db->where('lokasi_gaji', $lokasi);
        $this->db->where('plt_stop', 0);
        // $this->db->where('kelompok_gaji', $kelompok->kelompok_gaji);
        // $this->db->order_by('kelompok_gaji','DESC');
        $this->db->order_by('eselon_id','DESC');
        $this->db->order_by('golongan_id','DESC');
        $this->db->order_by('nip','ASC');
        $tpps = $this->db->get($tb_kalkulasi);
					
		// $nPeg = $tpps->reset(FALSE)->count_all();

        $this->db->where('periode', $periode);
		$this->db->where('lokasi_gaji', $lokasi);
        // $this->db->where('plt_stop', 1);
		$this->db->from($tb_kalkulasi);
		$nPeg = $this->db->count_all_results();
	
		if($nPeg) {
			$z = 0;
			$no = 1;
			$j = 1;
			
			$tpps = $tpps->result();
				// ->order_by('eselon_id','DESC')
				// ->order_by('golongan_id','DESC')
				// ->order_by('nip','ASC')
				// ->find_all();
				
			foreach($tpps as $tpp) {				
				if($z%$data_per_page==0 and $z==0) {
					echo showPageHeader($periode,$lokasi,$kelompok->lokasi,$j);		
					$j++;
				}

                
				echo showPageData($no,$tpp->nip,$tpp->golongan_string,$tpp->name,$tpp->jabatan_string,$tpp->kelas_jabatan,$tpp->plt,$tpp->plt_pot_bpjs,$tpp->plt_pph,$tpp->plt_nominal);
				$z++;
				// $no,$nip,$golongan_string,$name,$jabatan,$kelas,$tpp,$potongan,$potongan_bpjs,$pph,$tpp_diterima
				// echo ($no,$tpp->nip,$tpp->golongan_string,$tpp->name,$tpp->jabatan_string,$tpp->beban_kerja,$tpp->beban_kerja_pot,$tpp->beban_kerja_pot_bpjs,$tpp->beban_kerja_pph,$tpp->beban_kerja_nominal);
				// $z++;
				
				if($z%$data_per_page==0 and $z > 0) {
					echo $pageBreak;
					echo showPageHeader($periode,$lokasi,$kelompok->lokasi,$j);
					$j++;
				}
								
				$no++;
			}
		
			// JUMLAH PER LOKASI
			$sql = 
			"SELECT count(nip) AS pegawai, sum(plt) AS insentip, sum(plt_nominal) AS tpp, sum(plt_pot_bpjs) AS potongan_bpjs, sum(plt_pph) AS pph
			FROM $tb_kalkulasi WHERE lokasi_gaji = ".$lokasi." AND periode = '".$periode."' AND kondisi_stop = 0";	
			
			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();	
            $sqls = $this->db->query($sql)->result();
            // $pegawai,$tpp,$potongan,$potongan_bpjs,$pph,$tpp_diterima					
			echo showPageJumlahLokasi($sqls[0]->pegawai,$sqls[0]->insentip,$sqls[0]->potongan_bpjs,$sqls[0]->pph,$sqls[0]->tpp);
			$z++;	
			
			echo $pageBreak;
			$j++;
		}
	}
}
?>
</table>
</div>
</body>
</html>