<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="cetak">

<?
$tb_kalkulasi = 'simgaji_kalkulasi_tpp';
$tb_lokasi = 'simgaji_lokasis';
$tb_golongan = 'simgaji_golongans';
?>

<?
// Check Data Exist
// 


// echo json_encode($arrLokasiInduk);
// die();
$this->db->where('periode', $periode);
$this->db->where('tempat_stop', 0);
$this->db->where_in('lokasi_gaji', $arrLokasiInduk);
$tpps = $this->db->get($tb_kalkulasi);
$n_tpps = $tpps->num_rows();

if($n_tpps == 0) {
	echo "<center>";
	echo "<div style=\"width:500px;margin-top:250px\" align=\"center\">";
	echo "<div class='alert alert-danger' role='alert'>";
	echo "<strong>Perhatian !</strong><br>Data untuk Lokasi dan atau Periode ini tidak tersedia<br>Silakan pilih Lokasi atau Periode lain";
	echo "</div>";
	echo "</div>";
	echo "</center>";
	die();
}

$page_break = "<div style=\"page-break-after:always\"></div>";	

$page=1;
foreach($arrLokasiInduk AS $lokasi) {
	// $lokasi = ORM::factory('lokasi',$lokasi);
	// $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
	// $strLokasi = $lokasi->name;

    $this->db->where('id', $lokasi);
    $lokasi = $this->db->get($tb_lokasi)->row();

    $kode = substr($lokasi->kode,0,4)." - ".substr($lokasi->kode,-4);
	$strLokasi = $lokasi->name;
	
	// $xPeriode = explode("-",$periode);    	
	// $bulan = ORM::factory('bulan',intval($xPeriode[1]));
    $xPeriode = explode("-",$periode);
	$bulan = get_bulan($xPeriode[1]);   
	
	if($page > 1) {
		echo $page_break;
	}

	?>
	<table style="width:33.5cm" border="0" cellspacing="0" cellpadding="3">
		<tr>
    		<td colspan="2" align="left" valign="top">HALAMAN : 1</td>
			<td width="54%" rowspan="2" align="center" valign="top">PEMERINTAH PROVINSI JAWA TENGAH<br />REKAPITULASI IURAN TPP TEMPAT BERTUGAS BPJS KESEHATAN UNTUK PARA PEGAWAI / PEKERJA<br />
  																	KODE LOKASI : <? echo $kode; ?>&nbsp;&nbsp;&nbsp;<? echo $strLokasi; ?></td>
			<td width="18%" rowspan="2" align="center" valign="top">&nbsp;</td>
		</tr>
		<tr align="left">
		  <td width="12%" valign="bottom">BAGIAN BULAN<br />KODE REKENING </td>
		  <td width="16%" valign="top">: <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?></td>
      </tr>
	</table>
	<table cellpadding="0" cellspacing="0" border="0" style="border-top:dashed 1px #000000;width:33.5cm">
	  <tr>
			<td width="7%" rowspan="2" >GOL. /<br />RUANG</td>
			<td width="7%" rowspan="2" align="center" class="borBothBottom">JUMLAH<br />PEGAWAI</td>
			<td width="9%" rowspan="2" align="center" class="borBothBottom">INSENTIP</td>
            <td width="9%" rowspan="2" align="center" class="borBothBottom">JUMLAH <br />TPP TEMPAT BERTUGAS KOTOR</td>
            <td width="9%" rowspan="2" align="center" class="borRight">POTONGAN<br />BPJS KESEHATAN (1%)</td>
			<td width="9%" rowspan="2" align="center" class="borRight">JUMLAH<br />TPP TEMPAT BERTUGAS BERSIH</td>
			<td width="9%" rowspan="2" align="center" class="borRight">IURAN<br />BPJS KESEHATAN (4%)</td>
	  </tr>
		<tr>
	  </tr>
		<tr align="center">
			<td class="borTopBottom">1</td>
			<td width="7%" class="borTopBottom">2</td>
			<td width="9%" class="borTopBottom">3</td>
			<td width="9%" class="borTopBottom">4</td>
            <td width="9%" class="borTopBottom">5</td>
			<td width="9%" class="borTopBottom">6</td>
			<td class="borTopBottom">7</td>
			<!-- <td class="borTopBottom">8</td> -->
		</tr>
		<tr align="center">
			<td colspan="8">&nbsp;</td>
		</tr>
	<?
	// $prefixes = ORM::factory('golongan')->group_by('prefix')->find_all();
    $this->db->group_by('prefix');
    $prefixes = $this->db->get($tb_golongan)->result();
	foreach($prefixes as $prefix) {
		// $golongans = ORM::factory('golongan')->where('prefix','=',$prefix->prefix)->find_all();
        // $this->db->where('prefix', $prefix->id);
        $this->db->like('id', substr($prefix->id,0,1), 'AFTER');
        $golongans = $this->db->get($tb_golongan)->result();
		foreach($golongans as $golongan) {	
			$sql = 
            "SELECT count(nip) AS pegawai, 
            sum(tempat) AS insentip, 
            sum(tempat_nominal) AS tpp, 
            sum(tempat_pot_bpjs) AS potongan_tpp_bpjs, 
            sum(tempat_pph) AS pph,
            sum(tempat_tun_bpjs) AS askes_tpp
			FROM $tb_kalkulasi
			WHERE LEFT(lokasi_gaji_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND tempat_stop = 0 AND golongan_id = ".$golongan->id;	
			
			// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
            $sqls = $this->db->query($sql)->result();
			foreach($sqls as $row) {
				?>
				<tr>
					<td valign="top"><? echo "GOL. ".$golongan->kode; ?></td>
					<td align="center" width="7%" valign="top"><? echo numFormat($row->pegawai); ?></td>
					<td align="right" width="9%" valign="top"><? echo numFormat($row->insentip); ?></td>
                    <td align="right" width="9%" valign="top"><? echo numFormat($row->insentip); ?></td>
                    <td align="right" width="9%" valign="top"><? echo numFormat($row->potongan_tpp_bpjs); ?></td>
					<td align="right" valign="top"><? echo numFormat($row->tpp); ?></td>
					<td align="right" valign="top"><? echo numFormat($row->askes_tpp); ?></td>
				</tr>
			<?
			}
		}
		
		$sql = 
        "SELECT count(nip) AS pegawai, 
        sum(tempat) AS insentip, 
        sum(tempat_nominal) AS tpp, 
        sum(tempat_pot_bpjs) AS potongan_tpp_bpjs, 
        sum(tempat_pph) AS pph,
        sum(tempat_tun_bpjs) AS askes_tpp
        FROM $tb_kalkulasi
		WHERE LEFT(lokasi_gaji_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND  tempat_stop = 0 AND LEFT(golongan_id,1) = ".substr($prefix->id,0,1);	
		
		// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
        $sqls = $this->db->query($sql)->result();
		foreach($sqls as $row) {		
			?>
            <tr>
				<td colspan="7"><hr /></td>
			</tr>
			<tr>
				<td valign="top"><? echo "JML.GOL.".$prefix->prefix; ?></td>
				<td align="center" valign="top" width="7%"><? echo numFormat($row->pegawai); ?></td>
				<td align="right" valign="top" width="9%"><? echo numFormat($row->insentip); ?></td>
                <td align="right" width="9%" valign="top"><? echo numFormat($row->insentip); ?></td>
                <td align="right" width="9%" valign="top"><? echo numFormat($row->potongan_tpp_bpjs); ?></td>
				<td align="right" valign="top"><? echo numFormat($row->tpp); ?></td>
				<td align="right" valign="top"><? echo numFormat($row->askes_tpp); ?></td>
			</tr>
			<tr align="center">
				<td colspan="7" height="5"><hr /></td>
			</tr>
		<?
		}	
	}
	
	$sql = 
    "SELECT count(nip) AS pegawai, 
    sum(tempat) AS insentip, 
    sum(tempat_nominal) AS tpp, 
    sum(tempat_pot_bpjs) AS potongan_tpp_bpjs, 
    sum(tempat_pph) AS pph,
    sum(tempat_tun_bpjs) AS askes_tpp
    FROM $tb_kalkulasi
	WHERE LEFT(lokasi_gaji_kode,2)='".substr($lokasi->kode,0,2)."' AND periode = '".$periode."' AND tempat_stop = 0";	
	
	// $sqls = DB::query(Database::SELECT, $sql)->as_object()->execute();
    $sqls = $this->db->query($sql)->result();
	foreach($sqls as $row) {	
		?>
		<tr>
            <td valign="top">TOTAL</td>
           <td align="center" valign="top" width="7%"><? echo numFormat($row->pegawai); ?></td>
				<td align="right" valign="top" width="9%"><? echo numFormat($row->insentip); ?></td>
                <td align="right" width="9%" valign="top"><? echo numFormat($row->insentip); ?></td>
                <td align="right" width="9%" valign="top"><? echo numFormat($row->potongan_tpp_bpjs); ?></td>
				<td align="right" valign="top"><? echo numFormat($row->tpp); ?></td>
				<td align="right" valign="top"><? echo numFormat($row->askes_tpp); ?></td>
        </tr>
		<?
	}
	?>
    	<tr align="center">
            <td colspan="7" height="5"><hr /></td>
        </tr>
	</table>
<?	
}
?>
</div>
</body>
</html>