<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="<?=base_url('dashboard')?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                    <span class="breadcrumb-item active">Update Password</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="row">
            <div class="col-md-6 offset-md-3">
                <!-- Basic table -->
                <div class="card">
                    <!-- <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data</h5>
                        <div class="header-elements">
                            <a href="<?=base_url('pengumuman/tambah')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                        </div>
                    </div> -->

                    <div class="card-body">
                        <div class="alert bg-info text-white alert-styled-left alert-dismissible">
                            <!-- <button type="button" class="close" data-dismiss="alert"><span>×</span></button> -->
                            <span class="font-weight-semibold">Update Password</span>
                        </div>
                        <form action="<?=base_url('setting/ganti_password_action')?>" method="post">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Password Lama</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="password" name="lama" id="lama" class="form-control">
                                    </div>
                                    
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Password Baru</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="password" name="baru" id="baru" class="form-control">
                                    </div>
                                    
                                </div>

                                <br/>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Ulangi Password Baru</label>
                                    </div>
                                    <div class="col-md-8">
                                       <input type="password" name="rebaru" id="rebaru" class="form-control">
                                       <span id='message'></span>
                                    </div>
                                    
                                </div>
                            </div>

                            


                            

                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /basic table -->
            </div>
        </div>

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        var tabel = $('#tabledt').DataTable({

            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: null, sortable : false, searceable : false },
                { data: 'pengumuman' },
                { data: 'waktu' },
                { data: 'aksi', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

    });
</script>

<script>
                                $('#baru, #rebaru').on('keyup', function () {
                                if ($('#baru').val() == $('#rebaru').val()) {
                                    $('#message').html('Matching').css('color', 'green');
                                } else 
                                    $('#message').html('Not Matching').css('color', 'red');
                                });
                            </script>

<script>
    function hapusData(id){
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });
        
        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax');
                
                $.ajax({
                    url: "<?=base_url('pengumuman/hapus_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>