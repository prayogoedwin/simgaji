
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                    <?=$breadcrumb?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><?=$title?></h5>
                <div class="header-elements">
                    <!-- <a href="<?=base_url('pengumuman/tambah')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                    <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#mdlAdd" >Tambah <b><i class="icon-file-plus2"></i></b></button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead>
                        <tr >
                            
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Role</th>
                            <th>SKPD Yang Dikelola</th>

                            <!-- <th>SKPD Yang Diampu (Simpeg)</th> -->
                         
                            
                            <!-- <th width="10px"><input type="checkbox" id="check-all" ></th> -->
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<!-- <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script> -->
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

   


</div>
<!-- /main content -->


<!-- START MODAL DETAIL -->
<div id="mdlDetail" class="modal fade" >
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail Verifikator</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">

                        <div class="form-group row col-md-12 silek">
                                <label>Role</label>
                                <?php echo form_dropdown('role_edit', role_array('Pilih Role'), '', ' id="role_edit" class="form-control" required disabled'); ?>
                            </div>

                           
                            <!-- <hr> -->
                            <div class="form-group row col-md-12">
                                <label>NIP / Username</label>
                                <input type="hidden" readonly class="form-control" name="id" id="id" required autocomplete="off">
                                <input type="text" readonly class="form-control" name="nip" id="nip" required autocomplete="off">
                            </div>
                            <!-- <div class="form-group row col-md-12">
                                <label>SKPD yang diampu</label>
                                <input type="text" readonly class="form-control" name="kode_skpd" id="kode_skpd" required autocomplete="off">
                            </div> -->                    

                            <div class="form-group row col-md-12"  id="skpd_div">

                                <label>Tambah SKPD yang dikelola</label>
                                <?php echo form_multiselect('kode_skpd_edit[]', opd_gaji_array('', 'id', '2'), '', ' id="kode_skpd_edit" class="form-control select2" multiple="multiple" style="width:100%;" required'); ?>
                              
                            </div>

                          

                        </div>
                    

                        <div class="modal-footer">
                            <button type="submit" id="btn_update" class="btn btn-md btn-success">Update</button>
                            
                            <a href="javascript:void(0)" onclick="hapusData()" id="btn_del" class="btn btn-md btn-danger" style="display:none">Hapus</a>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->


        <style>
            #hidden_div {
    display: none;
}
        </style>
        <!-- START MODAL DETAIL -->
<div id="mdlAdd" class="modal fade" >
            <div class="modal-dialog modal-full">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Add Verifikator</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="id" id="id">
                            <!-- <hr> -->

                            <div class="form-group row col-md-12">
                                <label>Role</label>
                                <?php echo form_dropdown('role', role_array('Pilih Role'), '', ' id="role" class="form-control custom-select select2" onchange="IsChange()" required '); ?>
                            </div>

                            <div class="form-group row col-md-12" id="hidden_div">
                                <label>Username</label>  
                                <input type="text" id="username_verifikator" class="form-control" name="username_verifikator">
                            </div>


                            <div class="form-group row col-md-12" id="nama_div">
                                <label>Nama/NIP</label>
                                <select class="form-control" name="nipbos" id="nipbos" style="width:100%!important;" required data-fouc></select>
                                <input type="hidden" id="nip_verifikator" name="nip_verifikator">
                                <input type="hidden" id="nama_verifikator" name="nama_verifikator">
                            </div>

                            <div class="form-group row col-md-12" id="pass_div">
                                <label>Password</label>  
                                <input type="text" id="password" class="form-control" name="password">
                            </div>

                            
                           
                            <!-- <div class="form-group row col-md-12">
                                <label>Tambah SKPD yang diampu</label>
                                <?php echo form_multiselect('kode_skpd_add[]', opd_cabdin_array(NULL), '', ' id="kode_skpd_add" data-fouc class="form-control select2"  style="width:100%; height:100%" required'); ?>
                            </div> -->

                            <div class="form-group row col-md-12" id="show_div">
                                <label>Tambah SKPD yang dikelola</label>
                                <?php echo form_multiselect('kode_skpd_add_[]', opd_gaji_array('', 'id', '2'), '', ' id="kode_skpd_add" data-fouc class="form-control select2 parent_filter_select2"   style="width:100%; height:100%" required'); ?>
                            </div>

                            
                        </div>
                    

                        <div class="modal-footer">
                           
                            <button type="submit" id="btn_add" class="btn btn-md btn-success" >Add</button>
                    
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->

</div>
<!-- /page content -->

<?php $this->load->view('verifikator/js');?>

<script>

function IsChange() {
    var role = $("#role").val();
    if(role == 1){
        $("#show_div").hide();
        $("#hidden_div").show();
    }else if(role == 2){
        $("#show_div").show();
        $("#hidden_div").hide();
        $("#pass_div").show();
    }else if(role == 3){
        $("#show_div").hide();
        $("#hidden_div").show();
        $("#skpd_div").hide();
        $("#nama_div").hide();
        $("#pass_div").show();
    }
}

</script>







      

</body>
</html>