<script type="text/javascript" src="https://cdn.rawgit.com/ashl1/datatables-rowsgroup/fbd569b8768155c7a9a62568e66a64115887d7d0/dataTables.rowsGroup.js"></script>
<script>
    $(function() {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                },
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
            }
        });

        var tabel = $('#tabledt').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'Semua']
            ],
            // 'rowsGroup': [0],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                     'pdfHtml5'
                    // {
					// 	text: 'Hapus',
					// 	className: 'btn',
					// 	action: function(e, dt, node, config) {
					// 		bulk_delete();
					// 	}
					// },
                ]
            },
            "oLanguage": {
                "sEmptyTable": "Data Tidak Ditemukan",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> ',
            },
            "pageLength": 10,
            'destroy': true,
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': false,
            "serverSide": true,
            "ajax": {
                "url": "<?= base_url('log_verifikator/get_log_pegawai') ?>",
                "type": "POST",
                "data": function ( data ) {
                data.nip_ybs = $('#nip_ybs').val();
                data.verifikator = $('#verifikator').val();
                data.created_at = $('#created_at').val();
                data.periode = $('#periode').val();
                }
            },
            'columns': [
                {
                    data: 'ybs'
                },
                {
                    data: 'lama'
                },
                {
                    data: 'baru'
                },
                {
                    data: 'verfikator'
                },
                {
                    data: 'created_at'
                },
                {
                    data: 'periode'
                },
                {
                    data: 'perubahan'
                },
                // {
                //     data: 'id'
                // }
            ],

            columnDefs: [{}],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });

    $('#btn-filter').click(function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#nip_ybs').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });
    $('#verifikator').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });
    $('#created_at').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#periode').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    // $('#nip_s').on('change', function(){ //button filter event click
    //     // table.ajax.reload();  //just reload table
    //     $('#tabledt').DataTable().ajax.reload(null, false);
    // });

    // $('#nama_s').on('change', function(){ //button filter event click
    //     // table.ajax.reload();  //just reload table
    //     $('#tabledt').DataTable().ajax.reload(null, false);
    // });

    // $('#lokasi_kerja_s').on('change', function(){ //button filter event click
    //     // table.ajax.reload();  //just reload table
    //     $('#tabledt').DataTable().ajax.reload(null, false);
    // });

    // $('#lokasi_gaji_s').on('change', function(){ //button filter event click
    //     // table.ajax.reload();  //just reload table
    //     $('#tabledt').DataTable().ajax.reload(null, false);
    // });

   
</script>


