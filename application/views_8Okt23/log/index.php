<style>
    table.dataTable tbody td {vertical-align: top;}
    </style>
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">
    

    <!-- Page header -->
    <div class="page-header page-header-light">
        <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                    <?=$breadcrumb?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->



    



    <!-- Content area -->
    <div class="content">
   

     <!-- Form inputs -->
     
     <div class=" ">


<div class="">
    <!-- <form action="#"> -->
    <form id="form-filter" class="form-horizontal">


    

    <div class="form-group row" hidden>
       

    <?php 
    $nip =  $this->session->userdata('B_02B');
    $x = $this->db->query("SELECT * FROM simgaji_verifikator WHERE nip = '$nip' AND deleted_at IS NULL");
    if($x->num_rows() > 0){
        $x = $x->row();
        $role = $x->type_role;
        $skpd = $x->kode_skpd;
        // echo $skpd;
    }else{
        $role = '';
        $skpd = '';
        
    }

    ?>
      
        <div class="col-lg-3">
            <?php 
            echo form_dropdown('lokasi_kerja', opd_array_fix_array('Lokasi Kerja', 'kode', $role, $skpd), '', ' id="lokasi_kerja" class="form-control custom-select select2"   required');
            ?>
        </div>

        <div class="col-lg-3" >
        <?php 
            echo form_dropdown('lokasi_gaji', opd_array_fix_array('Lokasi Gaji', 'kode', $role, $skpd), '', ' id="lokasi_gaji" class="form-control custom-select select2"   required');
            ?>

        </div>

        <div class="col-lg-3">
        <div class="text-left">
        <button type="button" id="btn-filter" class="btn btn-primary pull-left"> <i class="fa fa-save"></i> Cari </button>
        </div>

        </div>

    </div>

    </form>
  
</div>

</div>
<!-- /form inputs -->
        
        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title"><?=$title?></h5>
                <div class="header-elements">
                    <!-- <a href="<?=base_url('masterpegawai_p3ks/add')?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                    <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#mdlAdd" >Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                    <!-- <table class="table">
                        <tr>
                            <th><i  class="icon-cog"></i> = Belum diproses</th>
                            <th><i style="color:red" class="icon-cancel-circle2"></i> = Ditolak</th>
                            <th><i style="color:green" class="icon-checkmark-circle"></i> = Disetujui</th>
                        </tr>
                    </table> -->
                    
                </div>
            </div>

           

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead >
                        <tr>
                            
                            <th >NIP YBS</th>
                            <th >Lama</th>
                            <th >Baru</th>
                            <th >Verifikator</th>
                            <th >Waktu Perubahan</th>
                            <th >Periode Perubahan</th>
                            <th >Perubahan Pada</th>
                            

                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>

                    <tfoot >
                        <tr>
                            
                            <th ><input id="nip_ybs" name="nip_ybs" placeholder="Cari NIP YBS"></th>
                            <th >Lama</th>
                            <th >Baru</th>
                            <th ><?php 
                                echo form_dropdown('verifikator', verifikator_array('Pilih Verifikator'), '', ' id="verifikator" class="form-control custom-select select2"   required');
                                ?></th>
                            <th ><input type="date" id="created_at" name="created_at" placeholder="Cari Berdasarkan Tanggal"></th>
                            <th ><input type="date" id="periode" name="periode" placeholder="Cari Periode"></th>
                            <!-- <th ><input type="text" id="perubahan" name="perubahan" placeholder="Cari Perubahan Pada"></th> -->
                            <th >Perubahan Pada</th>
                            

                        </tr>
                    </tfoot>
                   

                    

                   
                </table>
            </div>
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
   


</div>
<!-- /main content -->

</div>
<!-- /page content -->

<?php $this->load->view('log/js');?>

<script>
//   $('#lokasi_gaji').on('change', function() {
//     var lok = $("#lokasi_gaji").val();
    
//     alert(this.value)
//   });

</script>







      

</body>
</html>