<?php
$A_01 = $this->session->userdata('A_01');
$A_02 = $this->session->userdata('A_02');
$A_03 = $this->session->userdata('A_03');
$A_04 = $this->session->userdata('A_04');
$A_05 = $this->session->userdata('A_05');

$jenis = $this->input->get('jns', TRUE);
?>

<!-- Page content -->
<div class="page-content">
    
    <!-- Main content -->
    <div class="content-wrapper">
        
        <!-- Page header -->
        <div class="page-header page-header-light">
            
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                        <a href="<?=base_url('pegawai_p3k')?>" class="breadcrumb-item">TPP Pegawai P3K</a>
                        <span class="breadcrumb-item active">Detail  (<?=$detail->name;?> - <?=$detail->nip;?>)</span>
                    </div>
                    
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                
                
            </div>
        </div>
        <!-- /page header -->
        
        
        <!-- Content area -->
        <div class="content">
            
            <!-- Form layouts -->
            <div class="row">
                
                <div class="col-md-12">
                    <div class="row">
                        <!-- Vertical form -->
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header header-elements-inline">
                                    <h5 class="card-title">Data</h5>
                                    <div class="header-elements">
                                        <div class="list-icons">
                                            <a class="list-icons-item" data-action="collapse"></a>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="card-body">
                                    <!-- <form action="<?=base_url('pegawai_p3k/detail_action')?>" method="post" onsubmit="return confirm('Apakah anda sudah yakin data yang akan diproses sudah benar?');"> -->
                                        <?php echo form_open('masterpegawai_p3ks/update_action'); ?>
                                        <input type="hidden" name="id" id="idne" value="<?=$detail->id;?>">
                                        
                                        <?php foreach ($kodes as $key => $value) :?>
                                        <?php
                                        // $cb_index = '<input type="checkbox" name="kode_id[]" id="kode_id" value="'.$value->id.'">';
                                        $optionEl = '';
                                        $labelform = $value->name;
                                        $feld = $value->field;
                                        if($value->input == '1'){
                                            $elementForm = '<input type="'. $value->source .'" class="form-control" name="'. $value->field .'" value="' . $detail->$feld .'" autocomplete="off">';
                                        } else if($value->input == '2'){
                                            $elementForm = '<textarea rows="4" cols="4" class="form-control" name="'. $value->field .'">'. $detail->$feld .'</textarea>';
                                        } else {
                                            $dataSource = $value->source;
                                            $elementFormSelOpen = '<select class="form-control select-search" name="'. $value->field .'" id="'. $value->field .'">';
                                                foreach ($$dataSource as $ki => $val) {
                                                    $kon = '';
                                                    if($val->id == $detail->$feld){
                                                        $kon = "selected";
                                                    }
                                                    
                                                    $optionEl .= '<option value="'. $val->id .'"'. $kon .'>' . $val->name . '</option>';
                                                }
                                                $elementFormSelClose = '</select>';
                                                
                                                $elementForm = $elementFormSelOpen . $optionEl . $elementFormSelClose;
                                            }
                                            ?>
                                            
                                            <div class="form-group">
                                                <label for=""><?=$labelform;?></label>
                                                <!-- <?=$cb_index?> -->
                                                <?=$elementForm;?>
                                            </div>
                                            
                                            <?php endforeach; ?>
                                            
                                            <?php if($this->session->userdata('role') == 1 || $this->session->userdata('role') == 2){ ?>
                                                
                                                
                                                
                                                
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-info text-right">Simpan <i class="icon-checkmark2 ml-1"></i></button>
                                                    <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                                                </div> 
                                                
                                                
                                                
                                                <?php } ?>
                                                <!-- </form> -->
                                                <?php echo form_close(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /vertical form -->
                                </div>
                                
                            </div>
                        </div>
                        <!-- /form layouts -->
                        
                    </div>
                    <!-- /content area -->
                    
                    
                    
                    
                    <?php $this->load->view('template/footer_custom');?>
                    
                    
                    <script src="<?=base_url('assets/')?>global_assets/js/plugins/forms/selects/select2.min.js"></script>
                    <script src="<?=base_url('assets/')?>global_assets/js/demo_pages/form_select2.js"></script>
                    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
                </div>
                <!-- /main content -->
                
            </div>
            <!-- /page content -->
            
            <script>
                
                if (typeof Noty == 'undefined') {
                    console.warn('Warning - noty.min.js is not loaded.');
                }
                
                // Override Noty defaults
                Noty.overrideDefaults({
                    theme: 'limitless',
                    layout: 'topRight',
                    type: 'alert',
                    timeout: 2500
                });
                
                if ('<?=$this->session->userdata("status");?>' == 'error') {
                    new Noty({
                        text: '<?=$this->session->userdata("message")?>',
                        type: 'error'
                    }).show();
                } else if ('<?=$this->session->userdata("status");?>' == 'success') {
                    new Noty({
                        text: '<?=$this->session->userdata("message")?>',
                        type: 'success'
                    }).show();
                }
            </script>
            
            <script>
                function hapusData(){
                    
                    var url = document.location.origin+'/simgaji/masterpegawai_p3ks';
                    
                    
                    var id = $('#idne').val();
                    
                    
                    var swalInit = swal.mixin({
                        buttonsStyling: false,
                        confirmButtonClass: 'btn btn-primary',
                        cancelButtonClass: 'btn btn-light'
                    });
                    
                    swalInit.fire({
                        title: 'Apakah anda yakin?',
                        text: "Anda akan menghapus data ini?",
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak!',
                        confirmButtonClass: 'btn btn-success',
                        cancelButtonClass: 'btn btn-danger',
                        buttonsStyling: false
                    }).then(function(result) {
                        if(result.value) {
                            // alert('proses ajax');
                            
                            $.ajax({
                                url: "<?=base_url('masterpegawai_p3ks/hapus_action')?>",
                                type: "POST",
                                data: {
                                    id : id
                                },
                                dataType: "json",
                                success: function (data) {
                                    // console.log(data);
                                    
                                    var sts = data['status'];
                                    var msg = data['message'];
                                    
                                    if(sts == 1){
                                        swalInit.fire({
                                            title: 'Yeay',
                                            text: msg,
                                            type: 'success',
                                            onClose: function() {
                                                // alert('Notification is closed.');
                                                // location.reload();
                                                window.location.replace(url);
                                            }
                                        });
                                    } else {
                                        swalInit.fire(
                                        'Cancelled',
                                        msg,
                                        'error'
                                        );
                                    }
                                    
                                },
                                error: function (xhr, ajaxOptions, thrownError) {
                                    swalInit.fire({
                                        title: 'Oppss',
                                        text: 'Terjadi kesalahan nih',
                                        type: 'error',
                                        onClose: function() {
                                            // alert('Notification is closed.');
                                            location.reload();
                                        }
                                    });
                                }
                            });
                        }
                        else if(result.dismiss === swal.DismissReason.cancel) {
                            // swalInit.fire(
                            //     'Cancelled',
                            //     'Your imaginary file is safe :)',
                            //     'error'
                            // );
                        }
                    });
                }
            </script>
            
        </body>
        </html>