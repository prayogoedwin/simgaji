<?php
$A_01 = $this->session->userdata('A_01');
$role = $this->session->userdata('role');
$lokasis_id = $this->session->userdata('id_lokasis');
?>
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">

        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <span class="breadcrumb-item active">TPP Potongan Pegawai</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <?php if($A_01 == 'D0') :?>
        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Sub Unitkerja</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <!-- <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a> -->
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form action="" method="get">

                    <div class="form-group row col-md-12">
                        <div class="col-md-3">
                            <select class="form-control select2" name="kode" id="kode" required>
                                <option value="">::Pilih::</option>
                                <?php foreach ($subunit_disdik as $key => $value) :?>
                                    <option value="<?=$value->kode?>" <?php if($filter['kodedrop'] == $value->kode){echo "selected";} ?> ><?=$value->nama?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn bg-blue ml-3">Cari 
                            <!-- <i class="icon-search ml-2"></i> -->
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <?php endif; ?>

        <!-- Basic table -->
        <div class="card">
            <?php if($role == 8 || ($role == 3 && $this->session->userdata('A_01') == '86')) :?>
                <form action="<?=base_url('tpp_pot_pegawai/ajukan_tpp_actionBaru')?>" onsubmit="return confirm('Apakah anda sudah yakin data akan diajukan?');" method="post">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Data</h5>
                        <div class="header-elements">
                            <!-- <a href="<?=base_url('tpp_pegawai_p3k/ajukan_tpp_action')?>" class="btn btn-labeled btn-labeled-right bg-primary">Ajukan <b><i class="icon-paperplane"></i></b></a> -->
                            <?php if($periodetpp_simpeg): ?>
                                <button type="submit" class="btn btn-labeled btn-labeled-right bg-primary">Ajukan <b><i class="icon-paperplane"></i></b></button>
                            <?php else :?>
                                <label for="" class="badge badge-warning">Periode Usulan ditutup</label>
                            <?php endif; ?>
                        </div>
                    </div>
                    <small style="margin-left: 20px;">Total Data: <?=count($pegawais)?> Record</small>
                    <div class="table-responsive">
                        <table class="table table-stripe table-condensed" id="tableDefault">
                            <!-- <thead>
                                <tr>
                                    <th>No</th>
                                    <th>NIP <br>Nama <br>Jabatan</th>
                                    <th>Kelas Jab</th>
                                    <th>Cuti Sakit <br>(Hari)</th>
                                    <th>TB</th>
                                    <th>Hukdis</th>
                                    <th>SKP</th>
                                    <th>Alpha</th>
                                    <th>KWK</th>
                                    <th>Keterangan</th>
                                </tr>
                            </thead> -->
                            <tbody>
                                <tr>
                                    <td rowspan="2" align="center" bgcolor="#CCCCCC">No.</td>
                                    <td width="20%" rowspan="2" align="center" bgcolor="#CCCCCC">NIP<br>
                                        Nama<br>
                                        Pkt/Gol <br>
                                        Jab</td>
                                    <td rowspan="2" bgcolor="#CCCCCC">Kelas Jab</td>
                                    <td width="6%" rowspan="2" bgcolor="#CCCCCC">Cuti Sakit (Hari)</td>
                                    <td width="10%" rowspan="2" align="center" bgcolor="#CCCCCC">Tugas <br>Belajar</td>
                                    <td width="14%" rowspan="2" align="center" bgcolor="#CCCCCC">Nilai SKP K1-K5 / TB</td>
                                    <td colspan="3" align="center" bgcolor="#CCCCCC">Nilai Perilaku Kerja</td>
                                    <td width="11%" align="center" bgcolor="#CCCCCC">Hukuman Disiplin</td>
                                    <td rowspan="2" align="center" bgcolor="#CCCCCC">LHKPN<br>LHKASN<br>Grat<br>BMD<br>TPTGR</td>
                                    <td width="10%" rowspan="2" align="center" bgcolor="#CCCCCC">Keterangan</td>
                                </tr>
                                <tr>
                                    <td width="8%" align="center" bgcolor="#CCCCCC">K6 (Hari)</td>
                                    <td width="9%" align="center" bgcolor="#CCCCCC">K7 (Jam)</td>
                                    <td width="9%" align="center" bgcolor="#CCCCCC">K8 (PKSP)</td>
                                    <td align="center" bgcolor="#CCCCCC">K9-K11</td>
                                </tr>
                                <tr>
                                    <td align="center" bgcolor="#CCCCCC">1</td>
                                    <td align="center" bgcolor="#CCCCCC">2/3/4</td>
                                    <td align="center" bgcolor="#CCCCCC">5</td>
                                    <td align="center" bgcolor="#CCCCCC">6</td>
                                    <td align="center" bgcolor="#CCCCCC">7</td>
                                    <td align="center" bgcolor="#CCCCCC">8/9</td>
                                    <td align="center" bgcolor="#CCCCCC">10</td>
                                    <td align="center" bgcolor="#CCCCCC">11</td>
                                    <td align="center" bgcolor="#CCCCCC">12</td>
                                    <td align="center" bgcolor="#CCCCCC">13</td>
                                    <td align="center" bgcolor="#CCCCCC">14</td>
                                    <td align="center" bgcolor="#CCCCCC">15</td>
                                </tr>
                                <?php foreach ($pegawais as $key => $value) { ?>
                                    <tr>
                                        <td><?=$key+1;?></td>
                                        <td>
                                            <?=$value['B_02B'];?><br><?=$value['nama'];?><br><?=$value['pkt_gol']?><br><?=$value['jabatan'];?>
                                            <input type="hidden" value="<?=$value['jabatan']?>" class="form-control" name="jabatans[]">
                                        </td>
                                        <td>
                                            <div>
                                                <input type="hidden" name="basetpps[]" value="<?=$value['nominal_bebankerja']?>">
                                                <input type="hidden" name="tpp_bebankerjakhusus[]" value="<?=$value['nominal_bebankerjakhusus']?>">
                                                <input type="hidden" name="tpp_tempat[]" value="<?=$value['nominal_tempat']?>">
                                                <input type="hidden" name="tpp_kondisi[]" value="<?=$value['nominal_kondisi']?>">

                                                <input type="hidden" value="<?=$value['B_02B']?>" class="form-control" name="nips[]">
                                                <input type="hidden" value="<?=$value['F_03']?>" class="form-control" name="gols[]">

                                                <input type="text" name="kelasjabs[]" value="<?=$value['kelas_jabatan']?>" style="border: 0px; width: 20px;" readonly>
                                                <!-- <br><?=$value['rumpun']?> -->
                                            </div>
                                        </td>
                                        <td align="center">
                                            <?php if($value['cuti_2bulanlalu'] != 0) :?>
                                                <div class="alert bg-warning">
                                                    <span class="font-weight-semibold"><?=$value['cuti_2bulanlalu'];?></span>
                                                </div>
                                                <input type="hidden" class="form-control" name="sakit[]" value="<?=$value['cuti_2bulanlalu'];?>" readonly>
                                            <?php else :?>
                                                <input type="text" class="form-control" name="sakit[]" value="<?=$value['cuti_2bulanlalu'];?>">
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if($value['tb']) :?>
                                                <input type="checkbox" name="tb_<?php echo $value['B_02B'] ?>" value="1" checked>
                                            <?php else: ?>
                                                <input type="checkbox" name="tb_<?php echo $value['B_02B'] ?>" value="1">
                                            <?php endif;?>
                                        
                                        </td>
                                        <!-- <td>
                                            <div>
                                                <input type="text" value="0" class="form-control" name="potongan">
                                            </div>
                                        </td> -->
                                        <td>
                                            <div>
                                                <!-- <input type="text" name="skp[]" class="form-control" value="100"> -->
                                                <select class="form-control" name="skp[]">
                                                    <option value="100" selected="">100%</option>
                                                    <option value="95" >95%</option>
                                                    <option value="85" >85%</option>
                                                    <option value="75" >75%</option>
                                                    <option value="50" >50%</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td align="center">
                                            <?php if($value['alpha'] != null) :?>
                                                <input type="text" name="alphas[]" value="<?=$value['alpha']?>" style="border: 0px; width: 20px;" readonly>
                                            <?php else :?>
                                                <input type="text" name="alphas[]" value="0" style="border: 0px; width: 20px;" readonly>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            <?php if($value['kwk'] != null) :?>
                                                <input type="text" name="kwks[]" value="<?=$value['kwk']?>" style="border: 0px; width: 30px;" readonly>
                                            <?php else :?>
                                                <input type="text" name="kwks[]" value="0.00" style="border: 0px; width: 30px;" readonly>
                                            <?php endif; ?>
                                        </td>
                                        <td align="center">
                                            0
                                        </td>
                                        <td>
                                            <div>
                                                <select class="form-control" name="hukdis[]">
                                                    <option value="100" <?php if($value['kena_hk'] == '0'){echo "selected";} ?>>Tidak</option>
                                                    <option value="90" <?php if($value['tingkat_hukdis'] == '1' && $value['kena_hk'] == '1'){echo "selected";}?>>Ringan</option>
                                                    <option value="80" <?php if($value['tingkat_hukdis'] == '2' && $value['kena_hk'] == '1'){echo "selected";}?>>Sedang</option>
                                                    <option value="50" <?php if($value['tingkat_hukdis'] == '3' && $value['kena_hk'] == '1'){echo "selected";}?>>Berat</option>
                                                </select>
                                            </div>
                                        </td>
                                        <td align="center">
                                            <div>
                                                <input type="text" name="lhkpns[]" value="<?=$value['lhkpn']?>" style="border: 0px; width: 30px;" readonly>
                                                <input type="text" name="lhkasns[]" value="<?=$value['lhkasn']?>" style="border: 0px; width: 30px;" readonly>
                                                <input type="text" name="tptgrs[]" value="<?=$value['tptgr']?>" style="border: 0px; width: 30px;" readonly>
                                                <input type="text" name="kenabmds[]" value="<?=$value['kenabmd']?>" style="border: 0px; width: 30px;" readonly>
                                                <input type="text" name="kenagrats[]" value="<?=$value['kenagrat']?>" style="border: 0px; width: 30px;" readonly>
                                                <input type="hidden" name="xbmds[]" value="<?=$value['xbmd']?>">
                                                <input type="hidden" name="xgrats[]" value="<?=$value['xgrat']?>">
                                            </div>
                                        </td>
                                        <td>
                                            <input type="text" name="keterangan[]" class="form-control" value="<?=($value['kena_hk']=='1') ? $value['uraian'] : '' ?>">
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </form>
            <?php endif; ?>
            
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $('#btn_update').on('click',function(){
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        var id = $('#idne').val();
        var golongan = $('#golongan').val();
        var potongan = $('#potongan').val();

        $.ajax({
            type : "POST",
            url  : "<?php echo base_url('potongan/update_action')?>",
            data : {
                id : id,
                golongan : golongan,
                potongan : potongan
            },
            dataType : "JSON",
            success: function(data){

                var sts = data['status'];
                var msg = data['message'];

                if(sts == 1){
                    $('[name="idne"]').val("");
                    $('[name="golongan"]').val("");
                    $('[name="potongan"]').val("");
                    $('#mdlDetail').modal('hide');

                    $('#tabledt').DataTable().ajax.reload(null, false);
                    swalInit.fire({
                        title: 'Yeay',
                        text: msg,
                        type: 'success'
                    });
                } else {
                    swalInit.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            // alert('Notification is closed.');
                            location.reload();
                        }
                    });
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {
                    swalInit.fire({
                        title: 'Oppss',
                        text: 'Terjadi kesalahan nih',
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
            }
        });
        return false;
    });
</script>

<script>
    function hapusData(){
        // alert(id);

        // var nilaine_bos = $('input[name="nilai_bos"]')[row].value; // First

        var id = $('#idne').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {
                // alert('proses ajax');

                $.ajax({
                    url: "<?=base_url('potongan/hapus_action')?>",
                    type: "POST",
                    data: {
                        id : id
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }
            else if(result.dismiss === swal.DismissReason.cancel) {
                // swalInit.fire(
                //     'Cancelled',
                //     'Your imaginary file is safe :)',
                //     'error'
                // );
            }
        });
    }
</script>

<script>
function confirmAjukan(){
    alert('jal ajukan');
}
</script>

<script>

    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>
