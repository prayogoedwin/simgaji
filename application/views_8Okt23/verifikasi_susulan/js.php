<?php 
$periode = $this->uri->segment(3);
?>

<script>
    $(function() {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                }
            }
        });

        var tabel = $('#tabledt').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'Semua']
            ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn '
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    // 'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                    {
                        extend: 'colvis',
                        className: '',
                        text: '',
                        exportOptions: {
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
                        extend: 'excelHtml5',
                        className: 'btn-light',
                        text: '<i class="fa fa-download"></i> <i class="icon-file-excel"></i>',
                        exportOptions: {
                            modifier: {
                                page: 'current'
                            }
                        }
                    },
                    {
						text: '<i class="icon-checkmark-circle"></i>',
						className: 'btn-success',
						action: function(e, dt, node, config) {
							bulk_acc();
						}
					},
                    {
						text: '<i class="icon-cancel-circle2"></i>',
						className: 'btn-danger',
						action: function(e, dt, node, config) {
							bulk_tolak();
						}
					},
                ],
                
            },
            "oLanguage": {
                "sEmptyTable": "Data Tidak Ditemukan"
            },
            "pageLength": 10,
            'destroy': true,
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': false,
            "serverSide": true,
            "ajax": {
                "url": "<?= base_url('verifikasi_susulan/get_verifikasi/'.$this->uri->segment(3)) ?>",
                "type": "POST",
                "data": function ( data ) {
                data.lokasi_kerja = $('#lokasi_kerja').val();
                data.lokasi_gaji = $('#lokasi_gaji').val();
                // data.nip_s = $('#nip_s').val();
                // data.nama_s = $('#nama_s').val();
                // data.lokasi_kerja_s = $('#lokasi_kerja_s').val();
                // data.lokasi_gaji_s = $('#lokasi_gaji_s').val();
            }
            },


            'columns': [
                {  data: null,"sortable": false, 
                    render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                    }  
                },
                {
                    data: 'nip'
                },
                {
                    data: 'nama'
                },
                {
                    data: 'lokasikerja'
                },
                {
                    data: 'lokasigaji'
                },
                {
                    data: 'periode_start'
                },
                {
                    data: 'periode_end'
                },
                {
                    data: 'lama'
                },
                {
                    data: 'baru'
                },
                {
                    data: 'jumlah_bulan'
                },
                {
                    data: 'kekurangan'
                },
                {
                    data: 'tunjangan_istri', "visible": false
                },
                {
                    data: 'tunjangan_anak', "visible": false
                },
                {
                    data: 'jumlah_tunjangan_keluarga', "visible": false
                },
                {
                    data: 'tunjangan_umum', "visible": false
                },
                {
                    data: 'tunjangan_umum_tambahan', "visible": false
                },
                {
                    data: 'tunjangan_struktural', "visible": false
                },
                {
                    data: 'tunjangan_fungsional', "visible": false
                },
                {
                    data: 'tunjangan_beras', "visible": false
                },
                {
                    data: 'tunjangan_pph', "visible": false
                },
                {
                    data: 'pembulatan', "visible": false
                },
                {
                    data: 'status_verifikasi_kepala'
                },
                {
                    data: 'status_verifikasi_bkd'
                },
                {
                    data: 'jenis_susulan'
                },
                {
                    data: 'id', "sortable": false, 
                }
            ],

            // columnDefs: [{}],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });

    $('#btn-filter').click(function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#nip_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#nama_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#lokasi_kerja_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });

    $('#lokasi_gaji_s').on('change', function(){ //button filter event click
        // table.ajax.reload();  //just reload table
        $('#tabledt').DataTable().ajax.reload(null, false);
    });
</script>

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?= $this->session->userdata("status"); ?>' == 'error') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'error'
        }).show();
    } else if ('<?= $this->session->userdata("status"); ?>' == 'success') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

<script>
	//check all
	$("#check-all").click(function() {
		$(".data-check").prop('checked', $(this).prop('checked'));
	});

	function bulk_acc() {
		var list_id = [];
		$(".data-check:checked").each(function() {
			list_id.push(this.value);
		});

		if (list_id.length > 0) {
            
			Swal.fire({
				title: 'Anda yakin ingin acc?',
				// text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then((result) => {
                // alert(result.isConfirmed)
				// if (result.isConfirmed) {
                if (result) {
                    
					$.ajax({
						type: "POST",
						data: {
							id: list_id
						},
						url: "<?php echo site_url('verifikasi_susulan/acc_action') ?>",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
                                
								Swal.fire({
									icon: 'success',
									title: 'Berhasil Acc',
									showConfirmButton: false,
									timer: 1000
								})
								$('#tabledt').DataTable().ajax.reload(null, false);
							} else {
								// alert('Failed.');
								Swal.fire({
									icon: 'danger',
									title: 'Gagal Acc',
									showConfirmButton: false,
									timer: 1000
								})
							}

						},
						error: function(jqXHR, textStatus, errorThrown) {
							// alert('Error acc data');
							Swal.fire(
								'Gagal!',
								'Gagal Acc',
								'error'
							)
						}
					});

				}

			})
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Tidak Ada Data Dipilih',
				showConfirmButton: false,
				timer: 1000
			})
			// alert('no data selected');
		}
	}

    function bulk_tolak() {
		var list_id = [];
		$(".data-check:checked").each(function() {
			list_id.push(this.value);
		});

		if (list_id.length > 0) {
            
			Swal.fire({
				title: 'Anda yakin ingin acc?',
				// text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then((result) => {
                // alert(result.isConfirmed)
				// if (result.isConfirmed) {
                if (result) {
                    
					$.ajax({
						type: "POST",
						data: {
							id: list_id
						},
						url: "<?php echo site_url('verifikasi_susulan/tolak_action') ?>",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
                                
								Swal.fire({
									icon: 'success',
									title: 'Berhasil Tolak',
									showConfirmButton: false,
									timer: 1000
								})
								$('#tabledt').DataTable().ajax.reload(null, false);
							} else {
								// alert('Failed.');
								Swal.fire({
									icon: 'danger',
									title: 'Gagal Tolak',
									showConfirmButton: false,
									timer: 1000
								})
							}

						},
						error: function(jqXHR, textStatus, errorThrown) {
							// alert('Error acc data');
							Swal.fire(
								'Gagal!',
								'Gagal Tolak',
								'error'
							)
						}
					});

				}

			})
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Tidak Ada Data Dipilih',
				showConfirmButton: false,
				timer: 1000
			})
			// alert('no data selected');
		}
	}
</script>

<script>
    function detailData(id) {
        $.ajax({
            url: "<?= base_url('verifikasi_susulan/detail') ?>",
            type: "POST",
            data: {
                id: id
            },
            dataType: "json",
            success: function(data) {

                var sts = data['status'];
                var msg = data['message'];
                var dt = data['data'];
          
                if (sts == 1) {
                    $('#mdlDetail').modal('show');

                    $('#id').val(id)

                    if(dt['jenis_susulan'] == 1){
                        $('#jenis_susulan').val('Gaji Pokok Tanpa Perubahan')
                    }else if(dt['jenis_susulan'] == 2){
                        $('#jenis_susulan').val('Tunjangan Fungsional')
                    }else{
                        $('#jenis_susulan').val('')
                    }

                    if(dt['kepala_skpd_nip'] == ''){
                        $('#skpd').val('Ditolak')
                    }else{
                        $('#skpd').val('Acc')
                    }

                    if(dt['verifikasi_status'] == 1){
                        $('#bkd').val('Acc')
                    }else if(dt['verifikasi_status'] == 2){
                        $('#bkd').val('Ditolak')
                    }else{
                        $('#bkd').val('Menunggu')
                    }

                    $('#nip').val(dt['nip'])
                    $('#nama').val(dt['nama'])
                    $('#start').val(dt['periode_start'])
                    $('#end').val(dt['periode_end'])
                    $('#lama').val(dt['lama'])
                    $('#baru').val(dt['baru'])
                    $('#jml').val(dt['jumlah_bulan'])
                    $('#kekurangan').val(dt['kekurangan'])

                    $('#tunjangan_istri').val(dt['tunjangan_istri'])
                    $('#tunjangan_anak').val(dt['tunjangan_anak'])
                    $('#jumlah_tunjangan_keluarga').val(dt['jumlah_tunjangan_keluarga'])
                    $('#tunjangan_umum').val(dt['tunjangan_umum'])
                    $('#tunjangan_umum_tambahan').val(dt['tunjangan_umum_tambahan'])
                    $('#tunjangan_struktural').val(dt['tunjangan_struktural'])
                    $('#tunjangan_fungsional').val(dt['tunjangan_fungsional'])
                    $('#tunjangan_pph').val(dt['tunjangan_pph'])
                    $('#pembulatan').val(dt['pembulatan'])

                    // $("#btn_add").hide();
                    $("#btn_update").show();
                    $("#btn_del").show();

                    // console.log(dt['sel'])

                } else {

                    swal.fire({
                        title: 'Ops',
                        text: msg,
                        type: 'error',
                        onClose: function() {
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                    });
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                swal.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });

        $('#mdlDetail').on('hidden.bs.modal', function(e) {
            $(this)
            $('#id').val('')
            $('#nip').val('')
            $('#kode_skpd').val('').trigger("change");
            $('#role_edit').val('').trigger("change");
            // $('#fullname').val('')
            // $("input[name=gender][value='1']").prop('checked', true)
            // $('#group').val('').trigger("change");
            // $('#region_code').val('').trigger("change");
            // $("input[name=status][value='1']").prop('checked', true)
            // document.getElementById('btn_update').style.display = 'none';
            // document.getElementById('btn_add').style.display = 'none';
            // document.getElementById('btn_del').style.display = 'none';
        })

    }
</script>

<script>
   function acc() {
        $.ajax({
            url: "<?= base_url('verifikasi_susulan/acc') ?>",
            type: "POST",
            data: {
                id: $('#id').val()
            },
            dataType: "json",
            success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Berhasil Acc',
                        type: 'success',
                        onClose: function() {
                            $('#mdlDetail').modal('hide');
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                        });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                swal.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });

    }
</script>

<script>
   function tolak() {
        $.ajax({
            url: "<?= base_url('verifikasi_susulan/tolak') ?>",
            type: "POST",
            data: {
                id: $('#id').val()
            },
            dataType: "json",
            success: function(data) {
                    swal.fire({
                        icon: 'success',
                        title: 'Berhasil Tolak',
                        type: 'success',
                        onClose: function() {
                            $('#mdlDetail').modal('hide');
                            $('#tabledt').DataTable().ajax.reload(null, false);
                        }
                        });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                swal.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });
    }
</script>
