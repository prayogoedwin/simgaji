<!-- Page content -->
<div class="page-content">

<style>
    .dt-buttons .btn{
        margin:10px
    }
</style>

    <!-- Main content -->
    <div class="content-wrapper">


        <!-- Page header -->
        <div class="page-header page-header-light">
            <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?= $title ?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                        <?= $breadcrumb ?>
                    </div>

                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>


            </div>
        </div>
        <!-- /page header -->





        <!-- Content area -->
        <div class="content">

            <!-- Form inputs -->

            <div class=" ">


                <div class="">
                    <!-- <form action="#"> -->
                    <form id="form-filter" class="form-horizontal">




                        <div class="form-group row">

                        <input hidden name="periodez" id="periodez" value="<?=$this->uri->segment(3);?>">

                            <div class="col-lg-3">
                                <?php
                                //superadmin

                                echo form_dropdown('lokasi_kerja', opd_array_fix_array('Lokasi Kerja', 'kode', $session['role'], $session['pengampu'] ), '', ' id="lokasi_kerja" class="form-control custom-select select2"   required');
                                
                               
                                ?>
                            </div>

                            <div class="col-lg-3">
                                <?php
                                echo form_dropdown('lokasi_gaji', opd_array_fix_array('Lokasi Gaji', 'kode', $session['role'], $session['pengampu'] ), '', ' id="lokasi_gaji" class="form-control custom-select select2"   required');
                                ?>

                            </div>

                            <div class="col-lg-3">
                                <div class="text-left">
                                    <button type="button" id="btn-filter" class="btn btn-primary pull-left"> <i class="fa fa-search"></i> Cari </button>
                                </div>

                            </div>

                            <div class="col-lg-3">
                                <div class="text-right">
                                    <?php if($this->uri->segment(3) != 'riwayat'): ?>

                                        <a href="<?=base_url('verifikasi_susulan/index/riwayat')?>" id="btn-filter" class="btn btn-primary pull-left"> <i class="fa fa-forward"></i>&nbsp;Ke Data Riwayat </a>
 
                                    <?php else: ?>

                                    <a href="<?=base_url('verifikasi_susulan')?>" id="btn-filter" class="btn btn-warning pull-left"> <i class="fa fa-backward"></i>&nbsp;Ke Ajuan Baru </a>

                                    <?php endif ?>
                                   
                                </div>

                            </div>

                        </div>

                    </form>

                </div>

            </div>
            <!-- /form inputs -->

            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title"><?= $title ?></h5>
                    <div class="header-elements">
                        <!-- <a href="<?= base_url('pengumuman/tambah') ?>" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                        <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#mdlAdd" >Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                     
                        <table class="table">
                            <tr>
                                <!-- <th>Keterangan Jenis Susulan :</th> -->
                                <th>1 = GAJI GAJI_POKOK</th>
                                <th>2 = FUNGSIONAL</th>
                                <th>|</th>
                                <th><i style="color:black" class="icon-cog"></i> Belum Diproses</th>
                                <th><i style="color:green" class="icon-checkmark-circle"></i> Acc</th>
                                <th><i style="color:red" class="icon-cancel-circle2"></i> Ditolak</th>
                            </tr>
                           
                        </table>

                    </div>
                </div>



                <div class="table-responsive">
                    <table class="table table-stripe" id="tabledt">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Lokasi Kerja</th>
                                <th>Lokasi Gaji</th>
                                <th>Periode Start</th>
                                <th>Periode End</th>
                                <th>Jumlah Lama</th>
                                <th>Jumlah Baru</th>
                                <th>Jumlah Bulan</th>
                                <th>Kekurangan</th>
                                <th>tunjangan_istri</th>
                                <th>tunjangan_anak</th>
                                <th>jumlah_tunjangan_keluarga</th>
                                <th>tunjangan_umum</th>
                                <th>tunjangan_umum_tambahan</th>
                                <th>tunjangan_struktural</th>
                                <th>tunjangan_fungsional</th>
                                <th>tunjangan_beras</th>
                                <th>tunjangan_pph</th>
                                <th>pembulatan</th>
                                <th>Status Verif KA Dinas</th>
                                <th>Status Verif BKD</th>
                                <th>Jenis Susulan</th>
                                <th width="10px"><input type="checkbox" id="check-all" ></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->

        </div>
        <!-- /content area -->




        <?php $this->load->view('template/footer'); ?>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?= base_url('assets/') ?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>



    </div>
    <!-- /main content -->

</div>
<!-- /page content -->

<?php $this->load->view('verifikasi_susulan/js'); ?>

<!-- START MODAL DETAIL -->
<div id="mdlDetail" class="modal fade" >
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">

                         

                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>NIP</label>
                                    <input type="hidden" readonly class="form-control" name="id" id="id" required autocomplete="off">
                                    <input type="text" readonly class="form-control" name="nip" id="nip" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>Nama</label>
                                    <input type="text" readonly class="form-control" name="nama" id="nama" required autocomplete="off">
                                </div>
                            </div>

                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>Periode Start</label>
                                    <input type="text" readonly class="form-control" name="start" id="start" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>Periode End</label>
                                    <input type="text" readonly class="form-control" name="end" id="end" required autocomplete="off">
                                </div>
                            </div>

                            <!-- <hr> -->
                         <div class="form-group row ">
                                <div class="col-md-12">
                                    <label>Jenis Susulan</label>
                                    <input type="text" readonly class="form-control" name="jenis_susulan" id="jenis_susulan" required autocomplete="off">
                                </div>

                               
                            </div>

                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>Lama</label>
                                    <input type="text" readonly class="form-control" name="lama" id="lama" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>Baru</label>
                                    <input type="text" readonly class="form-control" name="baru" id="baru" required autocomplete="off">
                                </div>
                                
                            </div>

                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>Julah Bulan</label>
                                    <input type="text" readonly class="form-control" name="jml" id="jml" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>Kekurangan</label>
                                    <input type="text" readonly class="form-control" name="kekurangan" id="kekurangan" required autocomplete="off">
                                </div>
                                
                            </div>
                            
                            <br/>
                            <hr/>

                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>tunjangan_istri</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_istri" id="tunjangan_istri" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>tunjangan_anak</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_anak" id="tunjangan_anak" required autocomplete="off">
                                </div>
                            </div>

                             <!-- <hr> -->
                             <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>jumlah_tunjangan_keluarga</label>
                                    <input type="text" readonly class="form-control" name="jumlah_tunjangan_keluarga" id="jumlah_tunjangan_keluarga" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>tunjangan_umum</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_umum" id="tunjangan_umum" required autocomplete="off">
                                </div>
                            </div>


                             <!-- <hr> -->
                             <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>tunjangan_umum_tambahan</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_umum_tambahan" id="tunjangan_umum_tambahan" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>tunjangan_struktural</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_struktural" id="tunjangan_struktural" required autocomplete="off">
                                </div>
                            </div>


                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>tunjangan_fungsional</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_fungsional" id="tunjangan_fungsional" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>tunjangan_beras</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_beras" id="tunjangan_beras" required autocomplete="off">
                                </div>
                            </div>

                             <!-- <hr> -->
                             <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>tunjangan_pph</label>
                                    <input type="text" readonly class="form-control" name="tunjangan_pph" id="tunjangan_pph" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>pembulatan</label>
                                    <input type="text" readonly class="form-control" name="pembulatan" id="pembulatan" required autocomplete="off">
                                </div>
                            </div>

                            <!-- <hr> -->
                            <div class="form-group row ">
                                <div class="col-md-6">
                                    <label>status acc SKPD</label>
                                    <input type="text" readonly class="form-control" name="skpd" id="skpd" required autocomplete="off">
                                </div>

                                <div class="col-md-6">
                                    <label>status acc BKD</label>
                                    <input type="text" readonly class="form-control" name="bkd" id="bkd" required autocomplete="off">
                                </div>
                            </div>
                           

                          

                        </div>
                    

                        <div class="modal-footer">
                            <button type="submit" id="btn_update" class="btn btn-md btn-success d-none">Update</button>
                                <a href="javascript:void(0)" onclick="acc()" id="btn_del" class="btn btn-md btn-info">Verifikasi + Kalkulasi</a>
                                <a href="javascript:void(0)" onclick="tolak()" id="btn_del" class="btn btn-md btn-danger">Tolak</a>
                                <a href="javascript:void(0)" onclick="hapusData()" id="btn_del" class="btn btn-md btn-danger d-none" style="display:none">Hapus</a>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->









</body>

</html>