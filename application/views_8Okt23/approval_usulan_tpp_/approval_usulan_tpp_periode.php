<?php
if($detail_byperiode != null){
    $textStatusAjuan = '';
    if($detail_byperiode->status_ajuan == '0'){
        $textStatusAjuan = 'UPT / Sekolah';
    } else if($detail_byperiode->status_ajuan == '1'){
        $textStatusAjuan = 'Kasubag Keuangan';
    } else if($detail_byperiode->status_ajuan == '2'){
        $textStatusAjuan = 'Sekretaris';
    } else if($detail_byperiode->status_ajuan == '3'){
        $textStatusAjuan = 'Kepala SKPD';
    }
}
$role = $this->session->userdata('role');
?>

<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Approval Usulan TPP P3K</a>
                    <a href="<?=base_url('approval_usulan_tpp')?>" class="breadcrumb-item"> Periode</a>
                    <span class="breadcrumb-item active">Usulan</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <?php if($detail_byperiode != null) :?>
                    <h5 class="card-title">Data <span class="badge badge-info">Status Usulan : <?=$textStatusAjuan;?></span></h5>
                <?php else :?>
                    
                <?php endif; ?> -->
                <h5 class="card-title">Data</h5>
                
                <input type="hidden" name="period" id="periode" value="<?=$this->uri->segment(3)?>">
                <div class="header-elements">
                <?php if($detail_byperiode != null) :?>
                    <?php if($detail_byperiode->posisi_acc == $this->session->userdata('B_02B')) :?>
                    <a href="javascript:void(0);" onclick="ajukanSemuaData();" class="btn btn-labeled btn-labeled-right bg-success">Ajukan <b><i class="icon-file-check"></i></b></a>
                    <!-- <a href="javascript:void(0);" onclick="revisiSemuaData();" class="btn btn-labeled btn-labeled-right bg-danger">Revisi <b><i class="icon-close2"></i></b></a> -->
                    <input type="hidden" name="periode" id="periode" value="<?=$this->uri->segment(3);?>">
                    <input type="hidden" name="rolenya" id="rolenya" value="<?=$role;?>">
                    <?php endif; ?>
                <?php endif; ?>

                <?php if($role == 5 || $role == 6) :?>        
                    <table class="table">
                        <tbody>
                            <tr>
                                <th><i class="icon-cog"></i> = Belum diproses (<?=$sum_kumulatif['blm']?>)</th>
                                <th><i style="color:red" class="icon-cancel-circle2"></i> = Ditolak (<?=$sum_kumulatif['tlk']?>)</th>
                                <th><i style="color:green" class="icon-checkmark-circle"></i> = Disetujui (<?=$sum_kumulatif['stj']?>)</th>
                            </tr>
                        </tbody>
                    </table>
                <?php endif; ?>

                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tableAtas">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>NIP</br>Nama</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($datatableAtas as $key => $value) :?>
                            <?php 
                            $ongko = $key + 1;
                            $stringtag = '#' . 'pegawai' . $ongko;
                            $content = 'pegawai' . $ongko;
                            ?>
                        <tr>
                            <td><?=$ongko;?></td>
                            <td>
                                <div class="card-header">
                                    <h6 class="card-title">
                                        <a class="text-default" data-toggle="collapse" href="<?=$stringtag?>" aria-expanded="true">
                                            <i class="icon-person mr-2 text-slate"></i> <?=$value['nip'];?> / <?=$value['nama'];?>
                                        </a>
                                    </h6>
                                </div>

                                <div id="<?=$content;?>" class="collapse">
                                    <div class="card-body">
                                    <table class="table table-striped" cellspacing="0" width="100%" style="width: 100%;">
                                        <thead style="background: lightgray;">
                                            <tr>
                                                <th>No</th>
                                                <th>Params</th>
                                                <th>Lama</th>
                                                <th>Baru</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($value['detailYbs'] as $kii => $valdet) :?>
                                            <tr>
                                                <td><?=$kii + 1;?></td>
                                                <td><?=$valdet['params']?></td>
                                                <td><?=$valdet['lama']?></td>
                                                <td><?=$valdet['baru']?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                        
                                        <!-- She exposed painted fifteen are noisier mistake led waiting. Surprise not wandered speedily husbands although yet end. Are court tiled cease young built fat one man taken. We highest ye friends is exposed equally in. Ignorant had too strictly followed. Astonished as travelling assistance or unreserved oh pianoforte ye. Five with seen put need tore add neat. -->
                                    </div>

                                    <!-- <div class="card-footer bg-transparent d-sm-flex align-items-sm-center border-top-0 pt-0">
                                        <span class="text-muted">Created at: May 25, 2015</span>
                                    </div> -->
                                </div>
                            </td>
                        </tr>
                        <?php endforeach ;?>
                        
                    </tbody>
                </table>
            </div>

        </div>
        <!-- /basic table -->


        <!-- START TABLE HISTORY -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">History</h5>
                
                <div class="header-elements">
                    
                </div>
            </div>

            <span style="margin: 4px;" class="badge badge-info">Jika data history kosong maka anda adalah PLT pada data berjalan</span>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledtHist">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Periode</th>
                            <th>NIP <br>Nama <br>Jabatan</th>
                            <th>Status</th>
                            <th>Waktu</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END TABLE HISTORY -->

        <!-- START MODAL DETAIL -->
        <div id="mdlDetail" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">
                            <input type="text" name="idne" id="idne">
                            <!-- <hr> -->
                            <div class="form-group row col-md-12">
                                <label>Golongan</label>
                                <input type="text" class="form-control" name="golongan" id="golongan" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Potongan</label>
                                <input type="number" class="form-control" name="potongan" id="potongan" required autocomplete="off">
                            </div>
                        </div>
                    

                        <div class="modal-footer">
                            <button id="btn_update" type="submit" class="btn btn-md btn-success">Update</button>
                            <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        var tabel = $('#tabledtHist').DataTable({

            'destroy'     : true,
            'paging'      : false,
            'lengthChange': true,
            'searching'   : false,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: 'no' },
                { data: 'periode' },
                { data: 'nip_nama_jabatan' },
                { data: 'status' },
                { data: 'waktu' },
                // { data: 'username', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

        
        $('#tableAtas').DataTable({
            'searching'   : true,
            'info'        : true,
            columnDefs: [{ }],
        });

        // var tabel = $('#tabledt').DataTable({

        //     'destroy'     : true,
        //     'paging'      : false,
        //     'lengthChange': true,
        //     'searching'   : false,
        //     'ordering'    : false,
        //     'info'        : false,
        //     'autoWidth'   : false,
        //     'data': <?=json_encode($datatableAtas);?>,
        //     'columns': [
        //         { data: 'no' },
        //         { data: 'nip' },
        //         { data: 'nama' },
        //         // { data: 'username', sortable : false, searceable : false  }
        //     ],
        //     columnDefs: [{ }],
        // });

    });
</script>

<script>
    function revisiSemuaData(){
        var peran = $('#rolenya').val()
       
        if(peran == 5){
            //sek
            var msgConfirm = "Seluruh data usulan akan dikembalikan kepada Kasubbag Keuangan"
            var urle = "<?=base_url('approval_usulan_tpp/revisi_sek_action')?>"
        } else if (peran == 6){
            //kadinas
            var msgConfirm = "Seluruh data usulan akan dikembalikan kepada Kasubbag Keuangan"
            var urle = "<?=base_url('approval_usulan_tpp/revisi_kadis_action')?>"
        } else if (peran == 7){
            //tu cabdin
            var msgConfirm = "Seluruh data usulan akan dikembalikan kepada TU Sekolah"
            var urle = "<?=base_url('approval_usulan_tpp/revisi_cabdin_action')?>"
        } else if(peran == 3){
            //kasubbag keu disdik
            // var msgConfirm = "Seluruh data usulan akan di ajukan kepada Sekretaris SKPD"
            // var urle = "<?=base_url('approval_usulan_tpp/revisi_action')?>"
        }

        var period = $('#periode').val()

        // Defaults
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: msgConfirm,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya..kin!',
            cancelButtonText: 'Nanti!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {

                swalInit.fire({
                    title: 'Alasan tolak',
                    input: 'text',
                    inputPlaceholder: 'Masukkan alasan',
                    showCancelButton: true,
                    inputClass: 'form-control',
                    inputValidator: function(value) {
                        return !value && 'Alasan tidak boleh kosong!'
                    }
                }).then(function(restolak) {
                    if(restolak.value) {
                        // swalInit.fire({
                        //     type: 'success',
                        //     html: 'Hi iki alesane, ' + restolak.value
                        // });

                        $.ajax({
                            url: urle,
                            type: "POST",
                            data: {
                                periode : period,
                                ket : restolak.value
                            },
                            dataType: "json",
                            success: function (data) {
                                // console.log(data);
                                
                                var sts = data['status'];
                                var msg = data['message'];
                                
                                if(sts == 1){
                                    swalInit.fire({
                                        title: 'Yeay',
                                        text: msg,
                                        type: 'success',
                                        onClose: function() {
                                            // alert('Notification is closed.');
                                            location.reload();
                                        }
                                    });

                                    // swalInit.fire(
                                    //     'Deleted!',
                                    //     'Your file has been deleted.',
                                    //     'success'
                                    // );
                                } else {
                                    swalInit.fire(
                                    'Cancelled',
                                    msg,
                                    'error'
                                    );
                                }
                                
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                
                                swalInit.fire({
                                    title: 'Oppss',
                                    text: 'Terjadi kesalahan nih',
                                    type: 'error',
                                    onClose: function() {
                                        // alert('Notification is closed.');
                                        location.reload();
                                    }
                                });
                            }
                        });
                    }
                });
            }

            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     swalInit.fire(
            //         'Cancelled',
            //         'Your imaginary file is safe :)',
            //         'error'
            //     );
            // }
        });
    }
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

<script>
    function ajukanSemuaData(){

        var peran = $('#rolenya').val()
       
        if(peran == 5){
            //sek
            var msgConfirm = "Seluruh data usulan akan di ajukan kepada Kepala SKPD"
            var urle = "<?=base_url('approval_usulan_tpp/ajukan_action')?>"
        } else if (peran == 6){
            //kadinas
            var msgConfirm = "Seluruh data usulan akan di ajukan kepada BKD Provinsi Jawa Tengah"
            var urle = "<?=base_url('approval_usulan_tpp/ajukan_action')?>"
        } else if (peran == 7){
            //tu cabdin
            var msgConfirm = "Seluruh data usulan akan di ajukan kepada Kasubbag Keuangan DISDIKBUD Provinsi Jawa Tengah"
            var urle = "<?=base_url('approval_usulan_tpp/ajukan_action')?>"
        } else if(peran == 3){
            //kasubbag keu disdik
            var msgConfirm = "Seluruh data usulan akan di ajukan kepada Sekretaris SKPD"
            var urle = "<?=base_url('approval_usulan_tpp/ajukan_action_kasubbagkeudisdik')?>"
        }

        // alert(urle)

        var period = $('#periode').val()

        // Defaults
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: msgConfirm,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya..kin!',
            cancelButtonText: 'Nanti!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {

                $.ajax({
                    url: urle,
                    type: "POST",
                    data: {
                        periode : period
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            // alert(msg)
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });

                            // swalInit.fire(
                            //     'Deleted!',
                            //     'Your file has been deleted.',
                            //     'success'
                            // );
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }

            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     swalInit.fire(
            //         'Cancelled',
            //         'Your imaginary file is safe :)',
            //         'error'
            //     );
            // }
        });
    }
</script>

</body>
</html>