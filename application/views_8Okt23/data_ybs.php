<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Gaji Take Home Pay</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('assets/plugin/mobile/adminlte.min.css')?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


    <style type="text/css">
        .lg-a {
            align-items: center;
            background: #e9ecef;
            display: -ms-flexbox;
            display: flex;
            -ms-flex-direction: column;
            flex-direction: column;
            -ms-flex-pack: center;
            padding: 5px;
        }

        @media (min-height: 700px) {
            .lg-a {
                height: 90vh;
            }
        }

        @media (min-height: 500px) {
            .lg-a {
                height: 95vh;
            }
        }

        @media (min-height: 900px) {
            .lg-a {
                height: 90vh;
            }
        }

        .card {
            background-color: #fff;
            margin-top: 10px;
        }
    </style>
</head>
<body class="hold-transition lg-a">
    <?php
    $nip = $filter['nip'];
    $nama = $filter['nama'];
    $periode = $filter['periode'];
    // $kinerja_statis = 68;

    $nominal_thp = ($thp['nominalskp'] + $thp['nominalperilaku'] + $thp['gaji']) - $thp['nominalkurang'];
    ?>
    <div class="login-box" style="height: 100%; min-height: 10px;">
        <!-- /.login-logo -->
        <div class="card" style="text-align: center; border-radius: 10px;">
            <div class="card-body">
                <img src="https://simpeg.bkd.jatengprov.go.id/eps/showpic/<?=$nip;?>" style="text-align: center; border-radius: 50%; width: 100px; height: 100px; object-fit: cover;" alt="">
                <br><?=$nama?>
                <br><?=$nip?>
                <br><small>Periode : <?=$periode?></small>
            </div>
        </div>

        <div class="card" style="border-radius: 10px;">
            <div class="card-body">
              <table id="tableContent" class="table table-borderless">
                  <thead>
                      <tr>
                          <th rowspan="2">Perilaku</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>Kurang Jam Kerja</td>
                          <td><?=$thp['jam']?></td>
                      </tr>
                      <tr>
                          <td>Hukuman Disiplin</td>
                          <?php if($thp['hukdis'] == 100) :?>
                            <td>Tidak</td>
                          <?php else :?>
                            <td><?=$thp['ket']?></td>
                          <?php endif; ?>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>

        <div class="card" style="border-radius: 10px;">
            <div class="card-body">
              <table id="tableContent" class="table table-borderless">
                  <thead>
                      <tr>
                          <th rowspan="2">Penerimaan</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>SKP</td>
                          <td>Rp. <?=number_format($thp['nominalskp'])?></td>
                      </tr>
                      <tr>
                          <td>Perilaku</td>
                          <td>Rp. <?=number_format($thp['nominalperilaku'])?></td>
                      </tr>
                      <tr>
                          <td>TPP</td>
                          <td>Rp. <?=number_format($thp['nominaltpp'])?></td>
                      </tr>
                      <tr>
                          <td>Gaji</td>
                          <td>Rp. <?=number_format($thp['gaji'])?></td>
                      </tr>
                      <tr>
                          <td>Pengurang</td>
                          <td>Rp. <?=number_format($thp['nominalkurang'])?></td>
                      </tr>
                      <tr>
                          <td>Take Home Pay</td>
                          <td>Rp. <?=number_format($nominal_thp)?></td>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>

        <div class="card" style="border-radius: 10px;">
            <div class="card-body">
              <table id="tableContent" class="table table-borderless">
                  <thead>
                      <tr>
                          <th rowspan="2">Komponen Gaji</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td>Gaji Pokok</td>
                          <td>Rp. <?=number_format($thp['gaji_pokok'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Anak</td>
                          <td>Rp. <?=number_format($thp['tunjangan_anak'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Istri</td>
                          <td>Rp. <?=number_format($thp['tunjangan_istri'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Umum</td>
                          <td>Rp. <?=number_format($thp['tunjangan_umum'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Umum+</td>
                          <td>Rp. <?=number_format($thp['tunjangan_umum_tambahan'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Struktural</td>
                          <td>Rp. <?=number_format($thp['tunjangan_struktural'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Fungsional</td>
                          <td>Rp. <?=number_format($thp['tunjangan_fungsional'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Beras</td>
                          <td>Rp. <?=number_format($thp['tunjangan_beras'])?></td>
                      </tr>
                      <tr>
                          <td>Tunjangan Pph</td>
                          <td>Rp. <?=number_format($thp['tunjangan_pph'])?></td>
                      </tr>
                      <tr>
                          <td>Pembulatan</td>
                          <td>Rp. <?=number_format($thp['pembulatan'])?></td>
                      </tr>
                      <tr>
                          <td>Potongan IWP</td>
                          <td>Rp. <?=number_format($thp['potongan_iwp'])?></td>
                      </tr>
                      <tr>
                          <td>Potongan Lain</td>
                          <td>Rp. <?=number_format($thp['potongan_lain'])?></td>
                      </tr>
                      <tr>
                          <td>Potongan Beras</td>
                          <td>Rp. <?=number_format($thp['potongan_beras'])?></td>
                      </tr>
                      <tr>
                          <td>Potongan CP</td>
                          <td>Rp. <?=number_format($thp['potongan_cp'])?></td>
                      </tr>
                      <tr>
                          <td>Jumlah Bersih</td>
                          <td>Rp. <?=number_format($thp['jumlah_bersih'])?></td>
                      </tr>
                      <tr>
                          <td>Gaji dibayarkan</td>
                          <td>Rp. <?=number_format($thp['jumlah_bersih'])?></td>
                      </tr>
                  </tbody>
              </table>
            </div>
        </div>

    </div>
    <!-- /.login-box -->

    <style>.tb_button {padding:1px;cursor:pointer;border-right: 1px solid #8b8b8b;border-left: 1px solid #FFF;border-bottom: 1px solid #fff;}.tb_button.hover {borer:2px outset #def; background-color: #f8f8f8 !important;}.ws_toolbar {z-index:100000} .ws_toolbar .ws_tb_btn {cursor:pointer;border:1px solid #555;padding:3px}   .tb_highlight{background-color:yellow} .tb_hide {visibility:hidden} .ws_toolbar img {padding:2px;margin:0px}</style>
</body>
</html>
