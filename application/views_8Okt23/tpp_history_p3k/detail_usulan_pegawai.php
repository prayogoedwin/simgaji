<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <a href="<?=base_url('tpp_history_p3k')?>" class="breadcrumb-item">TPP History P3K</a>
                    <span class="breadcrumb-item active">Detail</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data / <?=$detail->nip;?> - <?=$detail->name;?></h5>
                <div class="header-elements">
                    
                </div>
            </div>

            <div class="table-responsive">
                <table id="dataTable" class="table table-bordered table-striped" cellspacing="0" width="100%" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Params</th>
                            <th>Lama</th>
                            <th>Baru</th>
                            <th>Ket</th>
                            <th>Status BKD</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

        <!-- Revisi modal -->
        <div id="modalRevisi" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-primary">
                        <h6 class="modal-title">Revisi Ajuan</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- <form action="<?=base_url('history_p3k/revisi_action')?>" method="post" onsubmit=""> -->
                        <div class="modal-body" id="revisimodalbody">
                            <!-- VIEW BODY ADA DI VIEWS/HISTORYP3K/revisi_ajuan -->
                        </div>

                        <div class="modal-footer">
                            <!-- <button type="submit" class="btn bg-primary">Save changes</button> -->
                            <a href="javascript:void(0)" class="btn btn-sm bg-primary" onclick="revisiAksi()">Simpan</a>
                        </div>
                    <!-- </form> -->
                </div>
            </div>
        </div>
        <!-- /Revisi modal -->
    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        var tabel = $('#dataTable').DataTable({

            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: null, sortable : false, searceable : false },
                { data: 'params' },
                { data: 'lama' },
                { data: 'baru' },
                { data: 'ket' },
                { data: 'sts_bkd'},
                { data: 'aksi', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

    });
</script>

<script>
    function revisiModal(idne){
        // alert(idne)
        var konten = "<?=base_url('history_p3k/revisiajuan?id_history=')?>" + idne
        $('#modalRevisi').modal('show');

        $("#revisimodalbody").load(konten);
    }

    function revisiAksi(){
        // alert(idhist)
        var idhist = $('#id_history').val()
        var valbaru = $('#val_baru').val()

        if(valbaru == '' || valbaru == null){
            alert('Kolom tidak boleh kosong')
            return false
        }

        var urle = "<?=base_url('history_p3k/revisi_action')?>"

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        $.ajax({
            url: urle,
            type: "POST",
            data: {
                id_history : idhist,
                val_baru : valbaru
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                $('#modalRevisi').modal('hide');
                
                var sts = data['status'];
                var msg = data['message'];
                
                if(sts == 1){
                    swalInit.fire({
                        title: 'Yeay',
                        text: msg,
                        type: 'success',
                        onClose: function() {
                            // alert('Notification is closed.');
                            location.reload();
                        }
                    });

                    // swalInit.fire(
                    //     'Deleted!',
                    //     'Your file has been deleted.',
                    //     'success'
                    // );
                } else {
                    swalInit.fire(
                    'Cancelled',
                    msg,
                    'error'
                    );
                }
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        // alert('Notification is closed.');
                        location.reload();
                    }
                });
            }
        });
    }
</script>

</body>
</html>