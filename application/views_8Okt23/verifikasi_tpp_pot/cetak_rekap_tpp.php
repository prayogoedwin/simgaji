<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Rekap TPP</title>
</head>
<body>

    <style>
        body {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 18px;
	color: #333;	
}

/* Typography */
	
h2, h3, h4 {
	margin: 0;
	padding: 0 0 5px 0;
	font-size: 12px;
	font-weight: bold;
}
	
h2 {
	font-size: 14px;
	font-weight: bold;
}
	
h3 {
	font-size: 13px;
	font-weight: bold;
}

h4 {
	font-size: 11px;
}

a {
	color: #096DD1;
	text-decoration: none;
	cursor: pointer;
}

a:hover {
	text-decoration: none;
}

p {
	margin: 0;
	padding: 0 0 9px 0;
}

.lite {
	color: #999;
}

/* Lists */

ul {
	list-style: outside;
	margin: 0 0 9px 16px;
	list-style-type: disc;
}

dt {
	font-weight: bold;
}

dd {
	padding: 0 0 9px 0;
}

/* Menus */

.menu-right li {
	list-style-type: none;
	display: inline;	
	margin: 0 0 0 15px;
}

/* Forms */

form fieldset {
	border: 1px solid #ddd;
	clear: both;
	margin-bottom: 9px;
	overflow: hidden;
	padding: 9px 18px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
}

form legend {
	background: #e5e5e5;
	color: #262626;
	margin-bottom: 9px;
	padding: 2px 11px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
}

form label {
	font-weight: bold;
	margin-right: 6px;
	text-align: right;
	width: 50px;
}

textarea, input, select {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	vertical-align: middle;	
	padding: 2px 0 2px 3px;
	border: 1px solid #ccc;	
}

.input {
	padding: 2px 0 2px 3px;
	border: 1px solid #ccc;	
}

form button {
	vertical-align: middle;	
}

.button {
	height: 24px;
	margin: 0;
	padding: 3px 6px;
	background: #f1f1f1 url(../images/bg-panel-header.gif) left bottom repeat-x;
	font-size: 12px;
	font-weight: bold;
	cursor: pointer;
	vertical-align: middle;
	border: 1px solid #808080;
	-moz-border-radius: 3px;
		
}

* html .button {
	padding: 3px 1px;		
}

.searchField {
	width: 150px;
	overflow: hidden;
	line-height: 15px; 
	margin: 0;
	padding: 4px 0 3px 5px;
	border: 1px solid #808080;	
	border-right: 0;
	vertical-align: middle;
	-moz-border-radius-topleft: 3px;
	-moz-border-radius-bottomleft: 3px;				
}

.searchButton {
	-moz-border-radius-topleft: 0;
	-moz-border-radius-bottomleft: 0;		
}	

* html .toolbox .searchField, * html .toolbox .searchButton { /* IE6 Hack */
	margin-top: -1px;		
}

* html .toolbox .button { /* IE6 Hack */
	vertical-align: top;
	margin-top: 0;			
}

*:first-child+html .toolbox .searchField, *:first-child+html .toolbox .searchButton { /* IE7 Hack */
	margin-top: -1px;	
}

*:first-child+html .toolbox .button { /* IE7 Hack */
	vertical-align: top;
	margin-top: 0;	
}

.formRow {
	margin: 0 0 8px 0;
}

.buttonRow {
	margin-bottom: 10px;	
}

/* Code */

pre {
	background-color: #f2f2f2;
	color: #070;
	display: block;
	font-family: 'Courier New', Courier, monospace;
	font-size: 11px;
	overflow: auto;
	margin: 0 0 10px 0;
	padding: 10px;
	border: 1px solid #ddd;	
}

/* Status Messages */
.error,
.notice, 
.success    { padding: 8px; margin-bottom: 10px; border: 2px solid #ddd; }
.error      { background: #FBE3E4; color: #D12F19; border-color: #FBC2C4; }
.notice     { background: #FFF6BF; color: #817134; border-color: #FFD324; }
.success    { background: #E6EFC2; color: #529214; border-color: #C6D880; }
.error a    { color: #D12F19; }
.notice a   { color: #817134; }
.success a  { color: #529214; }

/* Dividers */

hr {
	background-color: #d3d3d3;
	color: #ccc;
	height: 1px;
	border: 0px;
}

/* Misc */

.nobr {
	white-space: nowrap;
}
    </style>

<?php
// $A_01 = $this->session->userdata('A_01');
$A_01cetak = substr($_GET['kode'], 0, 2);
?>

<table width="640" border="0">
  <tbody><tr>
    <th align="center" scope="col"><strong>REKAPITULASI PENGUKURAN TAMBAHAN PENGAHASILAN PEJABAT</strong><br>
      <strong>DAN PEGAWAI PEMERINTAH PROVINSI JAWA TENGAH</strong><br>
      <strong>BERDASARKAN KINERJA</strong></th>
  </tr>
  <tr>
    <td>Periode Pembayaran TPP PNS : <?=bulanAngkaToHuruf($this->input->get('bulan'));?> <?=$this->input->get('tahun');?><br>
    SKPD : <?=$unor?></td>
  </tr>
  <?php if($A_01cetak == 'D0') :?>
  <tr>
	<td>Sub Unor : <?=$subunor_disdik?></td>
  </tr>
  <?php endif; ?>
  <tr>
    <td><table width="100%" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse">
      <tbody><tr>
        <td width="29" align="center" valign="top"><strong>NO</strong></td>
        <td width="156" align="center" valign="top"><strong>NIP<br>NAMA PEJABAT/</strong><br>              <strong>PEGAWAI</strong></td>
        <td width="155" align="center" valign="top"><strong>UNIT KERJA</strong></td>
        <td width="155" align="center" valign="top"><strong>JABATAN</strong></td>
        <td width="30" align="center" valign="top"><strong>KELAS JAB</strong></td>
        <td width="30" align="center" valign="top"><strong>GOL</strong></td>
        <td width="118" align="center" valign="top"><strong>JUMLAH TAMBAHAN PENGHASILAN SESUAI PERGUB</strong></td>
        <td width="98" align="center" valign="top"><strong>JUMLAH POTONGAN PENGHASILAN</strong></td>
        <td width="118" align="center" valign="top"><strong>JUMLAH TAMBAHAN PENGHASILAN SESUAI PENGUKURAN</strong></td>
        <td width="32" align="center" valign="top"><strong>KET</strong></td>
      </tr>

	  <?php
		$sumasli = 0;
		$sumpotongan = 0;
		$sumallybs = 0;
	  ?>

      <?php foreach ($datanya as $key => $value) :?>
		<?php 
			$sumasli += $value['nominaldefault'];
			$sumpotongan += $value['nominalkurang'] + $value['nominalkurangkepatuhan'];
			$sumallybs += $value['nominaltpp'];

			$tot_potongan = $value['nominalkurang'] + $value['nominalkurangkepatuhan'];
		?>
      <tr>
        <td width="29" align="right"><?=$key+1?></td>
        <td width="156"><?=$value['nip']?></br><?=$value['nama']?></td>
        <td width="156"><?=$value['nalok']?></td>
        <td width="155"><?=$value['jabatan']?></td>
        <td width="30" align="center"><?=$value['kelas_jab']?></td>
        <td width="30" align="center"><?=$value['pkt_gol']?></td>
        <td width="118" align="right">Rp. <?=number_format($value['nominaldefault'])?></td>
        <td width="98" align="right">Rp. <?=number_format($tot_potongan)?></td>
        <td width="118" align="right">Rp. <?=number_format($value['nominaltpp'])?></td>
		<?php if(strpos($value['ket'], 'masa kerja kurang dari 1 tahun') !== false) :?>
        	<td width="35"><1th</td>
		<?php else :?>
			<td width="35">&nbsp;</td>
		<?php endif; ?>
      </tr>
      <?php endforeach; ?>

      <tr>
        <td colspan="6" valign="top"><strong>JUMLAH</strong></td>
        <td width="118" align="right" valign="top"><strong>Rp. <?=number_format($sumasli)?></strong></td>
        <td width="118" align="right" valign="top"><strong>Rp. <?=number_format($sumpotongan)?></strong></td>
        <td width="118" align="right" valign="top"><strong>Rp. <?=number_format($sumallybs)?></strong></td>
        <td width="32" valign="top"><strong>&nbsp;</strong></td>
      </tr>

    </tbody></table></td>
  </tr>
  <tr>
    <td align="right"><table width="50%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
      <tbody><tr>
        <td scope="col">Semarang,</td>
      </tr>
      <tr>
		<?php if($kepala['isplt'] == 1) :?>
			<td align="center"><strong>Plt. KEPALA <?=$unor?></strong></td>
		<?php else :?>
			<td align="center"><strong><?=$kepala['rPeg']->NAJABP?></strong></td>
		<?php endif; ?>
      </tr>
      <tr>
        <td align="center">&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><strong><u><?=$kepala['rPeg']->B_03A?> <?=$kepala['rPeg']->B_03?> <?=$kepala['rPeg']->B_03B?></u></strong></td>
      </tr>
      <tr>
        <td align="center"><strong><?=$kepala['rPeg']->NAMAY?></strong></td>
      </tr>
      <tr>
        <td align="center"><strong>NIP.<?=$this->Verifikasi_tpp_pot_model->format_nip($kepala['rPeg']->B_02B)?></strong></td>
      </tr>
    </tbody>
</table></td>
  </tr>
  
</tbody></table>


</body>
</html>