<?php
// if($detail_byperiode != null){
//     $textStatusAjuan = '';
//     if($detail_byperiode->status_ajuan == '0'){
//         $textStatusAjuan = 'UPT / Sekolah';
//     } else if($detail_byperiode->status_ajuan == '1'){
//         $textStatusAjuan = 'Kasubag Keuangan';
//     } else if($detail_byperiode->status_ajuan == '2'){
//         $textStatusAjuan = 'Sekretaris';
//     } else if($detail_byperiode->status_ajuan == '3'){
//         $textStatusAjuan = 'Kepala SKPD';
//     }
// }
$role = $this->session->userdata('role');
?>

<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
           
        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <span class="breadcrumb-item active">TPP History</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <!-- <?php if($detail_byperiode != null) :?>
                    <h5 class="card-title">Data <span class="badge badge-info">Status Usulan : <?=$textStatusAjuan;?></span></h5>
                <?php else :?>
                    <h5 class="card-title">Data</h5>
                <?php endif; ?> -->

                <h5 class="card-title">Data</h5>
                
                <input type="hidden" name="period" id="periode" value="<?=$this->uri->segment(3)?>">
                <div class="header-elements">
                <?php if(($role == 3 && $detail_byperiode != null && $detail_byperiode[0]->updated_at == null) || ($role == 4 && $detail_byperiode != null && $detail_byperiode[0]->updated_at == null) || ($role == 7 && $detail_byperiode != null && $detail_byperiode[0]->updated_at == null)) :?>
                    <a href="javascript:void(0);" onclick="ajukanSemuaData();" class="btn btn-labeled btn-labeled-right bg-success">Ajukan <b><i class="icon-file-check"></i></b></a>
                    <input type="hidden" name="periode" id="periode" value="<?=$this->uri->segment(3);?>">
                    <input type="hidden" name="rolenya" id="rolenya" value="<?=$role;?>">
                    <input type="hidden" name="A_01" id="A_01" value="<?=$this->session->userdata('A_01')?>">
                <?php endif; ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

        <!-- START TABLE HISTORY -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">History</h5>
                
                <div class="header-elements">
                    
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledtHist">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Periode</th>
                            <th>NIP <br>Nama <br>Jabatan</th>
                            <th>Status</th>
                            <th>Waktu</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END TABLE HISTORY -->

        <!-- START MODAL DETAIL -->
        <div id="mdlDetail" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">Detail</h6>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <form action="" method="POST">
                        <div class="modal-body">
                            <input type="text" name="idne" id="idne">
                            <!-- <hr> -->
                            <div class="form-group row col-md-12">
                                <label>Golongan</label>
                                <input type="text" class="form-control" name="golongan" id="golongan" required autocomplete="off">
                            </div>
                            <div class="form-group row col-md-12">
                                <label>Potongan</label>
                                <input type="number" class="form-control" name="potongan" id="potongan" required autocomplete="off">
                            </div>
                        </div>
                    

                        <div class="modal-footer">
                            <button id="btn_update" type="submit" class="btn btn-md btn-success">Update</button>
                            <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a>
                            <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- END MODAL DETAIL -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        var tabel = $('#tabledtHist').DataTable({

            'destroy'     : true,
            'paging'      : false,
            'lengthChange': true,
            'searching'   : false,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: 'no' },
                { data: 'periode' },
                { data: 'nip_nama_jabatan' },
                { data: 'status' },
                { data: 'waktu' },
                // { data: 'username', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

    });
</script>

<script>
    // var tabel = null;
    // $(document).ready(function() {
    //     var per = $('#periode').val();
    //     // alert(per)
    //     tabel = $('#tabledt').DataTable({
    //         "processing": true,
    //         "responsive":true,
    //         "serverSide": true,
    //         "ordering": true, // Set true agar bisa di sorting
    //         "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
    //         "ajax":
    //         {
    //             "url": "<?= base_url('history_p3k/getData');?>", // URL file untuk proses select datanya
    //             "type": "POST",
    //             "data": { 
    //                 period: per
    //             },
    //         },
    //         "deferRender": true,
    //         "aLengthMenu": [[10, 20, 50],[ 10, 20, 50]], // Combobox Limit
    //         "columns": [
    //             {"data": 'id',"sortable": false, 
    //                 render: function (data, type, row, meta) {
    //                     return meta.row + meta.settings._iDisplayStart + 1;
    //                 }  
    //             },
    //             { "data": "nip"},
    //             { "data": "name" },  // Tampilkan nama sub kategori
    //             // { "data": "lokasikerja_simgaji",},
    //         ],
    //         columnDefs: [{ }],
    //     });
    // });

    $(function () {
        var tabel = $('#tabledt').DataTable({

            'destroy'     : true,
            'paging'      : false,
            'lengthChange': true,
            'searching'   : false,
            'ordering'    : false,
            'info'        : false,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable_top);?>,
            'columns': [
                { data: 'no' },
                { data: 'nip' },
                { data: 'nama' },
                { data: 'act' },
                // { data: 'username', sortable : false, searceable : false  }
            ],
            columnDefs: [{ }],
        });

    });
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

<script>
    function ajukanSemuaData(){

        var period = $('#periode').val()
        var peran = $('#rolenya').val()
        var a01 = $('#A_01').val()
        if(peran == 4){
            //UPT Sekolah
            var msgConfirm = "Seluruh data usulan akan di ajukan kepada Cabang Dinas"
        } else if(peran == 3) {
            if(a01 == '86'){
                var msgConfirm = "Seluruh data usulan akan di ajukan kepada Kepala SKPD" //soejarwadi
            } else {
                var msgConfirm = "Seluruh data usulan akan di ajukan kepada Sekretaris SKPD"
            }
            
        } else {
            var msgConfirm = "Seluruh data usulan akan di ajukan kepada Kepala SKPD Provinsi"
        }

        // Defaults
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: msgConfirm,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya..kin!',
            cancelButtonText: 'Nanti!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {

                $.ajax({
                    url: "<?=base_url('tpp_history/ajukan_action')?>",
                    type: "POST",
                    data: {
                        periode : period
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });

                            // swalInit.fire(
                            //     'Deleted!',
                            //     'Your file has been deleted.',
                            //     'success'
                            // );
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }

            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     swalInit.fire(
            //         'Cancelled',
            //         'Your imaginary file is safe :)',
            //         'error'
            //     );
            // }
        });
    }
</script>

<script>
    function hapusAjuan(id_hist){
        // Defaults
        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: 'Data akan dihapus',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya..kin!',
            cancelButtonText: 'Nanti!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if(result.value) {

                $.ajax({
                    url: "<?=base_url('tpp_history/hapus_ajuan_action')?>",
                    type: "POST",
                    data: {
                        id : id_hist
                    },
                    dataType: "json",
                    success: function (data) {
                        // console.log(data);
                        
                        var sts = data['status'];
                        var msg = data['message'];
                        
                        if(sts == 1){
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    // alert('Notification is closed.');
                                    location.reload();
                                }
                            });
                        } else {
                            swalInit.fire(
                            'Cancelled',
                            msg,
                            'error'
                            );
                        }
                        
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                // alert('Notification is closed.');
                                location.reload();
                            }
                        });
                    }
                });
            }

            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     swalInit.fire(
            //         'Cancelled',
            //         'Your imaginary file is safe :)',
            //         'error'
            //     );
            // }
        });
    }
</script>

</body>
</html>