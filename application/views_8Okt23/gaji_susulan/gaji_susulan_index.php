<?php
$role = $this->session->userdata('role');
$lokasis_id = $this->session->userdata('id_lokasis');
?>
<!-- Page content -->
<div class="page-content">
    
    <!-- Main content -->
    <div class="content-wrapper">
        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                
            </div>
            
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="<?=base_url('dashboard')?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                        <span class="breadcrumb-item active">Gaji Susulan Tanpa Perubahan</span>
                    </div>
                    
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                
                
            </div>
        </div>
        <!-- /page header -->
        
        
        <!-- Content area -->
        <div class="content">
            
            <?php if($role == 7) :?>
            <!-- START SEARCH FOR CABDIN -->
            <div class="card">
                <div class="card-body">
                    <form action="" method="get">
                        <div class="col-md-12 row">
                            <div class="col-md-4">
                                <select name="kode" id="kode" class="form-control" required>
                                    <option value="">Pilih</option>
                                    <?php foreach ($dropd as $kidrop => $valdrop) :?>
                                    <option value="<?=$valdrop->kode;?>" <?php if($valdrop->kode == $filter['kodefilter']){echo "selected";} ?> ><?=$valdrop->kode;?> - <?=$valdrop->name;?></option>
                                    <?php endforeach ;?>
                                </select>
                            </div>
                            
                            <button type="submit" class="btn btn-sm btn-info">Cari</button>
                        </div>
                        
                    </form>
                </div>
            </div>
            <!-- END SEARCH FOR CABDIN -->
            <?php endif ;?>
            
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <div class="header-elements">
                        <input type="hidden" name="rolenya" id="rolenya" value="<?=$role?>">
                        <a href="javascript:void(0)" onclick="add()" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a>
                        <a href="javascript:void(0)" onclick="ajukanSemuaData()" class="btn btn-labeled btn-labeled-right bg-info" style="margin-left: 8px">Usulkan <b><i class="icon-check"></i></b></a>
                    </div>
                </div>
                
                <div class="card-body">
                    <table class="table table-stripe" id="tableContent">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Lama</th>
                                <th>Baru</th>
                                <th>Jumlah <br>Bulan</th>
                                <!-- <th>OPD</th> -->
                                <th>Start</th>
                                <th>End</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->
            
            <!-- START MODAL DETAIL -->
            <div id="mdlAdd" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h6 class="modal-title">Tambah</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <form id="form_tambahsusulan">
                            <div class="modal-body">
                                <input type="hidden" name="idne" id="idne">
                                <!-- <hr> -->
                                <div class="form-group row col-md-12">
                                    <label>Jenis Susulan</label>
                                    <select class="form-control" name="jenis_susulan" id="jenis_susulan" required>
                                        <option value="">::Pilih::</option>
                                        <option value="1">Gaji Pokok</option>
                                        <option value="2">Fungsional</option>
                                    </select>
                                </div>
                                <div id="wrapGapok" class="form-group row col-md-12">
                                    <label>Kategori Gaji Pokok</label>
                                    <select class="form-control" name="kategori_gapok" id="kategori_gapok" required>
                                        <option value="">::Pilih::</option>
                                        <option value="1">Gaji pokok tanpa perubahan</option>
                                        <!-- <option value="2">Gaji pokok dengan perubahan</option> -->
                                    </select>
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>NIP</label>
                                    <select class="form-control" name="nips" id="nips" style="width:100%!important;" required></select>
                                    <input type="hidden" name="nipz" id="nipz">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="nama" id="nama" readonly autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>SKPD</label>
                                    <input type="text" class="form-control" name="skpd" id="skpd" readonly autocomplete="off">
                                </div>
                                <div id="wrapFungsional">
                                    <div class="form-group row col-md-12">
                                        <label>Awal</label>
                                        <select class="form-control" name="fung_awl" id="fung_awl">
                                            <option value="">::Pilih::</option>
                                            <option value="1">Struktural</option>
                                            <option value="2">Fungsional Tertentu</option>
                                            <option value="3">Fungsional Umum</option>
                                        </select>
                                        <input type="hidden" name="f_awl" id="f_awl">
                                    </div>
                                    <div class="form-group row col-md-12">
                                        <label>Akhir</label>
                                        <select class="form-control" name="fung_akh" id="fung_akh">
                                            <option value="">::Pilih::</option>
                                            <option value="1">Struktural</option>
                                            <option value="2">Fungsional Tertentu</option>
                                            <option value="3">Fungsional Umum</option>
                                        </select>
                                        <input type="hidden" name="f_akh" id="f_akh">
                                    </div>
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Periode Start</label>
                                    <input type="date" class="form-control" name="periode_start" id="periode_start" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Periode End</label>
                                    <input type="date" class="form-control" name="periode_end" id="periode_end" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12" id="kontenGajiLama">
                                    <div class="col-md-6">
                                        <label>Lama (Rp.)</label>
                                        <div id="gajiLama">
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Baru (Rp.)</label>
                                        <div id="gajiBaru">
                                            
                                        </div>
                                        <small>Tunj. Fungsional terakhir dari kalkulasi</small>
                                    </div>
                                </div>
                                <br>
                                <hr>
                                <div class="form-group row col-md-12" id="kontenGajiPokok">
                                    <div class="col-md-12 row" id="jumlahbersihbayar">
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group row col-md-12">
                                    <label>Keterangan</label>
                                    <textarea type="text" class="form-control" name="keterangan" id="keterangan" required autocomplete="off"></textarea>
                                    <input type="hidden" name="lokasi_kode" id="lokasi_kode">
                                    <input type="text" name="jml_bl" id="jml_bl">
                                </div>
                                
                                <?php if($role == 4) {?>
                                <div class="form-group row col-md-12">
                                    <label>NIP / Nama TU Cabang Dinas</label>
                                    <input type="text" class="form-control" name="posisi_acc" id="posisi_acc" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <div class="alert bg-info text-white alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                        <span class="font-weight-semibold"><?=$info_sekolah['NIP']?> -- </span><?=$info_sekolah['message']?></a>.
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            
                            
                            <div class="modal-footer">
                                <button id="btn_update" type="submit" class="btn btn-md btn-success">Simpan</button>
                                <!-- <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a> -->
                                <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MODAL DETAIL -->
            
        </div>
        <!-- /content area -->
        
        
        
        
        <?php $this->load->view('template/footer');?>
        
        <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
        <script src="<?=base_url();?>assets/plugin/select2new/select2.min.js"></script>
        <!-- jquery-validation -->
        <script src="<?=base_url('assets/')?>plugin/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?=base_url('assets/')?>plugin/jquery-validation/additional-methods.min.js"></script>

        <script>
            var readDataUrl = '<?php echo site_url('gaji_susulan/read');?>'
            var addUrl = '<?php echo site_url('gaji_susulan/tambah_action');?>'
            var delUrl = '<?php echo site_url('gaji_susulan/hapus_action');?>'
            var gajiLamaUrl = '<?php echo site_url('gaji_susulan/getgajilama');?>'
            var gajiAkhirUrl = '<?php echo site_url('gaji_susulan/getgajiakhir');?>'
            var gajiBersihBayarUrl = '<?php echo site_url('gaji_susulan/getJumlahBersihBayar');?>'
            var gajiBersihBayarFungUrl = '<?php echo site_url('gaji_susulan/getJumlahBersihBayarFungsional');?>'

            var ajukanUrl = '<?php echo site_url('gaji_susulan/ajukan_action');?>'
        </script>

        <script src="<?=base_url();?>assets/js/gaji_susulan.min.js"></script>

    </div>
    <!-- /main content -->
    
</div>
<!-- /page content -->

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }
    
    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });
    
    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    $(function () {
        var fixurl = '<?=base_url('gaji_susulan/cari_pegawais/');?>';
        $('#nips').select2({
            dropdownParent: $('#mdlAdd'),
            ajax: {
                url: fixurl,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    var query = {
                        nipnya: params.term
                    }
                    
                    // Query parameters will be ?search=[term]&page=[page]
                    return query;
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            placeholder: "Masukkan NIP pada aplikasi",
            minimumInputLength: 5,
            escapeMarkup: function (markup) { return markup; },
            templateResult: formatRepo,
            templateSelection: formatRepoSelection
        });
        
        function formatRepo (repo) {
            if (repo.loading) {
                return repo.text;
            }
            
            var markup = "<div>" + repo.name + "</div><div>" + repo.id_nip + "</div>";
            
            return markup;
        }
        
        function formatRepoSelection (repo) {
            return repo.id_nip;
        }
        
        $('#nips').change(function(){
            var nip = $('#nips').select2('data')[0].id_nip;
            var name = $('#nips').select2('data')[0].name;
            var lok = $('#nips').select2('data')[0].nama_lokasi;
            // var jabatan = $('#nips').select2('data')[0].status_pengajuan;

            // var i_5a = $('#nips').select2('data')[0].I_5A;
            // var i_05 = $('#nips').select2('data')[0].I_05;

            // alert(name)

            // $('#id').val(nip);
            $('#nama').val(name);
            $('#skpd').val(lok);
            $('#nipz').val(nip)
        });

        
    });
</script>

</body>
</html>
