<?php
$A_01 = $this->session->userdata('A_01');
$role = $this->session->userdata('role');
$lokasis_id = $this->session->userdata('id_lokasis');
?>
<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">

        </div>

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Data Pegawai</a>
                    <span class="breadcrumb-item active">Nominatif TPP Potongan Pegawai</span>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>


        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <div class="card">
            <div class="card-header header-elements-inline">
                <h6 class="card-title">Periode</h6>
                <div class="header-elements">
                    <div class="list-icons">
                        <a class="list-icons-item" data-action="collapse"></a>
                        <!-- <a class="list-icons-item" data-action="reload"></a>
                        <a class="list-icons-item" data-action="remove"></a> -->
                    </div>
                </div>
            </div>

            <div class="card-body">
                <form action="" method="get">
                    <?php if($A_01 == 'D0') :?>
                    <div class="form-group row col-md-12">
                        <div class="col-md-3">
                            <select class="form-control select2" name="kode" id="kode" required>
                                <option value="">::Pilih::</option>
                                <?php foreach ($subunit_disdik as $key => $value) :?>
                                    <option value="<?=$value->kode?>" <?php if($filter2['kodedrop'] == $value->kode){echo "selected";} ?> ><?=$value->nama?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <?php endif; ?>

                    <div class="form-group row col-md-12">
                        <div class="col-md-3">
                            <input type="hidden" id="a01" value="<?=$A_01?>">
                            <select class="form-control" name="bul" id="bul" required>
                                <option value="">::Pilih::</option>
                                <option value="1" <?php if($filter['bl'] == '1'){echo "selected";} ?>>Januari</option>
                                <option value="2" <?php if($filter['bl'] == '2'){echo "selected";} ?>>Februari</option>
                                <option value="3" <?php if($filter['bl'] == '3'){echo "selected";} ?>>Maret</option>
                                <option value="4" <?php if($filter['bl'] == '4'){echo "selected";} ?>>April</option>
                                <option value="5" <?php if($filter['bl'] == '5'){echo "selected";} ?>>Mei</option>
                                <option value="6" <?php if($filter['bl'] == '6'){echo "selected";} ?>>Juni</option>
                                <option value="7" <?php if($filter['bl'] == '7'){echo "selected";} ?>>Juli</option>
                                <option value="8" <?php if($filter['bl'] == '8'){echo "selected";} ?>>Agustus</option>
                                <option value="9" <?php if($filter['bl'] == '9'){echo "selected";} ?>>September</option>
                                <option value="10" <?php if($filter['bl'] == '10'){echo "selected";} ?>>Oktober</option>
                                <option value="11" <?php if($filter['bl'] == '11'){echo "selected";} ?>>November</option>
                                <option value="12" <?php if($filter['bl'] == '12'){echo "selected";} ?>>Desember</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select class="form-control" name="thn" id="thn" required>
                                <option value="">::Pilih::</option>
                                <option value="2022" <?php if($filter['th'] == '2022'){echo "selected";} ?>>2022</option>
                                <option value="2023" <?php if($filter['th'] == '2023'){echo "selected";} ?>>2023</option>
                                <option value="2024" <?php if($filter['th'] == '2024'){echo "selected";} ?>>2024</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn bg-blue ml-3">Cari 
                            <!-- <i class="icon-search ml-2"></i> -->
                            </button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>

        <!-- Basic table -->
        <div class="card">
            <form action="<?=base_url('tpp_pot_pegawai/ajukan_tpp_actionBaru')?>" onsubmit="return confirm('Apakah anda sudah yakin data akan diajukan?');" method="post">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <div class="header-elements">
                        <?php if($A_01 == 'D0') :?>
                            <?php if(isset($_GET['kode']) && isset($_GET['bul']) && isset($_GET['thn'])) :?>
                                <a id="bcetakusulan" href="<?=base_url('nominatif_tpp_pot_pegawai/cetak_usulan_tpp?bulan='.$filter['bl'].'&tahun='.$filter['th']).'&kode='.$filter2['kodedrop']?>" target="_blank" class="btn btn-sm bg-primary">Cetak Usulan TPP</a>
                                <a id="bcetakinstrumen" href="<?=base_url('nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian?bulan='.$filter['bl'].'&tahun='.$filter['th']).'&kode='.$filter2['kodedrop']?>" target="_blank" class="btn btn-sm bg-warning">Cetak Instrumen Penilaian</a>
                                <input type="checkbox" name="cetak100" id="cetak100">Cetak yang tidak 100% saja
                                <a id="bcetakrekap" href="<?=base_url('nominatif_tpp_pot_pegawai/cetak_rekap_tpp?bulan='.$filter['bl'].'&tahun='.$filter['th']).'&kode='.$filter2['kodedrop']?>" target="_blank" class="btn btn-sm bg-success">Cetak Rekap TPP</a>
                            <?php endif; ?>
                        <?php else :?>
                            <?php if(isset($_GET['bul']) && isset($_GET['thn'])) :?>
                                <a id="bcetakusulan" href="<?=base_url('nominatif_tpp_pot_pegawai/cetak_usulan_tpp?bulan='.$filter['bl'].'&tahun='.$filter['th'])?>" target="_blank" class="btn btn-sm bg-primary">Cetak Usulan TPP</a>
                                <a id="bcetakinstrumen" href="<?=base_url('nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian?bulan='.$filter['bl'].'&tahun='.$filter['th'])?>" target="_blank" class="btn btn-sm bg-warning">Cetak Instrumen Penilaian</a>
                                <input type="checkbox" name="cetak100" id="cetak100">Cetak yang tidak 100% saja
                                <a id="bcetakrekap" href="<?=base_url('nominatif_tpp_pot_pegawai/cetak_rekap_tpp?bulan='.$filter['bl'].'&tahun='.$filter['th'])?>" target="_blank" class="btn btn-sm bg-success">Cetak Rekap TPP</a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>

                <small style="margin-left: 20px;">Total Data: <?=$filter['jml_record']?> Record</small>

                <div class="table-responsive" id="tableDefault">
                    <table class="table table-stripe table-condensed" id="tabledt">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIP <br>Nama <br>Pkt/Gol</th>
                                <th>Kelas Jab</th>
                                <th>Jabatan</th>
                                <th>Cuti Sakit</th>
                                <th>TB</th>
                                <th>Nilai SKP</th>
                                <th>PKSP</th>
                                <th>Alpha</th>
                                <th>KWK</th>
                                <th>Hukdis</th>
                                <th>Atlit <br>Pelatih</th>
                                <th>Ket</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                        </tbody>
                    </table>
                </div>
            </form>
            
        </div>
        <!-- /basic table -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        var tabel = $('#tabledt').DataTable({
            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            'data': <?=json_encode($datatable);?>,
            'columns': [
                { data: null, sortable : false, searceable : false },
                { data: 'nama_nip' },
                { data: 'kelas_jab' },
                { data: 'jabatan' },
                { data: 'cs' },
                { data: 'tb' },
                { data: 'skp' },
                { data: 'pksp' },
                { data: 'alpha' },
                { data: 'kwk' },
                { data: 'hukdis' },
                { data: 'atlit_pelatih' },
                { data: 'keterangan' },
            ],
            // columnDefs: [{ }],
        });

        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

    });
</script>

<script>
    $("#cetak100").change(function() {
        var bs = '<?=base_url()?>'
        var b = '<?=$filter['bl']?>'
        var t = '<?=$filter['th']?>'
        var k = '<?=$filter2['kodedrop']?>'
        var a01 = $('#a01').val()

        if(a01 != 'D0'){
            if(this.checked) {
                var usulanUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_usulan_tpp?bulan=' + b + '&tahun=' + t + '&c100=1'
                $("#bcetakusulan").attr("href", usulanUrl)

                var instrumenUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian?bulan=' + b + '&tahun=' + t + '&c100=1'
                $("#bcetakinstrumen").attr("href", instrumenUrl)

                var rekapUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_rekap_tpp?bulan=' + b + '&tahun=' + t + '&c100=1'
                $('#bcetakrekap').attr("href", rekapUrl);
            } else {
                var usulanOriUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_usulan_tpp?bulan=' + b + '&tahun=' + t
                $("#bcetakusulan").attr("href", usulanOriUrl)

                var instrumenOriUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian?bulan=' + b + '&tahun=' + t
                $("#bcetakinstrumen").attr("href", instrumenOriUrl)

                var rekapOriUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_rekap_tpp?bulan=' + b + '&tahun=' + t
                $('#bcetakrekap').attr("href", rekapOriUrl);
            }
        } else {
            if(this.checked) {
                var usulanUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_usulan_tpp?bulan=' + b + '&tahun=' + t + '&c100=1&kode=' + k
                $("#bcetakusulan").attr("href", usulanUrl)

                var instrumenUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian?bulan=' + b + '&tahun=' + t + '&c100=1&kode=' + k
                $("#bcetakinstrumen").attr("href", instrumenUrl)

                var rekapUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_rekap_tpp?bulan=' + b + '&tahun=' + t + '&c100=1&kode=' + k
                $('#bcetakrekap').attr("href", rekapUrl);
            } else {
                var usulanOriUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_usulan_tpp?bulan=' + b + '&tahun=' + t + '&kode=' + k
                $("#bcetakusulan").attr("href", usulanOriUrl)

                var instrumenOriUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_instrumen_penilaian?bulan=' + b + '&tahun=' + t + '&kode=' + k
                $("#bcetakinstrumen").attr("href", instrumenOriUrl)

                var rekapOriUrl = bs + 'nominatif_tpp_pot_pegawai/cetak_rekap_tpp?bulan=' + b + '&tahun=' + t + '&kode=' + k
                $('#bcetakrekap').attr("href", rekapOriUrl);
            }
        }
        
    });
</script>

<script>

    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>
