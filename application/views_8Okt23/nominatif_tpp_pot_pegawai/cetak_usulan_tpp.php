<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Usulan TPP</title>
</head>
<body>
    <style>
        body {
	text-align: left;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 18px;
	color: #333;	
}

/* Typography */
	
h2, h3, h4 {
	margin: 0;
	padding: 0 0 5px 0;
	font-size: 12px;
	font-weight: bold;
}
	
h2 {
	font-size: 14px;
	font-weight: bold;
}
	
h3 {
	font-size: 13px;
	font-weight: bold;
}

h4 {
	font-size: 11px;
}

a {
	color: #096DD1;
	text-decoration: none;
	cursor: pointer;
}

a:hover {
	text-decoration: none;
}

p {
	margin: 0;
	padding: 0 0 9px 0;
}

.lite {
	color: #999;
}

/* Lists */

ul {
	list-style: outside;
	margin: 0 0 9px 16px;
	list-style-type: disc;
}

dt {
	font-weight: bold;
}

dd {
	padding: 0 0 9px 0;
}

/* Menus */

.menu-right li {
	list-style-type: none;
	display: inline;	
	margin: 0 0 0 15px;
}

/* Forms */

form fieldset {
	border: 1px solid #ddd;
	clear: both;
	margin-bottom: 9px;
	overflow: hidden;
	padding: 9px 18px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
}

form legend {
	background: #e5e5e5;
	color: #262626;
	margin-bottom: 9px;
	padding: 2px 11px;
	-moz-border-radius: 3px;
	-webkit-border-radius: 3px;
}

form label {
	font-weight: bold;
	margin-right: 6px;
	text-align: right;
	width: 50px;
}

textarea, input, select {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	vertical-align: middle;	
	padding: 2px 0 2px 3px;
	border: 1px solid #ccc;	
}

.input {
	padding: 2px 0 2px 3px;
	border: 1px solid #ccc;	
}

form button {
	vertical-align: middle;	
}

.button {
	height: 24px;
	margin: 0;
	padding: 3px 6px;
	background: #f1f1f1 url(../images/bg-panel-header.gif) left bottom repeat-x;
	font-size: 12px;
	font-weight: bold;
	cursor: pointer;
	vertical-align: middle;
	border: 1px solid #808080;
	-moz-border-radius: 3px;
		
}

* html .button {
	padding: 3px 1px;		
}

.searchField {
	width: 150px;
	overflow: hidden;
	line-height: 15px; 
	margin: 0;
	padding: 4px 0 3px 5px;
	border: 1px solid #808080;	
	border-right: 0;
	vertical-align: middle;
	-moz-border-radius-topleft: 3px;
	-moz-border-radius-bottomleft: 3px;				
}

.searchButton {
	-moz-border-radius-topleft: 0;
	-moz-border-radius-bottomleft: 0;		
}	

* html .toolbox .searchField, * html .toolbox .searchButton { /* IE6 Hack */
	margin-top: -1px;		
}

* html .toolbox .button { /* IE6 Hack */
	vertical-align: top;
	margin-top: 0;			
}

*:first-child+html .toolbox .searchField, *:first-child+html .toolbox .searchButton { /* IE7 Hack */
	margin-top: -1px;	
}

*:first-child+html .toolbox .button { /* IE7 Hack */
	vertical-align: top;
	margin-top: 0;	
}

.formRow {
	margin: 0 0 8px 0;
}

.buttonRow {
	margin-bottom: 10px;	
}

/* Code */

pre {
	background-color: #f2f2f2;
	color: #070;
	display: block;
	font-family: 'Courier New', Courier, monospace;
	font-size: 11px;
	overflow: auto;
	margin: 0 0 10px 0;
	padding: 10px;
	border: 1px solid #ddd;	
}

/* Status Messages */
.error,
.notice, 
.success    { padding: 8px; margin-bottom: 10px; border: 2px solid #ddd; }
.error      { background: #FBE3E4; color: #D12F19; border-color: #FBC2C4; }
.notice     { background: #FFF6BF; color: #817134; border-color: #FFD324; }
.success    { background: #E6EFC2; color: #529214; border-color: #C6D880; }
.error a    { color: #D12F19; }
.notice a   { color: #817134; }
.success a  { color: #529214; }

/* Dividers */

hr {
	background-color: #d3d3d3;
	color: #ccc;
	height: 1px;
	border: 0px;
}

/* Misc */

.nobr {
	white-space: nowrap;
}
</style>


<?php
$A_01 = $this->session->userdata('A_01');
?>

<p align="center"><strong>
INSTRUMEN PENGUKURAN TAMBAHAN PENGHASILAN PEJABAT DAN<br>
PEGAWAI PEMERINTAH PROVINSI JAWA TENGAH<br>
BERDASARKAN KINERJA</strong></p>
Periode Pembayaran TPP : <?=bulanAngkaToHuruf($this->input->get('bulan'));?> <?=$this->input->get('tahun');?><br>
SKPD : <?=$unor?> <br>
<?php if($A_01 == 'D0') :?>
Sub Unit : <?=$subunor_disdik?>
<?php endif; ?>

<?php if(count($datanya) > 0): ?>
<p align="right">Data Cetak : <?=$datanya[0]['waktu']?></p>
<?php endif; ?>
<table width="100%" border="1" cellpadding="2" cellspacing="2" bordercolor="#000000" style="border-collapse: collapse">
<tbody><tr bgcolor="#CCCCCC">
  <td width="24" rowspan="2" align="center" bgcolor="#DDDDDD"><strong> No.</strong></td>
  <td width="54" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>NIP</strong></td>
  <td width="119" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>NAMA</strong></td>
  <td width="119" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>Unit <br>Kerja</strong></td>
  <td width="35" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>Pkt/<br>Gol</strong></td>
  <td width="50" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>Kelas <br>Jab</strong></td>
    
  <td width="158" rowspan="2" align="center" bgcolor="#DDDDDD"><strong> JABATAN</strong></td>
  <td width="53" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>CUTI SAKIT (HARI)</strong></td>
  <td width="73" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>TUGAS BELAJAR</strong></td>
  <td colspan="2" align="center" bgcolor="#DDDDDD"><strong>NILAI SKP (60%)</strong></td>
  <td colspan="4" align="center" bgcolor="#DDDDDD"><strong>NILAI PERILAKU KERJA (40%)</strong></td>
  <td align="center" bgcolor="#DDDDDD"><strong>HUK. DIS.</strong></td>
  <td width="54" rowspan="2" align="center" bgcolor="#DDDDDD"><strong>KET</strong></td>
</tr>
<tr bgcolor="#CCCCCC">
    <td width="57" align="center" bgcolor="#DDDDDD"><strong>NILAI SKP TB</strong></td>
    <td width="78" align="center" bgcolor="#DDDDDD"><strong>NILAI SKP K1 sd. K5 (%)</strong></td>
    <td width="56" align="center" bgcolor="#DDDDDD"><strong>K6 (HARI)</strong></td>
    <td width="46" align="center" bgcolor="#DDDDDD"><strong>K7 (JAM)</strong></td>
    <td width="49" align="center" bgcolor="#DDDDDD"><strong>% K6+K7</strong></td>
    <td width="49" align="center" bgcolor="#DDDDDD"><strong>% K8 PKSP</strong></td>
    <td width="46" align="center" bgcolor="#DDDDDD"><strong>K9 sd. K12</strong></td>
    </tr>
        <?php foreach ($datanya as $key => $value) :?>
        <tr>
            <td width="24" align="right"><?=$key+1?></td>
            <td width="54"><?=$value['nip']?></td>
            <td width="119"><?=$value['nama']?></td>
            <td width="119"><?=$value['nalok']?></td>
            <td width="35" align="center"><?=$value['pkt_gol']?></td>
            <td width="50" align="center"><?=$value['kelas_jab']?></td>
            <td width="158"><?=$value['jabatan']?></td>
            <td width="53" align="center"><?=$value['cs']?></td>
            <td width="73" align="center"><?=$value['tb']?></td>
            <td width="57" align="center">-</td>
            <td width="78" align="center"><?=$value['skp']?></td>
            <td width="56" align="center"><?=$value['alpha']?></td>
            <td width="46" align="center"><?=$value['kwk']?></td>
            <td width="49" align="center"><?=$value['k6_k7']?></td>
            <td width="46" align="center">0</td>
            <td width="46" align="center"><?=$value['hukdis']?></td>
            <td width="54"></td>
        </tr>
        <?php endforeach; ?>
    </tbody></table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse">
  <tbody><tr>
    <td align="right"><table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse">
      <tbody><tr>
        <td colspan="3" scope="col">&nbsp;</td>
        <td width="3%" scope="col">&nbsp;</td>
        <td width="40%" scope="col">Semarang, <?=date("d M Y")?></td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
        <!-- <?=json_encode($kepala['rPeg']->NAJABP)?> -->
        <!-- <?=json_encode($kepala['isplt'])?> -->
        <!-- <?=json_encode($unor)?> -->
        <?php if($kepala['isplt'] == 1) :?>
          <td colspan="2" align="center"><strong>Plt. KEPALA <?=$unor?></strong></td>
        <?php else :?>
          <td colspan="2" align="center"><strong><?=$kepala['rPeg']->NAJABP?></strong></td>
        <?php endif; ?>
      </tr>
      <tr bordercolor="#000000">
        <td rowspan="5" valign="top">SASARAN KERJA PEGAWAI (SKP)</td>
        <td valign="top" style="text-align: center">K1</td>
        <td valign="top">Nilai 86 ke atas (100%)</td>
        <td colspan="2" align="center" valign="top"><strong>PROVINSI JAWA TENGAH</strong></td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K2</td>
        <td valign="top">Nilai 76 - 85 (95%)</td>
        <td colspan="2" valign="top">&nbsp;</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K3</td>
        <td valign="top">Nilai 61 - 75 (85%)</td>
        <td colspan="2" valign="top">&nbsp;</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K4</td>
        <td valign="top">Nilai 51 - 60 (75%)</td>
        <td colspan="2" rowspan="4" align="center" valign="top"><strong><u>
          <?=$kepala['rPeg']->B_03A?> <?=$kepala['rPeg']->B_03?> <?=$kepala['rPeg']->B_03B?>        </u></strong><strong> <br>
        <?=$kepala['rPeg']->NAMAY?>        <br>
        NIP. <?=$this->Nominatif_tpp_pot_pegawai_model->format_nip($kepala['rPeg']->B_02B)?></strong></td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K5</td>
        <td valign="top">Nilai 50 ke bawah (50%)</td>
        </tr>
      <tr bordercolor="#000000">
        <td rowspan="3" valign="top">PERILAKU KERJA</td>
        <td valign="top" style="text-align: center">K6</td>
        <td valign="top">Tidak masuk kerja tanpa alasan sah per hari (-2%)</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K7</td>
        <td valign="top">Tidak memenuhi jam kerja (terlambat atau pulang awal) tanpa alasan sah per 7,5 jam (-2%)</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K8</td>
        <td valign="top">PKSP</td>
      </tr>
      <tr bordercolor="#000000">
        <td rowspan="4" valign="top">HUKUMAN DISIPLIN</td>
        <td valign="top" style="text-align: center">K9</td>
        <td valign="top">Tidak dijatuhi hukuman disiplin (100%)</td>
        <td colspan="2" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K10</td>
        <td valign="top">Dijatuhi hukuman disiplin ringan (90%) berlaku 3 bulan</td>
        <td colspan="2" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K11</td>
        <td valign="top">Dijatuhi hukuman disiplin sedang (80%) berlaku 6 bulan</td>
        <td colspan="2" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr bordercolor="#000000">
        <td valign="top" style="text-align: center">K12</td>
        <td valign="top">Dijatuhi hukuman disiplin berat (80%) berlaku 1 tahun</td>
        <td colspan="2" align="center" valign="top">&nbsp;</td>
        </tr>
      <tr>
        <td width="12%">&nbsp;</td>
        <td width="4%">&nbsp;</td>
        <td width="41%">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>


</body>
</html>