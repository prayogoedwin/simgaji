<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Instrumen TPP</title>
</head>
<body>
    <style>
        body {
        text-align: left;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        line-height: 18px;
        color: #333;	
    }

    /* Typography */
        
    h2, h3, h4 {
        margin: 0;
        padding: 0 0 5px 0;
        font-size: 12px;
        font-weight: bold;
    }
        
    h2 {
        font-size: 14px;
        font-weight: bold;
    }
        
    h3 {
        font-size: 13px;
        font-weight: bold;
    }

    h4 {
        font-size: 11px;
    }

    a {
        color: #096DD1;
        text-decoration: none;
        cursor: pointer;
    }

    a:hover {
        text-decoration: none;
    }

    p {
        margin: 0;
        padding: 0 0 9px 0;
    }

    .lite {
        color: #999;
    }

    /* Lists */

    ul {
        list-style: outside;
        margin: 0 0 9px 16px;
        list-style-type: disc;
    }

    dt {
        font-weight: bold;
    }

    dd {
        padding: 0 0 9px 0;
    }

    /* Menus */

    .menu-right li {
        list-style-type: none;
        display: inline;	
        margin: 0 0 0 15px;
    }

    /* Forms */

    form fieldset {
        border: 1px solid #ddd;
        clear: both;
        margin-bottom: 9px;
        overflow: hidden;
        padding: 9px 18px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
    }

    form legend {
        background: #e5e5e5;
        color: #262626;
        margin-bottom: 9px;
        padding: 2px 11px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
    }

    form label {
        font-weight: bold;
        margin-right: 6px;
        text-align: right;
        width: 50px;
    }

    textarea, input, select {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        vertical-align: middle;	
        padding: 2px 0 2px 3px;
        border: 1px solid #ccc;	
    }

    .input {
        padding: 2px 0 2px 3px;
        border: 1px solid #ccc;	
    }

    form button {
        vertical-align: middle;	
    }

    .button {
        height: 24px;
        margin: 0;
        padding: 3px 6px;
        background: #f1f1f1 url(../images/bg-panel-header.gif) left bottom repeat-x;
        font-size: 12px;
        font-weight: bold;
        cursor: pointer;
        vertical-align: middle;
        border: 1px solid #808080;
        -moz-border-radius: 3px;
            
    }

    * html .button {
        padding: 3px 1px;		
    }

    .searchField {
        width: 150px;
        overflow: hidden;
        line-height: 15px; 
        margin: 0;
        padding: 4px 0 3px 5px;
        border: 1px solid #808080;	
        border-right: 0;
        vertical-align: middle;
        -moz-border-radius-topleft: 3px;
        -moz-border-radius-bottomleft: 3px;				
    }

    .searchButton {
        -moz-border-radius-topleft: 0;
        -moz-border-radius-bottomleft: 0;		
    }	

    * html .toolbox .searchField, * html .toolbox .searchButton { /* IE6 Hack */
        margin-top: -1px;		
    }

    * html .toolbox .button { /* IE6 Hack */
        vertical-align: top;
        margin-top: 0;			
    }

    *:first-child+html .toolbox .searchField, *:first-child+html .toolbox .searchButton { /* IE7 Hack */
        margin-top: -1px;	
    }

    *:first-child+html .toolbox .button { /* IE7 Hack */
        vertical-align: top;
        margin-top: 0;	
    }

    .formRow {
        margin: 0 0 8px 0;
    }

    .buttonRow {
        margin-bottom: 10px;	
    }

    /* Code */

    pre {
        background-color: #f2f2f2;
        color: #070;
        display: block;
        font-family: 'Courier New', Courier, monospace;
        font-size: 11px;
        overflow: auto;
        margin: 0 0 10px 0;
        padding: 10px;
        border: 1px solid #ddd;	
    }

    /* Status Messages */
    .error,
    .notice, 
    .success    { padding: 8px; margin-bottom: 10px; border: 2px solid #ddd; }
    .error      { background: #FBE3E4; color: #D12F19; border-color: #FBC2C4; }
    .notice     { background: #FFF6BF; color: #817134; border-color: #FFD324; }
    .success    { background: #E6EFC2; color: #529214; border-color: #C6D880; }
    .error a    { color: #D12F19; }
    .notice a   { color: #817134; }
    .success a  { color: #529214; }

    /* Dividers */

    hr {
        background-color: #d3d3d3;
        color: #ccc;
        height: 1px;
        border: 0px;
    }

    /* Misc */

    .nobr {
        white-space: nowrap;
    }
    </style>
    <?php
    $A_01 = $this->session->userdata('A_01');
    ?>

    <?php foreach ($datanya as $key => $val) :?>
    <table width="680" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; page-break-after:always">
        <tbody><tr>
            <td align="center"><strong>INSTRUMEN  PENGUKURAN <br>
                TAMBAHAN PENGHASILAN PEGAWAI NEGERI SIPIL</strong><br>
                <strong>&nbsp;PEGAWAI PEMERINTAH PROVINSI JAWA TENGAH</strong></td>
            </tr>
            <tr><td>
                <table width="680" border="0" cellpadding="2" cellspacing="0" style="border-collapse: collapse">
                    <tbody><tr>
                        <td width="153" valign="top">Periode Penilaian : Bulan</td>
                        <td width="118" valign="top">: 
                            <?=bulanAngkaToHuruf($this->input->get('bulan',TRUE))?></td>
                            <td width="348" valign="top" align="right">Tahun</td>
                            <td valign="top">: <?=$this->input->get('tahun',TRUE)?></td></td>
                        </tr>
                        <tr>
                            <td width="153" valign="top">SKPD</td>
                            <td colspan="3" valign="top">: <?=$skpd_induk->NALOK?></td>
                        </tr>
                        <?php if($A_01 == 'D0') :?>
                        <tr>
                            <td width="153" valign="top">Sub Unit</td>
                            <td colspan="3" valign="top">: <?=$subunor_disdik?></td>
                        </tr>
                        <?php endif; ?>
                    </tbody>
                </table>
                    <table id="identitas" width="680" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse">
                        <tbody><tr>
                            <td width="35" align="center" valign="top"><strong>      NO </strong></td>
                            <td colspan="2" align="center" valign="top"><strong>PEJABAT PENILAI</strong></td>
                            <td colspan="2" align="center" valign="top"><strong>PNS YANG DINILAI</strong></td>
                        </tr>
                        <tr>
                            <td width="35" align="right" valign="top">1</td>
                            <td width="89" valign="top">Nama</td>
                            <td width="203" valign="top"></td>
                            <td width="89" valign="top">Nama</td>
                            <td width="202" valign="top"><?=$val['nama']?></td>
                        </tr>
                        <tr>
                            <td width="35" align="right" valign="top">2</td>
                            <td width="89" valign="top">NIP</td>
                            <td width="203" valign="top"></td>
                            <td width="89" valign="top">NIP</td>
                            <td width="202" valign="top"><?=$val['nip']?></td>
                        </tr>
                        <tr>
                            <td width="35" align="right" valign="top">3</td>
                            <td width="89" valign="top">Pkt/Gol/Ruang</td>
                            <td width="203" valign="top"></td>
                            <td width="89" valign="top">Pkt/Gol/Ruang</td>
                            <td width="202" valign="top"><?=$val['pkt_gol']?></td>
                        </tr>
                        <tr>
                            <td width="35" align="right" valign="top">4</td>
                            <td width="89" valign="top">Nama Jabatan</td>
                            <td width="203" valign="top"></td>
                            <td width="89" valign="top">Nama Jabatan</td>
                            <td width="202" valign="top"><?=$val['jabatan']?></td>
                        </tr>
                        <tr>
                            <td width="35" align="right" valign="top">4</td>
                            <td width="89" valign="top">Kelas Jabatan</td>
                            <td width="203" valign="top"></td>
                            <td width="89" valign="top">Kelas Jabatan</td>
                            <td width="202" valign="top"><?=$val['kelas_jab']?></td>
                        </tr>
                        <tr>
                            <td width="35" align="right" valign="top">5</td>
                            <td width="89" valign="top">Unit Kerja</td>
                            <td width="203" valign="top"></td>
                            <td width="89" valign="top">Unit Kerja</td>
                            <td width="202" valign="top"><?=$skpd_induk->NALOK?></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <table width="680" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse;">
                    <tbody>
                        <tr>
                            <td width="24" align="center" valign="top">NO</td>
                            <td width="93" align="center" valign="top">INDIKATOR</td>
                            <td align="center" valign="top">KODE</td>
                            <td colspan="3" align="center" valign="top">KRITERIA</td>
                            <td align="center" valign="top">PROSENTASE</td>
                            <td align="center" valign="top">HASIL PENGUKURAN</td>
                        </tr>
                        <tr>
                            <td width="24" rowspan="5" valign="top">1</td>
                            <td width="93" rowspan="5" valign="top">SASARAN KERJA PEGAWAI (SKP)</td>
                            <td valign="top" style="text-align: center">K1</td>
                            <td colspan="3" valign="top">Nilai 86 ke atas</td>
                            <td width="111" valign="top" style="text-align: center">100%</td>
                            <td rowspan="5" align="center" valign="top"><?=$val['skp']?>%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K2</td>
                            <td colspan="3" valign="top">Nilai 76 - 85</td>
                            <td width="111" valign="top" style="text-align: center">95%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K3</td>
                            <td colspan="3" valign="top">Nilai 61 - 75</td>
                            <td width="111" valign="top" style="text-align: center">85%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K4</td>
                            <td colspan="3" valign="top">Nilai 51 - 60</td>
                            <td width="111" valign="top" style="text-align: center">75%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K5</td>
                            <td colspan="3" valign="top">Nilai 50 ke bawah</td>
                            <td width="111" valign="top" style="text-align: center">50%</td>
                        </tr>
                        <tr>
                            <td width="24" rowspan="2" valign="top">2</td>
                            <td width="93" rowspan="2" valign="top">PERILAKU KERJA</td>
                            <td valign="top" style="text-align: center">K6</td>
                            <td colspan="3" valign="top">Tidak masuk kerja tanpa alasan sah per hari</td>
                            <td width="111" valign="top" style="text-align: center">2%</td>
                            <td rowspan="2" align="center" valign="top"><?=40 - $val['perilaku']?>%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K7</td>
                            <td colspan="3" valign="top">Tidak memenuhi jam kerja (terlambat atau pulang awal) tanpa alasan sah per 7,5 jam</td>
                            <td width="111" valign="top" style="text-align: center">2%</td>
                        </tr>
                        <tr>
                            <td width="24" rowspan="4" valign="top">3</td>
                            <td width="93" rowspan="4" valign="top">HUKUMAN DISIPLIN</td>
                            <td valign="top" style="text-align: center">K8</td>
                            <td colspan="3" valign="top">Tidak dijatuhi hukuman disiplin</td>
                            <td width="111" valign="top" style="text-align: center">100%</td>
                            <td rowspan="4" align="center" valign="top"><?=$val['hukdis']?>%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K9</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman disiplin ringan</td>
                            <td width="111" valign="top" style="text-align: center">90%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K10</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman disiplin sedang</td>
                            <td valign="top" style="text-align: center">80%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K11</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman disiplin berat</td>
                            <td valign="top" style="text-align: center">50%</td>
                        </tr>
                        <tr>
                            <?php $totkepatuhanprosentase = (100 - ($val['lhkpn'] + $val['lhkasn'] + $val['bmd'] + $val['grat'] + $val['tptgr'])); ?>
                            <td width="24" rowspan="5" valign="top">4</td>
                            <td width="93" rowspan="5" valign="top">KEPATUHAN ASN</td>
                            <td valign="top" style="text-align: center">K12</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman LHKPN</td>
                            <td width="111" valign="top" style="text-align: center">90%</td>
                            <td rowspan="5" align="center" valign="top"><?=$totkepatuhanprosentase?>%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K13</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman LHKASN</td>
                            <td width="111" valign="top" style="text-align: center">90%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K14</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman BMD</td>
                            <td width="111" valign="top" style="text-align: center">90%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K15</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman GRATIFIKASI</td>
                            <td width="111" valign="top" style="text-align: center">90%</td>
                        </tr>
                        <tr>
                            <td valign="top" style="text-align: center">K16</td>
                            <td colspan="3" valign="top">Dijatuhi hukuman TPTGR</td>
                            <td width="111" valign="top" style="text-align: center">90%</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                    <table width="680" border="1" cellpadding="1" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse">
                        <tbody>
                            <tr>
                                <td colspan="8" valign="top">JUMLAH TAMBAHAN PENGHASILAN YANG    DIBAYARKAN</td>
                            </tr>
                        <tr>
                            <td colspan="5" valign="top"><strong>Jumlah TPP yang dibayarkan</strong><br>
                                bulan :
                                <?=bulanAngkaToHuruf($this->input->get('bulan',TRUE))?></td> Tahun 
                                <?=$this->input->get('tahun',TRUE)?></td></td>
                                <td width="439" colspan="4" valign="top"><strong>Besaran Standar TPP x (SKP + Perilaku) x (Hukdis + Kepatuhan ASN)</strong><br>
                                    (Standar TPP x ((60% x K1/K2/K3/K4/K5)+(40%-(K6+K7)) x ((K8/K9/K10/K11) + (K12/K13/K14/K15/K16)))</td>
                                </tr>
                                <tr>
                                    <td colspan="5" rowspan="5" valign="middle">Rp. 
                                        <?=number_format($val['nominaltpp'])?></td>
                                        <td colspan="5" valign="top"><table width="100%" border="0" style="border-collapse:collapse">
                                            <tbody><tr>
                                                <td width="51%">Jumlah TPP SKP</td>
                                                <td width="23%">= Rp. </td>
                                                <td width="26%" align="right"><?=number_format($val['nominalskp'])?></td>
                                            </tr>
                                        </tbody></table></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" valign="top"><table width="100%" border="0" style="border-collapse:collapse">
                                            <tbody><tr>
                                                <td width="51%">Jumlah TPP Perilaku Kerja</td>
                                                <td width="23%">= Rp.          </td>
                                                <td width="26%" align="right"><?=number_format($val['nominalperilaku'])?></td>
                                            </tr>
                                        </tbody></table></td>
                                    </tr>

                                    <?php
                                    $tpp_3kateg = $val['nominal_bebankerjakhusus'] + $val['nominal_tempat'] + $val['nominal_kondisi'];
                                    ?>
                                    <tr>
                                        <td colspan="5" valign="top"><table width="100%" border="0" style="border-collapse:collapse">
                                            <tbody><tr>
                                                <td width="51%">Jumlah TPP (beban kerja khusus/tempat/kondisi)</td>
                                                <td width="23%">= Rp.          </td>
                                                <td width="26%" align="right"><?=number_format($tpp_3kateg)?></td>
                                            </tr>
                                        </tbody></table></td>
                                    </tr>

                                    <tr>
                                        <td colspan="4" valign="top"><table width="100%" border="0" style="border-collapse:collapse">
                                            <tbody><tr>
                                                <td width="51%">Jumlah Pengurangan TPP (Hukdis)</td>
                                                <td width="23%">= Rp.          </td>
                                                <td width="26%" align="right"><?=number_format($val['nominalkurang'])?></td>
                                            </tr>
                                        </tbody></table></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" valign="top"><table width="100%" border="0" style="border-collapse:collapse">
                                            <tbody><tr>
                                                <td width="51%">Jumlah Pengurangan TPP <br> (Kepatuhan ASN)</td>
                                                <td width="23%">= Rp.          </td>
                                                <td width="26%" align="right"><?=number_format($val['nominalkurangkepatuhan'])?></td>
                                            </tr>
                                        </tbody></table></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" valign="top"><p>Tanda    tangan pejabat yang menilai :</p>
                                            <p>&nbsp;</p></td>
                                            <td colspan="5" valign="top">Tanda    tangan pejabat/pegawai yang dinilai :</td>
                                        </tr>
                                    </tbody></table>
                                </td></tr>
                                <tr>
                                    <td align="right">Hal. <?=$key+1?></td>
                                </tr>
                                <tr>
                                    <td align="right">Data Simpan Tanggal : <?=$val['waktu']?></td>
                                </tr>
                                <br>
                            </tbody>
                        </table>
    <?php endforeach; ?>

</body>
</html>