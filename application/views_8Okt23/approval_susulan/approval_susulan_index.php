<?php
$role = $this->session->userdata('role');
$lokasis_id = $this->session->userdata('id_lokasis');
$A_01 = $this->session->userdata('A_01');
?>
<!-- Page content -->
<div class="page-content">
    
    <!-- Main content -->
    <div class="content-wrapper">
        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="page-header-content header-elements-md-inline">
                
            </div>
            
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
                <div class="d-flex">
                    <div class="breadcrumb">
                        <a href="<?=base_url('dashboard')?>" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Dashboard</a>
                        <span class="breadcrumb-item active">Gaji Susulan</span>
                    </div>
                    
                    <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
                </div>
                
                
            </div>
        </div>
        <!-- /page header -->
        
        
        <!-- Content area -->
        <div class="content">
            
            <!-- Basic table -->
            <div class="card">
                <div class="card-header header-elements-inline">
                    <h5 class="card-title">Data</h5>
                    <div class="header-elements">
                        <input type="hidden" name="rolenya" id="rolenya" value="<?=$role?>">
                        <input type="hidden" name="A_01" id="A_01" value="<?=$A_01?>">
                        <!-- <a href="javascript:void(0)" onclick="add()" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                        <a href="javascript:void(0)" onclick="ajukanData()" class="btn btn-labeled btn-labeled-right bg-success" style="margin-left: 8px">Setuju <b><i class="icon-check"></i></b></a>
                    </div>
                </div>
                
                <div class="card-body">
                    <table class="table table-stripe" id="tableContent">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Jenis <br>Susulan</th>
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Lama</th>
                                <th>Baru</th>
                                <th>Jumlah <br>Bulan</th>
                                <!-- <th>OPD</th> -->
                                <th>Start</th>
                                <th>End</th>
                                <!-- <th>Aksi</th> -->
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /basic table -->
            
            <!-- START MODAL DETAIL -->
            <div id="mdlAdd" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h6 class="modal-title">Tambah</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        
                        <form id="form_tambahsusulan">
                            <div class="modal-body">
                                <input type="hidden" name="idne" id="idne">
                                <!-- <hr> -->
                                <div class="form-group row col-md-12">
                                    <label>Jenis Susulan</label>
                                    <select class="form-control" name="jenis_susulan" id="jenis_susulan" required>
                                        <option value="">::Pilih::</option>
                                        <option value="1">Gaji Pokok</option>
                                        <option value="2">Fungsional</option>
                                    </select>
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>NIP</label>
                                    <select class="form-control" name="nips" id="nips" style="width:100%!important;" required></select>
                                    <input type="hidden" name="nipz" id="nipz">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Nama</label>
                                    <input type="text" class="form-control" name="nama" id="nama" readonly autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>SKPD</label>
                                    <input type="text" class="form-control" name="skpd" id="skpd" readonly autocomplete="off">
                                </div>
                                <div id="wrapFungsional">
                                    <div class="form-group row col-md-12">
                                        <label>Awal</label>
                                        <select class="form-control" name="fung_awl" id="fung_awl">
                                            <option value="">::Pilih::</option>
                                            <option value="1">Struktural</option>
                                            <option value="2">Fungsional Tertentu</option>
                                            <option value="3">Fungsional Umum</option>
                                        </select>
                                        <input type="hidden" name="f_awl" id="f_awl">
                                    </div>
                                    <div class="form-group row col-md-12">
                                        <label>Akhir</label>
                                        <select class="form-control" name="fung_akh" id="fung_akh">
                                            <option value="">::Pilih::</option>
                                            <option value="1">Struktural</option>
                                            <option value="2">Fungsional Tertentu</option>
                                            <option value="3">Fungsional Umum</option>
                                        </select>
                                        <input type="hidden" name="f_akh" id="f_akh">
                                    </div>
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Periode Start</label>
                                    <input type="date" class="form-control" name="periode_start" id="periode_start" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <label>Periode End</label>
                                    <input type="date" class="form-control" name="periode_end" id="periode_end" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12" id="kontenGajiLama">
                                    <div class="col-md-6">
                                        <label>Lama (Rp.)</label>
                                        <div id="gajiLama">
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Baru (Rp.)</label>
                                        <div id="gajiBaru">
                                            
                                        </div>
                                        <small>Gaji terakhir dari kalkulasi</small>
                                    </div>
                                    <div>
                                    </div>
                                </div>
                                
                                <div class="form-group row col-md-12">
                                    <label>Keterangan</label>
                                    <textarea type="text" class="form-control" name="keterangan" id="keterangan" required autocomplete="off"></textarea>
                                    <input type="hidden" name="lokasi_kode" id="lokasi_kode">
                                    <input type="hidden" name="jml_bl" id="jml_bl">
                                </div>
                                
                                <?php if($role == 4) {?>
                                <div class="form-group row col-md-12">
                                    <label>NIP / Nama TU Cabang Dinas</label>
                                    <input type="text" class="form-control" name="posisi_acc" id="posisi_acc" required autocomplete="off">
                                </div>
                                <div class="form-group row col-md-12">
                                    <div class="alert bg-info text-white alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert"><span>×</span></button>
                                        <span class="font-weight-semibold"><?=$info_sekolah['NIP']?> -- </span><?=$info_sekolah['message']?></a>.
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                            
                            
                            <div class="modal-footer">
                                <button id="btn_update" type="submit" class="btn btn-md btn-success">Simpan</button>
                                <!-- <a href="javascript:void(0)" onclick="hapusData()" class="btn btn-md btn-danger">Hapus</a> -->
                                <!-- <button type="button" class="btn btn-link" data-dismiss="modal">Close</button> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- END MODAL DETAIL -->
            
        </div>
        <!-- /content area -->
        
        
        
        
        <?php $this->load->view('template/footer');?>
        
        <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
        <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
        <script src="<?=base_url();?>assets/plugin/select2new/select2.min.js"></script>
        <!-- jquery-validation -->
        <script src="<?=base_url('assets/')?>plugin/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?=base_url('assets/')?>plugin/jquery-validation/additional-methods.min.js"></script>

        <script>
            var readDataUrl = '<?php echo site_url('approval_susulan/read');?>'

            var ajukanKepalaUlr = '<?php echo site_url('approval_susulan/ajukankepala_action');?>'
            var ajukanSekUrl = '<?php echo site_url('approval_susulan/ajukansek_action');?>'
            var ajukanKasubbagKeuUrl = '<?php echo site_url('approval_susulan/ajukankeu_action');?>'
            var ajukanCabdinUrl = '<?php echo site_url('approval_susulan/ajukancabdin_action');?>'
            var ajukanUrl = '<?php echo site_url('approval_susulan/ajukan_action');?>'
        </script>

        <script src="<?=base_url();?>assets/js/approval_gaji_susulan.min.js"></script>

    </div>
    <!-- /main content -->
    
</div>
<!-- /page content -->

</body>
</html>
