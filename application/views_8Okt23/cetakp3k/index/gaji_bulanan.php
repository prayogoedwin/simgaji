<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">
    

    <!-- Page header -->
    <div class="page-header page-header-light">
        <!-- <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4> <span class="font-weight-semibold"><?=$title?></span></h4>
                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>
           
        </div> -->

        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <!-- <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Master</a>
                    <span class="breadcrumb-item active">Pengumuman</span> -->
                    <?=$breadcrumb?>
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->

    



    <!-- Content area -->
    <div class="content">

     <!-- Form inputs -->
     
     <div class=" ">

    </div>
    <!-- /form inputs -->
        


    </div>
    <!-- /content area -->

   






    <?php $this->load->view('template/footer');?>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>
   


</div>
<!-- /main content -->

</div>
<!-- /page content -->

 <!-- Template Modal -->
 <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><?=$title?></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
       
             <!-- <form action="#"> -->
    <?php
          $attributes = array('class' => 'form-horizontal', 'target' => '_blank');
          echo form_open('cetakp3k/gaji_action', $attributes); ?>

    <div class="form-group row">

    <div class="col-lg-12">

        <?php if($session['role'] == 3) :?>
        <div class="form-group">
            Jenis Cetak
            <?php echo form_dropdown('jenis', jenisbulananp3k_array('Pilih Jenis Cetak', 'ADMIN SKPD'), '', ' id="jenis" class="form-control custom-select select2"   required'); ?>
        </div> 
        <?php else: ?>
        <div class="form-group">
            Jenis Cetak
            <?php echo form_dropdown('jenis', jenisbulananp3k_array('Pilih Jenis Cetak'), '', ' id="jenis" class="form-control custom-select select2"   required'); ?>
        </div> 
        <?php endif; ?>
        
        
        <div class="form-group">
            <div class="row">
            <div class="col-md-6">Periode Bulan: <select class="form-control select2" style="width: 100%;" required name="bulan_id" id="bulan_id">
                                
                                <?php for ($j = 1; $j <= 12; $j++) {
                                    $selected = '';
                                    $bln = sprintf("%02d", $j);
                                    // if ($month == $bln) {
                                    //     $selected = 'selected="selected"';
                                    // }
                                    echo '<option ' . $selected . ' value="' . $bln . '">' . namaBulan($j) . '</option>';
                                } ?>

                            </select></div>
            <div class="col-md-6">Tahun: <input name="tahun" class="form-control" value="<?=date('Y')?>"></div>
            </div>
        </div> 


        <?php if($session['role'] == 3) :?>
            <?php $kode8digit = get_lokasiSKPD($this->session->userdata('id_lokasis'))->kode; ?>
            <div class="form-group" >
            Lokasi Start 
            <?php echo form_dropdown('lokasi_start', opd_array_fix_array('Lokasi Start', 'kode', $session['role'], $kode8digit), '', ' id="lokasi_start" class="form-control custom-select select2"   required'); ?>
            </div>
        <?php else :?>
            <div class="form-group" >
            Lokasi Start 
            <?php echo form_dropdown('lokasi_start', opd_array_fix_array('Lokasi Start', 'kode', $session['role'], $session['pengampu'] ), '', ' id="lokasi_start" class="form-control custom-select select2"   '); ?>
            </div>
        <?php endif ;?>


        <?php if($session['role'] == 3) :?>
            <?php $kode8digit = get_lokasiSKPD($this->session->userdata('id_lokasis'))->kode; ?>
            <div class="form-group" >
            Lokasi End
            <?php echo form_dropdown('lokasi_end',  opd_array_fix_array('Lokasi End', 'kode', $session['role'], $kode8digit), '', ' id="lokasi_end" class="form-control custom-select select2"  '); ?>
            </div>
        <?php else: ?>
            <div class="form-group" >
            Lokasi End
            <?php echo form_dropdown('lokasi_end',  opd_array_fix_array('Lokasi End', 'kode', $session['role'], $session['pengampu'] ), '', ' id="lokasi_end" class="form-control custom-select select2"  '); ?>
            </div>
        <?php endif; ?>

        <div class="form-group">
            NIP
            <input type="text" class="form-control d-none" name="nip" id="nip">
        </div> 

        <div class="form-group">
        <div class="text-right">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="submit" id="btn-filter" class="btn btn-primary pull-right"> <i class="fa fa-save"></i> CETAK SEKARANG </button>
        </div>

        </div>

    </div>

    </div>

   <?php form_close(); ?>

      </div>

      <!-- Modal footer -->
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div> -->

    </div>
  </div>
</div>
<!-- Template Modal END -->

<script type="text/javascript">
    $(window).on('load', function() {
        $('#myModal').modal('show');
    });
</script>

<script>

$(function() {
    $('#jenis').on('change', function() {

    var jns = this.value;
    if(jns == 'REKAP GAJI PPPK KIRI KANAN' ||
    jns == 'REKAP GAJI PPPK PER LOKASI INDUK' ){
        // alert(this.value);
        $("#lokasi_end").prop('required',true);
    }
    });

    // if(jns == 'REKAP BPJS KESEHATAN PPPK'  ){
    //     alert(this.value);
    //     // $('#lokasi_end').prop('disabled', true);
    // }
 });

    

</script>

<script>

$(function() {
    $('#jenis').on('change', function() {



    var jns = this.value;
    if(jns == 'REKAP BPJS KESEHATAN PPPK' ||
    jns == 'REKAP BPJS JKK PPPK' ||
    jns == 'REKAP BPJS JKM PPPK')
    {
        // alert(this.value);
        $("#lokasi_end").prop('required',false);
        $('#lokasi_end').next(".select2-container").hide();
        $("#nip").addClass("d-none");

    }else if( jns == 'REKAP GAJI PPPK PER LOKASI INDUK'){
        
        $('#lokasi_start').next(".select2-container").show();
        $('#lokasi_end').next(".select2-container").hide();
        $("#nip").addClass("d-none");
        
    }else if(jns == 'REKAP GAJI PPPK SELURUH SKPD'){

        $("#lokasi_start").prop('required',false);
        var start = '01000000';
        $("#lokasi_start").val(start).trigger('change');
        $("#lokasi_end").prop('required',false);
        $('#lokasi_start').next(".select2-container").hide()
        $('#lokasi_end').next(".select2-container").hide()
        $("#nip").addClass("d-none");
        
    }else if(jns == 'GAJI PERSONAL'){
        $("#nip").removeClass("d-none");
        $("#lokasi_start").prop('required',false);
        $("#lokasi_end").prop('required',false);
        $('#lokasi_start').next(".select2-container").hide()
        $('#lokasi_end').next(".select2-container").hide()
        
    }else{
        $("#nip").addClass("d-none");
        $('#lokasi_end').next(".select2-container").show();
    }
    });

    
 });

$('#lokasi_start').on('change', function() {

var start = $('#lokasi_start').val();
$("#lokasi_end").val(start).trigger('change');

});




    

</script>







      

</body>
</html>