<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?=base_url() ?>assets/cetak/cetak.css" rel="stylesheet" type="text/css" />
<link href="<?=base_url() ?>assets/cetak/bootstrap.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
<div class="cetak">
<?

$tb_kalkulasi = 'simgaji_kalkulasip3ks';
$tb_lokasi = 'simgaji_lokasis';
$tb_golongan = 'simgaji_golonganp3ks';


$page_break = "<div style=\"page-break-after:always\"></div>";	

$xPeriode = explode("-",$periode);    	
// $bulan = ORM::factory('bulan',intval($xPeriode[1]));
$bulan = get_bulan($xPeriode[1]);

$i = 1;
$j = 1;
// $golongans = ORM::factory('golongan')->find_all();
$golongans = $this->db->get($tb_golongan)->result();

// $lokasis = ORM::factory('lokasi')->where('RIGHT("kode",6)','=','000000');
// $lokasis = $lokasis->find_all();


// $this->db->where('RIGHT("kode",6)',000000);
$this->db->like('kode', '000000', 'before'); 
$this->db->where('bool_id',2);

$lokasis = $this->db->get($tb_lokasi)->result();;

// $n_lokasi = $lokasis->reset(FALSE)->count_all();
// $this->db->where('RIGHT("kode",6)',000000);
$this->db->like('kode', '000000', 'before'); 
$this->db->where('bool_id',2);
$this->db->from($tb_lokasi);
$n_lokasi = $this->db->count_all_results();

$jumlah_halaman = ceil($n_lokasi / 8);


// echo json_encode($lokasis);
// die();

foreach($lokasis as $lokasi) {	
	if(($i%9) == 0 || $i == 1) {
		?>
		<table style="width:30.5cm" cellspacing="0" cellpadding="0">
			<tr>
				<td width="13%" align="left" valign="bottom">Hal. <? echo ceil($j/8)." / ".$jumlah_halaman; ?></td>
				<td width="43%" align="center" valign="top">REKAPITULASI PEGAWAI PROPINSI JAWA TENGAH<br />PER GOLONGAN / RUANG DISUSUN MENURUT INSTANSI<br />
	      													BERDASAR PENGGAJIAN  BULAN : <? echo strtoupper($bulan->name)." ".$xPeriode[0]; ?></td>
				<td width="13%" align="center" valign="top">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3" align="center" valign="top">&nbsp;</td>
			</tr>
		</table>
		<table  border="0" cellpadding="5" cellspacing="0" style="width:30.5cm; border-top:dashed 1px #000000">
			<tr>
				<td width="5%" valign="middle" align="center" class="borBottom">No</td>
				<td width="27%" align="center" valign="top" class="borBottom">INSTANSI</td>
				<td width="8%" valign="middle" align="center" class="borBottom">JENPEG</td>
				<td width="9%" align="center" valign="top" class="borBottom">GOLONGAN</td>
				<td width="8%" align="center" valign="top" class="borBottom">A</td>
				<td width="8%" align="center" valign="top" class="borBottom">B</td>
				<td width="10%" align="center" valign="top" class="borBottom">C</td>
				<td width="9%" valign="middle" align="center" class="borBottom">D</td>
				<td width="8%" valign="middle" align="center" class="borBottom">E</td>
				<td width="8%" valign="middle" align="center" class="borBottom">JUMLAH</td>
			</tr>
			<tr>
				<td colspan="10">&nbsp;</td>
			</tr>
		<?
	}
	?>
	<tr>
		<td width="5%" align="center"><? echo $j; ?></td>
		<td width="27%" valign="middle"><? echo $lokasi->name; ?></td>
		<td width="8%" align="center">PEG.DRH</td>
		<td width="9%" align="right" valign="top">GOL. I<br />GOL. II<br />GOL. III<br />GOL. IV<br />TOTAL</td>
        <?
		$g = 1;
		for($g=1;$g<6;$g++) {
			$n_golongan_id = "";
			for($h=1;$h<5;$h++) {
				$golongan_id = $h.$g;
				
				// $n_golongan_id_result = ORM::factory('kalkulasi')
				// 	->where('periode','=',$periode)
				// 	->where('golongan_id','=',$golongan_id)
				// 	->where('LEFT("lokasi_kode",2)','=',substr($lokasi->kode,0,2))
				// 	->count_all();
                

                $this->db->where('periode', $periode);
                $this->db->where('golongan_id', $golongan_id);
                $this->db->where('LEFT("lokasi_kode",2)', substr($lokasi->kode,0,2));
                $this->db->from($tb_kalkulasi);
                $n_golongan_id_result = $this->db->count_all_results();
					
				// $n_golongan_id .= Model::factory('custom')->numFormat($n_golongan_id_result)."<br>";		
                $n_golongan_id .= numFormat($n_golongan_id_result)."<br>";					
			}
			
			echo "<td align='right'>";
			echo $n_golongan_id;
			echo "&nbsp;";
			echo "</td>";
		}
		
		echo "<td width='8%' align='right' valign='top'>";	
		$n_golongan = "";
		for($h=1;$h<5;$h++) {
			// $n_golongan_result = ORM::factory('kalkulasi')
			// 	->where('periode','=',$periode)
			// 	->where('LEFT("golongan_id",1)','=',$h)
			// 	->where('LEFT("lokasi_kode",2)','=',substr($lokasi->kode,0,2))
			// 	->count_all();	
            
               

                $this->db->where('periode', $periode);
                $this->db->where('LEFT("golongan_id",1)', $h);
                $this->db->where('LEFT("lokasi_kode",2)', substr($lokasi->kode,0,2));
                $this->db->from($tb_kalkulasi);
                $n_golongan_result = $this->db->count_all_results();
				
			    // echo Model::factory('custom')->numFormat($n_golongan_result)."<br>";
                echo numFormat($n_golongan_result)."<br>";					
		}
		
		// $n_total = ORM::factory('kalkulasi')
		// 	->where('periode','=',$periode)
		// 	->where('LEFT("lokasi_kode",2)','=',substr($lokasi->kode,0,2))
		// 	->count_all();	
        
            $this->db->where('periode', $periode);
            $this->db->where('LEFT("lokasi_kode",2)', substr($lokasi->kode,0,2));
            $this->db->from($tb_kalkulasi);
            $n_total = $this->db->count_all_results();

           
			
		// echo Model::factory('custom')->numFormat($n_total);
		// echo "</td>";	
        echo numFormat($n_total);	
        echo "</td>";	
		?>
		
	</tr>
	<tr>
		<td colspan="10" height="10"></td>
	</tr>
	<?
	if(is_int($j/8)) {
		?>
		<tr>
			<td colspan="10" height="15"></td>
		</tr>
	<?	
	}
	$i++;
	$j++;
}

?>
	<tr>
		<td colspan="10"><hr /></td> 
	</tr>
	<tr>
		<td width="5%" align="center">&nbsp;</td>
		<td width="27%" valign="middle">JUMLAH SELURUH INSTANSI</td>
		<td width="8%" align="center">PEG.DRH</td>
		<td width="9%" align="right" valign="top">GOL. I<br />GOL. II<br />GOL. III<br />GOL. IV<br />TOTAL</td>
		<?
		$g = 1;
		for($g=1;$g<6;$g++) {
			$n_golongan_id = "";
			for($h=1;$h<5;$h++) {
				$golongan_id = $h.$g;
				
				// $n_golongan_id_result = ORM::factory('kalkulasi')
				// 	->where('periode','=',$periode)
				// 	->where('golongan_id','=',$golongan_id)
				// 	->count_all();
                $this->db->where('periode', $periode);
                $this->db->where('golongan_id', $golongan_id);
                $this->db->from($tb_kalkulasi);
                $n_golongan_id_result = $this->db->count_all_results();
					
				// $n_golongan_id .= Model::factory('custom')->numFormat($n_golongan_id_result)."<br>";
                $n_golongan_id .= numFormat($n_golongan_id_result)."<br>";								
			}
			
			echo "<td align='right'>";
			echo $n_golongan_id;
			echo "&nbsp;";
			echo "</td>";
		}
		
		echo "<td width='8%' align='right' valign='top'>";	
		$n_golongan = "";
		for($h=1;$h<5;$h++) {
			// $n_golongan_result = ORM::factory('kalkulasi')
			// 	->where('periode','=',$periode)
			// 	->where('LEFT("golongan_id",1)','=',$h)
			// 	->count_all();
                
                $this->db->where('periode', $periode);
                $this->db->where('LEFT("lokasi_kode",1)', $h);
                $this->db->from($tb_kalkulasi);
                $n_golongan_result = $this->db->count_all_results();
				
			// echo Model::factory('custom')->numFormat($n_golongan_result)."<br>";					
            echo numFormat($n_golongan_result)."<br>";	
		}
		
		// $n_total = ORM::factory('kalkulasi')
		// 	->where('periode','=',$periode)
		// 	->count_all();	

            $this->db->where('periode', $periode);
            $this->db->from($tb_kalkulasi);
            $n_total = $this->db->count_all_results();
			
		// echo Model::factory('custom')->numFormat($n_total);
        echo numFormat($n_total);
		echo "</td>";	
		?>
	</tr>			
</table>
</div>
</body>
</html>