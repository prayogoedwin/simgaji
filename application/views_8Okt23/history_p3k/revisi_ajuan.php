<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Revisi Ajuan</title>
</head>
<body>
    <?php
    $lbl = $kodes->name;
    $tipe_input = $kodes->input;
    // $namefield = $kodes->field;
    $sumber = $kodes->source;
    ?>
    
    <div>
        <span>Masukkan data revisi</span>
        <input type="hidden" name="id_history" id="id_history" value="<?=$sekmen?>">
        <hr>
        <?php if($tipe_input == 1) :?>
        <div class="form-group">
            <label for=""><?=$lbl?></label>
            <input type="text" name="val_baru" id="val_baru" autocomplete="off">
        </div>
        <?php elseif($tipe_input == 2) :?>
        <div class="form-group">
            <label for=""><?=$lbl?></label>
            <textarea class="form-control" name="val_baru" id="val_baru" cols="30" rows="10"></textarea>
        </div>
        <?php else :?>
        <div class="form-group">
            <label for=""><?=$lbl?></label>
            <select class="form-control" name="val_baru" id="val_baru">
                <?php foreach ($$sumber as $key => $value) :?>
                <option value="<?=$value->id?>"><?=$value->name?></option>
                <?php endforeach ;?>
            </select>
        </div>
        <?php endif; ?>
    </div>
</body>
</html>