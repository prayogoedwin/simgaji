<!-- Page content -->
<div class="page-content">

<!-- Main content -->
<div class="content-wrapper">

    <!-- Page header -->
    <div class="page-header page-header-light">
        <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
            <div class="d-flex">
                <div class="breadcrumb">
                    <a href="#" class="breadcrumb-item"><i class="icon-home2 mr-2"></i> Rekap P3K</a>
                    <!-- <span class="breadcrumb-item active">Pengumuman</span> -->
                </div>

                <a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>
            </div>

            
        </div>
    </div>
    <!-- /page header -->


    <!-- Content area -->
    <div class="content">

        <!-- Basic table -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Data</h5>
                <div class="header-elements">
                    <!-- <a href="<?=base_url('rekap_p3k/cetak')?>" target="_blank" class="btn btn-labeled btn-labeled-right bg-success">Cetak <b><i class="icon-print"></i></b></a> -->
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledt">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Kode</th>
                            <th>Mutasi</th>
                            <th>Isi Lama</th>
                            <th>Isi Baru</th>
                            <!-- <th>Kode <br>Operator</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($rekap as $key => $value) :?>
                            <tr>
                                <td><?=$key+1?></td>
                                <td><?=$value->nip?></td>
                                <td><?=$value->name?></td>
                                <td><?=$value->kode_id?></td>
                                <td><?=$value->mutasi?></td>
                                <td><?=cekKode($value->kode_id, $value->lama);?></td>
                                <td><?=cekKode($value->kode_id, $value->baru);?></td>
                                <!-- <td> ?? </td> -->
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /basic table -->

        <!-- START KUMULATIF -->
        <div class="card">
            <div class="card-header header-elements-inline">
                <h5 class="card-title">Rekap Kumulatif P3K</h5>
                <div class="header-elements">
                    <!-- <a href="http://localhost/simgaji/pengumuman/tambah" class="btn btn-labeled btn-labeled-right bg-primary">Tambah <b><i class="icon-file-plus2"></i></b></a> -->
                    <!-- <button type="button" class="btn btn-labeled btn-labeled-right bg-primary text-center" data-toggle="modal" data-target="#mdlAdd" >Tambah <b><i class="icon-file-plus2"></i></b></button> -->
                    <table class="table">
                        <tbody>
                            <tr>
                                <th><i class="icon-cog"></i> = Belum diproses (<?=$sum_kumulatif['blm']?>)</th>
                                <th><i style="color:red" class="icon-cancel-circle2"></i> = Ditolak (<?=$sum_kumulatif['tlk']?>)</th>
                                <th><i style="color:green" class="icon-checkmark-circle"></i> = Disetujui (<?=$sum_kumulatif['stj']?>)</th>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-stripe" id="tabledtKumulatif">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th><i class="icon-cog"></i></th>
                            <th><i style="color:red" class="icon-cancel-circle2"></i></th>
                            <th><i style="color:green" class="icon-checkmark-circle"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($kumulatif as $key => $val) :?>
                            <?php
                            $sumRevTolak = $val->revisi + $val->tolak;
                            ?>
                            <tr>
                                <td><?=$key+1?></td>
                                <td><?=$val->nip?></td>
                                <td><?=$val->name?></td>
                                <td><?=$val->belum?></td>
                                <td><?=$sumRevTolak?></td>
                                <td><?=$val->setuju?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END KUMULATIF -->

    </div>
    <!-- /content area -->




    <?php $this->load->view('template/footer');?>

    <script type="text/javascript" src="<?=base_url('assets/plugin/datatables/')?>datatables.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/notifications/sweet_alert.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/datatables.min.js"></script> 
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
    <script src="<?=base_url('assets/')?>global_assets/js/plugins/tables/datatables/extensions/buttons.min.js"></script>

</div>
<!-- /main content -->

</div>
<!-- /page content -->

<script>
    $(function () {
        $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Filter:</span> _INPUT_',
            searchPlaceholder: 'Type to filter...',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
        }
        });
        var tabel = $('#tabledt').DataTable({
            buttons: { 
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    // 'excelHtml5',
                    // 'csvHtml5',
                    { extend: 'pdfHtml5', title:'Rekap P3K SKPD'}
                ]
            },
            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false,
            columnDefs: [{ }],
        });

        tabel.on( 'order.dt search.dt', function () {
            tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('#tabledtKumulatif').DataTable({
            buttons: { 
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    // 'excelHtml5',
                    // 'csvHtml5',
                    { extend: 'pdfHtml5', title:'Rekap Kumulatif P3K SKPD'}
                ]
            },
            'destroy'     : true,
            'paging'      : true,
            'lengthChange': true,
            'searching'   : true,
            'info'        : true,
            'autoWidth'   : false,
            columnDefs: [{ }],
        });

    });
</script>

<script>
    
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?=$this->session->userdata("status");?>' == 'error') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'error'
        }).show();
    } else if ('<?=$this->session->userdata("status");?>' == 'success') {
        new Noty({
            text: '<?=$this->session->userdata("message")?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

</body>
</html>