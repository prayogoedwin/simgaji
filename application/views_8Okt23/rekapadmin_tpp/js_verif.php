<?php 
$bulan = $this->uri->segment(3);
$tahun = $this->uri->segment(4);


?>


<script>
    $(function() {
        $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
            dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            language: {
                search: '<span>Filter:</span> _INPUT_',
                searchPlaceholder: 'Type to filter...',
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {
                    'first': 'First',
                    'last': 'Last',
                    'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;',
                    'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;'
                }
            }
        });

        var tabel = $('#tabledt').DataTable({
            'lengthMenu': [
                [10, 25, 50, 100, -1],
                ['10', '25', '50', '100', 'Semua']
            ],
            buttons: {
                dom: {
                    button: {
                        className: 'btn btn-light'
                    }
                },
                buttons: [
                    // 'copyHtml5',
                    'excelHtml5',
                    // 'csvHtml5',
                    //  'pdfHtml5'
                    // {
					// 	text: 'Hapus',
					// 	className: 'btn',
					// 	action: function(e, dt, node, config) {
					// 		bulk_delete();
					// 	}
					// },
                ]
            },
            "oLanguage": {
                "sEmptyTable": "Data Tidak Ditemukan"
            },
            "pageLength": 10,
            'destroy': true,
            'paging': false,
            'lengthChange': true,
            'searching': true,
            'ordering': true,
            'info': false,
            'autoWidth': false,
            "serverSide": true,
            "ajax": {
                "url": "<?= base_url('rekapadmin_tpp/get_rekapp3ks_by_verifikator/'.$this->uri->segment(3)) ?>",
                "type": "POST",
            },


            'columns': [
                
                {
                    data: 'nip'
                },
                {
                    data: 'nama'
                },
                {
                    data: 'usulan'
                },
                {
                    data: 'proses'
                },
                {
                    data: 'belum'
                }, 
                {
                    data: 'mngampu'
                }, 
                
               
                // {
                //     data: 'id'
                // }
            ],

            columnDefs: [{}],
        });

        // tabel.on( 'order.dt search.dt', function () {
        //     tabel.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
        //         cell.innerHTML = i+1;
        //     } );
        // } ).draw();

    });

   
    
</script>


<script>
    function detailData(id) {
        // $('#mdlDetail').modal('show');
        $.ajax({
            url: "<?= base_url('verifikasi_p3ks/get_detail_verifikasi') ?>",
            type: "POST",
            data: {
                id: id
            },
            dataType: "json",
            success: function(data) {
                // alert('test');
                $('#your_modal_detail').html(data);
                $('#pegawai_id').val(id);
                $('#mdlDetail').modal('show');
                
            },
            error: function(xhr, ajaxOptions, thrownError) {
                swalInit.fire({
                    title: 'Oppss',
                    text: 'Terjadi kesalahan nih',
                    type: 'error',
                    onClose: function() {
                        $('#tabledt').DataTable().ajax.reload(null, false);
                    }
                });
            }
        });
    }

    $('#mdlDetail').on('hidden.bs.modal', function () {
                $("#scs").addClass("d-none");
                $("#dgr").addClass("d-none");
                $('#pegawai_id').val('');
    })
</script>

<script>
    $('#btn_update').on('click', function() {    

        var pegawai_id = $("input[name='pegawai_id']").val();

        var id = $("input[name='id[]']")
        .map(function(){return $(this).val();}).get();
        
        var lama = $("input[name='lama[]']")
        .map(function(){return $(this).val();}).get();

        var baru = $("input[name='baru[]']")
        .map(function(){return $(this).val();}).get();

        var description = $("textarea[name='description[]']")
        .map(function(){return $(this).val();}).get();

        var status_acc_bkd = $("select[name='status_acc_bkd[]']")
        .map(function(){return $(this).val();}).get();

        
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('verifikasi_p3ks/verifikator_bulk_update') ?>",
            dataType: "TEXT",
            // data: $('#msform').serialize(),
            data: {
                id: id,
                lama: lama,
                baru: baru,
                description: description,
                status_acc_bkd: status_acc_bkd,
                pegawai_id: pegawai_id
            },
            // data: updateArray,
            async:false,
            success: function(data) {

                // swalInit.fire({
                //     title: 'Oppss',
                //     text: 'Terjadi kesalahan nih',
                //     type: 'error',
                //     onClose: function() {
                //         $('#tabledt').DataTable().ajax.reload(null, false);
                //     }
                // });
                // detailData(pegawai_id);
                $("#scs").removeClass("d-none");
                $("#dgr").addClass("d-none");
                $('#tabledt').DataTable().ajax.reload(null, false);
                // console.log(data);
                
            },
            error: function(xhr, ajaxOptions, thrownError) {

                $("#scs").addClass("d-none");
                $("#dgr").removeClass("d-none");
                $('#tabledt').DataTable().ajax.reload(null, false);

                // console.log(data);
                // swalInit.fire({
                //     title: 'Oppss',
                //     text: 'Terjadi kesalahan nih',
                //     type: 'error',
                //     onClose: function() {
                //         $('#tabledt').DataTable().ajax.reload(null, false);
                //     }
                // });
            }
        });
        return false;
    });
</script>

<script>
    function hapusData(id) {

        var id = $('#id').val();

        var swalInit = swal.mixin({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        swalInit.fire({
            title: 'Apakah anda yakin?',
            text: "Anda akan menghapus data ini?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false
        }).then(function(result) {
            if (result.value) {
                // alert('proses ajax'); 
                $.ajax({
                    url: "<?= base_url('verifikator/verifikator_hapus_action') ?>",
                    type: "POST",
                    data: {
                        id: id
                    },
                    dataType: "json",
                    success: function(data) {
                        // console.log(data);

                        var sts = data['status'];
                        var msg = data['message'];

                        if (sts == 1) {
                            swalInit.fire({
                                title: 'Yeay',
                                text: msg,
                                type: 'success',
                                onClose: function() {
                                    $('#mdlDetail').modal('hide');
                                    $('#tabledt').DataTable().ajax.reload(null, false);
                                }
                            });
                        } else {
                            swalInit.fire(
                                'Cancelled',
                                msg,
                                'error'
                            );
                        }

                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        swalInit.fire({
                            title: 'Oppss',
                            text: 'Terjadi kesalahan nih',
                            type: 'error',
                            onClose: function() {
                                $('#mdlDetail').modal('hide');
                                $('#tabledt').DataTable().ajax.reload(null, false);
                            }
                        });
                    }
                });
            }
            // else if(result.dismiss === swal.DismissReason.cancel) {
            //     // swalInit.fire(
            //     //     'Cancelled',
            //     //     'Your imaginary file is safe :)',
            //     //     'error'
            //     // );
            // }
        });
    }
</script>

<script>
    if (typeof Noty == 'undefined') {
        console.warn('Warning - noty.min.js is not loaded.');
    }

    // Override Noty defaults
    Noty.overrideDefaults({
        theme: 'limitless',
        layout: 'topRight',
        type: 'alert',
        timeout: 2500
    });

    if ('<?= $this->session->userdata("status"); ?>' == 'error') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'error'
        }).show();
    } else if ('<?= $this->session->userdata("status"); ?>' == 'success') {
        new Noty({
            text: '<?= $this->session->userdata("message") ?>',
            type: 'success'
        }).show();
    }

    // $('#noty_success').on('click', function() {
    //     new Noty({
    //         text: 'You successfully read this important alert message.',
    //         type: 'success'
    //     }).show();
    // });
</script>

<script>
	//check all
	$("#check-all").click(function() {
		$(".data-check").prop('checked', $(this).prop('checked'));
	});


	function bulk_delete() {
		var list_id = [];
		$(".data-check:checked").each(function() {
			list_id.push(this.value);
		});
		if (list_id.length > 0) {
			Swal.fire({
				title: 'Anda yakin ingin menghapus?',
				// text: "You won't be able to revert this!",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonClass: "btn btn-success",
				cancelButtonClass: "btn btn-danger",
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then((result) => {
				if (result.isConfirmed) {
					$.ajax({
						type: "POST",
						data: {
							id: list_id
						},
						url: "<?php echo site_url('verifikator/verifikator_hapus_soft_action') ?>",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
								Swal.fire({
									icon: 'success',
									title: 'Berhasil Hapus Data',
									showConfirmButton: false,
									timer: 1000
								})
								$('#tabledt').DataTable().ajax.reload(null, false);
							} else {
								// alert('Failed.');
								Swal.fire({
									icon: 'danger',
									title: 'Gagal Hapus Data',
									showConfirmButton: false,
									timer: 1000
								})
							}

						},
						error: function(jqXHR, textStatus, errorThrown) {
							// alert('Error deleting data');
							Swal.fire(
								'Gagal!',
								'Gagal Hapus Data',
								'error'
							)
						}
					});

				}
			})

			// if (confirm('Are you sure delete this ' + list_id.length + ' data?')) {

			// }
		} else {
			Swal.fire({
				icon: 'warning',
				title: 'Tidak Ada Data Dipilih',
				showConfirmButton: false,
				timer: 1000
			})
			// alert('no data selected');
		}
	}
</script>